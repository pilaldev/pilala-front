import './wdyr';

import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import ThemeProvider from '@bit/pilala.pilalalib.styles.theme-provider';
import { BrowserRouter as Router } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import MainContainer from './containers/MainContainer';
import { store, persistor } from './redux/store';
import './App.css';

const App = () => {
  return (
    <>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <ThemeProvider>
            <Router>
              <div className="App">
                <MainContainer />
              </div>
            </Router>
          </ThemeProvider>
        </PersistGate>
      </Provider>
      <Helmet>
        <script type="text/javascript">
          {`
            window.$crisp=[];
            window.CRISP_WEBSITE_ID="84984036-bf08-4caf-b17e-3467c23eced1";
            (function(){
              d=document;
              s=d.createElement("script");
              s.src="https://client.crisp.chat/l.js";
              s.async=1;
              d.getElementsByTagName("head")[0].appendChild(s);
              })();
          `}
        </script>
      </Helmet>
    </>
  );
};

export default App;
