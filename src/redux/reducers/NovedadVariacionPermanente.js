import { VARIACION_PERMANENTE } from '../ActionTypes';
import { emptyRecordFields } from './../../constants/CotizantesFields';

const initialState = {
	openVariacionPermanenteModal: false,
	cotizanteName: '',
	cotizanteSalary: '',
	cotizanteDocumentNumber: '',
	cotizanteType: '',
	duracionDias: '',
	fechaInicio: '',
	dateStarted: '',
	newSalary: '',
	regLine: { ...emptyRecordFields },
	idCotizante: '',
	idNovedad: '',
	activeChanges: false,
	ajustePermanente: false,
	salariales: []
};

const novedadVariacionPermanente = (state = initialState, action) => {
	switch (action.type) {
		case VARIACION_PERMANENTE.VARIACION_PERMANENTE_MODAL_SET_STATUS:
			return {
				...state,
				openVariacionPermanenteModal: !state.openVariacionPermanenteModal
			};

		case VARIACION_PERMANENTE.VARIACION_PERMANENTE_RESET_DATA:
			return {
				...state,
				...initialState
			};

		case VARIACION_PERMANENTE.VARIACION_PERMANENTE_SET_COTIZANTE_DATA:
			return {
				...state,
				...action.payload.value
			};

		case VARIACION_PERMANENTE.VARIACION_PERMANENTE_SET_NOVEDAD_DATA:
			return {
				...state,
				...action.payload.value,
				regLine: {
					...state.regLine,
					...action.payload.value.regLine
				}
			};

		case VARIACION_PERMANENTE.VARIACION_PERMANENTE_UPDATE_NOVEDAD_FIELDS:
			const temp_salariales = JSON.parse(JSON.stringify(state.salariales));
			if (temp_salariales.some(item => item.type === 'Salarios')) {
				temp_salariales.forEach((item, index) => {
					if (item.nombreNovedad === 'Ajuste de salario permanente') {
						temp_salariales[index].duracionDias = action.payload.value.duracionDias;
						temp_salariales[index].fechaInicio = action.payload.value.fechaInicio;
						temp_salariales[index].value = {
							...temp_salariales[index].value,
							newSalary: JSON.parse(JSON.stringify(state.newSalary)),
							previousSalary: JSON.parse(JSON.stringify(state.cotizanteSalary)),
							dateApply: action.payload.value.fechaInicio,
							code: JSON.parse(JSON.stringify(state.newSalary)),
							value: JSON.parse(JSON.stringify(state.newSalary))
						}
					}
				});
			}
			else {
				temp_salariales.push({
					nombreNovedad: 'Ajuste de salario permanente',
					duracionDias: action.payload.value.duracionDias,
					fechaInicio: action.payload.value.fechaInicio,
					value: {
						dateCreated: '',
						dateApply: action.payload.value.fechaInicio,
						newSalary: JSON.parse(JSON.stringify(state.newSalary)),
						previousSalary: JSON.parse(JSON.stringify(state.cotizanteSalary)),
						code: JSON.parse(JSON.stringify(state.newSalary)),
						value: JSON.parse(JSON.stringify(state.newSalary))
					},
					type: 'Salarios'
				});
			}
			return {
				...state,
				dateStarted: action.payload.value.fechaInicio,
				fechaInicio: action.payload.value.fechaInicio,
				regLine: {
					...state.regLine,
					...action.payload.value.regLine
				},
				ajustePermanente: action.payload.value.ajustePermanente,
				salariales: temp_salariales,
				activeChanges: true
			}

		case VARIACION_PERMANENTE.VARIACION_PERMANENTE_UPDATE_NEW_SALARY:
			const currentSalariales = JSON.parse(JSON.stringify(state.salariales));
			if (currentSalariales.some(item => item.type === 'Salarios')) {
				currentSalariales.forEach((item, index) => {
					if (item.nombreNovedad === 'Ajuste de salario permanente') {
						currentSalariales[index].value = {
							...currentSalariales[index].value,
							newSalary: action.payload.value,
							previousSalary: JSON.parse(JSON.stringify(state.cotizanteSalary)),
							code: action.payload.value,
							value: action.payload.value
						}
					}
				});
			}

			return {
				...state,
				salariales: currentSalariales,
				newSalary: action.payload.value,
				activeChanges: true
			}
		
		case VARIACION_PERMANENTE.VARIACION_PERMANENTE_DELETE_NOVEDAD:
			const new_salariales = state.salariales.filter(item => item.nombreNovedad !== 'Ajuste de salario permanente');
			return {
				...state,
				dateStarted: '',
				salariales: new_salariales,
				idNovedad: '',
				activeChanges: false,
				cotizanteName: '',
				cotizanteDocumentNumber: '',
				cotizanteType: '',
				duracionDias: '',
				fechaInicio: '',
				newSalary: '',
				regLine: { ...emptyRecordFields },
				ajustePermanente: false,
			}

		default:
			return state;
	}
};

export default novedadVariacionPermanente;
