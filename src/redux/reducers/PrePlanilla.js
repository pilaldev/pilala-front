import { PRE_PLANILLA } from './../ActionTypes';

const initialState = {};

const prePlanilla = (state = initialState, action) => {
	switch (action.type) {
		case PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA: {
			return {
				...state,
				...action.payload.value
			};
		}

		case PRE_PLANILLA.PRE_PLANILLA_SET_FETCHED_DATA: {
			return {
				...state,
				[action.payload.value.companyNit]: {
					...state[action.payload.value.companyNit],
					[action.payload.value.periodoActual]: {
						...(state[action.payload.value.companyNit] || {})[action.payload.value.periodoActual],
						dependientes: {
							...(state[action.payload.value.companyNit]
								? state[action.payload.value.companyNit][action.payload.value.periodoActual]
									? state[action.payload.value.companyNit][action.payload.value.periodoActual]
										? state[action.payload.value.companyNit][action.payload.value.periodoActual][
												'dependientes'
											]
											? state[action.payload.value.companyNit][action.payload.value.periodoActual][
													'dependientes'
												]
											: {}
										: {}
									: {}
								: {}),
							[action.payload.value.idPlanilla]: {
								...(state[action.payload.value.companyNit]
									? state[action.payload.value.companyNit][action.payload.value.periodoActual]
										? state[action.payload.value.companyNit][action.payload.value.periodoActual]
											? state[action.payload.value.companyNit][action.payload.value.periodoActual][
													'dependientes'
												]
												? state[action.payload.value.companyNit][action.payload.value.periodoActual][
														'dependientes'
													]
													? state[action.payload.value.companyNit][
															action.payload.value.periodoActual
														]['dependientes'][action.payload.value.idPlanilla]
														? state[action.payload.value.companyNit][
																action.payload.value.periodoActual
															]['dependientes'][action.payload.value.idPlanilla]
														: {}
													: {}
												: {}
											: {}
										: {}
									: {}),
								...action.payload.value.data
							}
						}
					}
				}
			};
		}

		case PRE_PLANILLA.PRE_PLANILLA_UPDATE_COTIZANTE_FIELDS:
			return {
				...state,
				[action.payload.value.companyNit]: {
					...state[action.payload.value.companyNit],
					[action.payload.value.periodoActual]: {
						...(state[action.payload.value.companyNit] || {})[action.payload.value.periodoActual],
						dependientes: {
							...(state[action.payload.value.companyNit][action.payload.value.periodoActual] || {})[
								'dependientes'
							],
							[action.payload.value.idPlanilla]: {
								...(state[action.payload.value.companyNit][action.payload.value.periodoActual][
									'dependientes'
								] || {})[action.payload.value.idPlanilla],
								[action.payload.value.identificationNumber]: {
									...(state[action.payload.value.companyNit][action.payload.value.periodoActual][
										'dependientes'
									][action.payload.value.idPlanilla] || {})[
										action.payload.value.identificationNumber
									],
									...action.payload.value.cotizanteFields
								}
							}
						}
					}
				}
			};

		case PRE_PLANILLA.PRE_PLANILLA_UPDATE_NOVEDAD:
			return {
				...state,
				[action.payload.value.companyNit]: {
					...state[action.payload.value.companyNit],
					[action.payload.value.periodoActual]: {
						...(state[action.payload.value.companyNit] || {})[action.payload.value.periodoActual],
						dependientes: {
							...(state[action.payload.value.companyNit][action.payload.value.periodoActual] || {})[
								'dependientes'
							],
							[action.payload.value.idPlanilla]: {
								...(state[action.payload.value.companyNit][action.payload.value.periodoActual][
									'dependientes'
								] || {})[action.payload.value.idPlanilla],
								[action.payload.value.identificationNumber]: {
									...(state[action.payload.value.companyNit][action.payload.value.periodoActual][
										'dependientes'
									][action.payload.value.idPlanilla] || {})[
										action.payload.value.identificationNumber
									],
									...action.payload.value.data,
									[action.payload.value.field]: state[action.payload.value.companyNit][
										action.payload.value.periodoActual
									]['dependientes'][action.payload.value.idPlanilla][
										action.payload.value.identificationNumber
									][action.payload.value.field].map((item, index) => {
										if (item.id === action.payload.value.id) {
											return {
												...item,
												...action.payload.value.novedad
											};
										} else {
											return item;
										}
									})
								}
							}
						}
					}
				}
			};

		case PRE_PLANILLA.PRE_PLANILLA_REMOVE_NOVEDAD:
			return {
				...state,
				[action.payload.value.companyNit]: {
					...state[action.payload.value.companyNit],
					[action.payload.value.periodoActual]: {
						...(state[action.payload.value.companyNit] || {})[action.payload.value.periodoActual],
						dependientes: {
							...(state[action.payload.value.companyNit][action.payload.value.periodoActual] || {})[
								'dependientes'
							],
							[action.payload.value.idPlanilla]: {
								...(state[action.payload.value.companyNit][action.payload.value.periodoActual][
									'dependientes'
								] || {})[action.payload.value.idPlanilla],
								[action.payload.value.identificationNumber]: {
									...(state[action.payload.value.companyNit][action.payload.value.periodoActual][
										'dependientes'
									][action.payload.value.idPlanilla] || {})[
										action.payload.value.identificationNumber
									],
									...action.payload.value.data,
									[action.payload.value.field]: state[action.payload.value.companyNit][
										action.payload.value.periodoActual
									]['dependientes'][action.payload.value.idPlanilla][
										action.payload.value.identificationNumber
									][action.payload.value.field].filter(
										(item, index) => item.id !== action.payload.value.id
									)
								}
							}
						}
					}
				}
			};

		case PRE_PLANILLA.PRE_PLANILLA_ADD_NOVEDAD:
			return {
				...state,
				[action.payload.value.companyNit]: {
					...state[action.payload.value.companyNit],
					[action.payload.value.periodoActual]: {
						...(state[action.payload.value.companyNit] || {})[action.payload.value.periodoActual],
						dependientes: {
							...(state[action.payload.value.companyNit][action.payload.value.periodoActual] || {})[
								'dependientes'
							],
							[action.payload.value.idPlanilla]: {
								...(state[action.payload.value.companyNit][action.payload.value.periodoActual][
									'dependientes'
								] || {})[action.payload.value.idPlanilla],
								[action.payload.value.identificationNumber]: {
									...(state[action.payload.value.companyNit][action.payload.value.periodoActual][
										'dependientes'
									][action.payload.value.idPlanilla] || {})[
										action.payload.value.identificationNumber
									],
									...action.payload.value.data,
									[action.payload.value.field]: [
										...state[action.payload.value.companyNit][action.payload.value.periodoActual][
											'dependientes'
										][action.payload.value.idPlanilla][action.payload.value.identificationNumber][
											action.payload.value.field
										],
										action.payload.value.novedad
									]
								}
							}
						}
					}
				}
			};

		case PRE_PLANILLA.PRE_PLANILLA_REMOVE_COTIZANTE:
			let planilla = JSON.parse(
				JSON.stringify(
					state[action.payload.value.companyNit][action.payload.value.periodoActual]['dependientes'][
						action.payload.value.idPlanilla
					]
				)
			);

			delete planilla[action.payload.value.documentNumber];

			return {
				...state,
				[action.payload.value.companyNit]: {
					...state[action.payload.value.companyNit],
					[action.payload.value.periodoActual]: {
						...(state[action.payload.value.companyNit] || {})[action.payload.value.periodoActual],
						dependientes: {
							...(state[action.payload.value.companyNit][action.payload.value.periodoActual] || {})[
								'dependientes'
							],
							[action.payload.value.idPlanilla]: planilla
						}
					}
				}
			};

		case PRE_PLANILLA.PRE_PLANILLA_RESET_DATA:
			return {
				...initialState
			};

		case PRE_PLANILLA.PRE_PLANILLA_SAVE_DATA:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...(state[action.payload.value.activeCompany] || {}),
					[action.payload.value.periodoActual]: {
						...(state[action.payload.value.activeCompany] || {})[action.payload.value.periodoActual],
						dependientes: {
							...(state[action.payload.value.activeCompany][action.payload.value.periodoActual] || {})['dependientes'],
							[action.payload.value.idPlanilla]: action.payload.value.data
						}
					}
				}
			}

		default:
			return state;
	}
};

export default prePlanilla;
