import { COTIZANTE_INFO } from './../ActionTypes';

export const initialState = {};

const cotizantesInfo = (state = initialState, action) => {
	switch (action.type) {
		case COTIZANTE_INFO.COTIZANTE_INFO_SET_COTIZANTES_DATA:
			return {
				...state,
				...action.payload.value
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_ADD_NEW_COTIZANTE:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...state[action.payload.value.activeCompany],
					[action.payload.value.documentNumber]: {
						...(state[action.payload.value.activeCompany] || {})[action.payload.value.documentNumber],
						...action.payload.value.cotizanteData
					}
				}
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_TYPE:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...state[action.payload.value.activeCompany],
					[action.payload.value.documentNumber]: {
						...state[action.payload.value.activeCompany][action.payload.value.documentNumber],
						tipoCotizante: action.payload.value.data
					}
				}
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_STATUS:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...state[action.payload.value.activeCompany],
					[action.payload.value.documentNumber]: {
						...state[action.payload.value.activeCompany][action.payload.value.documentNumber],
						activeInCompany: action.payload.value.data
					}
				}
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_ARL_LEVEL:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...state[action.payload.value.activeCompany],
					[action.payload.value.documentNumber]: {
						...state[action.payload.value.activeCompany][action.payload.value.documentNumber],
						nivelArl: action.payload.value.data
					}
				}
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_EMAIL:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...state[action.payload.value.activeCompany],
					[action.payload.value.documentNumber]: {
						...state[action.payload.value.activeCompany][action.payload.value.documentNumber],
						email: action.payload.value.data
					}
				}
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_PHONE_NUMBER:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...state[action.payload.value.activeCompany],
					[action.payload.value.documentNumber]: {
						...state[action.payload.value.activeCompany][action.payload.value.documentNumber],
						mobileNumber: action.payload.value.data
					}
				}
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_DISCARD_EDITED_COTIZANTE_INFO:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...state[action.payload.value.activeCompany],
					[action.payload.value.documentNumber]: {
						...state[action.payload.value.activeCompany][action.payload.value.documentNumber],
						email: action.payload.value.data.email,
						mobileNumber: action.payload.value.data.mobileNumber
					}
				}
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_SALARY:
			return {
				...state,
				[action.payload.value.activeCompany]: {
					...state[action.payload.value.activeCompany],
					[action.payload.value.documentNumber]: {
						...state[action.payload.value.activeCompany][action.payload.value.documentNumber],
						salaryVariation: action.payload.value.salaryVariation,
						salary: action.payload.value.data,
						salaryHistory: [
							...state[action.payload.value.activeCompany][action.payload.value.documentNumber]
								.salaryHistory,
							action.payload.value.data
						]
					}
				}
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_DELETE_COTIZANTE:
			let cotizantes = JSON.parse(JSON.stringify(state[action.payload.value.activeCompany]));

			delete cotizantes[action.payload.value.documentNumber];

			return {
				...state,
				[action.payload.value.activeCompany]: cotizantes
			};

		case COTIZANTE_INFO.COTIZANTE_INFO_RESET_DATA:
			return {
				...initialState
			};

		default:
			return state;
	}
};

export default cotizantesInfo;
