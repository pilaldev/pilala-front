import { FETCH_DATA } from './../ActionTypes';

export const initialState = {
	requestMisDatos: 'PENDING',
	requestCompanies: 'PENDING',
	requestCotizantes: 'PENDING',
	requestLists: 'PENDING',
	requestStatus: 'PENDING',
	firstTimeFlag: null,
};

const FetchData = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_DATA.SET_REQUEST_STATUS:
			return {
				...state,
				[action.payload.field]: action.payload.value,
			};

		case FETCH_DATA.RESET_REQUEST_STATUS:
			return {
				...state,
				...initialState,
			};

		default:
			return state;
	}
};

export default FetchData;
