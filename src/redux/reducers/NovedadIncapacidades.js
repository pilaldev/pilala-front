import { INCAPACIDADES } from '../ActionTypes';

const initialState = {
  openIncapacidadesModal: false,
  cotizanteName: '',
  cotizanteDocumentNumber: '',
  tipoCotizante: '',
  totalDaysIGE: 0,
  totalDaysIRL: 0,
  activeChanges: false,
  idCotizante: '',
  IGE: [
    {
      nombreNovedad: 'IGE',
      duracionDias: '',
      fechaInicio: '',
      fechaFin: '',
      regLine: {},
      value: {
        fechaInicio: '',
        fechaFin: '',
        duracionDias: '',
      },
    },
  ],
  IRL: [
    {
      nombreNovedad: 'IRL',
      duracionDias: '',
      fechaInicio: '',
      fechaFin: '',
      regLine: {},
      value: {
        fechaInicio: '',
        fechaFin: '',
        duracionDias: '',
      },
    },
  ],
};

const novedadIncapacidades = (state = initialState, action) => {
  switch (action.type) {
    case INCAPACIDADES.INCAPACIDADES_OPEN_INCAPACIDADES_MODAL:
      return {
        ...state,
        openIncapacidadesModal: !state.openIncapacidadesModal,
      };

    case INCAPACIDADES.INCAPACIDADES_SET_COTIZANTE_DATA:
      return {
        ...state,
        ...action.payload.value,
      };

    case INCAPACIDADES.INCAPACIDADES_ADD_NOVEDAD:
      return {
        ...state,
        [action.payload.value.incapacidadType]: [
          ...state[action.payload.value.incapacidadType],
          {
            nombreNovedad: action.payload.value.incapacidadType,
            duracionDias: '',
            fechaInicio: '',
            fechaFin: '',
            regLine: {},
            value: {
              fechaInicio: '',
              fechaFin: '',
              duracionDias: '',
            },
          },
        ],
      };

    case INCAPACIDADES.INCAPACIDADES_UPDATE_NOVEDAD:
      return {
        ...state,
        totalDaysIGE:
          action.payload.value.incapacidadType === 'IGE'
            ? state.IGE.reduce((a, b, index) => {
                if (index !== action.payload.value.index) {
                  return a + b.duracionDias;
                }
                return a;
              }, 0) + action.payload.value.totalDaysIGE
            : state.totalDaysIGE,
        totalDaysIRL:
          action.payload.value.incapacidadType === 'IRL'
            ? state.IRL.reduce((a, b, index) => {
                if (index !== action.payload.value.index) {
                  return a + b.duracionDias;
                }
                return a;
              }, 0) + action.payload.value.totalDaysIRL
            : state.totalDaysIRL,
        [action.payload.value.incapacidadType]: state[
          action.payload.value.incapacidadType
        ].map((item, index) => {
          if (index === action.payload.value.index) {
            return {
              ...item,
              ...action.payload.value.novedad,
              value: {
                ...item.value,
                ...action.payload.value.novedad.value,
              },
              regLine: {
                ...item.regLine,
                ...action.payload.value.novedad.regLine,
              },
            };
          }
          return item;
        }),
      };

    case INCAPACIDADES.INCAPACIDADES_REMOVE_NOVEDAD: {
      const daysKey = `totalDays${action.payload.value.incapacidadType}`;
      if (state[action.payload.value.incapacidadType].length === 1) {
        return {
          ...state,
          [daysKey]: 0,
          [action.payload.value.incapacidadType]: state[
            action.payload.value.incapacidadType
          ].map(() => {
            return {
              nombreNovedad: action.payload.value.incapacidadType,
              duracionDias: '',
              fechaInicio: '',
              fechaFin: '',
              regLine: {},
              value: {
                fechaInicio: '',
                fechaFin: '',
                duracionDias: '',
              },
            };
          }),
        };
      }
      return {
        ...state,
        [daysKey]:
          state[daysKey] -
          state[action.payload.value.incapacidadType][
            action.payload.value.index
          ].duracionDias,
        [action.payload.value.incapacidadType]: state[
          action.payload.value.incapacidadType
        ].filter((item, index) => index !== action.payload.value.index),
      };
    }

    case INCAPACIDADES.INCAPACIDADES_RESET_DATA:
      return {
        ...state,
        ...initialState,
      };

    case INCAPACIDADES.INCAPACIDADES_ENABLE_ACTIVE_CHANGES:
      return {
        ...state,
        activeChanges: !state.activeChanges,
      };

    default:
      return state;
  }
};

export default novedadIncapacidades;
