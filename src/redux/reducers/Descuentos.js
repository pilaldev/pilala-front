import { DESCUENTOS } from '../ActionTypes';

const initialState = {
	openedDescuentosARLModal: false,
	totalAportesARL: 0,
	totalDescuentosARL: 0,
	descuentosARL: [
		{
			authorizationNumber: '',
			ARLDiscountValue: '',
			otherRiskDiscountValue: '',
			creditNoteDiscontValue: '',
			repeatUntilchecked: false,
			repeatUntilDate: ''
		}
	]
};

const descuentosARL = (state = initialState, action) => {
	switch (action.type) {
		case DESCUENTOS.DESCUENTOS_SET_FETCHED_DATA:
			return {
				...state,
				...action.payload.value
			};

		case DESCUENTOS.DESCUENTOS_CHANGE_MODAL_STATUS: {
			return {
				...state,
				openedDescuentosARLModal: !state.openedDescuentosARLModal
			};
		}

		case DESCUENTOS.DESCUENTOS_ADD_DESCUENTO:
			return {
				...state,
				descuentosARL: [
					...state.descuentosARL,
					{
						authorizationNumber: '',
						ARLDiscountValue: '',
						otherRiskDiscountValue: '',
						creditNoteDiscontValue: '',
						repeatUntilchecked: false,
						repeatUntilDate: ''
					}
				]
			};

		case DESCUENTOS.DESCUENTOS_UPDATE_DESCUENTO:
			return {
				...state,
				// totalDescuentosARL: action.payload.value.incrementTotal
				// 	? state.totalDescuentosARL + action.payload.value.incrementTotal
				// 	: state.totalDescuentosARL,
				descuentosARL: state.descuentosARL.map((item, index) => {
					if (index === action.payload.value.index) {
						return {
							...item,
							[action.payload.value.field]: action.payload.value.data
						};
					} else {
						return item;
					}
				})
			};

		case DESCUENTOS.DESCUENTOS_REMOVE_DESCUENTO:
			return {
				...state,
				descuentosARL: state.descuentosARL.filter((item, index) => index !== action.payload.value.index)
			};

		case DESCUENTOS.DESCUENTOS_CLEAN_DESCUENTO:
			return {
				...state,
				descuentosARL: state.descuentosARL.map((item, index) => {
					if (index === action.payload.value.index) {
						return {
							...item,
							authorizationNumber: '',
							ARLDiscountValue: '',
							otherRiskDiscountValue: '',
							creditNoteDiscontValue: '',
							repeatUntilchecked: false,
							repeatUntilDate: ''
						};
					} else {
						return item;
					}
				})
			};

		case DESCUENTOS.DESCUENTOS_RESET_DATA:
			return {
				...state,
				...initialState
			};

		default:
			return state;
	}
};

export default descuentosARL;
