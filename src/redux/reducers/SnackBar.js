import { SNACKBAR } from '../ActionTypes';

const initialState = {
  isOpened: false,
  title: '',
  description: '',
  color: 'primary',
};

const snackBar = (state = initialState, action) => {
  switch (action.type) {
    case SNACKBAR.CLOSE_SNACKBAR:
      return {
        ...state,
        isOpened: false,
      };

    case SNACKBAR.OPEN_SNACKBAR:
      return {
        ...state,
        isOpened: true,
        ...action.payload.value,
      };

    case SNACKBAR.RESET_SNACKBAR:
      return {
        ...state,
        title: '',
        description: '',
        color: 'primary',
      };
    default:
      return state;
  }
};

export default snackBar;
