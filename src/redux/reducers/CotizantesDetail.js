import { COTIZANTES_DETAIL } from './../ActionTypes';

const initialState = {
	tipoCotizante: {},
	name: {},
	id: null,
	documentNumber: '',
	mobileNumber: '',
	email: '',
	salary: '',
	newSalary: '',
	newSalaryStartDate: '',
	disabledSalaryEdit: true,
	disabledInfoEdit: true,
	changeSalaryModalStatus: false,
	confirmSalaryChangeModalStatus: false,
	editedDataIsSaved: null
};

const SaveCotizanteDetailData = (state = initialState, action) => {
	switch (action.type) {
		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_DATA:
			return {
				...state,
				tipoCotizante: action.payload.value.type,
				id: action.payload.value.id,
				documentNumber: action.payload.value.documentNumber,
				disabledSalaryEdit: true,
				disabledInfoEdit: true
			};

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_EDIT_SALARY:
			return {
				...state,
				disabledSalaryEdit: !state.disabledSalaryEdit
			};

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_EDIT_INFO:
			return {
				...state,
				disabledInfoEdit: !state.disabledInfoEdit
			};

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_COTIZANTE_DATA: {
			return {
				...state,
				name: action.payload.value.name,
				mobileNumber: action.payload.value.mobileNumber,
				email: action.payload.value.email,
				salary: action.payload.value.salary
			};
		}

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_SAVED_DATA_STATUS: {
			return {
				...state,
				editedDataIsSaved: action.payload.value
			};
		}

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_RESET_DATA: {
			return {
				...state,
				...initialState
			};
		}

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_CHANGE_MODAL_STATUS: {
			return {
				...state,
				changeSalaryModalStatus: !state.changeSalaryModalStatus
			};
		}

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_CHANGE_CONFIRM_MODAL_STATUS: {
			return {
				...state,
				confirmSalaryChangeModalStatus: !state.confirmSalaryChangeModalStatus
			};
		}

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_NEW_SALARY: {
			return {
				...state,
				newSalary: action.payload.value
			};
		}

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_SALARY: {
			return {
				...state,
				salary: action.payload.value
			};
		}

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_NEW_SALARY_START_DATE: {
			return {
				...state,
				newSalaryStartDate: action.payload.value
			};
		}

		case COTIZANTES_DETAIL.COTIZANTES_DETAIL_RESET_EDIT_SALARY_DATA: {
			return {
				...state,
				newSalary: '',
				newSalaryStartDate: ''
			};
		}

		default:
			return state;
	}
};

export default SaveCotizanteDetailData;
