import { VARIACION_TRANSITORIA } from '../ActionTypes';
import { emptyRecordFields /*editRecordField*/ } from './../../constants/CotizantesFields';

const initialState = {
	// openedEditableTextfield: false,
	// editedData: false,
	// index: null,
	// cotizanteName: '',
	// cotizanteTransitorySalary: '',
	// cotizanteDocumentNumber: '',
	// nombreNovedad: 'VST',
	// duracionDias: '30',
	// regLine: { ...emptyRecordFields }
	// novedades: []
};

const novedadVariacionTransitoria = (state = initialState, action) => {
	switch (action.type) {
		case VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_SET_COTIZANTE_DATA:
			return {
				...state,
				[action.payload.value.documentNumber]: {
					openedEditableTextfield: false,
					editedData: false,
					existing: action.payload.value.index >= 0 ? true : false,
					preLoadedData: action.payload.value.index >= 0 ? true : false,
					opened: false,
					index: action.payload.value.index,
					cotizanteName: '',
					cotizanteTransitorySalary: action.payload.value.transitorySalary,
					cotizanteDocumentNumber: '',
					nombreNovedad: 'VST',
					duracionDias: '30',
					regLine: { ...emptyRecordFields },
					requestStatus: false,
					...action.payload.value.novedad
				}
			};

		case VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_UPDATE_NOVEDAD:
			return {
				...state,
				[action.payload.value.documentNumber]: {
					...state[action.payload.value.documentNumber],
					...action.payload.value.data
				}
			};

		case VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_UPDATE_NOVEDAD_STATUS:
			return {
				...state,
				[action.payload.value.documentNumber]: {
					...state[action.payload.value.documentNumber],
					// existing: !state[action.payload.value.documentNumber].existing,
					opened: !state[action.payload.value.documentNumber].opened
				}
			};

		case VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_SET_TRANSITORY_SALARY:
			return {
				...state,
				[action.payload.value.documentNumber]: {
					...state[action.payload.value.documentNumber],
					cotizanteTransitorySalary: action.payload.value.salary,
					editedData: true
				}
			};

		case VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_SET_REQUEST_STATUS:
			return {
				...state,
				[action.payload.value.documentNumber]: {
					...state[action.payload.value.documentNumber],
					requestStatus: !state[action.payload.value.documentNumber].requestStatus
				}
			};

		case VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_RESET_DATA:
			return {
				...initialState
			};

		default:
			return state;
	}
};

export default novedadVariacionTransitoria;
