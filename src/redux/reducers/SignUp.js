import { SIGN_UP } from './../ActionTypes';

const initialState = {
    name: '',
    documentType: 'CC',
    documentNumber: '',
    genre: {
        value: 0,
        display: 'Masculino'
    },
    phoneNumber: '',
    province: {
        code: '',
        display: '',
        value: ''
    },
    city: {
        code: '',
        display: '',
        provinceCode: '',
        value: ''
    },
    email: '',
    userPassword: '',
    companyNit: '',
    showPassword: false,
    currentForm: 'empresas'
}

const saveSignUpData = (state = initialState, action) => {
    switch (action.type) {

        case SIGN_UP.SIGN_UP_GET_USER_FULL_NAME:
            return {
                ...state,
                name: action.payload.value
            }

        case SIGN_UP.SIGN_UP_GET_TYPE_USER_ID:
            return {
                ...state,
                documentType: action.payload.value
            }

        case SIGN_UP.SIGN_UP_GET_USER_ID:
            return {
                ...state,
                documentNumber: action.payload.value
            }

        case SIGN_UP.SIGN_UP_GET_USER_PHONE_NUMBER:
            return {
                ...state,
                phoneNumber: action.payload.value
            }

        case SIGN_UP.SIGN_UP_GET_USER_GENRE:
            return {
                ...state,
                genre: action.payload.value
            }
        
        case SIGN_UP.SIGN_UP_GET_USER_EMAIL:
            return {
                ...state,
                email: action.payload.value
            }

        case SIGN_UP.SIGN_UP_GET_USER_PROVINCE:
            return {
                ...state,
                province: action.payload.value
            }

        case SIGN_UP.SIGN_UP_GET_USER_CITY:
            return {
                ...state,
                city: action.payload.value
            }

        case SIGN_UP.SIGN_UP_GET_USER_PASSWORD:
            return {
                ...state,
                userPassword: action.payload.value
            }

        case SIGN_UP.SIGN_UP_SHOW_PASSWORD_CONTENT:
            return {
                ...state,
                showPassword: !state.showPassword
            }

        case SIGN_UP.SIGN_UP_GET_COMPANY_NIT:
            return {
                ...state,
                companyNit: action.payload.value
            }

        case SIGN_UP.SIGN_UP_SET_FORM:
            return {
                ...state,
                currentForm: action.payload.value
            }

        case SIGN_UP.SIGN_UP_RESET_FIELDS:
            return {
                ...state,
                name: '',
                documentType: 'CC',
                documentNumber: '',
                genre: {
                    value: 0,
                    display: 'Masculino'
                },
                phoneNumber: '',
                province: {
                    code: '',
                    display: '',
                    value: ''
                },
                city: {
                    code: '',
                    display: '',
                    provinceCode: '',
                    value: ''
                },
                email: ''
            }
        
        default:
            return state;
    }
}

export default saveSignUpData;