import { PLANILLA_DETAIL } from './../ActionTypes';

const initialState = {
	openedModal: false,
	tipoPlanilla: '',
	periodoPlanilla: '',
	idPlanilla: '',
	cotizantes: [],
	numeroPlanilla: 0,
	selectedCotizantes: []
};

const planillaDetail = (state = initialState, action) => {
	switch (action.type) {
		case PLANILLA_DETAIL.PLANILLA_DETAIL_SET_DATA:
			return {
				...state,
				...action.payload.value
			};

		case PLANILLA_DETAIL.PLANILLA_DETAIL_SET_MODAL_STATUS:
			return {
				...state,
				openedModal: action.payload.value
			};

		case PLANILLA_DETAIL.PLANILLA_DETAIL_SET_SELECTED_COTIZANTES: {
			return {
				...state,
				selectedCotizantes: action.payload.value
			};
		}

		case PLANILLA_DETAIL.PLANILLA_DETAIL_SET_PLANILLA_NUMBER: {
			return {
				...state,
				numeroPlanilla: action.payload.value
			};
		}

		case PLANILLA_DETAIL.PLANILLA_DETAIL_RESET_DATA:
			return {
				...state,
				...initialState
			};

		default:
			return state;
	}
};

export default planillaDetail;
