import { RETIRO } from './../ActionTypes';
import { emptyRecordFields } from './../../constants/CotizantesFields';

const initialState = {
    openRetiroModal: false,
    cotizanteName: '',
    cotizanteSalary: '',
    /* Campos novedad*/
    dateRetirement: '',
    regLine: {
        ...emptyRecordFields
    },
    retired: false,
    showConfirmModal: false,
    basicas: [],
    idNovedad: '',
    activeChanges: false,
    idCotizante: '',
    cotizanteDocumentNumber: '',
    tipoCotizante: ''
}

const saveNovedadRetiro = (state = initialState, action) => {
    switch (action.type) {

        case RETIRO.RETIRO_OPEN_RETIRO_MODAL:
            return {
                ...state,
                openRetiroModal: !state.openRetiroModal
            }

        case RETIRO.RETIRO_RESET_NOVEDAD_FIELDS:
            return {
                ...state,
                ...initialState
                // cotizanteName: '',
                // cotizanteSalary: '',
                // dateRetirement: '',
                // regLine: {
                //     ...emptyRecordFields
                // },
                // retired: false,
                // basicas: [],
                // idNovedad: '',
                // activeChanges: false,
                // idCotizante: ''
            }

        case RETIRO.RETIRO_SAVE_NOVEDAD_FIELDS:
            return {
                ...state,
                ...action.payload.value,
                regLine: {
                    ...state.regLine,
                    ...action.payload.value.regLine
                }
            }

        case RETIRO.RETIRO_UPDATE_NOVEDAD_FIELDS:
            const temp_basicas = JSON.parse(JSON.stringify(state.basicas));
            if (temp_basicas.some(item => item.type === 'Retiros')) {
                temp_basicas.forEach((item, index) => {
                    if (item.type === 'Retiros') {
                        temp_basicas[index].nombreNovedad = 'Novedad de retiro';
                        temp_basicas[index].duracionDias = 0;
                        temp_basicas[index].fechaInicio = action.payload.value.dateRetirement;
                        temp_basicas[index].fechaFin = action.payload.value.dateRetirement;
                        temp_basicas[index].value = action.payload.value.dateRetirement
                    }
                });
            }
            else {
                temp_basicas.push({
                    nombreNovedad: 'Novedad de retiro',
                    duracionDias: 0,
                    fechaInicio: action.payload.value.dateRetirement,
                    fechaFin: action.payload.value.dateRetirement,
                    value: action.payload.value.dateRetirement,
                    type: 'Retiros'
                });
            }
            return {
                ...state,
                dateRetirement: action.payload.value.dateRetirement,
                regLine: {
                    ...state.regLine,
                    ...action.payload.value.regLine
                },
                retired: action.payload.value.retired,
                basicas: temp_basicas,
                activeChanges: true
            }

        case RETIRO.RETIRO_SHOW_CONFIRM_MODAL:
            return {
                ...state,
                showConfirmModal: !state.showConfirmModal
            }

        case RETIRO.RETIRO_DELETE_NOVEDAD:
            const new_basicas = state.basicas.filter(item => item.nombreNovedad !== 'Retiro');
            return {
                ...state,
                dateRetirement: '',
                retired: false,
                basicas: new_basicas,
                idNovedad: '',
                activeChanges: false,
                regLine: {
                    ...state.regLine,
                    ...action.payload.value.regLine
                },
            }

        default:
            return state;
    }
}

export default saveNovedadRetiro;