import { PLANILLAS_HISTORY } from "../ActionTypes";

const initialState = {
    planillas: []
}

export default (state = initialState, action) => {
    switch (action.type) {

        case PLANILLAS_HISTORY.PLANILLAS_HISTORY_SAVE_PLANILLAS:
            return {
                ...state,
                planillas: action.payload.value
            }

        default:
            return state;
    }
}
