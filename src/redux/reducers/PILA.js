import { PAY_PILA } from '../ActionTypes';

export const initialState = {
  currentStep: 1,
  validatePila: false,
  vacacionesModalStatus: false,
  incapacidadesModalStatus: false,
  licenciasModalStatus: false,
  suspensionesModalStatus: false,
  completedStep: 1,
  fullAmount: 0,
  initialDate: 'DD/MM/AAAA',
  finalDate: 'DD/MM/AAAA',
  validationFailed: false,
  paidCurrentPila: false,
  planillas: [],
  currentPlanilla: null,
  tableKey: 0,
  showSendPayButtonModal: false,
  showConfirmSendPayButtonModal: false,
  payButtonEmail: '',
  currentRowIndex: -1,
  areCredentialsEnabled: false,
  lastPilaFilePath: null,
};

const savePILAData = (state = initialState, action) => {
  switch (action.type) {
    case PAY_PILA.PAY_PILA_CHANGE_CURRENT_STEP:
      return {
        ...state,
        currentStep: action.payload.value,
      };

    case PAY_PILA.PAY_PILA_VALIDATE_CURRENT_PILA:
      return {
        ...state,
        validatePila: !state.validatePila,
      };

    case PAY_PILA.PAY_PILA_CHANGE_MODAL_STATUS:
      return {
        ...state,
        [action.payload.value.field]: !state[action.payload.value.field],
      };

    case PAY_PILA.PAY_PILA_CHANGE_COMPLETED_STEP:
      return {
        ...state,
        completedStep: action.payload.value,
      };

    case PAY_PILA.PAY_PILA_CHANGE_FULL_AMOUNT:
      return {
        ...state,
        fullAmount: state.fullAmount + +action.payload.value,
      };

    case PAY_PILA.PAY_PILA_SAVE_INITIAL_DATE:
      return {
        ...state,
        initialDate: action.payload.value,
      };

    case PAY_PILA.PAY_PILA_SAVE_FINAL_DATE:
      return {
        ...state,
        finalDate: action.payload.value,
      };

    case PAY_PILA.PAY_PILA_CHANGE_VALIDATION_STATUS:
      return {
        ...state,
        validationFailed: !state.validationFailed,
      };

    case PAY_PILA.PAY_PILA_CHANGE_PAID_PILA_STATUS:
      return {
        ...state,
        paidCurrentPila: !state.paidCurrentPila,
      };

    case PAY_PILA.PAY_PILA_ADD_PLANILLA_TO_PAY:
      return {
        ...state,
        planillas: [...state.planillas, action.payload.value],
      };

    case PAY_PILA.PAY_PILA_CALCULATE_FULL_AMOUNT: {
      const sumPlanillas = state.planillas.reduce((total, item) => {
        return total + item.amount;
      }, 0);
      return {
        ...state,
        fullAmount: sumPlanillas,
      };
    }

    case PAY_PILA.PAY_PILA_SET_CURRENT_PLANILLA:
      return {
        ...state,
        currentPlanilla: action.payload.value,
      };

    case PAY_PILA.PAY_PILA_SET_TABLE_KEY:
      return {
        ...state,
        tableKey: state.tableKey + 1,
      };

    case PAY_PILA.PAY_PILA_RESET_FIELDS:
      return {
        ...state,
        ...initialState,
      };

    case PAY_PILA.PAY_PILA_SHOW_SEND_PAY_BUTTON_MODAL:
      return {
        ...state,
        showSendPayButtonModal: !state.showSendPayButtonModal,
      };

    case PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL:
      return {
        ...state,
        showConfirmSendPayButtonModal: !state.showConfirmSendPayButtonModal,
      };

    case PAY_PILA.PAY_PILA_SAVE_PAY_BUTTON_EMAIL:
      return {
        ...state,
        payButtonEmail: action.payload.value,
      };

    case PAY_PILA.PAY_PILA_SAVE_CURRENT_ROW_INDEX:
      return {
        ...state,
        currentRowIndex: action.payload.value,
      };

    case PAY_PILA.UPDATE_CREDENTIALS_ENABLED:
      return {
        ...state,
        areCredentialsEnabled: action.payload.value,
      };

    case PAY_PILA.SAVE_LAST_PILA_FILE_PATH:
      return {
        ...state,
        lastPilaFilePath: action.payload.value,
      };

    default:
      return state;
  }
};

export default savePILAData;
