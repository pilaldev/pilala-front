import { RECORDS } from './../ActionTypes';
import { editRecordField, emptyRecordFields } from './../../constants/CotizantesFields';

const initialState = {
    cotizantesRecords: {},
    numberRecords: 0
}

const saveCotizantesRecords = (state = initialState, action) => {
    switch (action.type) {

        case RECORDS.CREATE_NEW_RECORD:
            let ref_cotizante = null;
            try {
                ref_cotizante = typeof state.cotizantesRecords[action.payload.value.companyNit] === undefined ? 0 :
                typeof state.cotizantesRecords[action.payload.value.companyNit][action.payload.value.cotizanteId] === undefined ? 0 :
                Object.keys(state.cotizantesRecords[action.payload.value.companyNit][action.payload.value.cotizanteId]).length;
            }
            catch (error) {
                ref_cotizante = 0;
            }
            const new_record_id = `${action.payload.value.recordId}_${ref_cotizante}`;
            const newFields = action.payload.value.newFields;
            let temp_new_fields = {};
            if (typeof action.payload.value.newFields === 'object') {
                Object.keys(newFields).map((key) => (
                    temp_new_fields = {
                        ...temp_new_fields,
                        [key]: editRecordField[key](newFields[key])
                    }
                ));
            }
            return {
                ...state,
                cotizantesRecords: {
                    ...state.cotizantesRecords,
                    [action.payload.value.companyNit]: {
                        ...state.cotizantesRecords[action.payload.value.companyNit],
                        [action.payload.value.cotizanteId]: {
                            ...(state.cotizantesRecords[action.payload.value.companyNit] || {})[action.payload.value.cotizanteId],
                            [new_record_id]: {
                                recordId: new_record_id,
                                recordType: action.payload.value.recordId,
                                ...emptyRecordFields,
                                ...temp_new_fields
                            }
                        }
                    }
                }
            }

        case RECORDS.UPDATE_RECORD:
            const updatedFields = action.payload.value.updatedFields;
            let temp_updated_fields = {};
            Object.keys(updatedFields).map((key) => (
                temp_updated_fields = {
                    ...temp_updated_fields,
                    [key]: editRecordField[key](updatedFields[key])
                }
            ));
            return {
                ...state,
                cotizantesRecords: {
                    ...state.cotizantesRecords,
                    [action.payload.value.companyNit]: {
                        ...state.cotizantesRecords[action.payload.value.companyNit],
                        [action.payload.value.cotizanteId]: {
                            ...(state.cotizantesRecords[action.payload.value.companyNit] || {})[action.payload.value.cotizanteId],
                            [action.payload.value.recordId]: {
                                ...state.cotizantesRecords[action.payload.value.companyNit][action.payload.value.cotizanteId][action.payload.value.recordId],
                                ...temp_updated_fields
                            }
                        }
                    }
                }
            }

        default:
            return state;
    }
}

export default saveCotizantesRecords;