import { COMPANY } from './../ActionTypes';
import { FIELDS_ID } from './../../company/Constants';

export const initialState = {
	companyName: '',
	economicActivity: '',
	province: {
		code: '',
		display: '',
		value: ''
	},
	city: {
		code: '',
		display: '',
		provinceCode: '',
		value: ''
	},
	phoneNumber: '',
	cellphoneNumber: '',
	identificationType: {
		code: 'NI',
		display: 'NIT'
	},
	identificationNumber: '',
	verificationDigit: '',
	address: {
		direccionCompleta: '',
		informacionAdicional: '',
		tipoVial: {
			display: '',
			code: ''
		},
		numeroVial: '',
		letraVial: '',
		tipoCuadrante: {
			display: '',
			code: '0'
		},
		numeroViaGeneradora: '',
		cuadranteViaGeneradora: '',
		numeroPlaca: ''
	},
	arl: '',
	claseArl: {
		code: -1,
		display: 'Nivel de riesgo',
		value: 'nivel de riesgo'
	},
	cajaCompensacion: {
        value: null,
        code: '',
        display: '',
        province: ''
    },
	camara: '',
	legalRepresentative: {
		name: {
			display: '',
			firstName: '',
			secondName: '',
			firstSurname: '',
			secondSurname: ''
		},
		email: '',
		identificationType: {
			code: 'CC',
			display: 'CC'
		},
		identificationNumber: '',
		phoneNumber: '',
		cellphoneNumber: ''
	},
	businessContact: {
		name: {
			display: '',
			firstName: '',
			secondName: '',
			firstSurname: '',
			secondSurname: ''
		},
		email: '',
		identificationType: {
			code: 'CC',
			display: 'CC'
		},
		identificationNumber: '',
		phoneNumber: '',
		cellphoneNumber: ''
	},
	editCompany: false,
	editLegalRepresentative: false,
	editBusinessContact: false,
	exentoParafiscales: false,
	beneficiadoLey590: false,
	obligacionSalud: true,
	obligacionPension: true,
	obligacionRiesgos: true,
	obligacionCCF: true,
	obligacionSena: true,
	obligacionICBF: true,
	claseAportante: {
		display: 'Clase de aportante',
		code: -1
	},
	constitucionDate: {
		display: 'DD/MM/AAAA',
		code: '',
		value: ''
	},
	newSucursalModal: false,
	selectedSucursal: 'Principal',
	newSucursalName: '',
	sucursales: [],
	copyCompanyData: {},
	copybusinessContactData: {},
	copylegalRepresentativeData: {},
	showClaseArlPicker: false,
	copyArlData: {},
	copyCCFData: {},
	mainCompany: true,
	companyRequiredFields: [],
	legalRepresentativeRequiredFields: [],
	businessContactRequiredFields: [],
	showCheckCompanyModal: false
};

const saveCompanyData = (state = initialState, action) => {
	switch (action.type) {
		case COMPANY.COMPANY_SAVE_ALL_DATA:
			return {
				...state,
				...action.payload.value
			};

		case COMPANY.COMPANY_SAVE_COMPANY_NAME:
			return {
				...state,
				companyName: action.payload.value
			};

		case COMPANY.COMPANY_RESET_DATA:
			return {
				...state,
				...initialState
			};

		case COMPANY.COMPANY_SAVE_ECONOMIC_ACTIVITY:
			return {
				...state,
				economicActivity: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_PROVINCE:
			return {
				...state,
				province: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_CITY:
			return {
				...state,
				city: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_PHONE_NUMBER:
			return {
				...state,
				phoneNumber: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_CELLPHONE_NUMBER:
			return {
				...state,
				cellphoneNumber: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_IDENTIFICATION_TYPE:
			return {
				...state,
				identificationType: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_IDENTIFICATION_NUMBER:
			return {
				...state,
				identificationNumber: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_VERIFICATION_DIGIT:
			return {
				...state,
				verificationDigit: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_ADDRESS:
			return {
				...state,
				address: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_ARL:
			return {
				...state,
				arl: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_CAJA_COMPENSACION:
			return {
				...state,
				cajaCompensacion: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_LR_NAME:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					name: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_LR_EMAIL:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					email: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_LR_IDENTIFICATION_TYPE:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					identificationType: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_LR_IDENTIFICATION_NUMBER:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					identificationNumber: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_LR_PHONE_NUMBER:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					phoneNumber: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_LR_CELLPHONE_NUMBER:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					cellphoneNumber: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_LR_ALL_DATA:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					name: action.payload.value.name,
					email: action.payload.value.email,
					identificationType: action.payload.value.identificationType,
					identificationNumber: action.payload.value.identificationNumber,
					phoneNumber: action.payload.value.phoneNumber,
					cellphoneNumber: action.payload.value.cellphoneNumber
				}
			};

		case COMPANY.COMPANY_SAVE_BC_NAME:
			return {
				...state,
				businessContact: {
					...state.businessContact,
					name: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_BC_EMAIL:
			return {
				...state,
				businessContact: {
					...state.businessContact,
					email: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_BC_IDENTIFICATION_TYPE:
			return {
				...state,
				businessContact: {
					...state.businessContact,
					identificationType: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_BC_IDENTIFICATION_NUMBER:
			return {
				...state,
				businessContact: {
					...state.businessContact,
					identificationNumber: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_BC_PHONE_NUMBER:
			return {
				...state,
				businessContact: {
					...state.businessContact,
					phoneNumber: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_BC_CELLPHONE_NUMBER:
			return {
				...state,
				businessContact: {
					...state.businessContact,
					cellphoneNumber: action.payload.value
				}
			};

		case COMPANY.COMPANY_SAVE_BC_ALL_DATA:
			return {
				...state,
				businessContact: {
					...state.businessContact,
					name: action.payload.value.name,
					email: action.payload.value.email,
					identificationType: action.payload.value.identificationType,
					identificationNumber: action.payload.value.identificationNumber,
					phoneNumber: action.payload.value.phoneNumber,
					cellphoneNumber: action.payload.value.cellphoneNumber
				}
			};

		case COMPANY.COMPANY_EDIT_COMPANY:
			return {
				...state,
				editCompany: !state.editCompany
			};

		case COMPANY.COMPANY_EDIT_LEGAL_REPRESENTATIVE:
			return {
				...state,
				editLegalRepresentative: !state.editLegalRepresentative
			};

		case COMPANY.COMPANY_EDIT_BUSINESS_CONTACT:
			return {
				...state,
				editBusinessContact: !state.editBusinessContact
			};

		case COMPANY.COMPANY_SAVE_CLASE_ARL:
			return {
				...state,
				claseArl: action.payload.value
			};

		case COMPANY.COMPANY_EXENTO_PARAFISCALES:
			return {
				...state,
				exentoParafiscales: !state.exentoParafiscales,
				obligacionSena: state.exentoParafiscales,
				obligacionICBF: state.exentoParafiscales
			};

		case COMPANY.COMPANY_BENEFICIADO_LEY590:
			return {
				...state,
				beneficiadoLey590: !state.beneficiadoLey590
			};

		case COMPANY.COMPANY_SAVE_APORTANTE_CLASS:
			return {
				...state,
				claseAportante: action.payload.value
			};

		case COMPANY.COMPANY_SAVE_CONSTITUCION_DATE:
			return {
				...state,
				constitucionDate: action.payload.value
			};

		case COMPANY.COMPANY_UPDATE_ADDRESS_FIELD:
			const { ADDRESS } = FIELDS_ID;
			const incoming_id = action.payload.value.idField;
			const temp_direccion_completa =
				(incoming_id === ADDRESS.TIPO_VIAL
					? action.payload.value.data.display
					: state.address.tipoVial.display) +
				' ' +
				(incoming_id === ADDRESS.NUMERO_VIAL ? action.payload.value.data : state.address.numeroVial) +
				' ' +
				(incoming_id === ADDRESS.LETRA_VIAL ? action.payload.value.data : state.address.letraVial) +
				' ' +
				(incoming_id === ADDRESS.TIPO_CUADRANTE
					? action.payload.value.data.display
					: state.address.tipoCuadrante.display) +
				' ' +
				(incoming_id === ADDRESS.NUMERO_VIA_GENERADORA
					? action.payload.value.data
					: state.address.numeroViaGeneradora) +
				' ' +
				(incoming_id === ADDRESS.CUADRANTE_VIA_GENERADORA
					? action.payload.value.data
					: state.address.cuadranteViaGeneradora) +
				' ' +
				(incoming_id === ADDRESS.NUMERO_PLACA ? action.payload.value.data : state.address.numeroPlaca);

			return {
				...state,
				address: {
					...state.address,
					[action.payload.value.idField]: action.payload.value.data,
					direccionCompleta: temp_direccion_completa.trim()
				}
			};

		case COMPANY.COMPANY_OPEN_NEW_SUCURSAL_MODAL:
			return {
				...state,
				newSucursalModal: !state.newSucursalModal
			};

		case COMPANY.COMPANY_SAVE_SUCURSAL_NAME:
			return {
				...state,
				newSucursalName: action.payload.value
			};

		case COMPANY.COMPANY_RESET_NEW_SUCURSAL_DATA:
			return {
				...state,
				newSucursalModal: false,
				newSucursalName: ''
			};

		case COMPANY.COMPANY_ADD_NEW_SUCURSAL:
			return {
				...state,
				sucursales: [...state.sucursales, action.payload.value]
			};

		case COMPANY.COMPANY_COPY_COMPANY_DATA:
			switch (action.payload.value) {
				case 'company':
					return {
						...state,
						copyCompanyData: {
							...state.copyCompanyData,
							companyName: JSON.parse(JSON.stringify(state.companyName)),
							economicActivity: JSON.parse(JSON.stringify(state.economicActivity)),
							province: JSON.parse(JSON.stringify(state.province)),
							city: JSON.parse(JSON.stringify(state.city)),
							phoneNumber: JSON.parse(JSON.stringify(state.phoneNumber)),
							cellphoneNumber: JSON.parse(JSON.stringify(state.cellphoneNumber)),
							address: JSON.parse(JSON.stringify(state.address)),
							arl: JSON.parse(JSON.stringify(state.arl)),
							claseArl: JSON.parse(JSON.stringify(state.claseArl)),
							cajaCompensacion: JSON.parse(JSON.stringify(state.cajaCompensacion)),
							exentoParafiscales: JSON.parse(JSON.stringify(state.exentoParafiscales)),
							beneficiadoLey590: JSON.parse(JSON.stringify(state.beneficiadoLey590)),
							claseAportante: JSON.parse(JSON.stringify(state.claseAportante)),
							constitucionDate: JSON.parse(JSON.stringify(state.constitucionDate)),
							identificationType: JSON.parse(JSON.stringify(state.identificationType)),
							identificationNumber: JSON.parse(JSON.stringify(state.identificationNumber)),
							verificationDigit: JSON.parse(JSON.stringify(state.verificationDigit))
						}
					};

				case 'businessContact':
					return {
						...state,
						copybusinessContactData: {
							...state.copybusinessContactData,
							name: JSON.parse(JSON.stringify(state.businessContact.name)),
							email: JSON.parse(JSON.stringify(state.businessContact.email)),
							identificationType: JSON.parse(JSON.stringify(state.businessContact.identificationType)),
							identificationNumber: JSON.parse(
								JSON.stringify(state.businessContact.identificationNumber)
							),
							phoneNumber: JSON.parse(JSON.stringify(state.businessContact.phoneNumber)),
							cellphoneNumber: JSON.parse(JSON.stringify(state.businessContact.cellphoneNumber))
						}
					};

				case 'legalRepresentative':
					return {
						...state,
						copylegalRepresentativeData: {
							...state.copylegalRepresentativeData,
							name: JSON.parse(JSON.stringify(state.legalRepresentative.name)),
							email: JSON.parse(JSON.stringify(state.legalRepresentative.email)),
							identificationType: JSON.parse(
								JSON.stringify(state.legalRepresentative.identificationType)
							),
							identificationNumber: JSON.parse(
								JSON.stringify(state.legalRepresentative.identificationNumber)
							),
							phoneNumber: JSON.parse(JSON.stringify(state.legalRepresentative.phoneNumber)),
							cellphoneNumber: JSON.parse(JSON.stringify(state.legalRepresentative.cellphoneNumber))
						}
					};

				default:
					return state;
			}

		case COMPANY.COMPANY_RESET_BACKUP_DATA:
			switch (action.payload.value) {
				case 'company':
					return {
						...state,
						companyName: JSON.parse(JSON.stringify(state.copyCompanyData.companyName)),
						economicActivity: JSON.parse(JSON.stringify(state.copyCompanyData.economicActivity)),
						province: JSON.parse(JSON.stringify(state.copyCompanyData.province)),
						city: JSON.parse(JSON.stringify(state.copyCompanyData.city)),
						phoneNumber: JSON.parse(JSON.stringify(state.copyCompanyData.phoneNumber)),
						cellphoneNumber: JSON.parse(JSON.stringify(state.copyCompanyData.cellphoneNumber)),
						address: JSON.parse(JSON.stringify(state.copyCompanyData.address)),
						arl: JSON.parse(JSON.stringify(state.copyCompanyData.arl)),
						claseArl: JSON.parse(JSON.stringify(state.copyCompanyData.claseArl)),
						cajaCompensacion: JSON.parse(JSON.stringify(state.copyCompanyData.cajaCompensacion)),
						exentoParafiscales: JSON.parse(JSON.stringify(state.copyCompanyData.exentoParafiscales)),
						beneficiadoLey590: JSON.parse(JSON.stringify(state.copyCompanyData.beneficiadoLey590)),
						claseAportante: JSON.parse(JSON.stringify(state.copyCompanyData.claseAportante)),
						constitucionDate: JSON.parse(JSON.stringify(state.copyCompanyData.constitucionDate)),
						identificationType: JSON.parse(JSON.stringify(state.copyCompanyData.identificationType)),
						identificationNumber: JSON.parse(JSON.stringify(state.copyCompanyData.identificationNumber)),
						verificationDigit: JSON.parse(JSON.stringify(state.copyCompanyData.verificationDigit)),
						obligacionSena: !state.copyCompanyData.exentoParafiscales,
						obligacionICBF: !state.copyCompanyData.exentoParafiscales,
						copyCompanyData: {},
						companyRequiredFields: []
					};

				case 'businessContact':
					return {
						...state,
						businessContact: {
							...state.businessContact,
							name: JSON.parse(JSON.stringify(state.copybusinessContactData.name)),
							email: JSON.parse(JSON.stringify(state.copybusinessContactData.email)),
							identificationType: JSON.parse(
								JSON.stringify(state.copybusinessContactData.identificationType)
							),
							identificationNumber: JSON.parse(
								JSON.stringify(state.copybusinessContactData.identificationNumber)
							),
							phoneNumber: JSON.parse(JSON.stringify(state.copybusinessContactData.phoneNumber)),
							cellphoneNumber: JSON.parse(JSON.stringify(state.copybusinessContactData.cellphoneNumber))
						},
						copybusinessContactData: {},
						businessContactRequiredFields: []
					};

				case 'legalRepresentative':
					return {
						...state,
						legalRepresentative: {
							...state.legalRepresentative,
							name: JSON.parse(JSON.stringify(state.copylegalRepresentativeData.name)),
							email: JSON.parse(JSON.stringify(state.copylegalRepresentativeData.email)),
							identificationType: JSON.parse(
								JSON.stringify(state.copylegalRepresentativeData.identificationType)
							),
							identificationNumber: JSON.parse(
								JSON.stringify(state.copylegalRepresentativeData.identificationNumber)
							),
							phoneNumber: JSON.parse(JSON.stringify(state.copylegalRepresentativeData.phoneNumber)),
							cellphoneNumber: JSON.parse(
								JSON.stringify(state.copylegalRepresentativeData.cellphoneNumber)
							)
						},
						copylegalRepresentativeData: {},
						legalRepresentativeRequiredFields: []
					};

				default:
					return state;
			}

		case COMPANY.COMPANY_UPDATE_SOME_FIELDS:
			return {
				...state,
				...action.payload.value
			};

		case COMPANY.COMPANY_SHOW_CLASE_ARL_PICKER:
			return {
				...state,
				showClaseArlPicker: !state.showClaseArlPicker
			}

		case COMPANY.COMPANY_COPY_ADMINS_DATA:
			return {
				...state,
				copyArlData: JSON.parse(JSON.stringify(state.arl)),
				copyCCFData: JSON.parse(JSON.stringify(state.cajaCompensacion))
			}

		case COMPANY.COMPANY_RESET_ADMINS_COPY:
			return {
				...state,
				arl: JSON.parse(JSON.stringify(state.copyArlData)),
				cajaCompensacion: JSON.parse(JSON.stringify(state.copyCCFData)),
				copyArlData: {},
				copyCCFData: {}
			}

		case COMPANY.COMPANY_RESET_COPY_OBJECTS:
			return {
				...state,
				copyArlData: {},
				copyCCFData: {}
			}

		case COMPANY.COMPANY_SAVE_SELECTED_SUCURSAL:
			return {
				...state,
				selectedSucursal: action.payload.value
			}

		case COMPANY.COMPANY_SHOW_MAIN_COMPANY:
			return {
				...state,
				mainCompany: action.payload.value
			}

		case COMPANY.COMPANY_REMOVE_CARDS_DATA:
			return {
				...state,
				companyName: '',
				economicActivity: {
					code: '-1',
					display: '',
					value: ''
				},
				province: {
					code: '',
					display: '',
					value: ''
				},
				city: {
					code: '',
					display: '',
					provinceCode: '',
					value: ''
				},
				phoneNumber: '',
				cellphoneNumber: '',
				identificationType: {
					display: 'NIT',
					code: 'NI'
				},
				identificationNumber: '',
				verificationDigit: '',
				address: {
					direccionCompleta: '',
					informacionAdicional: '',
					tipoVial: {
						display: '',
						code: ''
					},
					numeroVial: '',
					letraVial: '',
					tipoCuadrante: {
						display: '',
						code: '0'
					},
					numeroViaGeneradora: '',
					cuadranteViaGeneradora: '',
					numeroPlaca: ''
				},
				arl: '',
				claseArl: {
					code: -1,
					display: 'Nivel de riesgo',
					value: 'nivel de riesgo'
				},
				cajaCompensacion: {
					value: null,
					code: '',
					display: '',
					province: ''
				},
				exentoParafiscales: false,
				beneficiadoLey590: false,
				claseAportante: {
					display: 'Clase de aportante',
					code: -1
				},
				constitucionDate: {
					display: 'DD/MM/AAAA',
					code: '',
					value: ''
				},
				obligacionSena: true,
				obligacionICBF: true,
				legalRepresentative: {
					...state.legalRepresentative,
					name: {
						display: '',
						firstName: '',
						secondName: '',
						firstSurname: '',
						secondSurname: ''
					},
					email: '',
					identificationType: {
						display: 'CC',
						code: 'CC'
					},
					identificationNumber: '',
					phoneNumber: '',
					cellphoneNumber: ''
				},
				businessContact: {
					...state.businessContact,
					name: {
						display: '',
						firstName: '',
						secondName: '',
						firstSurname: '',
						secondSurname: ''
					},
					email: '',
					identificationType: {
						display: 'CC',
						code: 'CC'
					},
					identificationNumber: '',
					phoneNumber: '',
					cellphoneNumber: ''
				}
			}

		case COMPANY.COMPANY_COMPANY_REQUIRED_FIELD:
			if (action.payload.value.status) {
				return {
					...state,
					companyRequiredFields: [...state.companyRequiredFields, action.payload.value.field]
				}
			}
			else {
				return {
					...state,
					companyRequiredFields: state.companyRequiredFields.filter((item) => item !== action.payload.value.field)
				}
			}

		case COMPANY.COMPANY_LEGAL_REPRESENTATIVE_REQUIRED_FIELD:
			if (action.payload.value.status) {
				return {
					...state,
					legalRepresentativeRequiredFields: [...state.legalRepresentativeRequiredFields, action.payload.value.field]
				}
			}
			else {
				return {
					...state,
					legalRepresentativeRequiredFields: state.legalRepresentativeRequiredFields.filter((item) => item !== action.payload.value.field)
				}
			}

		case COMPANY.COMPANY_BUSINESS_CONTACT_REQUIRED_FIELD:
			if (action.payload.value.status) {
				return {
					...state,
					businessContactRequiredFields: [...state.businessContactRequiredFields, action.payload.value.field]
				}
			}
			else {
				return {
					...state,
					businessContactRequiredFields: state.businessContactRequiredFields.filter((item) => item !== action.payload.value.field)
				}
			}

		case COMPANY.COMPANY_COPY_CONTACT_DATA:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					name: state.businessContact.name,
					email: state.businessContact.email,
					identificationType: state.businessContact.identificationType,
					identificationNumber: state.businessContact.identificationNumber,
					phoneNumber: state.businessContact.phoneNumber,
					cellphoneNumber: state.businessContact.cellphoneNumber
				}
			}

		case COMPANY.COMPANY_RESET_LR_DATA:
			return {
				...state,
				legalRepresentative: {
					...state.legalRepresentative,
					name: {
						display: '',
						firstName: '',
						secondName: '',
						firstSurname: '',
						secondSurname: ''
					},
					email: '',
					identificationType: {
						code: 'CC',
						display: 'CC'
					},
					identificationNumber: '',
					phoneNumber: '',
					cellphoneNumber: ''
				}
			}

		case COMPANY.COMPANY_SHOW_CHECK_COMPANY_MODAL:
			return {
				...state,
				showCheckCompanyModal: !state.showCheckCompanyModal
			}

		default:
			return state;
	}
};

export default saveCompanyData;
