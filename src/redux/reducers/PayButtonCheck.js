import { PAY_BUTTON_CHECK } from './../ActionTypes';
import { FEEDBACK_MESSAGES } from '../../payButtonCheck/Constants';

const initialState = {
	validationStatus: false,
	validationError: false,
	feedBackMessage: FEEDBACK_MESSAGES.VALIDATION_MESSAGE,
	urlParams: {},
	estadoPlanilla: {},
	paymentURL: '',
	getURLStatus: false,
	feedBackSeverity: 'error'
};

const payButtonCheck = (state = initialState, action) => {
	switch (action.type) {
		case PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_STATUS:
			return {
				...state,
				validationStatus: action.payload.value
			};

		case PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_ERROR:
			return {
				...state,
				validationError: action.payload.value
			};

		case PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_MESSAGE:
			return {
				...state,
				feedBackMessage: action.payload.value
			};

		case PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_URL_PARAMS:
			return {
				...state,
				urlParams: action.payload.value
			};

		case PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_ESTADO_PLANILLA:
			return {
				...state,
				estadoPlanilla: action.payload.value
			};

		case PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_PAYMENT_URL:
			return {
				...state,
				paymentURL: action.payload.value
			};

		case PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_SEVERITY:
			return {
				...state,
				feedBackSeverity: action.payload.value
			};

		case PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_GET_URL_STATUS:
			return {
				...state,
				getURLStatus: action.payload.value
			};

		default:
			return state;
	}
};

export default payButtonCheck;
