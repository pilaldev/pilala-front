import { VACACIONES } from './../ActionTypes';
import { emptyRecordFields, editRecordField } from './../../constants/CotizantesFields';
import moment from 'moment';

const initialState = {
	openVacacionesModal: false,
	cotizanteType: '',
	cotizanteName: '',
	cotizanteIdentificationNumber: '',
	activeChanges: false,
	vacacionesAmount: 0,
	idCotizante: '',
	vacacionesRows: [
		{
			nombreNovedad: 'Vacaciones',
			duracionDias: '',
			fechaInicio: '',
			fechaFin: '',
			value: {
				fechaInicio: '',
				fechaFin: '',
				duracionDias: ''
			},
			regLine: {
				...emptyRecordFields
			}
		}
	]
};

const saveNovedadVacaciones = (state = initialState, action) => {
	switch (action.type) {
		case VACACIONES.VACACIONES_OPEN_VACACIONES_MODAL:
			return {
				...state,
				openVacacionesModal: !state.openVacacionesModal
			};

		case VACACIONES.VACACIONES_RESET_NOVEDAD_FIELDS:
			return {
				...state,
                openVacacionesModal: false,
                cotizanteType: '',
				cotizanteName: '',
				cotizanteIdentificationNumber: '',
				activeChanges: false,
				vacacionesAmount: 0,
				idCotizante: '',
				vacacionesRows: [
					{
						nombreNovedad: 'Vacaciones',
						duracionDias: '',
						fechaInicio: '',
						fechaFin: '',
						value: {
							fechaInicio: '',
							fechaFin: '',
							duracionDias: ''
						},
						regLine: {
							...emptyRecordFields
						}
					}
				]
			};

		case VACACIONES.VACACIONES_SAVE_NOVEDAD_FIELDS:
			return {
				...state,
				vacacionesRows: state.vacacionesRows.map((item, index) => {
					if (action.payload.value.index === index) {
						let temp_update_item = JSON.parse(JSON.stringify(item));
						if (action.payload.value.fields.fechaInicio) {
							if (item.fechaFin !== '') {
								const fechaInicio = action.payload.value.fields.fechaInicio;
								const fechaFin = item.fechaFin;
								const days = moment(fechaFin, 'DD/MM/YYYY').diff(moment(fechaInicio, 'DD/MM/YYYY'), 'days') + 1;
								temp_update_item = {
									...temp_update_item,
									duracionDias: days,
									value: {
										...temp_update_item.value,
										duracionDias: days
									},
								}
							}
						}
						if (action.payload.value.fields.fechaFin) {
							if (item.fechaInicio !== '') {
								const fechaInicio = item.fechaInicio;
								const fechaFin = action.payload.value.fields.fechaFin;
								const days = moment(fechaFin, 'DD/MM/YYYY').diff(moment(fechaInicio, 'DD/MM/YYYY'), 'days') + 1;
								temp_update_item = {
									...temp_update_item,
									duracionDias: days,
									value: {
										...temp_update_item.value,
										duracionDias: days
									},
								}
							}
						}
						temp_update_item = {
							...temp_update_item,
							...action.payload.value.fields,
							value: {
								...temp_update_item.value,
								...action.payload.value.fields.value
							},
							regLine: {
								...temp_update_item.regLine,
								...action.payload.value.fields.regLine
							}
						};
						return temp_update_item;
					}
					return item;
				})
			};

		case VACACIONES.VACACIONES_SHOW_CONFIRM_MODAL:
			return {
				...state,
				showConfirmModal: !state.showConfirmModal
			};

		case VACACIONES.VACACIONES_CALCULATE_DURATION_DAYS:
			const fechaInicio = state.vacacionesRows[action.payload.value].fechaInicio;
			const fechaFin = state.vacacionesRows[action.payload.value].fechaFin;
			const days = moment(fechaFin, 'DD/MM/YYYY').diff(moment(fechaInicio, 'DD/MM/YYYY'), 'days') + 1;
			return {
				...state,
				vacacionesRows: state.vacacionesRows.map((item, index) => {
					if (action.payload.value === index) {
						let temp_days_item = JSON.parse(JSON.stringify(item));
						temp_days_item = {
							...temp_days_item,
							duracionDias: days,
							value: {
								...temp_days_item.value,
								duracionDias: days
							},
							regLine: {
								...temp_days_item.regLine,
								36: editRecordField[36](days),
								37: editRecordField[37](days),
								38: editRecordField[38](days),
								39: editRecordField[39](days)
							}
						};
						return temp_days_item;
					}
					return item;
				})
			};

		case VACACIONES.VACACIONES_DELETE_VACACIONES_ROW:
			if (state.vacacionesRows.length === 1) {
				return {
					...state,
					vacacionesRows: state.vacacionesRows.map((item) => {
						return {
							nombreNovedad: 'Vacaciones',
							duracionDias: '',
							fechaInicio: '',
							fechaFin: '',
							value: {
								fechaInicio: '',
								fechaFin: '',
								duracionDias: ''
							},
							regLine: {
								...emptyRecordFields
							}
						};
					})
				};
			} else {
				return {
					...state,
					vacacionesRows: state.vacacionesRows.filter((item, index) => index !== action.payload.value)
				};
			}

		case VACACIONES.VACACIONES_SAVE_COTIZANTE_DATA:
			return {
				...state,
				...action.payload.value
			};

		case VACACIONES.VACACIONES_ADD_VACACIONES_ROW:
			return {
				...state,
				vacacionesRows: [
					...state.vacacionesRows,
					{
						nombreNovedad: 'Vacaciones',
						duracionDias: '',
						fechaInicio: '',
						fechaFin: '',
						value: {
							fechaInicio: '',
							fechaFin: '',
							duracionDias: ''
						},
						regLine: {
							...emptyRecordFields
						}
					}
				]
			};

		case VACACIONES.VACACIONES_ENABLE_ACTIVE_CHANGES:
			return {
				...state,
				activeChanges: !state.activeChanges
			};

		default:
			return state;
	}
};

export default saveNovedadVacaciones;
