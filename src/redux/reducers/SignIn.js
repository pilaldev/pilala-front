import { SIGN_IN } from './../ActionTypes';

const initialState = {
    userEmail: '',
    userPassword: '',
    showPassword: false,
    persistCredentials: false,
    currentForm: 'empresas'
}

const saveSignInData = (state = initialState, action) => {
    switch (action.type) {

        case SIGN_IN.SIGN_IN_GET_USER_EMAIL:
            return {
                ...state,
                userEmail: action.payload.value
            }

        case SIGN_IN.SIGN_IN_GET_USER_PASSWORD:
            return {
                ...state,
                userPassword: action.payload.value
            }

        case SIGN_IN.SIGN_IN_SHOW_PASSWORD_CONTENT:
            return {
                ...state,
                showPassword: !state.showPassword
            }

        case SIGN_IN.SIGN_IN_PERSIST_CREDENTIALS:
            return {
                ...state,
                persistCredentials: !state.persistCredentials
            }

        case SIGN_IN.SIGN_IN_RESET_FIELDS:
            return {
                ...initialState
            }

        case SIGN_IN.SIGN_IN_SET_FORM:
            return {
                ...state,
                currentForm: action.payload.value
            }

        default:
            return state;
    }
}

export default saveSignInData;