import { SUSPENSIONES } from './../ActionTypes';
import { emptyRecordFields, editRecordField } from './../../constants/CotizantesFields';
import moment from 'moment';

const initialState = {
	openSuspensionesModal: false,
	cotizanteName: '',
	cotizanteType: '',
	cotizanteIdentificationNumber: '',
	activeChanges: false,
	suspensionesAmount: 0,
	idCotizante: '',
	suspensionesRows: [
		{
			nombreNovedad: 'Suspensión',
			duracionDias: '',
			fechaInicio: '',
			fechaFin: '',
			value: {
				fechaInicio: '',
				fechaFin: '',
				duracionDias: ''
			},
			regLine: {
				...emptyRecordFields
			}
		}
	]
};

const saveNovedadSuspensiones = (state = initialState, action) => {
	switch (action.type) {
		case SUSPENSIONES.SUSPENSIONES_OPEN_SUSPENSIONES_MODAL:
			return {
				...state,
				openSuspensionesModal: !state.openSuspensionesModal
			};

		case SUSPENSIONES.SUSPENSIONES_RESET_NOVEDAD_FIELDS:
			return {
				...state,
				openSuspensionesModal: false,
				cotizanteName: '',
				cotizanteType: '',
				cotizanteIdentificationNumber: '',
				activeChanges: false,
				suspensionesAmount: 0,
				idCotizante: '',
				suspensionesRows: [
					{
						nombreNovedad: 'Suspensión',
						duracionDias: '',
						fechaInicio: '',
						fechaFin: '',
						value: {
							fechaInicio: '',
							fechaFin: '',
							duracionDias: ''
						},
						regLine: {
							...emptyRecordFields
						}
					}
				]
			};

		case SUSPENSIONES.SUSPENSIONES_SAVE_NOVEDAD_FIELDS:
			return {
				...state,
				suspensionesRows: state.suspensionesRows.map((item, index) => {
					if (action.payload.value.index === index) {
						let temp_update_item = JSON.parse(JSON.stringify(item));
						if (action.payload.value.fields.fechaInicio) {
							if (item.fechaFin !== '') {
								const fechaInicio = action.payload.value.fields.fechaInicio;
								const fechaFin = item.fechaFin;
								const days = moment(fechaFin, 'DD/MM/YYYY').diff(moment(fechaInicio, 'DD/MM/YYYY'), 'days') + 1;
								temp_update_item = {
									...temp_update_item,
									duracionDias: days,
									value: {
										...temp_update_item.value,
										duracionDias: days
									},
								}
							}
						}
						if (action.payload.value.fields.fechaFin) {
							if (item.fechaInicio !== '') {
								const fechaInicio = item.fechaInicio;
								const fechaFin = action.payload.value.fields.fechaFin;
								const days = moment(fechaFin, 'DD/MM/YYYY').diff(moment(fechaInicio, 'DD/MM/YYYY'), 'days') + 1;
								temp_update_item = {
									...temp_update_item,
									duracionDias: days,
									value: {
										...temp_update_item.value,
										duracionDias: days
									},
								}
							}
						}
						temp_update_item = {
							...temp_update_item,
							...action.payload.value.fields,
							value: {
								...temp_update_item.value,
								...action.payload.value.fields.value
							},
							regLine: {
								...temp_update_item.regLine,
								...action.payload.value.fields.regLine
							}
						};
						return temp_update_item;
					}
					return item;
				})
			};

		case SUSPENSIONES.SUSPENSIONES_CALCULATE_DURATION_DAYS:
			const fechaInicio = state.suspensionesRows[action.payload.value].fechaInicio;
			const fechaFin = state.suspensionesRows[action.payload.value].fechaFin;
			const days = moment(fechaFin, 'DD/MM/YYYY').diff(moment(fechaInicio, 'DD/MM/YYYY'), 'days') + 1;
			return {
				...state,
				suspensionesRows: state.suspensionesRows.map((item, index) => {
					if (action.payload.value === index) {
						let temp_days_item = JSON.parse(JSON.stringify(item));
						temp_days_item = {
							...temp_days_item,
							duracionDias: days,
							value: {
								...temp_days_item.value,
								duracionDias: days
							},
							regLine: {
								...temp_days_item.regLine,
								36: editRecordField[36](days),
								37: editRecordField[37](days),
								38: editRecordField[38](days),
								39: editRecordField[39](days)
							}
						};
						return temp_days_item;
					}
					return item;
				})
			};

		case SUSPENSIONES.SUSPENSIONES_DELETE_SUSPENSIONES_ROW:
			if (state.suspensionesRows.length === 1) {
				return {
					...state,
					suspensionesRows: state.suspensionesRows.map((item) => {
						return {
							nombreNovedad: 'Suspensión',
							duracionDias: '',
							fechaInicio: '',
							fechaFin: '',
							value: {
								fechaInicio: '',
								fechaFin: '',
								duracionDias: ''
							},
							regLine: {
								...emptyRecordFields
							}
						};
					})
				};
			} else {
				return {
					...state,
					suspensionesRows: state.suspensionesRows.filter((item, index) => index !== action.payload.value)
				};
			}

		case SUSPENSIONES.SUSPENSIONES_SAVE_COTIZANTE_DATA:
			return {
				...state,
				...action.payload.value
			};

		case SUSPENSIONES.SUSPENSIONES_ADD_SUSPENSIONES_ROW:
			return {
				...state,
				suspensionesRows: [
					...state.suspensionesRows,
					{
						nombreNovedad: 'Suspensión',
						duracionDias: '',
						fechaInicio: '',
						fechaFin: '',
						value: {
							fechaInicio: '',
							fechaFin: '',
							duracionDias: ''
						},
						regLine: {
							...emptyRecordFields
						}
					}
				]
			};

		case SUSPENSIONES.SUSPENSIONES_ENABLE_ACTIVE_CHANGES:
			return {
				...state,
				activeChanges: !state.activeChanges
			};

		default:
			return state;
	}
};

export default saveNovedadSuspensiones;
