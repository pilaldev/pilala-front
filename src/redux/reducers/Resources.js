import { RESOURCES } from './../ActionTypes';

const initialState = {
	cities: [],
	provinces: [],
	ciiu: [],
	arl: [],
	ccf: [],
	afp: [],
	eps: [],
	smlmv: '',
	camaras: [],
	clasesArl: [],
	integralSalary: '',
	subtiposCotizantes: [],
	tiposCotizantes: [],
	tipoVial: [],
	tiposCuadrante: [],
	clasesAportante: [],
	identificacionAportante: []
};

const saveResources = (state = initialState, action) => {
	switch (action.type) {
		case RESOURCES.RESOURCES_SAVE_LOCATION_DATA:
			return {
				...state,
				...action.payload.value
			};

		case RESOURCES.RESOURCES_RESET_DATA:
			return {
				...state,
                cities: [],
                provinces: [],
                ciiu: [],
                arl: [],
                ccf: [],
                afp: [],
				eps: [],
				smlmv: '',
				camaras: [],
				clasesArl: [],
				integralSalary: '',
				subtiposCotizantes: [],
				tiposCotizantes: [],
				clasesAportante: [],
				identificacionAportante: []
			};

		default:
			return state;
	}
};

export default saveResources;
