import { NAVIGATION } from '../ActionTypes';

export const initialState = {
  sideTab: '',
  activePanelMultiempresa: null,
  blockScreen: false,
  isCredentialsModalOpen: false,
};

const navigation = (state = initialState, action) => {
  switch (action.type) {
    case NAVIGATION.NAVIGATION_SAVE_SIDE_TAB:
      return {
        ...state,
        sideTab: action.payload.value,
      };

    case NAVIGATION.NAVIGATION_ACTIVE_PANEL_MULTIEMPRESA:
      return {
        ...state,
        activePanelMultiempresa: action.payload.value,
      };

    case NAVIGATION.NAVIGATION_RESET_FIELDS:
      return {
        ...initialState,
      };

    case NAVIGATION.NAVIGATION_BLOCK_SCREEN:
      return {
        ...state,
        blockScreen: action.payload.value,
      };

    case NAVIGATION.OPEN_CREDENTIALS_MODAL:
      return {
        ...state,
        isCredentialsModalOpen: !state.isCredentialsModalOpen,
      };

    default:
      return state;
  }
};

export default navigation;
