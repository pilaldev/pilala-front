import { DETALLE_APORTES_MODAL } from '../ActionTypes';

const initialState = {
  isOpen: false,
  cotizanteName: '',
  cotizanteDocumentType: '',
  cotizanteDocumentNumber: '',
  tarifas: {},
  total: {},
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case DETALLE_APORTES_MODAL.OPEN_DA_MODAL:
      return {
        ...state,
        isOpen: true,
        ...payload.value,
      };

    case DETALLE_APORTES_MODAL.CLOSE_DA_MODAL:
      return {
        ...state,
        isOpen: false,
      };

    case DETALLE_APORTES_MODAL.RESET_DA_VALUES:
      return {
        ...state,
        ...initialState,
      };

    default:
      return state;
  }
};
