import { CURRENT_USER } from '../ActionTypes';

export const initialState = {
  name: '',
  phoneNumber: '',
  email: '',
  documentNumber: '',
  documentType: '',
  nit: '',
  uid: '',
  companies: [],
  companiesInfo: [],
  cotizantesInfo: [],
  prePlanillaPeriodos: {},
  periodosInfo: {},
  correcionesInfo: {},
  firstSignIn: null,
  createdCompany: null,
  activeCompany: null,
  activeCorreciones: false,
  firstCheck: false,
  featureFlags: {},
  accountManager: [],
};

const saveCurrentUserData = (state = initialState, action) => {
  switch (action.type) {
    case CURRENT_USER.CURRENT_USER_SAVE_USER_NAME:
      return {
        ...state,
        name: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_USER_PHONE_NUMBER:
      return {
        ...state,
        phoneNumber: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_USER_EMAIL:
      return {
        ...state,
        email: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_USER_ID:
      return {
        ...state,
        documentNumber: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_TYPE_USER_ID:
      return {
        ...state,
        documentType: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_USER_COMPANIES:
      return {
        ...state,
        companies: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_COMPANY_NIT:
      return {
        ...state,
        nit: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_FIRST_SIGNIN:
      return {
        ...state,
        firstSignIn: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_USER_UID:
      return {
        ...state,
        uid: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_CREATED_COMPANY:
      return {
        ...state,
        createdCompany: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_USER_COMPANIES_INFO:
      return {
        ...state,
        companiesInfo: action.payload.value,
      };

    // case CURRENT_USER.CURRENT_USER_SAVE_USER_COTIZANTES_INFO:
    // 	return {
    // 		...state,
    // 		cotizantesInfo: action.payload.value
    // 	};

    case CURRENT_USER.CURRENT_USER_SAVE_USER_SELECTED_COMPANY:
      return {
        ...state,
        activeCompany: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_COMPANIES_PERIODOS_INFO:
      return {
        ...state,
        periodosInfo: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_COMPANIES_PREPLANILLA_PERIODOS_INFO:
      return {
        ...state,
        prePlanillaPeriodos: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_COMPANIES_CORRECIONES_INDO:
      return {
        ...state,
        correcionesInfo: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_RESET_DATA: {
      return {
        ...state,
        ...initialState,
      };
    }

    case CURRENT_USER.CURRENT_USER_UPDATE_COMPANIES:
      return {
        ...state,
        companies: [...state.companies, action.payload.value],
      };

    case CURRENT_USER.CURRENT_USER_UPDATE_COMPANIES_INFO:
      return {
        ...state,
        companiesInfo: {
          ...state.companiesInfo,
          [action.payload.value.idCompany]: action.payload.value.data,
        },
      };

    case CURRENT_USER.CURRENT_USER_UPDATE_PERIODOS_INFO:
      return {
        ...state,
        periodosInfo: {
          ...state.periodosInfo,
          [action.payload.value.activeCompany]: {
            ...action.payload.value.periodo,
          },
        },
      };

    case CURRENT_USER.CURRENT_USER_UPDATE_CORRECIONES_INFO:
      return {
        ...state,
        correcionesInfo: {
          ...state.correcionsInfo,
          [action.payload.value.activeCompany]: {
            ...action.payload.value.periodo,
          },
        },
      };

    case CURRENT_USER.CURRENT_USER_UPDATE_SUCURSALES:
      return {
        ...state,
        companies: state.companies.map((item) => {
          if (item.id === action.payload.value.idCompany) {
            return {
              ...item,
              sucursales: [
                ...item.sucursales,
                action.payload.value.newSucursal,
              ],
            };
          }
          return item;
        }),
      };

    case CURRENT_USER.CURRENT_USER_UPDATE_ACTIVE_CORRECIONES_FLAG:
      return {
        ...state,
        activeCorreciones: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_UPDATE_FIRST_CHECK:
      return {
        ...state,
        firstCheck: action.payload.value,
      };

    case CURRENT_USER.SAVE_FEATURE_FLAGS:
      return {
        ...state,
        featureFlags: action.payload.value,
      };

    case CURRENT_USER.SAVE_ACCOUNT_MANAGER:
      return {
        ...state,
        accountManager: action.payload.value,
      };

    case CURRENT_USER.CURRENT_USER_SAVE_DATA:
      return {
        ...state,
        ...action.payload.value,
      };

    default:
      return state;
  }
};

export default saveCurrentUserData;
