import { CREDENTIALS_MODAL } from '../ActionTypes';

const initialState = {
  isModalOpen: false,
  context: null,
  values: {
    operador: 'arus',
    companyIdType: 'NI',
    companyIdNumber: '',
    userType: 'CC',
    userNumber: '',
    password: '',
    credentials: 0,
  },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case CREDENTIALS_MODAL.OPEN_MODAL:
      return {
        ...state,
        isModalOpen: true,
        context: payload?.value ?? null,
      };

    case CREDENTIALS_MODAL.CLOSE_MODAL:
      return {
        ...state,
        isModalOpen: false,
      };

    case CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES:
      return {
        ...state,
        values: {
          ...state.values,
          ...payload.value,
        },
      };

    case CREDENTIALS_MODAL.RESET_MODAL_VALUES:
      return {
        ...state,
        ...initialState,
      };

    default:
      return state;
  }
};
