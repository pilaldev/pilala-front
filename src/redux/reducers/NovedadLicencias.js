import { LICENCIAS } from '../ActionTypes';

const initialState = {
	openLicenciasModal: false,
	cotizanteName: '',
	cotizanteDocumentNumber: '',
	tipoCotizante: '',
	activeChanges: false,
	totalDaysLR: 0,
	totalDaysLNR: 0,
	totalDaysLMA: 0,
	idCotizante: '',
	LR: [
		{
			nombreNovedad: 'LR',
			duracionDias: '',
			fechaInicio: '',
			fechaFin: '',
			value: {
				fechaInicio: '',
				fechaFin: '',
				duracionDias: ''
			},
			regLine: {}
		}
	],
	LMA: [
		{
			nombreNovedad: 'LMA',
			duracionDias: '',
			fechaInicio: '',
			fechaFin: '',
			value: {
				fechaInicio: '',
				fechaFin: '',
				duracionDias: ''
			},
			regLine: {}
		}
	],
	LNR: [
		{
			nombreNovedad: 'LNR',
			duracionDias: '',
			fechaInicio: '',
			fechaFin: '',
			value: {
				fechaInicio: '',
				fechaFin: '',
				duracionDias: ''
			},
			regLine: {}
		}
	]
};

const novedadLicencias = (state = initialState, action) => {
	switch (action.type) {

		case LICENCIAS.LICENCIAS_OPEN_LICENCIAS_MODAL:
			return {
				...state,
				openLicenciasModal: !state.openLicenciasModal
			};

		case LICENCIAS.LICENCIAS_SET_COTIZANTE_DATA:
			return {
				...state,
				...action.payload.value
			};

		case LICENCIAS.LICENCIAS_ADD_NOVEDAD:
			return {
				...state,
				[action.payload.value.licenciaType]: [
					...state[action.payload.value.licenciaType],
					{
						nombreNovedad: action.payload.value.licenciaType,
						duracionDias: '',
						fechaInicio: '',
						fechaFin: '',
						regLine: {},
						value: {
							fechaInicio: '',
							fechaFin: '',
							duracionDias: ''
						}
					}
				]
			};

		case LICENCIAS.LICENCIAS_UPDATE_NOVEDAD:
			return {
				...state,
				totalDaysLR:
					action.payload.value.licenciaType === 'LR'
						? state.LR.reduce(function (a, b, index) {
							if (index !== action.payload.value.index) {
								return a + b.duracionDias;
							} else {
								return a;
							}
						}, 0) + action.payload.value.totalDaysLR
						: state.totalDaysLR,
				totalDaysLNR:
					action.payload.value.licenciaType === 'LNR'
						? state.LNR.reduce(function (a, b, index) {
							if (index !== action.payload.value.index) {
								return a + b.duracionDias;
							} else {
								return a;
							}
						}, 0) + action.payload.value.totalDaysLNR
						: state.totalDaysLNR,
				totalDaysLMA:
					action.payload.value.licenciaType === 'LMA'
						? state.LMA.reduce(function (a, b, index) {
							if (index !== action.payload.value.index) {
								return a + b.duracionDias;
							} else {
								return a;
							}
						}, 0) + action.payload.value.totalDaysLMA
						: state.totalDaysLMA,
				[action.payload.value.licenciaType]: state[action.payload.value.licenciaType].map((item, index) => {
					if (index === action.payload.value.index) {
						return {
							...item,
							...action.payload.value.novedad,
							value: {
								...item.value,
								...action.payload.value.novedad.value
							},
							regLine: {
								...item.regLine,
								...action.payload.value.novedad.regLine
							}
						};
					} else {
						return item;
					}
				})
			};

		case LICENCIAS.LICENCIAS_REMOVE_NOVEDAD:
			const daysKey = `totalDays${action.payload.value.licenciaType}`;
			if (state[action.payload.value.licenciaType].length === 1) {
				return {
					...state,
					[daysKey]: 0,
					[action.payload.value.licenciaType]: state[action.payload.value.licenciaType].map(() => {
						return {
							nombreNovedad: action.payload.value.licenciaType,
							duracionDias: '',
							fechaInicio: '',
							fechaFin: '',
							regLine: {},
							value: {
								fechaInicio: '',
								fechaFin: '',
								duracionDias: ''
							},
						}
					})
				}
			}
			else {
				return {
					...state,
					[daysKey]: state[daysKey] - state[action.payload.value.licenciaType][action.payload.value.index].duracionDias,
					[action.payload.value.licenciaType]: state[action.payload.value.licenciaType].filter((item, index) => index !== action.payload.value.index)
				};
			}

		case LICENCIAS.LICENCIAS_RESET_DATA:
			return {
				...state,
				...initialState
			};

		case LICENCIAS.LICENCIAS_ENABLE_ACTIVE_CHANGES:
			return {
				...state,
				activeChanges: !state.activeChanges
			};

		default:
			return state;
	}
};

export default novedadLicencias;
