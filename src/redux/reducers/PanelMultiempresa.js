import { PANEL_MULTIEMPRESA } from './../ActionTypes';

const initialState = {
    showNewCompanyModal: false,
    multiPlanillas: [],
    credentialsId: ''
}

const panelMultiempresaData = (state = initialState, action) => {
    switch (action.type) {

        case PANEL_MULTIEMPRESA.PANEL_MULTIEMPRESA_SHOW_NEW_COMPANY_MODAL:
            return {
                ...state,
                showNewCompanyModal: !state.showNewCompanyModal
            }

        case PANEL_MULTIEMPRESA.PANEL_MULTIEMPRESA_SAVE_MULTI_PLANILLAS:
            return {
                ...state,
                multiPlanillas: action.payload.value
            }

        case PANEL_MULTIEMPRESA.PANEL_MULTIEMPRESA_SAVE_CREDENTIALS_ID:
            return {
                ...state,
                credentialsId: action.payload.value
            }

        default:
            return state;
    }
}

export default panelMultiempresaData;
