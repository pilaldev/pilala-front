import { INGRESO } from './../ActionTypes';
import { emptyRecordFields } from './../../constants/CotizantesFields';

const initialState = {
    openIngresoModal: false,
    cotizanteName: '',
    cotizanteSalary: '',
    /* Campos novedad*/
    dateEntry: '',
    regLine: {
        ...emptyRecordFields
    },
    newEntry: false,
    showConfirmModal: false,
    basicas: [],
    idNovedad: '',
    activeChanges: false,
    idCotizante: '',
    cotizanteDocumentNumber: '',
    tipoCotizante: ''
}

const saveNovedadIngreso = (state = initialState, action) => {
    switch (action.type) {

        case INGRESO.INGRESO_OPEN_INGRESO_MODAL:
            return {
                ...state,
                openIngresoModal: !state.openIngresoModal
            }

        case INGRESO.INGRESO_RESET_NOVEDAD_FIELDS:
            return {
                ...state,
                ...initialState
            }

        case INGRESO.INGRESO_SAVE_NOVEDAD_FIELDS:
            return {
                ...state,
                ...action.payload.value,
                regLine: {
                    ...state.regLine,
                    ...action.payload.value.regLine
                }
            }

        case INGRESO.INGRESO_UPDATE_NOVEDAD_FIELDS:
            const temp_basicas = JSON.parse(JSON.stringify(state.basicas));
            if (temp_basicas.some(item => item.type === 'Ingresos')) {
                temp_basicas.forEach((item, index) => {
                    if (item.type === 'Ingresos') {
                        temp_basicas[index].nombreNovedad = 'Novedad de ingreso';
                        temp_basicas[index].duracionDias = 0;
                        temp_basicas[index].fechaInicio = action.payload.value.dateEntry;
                        temp_basicas[index].fechaFin = action.payload.value.dateEntry;
                        temp_basicas[index].value = action.payload.value.dateEntry
                    }
                });
            }
            else {
                temp_basicas.push({
                    nombreNovedad: 'Novedad de ingreso',
                    duracionDias: 0,
                    fechaInicio: action.payload.value.dateEntry,
                    fechaFin: action.payload.value.dateEntry,
                    value: action.payload.value.dateEntry,
                    type: 'Ingresos'
                });
            }
            return {
                ...state,
                dateEntry: action.payload.value.dateEntry,
                regLine: {
                    ...state.regLine,
                    ...action.payload.value.regLine
                },
                newEntry: action.payload.value.newEntry,
                basicas: temp_basicas,
                activeChanges: true
            }

        case INGRESO.INGRESO_SHOW_CONFIRM_MODAL:
            return {
                ...state,
                showConfirmModal: !state.showConfirmModal
            }

        case INGRESO.INGRESO_DELETE_NOVEDAD:
            const new_basicas = state.basicas.filter(item => item.type !== 'Ingresos');
            return {
                ...state,
                dateEntry: '',
                newEntry: false,
                basicas: new_basicas,
                idNovedad: '',
                activeChanges: false,
                regLine: {
                    ...state.regLine,
                    ...action.payload.value.regLine
                },
                // idCotizante: ''
            }

        default:
            return state;
    }
}

export default saveNovedadIngreso;