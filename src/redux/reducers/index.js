import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import SignUp from './SignUp';
import SignIn from './SignIn';
import CurrentUser, {
  initialState as currenUserInitialState,
} from './CurrentUser';
import FetchData from './FetchData';
import Company, { initialState as companyInitialState } from './Company';
import Resources from './Resources';
import Cotizantes, {
  initialState as cotizantesInitialState,
} from './Cotizantes';
import SaveCotizanteDetailData from './CotizantesDetail';
import PILA, { initialState as payPilaInitialState } from './PILA';
import NovedadIngreso from './NovedadIngreso';
import NovedadRetiro from './NovedadRetiro';
import CotizantesInfo, {
  initialState as cotizantesInfoInitialState,
} from './CotizantesInfo';
import NovedadVacaciones from './NovedadVacaciones';
import PrePlanilla from './PrePlanilla';
import NovedadVariacionPermanente from './NovedadVariacionPermanente';
import NovedadVariacionTransitoria from './NovedadVariacionTransitoria';
import NovedadIncapacidades from './NovedadIncapacidades';
import NovedadSuspensiones from './NovedadSuspensiones';
import DescuentosARL from './Descuentos';
import NovedadLicencias from './NovedadLicencias';
import PanelMultiempresa from './PanelMultiempresa';
import Navigation, {
  initialState as navigationInitialState,
} from './Navigation';
import PlanillaDetail from './PlanillaDetail';
import PlanillasHistory from './PlanillasHistory';
import PayButtonCheck from './PayButtonCheck';
import SnackBar from './SnackBar';
import CredentialsModal from './CredentialsModal';
import DetalleAportes from './DetalleAportesModal';

// INITIAL STATE FOR PERSISTED REDUCERS //

// import { initialState as currenUserInitialState } from './CurrentUser';
// import { initialState as companyInitialState } from './Company';
// import { initialState as cotizantesInitialState } from './Cotizantes';
// import { initialState as cotizantesInfoInitialState } from './CotizantesInfo';
// import { initialState as navigationInitialState } from './Navigation';
// import { initialState as payPilaInitialState } from './PILA';

const companyConfig = {
  key: 'company',
  storage,
  blacklist: [
    'editCompany',
    'editLegalRepresentative',
    'editBusinessContact',
    'newSucursalModal',
    'newSucursalName',
    'copyCompanyData',
    'copybusinessContactData',
    'copylegalRepresentativeData',
    'showClaseArlPicker',
    'companyRequiredFields',
    'legalRepresentativeRequiredFields',
    'businessContactRequiredFields',
    'showCheckCompanyModal',
  ],
};

const payPilaConfig = {
  key: 'payPila',
  storage,
  blacklist: [
    'currentStep',
    'validatePila',
    'completedStep',
    'fullAmount',
    'initialDate',
    'finalDate',
    'validationFailed',
    'paidCurrentPila',
  ],
};

const cotizantesInfoConfig = {
  key: 'cotizantesInfo',
  storage,
};

const currentUserConfig = {
  key: 'currentUser',
  storage,
};

const cotizantesConfig = {
  key: 'cotizantes',
  storage,
  blacklist: [
    'cotizantesRequiredFields',
    'requestBDUA',
    'copyCCF',
    'showOptionsModal',
  ],
};

const signInConfig = {
  key: 'signIn',
  storage,
  blacklist: ['showPassword'],
};

const navigationConfig = {
  key: 'navigation',
  storage,
  blacklist: ['blockScreen'],
};

const appReducer = combineReducers({
  signUp: SignUp,
  signIn: persistReducer(signInConfig, SignIn),
  currentUser: persistReducer(currentUserConfig, CurrentUser),
  fetchData: FetchData,
  company: persistReducer(companyConfig, Company),
  resources: Resources,
  cotizantes: persistReducer(cotizantesConfig, Cotizantes),
  cotizanteDetailData: SaveCotizanteDetailData,
  payPila: persistReducer(payPilaConfig, PILA),
  novedadIngreso: NovedadIngreso,
  novedadRetiro: NovedadRetiro,
  cotizantesInfo: persistReducer(cotizantesInfoConfig, CotizantesInfo),
  novedadVacaciones: NovedadVacaciones,
  prePlanilla: PrePlanilla,
  novedadVariacionPermanente: NovedadVariacionPermanente,
  novedadVariacionTransitoria: NovedadVariacionTransitoria,
  novedadIncapacidades: NovedadIncapacidades,
  novedadSuspensiones: NovedadSuspensiones,
  descuentosARL: DescuentosARL,
  novedadLicencias: NovedadLicencias,
  panelMultiempresa: PanelMultiempresa,
  navigation: persistReducer(navigationConfig, Navigation),
  planillaDetail: PlanillaDetail,
  planillasHistory: PlanillasHistory,
  payButtonCheck: PayButtonCheck,
  snackBar: SnackBar,
  credentialsModal: CredentialsModal,
  detalleAportes: DetalleAportes,
});

const persistedKeys = [
  'root',
  'currentUser',
  'company',
  'cotizantes',
  'cotizantesInfo',
  'navigation',
  'payPila',
];

const persistedReducersInitialState = {
  currentUser: currenUserInitialState,
  company: companyInitialState,
  cotizantes: cotizantesInitialState,
  cotizantesInfo: cotizantesInfoInitialState,
  navigation: navigationInitialState,
  payPila: payPilaInitialState,
};

export default (state, action) => {
  if (action.type === 'SIGNOUT_REQUEST') {
    // for all keys defined in your persistConfig(s)
    persistedKeys.forEach((key) => {
      storage.removeItem(`persist:${key}`);
    });
    // storage.removeItem('persist:otherKey')

    Object.keys(state).forEach((reducer) => {
      if (reducer !== 'signIn') {
        // eslint-disable-next-line no-param-reassign
        state = {
          ...state,

          [reducer]: persistedKeys.includes(reducer)
            ? {
                ...persistedReducersInitialState[reducer],
                _persist: {
                  version: -1,
                  rehydrated: true,
                },
              }
            : undefined,
        };
      }
    });
  }

  return appReducer(state, action);
};
