import { COTIZANTES } from './../ActionTypes';

export const initialState = {
	identificationType: 'CC',
	identificationNumber: '',
	cotizanteType: {
		code: '',
		display: 'Tipo empleado',
	},
	cotizanteSubtype: {
		code: '',
		display: 'Subtipo empleado',
	},
	salary: {
		dateCreated: '',
		value: '',
		dateApply: '',
		code: '',
	},
	openNewCotizanteDialog: false,
	cotizanteName: {
		display: '',
		firstName: '',
		secondName: '',
		firstSurname: '',
		secondSurname: '',
	},
	cotizanteGenre: {
		display: 'Sexo',
		value: -1,
	},
	cotizanteSalud: {
		code: '',
		display: 'Salud',
		value: {},
	},
	cotizantePension: {
		code: '',
		display: 'Pensión',
		value: {},
	},
	arlRisk: {
		code: -1,
		display: 'Nivel de riesgo',
		value: 'nivel de riesgo',
	},
	cotizanteSucursal: '',
	cotizanteEmail: '',
	cotizanteCard: false,
	joinDate: {
		display: 'DD/MM/AAAA',
		code: '',
		value: '',
	},
	enableJoinDate: false,
	selectedMinSalary: false,
	selectedIntegralSalary: false,
	firstPila: false,
	newEntry: false,
	activeInCompany: true,
	activeText: 'activo',
	deleteInCompany: false,
	retired: false,
	dateRetired: '',
	cotizantesRequiredFields: [],
	initialFields: true,
	requestBDUA: false,
	cajaCompensacion: {
		value: null,
		code: '',
		display: '',
		province: '',
	},
	copyCCF: null,
	showOptionsModal: false,
	cotizanteProvince: {
		display: '',
		code: '',
		value: '',
	},
	cotizanteCity: {
		display: '',
		code: '',
		value: '',
	},
};

const saveCotizanteData = (state = initialState, action) => {
	switch (action.type) {
		case COTIZANTES.COTIZANTES_SAVE_IDENTIFICATION_TYPE:
			return {
				...state,
				identificationType: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_IDENTIFICATION_NUMBER:
			return {
				...state,
				identificationNumber: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_TYPE:
			return {
				...state,
				cotizanteType: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SUBTYPE:
			return {
				...state,
				cotizanteSubtype: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALARY:
			return {
				...state,
				salary: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_RESET_COTIZANTE_FIELDS:
			return {
				...state,
				...initialState,
			};

		case COTIZANTES.COTIZANTES_OPEN_NEW_COTIZANTE_DIALOG:
			return {
				...state,
				openNewCotizanteDialog: !state.openNewCotizanteDialog,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_NAME:
			return {
				...state,
				cotizanteName: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_GENRE:
			return {
				...state,
				cotizanteGenre: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALUD:
			return {
				...state,
				cotizanteSalud: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_PENSION:
			return {
				...state,
				cotizantePension: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_ARL_RISK:
			return {
				...state,
				arlRisk: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SUCURSAL:
			return {
				...state,
				cotizanteSucursal: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_COTIZANTE_EMAIL:
			return {
				...state,
				cotizanteEmail: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_CHANGE_CARD_TYPE_PROP:
			return {
				...state,
				cotizanteCard: !state.cotizanteCard,
			};

		case COTIZANTES.COTIZANTES_SAVE_JOIN_DATE:
			return {
				...state,
				joinDate: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_ENABLE_JOIN_DATE:
			return {
				...state,
				enableJoinDate: !state.enableJoinDate,
				newEntry: !state.newEntry,
			};

		case COTIZANTES.COTIZANTES_RESET_JOIN_DATE:
			return {
				...state,
				joinDate: {
					display: 'DD/MM/AAAA',
					code: '',
					value: '',
				},
			};

		case COTIZANTES.COTIZANTES_SELECT_MIN_SALARY:
			return {
				...state,
				selectedMinSalary: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SELECT_INTEGRAL_SALARY:
			return {
				...state,
				selectedIntegralSalary: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_DELETE_COTIZANTE_STATUS:
			return {
				...state,
			};

		case COTIZANTES.COTIZANTES_REQUIRED_FIELD:
			if (action.payload.value.status) {
				return {
					...state,
					cotizantesRequiredFields: [...state.cotizantesRequiredFields, action.payload.value.field],
				};
			} else {
				return {
					...state,
					cotizantesRequiredFields: state.cotizantesRequiredFields.filter(
						(item) => item !== action.payload.value.field
					),
				};
			}

		case COTIZANTES.COTIZANTES_CHANGE_INITIAL_FIELDS:
			return {
				...state,
				initialFields: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_CHANGE_BDUA_REQUEST_STATUS:
			return {
				...state,
				requestBDUA: !state.requestBDUA,
			};

		case COTIZANTES.COTIZANTES_SAVE_CCF:
			return {
				...state,
				cajaCompensacion: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_COPY_CCF:
			return {
				...state,
				copyCCF: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SHOW_OPTIONS_MODAL:
			return {
				...state,
				showOptionsModal: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_PROVINCE:
			return {
				...state,
				cotizanteProvince: action.payload.value,
			};

		case COTIZANTES.COTIZANTES_SAVE_CITY:
			return {
				...state,
				cotizanteCity: action.payload.value,
			};

		default:
			return state;
	}
};

export default saveCotizanteData;
