import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import RootReducer from './reducers';

const isDevelop = process.env.REACT_APP_ENV === 'DEV';

const rootConfig = {
  key: 'root',
  storage,
  blacklist: [
    'signUp',
    'signIn',
    'company',
    'cotizantes',
    'payPila',
    'novedadIngreso',
    'cotizantesInfo',
    'novedadRetiro',
    'novedadVacaciones',
    'novedadVariacionTransitoria',
    'novedadSuspensiones',
    'descuentosARL',
    'panelMultiempresa',
    'payButtonCheck',
    'snackBar',
    'navigation',
    'fetchData',
    'novedadVariacionPermanente',
    'credentialsModal',
    'detalleAportes',
  ],
  debug: isDevelop,
  writeFailHandler: isDevelop
    ? (error) => {
        console.error('Cuota excedida', error);
      }
    : null,
};

const persistedReducer = persistReducer(rootConfig, RootReducer);
const store = createStore(
  persistedReducer,
  // eslint-disable-next-line no-underscore-dangle
  window.__REDUX_DEVTOOLS_EXTENSION__ &&
    // eslint-disable-next-line no-underscore-dangle
    window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true, traceLimit: 25 })
  // isDevelop ?
  // 	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true, traceLimit: 25 })
  // 	: undefined
);
const persistor = persistStore(store);

export { store, persistor };
