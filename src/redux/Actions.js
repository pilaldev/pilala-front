/* Sign up */
export const signUpAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Sign in */
export const signInAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Current user */
export const currentUserAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Fetch Data */

export const fetchDataAction = (actionType, field, value) => {
  return {
    type: actionType,
    payload: { field, value },
  };
};

/* Company */
export const companyAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Resources */
export const resourcesAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Cotizantes */
export const cotizantesAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Cotizantes Detalle */
export const cotizantesDetailAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Pay pila */
export const payPilaAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Lineas registro cotizantes */
export const lineaRegistroAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Novedad ingreso */
export const novedadIngresoAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Novedad retiro */
export const novedadRetiroAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Información de cotizantes */
export const cotizantesInfoAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* PrePlanilla */
export const prePlanillaAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Novedad vacaciones */
export const novedadVacacionesAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Novedad Variacion Permanente de Salario */

export const novedadVariacionPermanenteAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Novedad variacion transitoria */

export const novedadVariacionTransitoriaAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Novedad de incapacidades */

export const novedadIncapacidadesAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Novedad suspensiones */

export const novedadSuspensionesAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Reporte de Descuentos */

export const descuentosAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Novedad de Licencias */

export const novedadLicenciasAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Panel multiempresa */
export const panelMultiempresaAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Navigation */
export const navigationAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Planilla Detaul */
export const planillaDetailAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Pay Button Check */
export const payButtonCheckAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Planillas history */
export const planillasHistoryAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* SnackBar */

export const snackBarAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Sign Out Clean Store and State */

export const signOutAction = () => {
  return {
    type: 'SIGNOUT_REQUEST',
  };
};

/* Credentials modal action */

export const credentialsModalAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};

/* Detalle aportes modal action */

export const detalleAportesModalAction = (actionType, value) => {
  return {
    type: actionType,
    payload: {
      value,
    },
  };
};
