import React, { Component } from 'react';
import { connect } from 'react-redux';
import SettingsLayout from './SettingsLayout';
import { FIREBASE_AUTH } from './../api/Firebase';
import Endpoints from '@bit/pilala.pilalalib.endpoints';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import {
  currentUserAction,
  companyAction,
  fetchDataAction,
  resourcesAction,
  prePlanillaAction,
  novedadVariacionTransitoriaAction,
  cotizantesInfoAction,
  payPilaAction,
  navigationAction,
  planillaDetailAction,
  snackBarAction,
} from './../redux/Actions';
import {
  CURRENT_USER,
  COMPANY,
  FETCH_DATA,
  RESOURCES,
  PRE_PLANILLA,
  COTIZANTE_INFO,
  VARIACION_TRANSITORIA,
  PAY_PILA,
  NAVIGATION,
  PLANILLA_DETAIL,
  SNACKBAR,
} from './../redux/ActionTypes';
// import UpdateOnlineStatus from './../utils/UpdateOnlineStatus';

class Settings extends Component {
  state = {
    disableChangeEmailButton: false,
    disableChangePasswordButton: false,
    editUserEmailStatus: false,
    editUserPasswordStatus: false,
  };

  componentDidMount() {
    ChangePageTitle('Configuración | Pilalá');
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'configuracion'
    );
  }

  changeUserEmail = (email) => {
    if (this.state.disableChangeEmailButton) return;
    this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, true);
    this.setState({
      disableChangeEmailButton: true,
    });

    if (email !== null && email !== '') {
      const { uid } = FIREBASE_AUTH.currentUser;
      const requestData = {
        userUid: uid,
        currentEmail: this.props.currentEmail,
        newEmail: email,
      };

      fetch(Endpoints.updateEmail, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.status === 'SUCCESS') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Actualización exitosa',
              description:
                'El correo electrónico ha sido actualizado correctamente',
              color: 'success',
            });
            FIREBASE_AUTH.signOut().then(async () => {
              this.setState({
                disableChangeEmailButton: false,
              });
              this.props.navigationAction(
                NAVIGATION.NAVIGATION_BLOCK_SCREEN,
                false
              );
            });
          } else if (data.status === 'FAILED') {
            switch (data.errorCode) {
              case 'auth/email-already-exists':
                this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                  title: 'Error al actualizar correo electrónico',
                  description:
                    'Este correo electrónico ya pertenece a otra cuenta, por favor ingrese uno diferente',
                  color: 'error',
                });
                this.setState({
                  disableChangeEmailButton: false,
                });
                break;

              case 'auth/invalid-email':
                this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                  title: 'Error al actualizar correo electrónico',
                  description:
                    'El correo electrónico ingresado no es válido, por favor ingrese otro',
                  color: 'error',
                });
                this.setState({
                  disableChangeEmailButton: false,
                });
                break;

              default:
                this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                  title: 'Error al actualizar correo electrónico',
                  description:
                    'Hemos tenido problemas para actualizar tu correo electrónico, inténtalo más tarde',
                  color: 'error',
                });

                this.setState({
                  disableChangeEmailButton: false,
                });
                break;
            }
            this.props.navigationAction(
              NAVIGATION.NAVIGATION_BLOCK_SCREEN,
              false
            );
          }
        })
        .catch((error) => {
          console.log('Error al solicitar el cambio de email', error);

          this.props.navigationAction(
            NAVIGATION.NAVIGATION_BLOCK_SCREEN,
            false
          );
          this.setState({
            disableChangeEmailButton: false,
          });
        });
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Información requerida',
        description: 'Por favor ingrese un correo electrónico válido',
        color: 'warning',
      });
      this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false);
      this.setState({
        disableChangeEmailButton: false,
      });
    }
  };

  changeUserPassword = (currentPassword, newPasswordOne, newPasswordTwo) => {
    if (this.state.disableChangePasswordButton) return;
    this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, true);
    this.setState({
      disableChangePasswordButton: true,
    });

    if (
      currentPassword === '' ||
      newPasswordOne === '' ||
      newPasswordTwo === ''
    ) {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Información requerida',
        description: 'Por favor ingresa la contraseña actual y la nueva',
        color: 'warning',
      });
      this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false);
      this.setState({
        disableChangePasswordButton: false,
      });
      return;
    }

    if (currentPassword === newPasswordOne) {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'No se puede continuar la acción',
        description: 'La contraseña nueva debe ser diferente a la actual',
        color: 'info',
      });
      this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false);
      this.setState({
        disableChangePasswordButton: false,
      });
      return;
    }

    if (newPasswordOne.length < 8) {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'No se puede continuar la acción',
        description: 'La nueva contraseña debe tener mínimo 8 caracteres',
        color: 'info',
      });
      this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false);
      this.setState({
        disableChangePasswordButton: false,
      });
      return;
    }

    if (newPasswordOne !== newPasswordTwo) {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Información requerida',
        description: 'Por favor confirme la nueva contraseña',
        color: 'warning',
      });
      this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false);
      this.setState({
        disableChangePasswordButton: false,
      });
      return;
    }

    const { uid } = FIREBASE_AUTH.currentUser;
    const requestData = {
      userUid: uid,
      newPassword: newPasswordOne,
    };

    fetch(Endpoints.updatePassword, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 'SUCCESS') {
          this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Actualización exitosa',
            description: 'La contraseña fue actualizada correctamente',
            color: 'success',
          });
          this.props.navigationAction(
            NAVIGATION.NAVIGATION_BLOCK_SCREEN,
            false
          );
          this.setState({
            editUserPasswordStatus: false,
            disableChangePasswordButton: false,
          });
        } else if (data.status === 'FAILED') {
          switch (data.errorCode) {
            case 'auth/invalid-password':
              this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: 'Error al actualizar la contraseña',
                description:
                  'La nueva contraseña debe tener mínimo 8 caracteres',
                color: 'error',
              });
              this.setState({
                disableChangePasswordButton: false,
              });
              break;

            default:
              this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: 'Error al actualizar la contraseña',
                description:
                  'Hemos tenido problemas para actualizar tu contraseña, inténtelo más tarde',
                color: 'error',
              });
              this.setState({
                disableChangePasswordButton: false,
              });
              break;
          }
          this.props.navigationAction(
            NAVIGATION.NAVIGATION_BLOCK_SCREEN,
            false
          );
        }
      })
      .catch((error) => {
        console.log('Error al solicitar el cambio de contraseña', error);

        this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false);
        this.setState({
          disableChangePasswordButton: false,
        });
      });
  };

  editUserEmail = () => {
    this.setState({
      editUserEmailStatus: !this.state.editUserEmailStatus,
    });
  };

  editUserPassword = () => {
    this.setState({
      editUserPasswordStatus: !this.state.editUserPasswordStatus,
    });
  };

  clearReduxOnSignOut = () => {
    this.props.currentUserAction(CURRENT_USER.CURRENT_USER_RESET_DATA, {});
    this.props.companyAction(COMPANY.COMPANY_RESET_DATA, {});
    this.props.fetchDataAction(FETCH_DATA.RESET_REQUEST_STATUS, '', {});
    this.props.resourcesAction(RESOURCES.RESOURCES_RESET_DATA, {});
    this.props.cotizantesInfoAction(
      COTIZANTE_INFO.COTIZANTE_INFO_RESET_DATA,
      {}
    );
    this.props.prePlanillaAction(PRE_PLANILLA.PRE_PLANILLA_RESET_DATA, {});
    this.props.novedadVariacionTransitoriaAction(
      VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_RESET_DATA,
      {}
    );
    this.props.payPilaAction(PAY_PILA.PAY_PILA_RESET_FIELDS, null);
    this.props.navigationAction(NAVIGATION.NAVIGATION_RESET_FIELDS, null);
    this.props.planillaDetailAction(
      PLANILLA_DETAIL.PLANILLA_DETAIL_RESET_DATA,
      null
    );
  };

  render() {
    const layoutProps = {
      disableChangeEmailButton: this.state.disableChangeEmailButton,
      disableChangePasswordButton: this.state.disableChangePasswordButton,
      changeUserEmail: this.changeUserEmail,
      editUserEmailStatus: this.state.editUserEmailStatus,
      editUserEmail: this.editUserEmail,
      editUserPasswordStatus: this.state.editUserPasswordStatus,
      editUserPassword: this.editUserPassword,
      changeUserPassword: this.changeUserPassword,
    };

    return <SettingsLayout {...layoutProps} />;
  }
}

const mapStateToProps = (state) => {
  return {
    currentEmail: state.currentUser.email,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDataAction: (actionType, field, value) =>
      dispatch(fetchDataAction(actionType, field, value)),
    currentUserAction: (actionType, value) =>
      dispatch(currentUserAction(actionType, value)),
    resourcesAction: (actionType, value) =>
      dispatch(resourcesAction(actionType, value)),
    companyAction: (actionType, value) =>
      dispatch(companyAction(actionType, value)),
    prePlanillaAction: (actionType, value) =>
      dispatch(prePlanillaAction(actionType, value)),
    cotizantesInfoAction: (actionType, value) =>
      dispatch(cotizantesInfoAction(actionType, value)),
    novedadVariacionTransitoriaAction: (actionType, value) =>
      dispatch(novedadVariacionTransitoriaAction(actionType, value)),
    payPilaAction: (actionType, value) =>
      dispatch(payPilaAction(actionType, value)),
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
    planillaDetailAction: (actionType, value) =>
      dispatch(planillaDetailAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
