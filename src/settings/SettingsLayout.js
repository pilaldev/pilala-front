import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
    Typography,
    Card,
    ButtonBase,
    TextField,
    CircularProgress,
    IconButton,
    // TableHead,
    // TableRow,
    // TableCell
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import HelperFunctions from './../utils/HelperFunctions';
// import PictureAsPdfTwoToneIcon from '@material-ui/icons/PictureAsPdfTwoTone';
import BorderColorTwoToneIcon from '@material-ui/icons/BorderColorTwoTone';
import {
    SUBTITLE_ONE,
    FULL_NAME,
    DOCUMENT_NUMBER,
    EMAIL,
    CHANGE_BUTTON,
    CANCEL_BUTTON,
    PASSWORD,
    CHANGE_PASSWORD,
    SERVICE_STATUS,
    // RAZON_SOCIAL,
    // PERIODO,
    // CARGO_BASICO,
    // ADICIONALES,
    // TOTAL,
    // SUBTITLE_TWO,
    SERVICE_STATUS_LABEL
} from './Constants';
import clsx from 'clsx';
import CheckCircleTwoToneIcon from '@material-ui/icons/CheckCircleTwoTone';
// import { CreditCard } from '../components';
// import ErrorOutlineRoundedIcon from '@material-ui/icons/ErrorOutlineRounded';
// import InfoTable from '../components/InfoTable';

import './styles.css';

const SettingsLayout = (props) => {

    const classes = useStyles();
    const [emailInput, setEmailInput] = useState('');
    const [currentPasswordInput, setCurrentPasswordInput] = useState('');
    const [newPasswordOneInput, setNewPasswordOneInput] = useState('');
    const [newPasswordTwoInput, setNewPasswordTwoInput] = useState('');
    const userName = useSelector(state => state.currentUser.name);
    const userEmail = useSelector(state => state.currentUser.email);
    const userDocumentNumber = useSelector(state => state.currentUser.documentNumber);

    const cancelEditEmail = () => {
        if (props.editUserEmailStatus === true) {
            setEmailInput('');
            props.editUserEmail();
        }
    }

    const cancelEditPassword = () => {
        if (props.editUserPasswordStatus === true) {
            setCurrentPasswordInput('');
            setNewPasswordOneInput('');
            setNewPasswordTwoInput('');
            props.editUserPassword();
        }
    }

    const getNewUserEmail = (event) => {
        const temp_email = event.target.value;
        setEmailInput(temp_email);
    }

    const saveNewUserEmail = (e) => {
        e.preventDefault();
        props.changeUserEmail(emailInput);
    }

    const getCurrentUserPassword = (event) => {
        const temp_password = event.target.value;
        setCurrentPasswordInput(temp_password);
    }

    const getNewUserPasswordOne = (event) => {
        const temp_password = event.target.value;
        setNewPasswordOneInput(temp_password);
    }

    const getNewUserPasswordTwo = (event) => {
        const temp_password = event.target.value;
        setNewPasswordTwoInput(temp_password);
    }

    const saveNewUserPassword = (e) => {
        e.preventDefault();
        props.changeUserPassword(currentPasswordInput, newPasswordOneInput, newPasswordTwoInput);
    }

    return (
        <div className={classes.container} id='settingsContainer'>
            <Typography className={classes.title}>
                {`¡Hola ${userName.split(' ')[0]}!`}
            </Typography>
            <Typography className={classes.userSubtitle}>
                {SUBTITLE_ONE}
            </Typography>
            <div className={classes.userContentContainer}>
                <Card className={classes.userInfoCard}>
                    <div className={classes.cardContentContainer}>
                        <div className={classes.nameTextContainer}>
                            <Typography className={classes.nameText}>
                                {FULL_NAME}
                            </Typography>
                        </div>
                        <div className={classes.nameContainer}>
                            <Typography className={classes.name}>
                                {userName}
                            </Typography>
                        </div>
                    </div>
                    <div className={classes.cardContentContainer}>
                        <div className={classes.idTextContainer}>
                            <Typography className={classes.idText}>
                                {DOCUMENT_NUMBER}
                            </Typography>
                        </div>
                        <div className={classes.idContainer}>
                            <Typography className={classes.id}>
                                {HelperFunctions.formatStringNumber(userDocumentNumber)}
                            </Typography>
                        </div>
                    </div>
                </Card>
                <Card className={classes.credentialsCard}>
                    <form className={classes.cardContentContainer}>
                        <div className={classes.emailTextContainer}>
                            <Typography className={classes.emailText}>
                                {EMAIL}
                            </Typography>
                        </div>
                        <div className={classes.emailContentContainer}>
                            <div className={classes.emailContainer}>
                                {
                                    props.editUserEmailStatus ?
                                        <TextField
                                            variant='outlined'
                                            value={emailInput}
                                            onChange={getNewUserEmail}
                                            className={classes.editEmailInput}
                                            size='small'
                                            inputProps={{
                                                className: classes.editEmailInputProps
                                            }}
                                            classes={{
                                                root: classes.emailInput
                                            }}
                                        />
                                        :
                                        <Typography className={classes.email}>
                                            {userEmail}
                                        </Typography>
                                }
                                {
                                    !props.editUserEmailStatus ?
                                        <IconButton
                                            size='small'
                                            onClick={props.editUserEmail}
                                            className={classes.editIconButton}
                                        >
                                            <BorderColorTwoToneIcon className={classes.editIcon} />
                                        </IconButton>
                                        : null
                                }
                            </div>
                            {
                                props.editUserEmailStatus ?
                                    <div className={classes.emailButtonsContainer}>
                                        <ButtonBase
                                            className={classes.cancelChangeEmailButton}
                                            disableRipple
                                            onClick={cancelEditEmail}
                                            disabled={props.disableChangeEmailButton}
                                        >
                                            <Typography className={clsx(
                                                classes.cancelChangeEmailText,
                                                {
                                                    [classes.disabledButtonText]: props.disableChangeEmailButton
                                                }
                                            )}>
                                                {CANCEL_BUTTON}
                                            </Typography>
                                        </ButtonBase>
                                        <ButtonBase
                                            className={classes.changeEmailButton}
                                            onClick={saveNewUserEmail}
                                            type='submit'
                                        >
                                            {
                                                props.disableChangeEmailButton ?
                                                    <CircularProgress size={18} className={classes.spinner} />
                                                    :
                                                    <Typography className={classes.changeEmailText}>
                                                        {CHANGE_BUTTON}
                                                    </Typography>
                                            }
                                        </ButtonBase>
                                    </div>
                                    :
                                    null
                            }
                        </div>
                    </form>
                    <div className={classes.cardContentContainer}>
                        <div className={classes.passwordTextContainer}>
                            <Typography className={classes.passwordText}>
                                {PASSWORD}
                            </Typography>
                        </div>
                        {
                            props.editUserPasswordStatus ?
                                <form className={classes.newPasswordContainer} >
                                    <TextField
                                        variant='outlined'
                                        type='password'
                                        onChange={getCurrentUserPassword}
                                        placeholder='Contraseña actual'
                                        className={classes.editPasswordInput}
                                        inputProps={{
                                            className: classes.editPasswordInputProps
                                        }}
                                        InputProps={{
                                            autoComplete: 'current-password',
                                            type: 'text'
                                        }}
                                        classes={{
                                            root: classes.emailInput
                                        }}
                                    />
                                    <TextField
                                        variant='outlined'
                                        type='password'
                                        onChange={getNewUserPasswordOne}
                                        placeholder='Nueva contraseña'
                                        className={classes.editPasswordInput}
                                        inputProps={{
                                            className: classes.editPasswordInputProps
                                        }}
                                        InputProps={{
                                            autoComplete: 'new-password',
                                            type: 'text'
                                        }}
                                        classes={{
                                            root: classes.emailInput
                                        }}
                                    />
                                    <TextField
                                        variant='outlined'
                                        type='password'
                                        onChange={getNewUserPasswordTwo}
                                        placeholder='Confirmar contraseña'
                                        className={classes.editPasswordInput}
                                        inputProps={{
                                            className: classes.editPasswordInputProps
                                        }}
                                        InputProps={{
                                            autoComplete: 'new-password',
                                            type: 'text'
                                        }}
                                        classes={{
                                            root: classes.emailInput
                                        }}
                                    />
                                    <div className={classes.editPasswordButtonsContainer}>
                                        <ButtonBase
                                            className={classes.cancelChangePasswordButton}
                                            disableRipple
                                            onClick={cancelEditPassword}
                                            disabled={props.disableChangePasswordButton}
                                        >
                                            <Typography className={clsx(classes.cancelChangePasswordText,
                                                {
                                                    [classes.disabledButtonText]: props.disableChangePasswordButton
                                                }
                                            )}>
                                                {CANCEL_BUTTON}
                                            </Typography>
                                        </ButtonBase>
                                        <ButtonBase
                                            className={classes.savePasswordButton}
                                            onClick={saveNewUserPassword}
                                            type='submit'
                                        >
                                            {
                                                props.disableChangePasswordButton ?
                                                    <CircularProgress size={18} className={classes.spinner} />
                                                    :
                                                    <Typography className={classes.savePasswordText}>
                                                        {CHANGE_BUTTON}
                                                    </Typography>
                                            }
                                        </ButtonBase>
                                    </div>
                                </form>
                                :
                                <div className={classes.passwordContainer}>
                                    <ButtonBase
                                        className={classes.changePasswordButton}
                                        onClick={props.editUserPassword}
                                    >
                                        <Typography className={classes.changePasswordText}>
                                            {CHANGE_PASSWORD}
                                        </Typography>
                                    </ButtonBase>
                                </div>
                        }
                    </div>
                </Card>
                <Card className={classes.statusCard}>
                    <Typography className={classes.statusText}>
                        {SERVICE_STATUS}
                    </Typography>
                    <div className={true ? classes.statusEnabledContainer : classes.statusDisabledContainer}>
                        <Typography className={true ? classes.statusEnabledText : classes.statusDisabledText}>
                            {true ? SERVICE_STATUS_LABEL[1] : SERVICE_STATUS_LABEL[2]}
                        </Typography>
                    </div>
                </Card>
            </div>
            <Typography className={classes.paymentSubtitle}>
                Detalle de tu suscripción
            </Typography>
            <div className={classes.paymentContainer}>
                <div className={classes.paymentPlan}>
                    <CheckCircleTwoToneIcon className={classes.checkIcon} />
                    <Typography className={classes.planTitle}>
                        Plan Pymes Pilalá
                    </Typography>
                    <div className={classes.priceContainer}>
                        <Typography className={classes.planSubtitleOne}>
                            {'$20.000/'}
                        </Typography>
                        <Typography className={classes.planSubtitleMonth}>
                            {'mes'}
                        </Typography>
                    </div>
                    <Typography className={classes.planSubtitleTwo}>
                        Más $2.000 por cotizante liquidado
                    </Typography>
                    {/* <div className={classes.paymentMessageContainer}>
                        <ErrorOutlineRoundedIcon className={classes.paymentMessageIcon} />
                        <Typography className={classes.paymentMessageText}>
                            {`Tienes un saldo pendiente de $${HelperFunctions.formatStringNumber(60000)}.`}
                        </Typography>
                    </div>
                    <ButtonBase className={classes.payButton}>
                        <Typography className={classes.payButtonText}>
                            Pagar
                        </Typography>
                    </ButtonBase> */}
                </div>
                {/* <div className={classes.paymentMethodContainer}>
                    <CreditCard />
                </div> */}
            </div>
            {/* <div className={classes.historyContentContainer}>
                <Typography className={classes.historySubtitle}>
                    {SUBTITLE_TWO}
                </Typography>

                <div className={classes.historyContainer}>
                    <InfoTable
                        data={[1, 2, 3, 4, 4, 4]}
                        RowItem={(props) =>
                            <RowLayout
                                {...props}
                            />
                        }
                        enablePagination={true}
                        TableHeader={TableHeader}
                        tableStyles={{
                            minHeight: [1, 2, 3, 4, 4, 4].length === 0 ? 200 : 0
                        }}
                    />
                </div>
            </div> */}
        </div>
    );
}

/*const RowLayout = ({ row, index }) => {
    const classes = useStyles();

    return (
        <TableRow
            style={{
                backgroundColor: (index % 2) === 0 ? '#F5F5F5' : '#FFFFFF'
            }}
        >
            <TableCell className={classes.rowCell}>
                Industria Repostería SAS
            </TableCell>
            <TableCell align='center' className={classes.rowCell}>
                Julio 2020
            </TableCell>
            <TableCell align='center' className={classes.rowCell}>
                {`$${HelperFunctions.formatStringNumber(19900)}`}
            </TableCell>
            <TableCell align='center' className={classes.rowCell}>
                {`$${HelperFunctions.formatStringNumber(11500)}`}
            </TableCell>
            <TableCell align='center' className={classes.rowCell}>
                {`$${HelperFunctions.formatStringNumber(31400)}`}
            </TableCell>
            <TableCell align='center'>
                <ButtonBase disableRipple>
                    <PictureAsPdfTwoToneIcon />
                </ButtonBase>
            </TableCell>
        </TableRow>
    );
}

const TableHeader = () => {
    const classes = useStyles();

    return (
        <TableHead>
            <TableRow>
                <TableCell className={classes.headerTitle}>
                    {RAZON_SOCIAL}
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    {PERIODO}
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    {CARGO_BASICO}
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    {ADICIONALES}
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    {TOTAL}
                </TableCell>
                <TableCell />
            </TableRow>
        </TableHead>
    );
}
*/
const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flex: 1,
        width: '100%',
        overflow: 'auto',
        padding: '2.5rem 2rem 3rem 3rem',
        flexDirection: 'column',
        [theme.breakpoints.down('xs')]: {
            padding: '2rem 1rem'
        },
        backgroundColor: '#FFFFFF'
    },
    title: {
        fontWeight: 'bold',
        color: '#263238',
        textAlign: 'left',
        [theme.breakpoints.up('xs')]: {
            fontSize: '1.3rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '1.5rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.8rem'
        }
    },
    userSubtitle: {
        fontSize: '1rem',
        fontWeight: 'bold',
        color: '#263238',
        marginBottom: '0.5rem',
        textAlign: 'left',
        marginTop: '2.5rem',
        [theme.breakpoints.down('xs')]: {
            marginTop: '2rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.2rem'
        }
    },
    userContentContainer: {
        display: 'flex',
        alignItems: 'flex-start',
        width: '100%',
        justifyContent: 'space-between',
        marginBottom: '3rem',
        flexWrap: 'wrap'
    },
    userInfoCard: {
        display: 'flex',
        flexDirection: 'column',
        padding: '1.15rem 1rem',
        boxShadow: '0px 3px 16px #00000029',
        borderRadius: 7,
        marginBottom: '2rem',
        [theme.breakpoints.up('xs')]: {
            minWidth: '100%',
            maxWidth: '100%',
        },
        [theme.breakpoints.up('sm')]: {
            minWidth: '85%',
            maxWidth: '85%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '47.5%',
            maxWidth: '47.5%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '35%',
            maxWidth: '35%',
            marginBottom: '0rem',
        }
    },
    credentialsCard: {
        display: 'flex',
        flexDirection: 'column',
        padding: '1rem',
        boxShadow: '0px 3px 16px #00000029',
        borderRadius: 7,
        marginBottom: '2rem',
        [theme.breakpoints.up('xs')]: {
            minWidth: '100%',
            maxWidth: '100%'
        },
        [theme.breakpoints.up('sm')]: {
            minWidth: '85%',
            maxWidth: '85%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '47.5%',
            maxWidth: '47.5%',
            marginBottom: '0rem',
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '35%',
            maxWidth: '35%',

        }
    },
    cardContentContainer: {
        display: 'flex',
        alignItems: 'flex-start',
        width: '100%',
        margin: '0.25rem 0rem',
        [theme.breakpoints.up('xs')]: {
            flexDirection: 'column',
            alignItems: 'flex-start',
        },
        [theme.breakpoints.up('sm')]: {
            flexDirection: 'row'
        }
    },
    nameTextContainer: {
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '35%'
        }
    },
    nameText: {
        textAlign: 'left',
        color: '#95989A',
        fontWeight: 'bold',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    nameContainer: {
        display: 'flex',
        flex: 1
    },
    name: {
        textAlign: 'left',
        color: '#263238',
        fontWeight: 'bold',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    idTextContainer: {
        width: '35%'
    },
    idText: {
        textAlign: 'left',
        color: '#95989A',
        fontWeight: 'bold',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    idContainer: {
        display: 'flex',
        flex: 1
    },
    id: {
        textAlign: 'left',
        color: '#263238',
        fontWeight: 'bold',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    emailTextContainer: {
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '35%'
        }
    },
    emailText: {
        textAlign: 'left',
        color: '#95989A',
        fontWeight: 'bold',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    emailContainer: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        width: '100%'
    },
    email: {
        textAlign: 'left',
        color: '#263238',
        fontWeight: 'bold',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    emailContentContainer: {
        flexDirection: 'column',
        display: 'flex',
        flex: 1,
        width: '100%'
    },
    passwordTextContainer: {
        width: '35%'
    },
    passwordText: {
        textAlign: 'left',
        color: '#95989A',
        fontWeight: 'bold',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    passwordContainer: {
        display: 'flex',
        flex: 1
    },
    password: {
        textAlign: 'left',
        color: '#263238',
        fontWeight: 'bold',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    statusCard: {
        display: 'flex',
        flexDirection: 'column',
        padding: '1.2rem 1rem',
        boxShadow: '0px 3px 16px #00000029',
        borderRadius: 7,
        [theme.breakpoints.up('xs')]: {
            minWidth: '100%',
            maxWidth: '100%'
        },
        [theme.breakpoints.up('sm')]: {
            minWidth: '40%',
            maxWidth: '40%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '30%',
            maxWidth: '30%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '20%',
            maxWidth: '20%'
        }
    },
    statusText: {
        textAlign: 'left',
        color: '#95989A',
        fontWeight: 'bold',
        marginBottom: '0.5rem',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    statusDisabledContainer: {
        backgroundColor: '#FF495A26',
        height: '2rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        border: '1px solid #FF495A',
        [theme.breakpoints.up('xs')]: {
            minWidth: '100%',
            maxWidth: '100%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '60%',
            maxWidth: '60%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '100%',
            maxWidth: '100%'
        }
    },
    statusDisabledText: {
        textAlign: 'left',
        color: '#FF495A',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    statusEnabledContainer: {
        backgroundColor: '#4CAF5026',
        height: '2rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        border: '1px solid #4CAF50',
        [theme.breakpoints.up('xs')]: {
            minWidth: '100%',
            maxWidth: '100%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '60%',
            maxWidth: '60%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '100%',
            maxWidth: '100%'
        }
    },
    statusEnabledText: {
        textAlign: 'left',
        color: '#4CAF50',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    historySubtitle: {
        fontSize: '1rem',
        fontWeight: 'bold',
        color: '#263238',
        marginBottom: '0.5rem',
        textAlign: 'left',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.2rem'
        }
    },
    historyCard: {
        width: '100%',
        boxShadow: '0px 3px 16px #00000029',
        borderRadius: 7
    },
    headerContainer: {
        display: 'flex',
        alignItems: 'center',
        padding: '2% 2% 2% 2%'
    },
    razonSocialTitle: {
        fontSize: '1.3em',
        fontWeight: 'bold',
        color: '#455A64'
    },
    periodoTitle: {
        fontSize: '1.3em',
        fontWeight: 'bold',
        color: '#455A64'
    },
    cargoBasicoTitle: {
        fontSize: '1.3em',
        fontWeight: 'bold',
        color: '#455A64'
    },
    adicionalesTitle: {
        fontSize: '1.3em',
        fontWeight: 'bold',
        color: '#455A64'
    },
    totalTitle: {
        fontSize: '1.3em',
        fontWeight: 'bold',
        color: '#455A64'
    },
    listItemContainer: {
        width: '100%',
        padding: '2%',
        alignItems: 'center',
        display: 'flex'
    },
    razonSocial: {
        fontSize: '1rem',
        color: '#455A64',
        textAlign: 'left'
    },
    periodo: {
        fontSize: '1rem',
        color: '#455A64'
    },
    cargoBasico: {
        fontSize: '1rem',
        color: '#455A64'
    },
    adicionales: {
        fontSize: '1rem',
        color: '#455A64',
    },
    total: {
        fontSize: '1rem',
        color: '#455A64',
    },
    statusContainerCorrect: {
        border: '2px solid #95989A',
        backgroundColor: '#95989A26',
        borderRadius: 5,
        width: '80%'

    },
    statusTextCorrect: {
        color: '#95989A',
        fontSize: '1rem',
        fontWeight: 'bold'
    },
    statusContainerIncorrect: {
        border: '2px solid #FF9E00',
        backgroundColor: '#FF9E0026',
        borderRadius: 5,
        width: '80%',
        cursor: 'pointer'
    },
    statusTextIncorrect: {
        color: '#FF9E00',
        fontSize: '1rem',
        fontWeight: 'bold'
    },
    razonSocialContainer: {
        display: 'flex',
        flex: 0.23
    },
    razonSocialTitleContainer: {
        display: 'flex',
        flex: 0.33,
        alignItems: 'center'
    },
    periodoTitleContainer: {
        display: 'flex',
        flex: 0.11,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cargoBasicoTitleContainer: {
        display: 'flex',
        flex: 0.17,
        justifyContent: 'center',
        alignItems: 'center'
    },
    adicionalesTitleContainer: {
        display: 'flex',
        flex: 0.16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    totalTitleContainer: {
        display: 'flex',
        flex: 0.17,
        justifyContent: 'center',
        alignItems: 'center'
    },
    statusMainContainer: {
        width: '10%',
        flex: 0.10,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    periodoMainContainer: {
        display: 'flex',
        alignItems: 'center',
        flex: 0.11,
        justifyContent: 'center'
    },
    cargoBasicoMainContainer: {
        display: 'flex',
        alignItems: 'center',
        flex: 0.17,
        justifyContent: 'center'
    },
    adicionalesMainContainer: {
        display: 'flex',
        alignItems: 'center',
        flex: 0.16,
        justifyContent: 'center'
    },
    totalMainContainer: {
        display: 'flex',
        alignItems: 'center',
        flex: 0.17,
        justifyContent: 'center'
    },
    pdfIconContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 0.06
    },
    editIcon: {
        color: '#2962FF',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    editIconButton: {
        marginLeft: '1rem'
    },
    emailInput: {
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#707070',
            },
            '&:hover fieldset': {
                borderColor: '#6A32B5',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#6A32B5',
            },
        },
    },
    editEmailInput: {
        width: '100%'
    },
    editEmailInputProps: {
        padding: '0.4rem 0.5rem',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    editPasswordInputProps: {
        padding: '0.4rem 0.5rem',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    emailButtonsContainer: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: '0.5rem'
    },
    changeEmailButton: {
        height: '2rem',
        borderRadius: 50,
        backgroundColor: '#6A32B5',
        [theme.breakpoints.up('xs')]: {
            minWidth: '30%',
            maxWidth: '30%',
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '32%',
            maxWidth: '32%',
        }
    },
    changeEmailText: {
        color: '#FFFFFF',
        fontSize: '0.8rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '0.9rem'
        }
    },
    cancelChangeEmailButton: {
        height: '2rem',
        borderRadius: 50,
        [theme.breakpoints.up('xs')]: {
            minWidth: '30%',
            maxWidth: '30%',
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '32%',
            maxWidth: '32%',
        }
    },
    cancelChangeEmailText: {
        color: '#707070',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    changePasswordButton: {
        borderRadius: 7,
        border: '1px solid #707070',
        backgroundColor: '#70707026',
        padding: '0.1rem 0.5rem'
    },
    changePasswordText: {
        color: '#707070',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    newPasswordContainer: {
        display: 'flex',
        flex: 1,
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection: 'column',
        width: '100%'
    },
    editPasswordInput: {
        width: '100%',
        marginBottom: '0.5rem'
    },
    editPasswordButtonsContainer: {
        width: '100%',
        alignItems: 'flex-start',
        display: 'flex',
        justifyContent: 'space-evenly'
    },
    savePasswordButton: {
        height: '2rem',
        borderRadius: 50,
        backgroundColor: '#6A32B5',
        [theme.breakpoints.up('xs')]: {
            minWidth: '30%',
            maxWidth: '30%',
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '32%',
            maxWidth: '32%',
        }
    },
    savePasswordText: {
        color: '#FFFFFF',
        fontSize: '0.8rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '0.9rem'
        }
    },
    cancelChangePasswordButton: {
        height: '2rem',
        borderRadius: 50,
        [theme.breakpoints.up('xs')]: {
            minWidth: '30%',
            maxWidth: '30%',
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '32%',
            maxWidth: '32%',
        }
    },
    cancelChangePasswordText: {
        color: '#707070',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    spinner: {
        color: '#FFFFFF'
    },
    disabledButtonText: {
        color: '#707070'
    },
    paymentContainer: {
        display: 'flex',
        width: '100%',
        marginBottom: '3rem',
        alignItems: 'flex-start',
        flexDirection: 'column',
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row'
        }
    },
    paymentSubtitle: {
        fontSize: '1rem',
        fontWeight: 'bold',
        color: '#263238',
        textAlign: 'left',
        marginBottom: '0.5rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.2rem'
        }
    },
    paymentPlan: {
        display: 'flex',
        backgroundColor: '#FFFFFF',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        borderRadius: 7,
        boxShadow: '0px 3px 16px #00000029',
        padding: '0.8rem',
        border: '1px solid #6A32B5',
        position: 'relative',
        [theme.breakpoints.up('xs')]: {
            minWidth: '100%',
            maxWidth: '100%'
        },
        [theme.breakpoints.up('sm')]: {
            minWidth: '50%',
            maxWidth: '50%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '35%',
            maxWidth: '35%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '30%',
            maxWidth: '30%'
        },
        [theme.breakpoints.up('xl')]: {
            minWidth: '18%',
            maxWidth: '18%'
        }
    },
    planTitle: {
        textAlign: 'left',
        fontWeight: 'bold',
        color: '#37474F',
        marginBottom: '1rem',
        marginTop: '0.5rem',
        [theme.breakpoints.up('sm')]: {
            fontSize: '0.9rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '1rem'
        }
    },
    planSubtitleOne: {
        textAlign: 'left',
        color: '#37474F',
        fontWeight: 'bold',
        [theme.breakpoints.up('xs')]: {
            fontSize: '1rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '1.1rem'
        },
        [theme.breakpoints.up('xl')]: {
            fontSize: '1.2rem'
        }
    },
    planSubtitleMonth: {
        textAlign: 'left',
        color: '#37474F',
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.9rem'
        },
        [theme.breakpoints.up('xl')]: {
            fontSize: '1rem'
        }
    },
    planSubtitleTwo: {
        textAlign: 'left',
        color: '#37474F',
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.7rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('xl')]: {
            fontSize: '0.9rem'
        }
    },
    checkIcon: {
        color: '#6A32B5',
        fontSize: '1rem',
        position: 'absolute',
        top: 0,
        right: 0,
        margin: '0.3rem'
    },
    paymentMethodContainer: {
        [theme.breakpoints.up('xs')]: {
            minWidth: '100%',
            maxWidth: '100%',
            marginTop: '2rem'
        },
        [theme.breakpoints.up('sm')]: {
            minWidth: '50%',
            maxWidth: '50%',
            marginTop: '2rem'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '40%',
            maxWidth: '40%',
            marginLeft: '2rem',
            marginTop: 0
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '33%',
            maxWidth: '33%'
        },
        [theme.breakpoints.up('xl')]: {
            minWidth: '25%',
            maxWidth: '25%'
        }
    },
    paymentMethodHeaderContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    paymentMethodTitle: {
        color: '#37474F',
        fontWeight: 'bold',
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.9rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    paymentMethodButton: {
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        boxShadow: '0px 3px 6px #00000029',
        minHeight: '2rem',
        padding: '0px 0.4rem'
    },
    paymentMethodButtonText: {
        color: '#37474F',
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.7rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '0.9rem'
        }
    },
    paymentMethodNumberContainer: {
        display: 'flex',
        alignItems: 'center',
        marginTop: '0.5rem'
    },
    paymentMethodInfoContainer: {
        display: 'flex',
        alignItems: 'center',
        marginTop: '0.5rem',
        justifyContent: 'space-between'
    },
    paymentMethodButtonsContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: '1rem'
    },
    paymentMethodSaveButton: {
        borderRadius: 100,
        textTransform: 'none',
        backgroundColor: '#6A32B5',
        padding: '0px 1rem',
        minHeight: '2rem'
    },
    paymentMethodSaveButtonText: {
        color: '#FFFFFF',
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.7rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '0.9rem'
        }
    },
    paymentMethodCancelButton: {
        borderRadius: 100,
        textTransform: 'none',
        padding: '0px 1rem',
        minHeight: '2rem'
    },
    paymentMethodCancelButtonText: {
        color: '#37474F',
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.7rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '0.9rem'
        }
    },
    paymentInfoText: {
        color: '#37474F',
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    paymentInfoDate: {
        color: '#37474F',
        fontSize: '1rem',
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    cardLogoContainer: {
        minWidth: '2.5rem',
        maxWidth: '2.5rem',
        height: '2rem',
        marginRight: '1rem'
    },
    cardLogo: {
        width: '100%',
        height: 'auto'
    },
    cardInput: {
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.7rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.9rem'
        }
    },
    paymentMessageContainer: {
        backgroundColor: '#6A32B526',
        display: 'flex',
        borderRadius: 7,
        padding: '0.5rem',
        marginTop: '1rem'
    },
    paymentMessageIcon: {
        color: '#6A32B5',
        fontSize: '1.2rem',
        marginRight: '0.5rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.5rem'
        }
    },
    paymentMessageText: {
        textAlign: 'left',
        color: '#6A32B5',
        fontSize: '0.9rem',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    payButton: {
        display: 'flex',
        alignSelf: 'flex-end',
        borderRadius: 7,
        backgroundColor: '#6A32B5',
        height: '2rem',
        marginTop: '0.5rem',
        [theme.breakpoints.up('xs')]: {
            minWidth: '25%',
            maxWidth: '25%'
        },
        [theme.breakpoints.up('sm')]: {
            minWidth: '35%',
            maxWidth: '35%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '30%',
            maxWidth: '30%'
        },
        [theme.breakpoints.up('xl')]: {
            minWidth: '35%',
            maxWidth: '35%'
        }
    },
    payButtonText: {
        color: '#FFFFFF',
        fontSize: '0.9rem',
        fontWeight: 'bold',
        [theme.breakpoints.up('lg')]: {
            fontSize: '1rem'
        }
    },
    headerTitle: {
        fontWeight: 'bold',
        color: '#263238',
        fontSize: '0.9rem',
        [theme.breakpoints.up('sm')]: {
            fontSize: '1rem'
        }
    },
    historyContainer: {
        width: '100%',
        height: '100%'
    },
    historyContentContainer: {
        display: 'flex',
        width: '100%',
        height: '100%',
        flexDirection: 'column'
    },
    rowCell: {
        color: '#455A64',
        fontSize: '0.9rem',
        [theme.breakpoints.up('sm')]: {
            fontSize: '1rem'
        }
    },
    priceContainer: {
        display: 'flex',
        alignItems: 'flex-end'
    }
}));

export default SettingsLayout;
