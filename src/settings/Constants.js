export const SUBTITLE_ONE = 'Datos del usuario';
export const FULL_NAME = 'Nombre completo:';
export const DOCUMENT_NUMBER = 'Documento:';
export const EMAIL = 'Correo electrónico:';
export const CHANGE_BUTTON = 'Cambiar';
export const CANCEL_BUTTON = 'Cancelar';
export const PASSWORD = 'Contraseña:';
export const CHANGE_PASSWORD = 'Cambiar contraseña';
export const SERVICE_STATUS = 'Estado del servicio';
export const RAZON_SOCIAL = 'Razón social';
export const PERIODO = 'Periodo';
export const CARGO_BASICO = 'Cargo básico';
export const ADICIONALES = 'Adicionales';
export const TOTAL = 'Total';
export const SUBTITLE_TWO = 'Estado de facturación';
export const PLANILLA_STATUS = {
    1: 'Pagada',
    2: 'Pagar ahora'
};
export const SERVICE_STATUS_LABEL = {
    1: 'Servicio activo',
    2: 'Servicio suspendido'
}