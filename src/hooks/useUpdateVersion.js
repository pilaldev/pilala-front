import { useCallback, useEffect, useState } from 'react';
import { useClearCache } from 'react-clear-cache';

export default () => {
	useClearCache();
	const [isLatestVersion, setIsLatestVersion] = useState(true);
	const [appVersion, setAppVersion] = useState('');

	useEffect(() => {
		if (!localStorage.getItem('LOCAL_VERSION')) {
			localStorage.setItem('LOCAL_VERSION', '');
		}
	});

	useEffect(() => {
		const checkVersion = () => {
			fetch('/meta.json')
				.then((response) => response.json())
				.then((meta) => {
					if (localStorage.getItem('LOCAL_VERSION') !== meta.version) {
						setAppVersion(meta.version);
						setIsLatestVersion(false);
					}
				});
		};
		checkVersion();
	}, []);

	const updateLatestVersion = useCallback(() => {
		localStorage.setItem('LOCAL_VERSION', appVersion);
		setIsLatestVersion(true);
	}, [appVersion]);

	return [isLatestVersion, updateLatestVersion];
};
