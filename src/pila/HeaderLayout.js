import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { HEADER_TITLE } from './Constants';
import StepperLayout from './StepperLayout';
import PopoverButton from '@bit/pilala.pilalalib.components.popover-button';
import moment from 'moment';
import 'moment/locale/es';
import OperatorStatus from './OperatorStatus';

const HeaderLayout = (props) => {
  const classes = useStyles();
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const periodoActual = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const currentPeriod = moment(periodoActual.value, 'DD_MM_YYYY').format(
    'MMMM YYYY'
  );
  const capitalizedPeriod =
    currentPeriod.charAt(0).toUpperCase() + currentPeriod.slice(1);

  return (
    <div className={classes.container}>
      <div className={classes.headerContainer}>
        <Typography className={classes.headerTitle}>{HEADER_TITLE}</Typography>
        <div className={classes.infoContainer}>
          <div className={classes.popoverButtonContainer}>
            <OperatorStatus />
          </div>
          <div className={classes.popoverButtonContainer}>
            <PopoverButton
              title="Periodo de cotización"
              subtitle={capitalizedPeriod}
            />
          </div>
        </div>
      </div>
      <div className={classes.subHeaderContainer}>
        <StepperLayout stepperOptions={props.stepperOptions} />
      </div>
    </div>
  );
};

HeaderLayout.propTypes = {
  stepperOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      icon: PropTypes.element.isRequired,
      action: PropTypes.func.isRequired,
    })
  ).isRequired,
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },
  headerContainer: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerTitle: {
    fontSize: '1.8em',
    fontWeight: 'bold',
    color: '#263238',
    marginTop: '-1.7%',
  },
  infoContainer: {
    display: 'flex',
    height: '100%',
    alignItems: 'center',
  },
  returnButton: {
    display: 'flex',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    paddingRight: 15,
  },
  returnButtonIcon: {
    color: '#6060B2',
    fontSize: '1em',
  },
  returnButtonText: {
    color: '#707070',
    fontSize: '1em',
  },
  cardContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  cardTitle: {
    color: '#6060B2',
    fontSize: '0.95rem',
    textAlign: 'left',
    fontWeight: 'bold',
  },
  cardSubcontainer: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
  },
  cardIcons: {
    color: '#757AFF',
    fontSize: '1.5em',
  },
  cardSubtitle: {
    color: '#263238',
    margin: '0px 5px 0px 5px',
    fontSize: '0.95rem',
    fontWeight: 'bold',
  },
  subHeaderContainer: {
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    padding: '2% 0px 3% 0px',
  },
  errorContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '1.5%',
  },
  errorCard: {
    display: 'flex',
    alignItems: 'center',
    width: '40%',
    justifyContent: 'center',
    borderRadius: 7,
    border: '2px solid black',
  },
  cardErrorIcon: {
    fontSize: '2.5em',
    margin: '10px 10px 10px 10px',
    color: '#FF495A',
  },
  cardErrorText: {
    textAlign: 'left',
    fontSize: '1em',
  },
  correccionContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '1.5%',
  },

  correccionCard: {
    display: 'flex',
    alignItems: 'center',
    paddingRight: 10,
    justifyContent: 'center',
    borderRadius: 7,
    border: '2px solid black',
  },
  correccionIcon: {
    fontSize: '2.5em',
    margin: '10px 10px 10px 10px',
    color: '#757AFF',
  },
  periodButton: {
    display: 'flex',
    alignItems: 'center',
    borderRadius: 7,
    border: '1px solid #757AFF',
    backgroundColor: '#757AFF29',
    padding: '10px',
  },
  downArrow: {
    color: '#757AFF',
    fontSize: '2rem',
  },
  periodsPopper: {
    backgroundColor: '#FFFFFF',
    width: 270,
    height: 'auto',
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    padding: '10px 0px 10px 15px',
    maxHeight: 150,
    borderRadius: 7,
    marginTop: 3,
  },
  periodItemButton: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    margin: '5px 0px',
    justifyContent: 'flex-start',
    borderRadius: 3,
  },
  popoverButtonContainer: {
    marginLeft: theme.spacing(3),
  },
}));

export default HeaderLayout;
