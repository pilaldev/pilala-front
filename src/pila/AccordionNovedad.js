import React from 'react';
import {
	Accordion,
	AccordionDetails,
	AccordionSummary,
	ButtonBase,
	Grid,
	Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import clsx from 'clsx';
import { Calendar } from '../components';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import DateRangeIcon from '@material-ui/icons/DateRange';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import Button from '@bit/pilala.pilalalib.components.button';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';

export default ({
	panel,
	title,
	subtitle,
	onChange,
	openInitialDatePicker,
	openFinalDatePicker,
	removeItem,
	addRow,
	addTitle,
	selectInitialDate,
	selectFinalDate,
	initialAnchor,
	finalAnchor,
	focusDate,
	rows,
	id,
	disableRemoveButton,
	removeIndex,
	disableButtons,
	daysSum,
}) => {
	const classes = useStyles();
	const expanded = id === panel;
	const showInitialDatePicker = Boolean(initialAnchor);
	const showFinalDatePicker = Boolean(finalAnchor);

	return (
		<Accordion
			square={true}
			elevation={0}
			expanded={expanded}
			onChange={onChange}
			classes={{
				root: classes.root,
			}}
		>
			<AccordionSummary
				expandIcon={
					<ExpandMoreIcon
						className={clsx(classes.expandIcon, {
							[classes.expandedIcon]: expanded,
						})}
					/>
				}
				className={clsx(classes.summaryRoot, {
					[classes.summary]: expanded,
				})}
				IconButtonProps={{
					disabled: true,
				}}
			>
				<Grid container direction='row' justify='space-between' alignItems='center'>
					<Typography
						variant='subtitle1'
						className={clsx(classes.title, {
							[classes.expandedTitle]: expanded,
						})}
					>
						{title}
					</Typography>
					<div
						className={clsx(classes.daysContainer, { [classes.daysContainerExpanded]: expanded })}
					>
						<Typography
							variant='subtitle1'
							className={clsx(classes.daysSum, { [classes.expandedColor]: expanded })}
						>
							{daysSum}
						</Typography>
					</div>
				</Grid>
			</AccordionSummary>
			<AccordionDetails
				classes={{
					root: classes.accordionDetailsRoot,
				}}
			>
				<div className={classes.dateContent}>
					<div className={classes.dateTitleContainer}>
						<Typography className={classes.dateTitle} variant='subtitle1'>
							{subtitle}
						</Typography>
					</div>
					<div className={classes.dateContainer}>
						{rows.map((item, index) => (
							<div className={classes.rangeContainer} key={index}>
								<IconButton
									onClick={openInitialDatePicker.bind(this, index)}
									filled={false}
									size='small'
								>
									<DateRangeIcon />
								</IconButton>
								<ButtonBase
									onClick={openInitialDatePicker.bind(this, index)}
									className={classes.dateField}
								>
									<Typography className={classes.dateText} variant='subtitle2'>
										{item.fechaInicio}
									</Typography>
								</ButtonBase>
								<ArrowForwardIcon className={classes.icon} />
								<IconButton
									onClick={openFinalDatePicker.bind(this, index)}
									filled={false}
									size='small'
								>
									<DateRangeIcon />
								</IconButton>
								<ButtonBase
									onClick={openFinalDatePicker.bind(this, index)}
									className={classes.dateField}
								>
									<Typography className={classes.dateText} variant='subtitle2'>
										{item.fechaFin}
									</Typography>
								</ButtonBase>
								<div className={classes.removeIconContainer}>
									{(item.fechaInicio !== '' || item.fechaFin !== '' || index > 0) && (
										<IconButton
											buttoncolor='error'
											filled={false}
											size='small'
											loading={disableRemoveButton && removeIndex === index}
											disabled={disableButtons}
											onClick={removeItem.bind(this, index)}
										>
											<RemoveCircleOutlineIcon />
										</IconButton>
									)}
								</div>
							</div>
						))}
						<div className={classes.addRow}>
							<Button startIcon={<AddCircleOutlineIcon />} variant='text' onClick={addRow}>
								{addTitle}
							</Button>
						</div>
						<Calendar
							open={showInitialDatePicker}
							onChange={selectInitialDate}
							anchorElement={initialAnchor}
							focusDate={focusDate}
							color='primary'
						/>
						<Calendar
							open={showFinalDatePicker}
							onChange={selectFinalDate}
							anchorElement={finalAnchor}
							focusDate={focusDate}
							color='primary'
						/>
					</div>
				</div>
			</AccordionDetails>
		</Accordion>
	);
};

const useStyles = makeStyles((theme) => ({
	root: {
		boxShadow: 'none',
	},
	title: {
		color: theme.palette.common.black,
	},
	expandedTitle: {
		color: theme.palette.primary.main,
	},
	expandIcon: {
		color: theme.palette.common.black,
	},
	expandedIcon: {
		color: theme.palette.primary.main,
	},
	summaryRoot: {
		paddingLeft: 0,
		borderBottom: '1px solid',
		borderBottomColor: theme.palette.grey.main,
	},
	summary: {
		borderBottom: '1px solid',
		borderBottomColor: theme.palette.primary.main,
	},
	accordionDetailsRoot: {
		padding: 0,
	},
	dateContent: {
		backgroundColor: theme.palette.grey[200],
		width: '100%',
		display: 'flex',
		padding: theme.spacing(3, 3, 5, 2),
	},
	dateTitleContainer: {
		flex: 1,
		display: 'flex',
	},
	dateTitle: {
		color: theme.palette.common.black,
	},
	dateContainer: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'flex-start',
		overflow: 'auto',
		maxHeight: 185,
		padding: '0px 1px',
	},
	dateField: {
		border: '1px solid',
		borderColor: theme.palette.primary.main,
		borderRadius: 8,
		width: 90,
		height: 30,
		margin: theme.spacing(0, 1),
		display: 'flex',
		alignItems: 'center',
	},
	dateText: {
		color: theme.palette.common.black,
	},
	removeSpinner: {
		color: theme.palette.primary.main,
	},
	removeIconContainer: {
		width: 30,
		height: 30,
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
	},
	icon: {
		color: theme.palette.primary.main,
		fontSize: '1.5rem',
		margin: theme.spacing(0, 1),
	},
	rangeContainer: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-end',
		marginBottom: 10,
	},
	addRow: {
		flex: 1,
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-end',
		paddingRight: theme.spacing(3.7),
	},
	daysContainer: {
		width: '3.5rem',
		height: '2.3rem',
		backgroundColor: theme.palette.common.white,
		borderRadius: 10,
		marginRight: theme.spacing(1),
		border: '1px solid',
		borderColor: theme.palette.common.black,
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	daysSum: {
		fontWeight: 'bold',
	},
	expandedColor: {
		color: theme.palette.primary.main,
	},
	daysContainerExpanded: {
		borderColor: theme.palette.primary.main,
	},
}));
