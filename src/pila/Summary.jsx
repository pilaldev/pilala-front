import React, { useCallback, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Typography,
  ButtonBase,
  Dialog,
  TextField,
  Checkbox,
  CircularProgress,
  Tooltip,
  Grid,
} from '@material-ui/core';
import CheckBoxTwoToneIcon from '@material-ui/icons/CheckBoxTwoTone';
import CheckBoxOutlineBlankRoundedIcon from '@material-ui/icons/CheckBoxOutlineBlankRounded';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import EmailTwoToneIcon from '@material-ui/icons/EmailTwoTone';
import Button from '@bit/pilala.pilalalib.components.button';
import SettingsEthernetIcon from '@material-ui/icons/SettingsEthernet';
import clsx from 'clsx';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';

import alexPagandoPila from '../assets/img/alex_pagando_pila.png';
import {
  CHECKOUT_TITLE,
  UPLOAD_PILA_FILE_VIDEO,
  HOW_TO_CONNECT_OPERATOR_VIDEO,
  ARUS_SIGN_UP,
} from './Constants';
import HelperFunctions from '../utils/HelperFunctions';
import ConfirmationModalLayout from './ConfirmationModalLayout';
import {
  credentialsModalAction,
  payPilaAction,
  snackBarAction,
} from '../redux/Actions';
import { CREDENTIALS_MODAL, PAY_PILA, SNACKBAR } from '../redux/ActionTypes';
import { useSummaryStyles } from './styles';
import LogoArus from '../assets/img/logo_arus.svg';
import { VideoModal } from '../components';
import { DownloadFile } from '../api';

const Summary = ({
  openSendPayButtonModal,
  cancelSendPayButton,
  getPlanillaPayLink,
  disablePayButton,
  returnToControlPanel,
  continueSendPayButton,
  confirmSendPayButtonModal,
  disableSendPayButton,
}) => {
  const classes = useSummaryStyles();
  const dispatch = useDispatch();
  const planillas = useSelector((state) => state.payPila.planillas);
  const showSendPayButtonModal = useSelector(
    (state) => state.payPila.showSendPayButtonModal
  );
  const showConfirmSendPayButtonModal = useSelector(
    (state) => state.payPila.showConfirmSendPayButtonModal
  );
  const [payButtonIndex, setPayButtonIndex] = useState(null);
  const [payButtonItem, setPayButtonItem] = useState('');
  const [payLinkIndex, setPayLinkIndex] = useState(null);
  const [videoModalData, setVideoModalData] = useState({
    open: false,
    url: undefined,
    title: '',
  });
  const companyNit = useSelector((state) => state.company.identificationNumber);
  const companyIdentificationType = useSelector(
    (state) => state.company.identificationType.code
  );
  const areCredentialsEnabled = useSelector(
    (state) => state.payPila.areCredentialsEnabled
  );
  const lastPilaFilePath = useSelector(
    (state) => state.payPila.lastPilaFilePath
  );
  const [disableDownloadButton, setDisableDownloadButton] = useState(false);

  const getPayButtonEmail = (event) => {
    dispatch(
      payPilaAction(PAY_PILA.PAY_PILA_SAVE_PAY_BUTTON_EMAIL, event.target.value)
    );
  };

  const openPayButtonModal = (item, index) => {
    setPayButtonIndex(index);
    setPayButtonItem(item);
    openSendPayButtonModal();
  };

  const closeConfirmSendPayButtonModal = () => {
    setPayButtonIndex(null);
    setPayButtonItem('');
    cancelSendPayButton();
  };

  const closeSendPayButtonModal = () => {
    setPayButtonIndex(null);
    setPayButtonItem('');
  };

  const handlePayLinkButton = (index, numeroPlanilla) => {
    setPayLinkIndex(index);
    getPlanillaPayLink(numeroPlanilla);
  };

  const downloadPilaFile = useCallback(async () => {
    if (disableDownloadButton) return;
    setDisableDownloadButton(true);

    if (lastPilaFilePath) {
      const downloadResult = await DownloadFile(true, lastPilaFilePath);
      if (downloadResult.status === 'SUCCESS') {
        window.open(downloadResult.data.url);
        setDisableDownloadButton(false);
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al descargar archivo plano',
            description:
              'Hemos tenido problemas para descargar el archivo, inténtalo nuevamente',
            color: 'error',
          })
        );
        setDisableDownloadButton(false);
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Archivo plano no disponible',
          description:
            'Lo sentimos, el archivo solicitado no se encuentra disponible en este momento',
          color: 'error',
        })
      );
      setDisableDownloadButton(false);
    }
  }, [dispatch, lastPilaFilePath, disableDownloadButton]);

  const uploadPilaFile = useCallback(() => {
    setVideoModalData((prevState) => ({
      ...prevState,
      open: true,
      url: UPLOAD_PILA_FILE_VIDEO,
      title: '¿Cómo cargar el archivo PILA?',
    }));
  }, []);

  const showHowItWorks = useCallback(() => {
    setVideoModalData((prevState) => ({
      ...prevState,
      open: true,
      url: HOW_TO_CONNECT_OPERATOR_VIDEO,
      title: '¿Cómo conectar tu operador?',
    }));
  }, []);

  const closeVideoModal = useCallback(() => {
    setVideoModalData({
      open: false,
      url: undefined,
      title: '',
    });
  }, []);

  const createArusAccount = useCallback(() => {
    window.open(ARUS_SIGN_UP);
  }, []);

  const connectOperator = useCallback(() => {
    dispatch(
      credentialsModalAction(CREDENTIALS_MODAL.OPEN_MODAL, 'credentials')
    );
    dispatch(
      credentialsModalAction(CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES, {
        companyIdType: companyIdentificationType,
        companyIdNumber: companyNit,
      })
    );
  }, [dispatch, companyNit, companyIdentificationType]);

  return (
    <div className={classes.container}>
      <div className={classes.contentContainer}>
        <div className={classes.sumContainer}>
          {areCredentialsEnabled ? (
            <>
              <Typography className={classes.headerTitle} variant="h6">
                {CHECKOUT_TITLE}
              </Typography>
              <div className={classes.planillasContainer}>
                {planillas.map((item, index) => (
                  <div
                    className={classes.planillaItem}
                    style={{
                      backgroundColor: index % 2 === 0 ? '#ECEFF1' : '#FFFFFF',
                      borderRadius: index % 2 === 0 ? 7 : 0,
                    }}
                    key={index.toString()}
                  >
                    <Typography className={classes.planillaAportante}>
                      {item.tipo}
                    </Typography>
                    <Typography className={classes.planillaNumber}>
                      {item.numeroPlanilla}
                    </Typography>
                    <Typography className={classes.planillaPeriod}>
                      {item.periodo}
                    </Typography>
                    <Typography className={classes.planillaValue}>
                      {`$${HelperFunctions.formatStringNumber(item.total)}`}
                    </Typography>
                    <ButtonBase
                      disableRipple
                      onClick={openPayButtonModal.bind(this, item, index)}
                    >
                      <Tooltip
                        arrow
                        placement="top"
                        classes={{
                          tooltip: classes.linkTooltip,
                          arrow: classes.linkTooltipArrow,
                        }}
                        title={<>Enviar link de pago</>}
                      >
                        <EmailTwoToneIcon
                          className={classes.sendPayButtonText}
                        />
                      </Tooltip>
                    </ButtonBase>
                    <ButtonBase
                      className={classes.payButton}
                      onClick={handlePayLinkButton.bind(
                        this,
                        index,
                        item.numeroPlanilla
                      )}
                      disabled={disablePayButton}
                    >
                      {disablePayButton && payLinkIndex === index ? (
                        <CircularProgress
                          size={18}
                          className={classes.payButtonSpinner}
                        />
                      ) : (
                        <Typography className={classes.payButtonText}>
                          Pagar
                        </Typography>
                      )}
                    </ButtonBase>
                  </div>
                ))}
              </div>
            </>
          ) : (
            <div>
              <Typography className={classes.headerTitle} variant="h6">
                Tu Archivo PILA se generó correctamente
              </Typography>
              <div className={classes.actionsContainer}>
                <ActionCard
                  title="Descargar tu archivo:"
                  message="Este archivo podrás cargarlo directamente en cualquier operador de forma segura."
                  type="one"
                  secondAction={uploadPilaFile}
                  mainAction={downloadPilaFile}
                  mainActionText="Descargar archivo"
                  secondActionText="¿Cómo cargar este archivo?"
                  disableDownloadButton={disableDownloadButton}
                />
                <ActionCard
                  title="Información importante:"
                  message="¡Recuerda que también puedes conectar tu operador para automatizar el proceso, enviar botones de pago o pagar directamente desde Pilalá."
                  type="two"
                  secondAction={showHowItWorks}
                  secondActionText="¿Cómo conectar tu operador?"
                />
              </div>
              <div>
                <Typography className={classes.headerTitle} variant="h6">
                  ¿Qué operador puedes conectar a Pilalá?
                </Typography>
                <div className={classes.operatorActions}>
                  <div className={classes.actionContainer}>
                    <LogoBox name="Logo de ARUS" logo={LogoArus} />
                  </div>
                  <div className={classes.actionContainer}>
                    <Button
                      startIcon={<SettingsEthernetIcon />}
                      onClick={connectOperator}
                    >
                      Conectar con ARUS
                    </Button>
                  </div>
                  <Button variant="outlined" onClick={createArusAccount}>
                    Crea tu cuenta ARUS
                  </Button>
                </div>
              </div>
            </div>
          )}
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className={classes.backButtonContainer}
          >
            <Button onClick={returnToControlPanel} variant="outlined">
              Volver al panel de control
            </Button>
          </Grid>
        </div>
        <div className={classes.checkoutImageContainer}>
          <img
            src={alexPagandoPila}
            alt="Imagen checkout"
            className={classes.checkoutImage}
          />
        </div>
      </div>
      <Dialog
        open={showSendPayButtonModal}
        maxWidth="sm"
        PaperProps={{
          className: classes.paperContainer,
        }}
        onClose={closeSendPayButtonModal}
      >
        <div style={{ flex: 1 }}>
          <Typography className={classes.sendButtonTitle}>
            ¿A quién le vas a enviar el botón?
          </Typography>
          <TextField
            variant="outlined"
            className={classes.sendButtonInput}
            inputProps={{
              style: {
                padding: '0.4em 0.8em',
              },
            }}
            classes={{
              root: classes.inputRoot,
            }}
            onChange={getPayButtonEmail}
          />
          <div className={classes.sendButtonPlanillaItem}>
            <Typography className={classes.planillaAportante}>
              {payButtonItem.tipo}
            </Typography>
            <Typography className={classes.planillaNumber}>
              {payButtonItem.numeroPlanilla}
            </Typography>
            <Typography className={classes.planillaPeriod}>
              {payButtonItem.periodo}
            </Typography>
            <Typography className={classes.planillaValue}>
              {`$${HelperFunctions.formatStringNumber(payButtonItem.total)}`}
            </Typography>
          </div>
          <div className={classes.messageContainer}>
            <Checkbox
              checked
              disabled
              checkedIcon={
                <CheckBoxTwoToneIcon className={classes.messageCheckedIcon} />
              }
              icon={
                <CheckBoxOutlineBlankRoundedIcon
                  className={classes.messageCheckbox}
                />
              }
            />
            <Typography className={classes.messageText}>
              Esta persona cuenta con autorización para recibir esta información
              y realizar el pago.
            </Typography>
          </div>
          <div className={classes.buttonsContainer}>
            <div className={classes.cancelButtonContainer}>
              <Button onClick={cancelSendPayButton}>Cancelar</Button>
            </div>
            <Button
              onClick={continueSendPayButton}
              endIcon={<ArrowForwardRoundedIcon />}
            >
              Continuar
            </Button>
          </div>
        </div>
      </Dialog>
      <ConfirmationModalLayout
        id="sendPayButton"
        showModal={showConfirmSendPayButtonModal}
        message="¡Enviaremos un correo electrónico a esta persona con un resumen de la planilla y el botón de pago. Los valores pueden ser ajustados por validaciones del operador de información!"
        cancelAction={closeConfirmSendPayButtonModal}
        confirmAction={confirmSendPayButtonModal.bind(this, payButtonIndex)}
        alertColor="#2962FF"
        textColor="#FFFFFF"
        disableConfirmButton={disableSendPayButton}
      />
      <VideoModal
        open={videoModalData.open}
        url={videoModalData.url}
        onClose={closeVideoModal}
        title={videoModalData.title}
      />
    </div>
  );
};

const LogoBox = React.memo(({ logo, name }) => {
  const classes = useSummaryStyles();

  return (
    <div className={classes.logoBox}>
      <img src={logo} alt={name} width="100%" height="auto" />
    </div>
  );
});

const ActionCard = React.memo(
  ({
    title,
    message,
    type,
    secondAction,
    mainAction,
    secondActionText,
    mainActionText,
    disableDownloadButton,
  }) => {
    const classes = useSummaryStyles();

    return (
      <div
        className={clsx(classes.actionCard, {
          [classes.actionCardColorOne]: type === 'one',
          [classes.actionCardColorTwo]: type === 'two',
        })}
      >
        <Typography className={classes.actionCardTitle} variant="subtitle2">
          {title}
        </Typography>
        <Typography className={classes.actionCardMessage} variant="body2">
          {message}
        </Typography>
        {secondAction && (
          <div className={classes.actionCardLinkContainer}>
            <Button
              startIcon={<PlayCircleOutlineIcon />}
              variant="text"
              onClick={secondAction}
            >
              {secondActionText}
            </Button>
          </div>
        )}
        {mainAction && (
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className={classes.actionCardFooter}
          >
            <Button
              startIcon={<CloudDownloadIcon />}
              onClick={mainAction}
              loading={disableDownloadButton}
            >
              {mainActionText}
            </Button>
          </Grid>
        )}
      </div>
    );
  }
);

export default Summary;
