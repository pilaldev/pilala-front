import Endpoints from '@bit/pilala.pilalalib.endpoints';

export const novedadVariacionSalario = async (data) => {
	return new Promise((resolve, reject) => {
		fetch(Endpoints.novedadVariacionSalario, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto la CF de variacion de salario',
					error,
				});
			});
	});
};

export const novedadIncapacidades = async (data) => {
	return new Promise((resolve, reject) => {
		fetch(Endpoints.novedadIncapacidades, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto la CF de variacion de incapacidades',
					error,
				});
			});
	});
};

export const novedadLicencias = async (data) => {
	return new Promise((resolve, reject) => {
		fetch(Endpoints.novedadLicencias, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto la CF de variacion de licencias',
					error,
				});
			});
	});
};

export const deleteNovedades = async (data) => {
	return new Promise((resolve, reject) => {
		fetch(Endpoints.deleteNovedades, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto la CF de eliminar novedades',
					error,
				});
			});
	});
};
