import Endpoints from '@bit/pilala.pilalalib.endpoints';

const checkPlanillaData = async (
	idCompany,
	idPeriodo,
	idPlanilla,
	companyCity,
	companyProvince
) => {
	let result = {};

	const requestData = {
		companyInfo: {
			location: {
				city: {
					value: companyCity,
				},
				province: {
					value: companyProvince,
				},
			},
			idCompany,
		},
		periodosInfo: {
			value: idPeriodo,
			month: idPlanilla,
		},
	};

	const requestBody = JSON.stringify(requestData);

	const requestOptions = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: requestBody,
	};

	await fetch(Endpoints.checkPlanillaData, requestOptions)
		.then((response) => response.json())
		.then((resultApi) => {
			result.status = resultApi.status;
			result.data = resultApi.data;
		})
		.catch((error) => {
			console.log('Error en fetch de chequear informacion de planilla', error);
			result.status = 'FAILED';
			result.data = null;
		});

	return result;
};

export default checkPlanillaData;
