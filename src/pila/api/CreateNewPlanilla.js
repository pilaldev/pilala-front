import Endpoints from '@bit/pilala.pilalalib.endpoints';

const createNewPlanilla = async (
  adminPlatform,
  sucursalId,
  planillas,
  tipoPlanilla,
  periodo,
  total,
  pathFile,
  credentials
) => {
  return new Promise((resolve) => {
    const requestData = {
      adminPlatform,
      sucursalId,
      planillas,
      tipoPlanilla,
      periodo,
      total,
      pathFile,
      credentials,
    };

    fetch(Endpoints.sendValidation, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    })
      .then((response) => response.json())
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        console.log(
          'Error en la solicitud de creación de nueva planilla',
          error
        );
      });
  });
};

export default createNewPlanilla;
