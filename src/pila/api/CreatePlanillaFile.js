import Endpoints from '@bit/pilala.pilalalib.endpoints';

const createPlanillaFile = async (data) => {
  let result = {};
  const requestBody = JSON.stringify(data);

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: requestBody,
  };

  await fetch(Endpoints.createPILAFile, requestOptions)
    .then((response) => response.json())
    .then((resultApi) => {
      result = resultApi;
    })
    .catch((error) => {
      console.log('Error en fetch de crear archivo PILA', error);
      result = error;
    });

  return result;
};

export default createPlanillaFile;
