import Endpoints from '@bit/pilala.pilalalib.endpoints';

const sendPayButton = async (
	userName,
	numeroPlanilla,
	companyName,
	cotizantesNumber,
	paymentValue,
	planillaType,
	idType,
	companyNit,
	email
) => {
	let result = {};

	const requestData = {
		userName,
		numeroPlanilla,
		companyName,
		cotizantesNumber,
		paymentValue,
		planillaType,
		idType,
		companyNit,
		email,
	};

	const requestBody = JSON.stringify(requestData);

	const requestOptions = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: requestBody,
	};

	await fetch(Endpoints.sendPayButton, requestOptions)
		.then((response) => response.json())
		.then((resultFetch) => {
			result.status = resultFetch.status;
			result.statusCode = resultFetch.statusCode;
		})
		.catch((error) => {
			console.log('Error en fetch de enviar botón de pago', error);
			result.status = 'FETCH_FAILED';
			result.statusCode = 0;
		});

	return result;
};

export default sendPayButton;
