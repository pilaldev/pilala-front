const validatePlanilla = async (file, idPlanilla) => {
    let result = {};
    const requestData = new FormData();
    requestData.append('archivo', file, `${idPlanilla}.txt`);
    requestData.append('parametros', '{\'planillaUGPP\':false,\'planillaNSoloNovedades\':false,\'tipoArchivo\':\'I\'}');

    const requestOptions = {
        method: 'POST',
        body: requestData
    };

    await fetch('/api/generadorPlanillas/v1/planillas/validacion', requestOptions)
        .then((response) => response)
        .then(async (resultApi) => {
            let resultBody = null;
            await resultApi.json().then((data) => {
                resultBody = data;
            });
            switch (resultApi.status) {
                case 200:
                    console.log(`Validación de planilla exitosa (${resultApi.status})`, resultBody)
                    result.status = 'SUCCESS';
                    result.data = resultBody;
                    break;
                case 201:
                    console.log(`Solicitud de validar planilla completada y nuevo recurso creado (${resultApi.status})`, resultBody);
                    result.status = 'FAILED';
                    result.data = null;
                    break;

                case 400:
                    console.log(`Solicitud de validar planilla con sintaxis errónea (${resultApi.status})`, resultBody)
                    result.status = 'FAILED';
                    result.data = null;
                    break;

                case 401:
                    console.log(`Solicitud de validar planilla no autorizada (${resultApi.status})`, resultBody)
                    result.status = 'FAILED';
                    result.data = null;
                    break;

                case 403:
                    console.log(`Solicitud de validar planilla prohibida (${resultApi.status})`, resultBody)
                    result.status = 'FAILED';
                    result.data = null;
                    break;

                case 404:
                    console.log(`Recurso de validar planilla no encontrado (${resultApi.status})`, resultBody)
                    result.status = 'FAILED';
                    result.data = null;
                    break;

                case 422:
                    console.log(`Solicitud de validar planilla con errores semánticos (${resultApi.status})`, resultBody)
                    result.status = 'FAILED';
                    result.data = null;
                    break;

                case 500:
                    console.log(`Solicitud de validar planilla fallida, error ajeno al servidor (${resultApi.status})`, resultBody)
                    result.status = 'FAILED';
                    result.data = null;
                    break;

                default:
                    console.log(`Error de validación de planilla no identificado (${resultApi.status})`, resultBody)
                    result.status = 'FAILED';
                    result.data = null;
                    break;
            }
        })
        .catch((error) => {
            console.log('Error en fetch de validar planilla', error);
            result.status = 'FAILED';
            result.data = null;
        });

    return result;
}

export default validatePlanilla;