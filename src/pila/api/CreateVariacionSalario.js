import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default async (data) => {
	let result = {};
	const requestBody = JSON.stringify(data);

	const requestOptions = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: requestBody,
	};

	await fetch(Endpoints.novedadVariacionSalario, requestOptions)
		.then(async (response) => {
			if (response.status === 200) {
				result.status = 'SUCCESS';
				result.data = await response.json();
			} else {
				result.status = 'FAILED';
				result.data = await response.json();
			}
		})
		.catch((error) => {
			console.log('Error fetch de creación de variación de salario', error);
		});
	return result;
};
