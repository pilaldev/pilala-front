import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default async (idType, idNumber, numeroPlanilla) => {
	let result = {};

	const data = JSON.stringify({
		idType,
		idNumber,
		numeroPlanilla,
	});

	const requestOptions = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: data,
	};
	await fetch(Endpoints.getPaymentURL, requestOptions)
		.then((response) => response.json())
		.then((resultApi) => {
			result = resultApi;
		})
		.catch((error) => {
			console.log('Error en fetch de link de pago', error);
			result = error;
		});

	return result;
};
