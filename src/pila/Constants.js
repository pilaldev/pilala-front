/* Footer bar */
export const STEP_TEXT = 'Paso';
export const STEPS = 2;
export const SAVE_BUTTON = 'CREAR PLANILLA';
export const STEP_ONE_TEXT =
  'Haz click en crear planilla para ir al siguiente paso.';
export const RETURN_BUTTON = 'Volver';
export const HOURS = {
  FROM: '4:30 PM',
  TO: '6:30 PM',
};
export const STEP_TWO_TEXT =
  'No se permite el pago de aportes debido a que en este horario ya los operadores se encuentran en proceso de compensación.';

/* Validate PILA */
export const VALIDATE_STEPS = {
  CHECK_DATA: 'Verificando información',
  CREATE_PILA: 'Creando archivo PILA',
  VALIDATE_PILA: 'Validando información con el operador',
};

/* Stepper */
export const STEPPER_VALUES = [
  {
    step: 'Paso 1',
    description: 'Crear y validar planilla',
  },
  {
    step: 'Paso 2',
    description: 'Pagar planilla',
  },
];
export const PESOS_SYMBOL = '$';
export const DOWNLOAD_OPTION = 'Descargar resumen';
export const TOTAL_PAY = 'Total a pagar planillas:';

/* Header */
export const HEADER_TITLE = 'Liquidar PILA';
export const HEADER_RETURN_BUTTON = 'Volver al tablero';
export const CARD_TITLE = 'Periodo de cotización:';
export const VALIDATION_ERROR =
  'TU PLANILLA NO HA PODIDO SER VALIDADA. PARECE QUE TIENES INFORMACIÓN POR CORREGIR.';
export const CORRECION_MESSAGE = ' Estás corrigiendo la Planilla Número';
export const ADD_COTIZANTE_MESSAGGE = 'Agregar nuevo cotizante';
export const DOWNLOAD_OPTIONS = {
  DOWNLOAD_EXCEL: 'Descargar en Excel',
  DOWNLOAD_PDF: 'Descargar en PDF',
};
export const RECEIPT_OPTIONS = {
  1: 'Hacer una corrección',
  2: 'Descargar planilla en Excel',
  3: 'Descargar planilla en PDF',
  4: 'Descargar comprobante agrupado',
  5: 'Descargar comprobante detallado',
  6: 'Enviar comprobante a los cotizantes',
};
export const CHECKOUT_TITLE = 'Resumen de totales a pagar por tus planillas:';
export const PAYMENT_SUCCESS_TITLE =
  'Felicidades, tus planillas han sido pagadas con éxito!';
export const PAY_BUTTON = 'Enviar botón de pago';
export const FINAL_AMOUNT = 'Total a pagar:';
export const RECEIPT_TITLE = 'Planilla de empleados';

export const DIVIDER_SYMBOL = '|';
export const SALARY = 'Salario: $';
export const CANCEL_BUTTON = 'Cancelar';
export const NEW_NOVEDAD = 'Guardar';

/* Retiro modal */
export const RETIRO_MODAL_TITLE = 'Retiro del cotizante';
export const RETIRO_NOVEDAD_NAME = 'Retiro (RET)';
export const RETIRO_DATE_TEXT = 'Fecha de retiro';
export const CREATE_RETIREMENT_BUTTON = 'Guardar';

/* Ingreso modal */
export const INGRESO_MODAL_TITLE = 'Ingreso del cotizante';
export const INGRESO_NOVEDAD_NAME = 'Ingreso (ING)';
export const INGRESO_DATE_TEXT = 'Fecha de ingreso';
export const CREATE_INGRESO_BUTTON = 'Guardar';

/* Variación permanente modal */

export const VARIACION_PERMANENTE_MODAL = {
  TITLE: 'Cambio de salario',
  SUBTITLE: 'Variación permanente de salario (VSP)',
  NEW_SALARY: 'Nuevo salario',
  NEW_SALARY_DATE: 'Inicio del nuevo salario',
  CHANGE_SALARY_BUTTON: 'Cambiar salario',
};

/* Incapacidades Modal */

export const INCAPACIDADES_MODAL = {
  TITLE: 'Reporte de Incapacidades',
  NOVEDAD_TITLE: 'Dias de Incapacidad',
  IGE_TITLE: 'Incapacidad por enfermedad general (IGE)',
  IRL_TITLE: 'Incapacidad por accidente de trabajo (IRL)',
  CANCEL_BUTTON: 'Cancelar',
  SAVE_BUTTON: 'Guardar',
  ADD_BUTTON: 'Incapacidad',
};

/* Vacaciones modal */
export const VACACIONES_MODAL_TITLE = 'Reporte de vacaciones';
export const VACACIONES_NOVEDAD_NAME = 'Vacaciones (VAC)';
export const VACACIONES_DAYS = 'Días de vacaciones';
export const VACACIONES_ADD_ROW = 'Vacaciones';

/* Suspensiones modal */
export const SUSPENSIONES_MODAL_TITLE = 'Reporte de suspensiones';
export const SUSPENSIONES_NOVEDAD_NAME = 'Suspensiones (SLN)';
export const SUSPENSIONES_DAYS = 'Días de suspensión';
export const SUSPENSIONES_ADD_ROW = 'Suspensión';

/* Descuentos Modal */

export const DESCUENTOS_MODAL = {
  TITLE: 'Reporte de descuentos',
  TOTAL_APORTES_TITLE: 'Total aportes ARL: $',
  TOTAL_DESCUENTOS_TITLE: 'Total descontado ARL: $',
  EXPANSION_PANEL_TITLE: 'Descuentos por incapacidad laboral',
  AUTHORIZATION_NUMBER_TEXT: 'Número de autorización ODI:',
  ARL_DISCOUNT_TEXT: 'Valor descontable ARL:',
  OTHER_RIKS_TEXT: 'Valor descontable Otros Riesgos:',
  CREDIT_NOTE_TEXT: 'Valor descontable nota crédito:',
  REPEAT_UNTIL_TEXT: 'Repetir hasta:',
  CANCEL_BUTTON: 'Cancelar',
  SAVE_BUTTON: 'Guardar',
  ADD_BUTTON: 'Descuento',
};

/* Licencias Modal */

export const LICENCIAS_MODAL = {
  TITLE: 'Reporte de Licencias',
  NOVEDAD_TITLE: 'Dias de licencia',
  LR_TITLE: 'Licencia remunerada (LR)',
  LNR_TITLE: 'Licencia no remunerada (SLN)',
  LMA_TITLE: 'Licencia de maternidad (LMA)',
  CANCEL_BUTTON: 'Cancelar',
  SAVE_BUTTON: 'Guardar',
  ADD_BUTTON: 'Licencia',
};

/* Summary Component */
export const UPLOAD_PILA_FILE_VIDEO =
  'https://www.youtube.com/embed/sxOHL_JECMU';
export const HOW_TO_CONNECT_OPERATOR_VIDEO =
  'https://www.youtube.com/embed/sxOHL_JECMU';
export const ARUS_SIGN_UP = 'https://www.suaporte.com.co/sso/#/new-account';
