import React from 'react';
import PopoverButton from '@bit/pilala.pilalalib.components.popover-button';
import SettingsEthernetIcon from '@material-ui/icons/SettingsEthernet';
import { useDispatch, useSelector } from 'react-redux';
import { credentialsModalAction } from '../redux/Actions';
import { CREDENTIALS_MODAL } from '../redux/ActionTypes';

const OperatorStatus = React.memo(() => {
  const dispatch = useDispatch();
  const accountManager = useSelector(
    (state) => state.currentUser.accountManager
  );
  const companyNit = useSelector((state) => state.company.identificationNumber);
  const companyIdentificationType = useSelector(
    (state) => state.company.identificationType.code
  );
  const isOperatorConnected = accountManager.length > 0;
  const options = [
    {
      id: 1,
      label: 'Conectar operador',
      action: () => {
        dispatch(
          credentialsModalAction(CREDENTIALS_MODAL.OPEN_MODAL, 'credentials')
        );
        dispatch(
          credentialsModalAction(CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES, {
            companyIdType: companyIdentificationType,
            companyIdNumber: companyNit,
          })
        );
      },
      icon: <SettingsEthernetIcon />,
    },
  ];

  return (
    <PopoverButton
      title="Estado del operador"
      subtitle={isOperatorConnected ? 'Conectado' : 'No conectado'}
      color={isOperatorConnected ? 'success' : 'grey'}
      menu={!isOperatorConnected}
      options={!isOperatorConnected ? options : undefined}
    />
  );
});

export default OperatorStatus;
