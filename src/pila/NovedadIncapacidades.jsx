import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import 'moment/locale/es';
import { novedadIncapacidadesAction, snackBarAction } from '../redux/Actions';
import { INCAPACIDADES, SNACKBAR } from '../redux/ActionTypes';
import ValidateDates from '../utils/ValidateDates';
import { AccordionNovedad, ModalNovedad } from '.';
import { INCAPACIDADES_MODAL } from './Constants';
import { CreateIncapacidad, RemoveNovedad } from './api';
import { useNovedadIncapacidadesStyles } from './styles';

moment.locale('es');

export default () => {
  const classes = useNovedadIncapacidadesStyles();
  const dispatch = useDispatch();
  const [initialDateAnchor, setInitialDateAnchor] = useState(null);
  const [finalDateAnchor, setFinalDateAnchor] = useState(null);
  const openIncapacidadesModal = useSelector(
    (state) => state.novedadIncapacidades.openIncapacidadesModal
  );
  const cotizanteName = useSelector(
    (state) => state.novedadIncapacidades.cotizanteName
  );
  const incapacidadesIGE = useSelector(
    (state) => state.novedadIncapacidades.IGE
  );
  const incapacidadesIRL = useSelector(
    (state) => state.novedadIncapacidades.IRL
  );
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [removeIndex, setRemoveIndex] = useState(-1);
  const [disableSaveButton, setDisableSaveButton] = useState(false);
  const [disableButtons, setDisableButtons] = useState(false);
  const [disableRemoveButton, setDisableRemoveButton] = useState(false);
  const [openedAccordion, setOpenAccordion] = useState(null);
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const periodoActual = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const prePlanilla = useSelector((state) => state.prePlanilla);
  const periodosInfo = useSelector((state) => state.currentUser.periodosInfo);
  const tipoCotizante = useSelector(
    (state) => state.novedadIncapacidades.tipoCotizante
  );
  const cotizanteDocumentNumber = useSelector(
    (state) => state.novedadIncapacidades.cotizanteDocumentNumber
  );
  const activeChanges = useSelector(
    (state) => state.novedadIncapacidades.activeChanges
  );
  const idCotizante = useSelector(
    (state) => state.novedadIncapacidades.idCotizante
  );
  const cotizantesInfo = useSelector((state) => state.cotizantesInfo);
  const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);
  const totalDaysIGE = useSelector(
    (state) => state.novedadIncapacidades.totalDaysIGE
  );
  const totalDaysIRL = useSelector(
    (state) => state.novedadIncapacidades.totalDaysIRL
  );

  // TODO: Verificar nombre de compañia cuando se activen sucursales

  const openInitialDatePicker = (index, event) => {
    setSelectedIndex(index);
    setInitialDateAnchor(event.currentTarget);
  };

  const openFinalDatePicker = (index, event) => {
    setSelectedIndex(index);
    setFinalDateAnchor(event.currentTarget);
  };

  const removeItem = async (index) => {
    if (disableRemoveButton) return;
    setDisableRemoveButton(true);
    setDisableButtons(true);
    setRemoveIndex(index);

    const incapacidadesData =
      openedAccordion === 'IGE' ? [...incapacidadesIGE] : [...incapacidadesIRL];

    if (incapacidadesData[index].id) {
      const incapacidadDeleteData = {
        idAportante: activeCompany,
        cotizanteData: {
          idCotizante,
          documentNumber: cotizanteDocumentNumber,
          companyName: companiesInfo[activeCompany].companyName,
          idNew: incapacidadesData[index].id,
          province: companiesInfo[activeCompany].location.province.value,
          city: companiesInfo[activeCompany].location.city.value,
          tipoCotizante: tipoCotizante.value,
          type: incapacidadesData[index].type,
        },
      };
      const deleteResult = await RemoveNovedad(incapacidadDeleteData);
      if (deleteResult.status === 'SUCCESS') {
        dispatch(
          novedadIncapacidadesAction(
            INCAPACIDADES.INCAPACIDADES_REMOVE_NOVEDAD,
            {
              incapacidadType: openedAccordion,
              index,
            }
          )
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
        setRemoveIndex(-1);
      } else if (deleteResult.status === 'FAILED') {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al eliminar novedad',
            description:
              'Hemos tenido problemas para eliminar la novedad de incapacidad, inténtelo nuevamente',
            color: 'error',
          })
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
        setRemoveIndex(-1);
      }
    } else {
      dispatch(
        novedadIncapacidadesAction(INCAPACIDADES.INCAPACIDADES_REMOVE_NOVEDAD, {
          incapacidadType: openedAccordion,
          index,
        })
      );
      setDisableRemoveButton(false);
      setDisableButtons(false);
      setRemoveIndex(-1);
    }
  };

  const resetFields = () => {
    if (openedAccordion) {
      setOpenAccordion(null);
    }
  };

  const onClose = () => {
    if (openIncapacidadesModal) {
      dispatch(
        novedadIncapacidadesAction(
          INCAPACIDADES.INCAPACIDADES_OPEN_INCAPACIDADES_MODAL,
          null
        )
      );
    }
    dispatch(
      novedadIncapacidadesAction(INCAPACIDADES.INCAPACIDADES_RESET_DATA, null)
    );
    resetFields();
  };

  const checkDates = (incapacidades) => {
    return (
      incapacidades.length > 0 &&
      incapacidades.every(
        (item) => item.fechaInicio !== '' && item.fechaFin !== ''
      )
    );
  };

  const onSave = async () => {
    if (disableSaveButton) return;
    setDisableSaveButton(true);
    setDisableButtons(true);

    const incapacidades = [...incapacidadesIGE, ...incapacidadesIRL].filter(
      (el) => el.fechaInicio && el.fechaFin
    );

    if (checkDates(incapacidades)) {
      if (activeChanges) {
        const changedIncapacidades = [...incapacidadesIGE, ...incapacidadesIRL]
          .filter((el) => el.fechaInicio && el.fechaFin)
          .map((item) => ({
            ...item,
            dateStart: item.fechaInicio,
            dateEnd: item.fechaFin,
          }));
        const incapacidadesFullData = {
          tipoCotizante: tipoCotizante.value,
          location:
            cotizantesInfo[activeCompany][cotizanteDocumentNumber].location,
          idCompany: activeCompany,
          cotizanteData: {
            idCotizante,
            arrayNew: changedIncapacidades,
          },
        };
        const method = changedIncapacidades.some(
          (item) => item.id !== undefined
        )
          ? 'PATCH'
          : 'POST';
        const result = await CreateIncapacidad(incapacidadesFullData, method);
        if (result.status === 'SUCCESS') {
          dispatch(
            novedadIncapacidadesAction(
              INCAPACIDADES.INCAPACIDADES_OPEN_INCAPACIDADES_MODAL,
              null
            )
          );
          dispatch(
            novedadIncapacidadesAction(
              INCAPACIDADES.INCAPACIDADES_RESET_DATA,
              null
            )
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        } else if (result.status === 'FAILED') {
          dispatch(
            snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear novedad',
              description:
                'Hemos tenido problemas para crear la novedad de incapacidad, inténtelo nuevamente',
              color: 'error',
            })
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'No se detectaron cambios',
            description:
              'La información sobre las incapacidades no ha cambiado',
            color: 'info',
          })
        );
        setDisableSaveButton(false);
        setDisableButtons(false);
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description:
            'Por favor complete las fechas del reporte de incapacidades',
          color: 'warning',
        })
      );
      setDisableSaveButton(false);
      setDisableButtons(false);
    }
  };

  const addRow = () => {
    const current =
      openedAccordion === 'IGE' ? incapacidadesIGE : incapacidadesIRL;
    if (checkDates(current)) {
      dispatch(
        novedadIncapacidadesAction(INCAPACIDADES.INCAPACIDADES_ADD_NOVEDAD, {
          incapacidadType: openedAccordion,
        })
      );
    }
  };

  const getInitialDate = (index, data, panel) => {
    let fechaFin = '';
    let durationDays = '';
    let id = null;
    let currentNovedades = [];

    if (panel === 'IGE') {
      fechaFin = incapacidadesIGE[index].fechaFin;
      id = incapacidadesIGE[index].id;
      currentNovedades = incapacidadesIGE.concat(incapacidadesIRL);
    } else {
      fechaFin = incapacidadesIRL[index].fechaFin;
      id = incapacidadesIRL[index].id;
      currentNovedades = incapacidadesIRL.concat(incapacidadesIGE);
    }

    if (fechaFin) {
      durationDays =
        moment(fechaFin, 'DD/MM/YYYY')
          .startOf('day')
          .diff(moment(data, 'DD/MM/YYYY').startOf('day'), 'days') + 1;
    }

    if (
      fechaFin === '' ||
      moment(fechaFin, 'DD/MM/YYYY')
        .startOf('day')
        .diff(moment(data, 'DD/MM/YYYY').startOf('day'), 'days') > 0
    ) {
      const cotizanteData =
        prePlanilla[activeCompany][periodosInfo[activeCompany].value][
          tipoCotizante.value
        ][periodosInfo[activeCompany].month][cotizanteDocumentNumber];

      const dateValidation = ValidateDates(
        cotizanteData,
        data,
        false,
        false,
        index,
        id,
        true,
        false,
        currentNovedades
      );

      if (dateValidation.result) {
        const value = {
          incapacidadType: panel,
          totalDaysIGE: panel === 'IGE' ? durationDays || 0 : 0,
          totalDaysIRL: panel === 'IRL' ? durationDays || 0 : 0,
          novedad: {
            fechaInicio: moment(data).format('DD/MM/YYYY'),
            value: {
              fechaInicio: moment(data).format('DD/MM/YYYY'),
            },
            duracionDias: durationDays || '',
            regLine: {},
          },
          index,
        };
        dispatch(
          novedadIncapacidadesAction(
            INCAPACIDADES.INCAPACIDADES_UPDATE_NOVEDAD,
            value
          )
        );
        if (!activeChanges) {
          dispatch(
            novedadIncapacidadesAction(
              INCAPACIDADES.INCAPACIDADES_ENABLE_ACTIVE_CHANGES,
              null
            )
          );
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al validar fechas',
            description: dateValidation.message,
            color: 'error',
          })
        );
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Fecha incorrecta',
          description: 'La fecha inicial debe ser menor que la fecha final',
          color: 'info',
        })
      );
    }
  };

  const getFinalDate = (index, data, panel) => {
    let fechaInicio = '';
    let durationDays = '';
    let id = null;
    let currentNovedades = [];

    if (panel === 'IGE') {
      fechaInicio = incapacidadesIGE[index].fechaInicio;
      id = incapacidadesIGE[index].id;
      currentNovedades = incapacidadesIGE.concat(incapacidadesIRL);
    } else {
      fechaInicio = incapacidadesIRL[index].fechaInicio;
      id = incapacidadesIRL[index].id;
      currentNovedades = incapacidadesIRL.concat(incapacidadesIGE);
    }

    if (fechaInicio) {
      durationDays =
        moment(data, 'DD/MM/YYYY')
          .startOf('day')
          .diff(moment(fechaInicio, 'DD/MM/YYYY').startOf('day'), 'days') + 1;
    }

    if (
      fechaInicio === '' ||
      moment(data, 'DD/MM/YYYY')
        .startOf('day')
        .diff(moment(fechaInicio, 'DD/MM/YYYY').startOf('day'), 'days') >= 0
    ) {
      const cotizanteData =
        prePlanilla[activeCompany][periodosInfo[activeCompany].value][
          tipoCotizante.value
        ][periodosInfo[activeCompany].month][cotizanteDocumentNumber];

      const dateValidation = ValidateDates(
        cotizanteData,
        data,
        false,
        false,
        index,
        id,
        false,
        true,
        currentNovedades
      );

      if (dateValidation.result) {
        const value = {
          incapacidadType: panel,
          totalDaysIGE: panel === 'IGE' ? durationDays || 0 : 0,
          totalDaysIRL: panel === 'IRL' ? durationDays || 0 : 0,
          novedad: {
            fechaFin: moment(data).format('DD/MM/YYYY'),
            value: {
              fechaFin: moment(data).format('DD/MM/YYYY'),
            },
            duracionDias: durationDays || '',
            regLine: {},
          },
          index,
        };
        dispatch(
          novedadIncapacidadesAction(
            INCAPACIDADES.INCAPACIDADES_UPDATE_NOVEDAD,
            value
          )
        );
        if (!activeChanges) {
          dispatch(
            novedadIncapacidadesAction(
              INCAPACIDADES.INCAPACIDADES_ENABLE_ACTIVE_CHANGES,
              null
            )
          );
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al validar fechas',
            description: dateValidation.message,
            color: 'error',
          })
        );
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Fecha incorrecta',
          description: 'La fecha final debe ser mayor que la fecha inicial',
          color: 'info',
        })
      );
    }
  };

  const selectInitialDate = (data) => {
    getInitialDate(selectedIndex, data, openedAccordion);
    setInitialDateAnchor(null);
  };

  const selectFinalDate = (data) => {
    getFinalDate(selectedIndex, data, openedAccordion);
    setFinalDateAnchor(null);
  };

  const expandAccordion = (panel) => (_, isExpanded) => {
    if (isExpanded) {
      setOpenAccordion(panel);
    } else {
      setOpenAccordion(null);
    }
  };

  return (
    <ModalNovedad
      open={openIncapacidadesModal}
      title="Reportar incapacidad"
      employeeName={cotizanteName}
      onClose={onClose}
      onCancel={onClose}
      onSave={onSave}
      disableSaveButton={disableSaveButton}
      disableButtons={disableButtons}
    >
      <div className={classes.novedadContainer}>
        <AccordionNovedad
          panel={openedAccordion}
          title={INCAPACIDADES_MODAL.IGE_TITLE}
          subtitle="Días de incapacidad"
          onChange={expandAccordion('IGE')}
          addTitle="Incapacidad"
          focusDate={periodoActual.display}
          rows={incapacidadesIGE}
          openInitialDatePicker={openInitialDatePicker}
          openFinalDatePicker={openFinalDatePicker}
          removeItem={removeItem}
          addRow={addRow}
          selectInitialDate={selectInitialDate}
          selectFinalDate={selectFinalDate}
          initialAnchor={initialDateAnchor}
          finalAnchor={finalDateAnchor}
          id="IGE"
          disableRemoveButton={disableRemoveButton}
          removeIndex={removeIndex}
          disableButtons={disableButtons}
          daysSum={totalDaysIGE}
        />
        <AccordionNovedad
          panel={openedAccordion}
          title={INCAPACIDADES_MODAL.IRL_TITLE}
          subtitle="Días de incapacidad"
          onChange={expandAccordion('IRL')}
          addTitle="Incapacidad"
          focusDate={periodoActual.display}
          rows={incapacidadesIRL}
          id="IRL"
          openInitialDatePicker={openInitialDatePicker}
          openFinalDatePicker={openFinalDatePicker}
          removeItem={removeItem}
          addRow={addRow}
          selectInitialDate={selectInitialDate}
          selectFinalDate={selectFinalDate}
          initialAnchor={initialDateAnchor}
          finalAnchor={finalDateAnchor}
          disableRemoveButton={disableRemoveButton}
          removeIndex={removeIndex}
          disableButtons={disableButtons}
          daysSum={totalDaysIRL}
        />
      </div>
    </ModalNovedad>
  );
};
