import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/es';
import { descuentosAction } from './../redux/Actions';
import { DESCUENTOS } from './../redux/ActionTypes';
import DescuentosModalLayout from './DescuentosModalLayout';

moment.locale('es');

class DescuentosModal extends Component {
	addDescuento = () => {
		this.props.descuentosAction(DESCUENTOS.DESCUENTOS_ADD_DESCUENTO, null);
	};

	updateDescuento = (index, field, data) => {
		if (field === 'authorizationNumber') {
			this.props.descuentosAction(DESCUENTOS.DESCUENTOS_UPDATE_DESCUENTO, {
				index,
				field,
				data
			});
		} else {
			this.props.descuentosAction(DESCUENTOS.DESCUENTOS_UPDATE_DESCUENTO, {
				index,
				field,
				data: parseInt(data),
				incrementTotal: parseInt(data)
			});
		}
	};

	getDescuentoDate = (index, date) => {
		let formattedDate = moment(date).format('MMMM YYYY');

		this.props.descuentosAction(DESCUENTOS.DESCUENTOS_UPDATE_DESCUENTO, {
			index,
			field: 'repeatUntilDate',
			data: formattedDate
		});
	};

	updateCheckBox = (index, currentValue) => {
		this.props.descuentosAction(DESCUENTOS.DESCUENTOS_UPDATE_DESCUENTO, {
			index,
			field: 'repeatUntilchecked',
			data: !currentValue
		});
	};

	removeDescuento = (index) => {
		if (index === 0 && this.props.descuentosArray.length === 1) {
			this.props.descuentosAction(DESCUENTOS.DESCUENTOS_CLEAN_DESCUENTO, { index });
		} else {
			this.props.descuentosAction(DESCUENTOS.DESCUENTOS_REMOVE_DESCUENTO, { index });
		}
	};

	saveDescuentos = () => {
		//DISPARAR FETCH

		this.props.descuentosAction(DESCUENTOS.DESCUENTOS_CHANGE_MODAL_STATUS, null);
	};

	cancelDescuentosModal = () => {
		this.props.descuentosAction(DESCUENTOS.DESCUENTOS_RESET_DATA, null);
	};

	render() {
		const layoutProps = {
			saveAction: this.saveDescuentos,
			cancelAction: this.cancelDescuentosModal,
			updateDescuento: this.updateDescuento,
            getDescuentoDate: this.getDescuentoDate,
            updateCheckBox: this.updateCheckBox,
			removeDescuento: this.removeDescuento,
			addDescuento: this.addDescuento
		};

		return <DescuentosModalLayout {...layoutProps} />;
	}
}

const mapStateToProps = (state) => {
	return {
		totalDescuentosARL: state.descuentosARL.totalDescuentosARL,
		descuentosArray: state.descuentosARL.descuentosARL
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		descuentosAction: (actionType, value) => dispatch(descuentosAction(actionType, value))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(DescuentosModal);
