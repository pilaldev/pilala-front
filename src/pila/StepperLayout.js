import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Divider,
    // ButtonBase,
    // Popper,
    // ClickAwayListener,
    // Paper,
    // MenuList,
    // MenuItem
} from '@material-ui/core';
import clsx from 'clsx';
import {
    STEPPER_VALUES,
    PESOS_SYMBOL,
    TOTAL_PAY,
    // DOWNLOAD_OPTION
} from './Constants'
import FileCopyTwoToneIcon from '@material-ui/icons/FileCopyTwoTone';
import MonetizationOnTwoToneIcon from '@material-ui/icons/MonetizationOnTwoTone';
import HelperFunctions from '../utils/HelperFunctions';
// import SaveAltRoundedIcon from '@material-ui/icons/SaveAltRounded';

const StepperLayout = (props) => {

    const classes = useStyles();
    const currentStep = useSelector(state => state.payPila.currentStep);
    const completedStep = useSelector(state => state.payPila.completedStep);
    const activeCompany = useSelector(state => state.currentUser.activeCompany);
    const periodoActual = useSelector(state => state.currentUser.periodosInfo[activeCompany]);
    const cotizantes = useSelector(state => state.prePlanilla[activeCompany] ? state.prePlanilla[activeCompany][periodoActual.value]['dependientes'][periodoActual.month] : {});
    const [fullAmount, setFullAmount] = useState(0);

    useEffect(() => {
        if (typeof cotizantes === 'object' && Object.keys(cotizantes).length > 0) {
            let total = 0;
            Object.keys(cotizantes).forEach((cotizanteId) => {
                const totalCotizante = cotizantes[cotizanteId].totalAportes;
                if (typeof totalCotizante === 'object') {
                    Object.keys(totalCotizante).forEach((administradora) => {
                        total += totalCotizante[administradora];
                    });
                }
            });
            setFullAmount(total);
        }
        else {
            setFullAmount(0);
        }
    }, [cotizantes]);

    // const [showDownloadMenu, setShowDownloadMenu] = useState(false);
    // const [anchorEl, setAnchorEl] = useState(null);

    // const showDownloadList = (event) => {
    //     setAnchorEl(event.currentTarget);
    //     setShowDownloadMenu(!showDownloadMenu);
    // }

    // const closeDownloadMenu = () => {
    //     if (showDownloadMenu) {
    //         setShowDownloadMenu(!showDownloadMenu);
    //     }
    // }

    return (
        <div className={classes.container}>
            <div className={classes.stepperContainer}>
                {
                    STEPPER_VALUES.map((item, index) => {
                        const STEPPER_ICONS = {
                            1: <FileCopyTwoToneIcon className={clsx(classes.stepOneIcon, { [classes.activeColor]: currentStep === index + 1, [classes.completedColor]: index + 1 <= completedStep })} />,
                            2: <MonetizationOnTwoToneIcon className={clsx(classes.stepTwoIcon, { [classes.activeColor]: currentStep === index + 1, [classes.completedColor]: index + 1 <= completedStep })} />
                        }

                        return (
                            <div
                                key={index}
                                className={classes.stepperContainer}
                                style={{
                                    marginRight: index + 1 < STEPPER_VALUES.length ? 100 : 0
                                }}
                            >
                                <div
                                    className={
                                        clsx(classes.stepIcon, {
                                            [classes.activeStepIcon]: currentStep === index + 1,
                                            [classes.completedStepIcon]: index + 1 <= completedStep
                                        })
                                    }
                                >
                                    {STEPPER_ICONS[index + 1]}
                                </div>
                                <div className={classes.stepContentContainer}>
                                    <Typography
                                        className={
                                            clsx(classes.stepContentText, {
                                                [classes.activeColor]: currentStep === index + 1,
                                                [classes.completedColor]: index + 1 <= completedStep
                                            })
                                        }
                                    >
                                        {item.step}
                                    </Typography>
                                    <div className={
                                        clsx(classes.stepLine, {
                                            [classes.activeBackgroundColor]: currentStep === index + 1,
                                            [classes.completedBackgroundColor]: index + 1 <= completedStep
                                        })}
                                    />
                                    <Typography
                                        className={
                                            clsx(classes.stepContentText, {
                                                [classes.activeDescriptionText]: currentStep === index + 1,
                                                [classes.completedDescriptionText]: index + 1 <= completedStep
                                            })
                                        }
                                    >
                                        {item.description}
                                    </Typography>
                                </div>
                            </div>
                        );
                    })
                }
            </div>
            <div
                style={{ width: currentStep === 1 ? '38%' : '100%' }}
                className={classes.dividerContainer}
            >
                <Divider className={classes.divider} />
            </div>
            {
                currentStep < 2 ?
                    <div className={classes.stepperOptionsContainer}>
                        <div className={classes.totalContainer}>
                            <Typography className={classes.totalText}>
                                {TOTAL_PAY}
                            </Typography>
                            <Typography className={classes.totalAmount}>
                                {`${PESOS_SYMBOL}${HelperFunctions.formatStringNumber(fullAmount)}`}
                            </Typography>
                        </div>
                        {/* <div className={classes.downloadContainer}>
                            <ButtonBase
                                className={classes.downloadButton}
                                onClick={showDownloadList}
                            >
                                <SaveAltRoundedIcon className={classes.downloadIcon} />
                                <Typography className={classes.downloadText}>
                                    {DOWNLOAD_OPTION}
                                </Typography>
                            </ButtonBase>
                        </div> */}
                    </div>
                    : null
            }
            {/* <Popper open={showDownloadMenu} disablePortal placement='bottom' anchorEl={anchorEl} className={classes.downloadMenu}>
                <Paper>
                    <ClickAwayListener onClickAway={closeDownloadMenu}>
                        <div>
                            <MenuList autoFocusItem={false}>
                                {
                                    props.stepperOptions.map((item, index) => {
                                        return (
                                            <MenuItem
                                                onClick={item.action}
                                                key={index}
                                                className={classes.downloadOption}
                                            >
                                                {item.icon}
                                                {item.label}
                                            </MenuItem>
                                        );
                                    })
                                }
                            </MenuList>
                        </div>
                    </ClickAwayListener>
                </Paper>
            </Popper> */}
        </div>
    );
}

StepperLayout.propTypes = {
    stepperOptions: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            icon: PropTypes.element.isRequired,
            action: PropTypes.func.isRequired
        })
    ).isRequired
}

const useStyles = makeStyles({
    container: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    stepperContainer: {
        display: 'flex'
    },
    stepContainer: {
        display: 'flex'
    },
    stepIcon: {
        backgroundColor: '#FFFFFF',
        zIndex: 1,
        width: 45,
        height: 45,
        display: 'flex',
        borderRadius: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        border: '3px solid #C6CCD0',
        boxShadow: '0px 3px 6px #00000029',
        marginTop: 5
    },
    stepContentContainer: {
        display: 'flex',
        flexDirection: 'column'
    },
    stepContentText: {
        textAlign: 'left',
        paddingLeft: 20,
        paddingRight: 20,
        fontSize: '1em',
        color: '#C6CCD0',
        fontWeight: 'bold'
    },
    activeStepIcon: {
        borderColor: '#5E35B1'
    },
    completedStepIcon: {
        borderColor: '#5E35B1'
    },
    stepLine: {
        height: 3,
        border: 0,
        backgroundColor: '#C6CCD0',
        borderRadius: 1,
        marginLeft: -1,
        boxShadow: '0px 3px 6px #00000029',
        margin: '5px 0px'
    },
    stepOneIcon: {
        color: '#C6CCD0',
        fontSize: '1.5em'
    },
    stepTwoIcon: {
        color: '#C6CCD0',
        fontSize: '2em'
    },
    activeColor: {
        color: '#5E35B1'
    },
    completedColor: {
        color: '#5E35B1'
    },
    activeBackgroundColor: {
        backgroundColor: '#5E35B1'
    },
    completedBackgroundColor: {
        backgroundColor: '#5E35B1'
    },
    activeDescriptionText: {
        color: '#263238'
    },
    completedDescriptionText: {
        color: '#263238'
    },
    dividerContainer: {
        padding: '15px 0px 10px 0px'
    },
    divider: {
        backgroundColor: '#C6CCD0',
        height: 1.5,
        width: '100%'
    },
    stepperOptionsContainer: {
        height: 20,
        width: '38%',
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    totalContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    totalText: {
        color: '#263238',
        fontSize: '1em'
    },
    totalAmount: {
        color: '#263238',
        fontSize: '1em',
        fontWeight: 'bold',
        paddingLeft: 5
    },
    className: {
        display: 'flex',
        alignItems: 'center'
    },
    downloadIcon: {
        color: '#757AFF',
        fontSize: '1.5em',
        marginRight: 5
    },
    downloadText: {
        color: '#757AFF',
        fontSize: '1em'
    },
    downloadButton: {
        borderRadius: 100
    },
    downloadOption: {
        fontSize: '1em',
        color: '#757AFF'
    },
    downloadMenu: {
        zIndex: 2
    }
});

export default StepperLayout;
