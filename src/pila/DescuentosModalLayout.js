import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
	Dialog,
	Typography,
	DialogContent,
	Divider,
	IconButton,
	ButtonBase,
	Button,
	ExpansionPanel,
	ExpansionPanelSummary,
	ExpansionPanelDetails,
	List,
	ListItem,
	TextField,
    InputAdornment,
    Checkbox
} from '@material-ui/core';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { DESCUENTOS_MODAL } from './Constants';
import TodayTwoToneIcon from '@material-ui/icons/TodayTwoTone';
import RemoveCircleTwoToneIcon from '@material-ui/icons/RemoveCircleTwoTone';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddCircleTwoToneIcon from '@material-ui/icons/AddCircleTwoTone';
import MonetizationOnTwoToneIcon from '@material-ui/icons/MonetizationOnTwoTone';
import CheckBoxTwoToneIcon from '@material-ui/icons/CheckBoxTwoTone';
import CheckBoxOutlineBlankRoundedIcon from '@material-ui/icons/CheckBoxOutlineBlankRounded';
import AssignmentTwoToneIcon from '@material-ui/icons/AssignmentTwoTone';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import 'moment/locale/es';

moment.locale('es');

const DescuentosModalLayout = (props) => {
	const classes = useStyles();

	const [ showDatePicker, setShowDatePicker ] = useState(false);
	const [ dateAnchor, setDateAnchor ] = useState(null);

	const showModal = useSelector((state) => state.descuentosARL.openedDescuentosARLModal);
	const totalDescuentosARL = useSelector((state) => state.descuentosARL.totalDescuentosARL);
	const descuentosArray = useSelector((state) => state.descuentosARL.descuentosARL);

	const openDatePicker = (event) => {
		setDateAnchor(event.currentTarget);
		setShowDatePicker(!showDatePicker);
	};

	const selectDescuentoDate = (index, date) => {
		props.getDescuentoDate(index, date);
		// console.log("DATA",date,index)
		setShowDatePicker(!showDatePicker);
	};

	return (
		<ThemeProvider theme={theme}>
			<Dialog
				open={showModal}
				maxWidth="sm"
				PaperProps={{
					className: classes.paperContainer
				}}
			>
				<DialogContent className={classes.dialogContent}>
					<Typography className={classes.dialogTitle}>{DESCUENTOS_MODAL.TITLE}</Typography>
					<Divider className={classes.divider} />
					<div className={classes.novedadContainer}>
						<div className={classes.headerSummaryContent}>RESUMEN DE TARIFAS</div>

						{descuentosArray.map((item, index) => {
							return (
								<div className={classes.expansionPanelContainer}>
									<ExpansionPanel defaultExpanded>
										<ExpansionPanelSummary
											expandIcon={<ExpandMoreIcon />}
											className={classes.expansionPanelSummary}
										>
											<Typography className={classes.novedadName}>
												{DESCUENTOS_MODAL.EXPANSION_PANEL_TITLE}
											</Typography>

											<ButtonBase
												className={classes.dayCounterField}
												style={{
													backgroundColor: '#FFFFFF'
												}}
												disableRipple
											>
												<Typography
													style={{
														color: '#2962FF',
														fontSize: '1.1em',
														fontWeight: 'bold'
													}}
												>
													{totalDescuentosARL}
												</Typography>
											</ButtonBase>

											{/* <Divider className={classes.divider} /> */}
										</ExpansionPanelSummary>
										{/* <Divider className={classes.divider} /> */}
										<ExpansionPanelDetails className={classes.expansionPanelDetails}>
											<List component="div" className={classes.list} disablePadding>
												<ListItem component="div" className={classes.listItem}>
													<div className={classes.listItemRowTitle}>
														<Typography className={classes.listItemRowTitleText}>
															{DESCUENTOS_MODAL.AUTHORIZATION_NUMBER_TEXT}
														</Typography>
													</div>
													<div className={classes.listItemRowContent}>
														<AssignmentTwoToneIcon className={classes.modalIcons} />
														<TextField
															variant="outlined"
															size="small"
															className={classes.salarytextInput}
															placeholder={'salario'}
															InputProps={{
																classes: { adornedStart: classes.inputAdornment },
																// className: classes.input,
																startAdornment: (
																	<InputAdornment
																		className={classes.inputAdornment}
																		position="start"
																	>
																		#
																	</InputAdornment>
																),
																style: {
																	height: 30
																}
															}}
															name="authorizationNumber"
															value={item.authorizationNumber}
															style={{
																height: 30
															}}
															onChange={(event) => {
																if (!isNaN(event.target.value)) {
																	props.updateDescuento(
																		index,
																		event.target.name,
																		event.target.value
																	);
																}
															}}
														/>
													</div>
													<div className={classes.listItemRowIcon}>
														<IconButton
															onClick={props.removeDescuento.bind(this, index)}
															className={classes.removeIcon}
															classes={{
																root: classes.removeIcon
															}}
														>
															<RemoveCircleTwoToneIcon />
														</IconButton>
													</div>
												</ListItem>
												<ListItem component="div" className={classes.listItem}>
													<div className={classes.listItemRowTitle}>
														<Typography className={classes.listItemRowTitleText}>
															{DESCUENTOS_MODAL.ARL_DISCOUNT_TEXT}
														</Typography>
													</div>
													<div className={classes.listItemRowContent}>
														<MonetizationOnTwoToneIcon className={classes.modalIcons} />
														<TextField
															variant="outlined"
															size="small"
															className={classes.salarytextInput}
															placeholder={'salario'}
															InputProps={{
																classes: { adornedStart: classes.inputAdornment },
																// className: classes.input,
																startAdornment: (
																	<InputAdornment
																		className={classes.inputAdornment}
																		position="start"
																	>
																		$
																	</InputAdornment>
																),
																style: {
																	height: 30
																}
															}}
															name="ARLDiscountValue"
															value={item.ARLDiscountValue}
															style={{
																height: 30
															}}
															onChange={(event) => {
																if (!isNaN(event.target.value)) {
																	props.updateDescuento(
																		index,
																		event.target.name,
																		event.target.value
																	);
																}
															}}
														/>
													</div>
													<div className={classes.listItemRowIcon} />
												</ListItem>
												<ListItem component="div" className={classes.listItem}>
													<div className={classes.listItemRowTitle}>
														<Typography className={classes.listItemRowTitleText}>
															{DESCUENTOS_MODAL.OTHER_RIKS_TEXT}
														</Typography>
													</div>
													<div className={classes.listItemRowContent}>
														<MonetizationOnTwoToneIcon className={classes.modalIcons} />
														<TextField
															variant="outlined"
															size="small"
															className={classes.salarytextInput}
															placeholder={'salario'}
															InputProps={{
																classes: { adornedStart: classes.inputAdornment },
																// className: classes.input,
																startAdornment: (
																	<InputAdornment
																		className={classes.inputAdornment}
																		position="start"
																	>
																		$
																	</InputAdornment>
																),
																style: {
																	height: 30
																}
															}}
															name="otherRiskDiscountValue"
															value={item.otherRiskDiscountValue}
															style={{
																height: 30
															}}
															onChange={(event) => {
																if (!isNaN(event.target.value)) {
																	props.updateDescuento(
																		index,
																		event.target.name,
																		event.target.value
																	);
																}
															}}
														/>
													</div>
													<div className={classes.listItemRowIcon} />
												</ListItem>
												<ListItem component="div" className={classes.listItem}>
													<div className={classes.listItemRowTitle}>
														<Typography className={classes.listItemRowTitleText}>
															{DESCUENTOS_MODAL.CREDIT_NOTE_TEXT}
														</Typography>
													</div>
													<div className={classes.listItemRowContent}>
														<MonetizationOnTwoToneIcon className={classes.modalIcons} />
														<TextField
															variant="outlined"
															size="small"
															className={classes.salarytextInput}
															placeholder={'salario'}
															InputProps={{
																classes: { adornedStart: classes.inputAdornment },
																// className: classes.input,
																startAdornment: (
																	<InputAdornment
																		className={classes.inputAdornment}
																		position="start"
																	>
																		$
																	</InputAdornment>
																),
																style: {
																	height: 30
																}
															}}
															name="creditNoteDiscontValue"
															value={item.creditNoteDiscontValue}
															style={{
																height: 30
															}}
															onChange={(event) => {
																if (!isNaN(event.target.value)) {
																	props.updateDescuento(
																		index,
																		event.target.name,
																		event.target.value
																	);
																}
															}}
														/>
													</div>
													<div className={classes.listItemRowIcon} />
												</ListItem>

												<ListItem
													component="div"
													className={classes.listItem}
													style={{ marginBottom: 0 }}
												>
													<div className={classes.listItemRowTitle}>
														<Checkbox
															checked={item.repeatUntilchecked}
															// disabled={true}
															// color="primary"
															className={classes.obligacionCheckbox}
															checkedIcon={
																<CheckBoxTwoToneIcon
																	className={classes.obligacionButtonCheckedIcon}
																/>
															}
															icon={
																<CheckBoxOutlineBlankRoundedIcon
																	className={classes.obligacionButtonIcon}
																/>
                                                            }
                                                            onClick={props.updateCheckBox.bind(this,index,item.repeatUntilchecked)}
														/>
														<Typography className={classes.listItemRowTitleText}>
															{DESCUENTOS_MODAL.REPEAT_UNTIL_TEXT}
														</Typography>
													</div>
													<div className={classes.listItemRowContent}>
														<IconButton
															onClick={openDatePicker}
															classes={{
																root: classes.dateIcon
                                                            }}
                                                            disabled={!item.repeatUntilchecked}
														>
															<TodayTwoToneIcon className={classes.modalIcons} />
														</IconButton>
														<ButtonBase
															onClick={openDatePicker}
                                                            className={classes.dateField}
                                                            disabled={!item.repeatUntilchecked}
														>
															<Typography
																style={{
																	color: '#2962FF',
																	fontSize: '1.1em',
																	fontWeight: 'bold'
																}}
															>
																{item.repeatUntilDate}
															</Typography>
														</ButtonBase>

														<MuiPickersUtilsProvider utils={MomentUtils}>
															<DatePicker
																open={showDatePicker}
																autoOk
																views={[ 'year', 'month' ]}
																onChange={selectDescuentoDate.bind(this, index)}
																variant="inline"
																PopoverProps={{
																	anchorEl: dateAnchor,
																	anchorOrigin: {
																		vertical: 'bottom',
																		horizontal: 'center'
																	},
																	transformOrigin: {
																		vertical: 'top',
																		horizontal: 'center'
																	}
																}}
																TextFieldComponent={() => null}
															/>
														</MuiPickersUtilsProvider>
													</div>
													<div className={classes.listItemRowIcon} />
												</ListItem>
											</List>
										</ExpansionPanelDetails>
									</ExpansionPanel>
								</div>
							);
						})}

						<div className={classes.addButtonContainer}>
							<ButtonBase onClick={props.addDescuento}>
								<AddCircleTwoToneIcon style={{ fontSize: '1.5em', color: '#2962FF', marginRight: 5 }} />
								<Typography
									style={{
										fontSize: '1em',
										color: '#2962FF',
										fontWeight: 'bold'
									}}
								>
									{DESCUENTOS_MODAL.ADD_BUTTON}
								</Typography>
							</ButtonBase>
						</div>

						<div className={classes.buttonsContainer}>
							<Button className={classes.cancelButton} onClick={props.cancelAction}>
								<Typography variant="subtitle2" className={classes.cancelButtonText}>
									{DESCUENTOS_MODAL.CANCEL_BUTTON}
								</Typography>
							</Button>
							<Button variant="contained" className={classes.saveButton} onClick={props.saveAction}>
								<Typography variant="subtitle2" className={classes.saveButtonText}>
									{DESCUENTOS_MODAL.SAVE_BUTTON}
								</Typography>
							</Button>
						</div>
					</div>
				</DialogContent>
			</Dialog>
		</ThemeProvider>
	);
};

const theme = createMuiTheme({
	overrides: {
		MuiPickersToolbar: {
			toolbar: {
				backgroundColor: 'rgba(94, 53, 177, 0.25)'
			}
		},
		MuiPickersToolbarText: {
			toolbarBtnSelected: {
				color: '#2962FFV'
			},
			toolbarTxt: {
				color: '#2962FF'
			}
		},
		MuiPickersDay: {
			daySelected: {
				backgroundColor: '#2962FF'
			}
		},
		MuiOutlinedInput: {
			root: {
				'& fieldset': {
					borderColor: '#2962FF',
					borderWidth: '1.5px'
				},
				// '&:hover fieldset': {
				// 	borderColor: 'purple'
				// },
				// '&.Mui-focused fieldset': {
				// 	borderColor: 'purple'
				// },
				'&:hover:not($disabled):not($focused):not($error) $notchedOutline': {
					borderColor: '#2962FF',
					borderWidth: '1.5px'
				},
				'& input:valid:focus + fieldset': {
					borderColor: '#2962FF',
					borderWidth: '2px'
				}
			}
		},
		MuiExpansionPanelDetails: {
			root: {
				padding: '30px 8px 30px'
			}
		},
		// MuiExpansionPanel: {
		// 	root: {
		//         marginBottom: 5
		// 		// border: '1px solid rgba(0, 0, 0, .125)',
		// 		// boxShadow: 'none',
		// 		// '&:not(:last-child)': {
		// 		// 	borderBottom: 0
		// 		// },
		// 		// '&:before': {
		// 		// 	display: 'none'
		// 		// },
		// 		// '&$expanded': {
		// 		// 	margin: 'auto'
		// 		// }
		// 	}
		// },
		MuiExpansionPanelSummary: {
			root: {
				backgroundColor: 'white',
				borderBottom: '1px solid #2962FF',
				marginBottom: 5,
				minHeight: 40,
				height: 40,
				'&$expanded': {
					maxHeight: 40,
					minHeight: 40
				},
				padding: 10
			},
			content: {
				margin: 0,
				'&$expanded': {
					margin: 0
				},
				display: 'flex',
				justifyContent: 'space-between',
				alignItems: 'center'
			}
		},
		MuiPaper: {
			elevation1: {
				boxShadow: 'none'
				// borderBottomColor: 'purple'
			}
		}
	}
});

const useStyles = makeStyles({
	expansionPanelContainer: {
		width: '100%',
		marginBottom: 20
	},
	expansionPanelSummary: {
		margin: 0
	},
	expansionPanelDetails: {
		// backgroundColor:'grey',
		backgroundColor: '#F7F7F7'
	},
	headerSummaryContent: {
		width: '80%',
		border: '1.5px solid grey',
		backgroundColor: '#ECEFF1',
		marginBottom: 40,
		borderRadius: 7
	},

	list: {
		width: '100%'
	},
	listItem: {
		width: '100%',
		display: 'flex',
		padding: 0,
		marginBottom: 20
	},
	listItemRowTitle: {
        display:'flex',
		width: '55%',
        height: '100%',
        alignContent:'center'
	},
	listItemRowTitleText: {
		color: 'grey',
		fontSize: '1.1em',
		fontWeight: 'bold'
	},
	listItemRowContent: {
		display: 'flex',
		alignItems: 'center',
		width: '35%',
		height: '100%',
		justifyContent: 'flex-end'
	},
	salarytextInput: {
		width: '70%',
		height: '100%',
		background: 'transparent',
		borderRadius: 7
	},
	listItemRowIcon: {
		width: '10%',
		height: '100%',
		display: 'flex',
		justifyContent: 'center'
	},
	modalIcons: {
		color: '#2962FF',
		marginRight: 10,
		padding: 0
    },
    obligacionCheckbox: {
        padding: 0,
        marginRight:5
    },
    obligacionButtonCheckedIcon: {
        color: '#4CAF50'
    },
    obligacionButtonIcon: {
        color: '#95989A'
    },
	dateField: {
		border: '1.5px solid #2962FF ',
		borderRadius: 4,
		width: '70%',
		height: 30
		// margin: '0px 6px 0px 6px'
	},
	dateIcon: {
		padding: 0
	},
	inputAdornment: {
		color: '#2962FF'
	},

	dayCounterField: {
		border: '1.5px solid #2962FF',
		borderRadius: 7,
		width: '20%',
		height: 30,
		margin: '0px 6px 0px 6px'
	},
	addButtonContainer: {
		display: 'flex',
		justifyContent: 'flex-end',
		width: '100%'
	},
	paperContainer: {
		backgroundColor: '#FFFFFF',
		display: 'flex',
		flex: 1
	},
	saveButton: {
		background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
		borderRadius: 22,
		marginLeft: 10,
		textTransform: 'none'
	},
	saveButtonText: {
		color: 'white'
	},
	cancelButtonText: {
		color: '#263238',
		transform: 'no'
	},
	cancelButton: {
		marginRight: 10,
		borderRadius: 22,
		textTransform: 'none'
	},
	dialogContent: {
		paddingRight: 0,
		paddingLeft: 0
	},
	dialogTitle: {
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: '2em',
		color: '#2962FF',
		paddingBottom: 20
	},
	dialogSubtitleContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	cotizanteNameText: {
		fontWeight: 'bold',
		fontSize: '1.3em',
		color: '#37474F'
	},
	dividerSymbol: {
		fontSize: '2em',
		margin: '0px 5px 0px 5px',
		color: '#37474F'
	},
	cotizanteSalary: {
		fontSize: '1.3em',
		color: '#37474F'
	},
	divider: {
		// margin: '15px 0px 15px 0px',
		backgroundColor: '#2962FF'
	},
	novedadContainer: {
		width: '100%',
		display: 'flex',
		padding: '8% 5% 0% 5%',
		flexDirection: 'column',
		alignItems: 'center'
	},
	novedadName: {
		color: '#2962FF',
		fontSize: '1.2em'
		// marginBottom: 5
	},
	dateContent: {
		backgroundColor: '#F7F7F7',
		width: '100%',
		borderTop: '1px solid #5E35B1',
		display: 'flex',
		padding: '5% 0px 5% 0px'
		// overflow: 'auto',
		// height: 30
	},
	dateTitleContainer: {
		// flex: 1,
		// alignItems: 'center',
		display: 'flex',
		paddingLeft: '3%',
		paddingTop: '0.7%'
	},
	dateTitle: {
		color: '#78909C'
	},
	dateContainer: {
		flex: 1,
		paddingRight: '3%',
		paddingLeft: '3%',
		display: 'flex',
		flexDirection: 'column',
		// alignItems: 'center',
		justifyContent: 'flex-start',
		// backgroundColor: 'lightblue'
		overflow: 'auto',
		maxHeight: 185

		// height: 30
	},
	rangeContainer: {
		flex: 1,
		// paddingRight: '3%',
		// paddingLeft: '3%',
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-end',
		marginBottom: 5
		// backgroundColor: 'red'
	},
	addRow: {
		flex: 1,
		// paddingRight: '3%',
		// paddingLeft: '3%',
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-end'
		// backgroundColor: 'blue'
	},
	calendarIcon: {
		color: '#5E35B1',
		margin: 0,
		fontSize: '0.9em'
	},
	removeIcon: {
		color: '#FF495A',
		padding: 0
	},
	buttonsContainer: {
		display: 'flex',
		alignItems: 'center',
		padding: '10% 0px 5% 0px',
		justifyContent: 'flex-end',
		width: '100%'
	},

	vacacionesTooltip: {
		backgroundColor: '#2962FF'
	},
	vacacionesTooltipArrow: {
		color: '#2962FF'
	}
});

export default DescuentosModalLayout;
