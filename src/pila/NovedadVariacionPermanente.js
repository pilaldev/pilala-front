import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import 'moment/locale/es';
import {
  novedadVariacionPermanenteAction,
  prePlanillaAction,
  snackBarAction,
} from '../redux/Actions';
import {
  PRE_PLANILLA,
  SNACKBAR,
  VARIACION_PERMANENTE,
} from '../redux/ActionTypes';
import { editRecordField } from '../constants/CotizantesFields';
import { ModalNovedad } from '.';
import { Calendar } from '../components';
import { ButtonBase, Grid, Typography } from '@material-ui/core';
import { VARIACION_PERMANENTE_MODAL } from './Constants';
import DateRangeIcon from '@material-ui/icons/DateRange';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { useDispatch, useSelector } from 'react-redux';
import { CreateVariacionSalario, RemoveNovedad } from './api';
import HelperFunctions from '../utils/HelperFunctions';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import MaskedInput from 'react-text-mask';
import { requestPreplanillaData } from './pilaTable/api/RequestPreplanillaData';
import TextInput from '@bit/pilala.pilalalib.components.textinput';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';

moment.locale('es');

export default () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [dateAnchor, setDateAnchor] = useState({});
  const [showDatePicker, setShowDatePicker] = useState(false);
  const openVariacionPermanenteModal = useSelector(
    (state) => state.novedadVariacionPermanente.openVariacionPermanenteModal
  );
  const cotizanteName = useSelector(
    (state) => state.novedadVariacionPermanente.cotizanteName
  );
  const cotizanteSalary = useSelector(
    (state) => state.novedadVariacionPermanente.cotizanteSalary
  );
  const dateStarted = useSelector(
    (state) => state.novedadVariacionPermanente.dateStarted
  );
  const newSalary = useSelector(
    (state) => state.novedadVariacionPermanente.newSalary
  );
  const ajustePermanente = useSelector(
    (state) => state.novedadVariacionPermanente.ajustePermanente
  );
  const activeChanges = useSelector(
    (state) => state.novedadVariacionPermanente.activeChanges
  );
  const idNovedad = useSelector(
    (state) => state.novedadVariacionPermanente.idNovedad
  );
  const cotizanteType = useSelector(
    (state) => state.novedadVariacionPermanente.cotizanteType
  );
  const regLine = useSelector(
    (state) => state.novedadVariacionPermanente.regLine
  );
  const cotizanteDocumentNumber = useSelector(
    (state) => state.novedadVariacionPermanente.cotizanteDocumentNumber
  );
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const periodoActual = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const [disableSaveButton, setDisableSaveButton] = useState(false);
  const [disableButtons, setDisableButtons] = useState(false);
  const [disableRemoveButton, setDisableRemoveButton] = useState(false);
  const periodosInfo = useSelector((state) => state.currentUser.periodosInfo);
  const cotizantesInfo = useSelector((state) => state.cotizantesInfo);
  const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);

  //TODO: Verificar nombre de compañia cuando se activen sucursales

  const openDatePicker = (event) => {
    setDateAnchor(event.currentTarget);
    setShowDatePicker(!showDatePicker);
  };

  const selectNewDate = (data) => {
    let day = moment(data).date();
    let daysInMonth = moment(data).daysInMonth();
    let days = 0;

    if (day === 1) {
      days = 30;
    } else if (daysInMonth === day) {
      days = 1;
    } else {
      days = 30 - day + 1;
    }

    const value = {
      dateStarted: moment(data).format('DD-MM-YYYY'),
      fechaInicio: moment(data).format('DD-MM-YYYY'),
      duracionDias: days,
      regLine: {
        21: editRecordField[21]('X'),
        36: editRecordField[36](days),
        37: editRecordField[37](days),
        38: editRecordField[38](days),
        39: editRecordField[39](days),
        82: editRecordField[82](moment(data).format('YYYY-MM-DD')),
      },
    };
    dispatch(
      novedadVariacionPermanenteAction(
        VARIACION_PERMANENTE.VARIACION_PERMANENTE_UPDATE_NOVEDAD_FIELDS,
        value
      )
    );
    setShowDatePicker(!showDatePicker);
  };

  const getNewSalary = (e) => {
    const value = e.target.value.trim().replace(/[.\s]/g, '');
    dispatch(
      novedadVariacionPermanenteAction(
        VARIACION_PERMANENTE.VARIACION_PERMANENTE_UPDATE_NEW_SALARY,
        value
      )
    );
  };

  const onClose = () => {
    if (openVariacionPermanenteModal) {
      dispatch(
        novedadVariacionPermanenteAction(
          VARIACION_PERMANENTE.VARIACION_PERMANENTE_MODAL_SET_STATUS,
          null
        )
      );
    }
    dispatch(
      novedadVariacionPermanenteAction(
        VARIACION_PERMANENTE.VARIACION_PERMANENTE_RESET_DATA,
        null
      )
    );
  };

  const onSave = async () => {
    if (disableSaveButton) return;
    setDisableSaveButton(true);
    setDisableButtons(true);

    if (dateStarted !== '' && newSalary !== '') {
      if (activeChanges) {
        const companyInfo = companiesInfo[activeCompany];
        const objetoNovedadVariacionSalario = {
          tipoCotizante:
            cotizantesInfo[activeCompany][cotizanteDocumentNumber]
              .tipoCotizante,
          location: companyInfo.location,
          nitCompany: companyInfo.nit,
          adminPlatform: companyInfo.platformAdmin,
          idCotizante:
            cotizantesInfo[activeCompany][cotizanteDocumentNumber].id,
          idAportante: companyInfo.idCompany,
          cotizanteData: {
            documentNumber: cotizanteDocumentNumber,
            companyName: companyInfo.companyName,
            type: 1,
            salary: {
              dateCreated: '',
              dateApply: dateStarted,
              code: newSalary,
              value: newSalary,
              newSalary: newSalary,
              previousSalary: cotizanteSalary,
            },
            regLine,
          },
        };
        const variacionResult = await CreateVariacionSalario(
          objetoNovedadVariacionSalario
        );
        if (variacionResult.status === 'SUCCESS') {
          requestPreplanillaData({
            nit: companyInfo.nit,
            companyId: companyInfo.idCompany,
            specificPlanilla: false,
            specificInfo: null,
          }).then((result) => {
            if (result.result === 'SUCCESS') {
              dispatch(
                prePlanillaAction(
                  PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA,
                  result.data
                )
              );

              setDisableSaveButton(false);
              setDisableButtons(false);
              onClose();
            }
          });
        } else if (variacionResult.status === 'FAILED') {
          dispatch(
            snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear novedad',
              description:
                'Hemos tenido problemas para crear la novedad de variación permanente, inténtelo nuevamente',
              color: 'error',
            })
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'No se detectaron cambios',
            description:
              'La información sobre la variación permanente no ha cambiado',
            color: 'info',
          })
        );
        setDisableSaveButton(false);
        setDisableButtons(false);
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description: 'Por favor ingrese la información del nuevo salario',
          color: 'warning',
        })
      );
      setDisableSaveButton(false);
      setDisableButtons(false);
    }
  };

  const removeVariacionPermanente = async () => {
    if (disableRemoveButton) return;
    setDisableRemoveButton(true);
    setDisableButtons(true);

    if (idNovedad !== '') {
      const variacionDeleteData = {
        idAportante: activeCompany,
        cotizanteData: {
          idCotizante:
            cotizantesInfo[activeCompany][cotizanteDocumentNumber].id,
          documentNumber: cotizanteDocumentNumber,
          companyName: companiesInfo[activeCompany].companyName,
          idNew: idNovedad,
          province: companiesInfo[activeCompany].location.province.value,
          city: companiesInfo[activeCompany].location.city.value,
          tipoCotizante: cotizanteType,
          type: 'Salarios',
        },
      };
      const removeResult = await RemoveNovedad(variacionDeleteData);
      if (removeResult.status === 'SUCCESS') {
        const cotizante_fields = {
          companyNit: activeCompany,
          periodoActual: periodosInfo[activeCompany].value,
          idPlanilla: periodosInfo[activeCompany].month,
          identificationNumber: cotizanteDocumentNumber,
          cotizanteFields: {
            ...removeResult.data.result,
          },
        };
        dispatch(
          prePlanillaAction(
            PRE_PLANILLA.PRE_PLANILLA_UPDATE_COTIZANTE_FIELDS,
            cotizante_fields
          )
        );
        dispatch(
          novedadVariacionPermanenteAction(
            VARIACION_PERMANENTE.VARIACION_PERMANENTE_DELETE_NOVEDAD,
            null
          )
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
      } else if (removeResult.status === 'FAILED') {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al eliminar novedad',
            description:
              'Hemos tenido problemas para eliminar la novedad de variación permanente, inténtelo nuevamente',
            color: 'error',
          })
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
      }
    } else {
      dispatch(
        novedadVariacionPermanenteAction(
          VARIACION_PERMANENTE.VARIACION_PERMANENTE_DELETE_NOVEDAD,
          null
        )
      );
      setDisableRemoveButton(false);
      setDisableButtons(false);
    }
  };

  return (
    <ModalNovedad
      open={openVariacionPermanenteModal}
      title="Reportar variación permanente"
      employeeName={cotizanteName}
      onClose={onClose}
      onCancel={onClose}
      onSave={onSave}
      disableSaveButton={disableSaveButton}
      disableButtons={disableButtons}
    >
      <div className={classes.novedadContainer}>
        <Typography className={classes.novedadName} variant="subtitle1">
          {VARIACION_PERMANENTE_MODAL.SUBTITLE}
        </Typography>
        <div className={classes.dateContent}>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <div className={classes.dateTitleContainer}>
              <Typography className={classes.dateTitle} variant="subtitle1">
                {VARIACION_PERMANENTE_MODAL.NEW_SALARY}
              </Typography>
            </div>
            <div className={classes.inputContainer}>
              <div className={classes.salaryContainer}>
                <Typography variant="subtitle2" noWrap>
                  {`$ ${HelperFunctions.formatStringNumber(cotizanteSalary)}`}
                </Typography>
              </div>
              <ArrowForwardIcon className={classes.icon} />
              <div className={classes.salaryInputContainer}>
                <TextInput
                  fullWidth={true}
                  placeholder="Nuevo salario"
                  startIcon={<AttachMoneyIcon />}
                  InputProps={{
                    inputComponent: MaskedSalaryInput,
                  }}
                  value={newSalary}
                  onChange={getNewSalary}
                />
              </div>
              <div className={classes.removeIconContainer}>
                {ajustePermanente && (
                  <IconButton
                    buttoncolor="error"
                    filled={false}
                    size="small"
                    loading={disableRemoveButton}
                    disabled={disableButtons}
                    onClick={removeVariacionPermanente}
                  >
                    <RemoveCircleOutlineIcon />
                  </IconButton>
                )}
              </div>
            </div>
          </Grid>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            className={classes.calendarRow}
          >
            <div className={classes.dateTitleContainer}>
              <Typography className={classes.dateTitle} variant="subtitle1">
                {VARIACION_PERMANENTE_MODAL.NEW_SALARY_DATE}
              </Typography>
            </div>
            <div className={classes.calendarContainer}>
              <IconButton filled={false} size="small" onClick={openDatePicker}>
                <DateRangeIcon />
              </IconButton>
              <ButtonBase
                onClick={openDatePicker}
                className={classes.dateField}
              >
                <Typography className={classes.dateText} variant="subtitle2">
                  {dateStarted}
                </Typography>
              </ButtonBase>
              <Calendar
                open={showDatePicker}
                onChange={selectNewDate}
                anchorElement={dateAnchor}
                focusDate={periodoActual.display}
                color="primary"
              />
            </div>
          </Grid>
        </div>
      </div>
    </ModalNovedad>
  );
};

const MaskedSalaryInput = (props) => {
  const { inputRef, ...other } = props;
  const salaryMask = createNumberMask({
    prefix: '',
    thousandsSeparatorSymbol: '.',
    allowDecimal: false,
  });

  return (
    <MaskedInput
      {...other}
      mask={salaryMask}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      guide={false}
    />
  );
};

const useStyles = makeStyles((theme) => ({
  novedadContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(8),
  },
  novedadName: {
    color: theme.palette.primary.main,
  },
  dateContent: {
    backgroundColor: theme.palette.grey[200],
    width: '100%',
    borderTop: '1px solid',
    borderTopColor: theme.palette.primary[400],
    display: 'flex',
    padding: theme.spacing(3, 2, 5, 2),
    flexDirection: 'column',
  },
  inputContainer: {
    display: 'flex',
    alignItems: 'center',
    flex: 1,
    height: 'auto',
    justifyContent: 'flex-end',
  },
  salaryContainer: {
    backgroundColor: theme.palette.primary[200],
    height: 40,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '6.5rem',
    color: theme.palette.primary.main,
    marginRight: theme.spacing(1),
    padding: theme.spacing(0, 1),
  },
  icon: {
    color: theme.palette.primary.main,
    fontSize: '1.5rem',
  },
  salaryInputContainer: {
    width: '10.5rem',
    margin: theme.spacing(1),
  },
  removeIconContainer: {
    width: 30,
    height: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dateField: {
    border: '1px solid',
    borderColor: theme.palette.primary.main,
    borderRadius: 8,
    width: '100%',
    height: 30,
    margin: theme.spacing(0, 4.5, 0, 1),
    display: 'flex',
    alignItems: 'center',
  },
  dateText: {
    color: theme.palette.common.black,
  },
  calendarContainer: {
    display: 'flex',
    alignItems: 'center',
    width: '12rem',
  },
  calendarRow: {
    marginTop: theme.spacing(3),
  },
}));
