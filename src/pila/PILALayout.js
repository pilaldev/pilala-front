import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import BottomTabLayout from './BottomTabLayout';
import ValidationLayout from './ValidationLayout';
import PilaTable from './pilaTable/PilaTable';
import HeaderLayout from './HeaderLayout';
import Summary from './Summary';
import {
  NovedadIngreso,
  NovedadRetiro,
  NovedadSuspensiones,
  NovedadVacaciones,
  NovedadVariacionPermanente,
  NovedadIncapacidades,
  NovedadLicencias,
  DetalleAportes,
} from '.';
import PlanillaDetailModal from '../planillaDetail/PlanillaDetailModal';
import './styles.css';

const PILALayout = (props) => {
  const classes = useStyles();
  const paidCurrentPila = useSelector((state) => state.payPila.paidCurrentPila);
  const currentStep = useSelector((state) => state.payPila.currentStep);
  const validationFailed = useSelector(
    (state) => state.payPila.validationFailed
  );

  return (
    <div className={classes.mainContainer} id="pilaContainer">
      <div className={classes.headerContainer}>
        <HeaderLayout stepperOptions={props.stepperOptions} />
      </div>
      <div className={classes.tableContainer}>
        {currentStep === 1 ? (
          <div className={classes.tableContentContainer}>
            <Typography className={classes.tableTitle}>Cotizantes</Typography>
            <PilaTable key={props.tableKey} />
          </div>
        ) : currentStep === 2 && !validationFailed ? (
          <Summary
            sendPlanillaPayButton={props.sendPlanillaPayButton}
            cancelSendPayButton={props.cancelSendPayButton}
            openSendPayButtonModal={props.openSendPayButtonModal}
            confirmSendPayButtonModal={props.confirmSendPayButtonModal}
            continueSendPayButton={props.continueSendPayButton}
            getPlanillaPayLink={props.getPlanillaPayLink}
            disablePayButton={props.disablePayButton}
            disableSendPayButton={props.disableSendPayButton}
            returnToControlPanel={props.returnToControlPanel}
          />
        ) : null}
      </div>
      {!paidCurrentPila ? (
        <BottomTabLayout
          saveAndContinueAction={props.saveAndContinueAction}
          returnToFirstStep={props.returnToFirstStep}
        />
      ) : null}
      <ValidationLayout
        validatePila={props.validatePila}
        checkStepStatus={props.checkStepStatus}
        createStepStatus={props.createStepStatus}
        validateStepStatus={props.validateStepStatus}
      />
      <NovedadIngreso />
      <NovedadRetiro />
      <NovedadVariacionPermanente />
      <NovedadVacaciones />
      <NovedadIncapacidades />
      <NovedadLicencias />
      <NovedadSuspensiones />
      <PlanillaDetailModal />
      <DetalleAportes />
    </div>
  );
};

PILALayout.propTypes = {
  saveAndContinueAction: PropTypes.func.isRequired,
  returnToFirstStep: PropTypes.func.isRequired,
};

const useStyles = makeStyles({
  mainContainer: {
    display: 'flex',
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    padding: '3% 2% 8% 3%',
    overflow: 'auto',
  },
  stepperContainer: {
    display: 'flex',
    flex: 0.7,
    backgroundColor: 'lightgreen',
    width: '100%',
  },
  headerContainer: {
    display: 'flex',
    width: '100%',
  },
  tableContainer: {
    display: 'flex',
    flex: 1,
    width: '100%',
    flexDirection: 'column',
  },
  tableTitle: {
    textAlign: 'left',
    fontSize: '1.5rem',
    fontWeight: 'bold',
    color: '#263238',
    margin: '1% 0%',
  },
  tableContentContainer: {
    display: 'flex',
    flex: 1,
    width: '100%',
    flexDirection: 'column',
  },
});

export default PILALayout;
