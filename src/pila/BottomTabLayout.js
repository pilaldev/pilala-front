import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { ButtonBase, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import QueryBuilderTwoToneIcon from '@material-ui/icons/QueryBuilderTwoTone';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import {
  STEPS,
  STEP_TEXT,
  SAVE_BUTTON,
  STEP_ONE_TEXT,
  RETURN_BUTTON,
  HOURS,
  STEP_TWO_TEXT,
} from './Constants';

const BottomTabLayout = (props) => {
  const bottomTabClasses = bottomTabStyles();
  const currentStep = useSelector((state) => state.payPila.currentStep);
  const featureFlags = useSelector((state) => state.currentUser.featureFlags);

  return (
    <div className={bottomTabClasses.container}>
      <div className={bottomTabClasses.footerContainer}>
        <FooterContent
          step={currentStep}
          returnToFirstStep={props.returnToFirstStep}
        />
      </div>
      <div className={bottomTabClasses.stepContainer}>
        <Typography className={bottomTabClasses.stepText}>
          {`${STEP_TEXT} ${currentStep}/${STEPS}`}
        </Typography>
      </div>
      {currentStep === 1 ? (
        <div>
          <ButtonBase
            className={bottomTabClasses.saveButton}
            onClick={props.saveAndContinueAction}
            disabled={!featureFlags.ccPayroll}
            classes={{
              disabled: bottomTabClasses.disabled,
            }}
          >
            <Typography className={bottomTabClasses.saveButtonText}>
              {SAVE_BUTTON}
            </Typography>
          </ButtonBase>
        </div>
      ) : null}
    </div>
  );
};

const FooterContent = (props) => {
  const footerClasses = footerStyles();

  switch (props.step) {
    case 1:
      return (
        <div className={footerClasses.stepOneContainer}>
          <Typography className={footerClasses.footerMainTextColor}>
            {STEP_ONE_TEXT}
          </Typography>
        </div>
      );

    case 2:
      return (
        <div className={footerClasses.stepTwoContainer}>
          <ButtonBase
            className={footerClasses.returnButton}
            onClick={props.returnToFirstStep}
          >
            <ArrowBackRoundedIcon
              className={footerClasses.footerMainTextColor}
            />
            <Typography className={footerClasses.footerMainTextColor}>
              {RETURN_BUTTON}
            </Typography>
          </ButtonBase>
          <div className={footerClasses.stepTwoInfoContainer}>
            <QueryBuilderTwoToneIcon className={footerClasses.clockIcon} />
            <div className={footerClasses.textInfoContainer}>
              <div className={footerClasses.hoursContainer}>
                <Typography className={footerClasses.hourText}>
                  {HOURS.FROM}
                </Typography>
                <ArrowForwardRoundedIcon className={footerClasses.hourArrow} />
                <Typography className={footerClasses.hourText}>
                  {HOURS.TO}
                </Typography>
              </div>
              <Typography className={footerClasses.mainTextInfo}>
                {STEP_TWO_TEXT}
              </Typography>
            </div>
          </div>
        </div>
      );

    default:
      return (
        <div className={footerClasses.stepOneContainer}>
          <Typography className={footerClasses.footerMainTextColor}>
            {STEP_ONE_TEXT}
          </Typography>
        </div>
      );
  }
};

BottomTabLayout.propTypes = {
  saveAndContinueAction: PropTypes.func.isRequired,
  returnToFirstStep: PropTypes.func.isRequired,
};

const footerStyles = makeStyles({
  stepOneContainer: {
    display: 'flex',
    flex: 1,
    paddingLeft: 30,
  },
  footerMainTextColor: {
    color: 'white',
  },
  stepTwoContainer: {
    display: 'flex',
    flex: 1,
    paddingLeft: 30,
    height: '100%',
  },
  returnButton: {
    display: 'flex',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    paddingLeft: 15,
    paddingRight: 15,
  },
  stepTwoInfoContainer: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  clockIcon: {
    fontSize: '2.5em',
    color: 'white',
  },
  textInfoContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'center',
    paddingLeft: 15,
  },
  hoursContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  hourText: {
    color: 'white',
    fontSize: '1em',
  },
  hourArrow: {
    color: 'white',
    margin: '0px 5px 0px 5px',
    fontSize: '1.3em',
  },
  mainTextInfo: {
    color: 'white',
    textAlign: 'left',
    fontSize: '0.8em',
  },
});

const bottomTabStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    paddingLeft: 220,
    backgroundColor: theme.palette.primary[600],
    height: '8%',
    position: 'fixed',
    bottom: 0,
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  footerContainer: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
  },
  stepContainer: {
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(4),
  },
  stepText: {
    color: 'white',
    fontSize: '0.9em',
  },
  saveButtonIcon: {
    color: 'white',
    margin: '0px 10px 0px 10px',
  },
  saveButton: {
    borderRadius: 10,
    backgroundColor: theme.palette.secondary.main,
    textTransform: 'none',
    height: 50,
    width: 190,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    boxShadow: 'none',
    margin: theme.spacing(0, 2, 0, 0),
  },
  saveButtonText: {
    color: 'white',
    fontSize: '1.3em',
    fontWeight: 'bold',
  },
  disabled: {
    backgroundColor: theme.palette.grey.main,
  },
}));

export default BottomTabLayout;
