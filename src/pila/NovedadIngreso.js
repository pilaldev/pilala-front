import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import 'moment/locale/es';
import {
  novedadIngresoAction,
  prePlanillaAction,
  snackBarAction,
} from '../redux/Actions';
import { INGRESO, PRE_PLANILLA, SNACKBAR } from '../redux/ActionTypes';
import { editRecordField } from '../constants/CotizantesFields';
import ValidateDates from '../utils/ValidateDates';
import { ModalNovedad } from '.';
import { Calendar } from '../components';
import { ButtonBase, Typography } from '@material-ui/core';
import { INGRESO_DATE_TEXT, INGRESO_NOVEDAD_NAME } from './Constants';
import DateRangeIcon from '@material-ui/icons/DateRange';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { useDispatch, useSelector } from 'react-redux';
import { CreateIngreso, RemoveNovedad } from './api';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
moment.locale('es');

export default () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [joinDateAnchor, setJoinDateAnchor] = useState({});
  const [showDatePicker, setShowDatePicker] = useState(false);
  const openIngresoModal = useSelector(
    (state) => state.novedadIngreso.openIngresoModal
  );
  const cotizanteName = useSelector(
    (state) => state.novedadIngreso.cotizanteName
  );
  const dateEntry = useSelector((state) => state.novedadIngreso.dateEntry);
  const newEntry = useSelector((state) => state.novedadIngreso.newEntry);
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const periodoActual = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const [disableSaveButton, setDisableSaveButton] = useState(false);
  const [disableButtons, setDisableButtons] = useState(false);
  const [disableRemoveButton, setDisableRemoveButton] = useState(false);
  const prePlanilla = useSelector((state) => state.prePlanilla);
  const periodosInfo = useSelector((state) => state.currentUser.periodosInfo);
  const cotizantesInfo = useSelector((state) => state.cotizantesInfo);
  const novedadIngreso = useSelector((state) => state.novedadIngreso);
  const adminName = useSelector((state) => state.currentUser.name);
  const adminEmail = useSelector((state) => state.currentUser.email);
  const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);

  //TODO: Verificar nombre de compañia cuando se activen sucursales

  const openDatePicker = (event) => {
    setJoinDateAnchor(event.currentTarget);
    setShowDatePicker(!showDatePicker);
  };

  const getJoinDate = (data) => {
    let cotizanteData =
      prePlanilla[activeCompany][periodosInfo[activeCompany].value][
        novedadIngreso.tipoCotizante
      ][periodosInfo[activeCompany].month][
        novedadIngreso.cotizanteDocumentNumber
      ];

    let dateValidation = ValidateDates(
      cotizanteData,
      data,
      true,
      false,
      null,
      null,
      null,
      null,
      null
    );

    if (dateValidation.result) {
      const value = {
        dateEntry: moment(data).format('DD/MM/YYYY'),
        newEntry: true,
        value: moment(data).format('DD/MM/YYYY'),
        regLine: {
          15: editRecordField[15]('X'),
          80: editRecordField[80](moment(data).format('YYYY-MM-DD')),
        },
      };
      dispatch(
        novedadIngresoAction(INGRESO.INGRESO_UPDATE_NOVEDAD_FIELDS, value)
      );
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error al validar fechas',
          description: dateValidation.message,
          color: 'error',
        })
      );
    }
  };

  const removeJoinDate = async () => {
    if (disableRemoveButton) return;
    setDisableRemoveButton(true);
    setDisableButtons(true);

    if (novedadIngreso.idNovedad !== '') {
      const ingresoDeleteData = {
        idAportante: activeCompany,
        cotizanteData: {
          idCotizante: novedadIngreso.idCotizante,
          documentNumber: novedadIngreso.cotizanteDocumentNumber,
          companyName: companiesInfo[activeCompany].companyName,
          idNew: novedadIngreso.idNovedad,
          province: companiesInfo[activeCompany].location.province.value,
          city: companiesInfo[activeCompany].location.city.value,
          tipoCotizante:
            cotizantesInfo[activeCompany][
              novedadIngreso.cotizanteDocumentNumber
            ].tipoCotizante.value,
          type: 'Ingresos',
        },
      };
      const removeResult = await RemoveNovedad(ingresoDeleteData);
      if (removeResult.status === 'SUCCESS') {
        const cotizante_fields = {
          companyNit: activeCompany,
          periodoActual: periodosInfo[activeCompany].value,
          idPlanilla: periodosInfo[activeCompany].month,
          identificationNumber: novedadIngreso.cotizanteDocumentNumber,
          cotizanteFields: {
            ...removeResult.data.result,
          },
        };
        const novedad_fields = {
          regLine: {
            15: editRecordField[15](''),
            80: editRecordField[80](''),
          },
        };

        dispatch(
          novedadIngresoAction(INGRESO.INGRESO_DELETE_NOVEDAD, novedad_fields)
        );
        dispatch(
          prePlanillaAction(
            PRE_PLANILLA.PRE_PLANILLA_UPDATE_COTIZANTE_FIELDS,
            cotizante_fields
          )
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
      } else if (removeResult.status === 'FAILED') {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al eliminar novedad',
            description:
              'Hemos tenido problemas para eliminar la novedad de ingreso, inténtelo nuevamente',
            color: 'error',
          })
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
      }
    } else {
      const value = {
        regLine: {
          15: editRecordField[15](''),
          80: editRecordField[80](''),
        },
      };
      dispatch(novedadIngresoAction(INGRESO.INGRESO_DELETE_NOVEDAD, value));
      setDisableRemoveButton(false);
      setDisableButtons(false);
    }
  };

  const selectJoinDate = (data) => {
    getJoinDate(data);
    setShowDatePicker(!showDatePicker);
  };

  const onClose = () => {
    if (openIngresoModal) {
      dispatch(novedadIngresoAction(INGRESO.INGRESO_OPEN_INGRESO_MODAL, null));
    }
    dispatch(novedadIngresoAction(INGRESO.INGRESO_RESET_NOVEDAD_FIELDS, null));
  };

  const onSave = async () => {
    if (disableSaveButton) return;
    setDisableSaveButton(true);
    setDisableButtons(true);

    if (novedadIngreso.dateEntry !== '') {
      if (novedadIngreso.activeChanges) {
        const newEntryData = {
          tipoCotizante:
            cotizantesInfo[activeCompany][
              novedadIngreso.cotizanteDocumentNumber
            ].tipoCotizante,
          location:
            cotizantesInfo[activeCompany][
              novedadIngreso.cotizanteDocumentNumber
            ].location,
          adminPlatform: {
            displayName: adminName,
            email: adminEmail,
          },
          idCotizante: novedadIngreso.idCotizante,
          idAportante: activeCompany,
          cotizanteData: {
            documentNumber: novedadIngreso.cotizanteDocumentNumber,
            companyName: companiesInfo[activeCompany].companyName,
            newEntry: novedadIngreso.newEntry,
            dateEntry: novedadIngreso.dateEntry,
            regLine: novedadIngreso.regLine,
          },
          update: novedadIngreso.idNovedad !== '',
        };
        const ingresoResult = await CreateIngreso(newEntryData);
        if (ingresoResult.status === 'SUCCESS') {
          const temp_basicas = JSON.parse(
            JSON.stringify(novedadIngreso.basicas)
          );
          if (temp_basicas.some((item) => item.type === 'Ingresos')) {
            temp_basicas.forEach((item, index) => {
              if (item.type === 'Ingresos') {
                temp_basicas[index] = ingresoResult.data.result;
              }
            });
          }
          const cotizante_fields = {
            companyNit: activeCompany,
            periodoActual: periodosInfo[activeCompany].value,
            idPlanilla: periodosInfo[activeCompany].month,
            identificationNumber: novedadIngreso.cotizanteDocumentNumber,
            cotizanteFields: {
              newEntry: novedadIngreso.newEntry,
              dateEntry: novedadIngreso.dateEntry,
              basicas: temp_basicas,
            },
          };

          dispatch(
            prePlanillaAction(
              PRE_PLANILLA.PRE_PLANILLA_UPDATE_COTIZANTE_FIELDS,
              cotizante_fields
            )
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
          onClose();
        } else if (ingresoResult.status === 'FAILED') {
          console.log(
            'Hubo un error al crear la novedad de ingreso, inténtelo nuevamente.'
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'No se detectaron cambios',
            description:
              'Para actualiar la novedad la fecha de ingreso debe ser diferente',
            color: 'info',
          })
        );
        setDisableSaveButton(false);
        setDisableButtons(false);
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description: 'Por favor seleccione la fecha de ingreso del cotizante',
          color: 'warning',
        })
      );
      setDisableSaveButton(false);
      setDisableButtons(false);
    }
  };

  return (
    <ModalNovedad
      open={openIngresoModal}
      title="Reportar ingreso"
      employeeName={cotizanteName}
      onClose={onClose}
      onCancel={onClose}
      onSave={onSave}
      disableSaveButton={disableSaveButton}
      disableButtons={disableButtons}
    >
      <div className={classes.novedadContainer}>
        <Typography className={classes.novedadName} variant="subtitle1">
          {INGRESO_NOVEDAD_NAME}
        </Typography>
        <div className={classes.dateContent}>
          <div className={classes.dateTitleContainer}>
            <Typography className={classes.dateTitle} variant="subtitle1">
              {INGRESO_DATE_TEXT}
            </Typography>
          </div>
          <div className={classes.dateContainer}>
            <IconButton filled={false} size="small" onClick={openDatePicker}>
              <DateRangeIcon />
            </IconButton>
            <ButtonBase onClick={openDatePicker} className={classes.dateField}>
              <Typography className={classes.dateText} variant="subtitle2">
                {dateEntry}
              </Typography>
            </ButtonBase>
            <div className={classes.removeIconContainer}>
              {newEntry && (
                <IconButton
                  buttoncolor="error"
                  filled={false}
                  size="small"
                  loading={disableRemoveButton}
                  disabled={disableButtons}
                  onClick={removeJoinDate}
                >
                  <RemoveCircleOutlineIcon />
                </IconButton>
              )}
            </div>
            <Calendar
              open={showDatePicker}
              onChange={selectJoinDate}
              anchorElement={joinDateAnchor}
              focusDate={periodoActual.display}
              color="primary"
            />
          </div>
        </div>
      </div>
    </ModalNovedad>
  );
};

const useStyles = makeStyles((theme) => ({
  novedadContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(8),
  },
  novedadName: {
    color: theme.palette.primary.main,
  },
  dateContent: {
    backgroundColor: theme.palette.grey[200],
    width: '100%',
    borderTop: '1px solid',
    borderTopColor: theme.palette.primary[400],
    display: 'flex',
    padding: theme.spacing(3, 3, 5, 2),
  },
  dateTitleContainer: {
    flex: 1,
    alignItems: 'center',
    display: 'flex',
  },
  dateTitle: {
    color: theme.palette.common.black,
  },
  dateContainer: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  dateField: {
    border: '1px solid',
    borderColor: theme.palette.primary.main,
    borderRadius: 8,
    width: '45%',
    height: 30,
    margin: theme.spacing(0, 1),
    display: 'flex',
    alignItems: 'center',
  },
  dateText: {
    color: theme.palette.common.black,
  },
  removeSpinner: {
    color: theme.palette.primary.main,
  },
  removeIconContainer: {
    width: 30,
    height: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
