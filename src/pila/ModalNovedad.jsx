import React from 'react';
import { Typography, Grid } from '@material-ui/core';

import Button from '@bit/pilala.pilalalib.components.button';
import CardModal from '@bit/pilala.pilalalib.components.card-modal';
import { useModalNovedadStyles } from './styles';

export default ({
  open,
  title,
  children,
  onCancel,
  onSave,
  onClose,
  disableSaveButton,
  disableButtons,
  employeeName,
}) => {
  const classes = useModalNovedadStyles();

  return (
    <CardModal
      open={open}
      title={title}
      onClose={onClose}
      fullWidth
      classes={{
        paperWidthSm: classes.paperWidthSm,
      }}
    >
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="center"
        className={classes.rowOne}
      >
        <Typography className={classes.employeeText} variant="subtitle1">
          {'Empleado: '}
        </Typography>
        <Typography className={classes.employeeName} variant="subtitle1">
          {employeeName}
        </Typography>
      </Grid>
      {children}
      <Grid container direction="row" justify="flex-end" alignItems="center">
        <div className={classes.cancelButtonContainer}>
          <Button
            onClick={onCancel}
            disabled={disableSaveButton || disableButtons}
            variant="outlined"
          >
            Cancelar
          </Button>
        </div>
        <Button
          onClick={onSave}
          disabled={disableButtons}
          loading={disableSaveButton}
        >
          Guardar
        </Button>
      </Grid>
    </CardModal>
  );
};
