import React from 'react';
import PropTypes from 'prop-types';
import { Dialog, Grid, Typography, CircularProgress } from '@material-ui/core';
import CheckCircleOutlineRoundedIcon from '@material-ui/icons/CheckCircleOutlineRounded';
import { makeStyles } from '@material-ui/core/styles';
import { VALIDATE_STEPS } from './Constants';
import { useSelector } from 'react-redux';

const ValidationLayout = (props) => {
  const classes = useStyles();
  const areCredentialsEnabled = useSelector(
    (state) => state.payPila.areCredentialsEnabled
  );

  return (
    <Dialog
      fullScreen={true}
      open={props.validatePila}
      className={classes.container}
      PaperProps={{
        className: classes.paperContainer,
      }}
      BackdropProps={{
        className: classes.validationBackdrop,
      }}
    >
      <Grid container direction="column" justify="center" alignItems="center">
        <div className={classes.contentContainer}>
          <div className={classes.stepContainer}>
            <div className={classes.stepIconContainer}>
              {props.checkStepStatus ? (
                <CheckCircleOutlineRoundedIcon className={classes.stepIcon} />
              ) : (
                <CircularProgress size={30} className={classes.stepIcon} />
              )}
            </div>
            <Typography className={classes.stepText}>
              {VALIDATE_STEPS.CHECK_DATA}
            </Typography>
          </div>
          <div className={classes.stepContainer}>
            <div className={classes.stepIconContainer}>
              {props.createStepStatus ? (
                <CheckCircleOutlineRoundedIcon className={classes.stepIcon} />
              ) : (
                <CircularProgress size={30} className={classes.stepIcon} />
              )}
            </div>
            <Typography className={classes.stepText}>
              {VALIDATE_STEPS.CREATE_PILA}
            </Typography>
          </div>
          {areCredentialsEnabled && (
            <div className={classes.stepContainer}>
              <div className={classes.stepIconContainer}>
                {props.validateStepStatus ? (
                  <CheckCircleOutlineRoundedIcon className={classes.stepIcon} />
                ) : (
                  <CircularProgress size={30} className={classes.stepIcon} />
                )}
              </div>
              <Typography className={classes.stepText}>
                {VALIDATE_STEPS.VALIDATE_PILA}
              </Typography>
            </div>
          )}
        </div>
      </Grid>
    </Dialog>
  );
};

ValidationLayout.propTypes = {
  validatePila: PropTypes.bool.isRequired,
};

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.primary[900],
  },
  paperContainer: {
    backgroundColor: 'transparent',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
  stepContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '10px 0px 10px 0px',
  },
  stepIcon: {
    fontSize: '2.5em',
    color: 'white',
  },
  stepText: {
    color: 'white',
    fontSize: '1.5em',
  },
  validationBackdrop: {
    backgroundColor: 'transparent',
  },
  stepIconContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 45,
    height: 45,
    marginRight: 10,
  },
}));

export default ValidationLayout;
