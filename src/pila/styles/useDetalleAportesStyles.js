import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  infoBox: {
    display: 'flex',
    flexDirection: 'column',
  },
  infoBoxPadding: {
    paddingRight: theme.spacing(12),
  },
  infoRow: {
    marginTop: theme.spacing(5),
  },
  subtitle: {
    fontWeight: 'bold',
    color: theme.palette.common.black,
  },
  detailRow: {
    marginTop: theme.spacing(3),
  },
  dividerRoot: {
    marginTop: theme.spacing(1),
    backgroundColor: theme.palette.grey[400],
  },
  totalContainer: {
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(3),
  },
  totalTitle: {
    fontSize: 12,
    color: theme.palette.grey.main,
  },
  total: {
    color: theme.palette.primary.main,
    fontWeight: 'bold',
  },
  totalSubcontainer: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: theme.spacing(2),
  },
  totalRow: {
    marginTop: theme.spacing(2),
  },
  fspBox: {
    position: 'relative',
    paddingRight: theme.spacing(3),
  },
  infoIcon: {
    color: theme.palette.info.main,
    position: 'absolute',
    top: '0.2188rem',
    right: 0,
    fontSize: '1.2rem',
  },
  tooltipInfo: {
    backgroundColor: theme.palette.common.black,
    fontSize: '0.8rem',
  },
  tooltipInfoArrow: {
    color: theme.palette.common.black,
  },
  cardContainer: {
    borderRadius: 10,
    boxShadow: 'none',
    padding: theme.spacing(4),
  },
}));
