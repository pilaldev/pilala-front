import { makeStyles } from '@material-ui/core/styles';

const useSummaryStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  headerContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerTitle: {
    fontWeight: 'bold',
    color: theme.palette.common.black,
    textAlign: 'left',
  },
  contentContainer: {
    display: 'flex',
    alignItems: 'center',
    flex: 1,
  },
  sumContainer: {
    display: 'flex',
    flex: 1,
    height: '100%',
    flexDirection: 'column',
  },
  checkoutImageContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'center',
    width: '35%',
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  checkoutImage: {
    width: '55%',
    height: 'auto',
    alignSelf: 'center',
  },
  downloadIcon: {
    color: '#757AFF',
    fontSize: '1.5em',
    marginRight: 5,
  },
  downloadText: {
    color: '#757AFF',
    fontSize: '1em',
  },
  downloadButton: {
    borderRadius: 100,
  },
  planillasContainer: {
    height: '75%',
    width: '80%',
    paddingTop: '5%',
    overflow: 'auto',
  },
  planillaItem: {
    width: '100%',
    height: '12.5%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  planillaAportante: {
    fontWeight: 'bold',
    color: '#455A64',
    fontSize: '0.9rem',
  },
  planillaNumber: {
    color: '#455A64',
    fontSize: '0.9rem',
  },
  planillaPeriod: {
    fontWeight: 'bold',
    color: '#455A64',
    fontSize: '0.9rem',
    textTransform: 'capitalize',
  },
  planillaValue: {
    color: '#455A64',
    fontSize: '0.9rem',
  },
  payButton: {
    backgroundColor: '#6A32B526',
    borderRadius: 100,
    border: '2px solid #6A32B5',
    padding: '0.25em 0px 0.25em 0px',
  },
  payButtonText: {
    fontSize: '0.9rem',
    color: '#6A32B5',
    padding: '0px 1.5em',
  },
  payButtonSpinner: {
    color: '#6A32B5',
    margin: '0.1em 2.4em',
  },
  sendPayButtonText: {
    fontSize: '1.5rem',
    color: '#2962FF',
  },
  paperContainer: {
    backgroundColor: '#FFFFFF',
    display: 'flex',
    flex: 1,
    padding: '2em',
  },
  sendButtonTitle: {
    color: '#5E35B1',
    fontWeight: 'bold',
    fontSize: '1.1rem',
  },
  sendButtonInput: {
    width: '100%',
    borderRadius: 7,
    marginTop: '5%',
    marginBottom: '5%',
  },
  inputRoot: {
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#707070',
      },
      '&:hover fieldset': {
        borderColor: '#5E35B1',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#5E35B1',
      },
    },
  },
  sendButtonPlanillaItem: {
    backgroundColor: '#ECEFF1',
    borderRadius: 7,
    padding: '0.4em 0.8em',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  messageCheckbox: {
    color: '#4CAF50',
    fontSize: '1em',
  },
  messageCheckedIcon: {
    fontSize: '1em',
    color: '#4CAF50',
  },
  messageText: {
    color: '#95989A',
    fontSize: '0.9rem',
    paddingRight: '10%',
  },
  messageContainer: {
    display: 'flex',
    marginTop: '3%',
  },
  cancelButtonText: {
    color: '#5E35B1',
  },
  cancelButton: {
    marginRight: 10,
    borderRadius: 100,
    textTransform: 'none',
    padding: '0.4em 1em',
  },
  saveButton: {
    background:
      'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
    borderRadius: 22,
    marginLeft: 10,
    textTransform: 'none',
    display: 'flex',
    alignItems: 'center',
    padding: '0.4em 1.5em',
  },
  saveButtonText: {
    color: 'white',
    fontWeight: 'bold',
    marginRight: '1em',
  },
  buttonsContainer: {
    display: 'flex',
    alignItems: 'center',
    marginTop: '5%',
    justifyContent: 'flex-end',
  },
  arrowIcon: {
    color: '#FFFFFF',
  },
  controlPanelButton: {
    border: '1px solid #5E35B1',
    padding: '0.5em 2em',
    borderRadius: 100,
  },
  controlPanelButtonText: {
    fontSize: '1rem',
    color: '#5E35B1',
    fontWeight: 'bold',
  },
  controlPanelButtonContainer: {
    display: 'flex',
    width: '80%',
    justifyContent: 'flex-end',
    paddingTop: '3.5%',
    backgroundColor: 'yellow',
  },
  linkTooltip: {
    backgroundColor: '#000000',
  },
  linkTooltipArrow: {
    color: '#000000',
  },
  operatorActions: {
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(2),
  },
  actionContainer: {
    marginRight: theme.spacing(3),
  },
  logoBox: {
    width: '9rem',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(1.5, 2),
    border: '1px solid',
    borderColor: theme.palette.grey[400],
    borderRadius: 10,
    backgroundColor: theme.palette.common.white,
  },
  actionCard: {
    width: '20rem',
    minHeight: '12.5rem',
    borderRadius: 10,
    padding: theme.spacing(2),
    marginRight: theme.spacing(8),
  },
  actionCardColorOne: {
    backgroundColor: theme.palette.primary[200],
  },
  actionCardColorTwo: {
    backgroundColor: theme.palette.grey[200],
  },
  actionCardTitle: {
    textAlign: 'left',
    color: theme.palette.primary[900],
    fontWeight: 'bold',
  },
  actionCardMessage: {
    textAlign: 'left',
    color: theme.palette.primary[900],
  },
  actionCardFooter: {
    marginTop: theme.spacing(2),
  },
  actionCardLinkContainer: {
    marginTop: theme.spacing(2),
  },
  actionsContainer: {
    display: 'flex',
    alignItems: 'flex-start',
    margin: theme.spacing(2, 0, 4, 0),
  },
  backButtonContainer: {
    marginTop: theme.spacing(8),
  },
  cancelButtonContainer: {
    marginRight: theme.spacing(3),
  },
}));

export default useSummaryStyles;
