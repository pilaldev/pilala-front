export { default as useSummaryStyles } from './useSummaryStyles';
export { default as useDetalleAportesStyles } from './useDetalleAportesStyles';
export { default as useNovedadIncapacidadesStyles } from './useNovedadIncapacidadesStyles';
export { default as useModalNovedadStyles } from './useModalNovedadStyles';
