import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  paperWidthSm: {
    maxWidth: 700,
  },
  employeeText: {
    fontWeight: 'bold',
    color: theme.palette.common.black,
  },
  employeeName: {
    color: theme.palette.common.black,
    marginLeft: theme.spacing(1),
  },
  rowOne: {
    margin: theme.spacing(3, 0, 2, 0),
  },
  cancelButtonContainer: {
    marginRight: theme.spacing(2),
  },
  dialogTitle: {
    textAlign: 'left',
    fontWeight: 'bold',
    color: theme.palette.common.black,
  },
}));
