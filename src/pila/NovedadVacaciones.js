import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import 'moment/locale/es';
import { novedadVacacionesAction, snackBarAction } from '../redux/Actions';
import { SNACKBAR, VACACIONES } from '../redux/ActionTypes';
import { editRecordField } from '../constants/CotizantesFields';
import ValidateDates from '../utils/ValidateDates';
import { ModalNovedad } from '.';
import { Calendar } from '../components';
import { ButtonBase, Typography } from '@material-ui/core';
import {
  VACACIONES_ADD_ROW,
  VACACIONES_DAYS,
  VACACIONES_NOVEDAD_NAME,
} from './Constants';
import DateRangeIcon from '@material-ui/icons/DateRange';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { useDispatch, useSelector } from 'react-redux';
import { CreateVacaciones, RemoveNovedad } from './api';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Button from '@bit/pilala.pilalalib.components.button';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
moment.locale('es');

export default () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [initialDateAnchor, setInitialDateAnchor] = useState({});
  const [finalDateAnchor, setFinalDateAnchor] = useState({});
  const vacacionesRows = useSelector(
    (state) => state.novedadVacaciones.vacacionesRows
  );
  const openVacacionesModal = useSelector(
    (state) => state.novedadVacaciones.openVacacionesModal
  );
  const cotizanteName = useSelector(
    (state) => state.novedadVacaciones.cotizanteName
  );
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [showInitialDatePicker, setShowInitialDatePicker] = useState(false);
  const [showFinalDatePicker, setShowFinalDatePicker] = useState(false);
  const [removeIndex, setRemoveIndex] = useState(-1);
  const [disableSaveButton, setDisableSaveButton] = useState(false);
  const [disableButtons, setDisableButtons] = useState(false);
  const [disableRemoveButton, setDisableRemoveButton] = useState(false);
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const periodoActual = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const prePlanilla = useSelector((state) => state.prePlanilla);
  const periodosInfo = useSelector((state) => state.currentUser.periodosInfo);
  const cotizanteType = useSelector(
    (state) => state.novedadVacaciones.cotizanteType
  );
  const cotizanteIdentificationNumber = useSelector(
    (state) => state.novedadVacaciones.cotizanteIdentificationNumber
  );
  const activeChanges = useSelector(
    (state) => state.novedadVacaciones.activeChanges
  );
  const idCotizante = useSelector(
    (state) => state.novedadVacaciones.idCotizante
  );
  const cotizantesInfo = useSelector((state) => state.cotizantesInfo);
  const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);

  //TODO: Verificar nombre de compañia cuando se activen sucursales

  const openInitialDatePicker = (index, event) => {
    setSelectedIndex(index);
    setInitialDateAnchor(event.currentTarget);
    setShowInitialDatePicker(!showInitialDatePicker);
  };

  const openFinalDatePicker = (index, event) => {
    setSelectedIndex(index);
    setFinalDateAnchor(event.currentTarget);
    setShowFinalDatePicker(!showFinalDatePicker);
  };

  const removeVacacionesItem = async (index) => {
    if (disableRemoveButton) return;
    setDisableRemoveButton(true);
    setDisableButtons(true);
    setRemoveIndex(index);

    if (vacacionesRows[index].id) {
      const vacacionesDeleteData = {
        idAportante: activeCompany,
        cotizanteData: {
          idCotizante,
          documentNumber: cotizanteIdentificationNumber,
          companyName: companiesInfo[activeCompany].companyName,
          idNew: vacacionesRows[index].id,
          province: companiesInfo[activeCompany].location.province.value,
          city: companiesInfo[activeCompany].location.city.value,
          tipoCotizante: cotizanteType.value,
          type: vacacionesRows[index].type,
        },
      };
      const deleteResult = await RemoveNovedad(vacacionesDeleteData);
      if (deleteResult.status === 'SUCCESS') {
        dispatch(
          novedadVacacionesAction(
            VACACIONES.VACACIONES_DELETE_VACACIONES_ROW,
            index
          )
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
        setRemoveIndex(-1);
      } else if (deleteResult.status === 'FAILED') {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al eliminar novedad',
            description:
              'Hemos tenido problemas para eliminar la novedad de vacaciones, inténtelo nuevamente',
            color: 'error',
          })
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
        setRemoveIndex(-1);
      }
    } else {
      dispatch(
        novedadVacacionesAction(
          VACACIONES.VACACIONES_DELETE_VACACIONES_ROW,
          index
        )
      );
      setDisableRemoveButton(false);
      setDisableButtons(false);
      setRemoveIndex(-1);
    }
  };

  const onClose = () => {
    if (openVacacionesModal) {
      dispatch(
        novedadVacacionesAction(
          VACACIONES.VACACIONES_OPEN_VACACIONES_MODAL,
          null
        )
      );
    }
    dispatch(
      novedadVacacionesAction(VACACIONES.VACACIONES_RESET_NOVEDAD_FIELDS, null)
    );
  };

  const onSave = async () => {
    if (disableSaveButton) return;
    setDisableSaveButton(true);
    setDisableButtons(true);

    if (checkVacacionesDates()) {
      if (activeChanges) {
        const changedVacacionesRows = vacacionesRows.map((item) => ({
          ...item,
          dateStart: item.fechaInicio,
          dateEnd: item.fechaFin,
        }));
        const vacacionesFullData = {
          tipoCotizante: cotizanteType.value,
          location:
            cotizantesInfo[activeCompany][cotizanteIdentificationNumber]
              .location,
          idCompany: activeCompany,
          cotizanteData: {
            idCotizante,
            arrayNew: changedVacacionesRows,
          },
        };
        const method = changedVacacionesRows.some(
          (item) => item.id !== undefined
        )
          ? 'PATCH'
          : 'POST';
        const result = await CreateVacaciones(vacacionesFullData, method);
        if (result.status === 'SUCCESS') {
          dispatch(
            novedadVacacionesAction(
              VACACIONES.VACACIONES_OPEN_VACACIONES_MODAL,
              null
            )
          );
          dispatch(
            novedadVacacionesAction(
              VACACIONES.VACACIONES_RESET_NOVEDAD_FIELDS,
              null
            )
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        } else if (result.status === 'FAILED') {
          dispatch(
            snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear la novedad',
              description:
                'Hemos tenido problemas para crear la novedad de vacaciones, inténtelo nuevamente',
              color: 'error',
            })
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'No se detectaron cambios',
            description: 'La información sobre las vacaciones no ha cambiado',
            color: 'info',
          })
        );
        setDisableSaveButton(false);
        setDisableButtons(false);
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description:
            'Por favor complete las fechas del reporte de vacaciones.',
          color: 'warning',
        })
      );
      setDisableSaveButton(false);
      setDisableButtons(false);
    }
  };

  const addVacacionesRow = () => {
    if (checkVacacionesDates()) {
      dispatch(
        novedadVacacionesAction(VACACIONES.VACACIONES_ADD_VACACIONES_ROW, null)
      );
    }
  };

  const checkVacacionesDates = () => {
    return vacacionesRows.every(
      (item) => item.fechaInicio !== '' && item.fechaFin !== ''
    );
  };

  const getInitialDate = (index, data) => {
    const { fechaFin, id } = vacacionesRows[index];
    if (
      fechaFin === '' ||
      moment(fechaFin, 'DD/MM/YYYY').diff(moment(data, 'DD/MM/YYYY'), 'days') >
        0
    ) {
      const cotizanteData =
        prePlanilla[activeCompany][periodosInfo[activeCompany].value][
          cotizanteType.value
        ][periodosInfo[activeCompany].month][cotizanteIdentificationNumber];

      const dateValidation = ValidateDates(
        cotizanteData,
        data,
        false,
        false,
        index,
        id,
        true,
        false,
        vacacionesRows
      );

      if (dateValidation.result) {
        const value = {
          fields: {
            fechaInicio: moment(data).format('DD/MM/YYYY'),
            value: {
              fechaInicio: moment(data).format('DD/MM/YYYY'),
            },
            regLine: {
              27: editRecordField[27]('X'),
              89: editRecordField[89](moment(data).format('YYYY-MM-DD')),
            },
          },
          index,
        };
        dispatch(
          novedadVacacionesAction(
            VACACIONES.VACACIONES_SAVE_NOVEDAD_FIELDS,
            value
          )
        );
        if (!activeChanges) {
          dispatch(
            novedadVacacionesAction(
              VACACIONES.VACACIONES_ENABLE_ACTIVE_CHANGES,
              null
            )
          );
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al validar fechas',
            description: dateValidation.message,
            color: 'error',
          })
        );
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Fecha incorrecta',
          description: 'La fecha inicial debe ser menor que la fecha final',
          color: 'info',
        })
      );
    }
  };

  const getFinalDate = (index, data) => {
    const { fechaInicio, id } = vacacionesRows[index];
    if (
      fechaInicio === '' ||
      moment(data, 'DD/MM/YYYY').diff(
        moment(fechaInicio, 'DD/MM/YYYY'),
        'days'
      ) >= 0
    ) {
      const cotizanteData =
        prePlanilla[activeCompany][periodosInfo[activeCompany].value][
          cotizanteType.value
        ][periodosInfo[activeCompany].month][cotizanteIdentificationNumber];

      const dateValidation = ValidateDates(
        cotizanteData,
        data,
        false,
        false,
        index,
        id,
        false,
        true,
        vacacionesRows
      );

      if (dateValidation.result) {
        const value = {
          fields: {
            fechaFin: moment(data).format('DD/MM/YYYY'),
            value: {
              fechaFin: moment(data).format('DD/MM/YYYY'),
            },
            regLine: {
              27: editRecordField[27]('X'),
              90: editRecordField[90](moment(data).format('YYYY-MM-DD')),
            },
          },
          index,
        };
        dispatch(
          novedadVacacionesAction(
            VACACIONES.VACACIONES_SAVE_NOVEDAD_FIELDS,
            value
          )
        );
        if (!activeChanges) {
          dispatch(
            novedadVacacionesAction(
              VACACIONES.VACACIONES_ENABLE_ACTIVE_CHANGES,
              null
            )
          );
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al validar fechas',
            description: dateValidation.message,
            color: 'error',
          })
        );
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Fecha incorrecta',
          description: 'La fecha final debe ser mayor que la fecha inicial',
          color: 'info',
        })
      );
    }
  };

  const selectVacacionesInitialDate = (data) => {
    getInitialDate(selectedIndex, data);
    setShowInitialDatePicker(!showInitialDatePicker);
  };

  const selectVacacionesFinalDate = (data) => {
    getFinalDate(selectedIndex, data);
    setShowFinalDatePicker(!showFinalDatePicker);
  };

  return (
    <ModalNovedad
      open={openVacacionesModal}
      title="Reportar vacaciones"
      employeeName={cotizanteName}
      onClose={onClose}
      onCancel={onClose}
      onSave={onSave}
      disableSaveButton={disableSaveButton}
      disableButtons={disableButtons}
    >
      <div className={classes.novedadContainer}>
        <Typography className={classes.novedadName} variant="subtitle1">
          {VACACIONES_NOVEDAD_NAME}
        </Typography>
        <div className={classes.dateContent}>
          <div className={classes.dateTitleContainer}>
            <Typography className={classes.dateTitle} variant="subtitle1">
              {VACACIONES_DAYS}
            </Typography>
          </div>
          <div className={classes.dateContainer}>
            {vacacionesRows.map((item, index) => (
              <div className={classes.rangeContainer} key={index}>
                <IconButton
                  onClick={openInitialDatePicker.bind(this, index)}
                  filled={false}
                  size="small"
                >
                  <DateRangeIcon />
                </IconButton>
                <ButtonBase
                  onClick={openInitialDatePicker.bind(this, index)}
                  className={classes.dateField}
                >
                  <Typography className={classes.dateText} variant="subtitle2">
                    {item.fechaInicio}
                  </Typography>
                </ButtonBase>
                <ArrowForwardIcon className={classes.icon} />
                <IconButton
                  onClick={openFinalDatePicker.bind(this, index)}
                  filled={false}
                  size="small"
                >
                  <DateRangeIcon />
                </IconButton>
                <ButtonBase
                  onClick={openFinalDatePicker.bind(this, index)}
                  className={classes.dateField}
                >
                  <Typography className={classes.dateText} variant="subtitle2">
                    {item.fechaFin}
                  </Typography>
                </ButtonBase>
                <div className={classes.removeIconContainer}>
                  {(item.fechaInicio !== '' ||
                    item.fechaFin !== '' ||
                    index > 0) && (
                    <IconButton
                      buttoncolor="error"
                      filled={false}
                      size="small"
                      loading={disableRemoveButton && removeIndex === index}
                      disabled={disableButtons}
                      onClick={removeVacacionesItem.bind(this, index)}
                    >
                      <RemoveCircleOutlineIcon />
                    </IconButton>
                  )}
                </div>
              </div>
            ))}
            <div className={classes.addRow}>
              <Button
                startIcon={<AddCircleOutlineIcon />}
                variant="text"
                onClick={addVacacionesRow}
              >
                {VACACIONES_ADD_ROW}
              </Button>
            </div>
            <Calendar
              open={showInitialDatePicker}
              onChange={selectVacacionesInitialDate}
              anchorElement={initialDateAnchor}
              focusDate={periodoActual.display}
              color="primary"
            />
            <Calendar
              open={showFinalDatePicker}
              onChange={selectVacacionesFinalDate}
              anchorElement={finalDateAnchor}
              focusDate={periodoActual.display}
              color="primary"
            />
          </div>
        </div>
      </div>
    </ModalNovedad>
  );
};

const useStyles = makeStyles((theme) => ({
  novedadContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(8),
  },
  novedadName: {
    color: theme.palette.primary.main,
  },
  dateContent: {
    backgroundColor: theme.palette.grey[200],
    width: '100%',
    borderTop: '1px solid',
    borderTopColor: theme.palette.primary[400],
    display: 'flex',
    padding: theme.spacing(3, 3, 5, 2),
  },
  dateTitleContainer: {
    flex: 1,
    display: 'flex',
  },
  dateTitle: {
    color: theme.palette.common.black,
  },
  dateContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    overflow: 'auto',
    maxHeight: 185,
    padding: '0px 1px',
  },
  dateField: {
    border: '1px solid',
    borderColor: theme.palette.primary.main,
    borderRadius: 8,
    width: 90,
    height: 30,
    margin: theme.spacing(0, 1),
    display: 'flex',
    alignItems: 'center',
  },
  dateText: {
    color: theme.palette.common.black,
  },
  removeSpinner: {
    color: theme.palette.primary.main,
  },
  removeIconContainer: {
    width: 30,
    height: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    color: theme.palette.primary.main,
    fontSize: '1.5rem',
    margin: theme.spacing(0, 1),
  },
  rangeContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 10,
  },
  addRow: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingRight: theme.spacing(3.7),
  },
}));
