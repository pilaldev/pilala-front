import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import 'moment/locale/es';
import { RETIRO, PRE_PLANILLA, SNACKBAR } from '../redux/ActionTypes';
import {
  novedadRetiroAction,
  prePlanillaAction,
  snackBarAction,
} from '../redux/Actions';
import { editRecordField } from '../constants/CotizantesFields';
import ValidateDates from '../utils/ValidateDates';
import { ModalNovedad } from '.';
import { ButtonBase, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { RETIRO_DATE_TEXT, RETIRO_NOVEDAD_NAME } from './Constants';
import { Calendar } from '../components';
import DateRangeIcon from '@material-ui/icons/DateRange';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { CreateRetiro, RemoveNovedad } from './api';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
moment.locale('es');

export default () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [retirementDateAnchor, setRetirementDateAnchor] = useState({});
  const [showDatePicker, setShowDatePicker] = useState(false);
  const openRetiroModal = useSelector(
    (state) => state.novedadRetiro.openRetiroModal
  );
  const cotizanteName = useSelector(
    (state) => state.novedadRetiro.cotizanteName
  );
  const dateRetirement = useSelector(
    (state) => state.novedadRetiro.dateRetirement
  );
  const retired = useSelector((state) => state.novedadRetiro.retired);
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const periodoActual = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const [disableSaveButton, setDisableSaveButton] = useState(false);
  const [disableButtons, setDisableButtons] = useState(false);
  const [disableRemoveButton, setDisableRemoveButton] = useState(false);
  const prePlanilla = useSelector((state) => state.prePlanilla);
  const periodosInfo = useSelector((state) => state.currentUser.periodosInfo);
  const cotizantesInfo = useSelector((state) => state.cotizantesInfo);
  const novedadRetiro = useSelector((state) => state.novedadRetiro);
  const adminName = useSelector((state) => state.currentUser.name);
  const adminEmail = useSelector((state) => state.currentUser.email);
  const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);

  const openDatePicker = (event) => {
    setRetirementDateAnchor(event.currentTarget);
    setShowDatePicker(!showDatePicker);
  };

  const selectRetirementDate = (data) => {
    getRetirementDate(data);
    setShowDatePicker(!showDatePicker);
  };

  const getRetirementDate = (data) => {
    let cotizanteData =
      prePlanilla[activeCompany][periodosInfo[activeCompany].value][
        novedadRetiro.tipoCotizante
      ][periodosInfo[activeCompany].month][
        novedadRetiro.cotizanteDocumentNumber
      ];

    let dateValidation = ValidateDates(
      cotizanteData,
      data,
      false,
      true,
      null,
      null,
      null,
      null,
      null
    );

    if (dateValidation.result) {
      const value = {
        dateRetirement: moment(data).format('DD/MM/YYYY'),
        retired: true,
        value: moment(data).format('DD/MM/YYYY'),
        regLine: {
          16: editRecordField[16]('X'),
          81: editRecordField[81](moment(data).format('YYYY-MM-DD')),
        },
      };
      dispatch(novedadRetiroAction(RETIRO.RETIRO_UPDATE_NOVEDAD_FIELDS, value));
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error al validar fechas',
          description: dateValidation.message,
          color: 'error',
        })
      );
    }
  };

  const removeRetirementDate = async () => {
    if (disableRemoveButton) return;
    setDisableRemoveButton(true);
    setDisableButtons(true);

    if (novedadRetiro.idNovedad !== '') {
      const retiroDeleteData = {
        idAportante: activeCompany,
        cotizanteData: {
          idCotizante: novedadRetiro.idCotizante,
          documentNumber: novedadRetiro.cotizanteDocumentNumber,
          companyName: companiesInfo[activeCompany].companyName,
          idNew: novedadRetiro.idNovedad,
          province: companiesInfo[activeCompany].location.province.value,
          city: companiesInfo[activeCompany].location.city.value,
          tipoCotizante:
            cotizantesInfo[activeCompany][novedadRetiro.cotizanteDocumentNumber]
              .tipoCotizante.value,
          type: 'Retiros',
        },
      };
      const removeResult = await RemoveNovedad(retiroDeleteData);
      if (removeResult.status === 'SUCCESS') {
        const cotizante_fields = {
          companyNit: activeCompany,
          periodoActual: periodosInfo[activeCompany].value,
          idPlanilla: periodosInfo[activeCompany].month,
          identificationNumber: novedadRetiro.cotizanteDocumentNumber,
          cotizanteFields: {
            ...removeResult.data.result,
          },
        };
        const novedad_fields = {
          regLine: {
            16: editRecordField[16](''),
            81: editRecordField[81](''),
          },
        };

        dispatch(
          novedadRetiroAction(RETIRO.RETIRO_DELETE_NOVEDAD, novedad_fields)
        );
        dispatch(
          prePlanillaAction(
            PRE_PLANILLA.PRE_PLANILLA_UPDATE_COTIZANTE_FIELDS,
            cotizante_fields
          )
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
      } else if (removeResult.status === 'FAILED') {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al eliminar novedad',
            description:
              'Hemos tenido problemas para eliminar la novedad de retiro, inténtelo nuevamente',
            color: 'error',
          })
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
      }
    } else {
      const value = {
        regLine: {
          16: editRecordField[16](''),
          81: editRecordField[81](''),
        },
      };
      dispatch(novedadRetiroAction(RETIRO.RETIRO_DELETE_NOVEDAD, value));
      setDisableRemoveButton(false);
      setDisableButtons(false);
    }
  };

  const onClose = () => {
    if (openRetiroModal) {
      dispatch(novedadRetiroAction(RETIRO.RETIRO_OPEN_RETIRO_MODAL, null));
    }
    dispatch(novedadRetiroAction(RETIRO.RETIRO_RESET_NOVEDAD_FIELDS, null));
  };

  const onSave = async () => {
    if (disableSaveButton) return;
    setDisableSaveButton(true);
    setDisableButtons(true);

    if (novedadRetiro.dateRetirement !== '') {
      if (novedadRetiro.activeChanges) {
        const newRetiredData = {
          tipoCotizante:
            cotizantesInfo[activeCompany][novedadRetiro.cotizanteDocumentNumber]
              .tipoCotizante,
          location:
            cotizantesInfo[activeCompany][novedadRetiro.cotizanteDocumentNumber]
              .location,
          adminPlatform: {
            displayName: adminName,
            email: adminEmail,
          },
          idCotizante: novedadRetiro.idCotizante,
          idAportante: activeCompany,
          cotizanteData: {
            documentNumber: novedadRetiro.cotizanteDocumentNumber,
            companyName: companiesInfo[activeCompany].companyName,
            retired: novedadRetiro.retired,
            dateRetired: novedadRetiro.dateRetirement,
            regLine: novedadRetiro.regLine,
          },
          update: novedadRetiro.idNovedad !== '',
        };
        const retiroResult = await CreateRetiro(newRetiredData);
        if (retiroResult.status === 'SUCCESS') {
          const temp_basicas = JSON.parse(
            JSON.stringify(novedadRetiro.basicas)
          );
          if (temp_basicas.some((item) => item.type === 'Retiros')) {
            temp_basicas.forEach((item, index) => {
              if (item.type === 'Retiros') {
                temp_basicas[index] = retiroResult.data.result;
              }
            });
          }
          const cotizante_fields = {
            companyNit: activeCompany,
            periodoActual: periodosInfo[activeCompany].value,
            idPlanilla: periodosInfo[activeCompany].month,
            identificationNumber: novedadRetiro.cotizanteDocumentNumber,
            cotizanteFields: {
              retired: novedadRetiro.retired,
              dateRetired: novedadRetiro.dateRetirement,
              basicas: temp_basicas,
            },
          };
          dispatch(
            prePlanillaAction(
              PRE_PLANILLA.PRE_PLANILLA_UPDATE_COTIZANTE_FIELDS,
              cotizante_fields
            )
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
          onClose();
        } else if (retiroResult.status === 'FAILED') {
          console.log(
            'Hubo un error al crear la novedad de retiro, inténtelo nuevamente.'
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'No se detectaron cambios',
            description:
              'Para actualizar la novedad la fecha de retiro debe ser diferente',
            color: 'info',
          })
        );
        setDisableSaveButton(false);
        setDisableButtons(false);
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description: 'Por favor seleccione la fecha de retiro del cotizante',
          color: 'warning',
        })
      );
      setDisableSaveButton(false);
      setDisableButtons(false);
    }
  };

  return (
    <ModalNovedad
      open={openRetiroModal}
      title="Reportar retiro"
      employeeName={cotizanteName}
      onClose={onClose}
      onCancel={onClose}
      onSave={onSave}
      disableSaveButton={disableSaveButton}
      disableButtons={disableButtons}
    >
      <div className={classes.novedadContainer}>
        <Typography className={classes.novedadName} variant="subtitle1">
          {RETIRO_NOVEDAD_NAME}
        </Typography>
        <div className={classes.dateContent}>
          <div className={classes.dateTitleContainer}>
            <Typography className={classes.dateTitle} variant="subtitle1">
              {RETIRO_DATE_TEXT}
            </Typography>
          </div>
          <div className={classes.dateContainer}>
            <IconButton filled={false} size="small" onClick={openDatePicker}>
              <DateRangeIcon />
            </IconButton>
            <ButtonBase onClick={openDatePicker} className={classes.dateField}>
              <Typography className={classes.dateText} variant="subtitle2">
                {dateRetirement}
              </Typography>
            </ButtonBase>
            <div className={classes.removeIconContainer}>
              {retired && (
                <IconButton
                  buttoncolor="error"
                  filled={false}
                  size="small"
                  loading={disableRemoveButton}
                  disabled={disableButtons}
                  onClick={removeRetirementDate}
                >
                  <RemoveCircleOutlineIcon />
                </IconButton>
              )}
            </div>
            <Calendar
              open={showDatePicker}
              onChange={selectRetirementDate}
              anchorElement={retirementDateAnchor}
              focusDate={periodoActual.display}
              color="primary"
            />
          </div>
        </div>
      </div>
    </ModalNovedad>
  );
};

const useStyles = makeStyles((theme) => ({
  novedadContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(8),
  },
  novedadName: {
    color: theme.palette.primary.main,
  },
  dateContent: {
    backgroundColor: theme.palette.grey[200],
    width: '100%',
    borderTop: '1px solid',
    borderTopColor: theme.palette.primary[400],
    display: 'flex',
    padding: theme.spacing(3, 3, 5, 2),
  },
  dateTitleContainer: {
    flex: 1,
    alignItems: 'center',
    display: 'flex',
  },
  dateTitle: {
    color: theme.palette.common.black,
  },
  dateContainer: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  dateField: {
    border: '1px solid',
    borderColor: theme.palette.primary.main,
    borderRadius: 8,
    width: '45%',
    height: 30,
    margin: theme.spacing(0, 1),
    display: 'flex',
    alignItems: 'center',
  },
  dateText: {
    color: theme.palette.common.black,
  },
  removeSpinner: {
    color: theme.palette.primary.main,
  },
  removeIconContainer: {
    width: 30,
    height: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
