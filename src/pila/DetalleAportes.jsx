import React, { useEffect, useState } from 'react';
import { Divider, Grid, Tooltip, Typography } from '@material-ui/core';
import clsx from 'clsx';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import CardModal from '@bit/pilala.pilalalib.components.card-modal';
import { useDispatch, useSelector } from 'react-redux';
import HelperFunctions from '../utils/HelperFunctions';
import totalIcon from '../assets/img/total_icon.svg';
import { useDetalleAportesStyles } from './styles';
import { detalleAportesModalAction } from '../redux/Actions';
import { DETALLE_APORTES_MODAL } from '../redux/ActionTypes';

const DetalleAportes = React.memo(() => {
  const classes = useDetalleAportesStyles();
  const dispatch = useDispatch();
  const open = useSelector((state) => state.detalleAportes.isOpen);
  const cotizanteName = useSelector(
    (state) => state.detalleAportes.cotizanteName
  );
  const cotizanteDocumentType = useSelector(
    (state) => state.detalleAportes.cotizanteDocumentType
  );
  const cotizanteDocumentNumber = useSelector(
    (state) => state.detalleAportes.cotizanteDocumentNumber
  );
  const tarifas = useSelector((state) => state.detalleAportes.tarifas);
  const total = useSelector((state) => state.detalleAportes.total);
  const [values, setValues] = useState({
    totalAportes: 0,
    percentage: {},
  });
  const fspValue =
    (total?.subcuentaSolidaridad ?? 0) + (total?.subcuentaSubsistencia ?? 0);

  useEffect(() => {
    let sum = 0;
    let percentageValue = {};
    if (total) {
      Object.keys(total).forEach((subsistema) => {
        sum += total[subsistema];
      });
    }

    if (tarifas) {
      Object.keys(tarifas).forEach((subsistema) => {
        if (
          subsistema === 'subcuentaSolidaridad' ||
          subsistema === 'subcuentaSubsistencia'
        ) {
          percentageValue = {
            ...percentageValue,
            fsp:
              (percentageValue.fsp ?? 0) +
              parseFloat((tarifas[subsistema].value * 100).toFixed(3)),
            [subsistema]: parseFloat(
              (tarifas[subsistema].value * 100).toFixed(3)
            ),
          };
        } else {
          percentageValue = {
            ...percentageValue,
            [subsistema]: parseFloat(
              (tarifas[subsistema].value * 100).toFixed(3)
            ),
          };
        }
      });
    }

    setValues({
      totalAportes: sum,
      percentage: percentageValue,
    });
  }, [tarifas, total]);

  const onClose = () => {
    dispatch(detalleAportesModalAction(DETALLE_APORTES_MODAL.CLOSE_DA_MODAL));
  };

  const resetModalValues = () => {
    dispatch(detalleAportesModalAction(DETALLE_APORTES_MODAL.RESET_DA_VALUES));
  };

  return (
    <CardModal
      open={open}
      onClose={onClose}
      onExited={resetModalValues}
      maxWidth="md"
      title="Detalle seguridad social del cotizante"
      fullWidth
    >
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="center"
        className={classes.infoRow}
      >
        <div className={clsx(classes.infoBox, classes.infoBoxPadding)}>
          <Typography variant="subtitle1" className={classes.subtitle}>
            Nombre
          </Typography>
          <Typography variant="subtitle1">{cotizanteName}</Typography>
        </div>
        <div className={classes.infoBox}>
          <Typography variant="subtitle1" className={classes.subtitle}>
            Documento
          </Typography>
          <Typography variant="subtitle1">
            {`${cotizanteDocumentType} ${cotizanteDocumentNumber}`}
          </Typography>
        </div>
      </Grid>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        className={classes.detailRow}
      >
        <div className={classes.infoBox}>
          <Typography variant="subtitle1" className={classes.subtitle}>
            {`Salud ${values.percentage?.salud ?? 0}%`}
          </Typography>
          <Typography variant="subtitle1">
            {`$${HelperFunctions.formatStringNumber(total?.salud ?? 0)}`}
          </Typography>
        </div>
        <div className={classes.infoBox}>
          <Typography variant="subtitle1" className={classes.subtitle}>
            {`Pensión ${values.percentage?.pension ?? 0}%`}
          </Typography>
          <Typography variant="subtitle1">
            {`$${HelperFunctions.formatStringNumber(total?.pension ?? 0)}`}
          </Typography>
        </div>
        <div className={classes.infoBox}>
          <Typography variant="subtitle1" className={classes.subtitle}>
            {`ARL ${values.percentage?.arl ?? 0}%`}
          </Typography>
          <Typography variant="subtitle1">
            {`$${HelperFunctions.formatStringNumber(total?.arl ?? 0)}`}
          </Typography>
        </div>
        <div className={classes.infoBox}>
          <Typography variant="subtitle1" className={classes.subtitle}>
            {`CCF ${values.percentage?.ccf ?? 0}%`}
          </Typography>
          <Typography variant="subtitle1">
            {`$${HelperFunctions.formatStringNumber(total?.ccf ?? 0)}`}
          </Typography>
        </div>
        <Tooltip
          arrow
          title={
            <>
              <b>Subcuenta Solidaridad:</b>{' '}
              {values.percentage?.subcuentaSolidaridad ?? 0}%
              <br />
              <b>Subcuenta Subsistencia:</b>{' '}
              {values.percentage?.subcuentaSubsistencia ?? 0}%
            </>
          }
          classes={{
            tooltip: classes.tooltipInfo,
            arrow: classes.tooltipInfoArrow,
          }}
        >
          <div className={clsx(classes.infoBox, classes.fspBox)}>
            <Typography variant="subtitle1" className={classes.subtitle}>
              {`FSP ${values.percentage?.fsp ?? 0}%`}
            </Typography>
            <Typography variant="subtitle1">
              {`$${HelperFunctions.formatStringNumber(fspValue)}`}
            </Typography>
            <InfoOutlinedIcon className={classes.infoIcon} />
          </div>
        </Tooltip>
        <div className={classes.infoBox}>
          <Typography variant="subtitle1" className={classes.subtitle}>
            {`SENA ${values.percentage?.sena ?? 0}%`}
          </Typography>
          <Typography variant="subtitle1">
            {`$${HelperFunctions.formatStringNumber(total?.sena ?? 0)}`}
          </Typography>
        </div>
        <div className={classes.infoBox}>
          <Typography variant="subtitle1" className={classes.subtitle}>
            {`ICBF ${values.percentage?.icbf ?? 0}%`}
          </Typography>
          <Typography variant="subtitle1">
            {`$${HelperFunctions.formatStringNumber(total?.icbf ?? 0)}`}
          </Typography>
        </div>
      </Grid>
      <Divider
        classes={{
          root: classes.dividerRoot,
        }}
      />
      <Grid
        container
        direction="row"
        justify="flex-end"
        alignItems="center"
        className={classes.totalRow}
      >
        <div className={classes.totalContainer}>
          <img src={totalIcon} alt="Total icon" width="35px" height="32px" />
          <div className={classes.totalSubcontainer}>
            <Typography className={classes.totalTitle}>
              Total planilla
            </Typography>
            <Typography variant="h6" className={classes.total}>
              {`$ ${HelperFunctions.formatStringNumber(values.totalAportes)}`}
            </Typography>
          </div>
        </div>
      </Grid>
    </CardModal>
  );
});

export default DetalleAportes;
