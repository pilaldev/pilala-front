import React from 'react';
import PropTypes from 'prop-types';
import {
    Dialog,
    Typography,
    DialogContent,
    Button,
    CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {

} from './Constants';

const ConfirmationModalLayout = (props) => {

    const classes = useStyles();

    return (
        <Dialog
            aria-describedby={props.id}
            open={props.showModal}
            maxWidth='sm'
            PaperProps={{
                className: classes.paperContainer,
                style: {
                    backgroundColor: props.alertColor
                }
            }}
        >
            <DialogContent className={classes.dialogContent}>
                <Typography
                    className={classes.dialogTitle}
                    style={{
                        color: props.textColor
                    }}
                >
                    {props.message}
                </Typography>
                <div className={classes.buttonsContainer}>
                    <Button
                        className={classes.cancelButton}
                        onClick={props.cancelAction}
                        disabled={props.disableConfirmButton}
                    >
                        <Typography
                            variant='subtitle2'
                            className={classes.cancelButtonText}
                            style={{
                                color: props.textColor
                            }}
                        >
                            {'Cancelar'}
                        </Typography>
                    </Button>
                    <Button
                        variant='contained'
                        className={classes.saveButton}
                        onClick={props.confirmAction}
                        style={{
                            backgroundColor: props.textColor
                        }}
                    >
                        {
                            props.disableConfirmButton ?
                                <CircularProgress size={20} style={{ color: props.alertColor }} />
                                :
                                <Typography
                                    variant='subtitle2'
                                    className={classes.saveButtonText}
                                    style={{
                                        color: props.alertColor
                                    }}
                                >
                                    {'Confirmar'}
                                </Typography>
                        }
                    </Button>
                </div>
            </DialogContent>
        </Dialog>
    );
}

ConfirmationModalLayout.propTypes = {
    showModal: PropTypes.bool.isRequired,
    cancelAction: PropTypes.func.isRequired,
    confirmAction: PropTypes.func.isRequired,
    alertColor: PropTypes.string,
    textColor: PropTypes.string,
    id: PropTypes.string,
    message: PropTypes.string.isRequired
}

ConfirmationModalLayout.defaultProps = {
    alertColor: '#FFFFFF',
    textColor: '#000000'
}

const useStyles = makeStyles({
    paperContainer: {
        display: 'flex',
        flex: 1,
    },
    saveButton: {
        borderRadius: 22,
        marginLeft: 10,
        textTransform: 'none',
        width: '20%'
    },
    saveButtonText: {
        fontWeight: 'bold'
    },
    cancelButtonText: {

    },
    cancelButton: {
        marginRight: 10,
        borderRadius: 22,
        textTransform: 'none',
    },
    dialogContent: {
        paddingRight: '5%',
        paddingLeft: '5%'
    },
    dialogTitle: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: '1.5em'
    },
    buttonsContainer: {
        display: 'flex',
        alignItems: 'center',
        padding: '8% 0px 4% 0px',
        justifyContent: 'flex-end'
    },
    spinner: {
        color: '#2962FF'
    }
});

export default ConfirmationModalLayout;
