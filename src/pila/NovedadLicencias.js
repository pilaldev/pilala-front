import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import 'moment/locale/es';
import { novedadLicenciasAction, snackBarAction } from '../redux/Actions';
import { LICENCIAS, SNACKBAR } from '../redux/ActionTypes';
import ValidateDates from '../utils/ValidateDates';
import { AccordionNovedad, ModalNovedad } from '.';
import { LICENCIAS_MODAL } from './Constants';
import { useDispatch, useSelector } from 'react-redux';
import { CreateLicencia, RemoveNovedad } from './api';
moment.locale('es');

export default () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [initialDateAnchor, setInitialDateAnchor] = useState(null);
  const [finalDateAnchor, setFinalDateAnchor] = useState(null);
  const openLicenciasModal = useSelector(
    (state) => state.novedadLicencias.openLicenciasModal
  );
  const cotizanteName = useSelector(
    (state) => state.novedadLicencias.cotizanteName
  );
  const licenciasLR = useSelector((state) => state.novedadLicencias.LR);
  const licenciasLNR = useSelector((state) => state.novedadLicencias.LNR);
  const licenciasLMA = useSelector((state) => state.novedadLicencias.LMA);
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [removeIndex, setRemoveIndex] = useState(-1);
  const [disableSaveButton, setDisableSaveButton] = useState(false);
  const [disableButtons, setDisableButtons] = useState(false);
  const [disableRemoveButton, setDisableRemoveButton] = useState(false);
  const [openedAccordion, setOpenAccordion] = useState(null);
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const periodoActual = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const prePlanilla = useSelector((state) => state.prePlanilla);
  const periodosInfo = useSelector((state) => state.currentUser.periodosInfo);
  const tipoCotizante = useSelector(
    (state) => state.novedadLicencias.tipoCotizante
  );
  const cotizanteDocumentNumber = useSelector(
    (state) => state.novedadLicencias.cotizanteDocumentNumber
  );
  const activeChanges = useSelector(
    (state) => state.novedadLicencias.activeChanges
  );
  const idCotizante = useSelector(
    (state) => state.novedadLicencias.idCotizante
  );
  const cotizantesInfo = useSelector((state) => state.cotizantesInfo);
  const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);
  const totalDaysLR = useSelector(
    (state) => state.novedadLicencias.totalDaysLR
  );
  const totalDaysLNR = useSelector(
    (state) => state.novedadLicencias.totalDaysLNR
  );
  const totalDaysLMA = useSelector(
    (state) => state.novedadLicencias.totalDaysLMA
  );

  //TODO: Verificar nombre de compañia cuando se activen sucursales

  const openInitialDatePicker = (index, event) => {
    setSelectedIndex(index);
    setInitialDateAnchor(event.currentTarget);
  };

  const openFinalDatePicker = (index, event) => {
    setSelectedIndex(index);
    setFinalDateAnchor(event.currentTarget);
  };

  const removeItem = async (index) => {
    if (disableRemoveButton) return;
    setDisableRemoveButton(true);
    setDisableButtons(true);
    setRemoveIndex(index);

    const licenciasData =
      openedAccordion === 'LR'
        ? [...licenciasLR]
        : openedAccordion === 'LNR'
        ? [...licenciasLNR]
        : [...licenciasLMA];

    if (licenciasData[index].id) {
      const licenciasDeleteData = {
        idAportante: activeCompany,
        cotizanteData: {
          idCotizante,
          documentNumber: cotizanteDocumentNumber,
          companyName: companiesInfo[activeCompany].companyName,
          idNew: licenciasData[index].id,
          province: companiesInfo[activeCompany].location.province.value,
          city: companiesInfo[activeCompany].location.city.value,
          tipoCotizante: tipoCotizante.value,
          type: licenciasData[index].type,
        },
      };
      const deleteResult = await RemoveNovedad(licenciasDeleteData);
      if (deleteResult.status === 'SUCCESS') {
        dispatch(
          novedadLicenciasAction(LICENCIAS.LICENCIAS_REMOVE_NOVEDAD, {
            licenciaType: openedAccordion,
            index,
          })
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
        setRemoveIndex(-1);
      } else if (deleteResult.status === 'FAILED') {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al eliminar novedad',
            description:
              'Hemos tenido problemas para eliminar la novedad de licencia, inténtelo nuevamente',
            color: 'error',
          })
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
        setRemoveIndex(-1);
      }
    } else {
      dispatch(
        novedadLicenciasAction(LICENCIAS.LICENCIAS_REMOVE_NOVEDAD, {
          licenciaType: openedAccordion,
          index,
        })
      );
      setDisableRemoveButton(false);
      setDisableButtons(false);
      setRemoveIndex(-1);
    }
  };

  const resetFields = () => {
    if (openedAccordion) {
      setOpenAccordion(null);
    }
  };

  const onClose = () => {
    if (openLicenciasModal) {
      dispatch(
        novedadLicenciasAction(LICENCIAS.LICENCIAS_OPEN_LICENCIAS_MODAL, null)
      );
    }
    dispatch(novedadLicenciasAction(LICENCIAS.LICENCIAS_RESET_DATA, null));
    resetFields();
  };

  const onSave = async () => {
    if (disableSaveButton) return;
    setDisableSaveButton(true);
    setDisableButtons(true);

    const licencias = [...licenciasLR, ...licenciasLNR, ...licenciasLMA].filter(
      (el) => el.fechaInicio && el.fechaFin
    );

    if (checkDates(licencias)) {
      if (activeChanges) {
        const changedLicencias = licencias.map((item) => ({
          ...item,
          dateStart: item.fechaInicio,
          dateEnd: item.fechaFin,
        }));
        const licenciasFullData = {
          tipoCotizante: tipoCotizante.value,
          location:
            cotizantesInfo[activeCompany][cotizanteDocumentNumber].location,
          idCompany: activeCompany,
          cotizanteData: {
            idCotizante,
            arrayNew: changedLicencias,
          },
        };
        const method = changedLicencias.some((item) => item.id !== undefined)
          ? 'PATCH'
          : 'POST';
        const result = await CreateLicencia(licenciasFullData, method);
        if (result.status === 'SUCCESS') {
          dispatch(
            novedadLicenciasAction(
              LICENCIAS.LICENCIAS_OPEN_LICENCIAS_MODAL,
              null
            )
          );
          dispatch(
            novedadLicenciasAction(LICENCIAS.LICENCIAS_RESET_DATA, null)
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        } else if (result.status === 'FAILED') {
          dispatch(
            snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear novedad',
              description:
                'Hemos tenido problemas para crear la novedad de licencia, inténtelo nuevamente',
              color: 'error',
            })
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'No se detectaron cambios',
            description: 'La información sobre licencias no ha cambiado',
            color: 'info',
          })
        );
        setDisableSaveButton(false);
        setDisableButtons(false);
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description: 'Por favor complete las fechas del reporte de licencias',
          color: 'warning',
        })
      );
      setDisableSaveButton(false);
      setDisableButtons(false);
    }
  };

  const checkDates = (incapacidades) => {
    return incapacidades.every(
      (item) => item.fechaInicio !== '' && item.fechaFin !== ''
    );
  };

  const addRow = () => {
    const current =
      openedAccordion === 'LR'
        ? licenciasLR
        : openedAccordion === 'LNR'
        ? licenciasLNR
        : licenciasLMA;
    if (checkDates(current)) {
      dispatch(
        novedadLicenciasAction(LICENCIAS.LICENCIAS_ADD_NOVEDAD, {
          licenciaType: openedAccordion,
        })
      );
    }
  };

  const getInitialDate = (index, date, panel) => {
    let fechaFin = '';
    let durationDays = '';
    let id = null;
    let currentNovedades = [];

    switch (panel) {
      case 'LR':
        fechaFin = licenciasLR[index].fechaFin;
        id = licenciasLR[index].id;
        currentNovedades = licenciasLR
          .concat(licenciasLNR)
          .concat(licenciasLMA);
        break;
      case 'LNR':
        fechaFin = licenciasLNR[index].fechaFin;
        id = licenciasLNR[index].id;
        currentNovedades = licenciasLNR
          .concat(licenciasLR)
          .concat(licenciasLMA);
        break;
      case 'LMA':
        fechaFin = licenciasLMA[index].fechaFin;
        id = licenciasLMA[index].id;
        currentNovedades = licenciasLMA
          .concat(licenciasLR)
          .concat(licenciasLNR);
        break;
      default:
        break;
    }

    if (fechaFin) {
      durationDays =
        moment(fechaFin, 'DD/MM/YYYY')
          .startOf('day')
          .diff(moment(date, 'DD/MM/YYYY').startOf('day'), 'days') + 1;
    }

    if (
      fechaFin === '' ||
      moment(fechaFin, 'DD/MM/YYYY')
        .startOf('day')
        .diff(moment(date, 'DD/MM/YYYY').startOf('day'), 'days') >= 0
    ) {
      const cotizanteData =
        prePlanilla[activeCompany][periodosInfo[activeCompany].value][
          tipoCotizante.value
        ][periodosInfo[activeCompany].month][cotizanteDocumentNumber];

      const dateValidation = ValidateDates(
        cotizanteData,
        date,
        false,
        false,
        index,
        id,
        true,
        false,
        currentNovedades
      );

      if (dateValidation.result) {
        const value = {
          licenciaType: panel,
          totalDaysLR: panel === 'LR' ? (durationDays ? durationDays : 0) : 0,
          totalDaysLNR: panel === 'LNR' ? (durationDays ? durationDays : 0) : 0,
          totalDaysLMA: panel === 'LMA' ? (durationDays ? durationDays : 0) : 0,
          novedad: {
            fechaInicio: moment(date).format('DD/MM/YYYY'),
            value: {
              fechaInicio: moment(date).format('DD/MM/YYYY'),
            },
            duracionDias: durationDays ? durationDays : '',
            regLine: {},
          },
          index,
        };
        dispatch(
          novedadLicenciasAction(LICENCIAS.LICENCIAS_UPDATE_NOVEDAD, value)
        );
        if (!activeChanges) {
          dispatch(
            novedadLicenciasAction(
              LICENCIAS.LICENCIAS_ENABLE_ACTIVE_CHANGES,
              null
            )
          );
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al validar fechas',
            description: dateValidation.message,
            color: 'error',
          })
        );
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Fecha incorrecta',
          description: 'La fecha inicial debe ser menor que la fecha final',
          color: 'info',
        })
      );
    }
  };

  const getFinalDate = (index, date, panel) => {
    let fechaInicio = '';
    let durationDays = '';
    let id = null;
    let currentNovedades = [];

    switch (panel) {
      case 'LR':
        fechaInicio = licenciasLR[index].fechaInicio;
        id = licenciasLR[index].id;
        currentNovedades = licenciasLR
          .concat(licenciasLNR)
          .concat(licenciasLMA);
        break;
      case 'LNR':
        fechaInicio = licenciasLNR[index].fechaInicio;
        id = licenciasLNR[index].id;
        currentNovedades = licenciasLNR
          .concat(licenciasLR)
          .concat(licenciasLMA);
        break;
      case 'LMA':
        fechaInicio = licenciasLMA[index].fechaInicio;
        id = licenciasLMA[index].id;
        currentNovedades = licenciasLMA
          .concat(licenciasLR)
          .concat(licenciasLNR);
        break;
      default:
        break;
    }

    if (fechaInicio) {
      durationDays =
        moment(date, 'DD/MM/YYYY')
          .startOf('day')
          .diff(moment(fechaInicio, 'DD/MM/YYYY').startOf('day'), 'days') + 1;
    }

    if (
      fechaInicio === '' ||
      moment(date, 'DD/MM/YYYY')
        .startOf('day')
        .diff(moment(fechaInicio, 'DD/MM/YYYY').startOf('day'), 'days') >= 0
    ) {
      const cotizanteData =
        prePlanilla[activeCompany][periodosInfo[activeCompany].value][
          tipoCotizante.value
        ][periodosInfo[activeCompany].month][cotizanteDocumentNumber];

      const dateValidation = ValidateDates(
        cotizanteData,
        date,
        false,
        false,
        index,
        id,
        false,
        true,
        currentNovedades
      );

      if (dateValidation.result) {
        const value = {
          licenciaType: panel,
          totalDaysLR: panel === 'LR' ? (durationDays ? durationDays : 0) : 0,
          totalDaysLNR: panel === 'LNR' ? (durationDays ? durationDays : 0) : 0,
          totalDaysLMA: panel === 'LMA' ? (durationDays ? durationDays : 0) : 0,
          novedad: {
            fechaFin: moment(date).format('DD/MM/YYYY'),
            value: {
              fechaFin: moment(date).format('DD/MM/YYYY'),
            },
            duracionDias: durationDays ? durationDays : '',
            regLine: {},
          },
          index,
        };
        dispatch(
          novedadLicenciasAction(LICENCIAS.LICENCIAS_UPDATE_NOVEDAD, value)
        );
        if (!activeChanges) {
          dispatch(
            novedadLicenciasAction(
              LICENCIAS.LICENCIAS_ENABLE_ACTIVE_CHANGES,
              null
            )
          );
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al validar fechas',
            description: dateValidation.message,
            color: 'error',
          })
        );
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Fecha incorrecta',
          description: 'La fecha final debe ser mayor que la fecha inicial',
          color: 'info',
        })
      );
    }
  };

  const selectInitialDate = (data) => {
    getInitialDate(selectedIndex, data, openedAccordion);
    setInitialDateAnchor(null);
  };

  const selectFinalDate = (data) => {
    getFinalDate(selectedIndex, data, openedAccordion);
    setFinalDateAnchor(null);
  };

  const expandAccordion = (panel) => (_, isExpanded) => {
    if (isExpanded) {
      setOpenAccordion(panel);
    } else {
      setOpenAccordion(null);
    }
  };

  return (
    <>
      <ModalNovedad
        open={openLicenciasModal}
        title="Reportar licencia"
        employeeName={cotizanteName}
        onClose={onClose}
        onCancel={onClose}
        onSave={onSave}
        disableSaveButton={disableSaveButton}
        disableButtons={disableButtons}
      >
        <div className={classes.novedadContainer}>
          <AccordionNovedad
            panel={openedAccordion}
            title={LICENCIAS_MODAL.LR_TITLE}
            subtitle="Días de licencia"
            onChange={expandAccordion('LR')}
            addTitle="Licencia"
            focusDate={periodoActual.display}
            rows={licenciasLR}
            openInitialDatePicker={openInitialDatePicker}
            openFinalDatePicker={openFinalDatePicker}
            removeItem={removeItem}
            addRow={addRow}
            selectInitialDate={selectInitialDate}
            selectFinalDate={selectFinalDate}
            initialAnchor={initialDateAnchor}
            finalAnchor={finalDateAnchor}
            id="LR"
            disableRemoveButton={disableRemoveButton}
            removeIndex={removeIndex}
            disableButtons={disableButtons}
            daysSum={totalDaysLR}
          />
          <AccordionNovedad
            panel={openedAccordion}
            title={LICENCIAS_MODAL.LNR_TITLE}
            subtitle="Días de licencia"
            onChange={expandAccordion('LNR')}
            addTitle="Licencia"
            focusDate={periodoActual.display}
            rows={licenciasLNR}
            openInitialDatePicker={openInitialDatePicker}
            openFinalDatePicker={openFinalDatePicker}
            removeItem={removeItem}
            addRow={addRow}
            selectInitialDate={selectInitialDate}
            selectFinalDate={selectFinalDate}
            initialAnchor={initialDateAnchor}
            finalAnchor={finalDateAnchor}
            id="LNR"
            disableRemoveButton={disableRemoveButton}
            removeIndex={removeIndex}
            disableButtons={disableButtons}
            daysSum={totalDaysLNR}
          />
          <AccordionNovedad
            panel={openedAccordion}
            title={LICENCIAS_MODAL.LMA_TITLE}
            subtitle="Días de licencia"
            onChange={expandAccordion('LMA')}
            addTitle="Licencia"
            focusDate={periodoActual.display}
            rows={licenciasLMA}
            openInitialDatePicker={openInitialDatePicker}
            openFinalDatePicker={openFinalDatePicker}
            removeItem={removeItem}
            addRow={addRow}
            selectInitialDate={selectInitialDate}
            selectFinalDate={selectFinalDate}
            initialAnchor={initialDateAnchor}
            finalAnchor={finalDateAnchor}
            id="LMA"
            disableRemoveButton={disableRemoveButton}
            removeIndex={removeIndex}
            disableButtons={disableButtons}
            daysSum={totalDaysLMA}
          />
        </div>
      </ModalNovedad>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  novedadContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(8),
  },
}));
