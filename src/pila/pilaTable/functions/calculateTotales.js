const calculateTotales = (cotizante) => {
	let licencias = cotizante.data().licencias;
	let incapacidades = cotizante.data().incapacidades;
	let suspensiones = cotizante.data().suspensiones;
	let vacaciones = cotizante.data().vacaciones;

	let transitorySalary = cotizante.data().transitorySalary.value ? cotizante.data().transitorySalary.value : 0;

	let salary = cotizante.data().salary.value + transitorySalary;

	// console.log('salary', salary);

	let totalAportes = 0;
	let IBC = 0;
	let tarifas = Object.assign({}, cotizante.data().subsistemas);

	// console.log('taridas', tarifas);

	if (licencias.length) {
		licencias.forEach((licencia) => {
			let type = licencia.nombreNovedad;

			IBC = Math.ceil(parseInt(salary) / 30 * licencia.duracionDias);

			if (type === 'LNR') {
				tarifas.ccf.value = 0;
				tarifas.salud.value = 0.085;
				tarifas.arl.value = 0;
				tarifas.sena.value = 0;
				tarifas.icbf.value = 0;
			}

			if (type === 'LR') {
				tarifas.arl.value = 0;
			}

			if (type === 'LMA') {
				tarifas.arl.value = 0;
				tarifas.ccf.value = 0;
				tarifas.sena.value = 0;
				tarifas.icbf.value = 0;
            }
            
            // console.log("tarifas", tarifas)

			Object.keys(tarifas).forEach((subsistema) => {
				totalAportes += Math.ceil(IBC * tarifas[subsistema].value);
			});
		});
    }
    
    // console.log("t",totalAportes)

	tarifas = Object.assign({}, cotizante.data().subsistemas);

	if (incapacidades.length) {
		incapacidades.forEach((incapacidad) => {
			let type = incapacidad.nombreNovedad;

			if (type === 'IGE') {
				IBC = Math.ceil(parseInt(salary) / 30 * incapacidad.duracionDias) * 0.6667;
				tarifas.arl.value = 0;
				tarifas.ccf.value = 0;
				tarifas.sena.value = 0;
				tarifas.icbf.value = 0;
			}

			if (type === 'IRL') {
				IBC = Math.ceil(parseInt(salary) / 30 * incapacidad.duracionDias);
				tarifas.arl.value = 0;
				tarifas.ccf.value = 0;
				tarifas.sena.value = 0;
				tarifas.icbf.value = 0;
			}

			Object.keys(tarifas).forEach((subsistema) => {
				totalAportes += Math.ceil(IBC * tarifas[subsistema].value);
			});
		});
    }
    
    tarifas = Object.assign({}, cotizante.data().subsistemas);

	if (suspensiones.length) {
		suspensiones.forEach((suspension) => {
			IBC = Math.ceil(parseInt(salary) / 30 * suspension.duracionDias);
			tarifas.arl.value = 0;
			Object.keys(tarifas).forEach((subsistema) => {
				totalAportes += Math.ceil(IBC * tarifas[subsistema].value);
			});
		});
    }
    
    tarifas = Object.assign({}, cotizante.data().subsistemas);

	if (vacaciones.length) {
		vacaciones.forEach((vacacion) => {
			IBC = Math.ceil(parseInt(salary) / 30 * vacacion.duracionDias);
			tarifas.ccf.value = 0;
			tarifas.salud.value = 0.085;
			tarifas.arl.value = 0;
			tarifas.sena.value = 0;
			tarifas.icbf.value = 0;
			Object.keys(tarifas).forEach((subsistema) => {
				totalAportes += Math.ceil(IBC * tarifas[subsistema].value);
			});
		});
	}

	return totalAportes;
};

export default calculateTotales;
