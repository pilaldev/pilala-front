import Endpoints from '@bit/pilala.pilalalib.endpoints';

export const requestPreplanillaData = async (data) => {
	return new Promise(async (resolve, reject) => {
		await fetch(Endpoints.requestPreplanillaData, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto la CF de edición de cotizante',
					error,
				});
			});
	});
};
