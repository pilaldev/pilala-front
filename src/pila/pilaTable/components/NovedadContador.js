import React from 'react';
import { ButtonBase, Typography } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const NovedadContador = (props) => {
	const classes = useStyles();
	return (
		<div className={classes.buttonContainer} onClick={props.onClick}>
			<Typography
				style={{
					fontSize: '1.1em',
					fontWeight: 'bold'
				}}
			>
				{props.novedadText}
			</Typography>

			<ButtonBase
				className={classes.dayCounterField}
				style={{
					backgroundColor: props.backgroundColor
				}}
				disableRipple
			>
				<Typography
					style={{
						color: props.typographyColor,
						fontSize: '1.1em',
						fontWeight: 'bold'
					}}
				>
					{props.durationDays}
				</Typography>
			</ButtonBase>
		</div>
	);
};

const useStyles = makeStyles({
	dayCounterField: {
		border: '2px solid #5E35B1',
		borderRadius: 7,
		width: '100%',
		height: 30,
		margin: '0px 6px 0px 6px'
	},

	buttonContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		width: 90,
		cursor: 'pointer'
	}
});

export default NovedadContador;
