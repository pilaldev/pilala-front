import React, { useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  InputAdornment,
  CircularProgress,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
} from '@material-ui/core';
import {
  COTIZANTES_DETAIL,
  VACACIONES,
  VARIACION_TRANSITORIA,
  VARIACION_PERMANENTE,
  INGRESO,
  RETIRO,
  PRE_PLANILLA,
  INCAPACIDADES,
  SUSPENSIONES,
  LICENCIAS,
  SNACKBAR,
  NAVIGATION,
  PAY_PILA,
  DETALLE_APORTES_MODAL,
} from '../../redux/ActionTypes';
import {
  cotizantesDetailAction,
  novedadVacacionesAction,
  novedadVariacionPermanenteAction,
  novedadRetiroAction,
  novedadIngresoAction,
  novedadVariacionTransitoriaAction,
  prePlanillaAction,
  novedadIncapacidadesAction,
  novedadSuspensionesAction,
  novedadLicenciasAction,
  snackBarAction,
  navigationAction,
  payPilaAction,
  detalleAportesModalAction,
} from '../../redux/Actions';
import { TextField, ButtonBase } from '@material-ui/core';
import NovedadContador from './components/NovedadContador';
import { editRecordField } from './../../constants/CotizantesFields';
import { requestPreplanillaData } from './api/RequestPreplanillaData';
import { novedadVariacionSalario } from './../api/novedadVariacionPermanente';
import { FIRESTORE } from '../../api/Firebase';
import { deleteNovedades } from '../api/novedadVariacionPermanente';
import clsx from 'clsx';
import HelperFunctions from '../../utils/HelperFunctions';
import EditRoundedIcon from '@material-ui/icons/EditRounded';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
import DoneRoundedIcon from '@material-ui/icons/DoneRounded';
import RemoveCircleOutlineRoundedIcon from '@material-ui/icons/RemoveCircleOutlineRounded';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import MaskedInput from 'react-text-mask';
import { Chip, Table, FloatingMenu } from '../../components';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Button from '@bit/pilala.pilalalib.components.button';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';

const MaskedSalaryInput = (props) => {
  const { inputRef, ...other } = props;
  const salaryMask = createNumberMask({
    prefix: '',
    thousandsSeparatorSymbol: '.',
    allowDecimal: false,
  });

  return (
    <MaskedInput
      {...other}
      mask={salaryMask}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      guide={false}
    />
  );
};

const PilaTable = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const params = useParams();
  const activeCorreciones = useSelector(
    (state) => state.currentUser.activeCorreciones
  );
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);
  const periodosInfo = useSelector((state) => state.currentUser.periodosInfo);
  const correcionesInfo = useSelector(
    (state) => state.currentUser.correcionesInfo
  );
  const companyInfo = companiesInfo[activeCompany];
  const companyId = companyInfo.isCompany
    ? companyInfo.idCompany
    : companyInfo.idSucursal;
  const prePlanilla = useSelector((state) => state.prePlanilla);
  const [cotizantes, setCotizantes] = useState([]);
  const [disableSaveVariacionButton, setDisableSaveVariacionButton] = useState(
    false
  );
  const [
    disableRemoveVariacionButton,
    setDisableRemoveVariacionButton,
  ] = useState(false);

  const formatPrePlanillaData = useCallback(
    (data) => {
      const periodoInfo = activeCorreciones
        ? correcionesInfo[activeCompany]
        : periodosInfo[activeCompany];
      let cotizantes = {};
      data.forEach((cotizante) => {
        const cotizanteData = cotizante.data();
        cotizantes = {
          ...cotizantes,
          [cotizanteData.documentNumber]: {
            ...cotizanteData,
          },
        };
      });
      const value = {
        activeCompany,
        periodoActual: periodoInfo.value,
        idPlanilla: periodoInfo.month,
        data: cotizantes,
      };
      dispatch(prePlanillaAction(PRE_PLANILLA.PRE_PLANILLA_SAVE_DATA, value));
    },
    [activeCompany, activeCorreciones, correcionesInfo, dispatch, periodosInfo]
  );

  // DidMount
  useEffect(() => {
    let firestoreListener = () => {};
    const periodoInfo = activeCorreciones
      ? correcionesInfo[activeCompany]
      : periodosInfo[activeCompany];
    if (companyInfo) {
      const getPreplanillaData = async () => {
        await requestPreplanillaData({
          companyId,
          specificPlanilla: activeCorreciones,
          specificInfo: activeCorreciones
            ? correcionesInfo[activeCompany]
            : null,
        })
          .then((result) => {
            if (result.result === 'SUCCESS') {
              return dispatch(
                prePlanillaAction(
                  PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA,
                  result.data
                )
              );
            } else {
              return null;
            }
          })
          .then((result) => {
            if (result) {
              firestoreListener = FIRESTORE.collection('preplanilla')
                .doc(companyId)
                .collection(periodoInfo.value)
                .doc('dependientes')
                .collection(periodoInfo.month)
                .onSnapshot(
                  (data) => {
                    formatPrePlanillaData(data.docs);
                    return;
                  },
                  (err) => {
                    console.log(`Encountered error: ${err}`);
                    return null;
                  }
                );
            }
          });
      };
      getPreplanillaData();
    }
    return () => {
      firestoreListener();
    };
  }, [
    activeCompany,
    activeCorreciones,
    companyId,
    companyInfo,
    correcionesInfo,
    dispatch,
    formatPrePlanillaData,
    periodosInfo,
  ]);

  // DidUpdate
  useEffect(() => {
    const mapCotizantes = () => {
      let mapedData = [];
      const periodoInfo = activeCorreciones
        ? correcionesInfo[activeCompany]
        : periodosInfo[activeCompany];

      try {
        if (activeCompany && prePlanilla[activeCompany] && periodoInfo) {
          Object.keys(
            prePlanilla[activeCompany][periodoInfo.value]['dependientes'][
              periodoInfo.month
            ]
          ).forEach((cotizante, index) => {
            const cotizanteData =
              prePlanilla[activeCompany][periodoInfo.value]['dependientes'][
                periodoInfo.month
              ][cotizante];
            if (cotizanteData.activeInCompany.value) {
              mapedData.push({
                ...cotizanteData,
              });
            }

            const VSTindex = cotizanteData.salariales.findIndex(
              (el) =>
                el.nombreNovedad ===
                  /*'VST'*/ 'Ajuste de salario transitorio' ||
                el.nombreNovedad === 'VST'
            );

            dispatch(
              novedadVariacionTransitoriaAction(
                VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_SET_COTIZANTE_DATA,
                VSTindex >= 0
                  ? {
                      documentNumber: cotizanteData.documentNumber,
                      index: VSTindex,
                      transitorySalary:
                        cotizanteData.salariales[VSTindex].value.newSalary,
                      novedad: cotizanteData.salariales[VSTindex],
                    }
                  : {
                      documentNumber: cotizanteData.documentNumber,
                      index: VSTindex,
                      transitorySalary: '',
                    }
              )
            );
          });
          setCotizantes(mapedData);
        }
      } catch (error) {}
    };
    mapCotizantes();
  }, [
    prePlanilla,
    activeCorreciones,
    periodosInfo,
    activeCompany,
    correcionesInfo,
    dispatch,
  ]);

  const goToDetail = (id, type, documentNumber) => {
    dispatch(
      cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_DATA, {
        id,
        type,
        documentNumber,
      })
    );
    history.push(`/${params.userId}/console/cotizantes/${id}`);
  };

  const openIngresoModal = (data) => {
    const periodoInfo = periodosInfo[activeCompany];
    dispatch(
      cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_DATA, {
        id: data.id,
        type: data.tipoCotizante,
        documentNumber: data.documentNumber,
      })
    );
    const cotizanteData =
      prePlanilla[activeCompany][periodoInfo.value]['dependientes'][
        periodoInfo.month
      ][data.documentNumber];
    const novedad = cotizanteData.basicas.find(
      (item) => item.type === 'Ingresos'
    );
    const novedad_fields = {
      cotizanteName: data.name,
      cotizanteSalary: data.salary,
      regLine: {}, //cotizanteData.regLine,
      newEntry: cotizanteData.newEntry,
      basicas: cotizanteData.basicas,
      idCotizante: cotizanteData.id,
      cotizanteDocumentNumber: cotizanteData.documentNumber,
      tipoCotizante: cotizanteData.tipoCotizante.value,
    };
    if (typeof novedad === 'object') {
      novedad_fields.dateEntry = novedad.fechaInicio;
      novedad_fields.idNovedad = novedad.id;
    }
    dispatch(
      novedadIngresoAction(INGRESO.INGRESO_SAVE_NOVEDAD_FIELDS, novedad_fields)
    );
    dispatch(novedadIngresoAction(INGRESO.INGRESO_OPEN_INGRESO_MODAL, null));
  };

  const openRetiroModal = (data) => {
    const periodoInfo = periodosInfo[activeCompany];
    dispatch(
      cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_DATA, {
        id: data.id,
        type: data.tipoCotizante,
        documentNumber: data.documentNumber,
      })
    );
    const cotizanteData =
      prePlanilla[activeCompany][periodoInfo.value]['dependientes'][
        periodoInfo.month
      ][data.documentNumber];
    const novedad = cotizanteData.basicas.find(
      (item) => item.type === 'Retiros'
    );
    const novedad_fields = {
      cotizanteName: data.name,
      cotizanteSalary: data.salary,
      regLine: {}, //cotizanteData.regLine,
      retired: cotizanteData.retired,
      basicas: cotizanteData.basicas,
      idCotizante: cotizanteData.id,
      cotizanteDocumentNumber: cotizanteData.documentNumber,
      tipoCotizante: cotizanteData.tipoCotizante.value,
    };
    if (typeof novedad === 'object') {
      novedad_fields.dateRetirement = novedad.fechaInicio;
      novedad_fields.idNovedad = novedad.id;
    }
    dispatch(
      novedadRetiroAction(RETIRO.RETIRO_SAVE_NOVEDAD_FIELDS, novedad_fields)
    );
    dispatch(novedadRetiroAction(RETIRO.RETIRO_OPEN_RETIRO_MODAL, null));
  };

  const openNovedadTransitoria = (documentNumber) => {
    dispatch(
      novedadVariacionTransitoriaAction(
        VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_UPDATE_NOVEDAD_STATUS,
        {
          documentNumber,
        }
      )
    );
  };

  const setTransitorySalaryValue = (salary, documentNumber) => {
    dispatch(
      novedadVariacionTransitoriaAction(
        VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_SET_TRANSITORY_SALARY,
        {
          documentNumber,
          salary,
        }
      )
    );
  };

  const saveNovedadTransitoria = (
    documentNumber,
    newSalary,
    oldSalary,
    editedData,
    preLoadedData,
    regLine,
    prePlanillaTransitorySalary,
    index,
    tipoCotizante,
    idCotizante
  ) => {
    if (disableSaveVariacionButton) return;
    dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, true));
    setDisableSaveVariacionButton(true);

    if (newSalary && editedData) {
      const companyInfo = companiesInfo[activeCompany];

      if (preLoadedData) {
        const value = {
          companyNit: activeCompany,
          identificationNumber: documentNumber,
          cotizanteFields: {
            transitorySalary: {
              code: parseInt(newSalary),
              display: newSalary,
            },
            transitorySalaryVariation: true,
            regLine: {
              ...regLine,
              23: editRecordField[23]('X'),
              40: editRecordField[40](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
              42: editRecordField[42](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
              43: editRecordField[43](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
              44: editRecordField[44](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
              45: editRecordField[45](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
            },
          },
          periodoActual: periodosInfo[activeCompany].value,
          idPlanilla: periodosInfo[activeCompany].month,
        };

        dispatch(
          prePlanillaAction(
            PRE_PLANILLA.PRE_PLANILLA_UPDATE_COTIZANTE_FIELDS,
            value
          )
        );

        let novedad = {
          value: {
            newSalary: newSalary,
            previousSalary: oldSalary,
          },
        };

        dispatch(
          prePlanillaAction(PRE_PLANILLA.PRE_PLANILLA_UPDATE_NOVEDAD, {
            companyNit: activeCompany,
            identificationNumber: documentNumber,
            index,
            novedad,
            periodoActual: periodosInfo[activeCompany].value,
            idPlanilla: periodosInfo[activeCompany].month,
            field: 'salariales',
          })
        );
        // dispatch(novedadVariacionTransitoriaAction(VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_UPDATE_NOVEDAD, {
        // 	documentNumber,
        // 	data: {
        // 		// existing: true,
        // 		opened: false,
        // 		// preLoadedData: true,
        // 		cotizanteTransitorySalary: newSalary
        // 	}
        // }));
        setDisableSaveVariacionButton(false);
        dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false));
      } else {
        const value = {
          companyNit: activeCompany,
          identificationNumber: documentNumber,
          field: 'salariales',
          data: {
            transitorySalary: {
              code: parseInt(newSalary),
              display: newSalary,
            },
            transitorySalaryVariation: true,
            // 	regLine: {
            // 		...regLine,
            // 		23: editRecordField[23]('X'),
            // 		40: editRecordField[40](parseInt(newSalary) + parseInt(oldSalary)),
            // 		42: editRecordField[42](parseInt(newSalary) + parseInt(oldSalary)),
            // 		43: editRecordField[43](parseInt(newSalary) + parseInt(oldSalary)),
            // 		44: editRecordField[44](parseInt(newSalary) + parseInt(oldSalary)),
            // 		45: editRecordField[45](parseInt(newSalary) + parseInt(oldSalary))
            // 	}
            // },
            // novedad: {
            // 	nombreNovedad: this.props.novedadVariacionTransitoria[documentNumber].nombreNovedad,
            // 	duracionDias: this.props.novedadVariacionTransitoria[documentNumber].duracionDias,
            // 	fechaInicio: '',
            // 	fechaFin: '',
            // 	descripcionNovedad: '',
            // 	dateCreated: '',
            // 	dateUpdate: '',
            // 	value: {
            // 		newSalary: newSalary,
            // 		previousSalary: oldSalary
            // 	},
            // 	regLine: '',
            // 	type: ''
            // },
          },
          periodoActual: periodosInfo[activeCompany].value,
          idPlanilla: periodosInfo[activeCompany].month,
        };

        const objetoNovedadVariacionSalario = {
          tipoCotizante: tipoCotizante,
          location: companyInfo.location,
          idCotizante,
          idAportante: activeCompany,
          cotizanteData: {
            documentNumber: documentNumber,
            companyName: companyInfo.companyName,
            type: 2,
            salary: {
              code: parseInt(newSalary),
              value: newSalary,
              dateCreated: '',
              dateApply: '',
              newSalary: newSalary,
              previousSalary: oldSalary,
            },
            regLine: {
              ...regLine,
              23: editRecordField[23]('X'),
              40: editRecordField[40](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
              42: editRecordField[42](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
              43: editRecordField[43](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
              44: editRecordField[44](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
              45: editRecordField[45](
                parseInt(newSalary) + parseInt(oldSalary)
              ),
            },
          },
        };
        novedadVariacionSalario(objetoNovedadVariacionSalario)
          .then((result) => {
            console.log('resultado variacion', result);
            if (result.status === 'SUCCESS') {
              value.novedad = {
                ...value.novedad,
                ...result.result,
              };
              dispatch(
                prePlanillaAction(PRE_PLANILLA.PRE_PLANILLA_ADD_NOVEDAD, value)
              );

              setDisableSaveVariacionButton(false);
              dispatch(
                navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false)
              );
            } else {
              dispatch(
                snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                  title: 'Error al crear novedad',
                  description:
                    'Hemos tenido problemas para crear la novedad de variación transitoria, inténtalo nuevamente',
                  color: 'error',
                })
              );
              setDisableSaveVariacionButton(false);
              dispatch(
                navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false)
              );
              throw result;
            }
          })
          .catch((error) => {
            console.log('error', error);
            setDisableSaveVariacionButton(false);
            dispatch(
              navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false)
            );
          });
      }
    } else {
      if (preLoadedData) {
        dispatch(
          novedadVariacionTransitoriaAction(
            VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_UPDATE_NOVEDAD,
            {
              documentNumber,
              data: {
                existing: true,
                opened: false,
                cotizanteTransitorySalary: prePlanillaTransitorySalary.display,
              },
            }
          )
        );
        setDisableSaveVariacionButton(false);
        dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false));
      } else {
        dispatch(
          novedadVariacionTransitoriaAction(
            VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_UPDATE_NOVEDAD,
            {
              documentNumber,
              data: {
                existing: false,
                opened: false,
              },
            }
          )
        );
        setDisableSaveVariacionButton(false);
        dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false));
      }
    }
  };

  const editNovedadTransitoria = (documentNumber) => {
    dispatch(
      novedadVariacionTransitoriaAction(
        VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_UPDATE_NOVEDAD,
        {
          documentNumber,
          data: {
            opened: true,
          },
        }
      )
    );
  };

  const openVacacionesModal = (data) => {
    const value = {
      cotizanteName: data.name.display,
      cotizanteIdentificationNumber: data.documentNumber,
      cotizanteType: data.tipoCotizante,
      idCotizante: data.id,
    };
    if (data.vacaciones.length > 0) {
      value.vacacionesRows = data.vacaciones;
      value.vacacionesAmount = data.vacaciones.length;
    }
    dispatch(
      novedadVacacionesAction(VACACIONES.VACACIONES_SAVE_COTIZANTE_DATA, value)
    );
    dispatch(
      novedadVacacionesAction(VACACIONES.VACACIONES_OPEN_VACACIONES_MODAL, null)
    );
  };

  const openIncapacidadesModal = (data) => {
    let IGEDays = 0;
    let IRLDays = 0;

    const IGE = data.incapacidades.filter((el) => el.nombreNovedad === 'IGE');
    const IRL = data.incapacidades.filter((el) => el.nombreNovedad === 'IRL');

    IGE.forEach((item) => {
      IGEDays += item.duracionDias;
    });

    IRL.forEach((item) => {
      IRLDays += item.duracionDias;
    });

    const value = {
      cotizanteName: data.name.display,
      cotizanteDocumentNumber: data.documentNumber,
      tipoCotizante: data.tipoCotizante,
      totalDaysIGE: IGEDays,
      totalDaysIRL: IRLDays,
      idCotizante: data.idCotizante,
    };

    if (IGE.length > 0) {
      value.IGE = IGE;
    }
    if (IRL.length > 0) {
      value.IRL = IRL;
    }

    dispatch(
      novedadIncapacidadesAction(
        INCAPACIDADES.INCAPACIDADES_SET_COTIZANTE_DATA,
        value
      )
    );
    dispatch(
      novedadIncapacidadesAction(
        INCAPACIDADES.INCAPACIDADES_OPEN_INCAPACIDADES_MODAL,
        null
      )
    );
  };

  const openLicenciasModal = (data) => {
    let LRDays = 0;
    let LNRDays = 0;
    let LMADays = 0;

    let LR = data.licencias.filter((el) => el.nombreNovedad === 'LR');
    let LNR = data.licencias.filter((el) => el.nombreNovedad === 'LNR');
    let LMA = data.licencias.filter((el) => el.nombreNovedad === 'LMA');

    LR.forEach((item) => {
      LRDays += item.duracionDias;
    });

    LNR.forEach((item) => {
      LNRDays += item.duracionDias;
    });

    LMA.forEach((item) => {
      LMADays += item.duracionDias;
    });

    const value = {
      cotizanteName: data.name.display,
      cotizanteDocumentNumber: data.documentNumber,
      tipoCotizante: data.tipoCotizante,
      idCotizante: data.idCotizante,
      totalDaysLR: LRDays,
      totalDaysLNR: LNRDays,
      totalDaysLMA: LMADays,
    };

    if (LR.length > 0) {
      value.LR = LR;
    }
    if (LNR.length > 0) {
      value.LNR = LNR;
    }
    if (LMA.length > 0) {
      value.LMA = LMA;
    }

    dispatch(
      novedadLicenciasAction(LICENCIAS.LICENCIAS_SET_COTIZANTE_DATA, value)
    );
    dispatch(
      novedadLicenciasAction(LICENCIAS.LICENCIAS_OPEN_LICENCIAS_MODAL, null)
    );
  };

  const openSuspensionesModal = (data) => {
    dispatch(
      cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_DATA, {
        id: data.id,
        type: data.tipoCotizante,
        documentNumber: data.documentNumber,
      })
    );
    const value = {
      cotizanteName: data.name.display,
      cotizanteIdentificationNumber: data.documentNumber,
      cotizanteType: data.tipoCotizante,
      idCotizante: data.id,
    };
    if (data.suspensiones.length > 0) {
      value.suspensionesRows = data.suspensiones;
      value.suspensionesAmount = data.suspensiones.length;
    }
    dispatch(
      novedadSuspensionesAction(
        SUSPENSIONES.SUSPENSIONES_SAVE_COTIZANTE_DATA,
        value
      )
    );
    dispatch(
      novedadSuspensionesAction(
        SUSPENSIONES.SUSPENSIONES_OPEN_SUSPENSIONES_MODAL,
        null
      )
    );
  };

  const removeNovedadTransitoria = (
    index,
    documentNumber,
    id,
    tipoCotizante,
    idCotizante
  ) => {
    if (disableRemoveVariacionButton) return;
    dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, true));
    setDisableRemoveVariacionButton(true);

    const companyInfo = companiesInfo[activeCompany];
    let removeRequestData = {
      idAportante: activeCompany,
      cotizanteData: {
        idCotizante,
        documentNumber: documentNumber,
        companyName: companyInfo.companyName,
        idNew: id,
        province: companyInfo.location.province.value,
        city: companyInfo.location.city.value,
        tipoCotizante: tipoCotizante,
        type: 'Salarios',
      },
    };

    deleteNovedades(removeRequestData)
      .then((result) => {
        if (result.status === 'SUCCESS') {
          setDisableRemoveVariacionButton(false);
          dispatch(
            prePlanillaAction(PRE_PLANILLA.PRE_PLANILLA_REMOVE_NOVEDAD, {
              field: 'salariales',
              companyNit: activeCompany,
              identificationNumber: documentNumber,
              index,
              id,
              data: {
                transitorySalary: {
                  code: '',
                  display: '',
                },
                transitorySalaryVariation: false,
                // regLine: {
                // 	...regLine,
                // 	23: editRecordField[23](''),
                // 	40: editRecordField[40](parseInt(oldSalary)),
                // 	42: editRecordField[42](parseInt(oldSalary)),
                // 	43: editRecordField[43](parseInt(oldSalary)),
                // 	44: editRecordField[44](parseInt(oldSalary)),
                // 	45: editRecordField[45](parseInt(oldSalary))
                // }
              },
              periodoActual: periodosInfo[activeCompany].value,
              idPlanilla: periodosInfo[activeCompany].month,
            })
          );
          dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false));
        } else {
          setDisableRemoveVariacionButton(false);
          dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false));
          dispatch(
            novedadVariacionTransitoriaAction(
              VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_SET_REQUEST_STATUS,
              {
                documentNumber: documentNumber,
              }
            )
          );
        }
      })
      .catch((error) => {
        setDisableRemoveVariacionButton(false);
        dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false));
        // this.props.novedadVariacionTransitoriaAction(
        // 	VARIACION_TRANSITORIA.VARIACION_TRANSITORIA_SET_REQUEST_STATUS,
        // 	{
        // 		documentNumber: documentNumber
        // 	}
        // );
      });
  };

  const openVariacionPermanenteModal = (data) => {
    const periodoInfo = periodosInfo[activeCompany];
    const cotizanteData =
      prePlanilla[activeCompany][periodoInfo.value]['dependientes'][
        periodoInfo.month
      ][data.documentNumber];
    const novedad = cotizanteData.salariales.find(
      (item) => item.nombreNovedad === 'Ajuste de salario permanente'
    );
    const novedad_fields = {
      cotizanteName: data.displayName,
      cotizanteSalary: data.salary,
      cotizanteDocumentNumber: cotizanteData.documentNumber,
      cotizanteType: cotizanteData.tipoCotizante.value,
      idCotizante: cotizanteData.id,
      salariales: cotizanteData.salariales,
      ajustePermanente: cotizanteData.salaryVariation,
      regLine: {},
    };
    if (typeof novedad === 'object') {
      novedad_fields.dateStarted = novedad.fechaInicio;
      novedad_fields.fechaInicio = novedad.fechaInicio;
      novedad_fields.idNovedad = novedad.id;
      novedad_fields.newSalary = novedad.value.newSalary;
      novedad_fields.duracionDias = novedad.duracionDias;
      novedad_fields.cotizanteSalary = novedad.value.previousSalary;
    }
    dispatch(
      novedadVariacionPermanenteAction(
        VARIACION_PERMANENTE.VARIACION_PERMANENTE_SET_COTIZANTE_DATA,
        novedad_fields
      )
    );
    dispatch(
      novedadVariacionPermanenteAction(
        VARIACION_PERMANENTE.VARIACION_PERMANENTE_MODAL_SET_STATUS,
        null
      )
    );
  };

  return (
    <>
      <Table
        Row={(props) => (
          <RowLayout
            {...props}
            goToDetail={goToDetail}
            openIngresoModal={openIngresoModal}
            openRetiroModal={openRetiroModal}
            openNovedadTransitoria={openNovedadTransitoria}
            setTransitorySalaryValue={setTransitorySalaryValue}
            saveNovedadTransitoria={saveNovedadTransitoria}
            disableSaveVariacionButton={disableSaveVariacionButton}
            editNovedadTransitoria={editNovedadTransitoria}
            openVacacionesModal={openVacacionesModal}
            openIncapacidadesModal={openIncapacidadesModal}
            openLicenciasModal={openLicenciasModal}
            openSuspensionesModal={openSuspensionesModal}
            removeNovedadTransitoria={removeNovedadTransitoria}
            disableRemoveVariacionButton={disableRemoveVariacionButton}
            openVariacionPermanenteModal={openVariacionPermanenteModal}
          />
        )}
        Head={HeaderLayout}
        data={cotizantes}
        withTabs={true}
        tabsArray={['Dependientes']}
        TextInputProps={{
          placeholder: 'Buscar empleado',
        }}
        filterKeys={['documentNumber', 'name.display']}
      />
    </>
  );
};

const HeaderLayout = () => {
  const classes = useStyles();
  return (
    <TableHead>
      <TableRow>
        <TableCell className={classes.headerTitle}>Empleado</TableCell>
        <TableCell
          align="center"
          className={clsx(classes.headerTitle, classes.salaryCell)}
        >
          Salario/IBC
        </TableCell>
        <TableCell align="center" className={classes.headerTitle}>
          Vacaciones
        </TableCell>
        <TableCell align="center" className={classes.headerTitle}>
          Incapacidades
        </TableCell>
        <TableCell align="center" className={classes.headerTitle}>
          Licencias
        </TableCell>
        <TableCell align="center" className={classes.headerTitle}>
          Suspensiones
        </TableCell>
        <TableCell align="right" className={classes.headerTitle}>
          Total a pagar
        </TableCell>
      </TableRow>
    </TableHead>
  );
};

const RowLayout = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    row,
    goToDetail,
    openIngresoModal,
    openRetiroModal,
    openNovedadTransitoria,
    setTransitorySalaryValue,
    saveNovedadTransitoria,
    disableSaveVariacionButton,
    editNovedadTransitoria,
    openVacacionesModal,
    openIncapacidadesModal,
    openLicenciasModal,
    openSuspensionesModal,
    removeNovedadTransitoria,
    disableRemoveVariacionButton,
    openVariacionPermanenteModal,
    index,
  } = props;

  const {
    name,
    documentNumber,
    genre,
    newEntry,
    firstPila,
    retired,
    id,
    tipoCotizante,
    salary,
    regLine,
    transitorySalary,
    salaryVariation,
    documentType,
    subsistemas,
  } = row;

  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const activeCorreciones = useSelector(
    (state) => state.currentUser.activeCorreciones
  );
  const periodoInfo = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const correcionesInfo = useSelector(
    (state) => state.currentUser.correcionesInfo[activeCompany]
  );
  const prePlanilla = useSelector((state) => state.prePlanilla);
  const [vacacionesDays, setVacacionesDays] = useState(0);
  const [incapacidadesDays, setIncapacidadesDays] = useState(0);
  const [licenciasDays, setLicenciasDays] = useState(0);
  const [suspensionesDays, setSuspensionesDays] = useState(0);
  const [totalAportesValue, setTotalAportesValue] = useState(0);

  const novedadTransitoria = useSelector(
    (state) => state.novedadVariacionTransitoria[documentNumber]
  );
  const transitorySalaryVariation =
    prePlanilla[activeCompany][
      activeCorreciones ? correcionesInfo.value : periodoInfo.value
    ]['dependientes'][
      activeCorreciones ? correcionesInfo.month : periodoInfo.month
    ][documentNumber].transitorySalaryVariation;

  const vacaciones =
    prePlanilla[activeCompany][
      activeCorreciones ? correcionesInfo.value : periodoInfo.value
    ]['dependientes'][
      activeCorreciones ? correcionesInfo.month : periodoInfo.month
    ][documentNumber].vacaciones;
  const incapacidades =
    prePlanilla[activeCompany][
      activeCorreciones ? correcionesInfo.value : periodoInfo.value
    ]['dependientes'][
      activeCorreciones ? correcionesInfo.month : periodoInfo.month
    ][documentNumber].incapacidades;
  const licencias =
    prePlanilla[activeCompany][
      activeCorreciones ? correcionesInfo.value : periodoInfo.value
    ]['dependientes'][
      activeCorreciones ? correcionesInfo.month : periodoInfo.month
    ][documentNumber].licencias;
  const suspensiones =
    prePlanilla[activeCompany][
      activeCorreciones ? correcionesInfo.value : periodoInfo.value
    ]['dependientes'][
      activeCorreciones ? correcionesInfo.month : periodoInfo.month
    ][documentNumber].suspensiones;
  const totalAportes =
    prePlanilla[activeCompany][
      activeCorreciones ? correcionesInfo.value : periodoInfo.value
    ]['dependientes'][
      activeCorreciones ? correcionesInfo.month : periodoInfo.month
    ][documentNumber].totalAportes;

  useEffect(() => {
    const sum = vacaciones.reduce(
      (total, current) => total + current.duracionDias,
      0
    );
    setVacacionesDays(sum);
  }, [vacaciones]);

  useEffect(() => {
    const sum = incapacidades.reduce(
      (total, current) => total + current.duracionDias,
      0
    );
    setIncapacidadesDays(sum);
  }, [incapacidades]);

  useEffect(() => {
    const sum = licencias.reduce(
      (total, current) => total + current.duracionDias,
      0
    );
    setLicenciasDays(sum);
  }, [licencias]);

  useEffect(() => {
    const sum = suspensiones.reduce(
      (total, current) => total + current.duracionDias,
      0
    );
    setSuspensionesDays(sum);
  }, [suspensiones]);

  useEffect(() => {
    let sum = 0;
    if (totalAportes) {
      Object.keys(totalAportes).forEach((subsistema) => {
        sum += totalAportes[subsistema];
      });
    }
    setTotalAportesValue(sum);
  }, [totalAportes]);

  const showVacacionesModal = () => {
    const data = {
      id,
      tipoCotizante,
      documentNumber,
      name,
      salary,
      vacaciones,
    };
    openVacacionesModal(data);
  };

  const showIncapacidadesModal = () => {
    const data = {
      incapacidades,
      name,
      salary,
      documentNumber,
      tipoCotizante,
      idCotizante: id,
    };
    openIncapacidadesModal(data);
  };

  const showLicenciasModal = () => {
    const data = {
      licencias,
      name,
      salary,
      documentNumber,
      tipoCotizante,
      idCotizante: id,
    };
    openLicenciasModal(data);
  };

  const showSuspensionesModal = () => {
    const data = {
      id,
      tipoCotizante,
      documentNumber,
      name,
      salary,
      suspensiones,
    };
    openSuspensionesModal(data);
  };

  const handleDetalleAportes = () => {
    dispatch(
      detalleAportesModalAction(DETALLE_APORTES_MODAL.OPEN_DA_MODAL, {
        cotizanteName: name.display,
        cotizanteDocumentType: documentType,
        cotizanteDocumentNumber: documentNumber,
        tarifas: subsistemas,
        total: totalAportes,
      })
    );
  };

  return (
    <TableRow className={classes.tableRow}>
      <TableCell className={classes.userInfoCell}>
        <UserInfoCard
          name={name.display}
          documentNumber={documentNumber}
          genre={genre.value}
          newEntry={newEntry}
          firstPila={firstPila}
          retired={retired}
          goToDetail={goToDetail}
          id={id}
          tipoCotizante={tipoCotizante}
          openIngresoModal={openIngresoModal}
          salary={salary.value}
          openRetiroModal={openRetiroModal}
        />
      </TableCell>
      <TableCell align="center">
        <SalaryComponent
          novedadTransitoria={novedadTransitoria}
          openNovedadTransitoria={openNovedadTransitoria}
          documentNumber={documentNumber}
          setTransitorySalaryValue={setTransitorySalaryValue}
          saveNovedadTransitoria={saveNovedadTransitoria}
          salary={salary.value}
          regLine={regLine}
          transitorySalary={transitorySalary}
          tipoCotizante={tipoCotizante}
          disableSaveVariacionButton={disableSaveVariacionButton}
          editNovedadTransitoria={editNovedadTransitoria}
          transitorySalaryVariation={transitorySalaryVariation}
          removeNovedadTransitoria={removeNovedadTransitoria}
          disableRemoveVariacionButton={disableRemoveVariacionButton}
          salaryVariation={salaryVariation}
          openVariacionPermanenteModal={openVariacionPermanenteModal}
          name={name}
          id={id}
          index={index}
        />
      </TableCell>
      <TableCell align="center">
        <div className={classes.columnCellContainer}>
          {vacacionesDays ? (
            <NovedadContador
              novedadText={'V'}
              durationDays={vacacionesDays}
              backgroundColor={'#5E35B126'}
              typographyColor={'#5E35B1'}
              onClick={showVacacionesModal}
            />
          ) : (
            <AddNovedad text="Vacaciones" onPress={showVacacionesModal} />
          )}
        </div>
      </TableCell>
      <TableCell align="center">
        <div className={classes.columnCellContainer}>
          {incapacidadesDays ? (
            <NovedadContador
              novedadText={'I'}
              durationDays={incapacidadesDays}
              backgroundColor={'#5E35B126'}
              typographyColor={'#5E35B1'}
              onClick={showIncapacidadesModal}
            />
          ) : (
            <AddNovedad text="Incapacidades" onPress={showIncapacidadesModal} />
          )}
        </div>
      </TableCell>
      <TableCell align="center">
        <div className={classes.columnCellContainer}>
          {licenciasDays ? (
            <NovedadContador
              novedadText={'L'}
              durationDays={licenciasDays}
              backgroundColor={'#5E35B126'}
              typographyColor={'#5E35B1'}
              onClick={showLicenciasModal}
            />
          ) : (
            <AddNovedad text="Licencias" onPress={showLicenciasModal} />
          )}
        </div>
      </TableCell>
      <TableCell align="center">
        <div className={classes.columnCellContainer}>
          {suspensionesDays ? (
            <NovedadContador
              novedadText={'S'}
              durationDays={suspensionesDays}
              backgroundColor={'#5E35B126'}
              typographyColor={'#5E35B1'}
              onClick={showSuspensionesModal}
            />
          ) : (
            <AddNovedad text="Suspensiones" onPress={showSuspensionesModal} />
          )}
        </div>
      </TableCell>
      <TableCell align="right">
        <div className={classes.totalContentContainer}>
          <Typography className={classes.totalText} variant="body1" noWrap>
            {`$ ${HelperFunctions.formatStringNumber(totalAportesValue)}`}
          </Typography>
          <IconButton
            size="small"
            filled={false}
            onClick={handleDetalleAportes}
          >
            <VisibilityIcon />
          </IconButton>
        </div>
      </TableCell>
    </TableRow>
  );
};

const UserInfoCard = (props) => {
  const classes = useStyles();
  const {
    name,
    documentNumber,
    genre,
    newEntry,
    retired,
    goToDetail,
    id,
    tipoCotizante,
    openIngresoModal,
    salary,
    openRetiroModal,
  } = props;

  const showCotizanteDetail = () => {
    goToDetail(id, tipoCotizante, documentNumber);
  };

  const openEntryModal = (e) => {
    e.stopPropagation();
    const data = {
      id,
      tipoCotizante,
      documentNumber,
      name,
      salary,
    };
    openIngresoModal(data);
  };

  const openRetireModal = (e) => {
    e.stopPropagation();
    const data = {
      id,
      tipoCotizante,
      documentNumber,
      name,
      salary,
    };
    openRetiroModal(data);
  };

  return (
    <div className={classes.userInfoContainer} onClick={showCotizanteDetail}>
      <img
        alt="User avatar"
        className={classes.userInfoImage}
        src={
          genre
            ? require(`../../assets/img/women_one.svg`)
            : require(`../../assets/img/men_one.svg`)
        }
      />
      <div className={classes.userInfoTextContainer}>
        <Typography className={classes.userInfoName} noWrap>
          {name}
        </Typography>
        <Typography className={classes.userInfoDocument} noWrap>
          {HelperFunctions.formatStringNumber(documentNumber)}
        </Typography>
        <div className={classes.novedadesChipsContainer}>
          <div className={classes.chipsContainerOne}>
            <Chip
              label={'Ingreso'}
              outlined={true}
              clickable
              handleclick={openEntryModal}
              size="small"
              chipcolor={newEntry ? 'info' : 'grey'}
            />
            <Chip
              label={'Retiro'}
              outlined={true}
              clickable
              handleclick={openRetireModal}
              size="small"
              chipcolor={retired ? 'error' : 'grey'}
              style={{ marginLeft: 8 }}
            />
          </div>
          {/* <div className={classes.chipsContainerTwo}>
						{
							firstPila && !newEntry ?
								<Chip
									className={classes.novedadChip}
									label={'Ingresar cotizante'}
									variant='outlined'
									clickable
									onClick={openEntryModal}
								/>
								: null
						}
					</div> */}
        </div>
      </div>
    </div>
  );
};

const SalaryComponent = (props) => {
  const classes = useStyles();
  const {
    novedadTransitoria,
    openNovedadTransitoria,
    documentNumber,
    setTransitorySalaryValue,
    saveNovedadTransitoria,
    salary,
    regLine,
    transitorySalary,
    tipoCotizante,
    disableSaveVariacionButton,
    editNovedadTransitoria,
    removeNovedadTransitoria,
    disableRemoveVariacionButton,
    salaryVariation,
    openVariacionPermanenteModal,
    name,
    id,
    index,
  } = props;
  const sumSalary = +salary + +novedadTransitoria.cotizanteTransitorySalary;
  const selectedRowIndex = useSelector(
    (state) => state.payPila.currentRowIndex
  );
  const dispatch = useDispatch();
  const [salaryMenuAnchor, setSalaryMenuAnchor] = useState(null);

  const openSalaryMenu = (e) => {
    setSalaryMenuAnchor(e.currentTarget);
  };

  const showNovedadTransitoria = () => {
    openNovedadTransitoria(documentNumber);
  };

  const getVariacionValue = (event) => {
    const salaryValue = event.target.value.replace(/[.\s]/g, '');
    setTransitorySalaryValue(salaryValue, documentNumber);
  };

  const saveTransitorySalary = () => {
    saveNovedadTransitoria(
      documentNumber,
      novedadTransitoria.cotizanteTransitorySalary,
      salary,
      novedadTransitoria.editedData,
      novedadTransitoria.preLoadedData,
      regLine,
      transitorySalary,
      novedadTransitoria.index,
      tipoCotizante,
      id
    );
  };

  const editTransitorySalary = () => {
    editNovedadTransitoria(documentNumber);
  };

  const deleteNovedadTransitoria = () => {
    removeNovedadTransitoria(
      novedadTransitoria.index,
      documentNumber,
      novedadTransitoria.id,
      tipoCotizante.value,
      id
    );
  };

  const showAjustePermanenteModal = () => {
    openVariacionPermanenteModal({
      displayName: name.display,
      salary,
      documentNumber,
    });
  };

  const setCurrentIndex = () => {
    if (selectedRowIndex !== index) {
      dispatch(payPilaAction(PAY_PILA.PAY_PILA_SAVE_CURRENT_ROW_INDEX, index));
    }
  };

  const onClose = () => {
    setSalaryMenuAnchor(null);
  };

  const SALARY_MENU_OPTIONS = [
    {
      label: 'Ajuste transitorio',
      action: () => {
        showNovedadTransitoria();
        onClose();
      },
    },
    {
      label: 'Ajuste permanente',
      action: () => {
        showAjustePermanenteModal();
        onClose();
      },
    },
  ];

  return (
    <div className={classes.salaryContainer} onMouseEnter={setCurrentIndex}>
      {!novedadTransitoria.opened ? (
        <div className={classes.salaryValueContainer}>
          <Typography
            className={clsx(classes.salaryValueText, {
              [classes.salaryWithTransitoria]: novedadTransitoria.existing,
            })}
          >
            {`$ ${HelperFunctions.formatStringNumber(sumSalary)}`}
          </Typography>
          {!novedadTransitoria.existing ? (
            <>
              <Tooltip
                className={classes.tooltipContainer}
                classes={{
                  tooltip: classes.salaryTooltip,
                  arrow: classes.salaryTooltipArrow,
                }}
                arrow
                title={
                  <>
                    <b>{'Ajuste transitorio:'}</b> {' Solo si el'}
                    <br />
                    {'salario del cotizante cambia para'}
                    <br />
                    <b>{'este'}</b> {'periodo.'}
                    <br />
                  </>
                }
                placement="top"
              >
                <ButtonBase
                  className={classes.salaryEditContainer}
                  onClick={openSalaryMenu}
                >
                  <EditRoundedIcon className={classes.salaryEditIcon} />
                  <ExpandMoreIcon style={{ fontSize: '0.8rem' }} />
                </ButtonBase>
              </Tooltip>
              <FloatingMenu
                options={SALARY_MENU_OPTIONS}
                anchorEl={salaryMenuAnchor}
                onClose={onClose}
              />
            </>
          ) : null}
        </div>
      ) : (
        <div className={classes.salaryValueContainer}>
          <TextField
            variant="outlined"
            value={novedadTransitoria.cotizanteTransitorySalary}
            size="small"
            className={classes.salaryTextInput}
            placeholder="Ajuste"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Typography className={classes.inputAdornment}>$</Typography>
                </InputAdornment>
              ),
              classes: {
                input: classes.ajusteTransitorioInput,
              },
              inputComponent: MaskedSalaryInput,
            }}
            inputProps={{
              style: {
                padding: '5px 10px 5px 0px',
              },
            }}
            classes={{
              root: classes.searchBarBorder,
            }}
            onChange={getVariacionValue}
          />
          {disableSaveVariacionButton && selectedRowIndex === index ? (
            <CircularProgress size={20} className={classes.ajusteSpinner} />
          ) : (
            <IconButton
              size="small"
              onClick={saveTransitorySalary}
              disabled={disableSaveVariacionButton}
            >
              <DoneRoundedIcon className={classes.ajusteDoneIcon} />
            </IconButton>
          )}
        </div>
      )}
      <div className={classes.variacionesContainer}>
        {novedadTransitoria.existing ? (
          <div className={classes.variacionContainer}>
            <Chip
              label={
                <Typography className={classes.chipTransitorioText}>
                  Ajuste transitorio
                </Typography>
              }
              outlined={true}
              clickable
              size="small"
              chipcolor="primary"
              handleclick={editTransitorySalary}
            />
            {disableRemoveVariacionButton && selectedRowIndex === index ? (
              <CircularProgress
                size={20}
                className={classes.ajusteRemoveSpinner}
              />
            ) : (
              <IconButton
                size="small"
                className={classes.ajusteRemoveButton}
                onClick={deleteNovedadTransitoria}
                disabled={disableRemoveVariacionButton}
              >
                <RemoveCircleOutlineRoundedIcon
                  className={classes.ajusteRemoveIcon}
                />
              </IconButton>
            )}
          </div>
        ) : null}
        {salaryVariation ? (
          <div className={classes.variacionContainer}>
            <Chip
              label={
                <Typography className={classes.chipPermanenteText}>
                  Ajuste permanente
                </Typography>
              }
              outlined={true}
              clickable
              size="small"
              chipcolor="info"
              handleclick={showAjustePermanenteModal}
            />
            {/* {
								false ?
									<IconButton size='small' className={classes.ajusteRemoveButton}>
										<RemoveCircleOutlineRoundedIcon className={classes.ajusteRemoveIcon} />
									</IconButton>
									:
									<CircularProgress size={15} className={classes.ajusteRemoveSpinner} />
							} */}
          </div>
        ) : null}
      </div>
    </div>
  );
};

const AddNovedad = ({ text, onPress }) => {
  const classes = useStyles();
  return (
    <div className={classes.addNovedadContainer}>
      <Button
        onClick={onPress}
        variant="text"
        startIcon={<AddCircleOutlineRoundedIcon />}
      >
        {text}
      </Button>
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  emptyContainer: {
    height: '80%',
  },
  userInfoCell: {
    width: '20%',
  },
  columnCellContainer: {
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
  },
  salaryCell: {
    width: '12%',
  },
  tableRow: {
    height: 110,
  },
  spinner: {
    color: '#6A32B5',
  },
  tableSpinner: {
    color: '#6A32B5',
  },
  tableContainer: {
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    height: 'content-fit',
    overflow: 'hidden',
    boxShadow: '0px 3px 16px #00000029',
    borderRadius: 7,
  },
  headerTitle: {
    fontSize: '1rem',
    fontWeight: 'bold',
    color: '#263238',
  },
  searchBarContainer: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#5E35B126',
    padding: '1em 0em 1em 2em',
  },
  searchBarInput: {
    fontSize: '0.9rem',
  },
  searchBarIcon: {
    color: '#C6CCD0',
    fontSize: '1.2rem',
  },
  searchBar: {
    backgroundColor: '#FFFFFF',
    borderRadius: 7,
  },
  searchBarBorder: {
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#5E35B1',
      },
      '&:hover fieldset': {
        borderColor: '#5E35B1',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#5E35B1',
      },
    },
  },
  tabsContainer: {
    padding: '10px 0px 0px 15px',
  },
  tabContentContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabsIndicator: {
    backgroundColor: 'transparent',
    '& > div': {
      maxWidth: '90%',
      width: '100%',
      backgroundColor: '#5E35B1',
    },
    display: 'flex',
    justifyContent: 'center',
  },
  tabTextOne: {
    textTransform: 'none',
    color: '#5E35B1',
    fontWeight: 'bold',
    fontSize: '1.1rem',
  },
  tabTextTwo: {
    textTransform: 'none',
    color: '#95989A',
    fontSize: '1.1rem',
  },
  tabNumberContainerOne: {
    width: 25,
    height: 25,
    borderRadius: 100,
    backgroundColor: '#5E35B126',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 8,
  },
  tabNumberContainerTwo: {
    width: 25,
    height: 25,
    borderRadius: 100,
    backgroundColor: '#95989A26',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 8,
  },
  tabNumberTextOne: {
    color: '#5E35B1',
    fontSize: '0.95rem',
  },
  tabNumberTextTwo: {
    color: '#95989A',
    fontSize: '0.95rem',
  },
  userInfoContainer: {
    display: 'flex',
    width: '100%',
    cursor: 'pointer',
  },
  userInfoImage: {
    height: 'auto',
    width: 40,
  },
  userInfoTextContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    marginLeft: 10,
  },
  userInfoName: {
    fontWeight: 'bold',
    fontSize: '1.1rem',
    color: '#263238',
  },
  userInfoDocument: {
    fontSize: '0.9rem',
    color: '#263238',
  },
  novedadesChipsContainer: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-start',
    flex: 1,
    flexDirection: 'column',
  },
  novedadChip: {
    marginLeft: 8,
  },
  variacionChip: {
    color: '#5E35B1',
    backgroundColor: '#5E35B126',
  },
  ingresoChip: {
    color: '#2962FF',
    backgroundColor: '#2962FF26',
    marginRight: 5,
  },
  retiroChip: {
    color: '#FF495A',
    backgroundColor: '#FF495A26',
    marginRight: 5,
  },
  chipsContainerOne: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 5,
  },
  salaryContainer: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  salaryEditContainer: {
    width: 36,
    height: 25,
    borderRadius: 5,
    backgroundColor: theme.palette.primary[200],
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: theme.palette.primary.main,
  },
  salaryEditIcon: {
    color: theme.palette.primary.main,
    fontSize: '0.9rem',
  },
  salaryValueContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    maxWidth: 143,
    minWidth: 143,
  },
  salaryValueText: {
    color: '#263238',
    fontSize: '1rem',
  },
  tooltipContainer: {
    marginLeft: 10,
  },
  salaryTooltip: {
    backgroundColor: '#2962FF',
    fontSize: '0.7rem',
  },
  salaryTooltipArrow: {
    color: '#2962FF',
  },
  inputAdornment: {
    color: '#5E35B1',
  },
  ajusteTransitorioInput: {
    fontSize: '0.9rem',
  },
  salaryTextInput: {
    width: 120,
    borderRadius: 7,
    marginRight: 10,
  },
  ajusteDoneIcon: {
    color: '#2962FF',
    fontSize: '1.3rem',
  },
  ajusteSpinner: {
    color: '#2962FF',
  },
  variacionesContainer: {
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 5,
  },
  ajusteRemoveIcon: {
    color: '#FF495A',
    fontSize: '1rem',
  },
  ajusteRemoveButton: {
    marginLeft: 5,
  },
  ajusteRemoveSpinner: {
    color: '#FF495A',
    marginLeft: 5,
  },
  variacionContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 5,
  },
  variacionPermanenteChip: {
    color: '#2962FF',
    backgroundColor: '#2962FF26',
  },
  ajusteEditIcon: {
    fontSize: '0.5rem',
  },
  chipLabelContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  chipTransitorioText: {
    color: '#5E35B1',
    fontSize: '0.75rem',
  },
  chipTransitorioIcon: {
    color: '#5E35B1',
    fontSize: '0.6rem',
    marginLeft: 3,
  },
  chipPermanenteText: {
    color: '#2962FF',
    fontSize: '0.75rem',
  },
  chipPermanenteIcon: {
    color: '#2962FF',
    fontSize: '0.6rem',
    marginLeft: 3,
  },
  addNovedadContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addNovedadIcon: {
    color: '#5E35B1',
    fontSize: '1.8rem',
  },
  addNovedadText: {
    color: '#5E35B1',
    fontSize: '1rem',
    fontWeight: 'bold',
    marginLeft: 3,
  },
  totalText: {
    color: theme.palette.common.black,
    fontWeight: 'bold',
    marginRight: theme.spacing(1),
  },
  totalContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  emptyDataContainer: {
    minHeight: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tableImage: {
    cursor: 'pointer',
    width: 300,
    height: 'auto',
  },
  salaryWithTransitoria: {
    fontWeight: 'bold',
    color: '#5E35B1',
  },
  totalContentContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
}));

export default PilaTable;
