/* eslint-disable no-throw-literal */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import PILALayout from './PILALayout';
import {
  payPilaAction,
  novedadVacacionesAction,
  prePlanillaAction,
  cotizantesInfoAction,
  novedadVariacionPermanenteAction,
  novedadSuspensionesAction,
  navigationAction,
  snackBarAction,
} from './../redux/Actions';
import { PAY_PILA, NAVIGATION, SNACKBAR } from './../redux/ActionTypes';
import PictureAsPdfOutlinedIcon from '@material-ui/icons/PictureAsPdfOutlined';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';
import BugReportTwoToneIcon from '@material-ui/icons/BugReportTwoTone';
import DescriptionTwoToneIcon from '@material-ui/icons/DescriptionTwoTone';
import PictureAsPdfTwoToneIcon from '@material-ui/icons/PictureAsPdfTwoTone';
import SendTwoToneIcon from '@material-ui/icons/SendTwoTone';
import { DOWNLOAD_OPTIONS, RECEIPT_OPTIONS } from './Constants';
import moment from 'moment';
import 'moment/locale/es';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import CreatePlanillaFile from './api/CreatePlanillaFile';
import CheckPlanillaData from './api/CheckPlanillaData';
import CreateNewPlanilla from './api/CreateNewPlanilla';
import SendPayButton from './api/SendPayButton';

class PILA extends Component {
  state = {
    checkStepStatus: false,
    createStepStatus: false,
    validateStepStatus: false,
    disableVariacionPermanenteButton: false,
    disablePayButton: false,
    disableSendPayButton: false,
  };

  componentDidMount() {
    ChangePageTitle('Liquidar PILA | Pilalá');
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'liquidarPila'
    );
    this.setDates();
  }

  componentWillUnmount() {
    this.props.payPilaAction(PAY_PILA.PAY_PILA_CHANGE_CURRENT_STEP, 1);
  }

  setDates = () => {
    let dateInfo = this.props.periodosInfo[this.props.activeCompany];
    if (dateInfo.display) {
      let startDate = moment(dateInfo.display, 'DD-MM-YYYY')
        .startOf('month')
        .format('DD/MM/YYYY');
      let finalDate = moment(dateInfo.display, 'DD-MM-YYYY')
        .endOf('month')
        .format('DD/MM/YYYY');
      if (startDate && finalDate) {
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SAVE_INITIAL_DATE,
          startDate
        );
        this.props.payPilaAction(PAY_PILA.PAY_PILA_SAVE_FINAL_DATE, finalDate);
      }
    }
  };

  saveAndContinueAction = async () => {
    const { prePlanilla, activeCompany, periodosInfo } = this.props;
    const periodo = periodosInfo[activeCompany];
    const prePlanillaData =
      prePlanilla[activeCompany][periodo.value]['dependientes'][periodo.month];
    if (Object.keys(prePlanillaData).length > 0) {
      let planillaInfo;
      const areCredentialsEnabled = this.props.accountManager.length > 0;
      this.props.payPilaAction(PAY_PILA.PAY_PILA_VALIDATE_CURRENT_PILA, null);
      this.props.payPilaAction(
        PAY_PILA.UPDATE_CREDENTIALS_ENABLED,
        areCredentialsEnabled
      );
      try {
        const { value: idPeriodo, month: idPlanilla } = this.props.periodosInfo[
          this.props.activeCompany
        ];
        const { value: city } = this.props.companyCity;
        const { value: province } = this.props.companyProvince;
        const { companyIdentificationType: idType } = this.props;
        const { activeCompanyNit: idNumber } = this.props;
        const checkDataResult = await CheckPlanillaData(
          this.props.activeCompany,
          idPeriodo,
          idPlanilla,
          city,
          province
        );
        console.log('Resultado check data', checkDataResult);
        // Definir que hacer con los datos de la respuesta de la información chequeada
        if (checkDataResult.status === 'SUCCESS') {
          planillaInfo = checkDataResult.data;
          this.setState({
            checkStepStatus: true,
          });
          const pilaFileParams = {
            idType: areCredentialsEnabled ? idType : '',
            idNumber: areCredentialsEnabled ? idNumber : '',
            idCompany: activeCompany,
            companyCity: city,
            companyProvince: province,
            idPlanilla,
            idPeriodo,
            validatePlanilla: areCredentialsEnabled,
          };
          const fileResult = await CreatePlanillaFile(pilaFileParams);
          console.log('Resultado archivo', fileResult);
          if (fileResult.status === 'SUCCESS') {
            if (areCredentialsEnabled) {
              this.setState({
                createStepStatus: true,
              });
            }
            const {
              HealthSystemPeriod,
              totalAPagar,
              cotizantesNumber,
            } = planillaInfo;
            const periodoActual = this.props.periodosInfo[this.props.idCompany];
            const planillas = [
              {
                idPlanilla: areCredentialsEnabled
                  ? fileResult.data.numeroPlanilla
                  : null,
                tipoCotizante: 'dependientes',
              },
            ];
            const tipoPlanilla = {
              display: 'Liquidación',
              correction: false,
              default: true,
            };
            if (!areCredentialsEnabled) {
              this.props.payPilaAction(
                PAY_PILA.SAVE_LAST_PILA_FILE_PATH,
                fileResult.data.filePath
              );
            }
            const newPlanillaResult = await CreateNewPlanilla(
              this.props.adminEmail,
              this.props.idCompany,
              planillas,
              tipoPlanilla,
              periodoActual,
              totalAPagar,
              fileResult.data.filePath,
              {
                value: areCredentialsEnabled,
                code: areCredentialsEnabled ? 'arus' : null,
              }
            );

            if (newPlanillaResult.status === 'SUCCESS') {
              const periodo = moment(HealthSystemPeriod, 'YYYY-MM').format(
                'MMMM'
              );
              const planillaItem = {
                total: totalAPagar,
                numeroPlanilla: fileResult.data.numeroPlanilla ?? '***********',
                periodo,
                tipo: 'Planilla de empleados',
                numeroCotizantes: cotizantesNumber,
                filePath: fileResult.data.filePath,
              };
              this.props.payPilaAction(
                PAY_PILA.PAY_PILA_ADD_PLANILLA_TO_PAY,
                planillaItem
              );
              this.setState({
                createStepStatus: true,
                validateStepStatus: true,
              });
              this.props.payPilaAction(
                PAY_PILA.PAY_PILA_CHANGE_CURRENT_STEP,
                2
              );
              this.props.payPilaAction(
                PAY_PILA.PAY_PILA_VALIDATE_CURRENT_PILA,
                null
              );
              this.props.history.push(
                `/${this.props.match.params.userId}/console/liquidarPila/${this.props.currentStep}`
              );
            } else {
              throw { errorCode: 'newPlanilla_error' };
            }
          } else {
            throw {
              errorCode: 'createFile_error',
              message: fileResult.message
                ? fileResult.message
                : 'Ocurrió algo inesperado, intenta nuevamente',
            };
          }
        } else {
          throw { errorCode: 'checkData_error' };
        }
      } catch (data) {
        if (data.errorCode !== undefined) {
          switch (data.errorCode) {
            case 'checkData_error':
              this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: 'Error al verificar información',
                description:
                  'Hemos tenido problemas para verificar la información de tu planilla, inténtalo nuevamente',
                color: 'error',
              });

              break;

            case 'createFile_error':
              this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: 'Error al crear archivo plano',
                description: data.message,
                color: 'error',
              });
              break;

            case 'newPlanilla_error':
              this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: 'Error al crear planilla',
                description:
                  'Hemos tenido problemas para crear la nueva planilla, inténtalo nuevamente',
                color: 'error',
              });
              break;

            default:
              this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: 'Error en módulo liquidar PILA',
                description:
                  'Nuestro módulo de liquidar PILA presenta fallas, inténtalo más tarde',
                color: 'error',
              });
              break;
          }
        } else {
          console.log('Error interno módulo pila', data);
        }
        this.props.payPilaAction(PAY_PILA.PAY_PILA_VALIDATE_CURRENT_PILA, null);
        this.setState({
          checkStepStatus: false,
          createStepStatus: false,
          validateStepStatus: false,
        });
      }
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Información requerida',
        description: 'Para guardar tu planilla debes agregar cotizantes',
        color: 'warning',
      });
    }
  };

  returnToFirstStep = () => {
    this.setState({
      checkStepStatus: false,
      createStepStatus: false,
      validateStepStatus: false,
    });
    this.props.payPilaAction(PAY_PILA.PAY_PILA_CHANGE_CURRENT_STEP, 1);
    this.props.payPilaAction(PAY_PILA.SAVE_LAST_PILA_FILE_PATH, null);
    this.props.payPilaAction(PAY_PILA.UPDATE_CREDENTIALS_ENABLED, false);
    this.props.history.push(
      `/${this.props.match.params.userId}/console/liquidarPila/1`
    );
  };

  openSendPayButtonModal = () => {
    if (!this.props.showSendPayButtonModal) {
      this.props.payPilaAction(
        PAY_PILA.PAY_PILA_SHOW_SEND_PAY_BUTTON_MODAL,
        null
      );
    }
  };

  continueSendPayButton = () => {
    if (this.props.showSendPayButtonModal) {
      if (
        /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
          this.props.payButtonEmail
        )
      ) {
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SHOW_SEND_PAY_BUTTON_MODAL,
          null
        );
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL,
          null
        );
      } else {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description: 'Ingrese el correo electrónico del destinatario',
          color: 'warning',
        });
      }
    }
  };

  confirmSendPayButtonModal = async (index) => {
    if (this.state.disableSendPayButton) return;
    this.setState({
      disableSendPayButton: true,
    });

    const {
      adminName,
      currentCompanyName,
      companyIdentificationType,
      activeCompanyNit,
      payButtonEmail,
      planillas,
    } = this.props;
    const { total, numeroPlanilla, numeroCotizantes } = planillas[index];
    if (this.props.showConfirmSendPayButtonModal) {
      const payButtonResult = await SendPayButton(
        adminName,
        numeroPlanilla,
        currentCompanyName,
        numeroCotizantes,
        total,
        'dependientes',
        companyIdentificationType,
        activeCompanyNit,
        payButtonEmail
      );
      if (payButtonResult.status === 'SUCCESS') {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Envío exitoso',
          description: 'El botón de pago ha sido enviado correctamente',
          color: 'success',
        });
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL,
          null
        );
        this.props.payPilaAction(PAY_PILA.PAY_PILA_SAVE_PAY_BUTTON_EMAIL, '');
        this.setState({
          disableSendPayButton: false,
        });
      } else {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error al enviar botón de pago',
          description:
            'Hemos tenido problemas para enviar el botón pago, inténtalo nuevamente',
          color: 'error',
        });
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL,
          null
        );
        this.setState({
          disableSendPayButton: false,
        });
      }
    }
  };

  cancelSendPayButton = () => {
    if (this.props.showSendPayButtonModal) {
      this.props.payPilaAction(
        PAY_PILA.PAY_PILA_SHOW_SEND_PAY_BUTTON_MODAL,
        null
      );
    }

    if (this.props.showConfirmSendPayButtonModal) {
      this.props.payPilaAction(
        PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL,
        null
      );
    }
    this.props.payPilaAction(PAY_PILA.PAY_PILA_SAVE_PAY_BUTTON_EMAIL, '');
  };

  getPlanillaPayLink = (numeroPlanilla) => {
    this.props.history.replace(
      `/paymentCheck?NP=${numeroPlanilla}&IT=${this.props.companyIdentificationType}&IN=${this.props.activeCompanyNit}`
    );
  };

  returnToControlPanel = () => {
    this.props.payPilaAction(PAY_PILA.PAY_PILA_CHANGE_CURRENT_STEP, 1);
    this.props.payPilaAction(PAY_PILA.SAVE_LAST_PILA_FILE_PATH, null);
    this.props.payPilaAction(PAY_PILA.UPDATE_CREDENTIALS_ENABLED, false);
    this.props.history.replace(
      `/${this.props.match.params.userId}/console/controlPanel`
    );
  };

  render() {
    const stepperOptions = [
      {
        label: DOWNLOAD_OPTIONS.DOWNLOAD_EXCEL,
        icon: <DescriptionOutlinedIcon />,
        action: () => {},
      },
      {
        label: DOWNLOAD_OPTIONS.DOWNLOAD_PDF,
        icon: <PictureAsPdfOutlinedIcon />,
        action: () => {},
      },
    ];

    const receiptOptions = [
      {
        label: RECEIPT_OPTIONS[1],
        icon: (
          <BugReportTwoToneIcon
            style={{ fontSize: '1.5em', color: '#757AFF', marginRight: 5 }}
          />
        ),
        action: () => {},
      },
      {
        label: RECEIPT_OPTIONS[2],
        icon: (
          <DescriptionTwoToneIcon
            style={{ fontSize: '1.5em', color: '#757AFF', marginRight: 5 }}
          />
        ),
        action: () => {},
      },
      {
        label: RECEIPT_OPTIONS[3],
        icon: (
          <PictureAsPdfTwoToneIcon
            style={{ fontSize: '1.5em', color: '#757AFF', marginRight: 5 }}
          />
        ),
        action: () => {},
      },
      {
        label: RECEIPT_OPTIONS[4],
        icon: (
          <PictureAsPdfTwoToneIcon
            style={{ fontSize: '1.5em', color: '#757AFF', marginRight: 5 }}
          />
        ),
        action: () => {},
      },
      {
        label: RECEIPT_OPTIONS[5],
        icon: (
          <PictureAsPdfTwoToneIcon
            style={{ fontSize: '1.5em', color: '#757AFF', marginRight: 5 }}
          />
        ),
        action: () => {},
      },
      {
        label: RECEIPT_OPTIONS[6],
        icon: (
          <SendTwoToneIcon
            style={{ fontSize: '1.5em', color: '#757AFF', marginRight: 5 }}
          />
        ),
        action: () => {},
      },
    ];

    let planillasSwitchArray = [];

    if ((this.props.correcionesInfo[this.props.activeCompany] || {})['month']) {
      console.log(
        'ENTRÓ NEA',
        this.props.correcionesInfo[this.props.activeCompany]['month']
      );

      planillasSwitchArray.push(
        `correccion ${
          this.props.correcionesInfo[this.props.activeCompany].month
        }`
      );
      planillasSwitchArray.push(
        this.props.periodosInfo[this.props.activeCompany].month
      );

      console.log('plan', planillasSwitchArray);
    }

    const layoutProps = {
      saveAndContinueAction: this.saveAndContinueAction,
      returnToFirstStep: this.returnToFirstStep,
      validatePila: this.props.validatePila,
      stepperOptions,
      receiptOptions,
      planillasSwitchArray,
      tableKey: this.props.tableKey,
      checkStepStatus: this.state.checkStepStatus,
      createStepStatus: this.state.createStepStatus,
      validateStepStatus: this.state.validateStepStatus,
      sendPlanillaPayButton: this.sendPlanillaPayButton,
      openSendPayButtonModal: this.openSendPayButtonModal,
      confirmSendPayButtonModal: this.confirmSendPayButtonModal,
      cancelSendPayButton: this.cancelSendPayButton,
      continueSendPayButton: this.continueSendPayButton,
      getPlanillaPayLink: this.getPlanillaPayLink,
      disablePayButton: this.state.disablePayButton,
      disableSendPayButton: this.state.disableSendPayButton,
      returnToControlPanel: this.returnToControlPanel,
    };

    return <PILALayout {...layoutProps} />;
  }
}

const mapStateToProps = (state, currentProps) => {
  return {
    validatePila: state.payPila.validatePila,
    showConfirmVacacionesModal: state.novedadVacaciones.showConfirmModal,
    novedadVacaciones: state.novedadVacaciones,
    activeCompany: state.currentUser.activeCompany,
    activeCompanyNit: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany].nit
      : null,
    novedadVariacionPermanente: state.novedadVariacionPermanente,
    showIngresoConfirmModal: state.novedadIngreso.showConfirmModal,
    showRetiroConfirmModal: state.novedadRetiro.showConfirmModal,
    cotizanteDetailData: state.cotizanteDetailData,
    novedadIngreso: state.novedadIngreso,
    novedadRetiro: state.novedadRetiro,
    prePlanilla: state.prePlanilla,
    periodosInfo: state.currentUser.periodosInfo,
    cotizantesInfo: state.cotizantesInfo,
    adminName: state.currentUser.name,
    adminEmail: state.currentUser.email,
    companyName: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
          .isCompany
        ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .companyName
        : state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .sucursalName
      : null,
    novedadSuspensiones: state.novedadSuspensiones,
    activeCorreciones: state.currentUser.activeCorreciones,
    correcionesInfo: state.currentUser.correcionesInfo,
    tableKey: state.payPila.tableKey,
    companyIdentificationType: state.company.identificationType.code,
    companyCity: state.company.city,
    companyProvince: state.company.province,
    showSendPayButtonModal: state.payPila.showSendPayButtonModal,
    showConfirmSendPayButtonModal: state.payPila.showConfirmSendPayButtonModal,
    currentCompanyName: state.company.companyName,
    payButtonEmail: state.payPila.payButtonEmail,
    companiesInfo: state.currentUser.companiesInfo,
    planillas: state.payPila.planillas,
    idCompany: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
          .isCompany
        ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .idCompany
        : state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .idSucursal
      : null,
    currentStep: state.payPila.currentStep,
    accountManager: state.currentUser.accountManager,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    payPilaAction: (actionType, value) =>
      dispatch(payPilaAction(actionType, value)),
    novedadVacacionesAction: (actionType, value) =>
      dispatch(novedadVacacionesAction(actionType, value)),
    prePlanillaAction: (actionType, value) =>
      dispatch(prePlanillaAction(actionType, value)),
    novedadVariacionPermanenteAction: (actionType, value) =>
      dispatch(novedadVariacionPermanenteAction(actionType, value)),
    cotizantesInfoAction: (actionType, value) =>
      dispatch(cotizantesInfoAction(actionType, value)),
    novedadSuspensionesAction: (actionType, value) =>
      dispatch(novedadSuspensionesAction(actionType, value)),
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PILA));
