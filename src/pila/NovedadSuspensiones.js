import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import 'moment/locale/es';
import { novedadSuspensionesAction, snackBarAction } from '../redux/Actions';
import { SNACKBAR, SUSPENSIONES } from '../redux/ActionTypes';
import { editRecordField } from '../constants/CotizantesFields';
import ValidateDates from '../utils/ValidateDates';
import { ModalNovedad } from '.';
import { Calendar } from '../components';
import { ButtonBase, Typography } from '@material-ui/core';
import {
  SUSPENSIONES_NOVEDAD_NAME,
  SUSPENSIONES_ADD_ROW,
  SUSPENSIONES_DAYS,
} from './Constants';
import DateRangeIcon from '@material-ui/icons/DateRange';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { useDispatch, useSelector } from 'react-redux';
import { CreateSuspension, RemoveNovedad } from './api';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Button from '@bit/pilala.pilalalib.components.button';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
moment.locale('es');

export default () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [initialDateAnchor, setInitialDateAnchor] = useState({});
  const [finalDateAnchor, setFinalDateAnchor] = useState({});
  const suspensionesRows = useSelector(
    (state) => state.novedadSuspensiones.suspensionesRows
  );
  const openSuspensionesModal = useSelector(
    (state) => state.novedadSuspensiones.openSuspensionesModal
  );
  const cotizanteName = useSelector(
    (state) => state.novedadSuspensiones.cotizanteName
  );
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [showInitialDatePicker, setShowInitialDatePicker] = useState(false);
  const [showFinalDatePicker, setShowFinalDatePicker] = useState(false);
  const [removeIndex, setRemoveIndex] = useState(-1);
  const [disableSaveButton, setDisableSaveButton] = useState(false);
  const [disableButtons, setDisableButtons] = useState(false);
  const [disableRemoveButton, setDisableRemoveButton] = useState(false);
  const activeCompany = useSelector((state) => state.currentUser.activeCompany);
  const periodoActual = useSelector(
    (state) => state.currentUser.periodosInfo[activeCompany]
  );
  const prePlanilla = useSelector((state) => state.prePlanilla);
  const periodosInfo = useSelector((state) => state.currentUser.periodosInfo);
  const cotizanteType = useSelector(
    (state) => state.novedadSuspensiones.cotizanteType
  );
  const cotizanteIdentificationNumber = useSelector(
    (state) => state.novedadSuspensiones.cotizanteIdentificationNumber
  );
  const activeChanges = useSelector(
    (state) => state.novedadSuspensiones.activeChanges
  );
  const idCotizante = useSelector(
    (state) => state.novedadSuspensiones.idCotizante
  );
  const cotizantesInfo = useSelector((state) => state.cotizantesInfo);
  const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);

  //TODO: Verificar nombre de compañia cuando se activen sucursales

  const openInitialDatePicker = (index, event) => {
    setSelectedIndex(index);
    setInitialDateAnchor(event.currentTarget);
    setShowInitialDatePicker(!showInitialDatePicker);
  };

  const openFinalDatePicker = (index, event) => {
    setSelectedIndex(index);
    setFinalDateAnchor(event.currentTarget);
    setShowFinalDatePicker(!showFinalDatePicker);
  };

  const removeSuspensionesItem = async (index) => {
    if (disableRemoveButton) return;
    setDisableRemoveButton(true);
    setDisableButtons(true);
    setRemoveIndex(index);

    if (suspensionesRows[index].id) {
      const suspensionesDeleteData = {
        idAportante: activeCompany,
        cotizanteData: {
          idCotizante,
          documentNumber: cotizanteIdentificationNumber,
          companyName: companiesInfo[activeCompany].companyName,
          idNew: suspensionesRows[index].id,
          province: companiesInfo[activeCompany].location.province.value,
          city: companiesInfo[activeCompany].location.city.value,
          tipoCotizante: cotizanteType.value,
          type: suspensionesRows[index].type,
        },
      };
      const deleteResult = await RemoveNovedad(suspensionesDeleteData);
      if (deleteResult.status === 'SUCCESS') {
        dispatch(
          novedadSuspensionesAction(
            SUSPENSIONES.SUSPENSIONES_DELETE_SUSPENSIONES_ROW,
            index
          )
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
        setRemoveIndex(-1);
      } else if (deleteResult.status === 'FAILED') {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al eliminar novedad',
            description:
              'Hemos tenido problemas para eliminar la novedad de suspensión, inténtelo nuevamente',
            color: 'error',
          })
        );
        setDisableRemoveButton(false);
        setDisableButtons(false);
        setRemoveIndex(-1);
      }
    } else {
      dispatch(
        novedadSuspensionesAction(
          SUSPENSIONES.SUSPENSIONES_DELETE_SUSPENSIONES_ROW,
          index
        )
      );
      setDisableRemoveButton(false);
      setDisableButtons(false);
      setRemoveIndex(-1);
    }
  };

  const onClose = () => {
    if (openSuspensionesModal) {
      dispatch(
        novedadSuspensionesAction(
          SUSPENSIONES.SUSPENSIONES_OPEN_SUSPENSIONES_MODAL,
          null
        )
      );
    }
    dispatch(
      novedadSuspensionesAction(
        SUSPENSIONES.SUSPENSIONES_RESET_NOVEDAD_FIELDS,
        null
      )
    );
  };

  const onSave = async () => {
    if (disableSaveButton) return;
    setDisableSaveButton(true);
    setDisableButtons(true);

    if (checkSuspensionesDates()) {
      if (activeChanges) {
        const changedSuspensionesRows = suspensionesRows.map((item) => ({
          ...item,
          dateStart: item.fechaInicio,
          dateEnd: item.fechaFin,
        }));
        const suspensionesFullData = {
          tipoCotizante: cotizanteType.value,
          location:
            cotizantesInfo[activeCompany][cotizanteIdentificationNumber]
              .location,
          idCompany: activeCompany,
          cotizanteData: {
            idCotizante,
            arrayNew: changedSuspensionesRows,
          },
        };
        const method = changedSuspensionesRows.some(
          (item) => item.id !== undefined
        )
          ? 'PATCH'
          : 'POST';
        const result = await CreateSuspension(suspensionesFullData, method);
        if (result.status === 'SUCCESS') {
          dispatch(
            novedadSuspensionesAction(
              SUSPENSIONES.SUSPENSIONES_OPEN_SUSPENSIONES_MODAL,
              null
            )
          );
          dispatch(
            novedadSuspensionesAction(
              SUSPENSIONES.SUSPENSIONES_RESET_NOVEDAD_FIELDS,
              null
            )
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        } else if (result.status === 'FAILED') {
          dispatch(
            snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear novedad',
              description:
                'Hemos tenido problemas para crear la novedad de suspensión, inténtelo nuevamente',
              color: 'error',
            })
          );
          setDisableSaveButton(false);
          setDisableButtons(false);
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'No se detectaron cambios',
            description: 'La información sobre las suspensiones no ha cambiado',
            color: 'info',
          })
        );
        setDisableSaveButton(false);
        setDisableButtons(false);
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description:
            'Por favor complete las fechas del reporte de suspensión',
          color: 'warning',
        })
      );
      setDisableSaveButton(false);
      setDisableButtons(false);
    }
  };

  const addSuspensionesRow = () => {
    if (checkSuspensionesDates()) {
      dispatch(
        novedadSuspensionesAction(
          SUSPENSIONES.SUSPENSIONES_ADD_SUSPENSIONES_ROW,
          null
        )
      );
    }
  };

  const checkSuspensionesDates = () => {
    return suspensionesRows.every(
      (item) => item.fechaInicio !== '' && item.fechaFin !== ''
    );
  };

  const getInitialDate = (index, data) => {
    const { fechaFin, id } = suspensionesRows[index];
    if (
      fechaFin === '' ||
      moment(fechaFin, 'DD/MM/YYYY').diff(moment(data, 'DD/MM/YYYY'), 'days') >
        0
    ) {
      const cotizanteData =
        prePlanilla[activeCompany][periodosInfo[activeCompany].value][
          cotizanteType.value
        ][periodosInfo[activeCompany].month][cotizanteIdentificationNumber];

      const dateValidation = ValidateDates(
        cotizanteData,
        data,
        false,
        false,
        index,
        id,
        true,
        false,
        suspensionesRows
      );

      if (dateValidation.result) {
        const value = {
          fields: {
            fechaInicio: moment(data).format('DD/MM/YYYY'),
            value: {
              fechaInicio: moment(data).format('DD/MM/YYYY'),
            },
            regLine: {
              27: editRecordField[27]('X'),
              89: editRecordField[89](moment(data).format('YYYY-MM-DD')),
            },
          },
          index,
        };
        dispatch(
          novedadSuspensionesAction(
            SUSPENSIONES.SUSPENSIONES_SAVE_NOVEDAD_FIELDS,
            value
          )
        );
        if (!activeChanges) {
          dispatch(
            novedadSuspensionesAction(
              SUSPENSIONES.SUSPENSIONES_ENABLE_ACTIVE_CHANGES,
              null
            )
          );
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al validar fechas',
            description: dateValidation.message,
            color: 'error',
          })
        );
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Fecha incorrecta',
          description: 'La fecha inicial debe ser menor que la fecha final',
          color: 'info',
        })
      );
    }
  };

  const getFinalDate = (index, data) => {
    const { fechaInicio, id } = suspensionesRows[index];
    if (
      fechaInicio === '' ||
      moment(data, 'DD/MM/YYYY').diff(
        moment(fechaInicio, 'DD/MM/YYYY'),
        'days'
      ) >= 0
    ) {
      const cotizanteData =
        prePlanilla[activeCompany][periodosInfo[activeCompany].value][
          cotizanteType.value
        ][periodosInfo[activeCompany].month][cotizanteIdentificationNumber];

      const dateValidation = ValidateDates(
        cotizanteData,
        data,
        false,
        false,
        index,
        id,
        false,
        true,
        suspensionesRows
      );

      if (dateValidation.result) {
        const value = {
          fields: {
            fechaFin: moment(data).format('DD/MM/YYYY'),
            value: {
              fechaFin: moment(data).format('DD/MM/YYYY'),
            },
            regLine: {
              27: editRecordField[27]('X'),
              90: editRecordField[90](moment(data).format('YYYY-MM-DD')),
            },
          },
          index,
        };
        dispatch(
          novedadSuspensionesAction(
            SUSPENSIONES.SUSPENSIONES_SAVE_NOVEDAD_FIELDS,
            value
          )
        );
        if (!activeChanges) {
          dispatch(
            novedadSuspensionesAction(
              SUSPENSIONES.SUSPENSIONES_ENABLE_ACTIVE_CHANGES,
              null
            )
          );
        }
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al validar fechas',
            description: dateValidation.message,
            color: 'error',
          })
        );
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Fecha incorrecta',
          description: 'La fecha final debe ser mayor que la fecha inicial',
          color: 'info',
        })
      );
    }
  };

  const selectSuspensionesInitialDate = (data) => {
    getInitialDate(selectedIndex, data);
    setShowInitialDatePicker(!showInitialDatePicker);
  };

  const selectSuspensionesFinalDate = (data) => {
    getFinalDate(selectedIndex, data);
    setShowFinalDatePicker(!showFinalDatePicker);
  };

  return (
    <ModalNovedad
      open={openSuspensionesModal}
      title="Reportar suspensión"
      employeeName={cotizanteName}
      onClose={onClose}
      onCancel={onClose}
      onSave={onSave}
      disableSaveButton={disableSaveButton}
      disableButtons={disableButtons}
    >
      <div className={classes.novedadContainer}>
        <Typography className={classes.novedadName} variant="subtitle1">
          {SUSPENSIONES_NOVEDAD_NAME}
        </Typography>
        <div className={classes.dateContent}>
          <div className={classes.dateTitleContainer}>
            <Typography className={classes.dateTitle} variant="subtitle1">
              {SUSPENSIONES_DAYS}
            </Typography>
          </div>
          <div className={classes.dateContainer}>
            {suspensionesRows.map((item, index) => (
              <div className={classes.rangeContainer} key={index}>
                <IconButton
                  onClick={openInitialDatePicker.bind(this, index)}
                  filled={false}
                  size="small"
                >
                  <DateRangeIcon />
                </IconButton>
                <ButtonBase
                  onClick={openInitialDatePicker.bind(this, index)}
                  className={classes.dateField}
                >
                  <Typography className={classes.dateText} variant="subtitle2">
                    {item.fechaInicio}
                  </Typography>
                </ButtonBase>
                <ArrowForwardIcon className={classes.icon} />
                <IconButton
                  onClick={openFinalDatePicker.bind(this, index)}
                  filled={false}
                  size="small"
                >
                  <DateRangeIcon />
                </IconButton>
                <ButtonBase
                  onClick={openFinalDatePicker.bind(this, index)}
                  className={classes.dateField}
                >
                  <Typography className={classes.dateText} variant="subtitle2">
                    {item.fechaFin}
                  </Typography>
                </ButtonBase>
                <div className={classes.removeIconContainer}>
                  {(item.fechaInicio !== '' ||
                    item.fechaFin !== '' ||
                    index > 0) && (
                    <IconButton
                      buttoncolor="error"
                      filled={false}
                      size="small"
                      loading={disableRemoveButton && removeIndex === index}
                      disabled={disableButtons}
                      onClick={removeSuspensionesItem.bind(this, index)}
                    >
                      <RemoveCircleOutlineIcon />
                    </IconButton>
                  )}
                </div>
              </div>
            ))}
            <div className={classes.addRow}>
              <Button
                startIcon={<AddCircleOutlineIcon />}
                variant="text"
                onClick={addSuspensionesRow}
              >
                {SUSPENSIONES_ADD_ROW}
              </Button>
            </div>
            <Calendar
              open={showInitialDatePicker}
              onChange={selectSuspensionesInitialDate}
              anchorElement={initialDateAnchor}
              focusDate={periodoActual.display}
              color="primary"
            />
            <Calendar
              open={showFinalDatePicker}
              onChange={selectSuspensionesFinalDate}
              anchorElement={finalDateAnchor}
              focusDate={periodoActual.display}
              color="primary"
            />
          </div>
        </div>
      </div>
    </ModalNovedad>
  );
};

const useStyles = makeStyles((theme) => ({
  novedadContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(8),
  },
  novedadName: {
    color: theme.palette.primary.main,
  },
  dateContent: {
    backgroundColor: theme.palette.grey[200],
    width: '100%',
    borderTop: '1px solid',
    borderTopColor: theme.palette.primary[400],
    display: 'flex',
    padding: theme.spacing(3, 3, 5, 2),
  },
  dateTitleContainer: {
    flex: 1,
    display: 'flex',
  },
  dateTitle: {
    color: theme.palette.common.black,
  },
  dateContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    overflow: 'auto',
    maxHeight: 185,
    padding: '0px 1px',
  },
  dateField: {
    border: '1px solid',
    borderColor: theme.palette.primary.main,
    borderRadius: 8,
    width: 90,
    height: 30,
    margin: theme.spacing(0, 1),
    display: 'flex',
    alignItems: 'center',
  },
  dateText: {
    color: theme.palette.common.black,
  },
  removeSpinner: {
    color: theme.palette.primary.main,
  },
  removeIconContainer: {
    width: 30,
    height: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    color: theme.palette.primary.main,
    fontSize: '1.5rem',
    margin: theme.spacing(0, 1),
  },
  rangeContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 10,
  },
  addRow: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingRight: theme.spacing(3.7),
  },
}));
