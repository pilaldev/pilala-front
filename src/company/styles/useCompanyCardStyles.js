import { makeStyles } from '@material-ui/core/styles';

const useCompanyCardStyles = makeStyles((theme) => ({
  container: {
    width: '90%',
  },
  contentContainer: {
    display: 'flex',
  },
  card: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    paddingTop: 30,
    paddingBottom: 30,
    // border: '1px solid #6A32B5',
    boxShadow: '0px 3px 16px #00000029',
    borderRadius: 7,
  },
  firstContainer: {
    width: '15%',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
  },
  avatar: {
    [theme.breakpoints.down('md')]: {
      width: 70,
      height: 70,
    },
    [theme.breakpoints.up('lg')]: {
      width: 100,
      height: 100,
    },
    marginTop: 30,
    marginBottom: 20,
    boxSizing: 'border-box',
    border: '5px solid #C6CCD0',
    backgroundColor: '#F7F7F7',
  },
  uploadLogoLink: {
    textDecoration: 'underline',
    color: '#95989A',
  },
  secondContainer: {
    width: '55%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    paddingRight: 30,
  },
  textField: {
    margin: '10px 0px 10px 0px',
  },
  fieldsGrid: {
    margin: '10px 0px 10px 0px',
  },
  fieldGrid: {
    width: '20%',
  },
  phoneGrid: {
    width: '20%',
    marginTop: 15,
  },
  thirdContainer: {
    width: '30%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  thirdGrid: {
    margin: '10px 20px 10px 0px',
  },
  nitField: {
    width: '70%',
  },
  nitDetail: {
    marginLeft: 10,
    marginRight: 10,
  },
  dvField: {
    width: '10%',
  },
  directionField: {
    margin: '8px 20px 10px 0px',
    width: '88%',
  },
  buttonsGrid: {
    marginTop: 10,
  },
  arlButton: {
    width: '40%',
    border: '2px solid #6A32B5',
    borderRadius: 7,
    height: 51,
  },
  arlButtonRequired: {
    border: '2px solid #FF495A',
    backgroundColor: '#FF495A26',
  },
  selectedARL: {
    fontSize: '0.85em',
  },
  arlText: {
    color: 'black',
  },
  arlTextRequired: {
    color: '#FF495A',
  },
  ccfButton: {
    width: '40%',
    marginLeft: 15,
    border: '2px solid #6A32B5',
    height: 51,
    borderRadius: 7,
  },
  ccfButtonRequired: {
    border: '2px solid #FF495A',
    backgroundColor: '#FF495A26',
  },
  ccfText: {
    color: 'black',
    fontSize: 10,
  },
  ccfTextRequired: {
    color: '#FF495A',
    fontSize: 10,
  },
  saveButtonsContainer: {
    paddingTop: 30,
  },
  saveButton: {
    background:
      'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
    borderRadius: 100,
    marginLeft: 10,
    width: '10%',
    textTransform: 'none',
  },
  saveButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },
  cancelButtonText: {
    color: '#6A32B5',
  },
  cancelButton: {
    marginRight: 10,
    borderRadius: 100,
    textTransform: 'none',
  },
  editGrid: {
    paddingRight: 30,
    paddingTop: 10,
    paddingBottom: 10,
  },
  editButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  editIcon: {
    width: 17,
    height: 17,
    marginRight: 5,
    color: '#6A32B5',
  },
  closeButton: {
    color: theme.palette.grey[500],
  },
  dialogTitleContainer: {
    borderBottom: '1px solid black',
  },
  arlsContainer: {
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '80%',
    flexWrap: 'wrap',
  },
  ccfsContainer: {
    padding: 16,
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  arlButtonContainer: {
    width: '100%',
  },
  ccfButtonContainer: {
    width: '15%',
    margin: 25,
  },
  arlCardContainer: {
    width: '100%',
    padding: '0px 8px 0px 8px',
    minHeight: 140,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  ccfCardContainer: {
    width: '100%',
    padding: '0px 8px 0px 8px',
    minHeight: 140,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  arlImage: {
    width: '80%',
    height: 'auto',
    display: 'block',
  },
  arlName: {
    fontSize: '0.85em',
    width: '100%',
  },
  arlButtonsContainer: {
    paddingTop: 30,
    paddingBottom: 30,
  },
  arlCardMainContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '25%',
    height: '25%',
    margin: '23px 40px 23px 40px',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ccfCardMainContainer: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: 'yellow',
    width: '15%',
    height: '25%',
    margin: '25px 25px 25px 25px',
    justifyContent: 'center',
    alignItems: 'center',
  },
  arlClaseContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    height: '100%',
  },
  arlSelect: {
    textAlign: 'left',
    marginTop: '3%',
    fontSize: '1em',
  },
  obligacionesContainer: {
    backgroundColor: '#ECEFF1',
    padding: '15px 0px 15px 0px',
    justifyContent: 'space-evenly',
    borderRadius: 7,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  parafiscalesTooltip: {
    backgroundColor: '#2962FF',
  },
  parafiscalesTooltipArrow: {
    color: '#2962FF',
  },
  constitucionCalendarContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'left',
    alignItems: 'center',
    flex: 1,
    paddingLeft: 10,
  },
  constitucionDateContainer: {
    border: '2px solid #6A32B5',
    width: '35%',
    borderRadius: 5,
    padding: '3px 10% 3px 10%',
    marginLeft: '2%',
    backgroundColor: 'rgba(106,50,181,0.2)',
  },
  constitucionDateContainerRequired: {
    border: '2px solid #FF495A',
    backgroundColor: '#FF495A26',
  },
  paperContainer: {
    backgroundColor: 'transparent',
    display: 'flex',
    alignItems: 'center',
  },
  ccfPaperContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#2962FF',
  },
  arlSelectColor: {
    color: '#6A32B5',
    fontWeight: 'bold',
    textAlign: 'left',
  },
  arlSelectColorCard: {
    color: '#78909C',
    textAlign: 'left',
  },
  selectArlClassContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '25%',
    height: '19.5%',
    margin: '23px 40px 23px 40px',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tipoVial: {
    width: '15%',
    textAlign: 'left',
  },
  numeroVial: {
    width: '8%',
  },
  letraVial: {
    width: '8%',
  },
  tipoCuadrante: {
    width: '12%',
    textAlign: 'left',
  },
  numeroViaGeneradora: {
    width: '8%',
  },
  cuadranteViaGeneradora: {
    width: '8%',
  },
  numeroPlaca: {
    width: '8%',
  },
  obligacionesTitle: {
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#263238',
    paddingBottom: 5,
  },
  obligacionRowContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  obligacionCheckbox: {
    padding: 0,
  },
  obligacionCheckboxIcon: {
    width: 18,
    height: 18,
    color: '#263238',
  },
  obligacionName: {
    fontSize: '1em',
    marginLeft: 5,
    color: '#263238',
  },
  obligacionesButtonsContainer: {
    paddingTop: 10,
  },
  obligacionesButtonRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  obligacionButtonCheckedIcon: {
    color: '#4CAF50',
  },
  obligacionButtonIcon: {
    color: '#95989A',
  },
  obligacionButtonTitle: {
    fontSize: '1.2em',
    marginLeft: 5,
    marginRight: 5,
    color: '#263238',
  },
  obligacionInfoIcon: {
    fontSize: '1em',
    color: '#2962FF',
  },
  constitucionDateRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  constitucionCalendarIcon: {
    color: '#6A32B5',
  },
  claseAportante: {
    width: '88%',
    margin: '27px 0px 10px 0px',
    textAlign: 'left',
  },
  administradorasTitle: {
    textAlign: 'left',
    margin: '1.8rem 0px 0px 0px',
    color: '#263238',
    fontWeight: 'bold',
  },
  arlModalColor: {
    backgroundColor: '#6A32B5',
  },
  arlBackdrop: {
    backgroundColor: 'transparent',
  },
  selectArlTitle: {
    textAlign: 'center',
    fontSize: '2em',
    color: '#FFFFFF',
    fontWeight: 'bold',
    paddingTop: '5%',
  },
  arlCheckIconContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: '10px 10px 0px 0px',
  },
  arlCheckedIcon: {
    fontSize: '1.2em',
    color: '#6A32B5',
  },
  arlCheckRadioButton: {
    fontSize: '1.2em',
    color: '#C6CCD0',
  },
  ccfBackdrop: {
    backgroundColor: 'transparent',
  },
  selectCCFTitle: {
    textAlign: 'center',
    fontSize: '2em',
    color: '#FFFFFF',
    fontWeight: 'bold',
    padding: '5% 0px 1% 0px',
  },
  ccfOptionsContainer: {
    display: 'flex',
    width: '80%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    paddingLeft: '3%',
  },
  ccfCheckIconContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: '10px 10px 0px 0px',
  },
  ccfCheckedIcon: {
    fontSize: '1.2em',
    color: '#6A32B5',
  },
  ccfCheckRadioButton: {
    fontSize: '1.2em',
    color: '#C6CCD0',
  },
  justifyTextfieldText: {
    textAlign: 'center',
  },
  editButtonText: {
    color: '#6A32B5',
    fontWeight: 'bold',
  },
  dateText: {
    color: '#6A32B5',
    fontSize: '1.1em',
    fontWeight: 'bold',
  },
  dateTextRequired: {
    color: '#FF495A',
  },
  backButton: {
    position: 'absolute',
    top: '5%',
    left: '2%',
    display: 'flex',
    flexDirection: 'row',
    padding: '0px 8px 0px 8px',
    borderRadius: 50,
  },
  backButtonText: {
    color: '#FFFFFF',
    marginLeft: 5,
  },
  backButtonIcon: {
    color: '#FFFFFF',
  },
  spinner: {
    color: '#FFFFFF',
  },
  underline: {
    '&.MuiInput-underline:after': {
      borderBottom: '2px solid #6A32B5',
    },
  },
  disabledUnderline: {
    '&.MuiInput-underline:after': {
      borderBottom: 'none',
    },
  },
  disabledField: {
    backgroundColor: '#ECEFF1',
    borderRadius: 7,
    padding: '0px 5px',
  },
  numberSymbol: {
    marginTop: '5px',
    fontSize: '1rem',
    color: '#95989A',
  },
  arlLevel: {
    textAlign: 'left',
    marginTop: '3%',
    fontSize: '0.9rem',
    fontWeight: 'bold',
  },
  administradoraDisabledButton: {
    border: '2px solid #6A32B5',
  },
  arlLevelRequired: {
    color: '#FF495A',
  },
}));

const useRequiredFieldStyles = makeStyles({
  underlineRequired: {
    '&.MuiInput-underline:after': {
      borderBottom: '2px solid #FF495A',
    },
    '&.MuiInput-underline:before': {
      borderBottom: '1px solid #FF495A',
    },
    '&.MuiInput-underline:hover:before': {
      borderBottom: '2px solid #FF495A',
    },
  },
  inputRoot: {
    '&.MuiAutocomplete-inputRoot': {
      backgroundColor: '#FF495A26',
    },
  },
  disabledInputRoot: {
    '&.MuiAutocomplete-inputRoot': {
      backgroundColor: '#ECEFF1',
      borderRadius: 7,
      padding: '0px 7px',
      height: 32,
    },
  },
  selectMenu: {
    '&.MuiSelect-selectMenu': {
      backgroundColor: '#FF495A26',
    },
  },
  disabledSelectMenu: {
    '&.MuiSelect-root': {
      backgroundColor: '#ECEFF1',
      borderRadius: 7,
      padding: '0px 7px',
      height: 32,
      display: 'flex',
      alignItems: 'center',
    },
  },
});

const useDisabledInputStyles = makeStyles({
  disabled: {
    '&.Mui-disabled:before': {
      borderBottomStyle: 'solid',
    },
    '&.MuiInputBase-input': {
      color: '#545454',
    },
  },
});

export { useCompanyCardStyles, useRequiredFieldStyles, useDisabledInputStyles };
