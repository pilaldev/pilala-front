import { makeStyles } from '@material-ui/core/styles';

const useCheckCompanyStyles = makeStyles((theme) => ({
  dialogContainer: {
    background:
      'radial-gradient(circle, rgba(53,26,124,1) 0%, rgba(42,27,89,1) 33%, rgba(31,28,53,1) 66%, rgba(29,14,51,1) 100%)',
  },
  paperContainer: {
    backgroundColor: 'transparent',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dialogBackdrop: {
    backgroundColor: 'transparent',
  },
  checkNitTitle: {
    fontWeight: 'bold',
    color: theme.palette.common.black,
    textAlign: 'left',
    marginBottom: theme.spacing(3),
  },
  checkNitCard: {
    display: 'flex',
    borderRadius: 8,
    boxShadow: theme.elevations[2],
    width: '100%',
    padding: theme.spacing(4, 6),
    flexDirection: 'column',
    [theme.breakpoints.up('xs')]: {
      width: '90%',
    },
    [theme.breakpoints.up('sm')]: {
      minWidth: '450px',
      maxWidth: '450px',
    },
    [theme.breakpoints.up('xl')]: {
      minWidth: '25%',
      maxWidth: '25%',
    },
  },
  confirmButton: {
    marginLeft: 0,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
    },
  },
  buttonsContainer: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginTop: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      justifyContent: 'flex-end',
    },
  },
}));

export default useCheckCompanyStyles;
