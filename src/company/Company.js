import React, { Component } from 'react';
import { connect } from 'react-redux';
import CompanyLayout from './CompanyLayout';
import Endpoints from '@bit/pilala.pilalalib.endpoints';
import {
  companyAction,
  currentUserAction,
  cotizantesAction,
  navigationAction,
  snackBarAction,
  prePlanillaAction,
  credentialsModalAction,
} from './../redux/Actions';
import {
  COMPANY,
  CURRENT_USER,
  COTIZANTES,
  NAVIGATION,
  SNACKBAR,
  PRE_PLANILLA,
  CREDENTIALS_MODAL,
} from './../redux/ActionTypes';
import { FIELDS_ID, BC_FIELDS_ID, LR_FIELDS_ID } from './Constants';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';

export class Company extends Component {
  state = {
    disableCompanyButton: false,
    disableLRButton: false,
    disableBCButton: false,
    disableCreateCompanyButton: false,
    disableSucursalButton: false,
  };

  componentDidMount() {
    ChangePageTitle('Mi compañía | Pilalá');
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'compañia'
    );
    this.showCheckCompany();
  }

  showCheckCompany = () => {
    if (!this.props.createdCompany) {
      this.props.credentialsModalAction(
        CREDENTIALS_MODAL.OPEN_MODAL,
        'default'
      );
    }
  };

  createNewCompany = async (e) => {
    e.preventDefault();
    if (this.state.disableCreateCompanyButton) return;
    this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, true);
    this.setState({
      disableCreateCompanyButton: true,
    });

    const fieldsStatus = await this.checkAllFields();
    if (fieldsStatus === true) {
      const managerData = {
        name: this.props.managerName,
        email: this.props.managerEmail,
        documentType: this.props.managerDocumentType,
        documentNumber: this.props.managerDocumentNumber,
        phone: this.props.managerPhoneNumber,
        mobileNumber: this.props.managerCellphoneNumber,
      };

      const companyContact = {
        name: this.props.companyContactName,
        email: this.props.companyContactEmail,
        documentType: this.props.companyContactDocumentType,
        documentNumber: this.props.companyContactDocumentNumber,
        phone: this.props.companyContactPhoneNumber,
        mobileNumber: this.props.companyContactcellphoneNumber,
      };

      const companyData = {
        companyName: this.props.companyName,
        arl: this.props.arl,
        claseArl: this.props.claseArl,
        cajaCompensacion: this.props.cajaCompensacion,
        phoneNumber: this.props.phoneNumber,
        mobileNumber: this.props.cellphoneNumber,
        nitCompany: this.props.nit,
        identificationType: this.props.identificationType,
        verificationDigit: this.props.verificationDigit,
        actividadEconomica: this.props.economicActivity,
        companyContact: companyContact,
        managerData: managerData,
        exentoPagoParafiscales: this.props.exentoPagoParafiscales,
        beneficiadoLey590Del2000: this.props.beneficiadoLey590Del2000,
        fechaConstitucion: this.props.fechaConstitucion,
        clasificacionAportanteCodigo: this.props.claseAportante,
      };
      const companyFullDataToLoad = {
        companyData: companyData,
        location: {
          province: this.props.province,
          city: this.props.city,
          address: this.props.address,
        },
        adminPlatform: {
          displayName: this.props.adminName,
          email: this.props.adminEmail,
        },
        nitCompany: companyData.nitCompany,
      };

      await fetch(Endpoints.sendCompanyData, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(companyFullDataToLoad),
      })
        .then((response) => response.json())
        .then(async (resultData) => {
          if (resultData.status === 'SUCCESS') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'El aportante fue creado correctamente',
              description: 'Se ha creado tu nueva empresa en Pilalá',
              color: 'success',
            });
            this.props.navigationAction(
              NAVIGATION.NAVIGATION_BLOCK_SCREEN,
              false
            );
            await this.formatCompanyData(resultData.result.companyObject);
            await this.createdCompanyNextSteps();
          } else if (resultData.status === 'FAILED') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear el aportante',
              description:
                'Hemos tenido problemas para crear tu aportante, inténtalo nuevamente',
              color: 'error',
            });
            this.props.navigationAction(
              NAVIGATION.NAVIGATION_BLOCK_SCREEN,
              false
            );
            this.setState({
              disableCreateCompanyButton: false,
            });
          }
        })
        .catch((error) => {
          console.log('ERROR', error);
          this.props.navigationAction(
            NAVIGATION.NAVIGATION_BLOCK_SCREEN,
            false
          );
          this.setState({
            disableCreateCompanyButton: false,
          });
        });
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Información requerida',
        description:
          'Hacen falta algunos datos obligatorios para crear el aportante',
        color: 'warning',
      });
      this.props.navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false);
      this.setState({
        disableCreateCompanyButton: false,
      });
    }
  };

  saveCompanyChanges = async () => {
    if (this.state.disableCompanyButton) return;
    this.setState({
      disableCompanyButton: true,
    });

    const companyStatus = await this.checkCompanyFields();
    if (companyStatus === true) {
      const companyData = {
        companyName: this.props.companyName,
        arl: this.props.arl,
        claseArl: this.props.claseArl,
        cajaCompensacion: this.props.cajaCompensacion,
        phoneNumber: this.props.phoneNumber,
        mobileNumber: this.props.cellphoneNumber,
        exentoPagoParafiscales: this.props.exentoPagoParafiscales,
        beneficiadoLey590Del2000: this.props.beneficiadoLey590Del2000,
        fechaConstitucion: this.props.fechaConstitucion,
        clasificacionAportanteCodigo: this.props.claseAportante,
      };
      const companyDataToLoad = {
        companyData: companyData,
        location: {
          province: this.props.province,
          city: this.props.city,
          address: this.props.address,
        },
        adminPlatform: {
          displayName: this.props.adminName,
          email: this.props.adminEmail,
        },
        nitCompany: this.props.nit,
      };

      fetch(Endpoints.sendCompanyData, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(companyDataToLoad),
      })
        .then((response) => response.json())
        .then((result) => {
          if (result.status === 'SUCCESS') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Actualización exitosa',
              description: 'El aportante ha sido actualizado correctamente',
              color: 'success',
            });
            const fields = {
              copyCompanyData: {},
            };
            this.props.companyAction(
              COMPANY.COMPANY_UPDATE_SOME_FIELDS,
              fields
            );
            this.props.companyAction(COMPANY.COMPANY_EDIT_COMPANY, null);
            this.setState({
              disableCompanyButton: false,
            });
          } else if (result.status === 'FAILED') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al actualizar el aportante',
              description:
                'Hemos tenido problemas para actualizar tu aportante, inténtalo nuevamente',
              color: 'error',
            });
            this.setState({
              disableCompanyButton: false,
            });
          }
        })
        .catch((error) => {
          console.log('ERROR', error);
          this.setState({
            disableCompanyButton: false,
          });
        });
    } else {
      this.setState({
        disableCompanyButton: false,
      });
    }
  };

  saveLegalRepresentativeChanges = async () => {
    if (this.state.disableLRButton) return;
    this.setState({
      disableLRButton: true,
    });

    const LRStatus = await this.checkLegalRepresentativeFields();
    if (LRStatus === true) {
      const managerData = {
        name: this.props.managerName,
        email: this.props.managerEmail,
        documentType: this.props.managerDocumentType,
        documentNumber: this.props.managerDocumentNumber,
        phone: this.props.managerPhoneNumber,
        mobileNumber: this.props.managerCellphoneNumber,
      };
      const managerDataToLoad = {
        managerData: managerData,
        location: {
          province: this.props.province.value,
          city: this.props.city.value,
        },
        adminPlatform: {
          displayName: this.props.adminName,
          email: this.props.adminEmail,
        },
        nitCompany: this.props.nit,
        companyName: this.props.companyName,
      };

      fetch(Endpoints.sendManagerData, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(managerDataToLoad),
      })
        .then((response) => response.json())
        .then((result) => {
          if (result.status === 'SUCCESS') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Actualización exitosa',
              description:
                'El representante legal fue actualizado correctamente',
              color: 'success',
            });
            const fields = {
              copylegalRepresentativeData: {},
            };
            this.props.companyAction(
              COMPANY.COMPANY_UPDATE_SOME_FIELDS,
              fields
            );
            this.props.companyAction(
              COMPANY.COMPANY_EDIT_LEGAL_REPRESENTATIVE,
              null
            );
            this.setState({
              disableLRButton: false,
            });
          } else if (result.status === 'FAILED') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al actualizar el representante legal',
              description:
                'Hemos tenido problemas para actualizar el representante legal, inténtalo nuevamente',
              color: 'error',
            });
            this.setState({
              disableLRButton: false,
            });
          }
        })
        .catch((error) => {
          console.log('ERROR', error);
          this.setState({
            disableLRButton: false,
          });
        });
    } else {
      this.setState({
        disableLRButton: false,
      });
    }
  };

  saveBusinessContactChanges = async () => {
    if (this.state.disableBCButton) return;
    this.setState({
      disableBCButton: true,
    });

    const BCStatus = await this.checkBusinessContactFields();
    if (BCStatus === true) {
      const companyContact = {
        name: this.props.companyContactName,
        email: this.props.companyContactEmail,
        documentType: this.props.companyContactDocumentType,
        documentNumber: this.props.companyContactDocumentNumber,
        phone: this.props.companyContactPhoneNumber,
        mobileNumber: this.props.companyContactcellphoneNumber,
      };
      const contactDataToLoad = {
        companyContact: companyContact,
        location: {
          province: this.props.province.value,
          city: this.props.city.value,
        },
        adminPlatform: {
          displayName: this.props.adminName,
          email: this.props.adminEmail,
        },
        nitCompany: this.props.nit,
        companyName: this.props.companyName,
      };

      fetch(Endpoints.sendContactData, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(contactDataToLoad),
      })
        .then((response) => response.json())
        .then((result) => {
          if (result.status === 'SUCCESS') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Actualización exitosa',
              description:
                'El contacto empresarial fue actualizado correctamente',
              color: 'success',
            });
            const fields = {
              copybusinessContactData: {},
            };
            this.props.companyAction(
              COMPANY.COMPANY_UPDATE_SOME_FIELDS,
              fields
            );
            this.props.companyAction(
              COMPANY.COMPANY_EDIT_BUSINESS_CONTACT,
              null
            );
            this.setState({
              disableBCButton: false,
            });
          } else if (result.status === 'FAILED') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al actualizar el contacto empresarial',
              description:
                'Hemos tenido problemas para actualizar el contacto empresarial, inténtalo nuevamente',
              color: 'error',
            });
            this.setState({
              disableBCButton: false,
            });
          }
        })
        .catch((error) => {
          console.log('ERROR', error);
          this.setState({
            disableBCButton: false,
          });
        });
    } else {
      this.setState({
        disableBCButton: false,
      });
    }
  };

  saveNewSucursal = (e) => {
    e.preventDefault();
    if (this.state.disableSucursalButton) return;
    this.setState({
      disableSucursalButton: true,
    });

    if (this.props.newSucursalName !== '') {
      const sucursalData = {
        adminPlatform: {
          displayName: this.props.adminName,
          email: this.props.adminEmail,
        },
        nitCompany: this.props.nit,
        sucursal: this.props.newSucursalName,
      };
      fetch(Endpoints.sendNewSucursal, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(sucursalData),
      })
        .then((response) => response.json())
        .then((result) => {
          if (result.status === 'SUCCESS') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Sucursal creada correctamente',
              description: 'Se ha creado la nueva sucursal de tu empresa',
              color: 'success',
            });

            const sucursal = {
              newSucursal: {
                idSucursal: result.result.idSucursal,
                sucursalName: result.result.sucursalName,
              },
              idCompany: this.props.activeCompany,
            };
            const sucursalData = {
              data: {
                idSucursal: result.result.idSucursal,
                sucursalName: result.result.sucursalName,
                ...result.result.companyData,
              },
              idCompany: result.result.idSucursal,
            };
            this.props.currentUserAction(
              CURRENT_USER.CURRENT_USER_UPDATE_SUCURSALES,
              sucursal
            );
            this.props.currentUserAction(
              CURRENT_USER.CURRENT_USER_UPDATE_COMPANIES_INFO,
              sucursalData
            );
            this.props.currentUserAction(
              CURRENT_USER.CURRENT_USER_UPDATE_PERIODOS_INFO,
              {
                activeCompany: result.result.idSucursal,
                periodo: result.result.periodoActual,
              }
            );
            this.openNewSucursalModal();
            this.setState({
              disableSucursalButton: false,
            });
          } else if (result.status === 'FAILED') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear la sucursal',
              description:
                'Hemos tenido problemas para crear la sucursal, inténtalo nuevamente',
              color: 'error',
            });
            this.setState({
              disableSucursalButton: true,
            });
          }
        })
        .catch((error) => {
          console.log('ERROR', error);
          this.setState({
            disableSucursalButton: false,
          });
        });
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Información requerida',
        description: 'Hace falta ingresar el nombre de la nueva sucursal',
        color: 'warning',
      });
      this.setState({
        disableSucursalButton: false,
      });
    }
  };

  cancelNewSucursal = () => {
    this.props.companyAction(COMPANY.COMPANY_RESET_NEW_SUCURSAL_DATA, null);
  };

  getNewSucursalName = (event) => {
    this.props.companyAction(
      COMPANY.COMPANY_SAVE_SUCURSAL_NAME,
      event.target.value
    );
  };

  openNewSucursalModal = () => {
    this.props.companyAction(COMPANY.COMPANY_OPEN_NEW_SUCURSAL_MODAL, null);
  };

  formatCompanyData = async (data) => {
    const company = {
      city: data.location.city.value,
      companyName: data.companyName,
      id: data.idCompany,
      nitCompany: data.nit,
      province: data.location.province.value,
      sucursales: [],
    };
    const companyInfo = {
      data: {
        ...data,
      },
      idCompany: data.idCompany,
    };
    const periodoInfo = {
      activeCompany: data.idCompany,
      periodo: data.periodoCotizacionActual,
    };
    const prePlanillaValue = {
      [data.idCompany]: {
        [data.periodoCotizacionActual.value]: {
          dependientes: {
            [data.periodoCotizacionActual.month]: {},
          },
        },
      },
    };

    await this.props.prePlanillaAction(
      PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA,
      prePlanillaValue
    );
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_SAVE_USER_SELECTED_COMPANY,
      data.idCompany
    );
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_UPDATE_COMPANIES,
      company
    );
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_UPDATE_COMPANIES_INFO,
      companyInfo
    );
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_UPDATE_PERIODOS_INFO,
      periodoInfo
    );
  };

  createdCompanyNextSteps = async () => {
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_SAVE_CREATED_COMPANY,
      true
    );
    await this.props.cotizantesAction(
      COTIZANTES.COTIZANTES_OPEN_NEW_COTIZANTE_DIALOG,
      null
    );
    this.setState({
      disableCreateCompanyButton: false,
    });
    this.props.history.push(`/${this.props.userUid}/console/cotizantes`);
  };

  checkCompanyFields = async () => {
    if (this.props.companyName === '') {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.COMPANY_NAME)) {
        await this.addRequiredField(FIELDS_ID.COMPANY_NAME, 'company');
      }
    }

    if (this.props.identificationType.code === '') {
      if (
        !this.props.companyRequiredFields.includes(
          FIELDS_ID.IDENTIFICATION_TYPE
        )
      ) {
        await this.addRequiredField(FIELDS_ID.IDENTIFICATION_TYPE, 'company');
      }
    }

    if (this.props.identificationNumber === '') {
      if (
        !this.props.companyRequiredFields.includes(
          FIELDS_ID.IDENTIFICATION_NUMBER
        )
      ) {
        await this.addRequiredField(FIELDS_ID.IDENTIFICATION_NUMBER, 'company');
      }
    }

    if (this.props.economicActivity === '') {
      if (
        !this.props.companyRequiredFields.includes(FIELDS_ID.ECONOMIC_ACTIVITY)
      ) {
        await this.addRequiredField(FIELDS_ID.ECONOMIC_ACTIVITY, 'company');
      }
    }

    if (this.props.claseAportante.code === -1) {
      if (
        !this.props.companyRequiredFields.includes(FIELDS_ID.CLASE_APORTANTE)
      ) {
        await this.addRequiredField(FIELDS_ID.CLASE_APORTANTE, 'company');
      }
    }

    // if (this.props.address.tipoVial.code === '') {
    // 	if (!this.props.companyRequiredFields.includes(FIELDS_ID.ADDRESS.TIPO_VIAL)) {
    // 		await this.addRequiredField(FIELDS_ID.ADDRESS.TIPO_VIAL, 'company');
    // 	}
    // }

    // if (this.props.address.numeroVial === '') {
    // 	if (!this.props.companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_VIAL)) {
    // 		await this.addRequiredField(FIELDS_ID.ADDRESS.NUMERO_VIAL, 'company');
    // 	}
    // }

    // if (this.props.address.numeroViaGeneradora === '') {
    // 	if (!this.props.companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA)) {
    // 		await this.addRequiredField(FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA, 'company');
    // 	}
    // }

    // if (this.props.address.numeroPlaca === '') {
    // 	if (!this.props.companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_PLACA)) {
    // 		await this.addRequiredField(FIELDS_ID.ADDRESS.NUMERO_PLACA, 'company');
    // 	}
    // }

    if (this.props.province === '') {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.PROVINCE)) {
        await this.addRequiredField(FIELDS_ID.PROVINCE, 'company');
      }
    }

    if (this.props.city === '') {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.CITY)) {
        await this.addRequiredField(FIELDS_ID.CITY, 'company');
      }
    }

    if (this.props.arl === '') {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.ARL)) {
        await this.addRequiredField(FIELDS_ID.ARL, 'company');
      }
    }

    if (this.props.cajaCompensacion.value === null) {
      if (
        !this.props.companyRequiredFields.includes(FIELDS_ID.CAJA_COMPENSACION)
      ) {
        await this.addRequiredField(FIELDS_ID.CAJA_COMPENSACION, 'company');
      }
    }

    if (this.props.claseArl.code === -1) {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.CLASE_ARL)) {
        await this.addRequiredField(FIELDS_ID.CLASE_ARL, 'company');
      }
    }

    if (this.props.beneficiadoLey590Del2000 === true) {
      if (this.props.fechaConstitucion.code === '') {
        if (
          !this.props.companyRequiredFields.includes(
            FIELDS_ID.FECHA_CONSTITUCION
          )
        ) {
          await this.addRequiredField(FIELDS_ID.FECHA_CONSTITUCION, 'company');
        }
      }
    }

    if (this.props.companyRequiredFields.length === 0) {
      return true;
    } else {
      return false;
    }
  };

  checkBusinessContactFields = async () => {
    if (this.props.companyContactName.display === '') {
      if (
        !this.props.businessContactRequiredFields.includes(BC_FIELDS_ID.NAME)
      ) {
        await this.addRequiredField(BC_FIELDS_ID.NAME, 'businessContact');
      }
    }

    if (this.props.companyContactDocumentType === '') {
      if (
        !this.props.businessContactRequiredFields.includes(
          BC_FIELDS_ID.IDENTIFICATION_TYPE
        )
      ) {
        await this.addRequiredField(
          BC_FIELDS_ID.IDENTIFICATION_TYPE,
          'businessContact'
        );
      }
    }

    if (this.props.companyContactDocumentNumber === '') {
      if (
        !this.props.businessContactRequiredFields.includes(
          BC_FIELDS_ID.IDENTIFICATION_NUMBER
        )
      ) {
        await this.addRequiredField(
          BC_FIELDS_ID.IDENTIFICATION_NUMBER,
          'businessContact'
        );
      }
    }

    if (this.props.companyContactPhoneNumber === '') {
      if (
        !this.props.businessContactRequiredFields.includes(
          BC_FIELDS_ID.PHONE_NUMBER
        )
      ) {
        await this.addRequiredField(
          BC_FIELDS_ID.PHONE_NUMBER,
          'businessContact'
        );
      }
    }

    // if (this.props.companyContactcellphoneNumber === '') {
    // 	if (!this.props.businessContactRequiredFields.includes(BC_FIELDS_ID.CELLPHONE_NUMBER)) {
    // 		await this.addRequiredField(BC_FIELDS_ID.CELLPHONE_NUMBER, 'businessContact');
    // 	}
    // }

    if (this.props.companyContactEmail === '') {
      if (
        !this.props.businessContactRequiredFields.includes(BC_FIELDS_ID.EMAIL)
      ) {
        await this.addRequiredField(BC_FIELDS_ID.EMAIL, 'businessContact');
      }
    }

    if (this.props.businessContactRequiredFields.length === 0) {
      return true;
    } else {
      return false;
    }
  };

  checkLegalRepresentativeFields = async () => {
    if (this.props.managerName.display === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.NAME
        )
      ) {
        await this.addRequiredField(LR_FIELDS_ID.NAME, 'legalRepresentative');
      }
    }

    if (this.props.managerDocumentType === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.IDENTIFICATION_TYPE
        )
      ) {
        await this.addRequiredField(
          LR_FIELDS_ID.IDENTIFICATION_TYPE,
          'legalRepresentative'
        );
      }
    }

    if (this.props.managerDocumentNumber === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.IDENTIFICATION_NUMBER
        )
      ) {
        await this.addRequiredField(
          LR_FIELDS_ID.IDENTIFICATION_NUMBER,
          'legalRepresentative'
        );
      }
    }

    if (this.props.managerPhoneNumber === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.PHONE_NUMBER
        )
      ) {
        await this.addRequiredField(
          LR_FIELDS_ID.PHONE_NUMBER,
          'legalRepresentative'
        );
      }
    }

    // if (this.props.managerCellphoneNumber === '') {
    // 	if (!this.props.legalRepresentativeRequiredFields.includes(LR_FIELDS_ID.CELLPHONE_NUMBER)) {
    // 		await this.addRequiredField(LR_FIELDS_ID.CELLPHONE_NUMBER, 'legalRepresentative');
    // 	}
    // }

    if (this.props.managerEmail === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.EMAIL
        )
      ) {
        await this.addRequiredField(LR_FIELDS_ID.EMAIL, 'legalRepresentative');
      }
    }

    if (this.props.legalRepresentativeRequiredFields.length === 0) {
      return true;
    } else {
      return false;
    }
  };

  selectSucursal = (item) => {
    if (
      this.props.editCompany === false &&
      this.props.editLegalRepresentative === false &&
      this.props.editBusinessContact === false
    ) {
      const currentCompany = this.props.companies.find(
        (company) => company.nitCompany === this.props.nit
      );
      const mainCompany = currentCompany.sucursales.find(
        (sucursal) => sucursal.idSucursal === item.idSucursal
      ).isCompany;
      this.props.currentUserAction(
        CURRENT_USER.CURRENT_USER_SAVE_USER_SELECTED_COMPANY,
        item.idSucursal
      );
      this.props.companyAction(
        COMPANY.COMPANY_SAVE_SELECTED_SUCURSAL,
        item.sucursalName
      );
      this.props.companyAction(COMPANY.COMPANY_SHOW_MAIN_COMPANY, mainCompany);
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Acción no permitida',
        description:
          'No puedes cambiar a otra sucursal mientras realizas una edición',
        color: 'warning',
      });
    }
  };

  returnToMainCompany = () => {
    const currentCompany = this.props.companies.find(
      (company) => company.nitCompany === this.props.nit
    );
    const mainCompany = this.props.companiesInfo[currentCompany.id].isCompany;
    this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_SAVE_USER_SELECTED_COMPANY,
      currentCompany.id
    );
    this.props.companyAction(
      COMPANY.COMPANY_SAVE_SELECTED_SUCURSAL,
      'Principal'
    );
    this.props.companyAction(COMPANY.COMPANY_SHOW_MAIN_COMPANY, mainCompany);
  };

  addRequiredField = async (field, card) => {
    const data = {
      field,
      status: true,
    };

    if (card === 'company')
      await this.props.companyAction(
        COMPANY.COMPANY_COMPANY_REQUIRED_FIELD,
        data
      );

    if (card === 'legalRepresentative')
      await this.props.companyAction(
        COMPANY.COMPANY_LEGAL_REPRESENTATIVE_REQUIRED_FIELD,
        data
      );

    if (card === 'businessContact')
      await this.props.companyAction(
        COMPANY.COMPANY_BUSINESS_CONTACT_REQUIRED_FIELD,
        data
      );
  };

  checkAllFields = async () => {
    const resultCompany = await this.checkCompanyFields();
    const resultLR = await this.checkLegalRepresentativeFields();
    const resultBC = await this.checkBusinessContactFields();
    return resultCompany && resultLR && resultBC;
  };

  render() {
    const sucursales = this.props.companies.length
      ? this.props.companies.find((item) => item.nitCompany === this.props.nit)
          .sucursales
      : [];

    const layoutProps = {
      createdCompany: this.props.createdCompany,
      createNewCompany: this.createNewCompany,
      saveCompanyChanges: this.saveCompanyChanges,
      saveLegalRepresentativeChanges: this.saveLegalRepresentativeChanges,
      saveBusinessContactChanges: this.saveBusinessContactChanges,
      saveNewSucursal: this.saveNewSucursal,
      cancelNewSucursal: this.cancelNewSucursal,
      getNewSucursalName: this.getNewSucursalName,
      openNewSucursalModal: this.openNewSucursalModal,
      sucursales,
      companyName: this.props.companyName,
      selectSucursal: this.selectSucursal,
      selectedSucursal: this.props.selectedSucursal,
      mainCompany: this.props.mainCompany,
      returnToMainCompany: this.returnToMainCompany,
      editCompany: this.props.editCompany,
      editBusinessContact: this.props.editBusinessContact,
      editLegalRepresentative: this.props.editLegalRepresentative,
      disableCompanyButton: this.state.disableCompanyButton,
      disableLRButton: this.state.disableLRButton,
      disableBCButton: this.state.disableBCButton,
      disableCreateCompanyButton: this.state.disableCreateCompanyButton,
      disableSucursalButton: this.state.disableSucursalButton,
    };

    return <CompanyLayout {...layoutProps} />;
  }
}

const mapStateToProps = (state) => {
  return {
    createdCompany: state.currentUser.createdCompany,
    activeCompany: state.currentUser.activeCompany,
    companyName: state.company.companyName,
    arl: state.company.arl,
    claseArl: state.company.claseArl,
    cajaCompensacion: state.company.cajaCompensacion,
    phoneNumber: state.company.phoneNumber,
    cellphoneNumber: state.company.cellphoneNumber,
    identificationType: state.company.identificationType,
    identificationNumber: state.company.identificationNumber,
    nit: state.company.identificationNumber,
    verificationDigit: state.company.verificationDigit,
    economicActivity: state.company.economicActivity,
    address: state.company.address,
    province: state.company.province,
    city: state.company.city,
    adminName: state.currentUser.name,
    adminEmail: state.currentUser.email,
    managerName: state.company.legalRepresentative.name,
    managerEmail: state.company.legalRepresentative.email,
    managerDocumentType: state.company.legalRepresentative.identificationType,
    managerDocumentNumber:
      state.company.legalRepresentative.identificationNumber,
    managerPhoneNumber: state.company.legalRepresentative.phoneNumber,
    managerCellphoneNumber: state.company.legalRepresentative.cellphoneNumber,
    companyContactName: state.company.businessContact.name,
    companyContactEmail: state.company.businessContact.email,
    companyContactDocumentType:
      state.company.businessContact.identificationType,
    companyContactDocumentNumber:
      state.company.businessContact.identificationNumber,
    companyContactPhoneNumber: state.company.businessContact.phoneNumber,
    companyContactcellphoneNumber:
      state.company.businessContact.cellphoneNumber,
    companies: state.currentUser.companies,
    newSucursalName: state.company.newSucursalName,
    exentoPagoParafiscales: state.company.exentoParafiscales,
    beneficiadoLey590Del2000: state.company.beneficiadoLey590,
    fechaConstitucion: state.company.constitucionDate,
    claseAportante: state.company.claseAportante,
    userUid: state.currentUser.uid,
    selectedSucursal: state.company.selectedSucursal,
    mainCompany: state.company.mainCompany,
    companiesInfo: state.currentUser.companiesInfo,
    editCompany: state.company.editCompany,
    editLegalRepresentative: state.company.editLegalRepresentative,
    editBusinessContact: state.company.editBusinessContact,
    companyRequiredFields: state.company.companyRequiredFields,
    legalRepresentativeRequiredFields:
      state.company.legalRepresentativeRequiredFields,
    businessContactRequiredFields: state.company.businessContactRequiredFields,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    companyAction: (actionType, value) =>
      dispatch(companyAction(actionType, value)),
    currentUserAction: (actionType, value) =>
      dispatch(currentUserAction(actionType, value)),
    cotizantesAction: (actionType, value) =>
      dispatch(cotizantesAction(actionType, value)),
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
    prePlanillaAction: (actionType, value) =>
      dispatch(prePlanillaAction(actionType, value)),
    credentialsModalAction: (actionType, value) =>
      dispatch(credentialsModalAction(actionType, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Company);
