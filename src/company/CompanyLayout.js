import React from 'react';
import {
  Grid,
  Button,
  Typography,
  CircularProgress,
  // ButtonBase
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import CompanyCardLayout from './CompanyCardLayout';
import LegalRepresentativeCardLayout from './LegalRepresentativeCardLayout';
import BusinessContactCardLayout from './BusinessContactCardLayout';
import NewSucursalLayout from './NewSucursalLayout';
import {
  COMPANY_INFO_TITLE,
  // ADD_SUCURSAL_BUTTON,
  CONTINUE_BUTTON,
} from './Constants';

const CompanyLayout = (props) => {
  const classes = useStyles();

  return (
    <form className={classes.formContainer} id="companyForm">
      <Grid
        container
        direction="column"
        justify="flex-start"
        className={classes.mainContainer}
      >
        {/* <div className={classes.sucursalesContainer}>
                    {
                        props.mainCompany ?
                            <ButtonBase
                                disabled={!props.createdCompany}
                                className={classes.addSucursalButton}
                                onClick={props.openNewSucursalModal}
                            >
                                <Typography className={classes.addSucursalButtonText}>
                                    {ADD_SUCURSAL_BUTTON}
                                </Typography>
                            </ButtonBase>
                            :
                            <ButtonBase
                                className={classes.newSucursalButton}
                                onClick={props.returnToMainCompany}
                            >
                                <Typography className={classes.sucursalButtonText}>
                                    {'Principal'}
                                </Typography>
                            </ButtonBase>
                    }
                    {
                        props.sucursales.map((item, index) => (
                            <ButtonBase
                                className={classes.newSucursalButton}
                                key={index}
                                onClick={props.selectSucursal.bind(this, item)}
                            >
                                <Typography className={classes.sucursalButtonText} noWrap>
                                    {`${item.sucursalName}`}
                                </Typography>
                            </ButtonBase>
                        ))
                    }
                </div> */}
        <div className={classes.titleContainer}>
          <Typography className={classes.titleText}>
            {COMPANY_INFO_TITLE}
          </Typography>
          <Typography className={classes.titleSectionText}>
            {`/${props.selectedSucursal}`}
          </Typography>
        </div>
        <CompanyCardLayout
          saveCompanyChanges={props.saveCompanyChanges}
          createdCompany={props.createdCompany}
          disableInputs={false}
          editCompany={props.editCompany}
          disableCompanyButton={props.disableCompanyButton}
        />
        <BusinessContactCardLayout
          saveBusinessContactChanges={props.saveBusinessContactChanges}
          createdCompany={props.createdCompany}
          editBusinessContact={props.editBusinessContact}
          disableBCButton={props.disableBCButton}
        />
        <LegalRepresentativeCardLayout
          saveLegalRepresentativeChanges={props.saveLegalRepresentativeChanges}
          createdCompany={props.createdCompany}
          editLegalRepresentative={props.editLegalRepresentative}
          disableLRButton={props.disableLRButton}
        />
        {props.createdCompany === false ? (
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="flex-end"
            className={classes.buttonContainer}
          >
            <Button
              variant="contained"
              type="submit"
              className={classes.continueButton}
              endIcon={
                props.disableCreateCompanyButton ? null : (
                  <ArrowForwardRoundedIcon className={classes.continueIcon} />
                )
              }
              onClick={props.createNewCompany}
            >
              {props.disableCreateCompanyButton ? (
                <CircularProgress className={classes.spinner} size={27} />
              ) : (
                <Typography
                  variant="subtitle1"
                  className={classes.continueText}
                >
                  {CONTINUE_BUTTON}
                </Typography>
              )}
            </Button>
          </Grid>
        ) : null}
        <NewSucursalLayout
          cancelAction={props.cancelNewSucursal}
          saveAction={props.saveNewSucursal}
          getNewSucursalName={props.getNewSucursalName}
          disableSucursalButton={props.disableSucursalButton}
        />
      </Grid>
    </form>
  );
};

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    padding: '3% 2% 0% 3%',
  },
  formContainer: {
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  buttonContainer: {
    marginBottom: 50,
  },
  continueButton: {
    borderRadius: 100,
    width: 200,
    marginRight: '10%',
    background:
      'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
    textTransform: 'none',
  },
  continueText: {
    color: 'white',
    fontWeight: 'bold',
  },
  continueIcon: {
    fill: 'white',
  },
  sucursalesContainer: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    marginBottom: '1.5%',
  },
  addSucursalButton: {
    marginRight: 5,
    border: '1px solid #5E35B1',
    borderRadius: 5,
    height: 35,
    width: '10%',
  },
  addSucursalButtonText: {
    color: '#5E35B1',
    fontWeight: 'bold',
    fontSize: '1.1em',
  },
  sucursalButtonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: '1.1em',
  },
  newSucursalButton: {
    margin: '0px 5px 0px 5px',
    backgroundColor: '#5E35B1',
    borderRadius: 5,
    height: 35,
    width: '10%',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    paddingLeft: 8,
    paddingRight: 8,
  },
  titleContainer: {
    display: 'flex',
    width: '100%',
    margin: '0% 0% 1.5% 0%',
  },
  titleText: {
    textAlign: 'left',
    fontSize: '1.8em',
    fontWeight: 'bold',
    color: '#263238',
  },
  titleSectionText: {
    alignSelf: 'flex-end',
    paddingBottom: 4,
    color: '#78909C',
  },
  spinner: {
    color: '#FFFFFF',
  },
}));

export default CompanyLayout;
