/* Company card */
export const ARL = 'ARL';
export const CAJA_COMPENSACION = 'Caja de compensación';
export const SELECT_ARL = 'Selecciona tu ARL';
export const SELECT_CCF = 'Selecciona tu caja de compensación'
export const SELECT_CLASE_ARL = 'Seleccione una clase';
export const FIELDS_ID = {
    COMPANY_NAME: 'companyName',
    ECONOMIC_ACTIVITY: 'economicActivity',
    PROVINCE: 'province',
    CITY: 'city',
    PHONE_NUMBER: 'phoneNumber',
    CELLPHONE_NUMBER: 'cellphoneNumber',
    IDENTIFICATION_TYPE: 'identificationType',
    IDENTIFICATION_NUMBER: 'identificationNumber',
    VERIFICATION_DIGIT: 'verificationDigit',
    ADDRESS: {
        DIRECCION_COMPLETA: 'direccionCompleta',
        TIPO_VIAL: 'tipoVial',
        NUMERO_VIAL: 'numeroVial',
        LETRA_VIAL: 'letraVial',
        TIPO_CUADRANTE: 'tipoCuadrante',
        NUMERO_VIA_GENERADORA: 'numeroViaGeneradora',
        CUADRANTE_VIA_GENERADORA: 'cuadranteViaGeneradora',
        NUMERO_PLACA: 'numeroPlaca',
        INFO_ADICIONAL: 'informacionAdicional'
    },
    ARL: 'arl',
    CAJA_COMPENSACION: 'cajaCompensacion',
    CLASE_ARL: 'claseArl',
    CLASE_APORTANTE: 'claseAportante',
    FECHA_CONSTITUCION: 'fechaConstitucion'
}
export const ENABLED_FIELDS_TO_EDIT = [
    'phoneNumber',
    'cellphoneNumber',
    'direccionCompleta',
    'tipoVial',
    'numeroVial',
    'letraVial',
    'tipoCuadrante',
    'numeroViaGeneradora',
    'cuadranteViaGeneradora',
    'numeroPlaca',
    'informacionAdicional',
];

/* Legal representative */
export const LR_FIELDS_ID = {
    NAME: 'lr_name',
    EMAIL: 'lr_email',
    PROVINCE: 'lr_province',
    CITY: 'lr_city',
    IDENTIFICATION_TYPE: 'lr_identificationType',
    IDENTIFICATION_NUMBER: 'lr_identificationNumber',
    PHONE_NUMBER: 'lr_phoneNumber',
    CELLPHONE_NUMBER: 'lr_cellphoneNumber'
}
export const LEGAL_REPRESENTATIVE = 'Representante legal';

/* Business contact */
export const BC_FIELDS_ID = {
    NAME: 'bc_name',
    EMAIL: 'bc_email',
    PROVINCE: 'bc_province',
    CITY: 'bc_city',
    IDENTIFICATION_TYPE: 'bc_identificationType',
    IDENTIFICATION_NUMBER: 'bc_identificationNumber',
    PHONE_NUMBER: 'bc_phoneNumber',
    CELLPHONE_NUMBER: 'bc_cellphoneNumber'
}
export const BUSINESS_CONTACT = 'Contacto empresarial';

/* Document type array */
export const DOCUMENT_TYPE = [
    {
        display: 'CC',
        code: 'CC'
    },
    {
        display: 'CE',
        code: 'CE'
    }
];
export const CLASES_ARL = [
    'Nivel de riesgo',
    'Clase 1 - Riesgo Minimo',
    'Clase 2 - Riesgo Bajo',
    'Clase 3 - Riesgo Medio',
    'Clase 4 - Riesgo Alto',
    'Clase 5 - Riesgo Maximo'
]

/* Buttons, etc. */
export const CANCEL_BUTTON = 'Cancelar';
export const SAVE_BUTTON = 'Guardar';
export const EDIT_BUTTON = 'Editar';

/* Custom styles */
export const CUSTOM_CCF = [
    'COMFACHOCO',
    'COMFAMILIAR NARIÑO',
    'COMFENALCO TOLIMA'
];

export const CUSTOM_COMPANY_ARL = [
    'VIDA AURORA SA',
    'SEGUROS DE VIDA COLPATRIA S.A.'
];

export const CUSTOM_COMPANY_CCF = {
    'CAFABA': '75%',
    'CAFAM': '75%',
    'CAFAMAZ': '95%',
    'CAFASUR': '95%',
    'CAJACOPI ATLANTICO': '85%',
    'CAJAMAG': '65%',
    'CAJASAI': '55%',
    'CAJASAN': '50%',
    'COFREM': '95%',
    'COLSUBSIDIO': '95%',
    'COMBARRANQUILLA': '95%',
    'COMCAJA': '95%',
    'COMFA GUAJIRA': '95%',
    'COMFABOY': '95%',
    'COMFACA': '55%',
    'COMFACASANARE': '75%',
    'COMFACAUCA': '95%',
    'COMFACESAR': '95%',
    'COMFACHOCO': '35%',
    'COMFACOR': '75%',
    'COMFACUNDI': '95%',
    'COMFAMA': '95%',
    'COMFAMILIAR CAMACOL': '95%',
    'COMFAMILIAR CARTAGENA': '95%',
    'COMFAMILIAR DEL ATLANTICO': '95%',
    'COMFAMILIAR HUILA': '80%',
    'COMFAMILIAR NARIÑO': '20%',
    'COMFAMILIAR PUTUMAYO': '95%',
    'COMFAMILIAR RISARALDA': '95%',
    'COMFAMILIARES': '73%',
    'COMFANDI': '95%',
    'COMFANORTE': '95%',
    'COMFAORIENTE': '95%',
    'COMFASUCRE': '95%',
    'COMFATOLIMA': '95%',
    'COMFENALCO ANTIOQUIA': '95%',
    'COMFENALCO CARTAGENA': '95%',
    'COMFENALCO QUINDIO': '95%',
    'COMFENALCO SANTANDER': '75%',
    'COMFENALCO TOLIMA': '23%',
    'COMFENALCO VALLE': '95%',
    'COMFIAR': '95%',
    'COMPENSAR CCF': '95%'
};

export const CUSTOM_DIALOG_ARL = {
    'ARL SURA': '90%',
    'COLMENA VIDA Y RIESGOS PROFESIONALES': '90%',
    'LA EQUIDAD SEGUROS DE VIDA': '90%',
    'MAPFRE COLOMBIA': '90%',
    'POSITIVA DE SEGUROS': '90%',
    'SEGUROS ALFA S.A': '90%',
    'SEGUROS BOLIVAR': '90%',
    'SEGUROS DE VIDA COLPATRIA S.A.': '75%',
    'VIDA AURORA SA': '55%'
};

export const CUSTOM_DIALOG_CCF = {
    'CAFABA': '90%',
    'CAFAM': '90%',
    'CAFAMAZ': '90%',
    'CAFASUR': '90%',
    'CAJACOPI ATLANTICO': '90%',
    'CAJAMAG': '90%',
    'CAJASAI': '90%',
    'CAJASAN': '90%',
    'COFREM': '90%',
    'COLSUBSIDIO': '90%',
    'COMBARRANQUILLA': '90%',
    'COMCAJA': '90%',
    'COMFA GUAJIRA': '90%',
    'COMFABOY': '90%',
    'COMFACA': '90%',
    'COMFACASANARE': '90%',
    'COMFACAUCA': '100%',
    'COMFACESAR': '90%',
    'COMFACHOCO': '80%',
    'COMFACOR': '90%',
    'COMFACUNDI': '90%',
    'COMFAMA': '90%',
    'COMFAMILIAR CAMACOL': '90%',
    'COMFAMILIAR CARTAGENA': '90%',
    'COMFAMILIAR DEL ATLANTICO': '90%',
    'COMFAMILIAR HUILA': '90%',
    'COMFAMILIAR NARIÑO': '40%',
    'COMFAMILIAR PUTUMAYO': '90%',
    'COMFAMILIAR RISARALDA': '90%',
    'COMFAMILIARES': '90%',
    'COMFANDI': '90%',
    'COMFANORTE': '90%',
    'COMFAORIENTE': '90%',
    'COMFASUCRE': '90%',
    'COMFATOLIMA': '100%',
    'COMFENALCO ANTIOQUIA': '90%',
    'COMFENALCO CARTAGENA': '90%',
    'COMFENALCO QUINDIO': '90%',
    'COMFENALCO SANTANDER': '90%',
    'COMFENALCO TOLIMA': '40%',
    'COMFENALCO VALLE': '90%',
    'COMFIAR': '90%',
    'COMPENSAR CCF': '90%'
};

export const UPLOAD_LOGO = 'Subir logo';

export const OBLIGACIONES_TITLE = 'Obligaciones';

export const OBLIGACIONES = {
    SALUD: 'Salud',
    PENSION: 'Pensión',
    RIESGOS: 'Riesgos',
    CCF: 'CCF',
    SENA: 'SENA',
    ICBF: 'ICBF'
}

export const EXENTO_PARAFISCALES = 'Exento de pago de parafiscales';

export const BENEFIADO_LEY590 = 'Beneficiado ley 590 del 2000';

export const FECHA_CONSTITUCION = 'Fecha constitución compañía';

export const ADMINISTRADORAS = 'Administradoras';

export const HELPER_TEXT = {
    COMPANY_NAME: 'Nombre de la empresa / Razón social',
    CIIU: 'CIIU - Actividad económica',
    TIPO_VIAL: 'Dirección',
    NUMERO_VIAL: 'Ej: 8',
    LETRA_VIAL: 'Ej: A',
    TIPO_CUADRANTE: 'Ej: Sur',
    NUMERO_VIA_GENERADORA: 'Ej: 53',
    CUADRANTE_VIA_GENERADORA: 'Ej: B',
    NUMERO_PLACA: 'Ej: 112',
    INFO_ADICIONAL: '',
    PROVINCE: 'Departamento',
    CITY: 'Municipio',
    PHONE_NUMBER: 'Teléfono fijo',
    MOBILE_NUMBER: 'Celular',
    COMPANY_IDENTIFICATION_NUMBER: 'Identificación',
    IDENTIFICATION_DIGIT: 'DV',
    CLASE_APORTANTE: 'Clase de aportante',
    ADICIONALES_DIRECCION: 'Adicionales dirección'
}

export const COMPANY_INFO_TITLE = 'Información de la empresa';
export const ADD_SUCURSAL_BUTTON = 'Crear sucursal';
export const CONTINUE_BUTTON = 'Continuar';