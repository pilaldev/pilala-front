import React, { useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Card,
  Avatar,
  Typography,
  // Link,
  TextField,
  Grid,
  InputAdornment,
  Button,
  ButtonBase,
  MenuItem,
  Dialog,
  IconButton,
  Paper,
  Select,
  Checkbox,
  Tooltip,
  Collapse,
  CircularProgress,
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import BusinessTwoToneIcon from '@material-ui/icons/BusinessTwoTone';
// import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import CheckBoxTwoToneIcon from '@material-ui/icons/CheckBoxTwoTone';
import CheckBoxOutlineBlankRoundedIcon from '@material-ui/icons/CheckBoxOutlineBlankRounded';
import InfoTwoToneIcon from '@material-ui/icons/InfoTwoTone';
import TodayTwoToneIcon from '@material-ui/icons/TodayTwoTone';
import RadioButtonUncheckedTwoToneIcon from '@material-ui/icons/RadioButtonUncheckedTwoTone';
import CheckCircleTwoToneIcon from '@material-ui/icons/CheckCircleTwoTone';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import clsx from 'clsx';
import moment from 'moment';
import PropTypes from 'prop-types';
import 'moment/locale/es';
import './Styles.css';
import { COMPANY, SNACKBAR } from '../redux/ActionTypes';
import { companyAction, snackBarAction } from '../redux/Actions';
import {
  ARL,
  CAJA_COMPENSACION,
  FIELDS_ID,
  CANCEL_BUTTON,
  SAVE_BUTTON,
  // EDIT_BUTTON,
  SELECT_ARL,
  CUSTOM_COMPANY_ARL,
  CUSTOM_COMPANY_CCF,
  CUSTOM_DIALOG_ARL,
  // UPLOAD_LOGO,
  HELPER_TEXT,
  OBLIGACIONES_TITLE,
  OBLIGACIONES,
  EXENTO_PARAFISCALES,
  BENEFIADO_LEY590,
  FECHA_CONSTITUCION,
  ADMINISTRADORAS,
  ENABLED_FIELDS_TO_EDIT,
} from './Constants';
import REGEX from '../utils/RegularExpressions';
import CalcularDV from '../utils/CalcularDV';
import HelperFunctions from '../utils/HelperFunctions';
import { Calendar } from '../components';
import ModalCCF from './ModalCCF';
import {
  useCompanyCardStyles,
  useDisabledInputStyles,
  useRequiredFieldStyles,
} from './styles';

moment.locale('es');

const textfieldStyles = {
  required: {
    backgroundColor: '#FF495A26',
  },
  disabled: {
    backgroundColor: '#ECEFF1',
    borderRadius: 7,
    padding: '5px 10px',
    height: 22,
  },
  disabledNitInput: {
    height: 32,
  },
};

const CompanyCard = ({
  disableCompanyButton,
  saveCompanyChanges,
  editCompany,
  createdCompany,
}) => {
  const classes = useCompanyCardStyles();
  const disabledInputClasses = useDisabledInputStyles();
  const requiredFieldClasses = useRequiredFieldStyles();
  const dispatch = useDispatch();
  const companyName = useSelector((state) => state.company.companyName);
  const economicActivity = useSelector(
    (state) => state.company.economicActivity
  );
  const province = useSelector((state) => state.company.province);
  const city = useSelector((state) => state.company.city);
  const phoneNumber = useSelector((state) => state.company.phoneNumber);
  const cellphoneNumber = useSelector((state) => state.company.cellphoneNumber);
  const identificationType = useSelector(
    (state) => state.company.identificationType
  );
  const identificationNumber = useSelector(
    (state) => state.company.identificationNumber
  );
  const verificationDigit = useSelector(
    (state) => state.company.verificationDigit
  );
  const address = useSelector((state) => state.company.address);
  const getDisableStatus = useCallback(() => {
    if (createdCompany === true) {
      if (editCompany) return false;
      return true;
    }
    return false;
  }, [createdCompany, editCompany]);
  const disableInput = getDisableStatus();
  const cities = useSelector((state) => state.resources.cities);
  const provinces = useSelector((state) => state.resources.provinces);
  const ciiu = useSelector((state) => state.resources.ciiu);
  const ARLs = useSelector((state) => state.resources.arl);
  const arl = useSelector((state) => state.company.arl);
  const claseArl = useSelector((state) => state.company.claseArl);
  const clasesArl = useSelector((state) => state.resources.clasesArl);
  const cajaCompensacion = useSelector(
    (state) => state.company.cajaCompensacion
  );
  const fullClasesArl = [
    {
      code: -1,
      display: 'Nivel de riesgo',
      value: 'nivel de riesgo',
    },
    ...clasesArl,
  ];
  const [openARLModal, setOpenARLModal] = useState(false);
  const [openCCFModal, setOpenCCFModal] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [claseArlError, setClaseArlError] = useState(false);
  const exentoParafiscales = useSelector(
    (state) => state.company.exentoParafiscales
  );
  const beneficiadoLey590 = useSelector(
    (state) => state.company.beneficiadoLey590
  );
  const obligacionSalud = useSelector((state) => state.company.obligacionSalud);
  const obligacionPension = useSelector(
    (state) => state.company.obligacionPension
  );
  const obligacionRiesgos = useSelector(
    (state) => state.company.obligacionRiesgos
  );
  const obligacionCCF = useSelector((state) => state.company.obligacionCCF);
  const obligacionSena = useSelector((state) => state.company.obligacionSena);
  const obligacionICBF = useSelector((state) => state.company.obligacionICBF);
  // const tipoVial = useSelector(state => state.resources.tipoVial);
  // const tiposCuadrante = useSelector(state => state.resources.tiposCuadrante);
  const clasesAportante = useSelector(
    (state) => state.resources.clasesAportante
  );
  const claseAportante = useSelector((state) => state.company.claseAportante);
  const showClaseArlPicker = useSelector(
    (state) => state.company.showClaseArlPicker
  );
  const mainCompany = useSelector((state) => state.company.mainCompany);
  const companyRequiredFields = useSelector(
    (state) => state.company.companyRequiredFields
  );
  const identificacionAportante = useSelector(
    (state) => state.resources.identificacionAportante
  );
  // const tipoVialFull = [
  //     {
  //         display: '',
  //         code: ''
  //     },
  //     ...tipoVial
  // ];
  // const tiposCuadranteFull = [
  //     {
  //         display: '',
  //         code: '0'
  //     },
  //     ...tiposCuadrante
  // ];
  const claseAportanteFull = [
    {
      display: 'Clase de aportante',
      code: -1,
    },
    ...clasesAportante,
  ];
  const [openDatePicker, setOpenDatePicker] = useState(false);
  const [constitucionAnchor, setConstitucionAnchor] = useState({});
  const constitucionDate = useSelector(
    (state) => state.company.constitucionDate
  );

  useEffect(() => {
    dispatch(
      companyAction(COMPANY.COMPANY_SAVE_APORTANTE_CLASS, clasesAportante[0])
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSetOpenARLModal = () => {
    setOpenARLModal(!openARLModal);
    if (!openARLModal) {
      dispatch(companyAction(COMPANY.COMPANY_COPY_ADMINS_DATA, null));
    }
  };

  const handleSetOpenCCFModal = () => {
    if (province.value !== '') {
      setOpenCCFModal(!openCCFModal);
      if (!openCCFModal) {
        dispatch(companyAction(COMPANY.COMPANY_COPY_ADMINS_DATA, null));
      }
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description:
            'Hace falta que primero selecciones el departamento de la compañía',
          color: 'warning',
        })
      );
    }
  };

  const removeRequiredField = (field) => {
    const data = {
      field,
      status: false,
    };
    dispatch(companyAction(COMPANY.COMPANY_COMPANY_REQUIRED_FIELD, data));
  };

  const selectARL = (item) => {
    dispatch(companyAction(COMPANY.COMPANY_SAVE_ARL, item));
    if (showClaseArlPicker === false) {
      dispatch(companyAction(COMPANY.COMPANY_SHOW_CLASE_ARL_PICKER, null));
    }
    if (arl === '') {
      removeRequiredField(FIELDS_ID.ARL);
    }
  };

  const selectCCF = (item) => {
    dispatch(companyAction(COMPANY.COMPANY_SAVE_CAJA_COMPENSACION, item));
    handleSetOpenCCFModal();
    if (cajaCompensacion.value === null) {
      removeRequiredField(FIELDS_ID.CAJA_COMPENSACION);
    }
  };

  const filterCitiesList = (currentOptions, optionsState) => {
    if (optionsState.inputValue !== '') {
      return currentOptions.filter(
        (item) =>
          item.provinceCode === province.code &&
          item.value.indexOf(optionsState.inputValue.toLowerCase()) > -1
      );
    }
    return cities.filter((item) => item.provinceCode === province.code);
  };

  const getListValue = (event, value) => {
    const fieldId = event.target.id.split('-')[0];
    switch (fieldId) {
      case FIELDS_ID.ECONOMIC_ACTIVITY:
        dispatch(companyAction(COMPANY.COMPANY_SAVE_ECONOMIC_ACTIVITY, value));
        if (economicActivity.code === '-1') {
          removeRequiredField(FIELDS_ID.ECONOMIC_ACTIVITY);
        }
        break;

      case FIELDS_ID.PROVINCE:
        dispatch(companyAction(COMPANY.COMPANY_SAVE_PROVINCE, value));
        if (province.code === '') {
          removeRequiredField(FIELDS_ID.PROVINCE);
        }
        break;

      case FIELDS_ID.CITY:
        dispatch(companyAction(COMPANY.COMPANY_SAVE_CITY, value));
        if (city.code === '') {
          removeRequiredField(FIELDS_ID.CITY);
        }
        break;

      default:
    }
  };

  const editCompanyFieldValue = (event) => {
    const { value } = event.target;
    const fieldId = event.target.id;
    const fieldName = event.target.name;

    switch (fieldId || fieldName) {
      case FIELDS_ID.COMPANY_NAME:
        if (value === '' || REGEX.NAMES.test(value)) {
          dispatch(companyAction(COMPANY.COMPANY_SAVE_COMPANY_NAME, value));
        }
        if (companyName === '') {
          removeRequiredField(FIELDS_ID.COMPANY_NAME);
        }
        break;

      case FIELDS_ID.PHONE_NUMBER:
        if (value === '' || REGEX.PHONENUMBER.test(value)) {
          dispatch(companyAction(COMPANY.COMPANY_SAVE_PHONE_NUMBER, value));
        }
        break;

      case FIELDS_ID.CELLPHONE_NUMBER:
        if (value === '' || REGEX.PHONENUMBER.test(value)) {
          dispatch(companyAction(COMPANY.COMPANY_SAVE_CELLPHONE_NUMBER, value));
        }
        break;

      case FIELDS_ID.IDENTIFICATION_TYPE:
        dispatch(
          companyAction(COMPANY.COMPANY_SAVE_IDENTIFICATION_TYPE, value)
        );
        if (identificationType.code === '') {
          removeRequiredField(FIELDS_ID.IDENTIFICATION_TYPE);
        }
        break;

      case FIELDS_ID.IDENTIFICATION_NUMBER:
        if (value === '' || REGEX.NUMBERS.test(value)) {
          dispatch(
            companyAction(COMPANY.COMPANY_SAVE_IDENTIFICATION_NUMBER, value)
          );
          dispatch(
            companyAction(
              COMPANY.COMPANY_SAVE_VERIFICATION_DIGIT,
              CalcularDV(value)
            )
          );
        }
        if (identificationNumber === '') {
          removeRequiredField(FIELDS_ID.IDENTIFICATION_NUMBER);
        }
        break;

      case FIELDS_ID.CLASE_ARL: {
        const tempClase = fullClasesArl.find(
          (option) => option.code === value.code
        );
        dispatch(companyAction(COMPANY.COMPANY_SAVE_CLASE_ARL, tempClase));
        dispatch(companyAction(COMPANY.COMPANY_RESET_COPY_OBJECTS, null));
        handleSetOpenARLModal();
        if (claseArl.code === -1) {
          removeRequiredField(FIELDS_ID.CLASE_ARL);
        }
        break;
      }

      case FIELDS_ID.CLASE_APORTANTE:
        dispatch(companyAction(COMPANY.COMPANY_SAVE_APORTANTE_CLASS, value));
        if (claseAportante.code === -1) {
          removeRequiredField(FIELDS_ID.CLASE_APORTANTE);
        }
        break;

      case FIELDS_ID.ADDRESS.TIPO_VIAL: {
        const tempTipoVial = {
          idField: FIELDS_ID.ADDRESS.TIPO_VIAL,
          data: value,
        };
        dispatch(
          companyAction(COMPANY.COMPANY_UPDATE_ADDRESS_FIELD, tempTipoVial)
        );
        if (address.tipoVial.code === '') {
          removeRequiredField(FIELDS_ID.ADDRESS.TIPO_VIAL);
        }
        break;
      }

      case FIELDS_ID.ADDRESS.NUMERO_VIAL:
        if (value === '' || REGEX.ADDRESS_NUMBER.test(value)) {
          const tempNumeroVial = {
            idField: FIELDS_ID.ADDRESS.NUMERO_VIAL,
            data: value,
          };
          dispatch(
            companyAction(COMPANY.COMPANY_UPDATE_ADDRESS_FIELD, tempNumeroVial)
          );
        }
        if (address.numeroVial === '') {
          removeRequiredField(FIELDS_ID.ADDRESS.NUMERO_VIAL);
        }
        break;

      case FIELDS_ID.ADDRESS.LETRA_VIAL:
        if (value === '' || REGEX.ADDRESS_LETTER.test(value)) {
          const tempLetraVial = {
            idField: FIELDS_ID.ADDRESS.LETRA_VIAL,
            data: value.toUpperCase(),
          };
          dispatch(
            companyAction(COMPANY.COMPANY_UPDATE_ADDRESS_FIELD, tempLetraVial)
          );
        }
        break;

      case FIELDS_ID.ADDRESS.TIPO_CUADRANTE: {
        const tempTipoCuadrante = {
          idField: FIELDS_ID.ADDRESS.TIPO_CUADRANTE,
          data: value,
        };
        dispatch(
          companyAction(COMPANY.COMPANY_UPDATE_ADDRESS_FIELD, tempTipoCuadrante)
        );
        break;
      }

      case FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA:
        if (value === '' || REGEX.ADDRESS_NUMBER.test(value)) {
          const tempNumeroViaGeneradora = {
            idField: FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA,
            data: value,
          };
          dispatch(
            companyAction(
              COMPANY.COMPANY_UPDATE_ADDRESS_FIELD,
              tempNumeroViaGeneradora
            )
          );
        }
        if (address.numeroViaGeneradora === '') {
          removeRequiredField(FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA);
        }
        break;

      case FIELDS_ID.ADDRESS.CUADRANTE_VIA_GENERADORA:
        if (value === '' || REGEX.ADDRESS_LETTER.test(value)) {
          const tempCuadranteViaGeneradora = {
            idField: FIELDS_ID.ADDRESS.CUADRANTE_VIA_GENERADORA,
            data: value.toUpperCase(),
          };
          dispatch(
            companyAction(
              COMPANY.COMPANY_UPDATE_ADDRESS_FIELD,
              tempCuadranteViaGeneradora
            )
          );
        }
        break;

      case FIELDS_ID.ADDRESS.NUMERO_PLACA:
        if (value === '' || REGEX.ADDRESS_NUMBER.test(value)) {
          const tempNumeroPlaca = {
            idField: FIELDS_ID.ADDRESS.NUMERO_PLACA,
            data: value,
          };
          dispatch(
            companyAction(COMPANY.COMPANY_UPDATE_ADDRESS_FIELD, tempNumeroPlaca)
          );
        }
        if (address.numeroPlaca === '') {
          removeRequiredField(FIELDS_ID.ADDRESS.NUMERO_PLACA);
        }
        break;

      case FIELDS_ID.ADDRESS.INFO_ADICIONAL: {
        const tempInfoAdicional = {
          idField: FIELDS_ID.ADDRESS.INFO_ADICIONAL,
          data: value,
        };
        dispatch(
          companyAction(COMPANY.COMPANY_UPDATE_ADDRESS_FIELD, tempInfoAdicional)
        );
        break;
      }

      default:
    }
  };

  // const editCompanyAction = () => {
  //     dispatch(companyAction(COMPANY.COMPANY_EDIT_COMPANY, null));
  //     dispatch(companyAction(COMPANY.COMPANY_COPY_COMPANY_DATA, 'company'));
  // }

  const cancelEditCompanyAction = () => {
    dispatch(companyAction(COMPANY.COMPANY_EDIT_COMPANY, null));
    dispatch(companyAction(COMPANY.COMPANY_RESET_BACKUP_DATA, 'company'));
  };

  const selectExentoParafiscales = () => {
    if (beneficiadoLey590 === false) {
      dispatch(companyAction(COMPANY.COMPANY_EXENTO_PARAFISCALES, null));
    }
  };

  const selectBeneficiadoLey590 = () => {
    if (exentoParafiscales === false) {
      if (beneficiadoLey590) {
        const value = {
          display: 'DD/MM/YYYY',
          code: '',
          value: '',
        };
        dispatch(companyAction(COMPANY.COMPANY_SAVE_CONSTITUCION_DATE, value));
      }
      dispatch(companyAction(COMPANY.COMPANY_BENEFICIADO_LEY590, null));
    }
  };

  const showDatePicker = (event) => {
    setConstitucionAnchor(event.currentTarget);
    setOpenDatePicker(!openDatePicker);
  };

  const getConstitucionDate = (data) => {
    const value = {
      display: moment(data).format('DD/MM/YYYY'),
      code: moment(data).format('YYYY-MM-DD'),
      value: new Date(data),
    };
    dispatch(companyAction(COMPANY.COMPANY_SAVE_CONSTITUCION_DATE, value));
    setOpenDatePicker(!openDatePicker);
    if (constitucionDate.code === '') {
      removeRequiredField(FIELDS_ID.FECHA_CONSTITUCION);
    }
  };

  const closedArlDialog = () => {
    if (showClaseArlPicker) {
      dispatch(companyAction(COMPANY.COMPANY_SHOW_CLASE_ARL_PICKER, null));
    }
  };

  const returnArlDialog = () => {
    setOpenARLModal(!openARLModal);
    dispatch(companyAction(COMPANY.COMPANY_RESET_ADMINS_COPY, null));
  };

  const returnCCFDialog = () => {
    setOpenCCFModal(!openCCFModal);
    dispatch(companyAction(COMPANY.COMPANY_RESET_ADMINS_COPY, null));
  };

  return (
    <div className={classes.container}>
      <Card className={classes.card}>
        {mainCompany ? (
          <div>
            {createdCompany ? (
              <Grid
                container
                direction="row"
                justify="flex-end"
                alignItems="center"
                className={classes.editGrid}
              >
                {/* <ButtonBase
                                                className={classes.editButton}
                                                disabled={editCompany}
                                                onClick={editCompanyAction}
                                            >
                                                <BorderColorRoundedIcon className={classes.editIcon} />
                                                <Typography
                                                    variant='subtitle2'
                                                    className={classes.editButtonText}
                                                >
                                                    {EDIT_BUTTON}
                                                </Typography>
                                            </ButtonBase> */}
              </Grid>
            ) : null}
          </div>
        ) : null}
        <div
          className={classes.contentContainer}
          style={{
            pointerEvents: disableInput ? 'none' : 'auto',
          }}
        >
          <div className={classes.firstContainer}>
            <Avatar className={classes.avatar}>
              <BusinessTwoToneIcon
                style={{ color: '#C6CCD0', fontSize: '3rem' }}
              />
            </Avatar>
            {/* <Link id='uploadLogoLink'>
                                <Typography
                                    variant='subtitle2'
                                    className={classes.uploadLogoLink}
                                >
                                    {UPLOAD_LOGO}
                                </Typography>
                            </Link> */}
          </div>
          <div className={classes.secondContainer}>
            <TextField
              className={classes.textField}
              helperText={HELPER_TEXT.COMPANY_NAME}
              value={companyName}
              id={FIELDS_ID.COMPANY_NAME}
              onChange={editCompanyFieldValue}
              style={{
                pointerEvents: createdCompany ? 'none' : 'auto',
              }}
              InputProps={{
                inputProps: {
                  style: companyRequiredFields.includes(FIELDS_ID.COMPANY_NAME)
                    ? textfieldStyles.required
                    : createdCompany
                    ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.COMPANY_NAME)
                      ? !editCompany
                        ? textfieldStyles.disabled
                        : {}
                      : textfieldStyles.disabled
                    : {},
                },
                classes: {
                  underline: companyRequiredFields.includes(
                    FIELDS_ID.COMPANY_NAME
                  )
                    ? requiredFieldClasses.underlineRequired
                    : classes.underline,
                },
                disableUnderline: createdCompany
                  ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.COMPANY_NAME)
                    ? !editCompany
                    : true
                  : false,
              }}
            />
            <Autocomplete
              id={FIELDS_ID.ECONOMIC_ACTIVITY}
              style={{
                pointerEvents: createdCompany ? 'none' : 'auto',
              }}
              onChange={getListValue}
              disableClearable
              value={economicActivity}
              className={classes.textField}
              getOptionLabel={(option) => `${option.code} - ${option.display}`}
              options={ciiu}
              popupIcon={
                createdCompany ? (
                  ENABLED_FIELDS_TO_EDIT.includes(
                    FIELDS_ID.ECONOMIC_ACTIVITY
                  ) ? (
                    !editCompany ? null : (
                      <ArrowDropDownIcon />
                    )
                  ) : null
                ) : (
                  <ArrowDropDownIcon />
                )
              }
              classes={{
                inputRoot: companyRequiredFields.includes(
                  FIELDS_ID.ECONOMIC_ACTIVITY
                )
                  ? requiredFieldClasses.inputRoot
                  : createdCompany
                  ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.ECONOMIC_ACTIVITY)
                    ? !editCompany
                      ? requiredFieldClasses.disabledInputRoot
                      : null
                    : requiredFieldClasses.disabledInputRoot
                  : null,
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  margin="normal"
                  helperText={HELPER_TEXT.CIIU}
                  InputProps={{
                    ...params.InputProps,
                    classes: {
                      underline: companyRequiredFields.includes(
                        FIELDS_ID.ECONOMIC_ACTIVITY
                      )
                        ? requiredFieldClasses.underlineRequired
                        : classes.underline,
                    },
                    disableUnderline: createdCompany
                      ? ENABLED_FIELDS_TO_EDIT.includes(
                          FIELDS_ID.ECONOMIC_ACTIVITY
                        )
                        ? !editCompany
                        : true
                      : false,
                  }}
                />
              )}
            />
            {/* <Grid
                                direction='row'
                                container
                                justify='space-between'
                            >
                                <TextField
                                    select={true}
                                    helperText={HELPER_TEXT.TIPO_VIAL}
                                    value={tipoVialFull.find(tipo => tipo.code === address.tipoVial.code)}
                                    onChange={editCompanyFieldValue}
                                    className={classes.tipoVial}
                                    name={FIELDS_ID.ADDRESS.TIPO_VIAL}
                                    SelectProps={{
                                        classes: {
                                            selectMenu: companyRequiredFields.includes(FIELDS_ID.ADDRESS.TIPO_VIAL) ?
                                                requiredFieldClasses.selectMenu
                                                : null,
                                            root: createdCompany ?
                                                !editCompany ?
                                                    requiredFieldClasses.disabledSelectMenu
                                                    : null
                                                : null

                                        },
                                        IconComponent: createdCompany ? !editCompany ? (props) => null : ArrowDropDownIcon : ArrowDropDownIcon
                                    }}
                                    InputProps={{
                                        classes: {
                                            underline: companyRequiredFields.includes(FIELDS_ID.ADDRESS.TIPO_VIAL) ? requiredFieldClasses.underlineRequired : classes.underline
                                        },
                                        disableUnderline: createdCompany ? !editCompany : false
                                    }}
                                >
                                    {
                                        tipoVialFull.map((item, index) => (
                                            <MenuItem key={index} value={item} disabled={index === 0}>
                                                {item.display}
                                            </MenuItem>
                                        ))
                                    }
                                </TextField>
                                <TextField
                                    helperText={HELPER_TEXT.NUMERO_VIAL}
                                    className={classes.numeroVial}
                                    inputProps={{
                                        className: classes.justifyTextfieldText,
                                        style: companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_VIAL) ?
                                            textfieldStyles.required
                                            :
                                            createdCompany ?
                                                !editCompany ?
                                                    textfieldStyles.disabled
                                                    :
                                                    {}
                                                : {}
                                    }}
                                    InputProps={{
                                        classes: {
                                            underline: companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_VIAL) ? requiredFieldClasses.underlineRequired : classes.underline
                                        },
                                        disableUnderline: createdCompany ? !editCompany : false
                                    }}
                                    onChange={editCompanyFieldValue}
                                    name={FIELDS_ID.ADDRESS.NUMERO_VIAL}
                                    value={address.numeroVial}
                                />
                                <TextField
                                    helperText={HELPER_TEXT.LETRA_VIAL}
                                    className={classes.letraVial}
                                    inputProps={{
                                        className: classes.justifyTextfieldText,
                                        style: createdCompany ?
                                            !editCompany ?
                                                textfieldStyles.disabled
                                                :
                                                {}
                                            : {}
                                    }}
                                    onChange={editCompanyFieldValue}
                                    name={FIELDS_ID.ADDRESS.LETRA_VIAL}
                                    value={address.letraVial}
                                    InputProps={{
                                        classes: {
                                            underline: classes.underline
                                        },
                                        disableUnderline: createdCompany ? !editCompany : false
                                    }}
                                />
                                <TextField
                                    select={true}
                                    helperText={HELPER_TEXT.TIPO_CUADRANTE}
                                    value={tiposCuadranteFull.find(tipo => tipo.code === address.tipoCuadrante.code)}
                                    onChange={editCompanyFieldValue}
                                    className={classes.tipoCuadrante}
                                    name={FIELDS_ID.ADDRESS.TIPO_CUADRANTE}
                                    InputProps={{
                                        classes: {
                                            underline: classes.underline
                                        },
                                        disableUnderline: createdCompany ? !editCompany : false
                                    }}
                                    SelectProps={{
                                        classes: {
                                            root: createdCompany ?
                                                !editCompany ?
                                                    requiredFieldClasses.disabledSelectMenu
                                                    : null
                                                : null
                                        },
                                        IconComponent: createdCompany ? !editCompany ? (props) => null : ArrowDropDownIcon : ArrowDropDownIcon
                                    }}
                                >
                                    {
                                        tiposCuadranteFull.map((item, index) => (
                                            <MenuItem key={index} value={item} disabled={index === 0}>
                                                {item.display}
                                            </MenuItem>
                                        ))
                                    }
                                </TextField>
                                <Typography className={classes.numberSymbol}>
                                    #
                                </Typography>
                                <TextField
                                    helperText={HELPER_TEXT.NUMERO_VIA_GENERADORA}
                                    className={classes.numeroViaGeneradora}
                                    inputProps={{
                                        className: classes.justifyTextfieldText,
                                        style: companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA) ?
                                            textfieldStyles.required
                                            :
                                            createdCompany ?
                                                !editCompany ?
                                                    textfieldStyles.disabled
                                                    :
                                                    {}
                                                : {}

                                    }}
                                    InputProps={{
                                        classes: {
                                            underline: companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA) ? requiredFieldClasses.underlineRequired : classes.underline
                                        },
                                        disableUnderline: createdCompany ? !editCompany : false
                                    }}
                                    onChange={editCompanyFieldValue}
                                    name={FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA}
                                    value={address.numeroViaGeneradora}
                                />
                                <TextField
                                    helperText={HELPER_TEXT.CUADRANTE_VIA_GENERADORA}
                                    className={classes.cuadranteViaGeneradora}
                                    inputProps={{
                                        className: classes.justifyTextfieldText,
                                        style: createdCompany ?
                                            !editCompany ?
                                                textfieldStyles.disabled
                                                :
                                                {}
                                            : {}
                                    }}
                                    onChange={editCompanyFieldValue}
                                    name={FIELDS_ID.ADDRESS.CUADRANTE_VIA_GENERADORA}
                                    value={address.cuadranteViaGeneradora}
                                    InputProps={{
                                        classes: {
                                            underline: classes.underline
                                        },
                                        disableUnderline: createdCompany ? !editCompany : false
                                    }}
                                />
                                <TextField
                                    helperText={HELPER_TEXT.NUMERO_PLACA}
                                    className={classes.numeroPlaca}
                                    inputProps={{
                                        className: classes.justifyTextfieldText,
                                        style: companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_PLACA) ?
                                            textfieldStyles.required
                                            :
                                            createdCompany ?
                                                !editCompany ?
                                                    textfieldStyles.disabled
                                                    :
                                                    {}
                                                : {}
                                    }}
                                    InputProps={{
                                        classes: {
                                            underline: companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_PLACA) ? requiredFieldClasses.underlineRequired : classes.underline
                                        },
                                        disableUnderline: createdCompany ? !editCompany : false
                                    }}
                                    onChange={editCompanyFieldValue}
                                    name={FIELDS_ID.ADDRESS.NUMERO_PLACA}
                                    value={address.numeroPlaca}
                                    variant='standard'
                                />
                            </Grid> */}
            <Grid
              direction="row"
              container
              justify="space-between"
              className={classes.fieldsGrid}
            >
              <Autocomplete
                id={FIELDS_ID.PROVINCE}
                style={{
                  pointerEvents: createdCompany ? 'none' : 'auto',
                }}
                options={provinces}
                onChange={getListValue}
                disableClearable
                value={province}
                getOptionLabel={(option) => option.display}
                className={classes.fieldGrid}
                popupIcon={
                  createdCompany ? (
                    ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.PROVINCE) ? (
                      !editCompany ? null : (
                        <ArrowDropDownIcon />
                      )
                    ) : null
                  ) : (
                    <ArrowDropDownIcon />
                  )
                }
                classes={{
                  inputRoot: companyRequiredFields.includes(FIELDS_ID.PROVINCE)
                    ? requiredFieldClasses.inputRoot
                    : createdCompany
                    ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.PROVINCE)
                      ? !editCompany
                        ? requiredFieldClasses.disabledInputRoot
                        : null
                      : requiredFieldClasses.disabledInputRoot
                    : null,
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    helperText={HELPER_TEXT.PROVINCE}
                    InputProps={{
                      ...params.InputProps,
                      classes: {
                        underline: companyRequiredFields.includes(
                          FIELDS_ID.PROVINCE
                        )
                          ? requiredFieldClasses.underlineRequired
                          : classes.underline,
                      },
                      disableUnderline: createdCompany
                        ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.PROVINCE)
                          ? !editCompany
                          : true
                        : false,
                    }}
                  />
                )}
              />
              <Autocomplete
                id={FIELDS_ID.CITY}
                style={{
                  pointerEvents: createdCompany ? 'none' : 'auto',
                }}
                onChange={getListValue}
                value={city}
                className={classes.fieldGrid}
                disableClearable
                options={cities}
                filterOptions={filterCitiesList}
                getOptionLabel={(option) => option.display}
                popupIcon={
                  createdCompany ? (
                    ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.CITY) ? (
                      !editCompany ? null : (
                        <ArrowDropDownIcon />
                      )
                    ) : null
                  ) : (
                    <ArrowDropDownIcon />
                  )
                }
                classes={{
                  inputRoot: companyRequiredFields.includes(FIELDS_ID.CITY)
                    ? requiredFieldClasses.inputRoot
                    : createdCompany
                    ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.CITY)
                      ? !editCompany
                        ? requiredFieldClasses.disabledInputRoot
                        : null
                      : requiredFieldClasses.disabledInputRoot
                    : null,
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    helperText={HELPER_TEXT.CITY}
                    InputProps={{
                      ...params.InputProps,
                      classes: {
                        underline: companyRequiredFields.includes(
                          FIELDS_ID.CITY
                        )
                          ? requiredFieldClasses.underlineRequired
                          : classes.underline,
                      },
                      disableUnderline: createdCompany
                        ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.CITY)
                          ? !editCompany
                          : true
                        : false,
                    }}
                  />
                )}
              />
              <TextField
                className={classes.phoneGrid}
                helperText={HELPER_TEXT.PHONE_NUMBER}
                value={phoneNumber}
                onChange={editCompanyFieldValue}
                id={FIELDS_ID.PHONE_NUMBER}
                InputProps={{
                  inputProps: {
                    style: createdCompany
                      ? !editCompany
                        ? textfieldStyles.disabled
                        : {}
                      : {},
                  },
                  classes: {
                    underline: classes.underline,
                  },
                  disableUnderline: createdCompany ? !editCompany : false,
                }}
              />
              <TextField
                className={classes.phoneGrid}
                helperText={HELPER_TEXT.MOBILE_NUMBER}
                value={cellphoneNumber}
                onChange={editCompanyFieldValue}
                id={FIELDS_ID.CELLPHONE_NUMBER}
                InputProps={{
                  inputProps: {
                    style: createdCompany
                      ? !editCompany
                        ? textfieldStyles.disabled
                        : {}
                      : {},
                  },
                  classes: {
                    underline: classes.underline,
                  },
                  disableUnderline: createdCompany ? !editCompany : false,
                }}
              />
            </Grid>
            <Grid direction="column" container justify="space-evenly">
              <Typography className={classes.obligacionesTitle}>
                {OBLIGACIONES_TITLE}
              </Typography>
              <div className={classes.obligacionesContainer}>
                <div className={classes.obligacionRowContainer}>
                  <Checkbox
                    checked={obligacionSalud}
                    disabled
                    color="secondary"
                    className={classes.obligacionCheckbox}
                    checkedIcon={
                      <CheckBoxTwoToneIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                    icon={
                      <CheckBoxOutlineBlankRoundedIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                  />
                  <Typography className={classes.obligacionName}>
                    {OBLIGACIONES.SALUD}
                  </Typography>
                </div>
                <div className={classes.obligacionRowContainer}>
                  <Checkbox
                    checked={obligacionPension}
                    disabled
                    color="secondary"
                    className={classes.obligacionCheckbox}
                    checkedIcon={
                      <CheckBoxTwoToneIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                    icon={
                      <CheckBoxOutlineBlankRoundedIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                  />
                  <Typography className={classes.obligacionName}>
                    {OBLIGACIONES.PENSION}
                  </Typography>
                </div>
                <div className={classes.obligacionRowContainer}>
                  <Checkbox
                    checked={obligacionRiesgos}
                    disabled
                    color="secondary"
                    className={classes.obligacionCheckbox}
                    checkedIcon={
                      <CheckBoxTwoToneIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                    icon={
                      <CheckBoxOutlineBlankRoundedIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                  />
                  <Typography className={classes.obligacionName}>
                    {OBLIGACIONES.RIESGOS}
                  </Typography>
                </div>
                <div className={classes.obligacionRowContainer}>
                  <Checkbox
                    checked={obligacionCCF}
                    disabled
                    color="secondary"
                    className={classes.obligacionCheckbox}
                    checkedIcon={
                      <CheckBoxTwoToneIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                    icon={
                      <CheckBoxOutlineBlankRoundedIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                  />
                  <Typography className={classes.obligacionName}>
                    {OBLIGACIONES.CCF}
                  </Typography>
                </div>
                <div className={classes.obligacionRowContainer}>
                  <Checkbox
                    checked={obligacionSena}
                    disabled
                    color="secondary"
                    className={classes.obligacionCheckbox}
                    checkedIcon={
                      <CheckBoxTwoToneIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                    icon={
                      <CheckBoxOutlineBlankRoundedIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                  />
                  <Typography className={classes.obligacionName}>
                    {OBLIGACIONES.SENA}
                  </Typography>
                </div>
                <div className={classes.obligacionRowContainer}>
                  <Checkbox
                    checked={obligacionICBF}
                    disabled
                    color="secondary"
                    className={classes.obligacionCheckbox}
                    checkedIcon={
                      <CheckBoxTwoToneIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                    icon={
                      <CheckBoxOutlineBlankRoundedIcon
                        className={classes.obligacionCheckboxIcon}
                      />
                    }
                  />
                  <Typography className={classes.obligacionName}>
                    {OBLIGACIONES.ICBF}
                  </Typography>
                </div>
              </div>
            </Grid>
            <Grid
              direction="row"
              container
              justify="space-between"
              className={classes.obligacionesButtonsContainer}
            >
              <div className={classes.obligacionesButtonRow}>
                <ButtonBase
                  onClick={selectExentoParafiscales}
                  disabled={createdCompany}
                >
                  <Checkbox
                    checked={exentoParafiscales}
                    disabled
                    color="primary"
                    className={classes.obligacionCheckbox}
                    checkedIcon={
                      <CheckBoxTwoToneIcon
                        className={classes.obligacionButtonCheckedIcon}
                      />
                    }
                    icon={
                      <CheckBoxOutlineBlankRoundedIcon
                        className={classes.obligacionButtonIcon}
                      />
                    }
                  />
                  <Typography className={classes.obligacionButtonTitle}>
                    {EXENTO_PARAFISCALES}
                  </Typography>
                </ButtonBase>
                <Tooltip
                  arrow
                  placement="top"
                  classes={{
                    tooltip: classes.parafiscalesTooltip,
                    arrow: classes.parafiscalesTooltipArrow,
                  }}
                  title={
                    <>
                      Si eres <b>PERSONA JURÍDICA</b> {'y\n'}
                      <br />
                      declaras el impuesto de renta y
                      <br />
                      complementarios o <b>PERSONA</b>
                      <br />
                      <b>NATURAL</b> con más de 2 empleados,
                      <br />
                      estas exento de pagar SENA e ICBF y
                      <br />
                      la tarifa de aportes a SALUD es del
                      <br />
                      4% para empleados que ganen
                      <br />
                      menos de 10 SMLMV.
                    </>
                  }
                >
                  <InfoTwoToneIcon
                    className={classes.obligacionInfoIcon}
                    pointerEvents="auto"
                  />
                </Tooltip>
              </div>
              <div className={classes.obligacionesButtonRow}>
                <ButtonBase
                  onClick={selectBeneficiadoLey590}
                  disabled={createdCompany}
                >
                  <Checkbox
                    checked={beneficiadoLey590}
                    disabled
                    color="primary"
                    className={classes.obligacionCheckbox}
                    checkedIcon={
                      <CheckBoxTwoToneIcon
                        className={classes.obligacionButtonCheckedIcon}
                      />
                    }
                    icon={
                      <CheckBoxOutlineBlankRoundedIcon
                        className={classes.obligacionButtonIcon}
                      />
                    }
                  />
                  <Typography className={classes.obligacionButtonTitle}>
                    {BENEFIADO_LEY590}
                  </Typography>
                </ButtonBase>
                <Tooltip
                  arrow
                  placement="top"
                  classes={{
                    tooltip: classes.parafiscalesTooltip,
                    arrow: classes.parafiscalesTooltipArrow,
                  }}
                  title={
                    <>
                      Esta ley te beneficia con descuentos
                      <br />
                      en el pago de SENA, ICBF y Cajas de
                      <br />
                      compensación por tres años a partir
                      <br />
                      {'de tu fecha de constitución. '}
                      <b>Para ser</b>
                      <br />
                      <b>beneficiado has debido presentar</b>
                      <br />
                      <b>tu solicitud y estar autorizado por</b>
                      <br />
                      <b>la DIAN.</b>
                    </>
                  }
                >
                  <InfoTwoToneIcon
                    className={classes.obligacionInfoIcon}
                    pointerEvents="auto"
                  />
                </Tooltip>
              </div>
            </Grid>
            <Collapse
              in={beneficiadoLey590}
              style={{
                pointerEvents: createdCompany ? 'none' : 'auto',
              }}
            >
              <div className={classes.constitucionDateRow}>
                <Typography>{FECHA_CONSTITUCION}</Typography>
                <div className={classes.constitucionCalendarContainer}>
                  <IconButton onClick={showDatePicker}>
                    <TodayTwoToneIcon
                      className={classes.constitucionCalendarIcon}
                    />
                  </IconButton>
                  <ButtonBase
                    className={clsx(classes.constitucionDateContainer, {
                      [classes.constitucionDateContainerRequired]: companyRequiredFields.includes(
                        FIELDS_ID.FECHA_CONSTITUCION
                      ),
                    })}
                    onClick={showDatePicker}
                  >
                    <Typography
                      className={clsx(classes.dateText, {
                        [classes.dateTextRequired]: companyRequiredFields.includes(
                          FIELDS_ID.FECHA_CONSTITUCION
                        ),
                      })}
                    >
                      {constitucionDate.display}
                    </Typography>
                  </ButtonBase>
                  <Calendar
                    open={openDatePicker}
                    onChange={getConstitucionDate}
                    anchorElement={constitucionAnchor}
                    color="primary"
                  />
                </div>
              </div>
            </Collapse>
          </div>
          <div className={classes.thirdContainer}>
            <Grid direction="row" container className={classes.thirdGrid}>
              <TextField
                style={{
                  pointerEvents: createdCompany ? 'none' : 'auto',
                }}
                // disabled={props.disableInputs}
                disabled
                className={classes.nitField}
                helperText={HELPER_TEXT.COMPANY_IDENTIFICATION_NUMBER}
                value={
                  !createdCompany
                    ? identificationNumber
                    : HelperFunctions.formatStringNumber(identificationNumber)
                }
                onChange={editCompanyFieldValue}
                id={FIELDS_ID.IDENTIFICATION_NUMBER}
                InputProps={{
                  inputProps: {
                    style: companyRequiredFields.includes(
                      FIELDS_ID.IDENTIFICATION_NUMBER
                    )
                      ? textfieldStyles.required
                      : {},
                  },
                  classes: {
                    disabled: disabledInputClasses.disabled,
                    underline: companyRequiredFields.includes(
                      FIELDS_ID.IDENTIFICATION_NUMBER
                    )
                      ? requiredFieldClasses.underlineRequired
                      : classes.underline,
                  },
                  style: createdCompany
                    ? ENABLED_FIELDS_TO_EDIT.includes(
                        FIELDS_ID.IDENTIFICATION_NUMBER
                      )
                      ? !editCompany
                        ? {
                            ...textfieldStyles.disabled,
                            ...textfieldStyles.disabledNitInput,
                          }
                        : {}
                      : {
                          ...textfieldStyles.disabled,
                          ...textfieldStyles.disabledNitInput,
                        }
                    : {},
                  startAdornment: (
                    <InputAdornment position="start">
                      <TextField
                        // disabled={props.disableInputs}
                        disabled
                        select
                        value={identificacionAportante.find(
                          (id) => id.code === identificationType.code
                        )}
                        onChange={editCompanyFieldValue}
                        name={FIELDS_ID.IDENTIFICATION_TYPE}
                        InputProps={{
                          disableUnderline: true,
                        }}
                        SelectProps={{
                          classes: {
                            selectMenu: companyRequiredFields.includes(
                              FIELDS_ID.IDENTIFICATION_TYPE
                            )
                              ? requiredFieldClasses.selectMenu
                              : null,
                          },
                          IconComponent: createdCompany
                            ? ENABLED_FIELDS_TO_EDIT.includes(
                                FIELDS_ID.IDENTIFICATION_NUMBER
                              )
                              ? !editCompany
                                ? () => null
                                : ArrowDropDownIcon
                              : () => null
                            : ArrowDropDownIcon,
                        }}
                      >
                        {identificacionAportante.map((item) => (
                          <MenuItem key={item.display} value={item}>
                            {item.display}
                          </MenuItem>
                        ))}
                      </TextField>
                    </InputAdornment>
                  ),
                  disableUnderline: createdCompany
                    ? ENABLED_FIELDS_TO_EDIT.includes(
                        FIELDS_ID.IDENTIFICATION_NUMBER
                      )
                      ? !editCompany
                      : true
                    : false,
                }}
              />
              <Typography className={classes.nitDetail}>_</Typography>
              <TextField
                disabled
                className={classes.dvField}
                helperText={HELPER_TEXT.IDENTIFICATION_DIGIT}
                value={verificationDigit}
                id={FIELDS_ID.VERIFICATION_DIGIT}
                InputProps={{
                  inputProps: {
                    className: classes.justifyTextfieldText,
                    style: textfieldStyles.disabled,
                  },
                  classes: disabledInputClasses,
                  disableUnderline: true,
                }}
              />
            </Grid>
            <TextField
              select
              helperText={HELPER_TEXT.CLASE_APORTANTE}
              value={claseAportanteFull.find(
                (clase) => clase.code === claseAportante.code
              )}
              onChange={editCompanyFieldValue}
              style={{
                pointerEvents: createdCompany
                  ? 'none'
                  : beneficiadoLey590
                  ? 'none'
                  : 'auto',
              }}
              className={classes.claseAportante}
              name={FIELDS_ID.CLASE_APORTANTE}
              SelectProps={{
                classes: {
                  selectMenu: companyRequiredFields.includes(
                    FIELDS_ID.CLASE_APORTANTE
                  )
                    ? requiredFieldClasses.selectMenu
                    : null,
                  root: createdCompany
                    ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.CLASE_APORTANTE)
                      ? !editCompany
                        ? requiredFieldClasses.disabledSelectMenu
                        : null
                      : requiredFieldClasses.disabledSelectMenu
                    : null,
                },
                IconComponent: createdCompany
                  ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.CLASE_APORTANTE)
                    ? !editCompany
                      ? () => null
                      : ArrowDropDownIcon
                    : () => null
                  : ArrowDropDownIcon,
              }}
              InputProps={{
                classes: {
                  underline: companyRequiredFields.includes(
                    FIELDS_ID.CLASE_APORTANTE
                  )
                    ? requiredFieldClasses.underlineRequired
                    : classes.underline,
                },
                disableUnderline: createdCompany
                  ? ENABLED_FIELDS_TO_EDIT.includes(FIELDS_ID.CLASE_APORTANTE)
                    ? !editCompany
                    : true
                  : false,
              }}
            >
              {claseAportanteFull.map((item, index) => (
                <MenuItem
                  key={item.display}
                  value={item}
                  disabled={index === 0}
                >
                  {item.display}
                </MenuItem>
              ))}
            </TextField>
            {/* <TextField
                                className={classes.directionField}
                                helperText={HELPER_TEXT.ADICIONALES_DIRECCION}
                                value={address.informacionAdicional}
                                onChange={editCompanyFieldValue}
                                name={FIELDS_ID.ADDRESS.INFO_ADICIONAL}
                                InputProps={{
                                    classes: {
                                        underline: classes.underline
                                    },
                                    disableUnderline: createdCompany ? !editCompany : false
                                }}
                                inputProps={{
                                    className: classes.justifyTextfieldText,
                                    style: createdCompany ?
                                        !editCompany ?
                                            textfieldStyles.disabled
                                            :
                                            {}
                                        : {}
                                }}
                            /> */}
            <Typography className={classes.administradorasTitle}>
              {ADMINISTRADORAS}
            </Typography>
            <Grid container direction="row" className={classes.buttonsGrid}>
              <Button
                variant="outlined"
                className={clsx(classes.arlButton, {
                  [classes.arlButtonRequired]: companyRequiredFields.includes(
                    FIELDS_ID.ARL
                  ),
                })}
                onClick={handleSetOpenARLModal}
                disabled={createdCompany}
                classes={{
                  disabled: classes.administradoraDisabledButton,
                }}
              >
                {arl !== '' ? (
                  <img
                    src={arl.value.logo}
                    alt={arl.display}
                    style={{
                      height: 'auto',
                      width: !CUSTOM_COMPANY_ARL.includes(arl.display)
                        ? '110%'
                        : '55%',
                    }}
                  />
                ) : (
                  <Typography
                    variant="subtitle2"
                    className={clsx(classes.arlText, {
                      [classes.arlTextRequired]: companyRequiredFields.includes(
                        FIELDS_ID.ARL
                      ),
                    })}
                  >
                    {ARL}
                  </Typography>
                )}
              </Button>
              <Button
                variant="outlined"
                className={clsx(classes.ccfButton, {
                  [classes.ccfButtonRequired]: companyRequiredFields.includes(
                    FIELDS_ID.CAJA_COMPENSACION
                  ),
                })}
                onClick={handleSetOpenCCFModal}
                disabled={createdCompany}
              >
                {cajaCompensacion.value ? (
                  <img
                    src={cajaCompensacion.value.logo}
                    alt={cajaCompensacion.display}
                    style={{
                      height: 'auto',
                      width: CUSTOM_COMPANY_CCF[cajaCompensacion.display],
                    }}
                  />
                ) : (
                  <Typography
                    variant="subtitle2"
                    className={clsx(classes.ccfText, {
                      [classes.ccfTextRequired]: companyRequiredFields.includes(
                        FIELDS_ID.CAJA_COMPENSACION
                      ),
                    })}
                  >
                    {CAJA_COMPENSACION}
                  </Typography>
                )}
              </Button>
            </Grid>
            {/* <Typography
                                className={clsx(classes.arlLevel, {
                                    [classes.arlLevelRequired]: companyRequiredFields.includes(FIELDS_ID.ARL)
                                })}
                            >
                                {claseArl.display}
                            </Typography> */}
          </div>
        </div>
        {editCompany ? (
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className={classes.saveButtonsContainer}
          >
            <Button
              className={classes.cancelButton}
              onClick={cancelEditCompanyAction}
              disabled={disableCompanyButton}
            >
              <Typography
                variant="subtitle2"
                className={classes.cancelButtonText}
              >
                {CANCEL_BUTTON}
              </Typography>
            </Button>
            <Button
              variant="contained"
              className={classes.saveButton}
              onClick={saveCompanyChanges}
            >
              {disableCompanyButton ? (
                <CircularProgress className={classes.spinner} size={20} />
              ) : (
                <Typography
                  variant="subtitle2"
                  className={classes.saveButtonText}
                >
                  {SAVE_BUTTON}
                </Typography>
              )}
            </Button>
          </Grid>
        ) : null}
      </Card>
      <Dialog
        open={openARLModal}
        maxWidth="md"
        scroll="paper"
        className={classes.arlModalColor}
        fullScreen
        PaperProps={{
          className: classes.paperContainer,
        }}
        BackdropProps={{
          className: classes.arlBackdrop,
        }}
        onExited={closedArlDialog}
      >
        <Typography className={classes.selectArlTitle}>{SELECT_ARL}</Typography>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="flex-start"
          className={classes.arlsContainer}
        >
          <ButtonBase className={classes.backButton} onClick={returnArlDialog}>
            <ArrowBackRoundedIcon className={classes.backButtonIcon} />
            <Typography className={classes.backButtonText}>Volver</Typography>
          </ButtonBase>
          {ARLs.map((item) => (
            <div key={item.display} className={classes.arlCardMainContainer}>
              <ButtonBase
                className={classes.arlButtonContainer}
                onClick={selectARL.bind(this, item)}
              >
                <Paper className={classes.arlCardContainer}>
                  <div className={classes.arlCheckIconContainer}>
                    {item.display === arl.display ? (
                      <CheckCircleTwoToneIcon
                        className={classes.arlCheckedIcon}
                      />
                    ) : (
                      <RadioButtonUncheckedTwoToneIcon
                        className={classes.arlCheckRadioButton}
                      />
                    )}
                  </div>
                  {showClaseArlPicker && item.display === arl.display ? (
                    <div
                      key="arlClass"
                      className={classes.selectArlClassContainer}
                    >
                      <Select
                        value={fullClasesArl.find(
                          (clase) => clase.code === claseArl.code
                        )}
                        onChange={editCompanyFieldValue}
                        name={FIELDS_ID.CLASE_ARL}
                        className={classes.arlSelect}
                        disableUnderline
                        classes={{
                          root: classes.arlSelectColor,
                          icon: classes.arlSelectColor,
                        }}
                      >
                        {fullClasesArl.map((itemClass, index) => (
                          <MenuItem
                            key={itemClass.display}
                            value={itemClass}
                            disabled={index === 0}
                          >
                            {itemClass.display}
                          </MenuItem>
                        ))}
                      </Select>
                    </div>
                  ) : (
                    <img
                      src={item.value.logo}
                      alt={item.display}
                      className={classes.arlImage}
                      style={{
                        width: CUSTOM_DIALOG_ARL[item.display],
                      }}
                    />
                  )}
                </Paper>
              </ButtonBase>
            </div>
          ))}
        </Grid>
      </Dialog>
      <ModalCCF
        open={openCCFModal}
        onClose={returnCCFDialog}
        onSelectCCF={selectCCF}
        cajaCompensacion={cajaCompensacion}
        filterValue={province}
      />
    </div>
  );
};

CompanyCard.propTypes = {
  disableCompanyButton: PropTypes.bool,
  saveCompanyChanges: PropTypes.func,
  editCompany: PropTypes.bool,
  createdCompany: PropTypes.bool,
};

CompanyCard.defaultProps = {
  disableCompanyButton: false,
  saveCompanyChanges: () => {},
  editCompany: false,
  createdCompany: false,
};

export default CompanyCard;
