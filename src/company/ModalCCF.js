import React from 'react';
import { ButtonBase, Dialog, Paper, Typography } from '@material-ui/core';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import CheckCircleTwoToneIcon from '@material-ui/icons/CheckCircleTwoTone';
import RadioButtonUncheckedTwoToneIcon from '@material-ui/icons/RadioButtonUncheckedTwoTone';
import { CUSTOM_DIALOG_CCF, SELECT_CCF } from './Constants';
import { makeStyles } from '@material-ui/styles';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

const ModalCCF = ({open, onClose, onSelectCCF, cajaCompensacion, filterValue}) => {
    const classes = useStyles();
    const CCFs = useSelector(state => state.resources.ccf);

    const handleFilterCCF = (item) => {
        if (!filterValue.value) return;

        const ccfProvince = item.province.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
        const selectedProvince = filterValue.value.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
        if (ccfProvince === selectedProvince) {
            return item;
        }
    }
    const ccfArray = CCFs.filter(handleFilterCCF);

    return (
        <Dialog
            open={open}
            maxWidth='md'
            fullScreen={true}
            BackdropProps={{
                className: classes.ccfBackdrop
            }}
            PaperProps={{
                className: classes.ccfPaperContainer
            }}
        >
            <Typography
                className={classes.selectCCFTitle}
            >
                {SELECT_CCF}
            </Typography>
            <div
                className={classes.ccfOptionsContainer}
            >
                <ButtonBase
                    className={classes.backButton}
                    onClick={onClose}
                >
                    <ArrowBackRoundedIcon className={classes.backButtonIcon} />
                    <Typography className={classes.backButtonText}>
                        {'Volver'}
                    </Typography>
                </ButtonBase>
                {
                    ccfArray.map((item, index) => (
                        <ButtonBase
                            className={classes.ccfButtonContainer}
                            onClick={onSelectCCF.bind(this, item)}
                            key={`${item.display}_${index}`}
                        >
                            <Paper className={classes.ccfCardContainer}>
                                <div className={classes.ccfCheckIconContainer}>
                                    {
                                        item.display === cajaCompensacion.display ?
                                            <CheckCircleTwoToneIcon className={classes.ccfCheckedIcon} />
                                            :
                                            <RadioButtonUncheckedTwoToneIcon className={classes.ccfCheckRadioButton} />
                                    }
                                </div>
                                <img
                                    src={item.value.logo}
                                    alt={item.display}
                                    title={item.display}
                                    style={{
                                        width: CUSTOM_DIALOG_CCF[item.display]
                                    }}
                                />
                            </Paper>
                        </ButtonBase>
                    ))
                }
            </div>
        </Dialog>
    );
}

ModalCCF.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSelectCCF: PropTypes.func.isRequired,
    cajaCompensacion: PropTypes.object.isRequired,
    filterValue: PropTypes.object.isRequired
}

const useStyles = makeStyles((theme) => ({
    ccfBackdrop: {
        backgroundColor: 'transparent'
    },
    ccfPaperContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#2962FF',
    },
    selectCCFTitle: {
        textAlign: 'center',
        fontSize: '2em',
        color: '#FFFFFF',
        fontWeight: 'bold',
        padding: '5% 0px 1% 0px'
    },
    ccfOptionsContainer: {
        display: 'flex',
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        paddingLeft: '3%'
    },
    backButton: {
        position: 'absolute',
        top: '5%',
        left: '2%',
        display: 'flex',
        flexDirection: 'row',
        padding: '0px 8px 0px 8px',
        borderRadius: 50
    },
    backButtonIcon: {
        color: '#FFFFFF'
    },
    backButtonText: {
        color: '#FFFFFF',
        marginLeft: 5
    },
    ccfButtonContainer: {
        width: '15%',
        margin: 25
    },
    ccfCardContainer: {
        width: '100%',
        padding: '0px 8px 0px 8px',
        minHeight: 140,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center'
    },
    ccfCheckIconContainer: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: '10px 10px 0px 0px'
    },
    ccfCheckedIcon: {
        fontSize: '1.2em',
        color: '#6A32B5'
    },
    ccfCheckRadioButton: {
        fontSize: '1.2em',
        color: '#C6CCD0'
    },
}));

export default ModalCCF;
