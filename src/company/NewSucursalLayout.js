import React from 'react';
import { useSelector } from 'react-redux';
import {
    Dialog,
    Typography,
    DialogContent,
    Divider,
    Button,
    TextField,
    CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {

} from './Constants';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';

const NewSucursalLayout = (props) => {

    const classes = useStyles();
    const newSucursalModal = useSelector(state => state.company.newSucursalModal);
    const companyName = useSelector(state => state.company.companyName);

    return (
        <Dialog
            open={newSucursalModal}
            maxWidth='sm'
            PaperProps={{
                className: classes.paperContainer
            }}
        >
            <DialogContent className={classes.dialogContent}>
                <div style={{ display: 'flex', paddingLeft: '5%' }}>
                    <Typography className={classes.dialogTitle}>
                        {'Crear sucursal'}
                    </Typography>
                    <Typography style={{ alignSelf: 'flex-end', paddingBottom: 5, color: '#95989A' }}>
                        {`/${companyName}`}
                    </Typography>
                </div>
                <Divider className={classes.divider} />
                <form className={classes.novedadContainer}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Typography style={{ color: '#263238', fontWeight: 'bold', marginRight: '2%' }}>
                            Ponle un nombre a la nueva sucursal:
                        </Typography>
                        <TextField
                            variant='outlined'
                            onChange={props.getNewSucursalName}
                            style={{ width: '47%', borderColor: '#707070' }}
                            inputProps={{
                                style: {
                                    padding: '10px 10px 10px 10px'
                                }
                            }}
                            classes={{
                                root: classes.sucursalInput
                            }}
                        />
                    </div>
                    <div className={classes.buttonsContainer}>
                        <Button
                            className={classes.cancelButton}
                            onClick={props.cancelAction}
                            disabled={props.disableSucursalButton}
                        >
                            <Typography
                                variant='subtitle2'
                                className={classes.cancelButtonText}
                            >
                                {'Cancelar'}
                            </Typography>
                        </Button>
                        <Button
                            variant='contained'
                            className={classes.saveButton}
                            onClick={props.saveAction}
                            type='submit'
                            endIcon={
                                props.disableSucursalButton ? null : <ArrowForwardRoundedIcon style={{ color: '#FFFFFF' }} />
                            }
                        >
                            {
                                props.disableSucursalButton ?
                                    <CircularProgress size={20} className={classes.spinner} />
                                    :
                                    <Typography
                                        variant='subtitle2'
                                        className={classes.saveButtonText}
                                    >
                                        {'Continuar'}
                                    </Typography>
                            }
                        </Button>
                    </div>
                </form>
            </DialogContent>
        </Dialog>
    );
}

const useStyles = makeStyles({
    paperContainer: {
        backgroundColor: '#FFFFFF',
        display: 'flex',
        flex: 1,
    },
    saveButton: {
        background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
        borderRadius: 22,
        marginLeft: 10,
        textTransform: 'none',
        width: '25%'
    },
    saveButtonText: {
        color: 'white',
        fontWeight: 'bold'
    },
    cancelButtonText: {
        color: '#5E35B1',
        transform: 'none'
    },
    cancelButton: {
        marginRight: 10,
        borderRadius: 22,
        textTransform: 'none',
    },
    dialogContent: {
        paddingRight: 0,
        paddingLeft: 0
    },
    dialogTitle: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '2em',
        color: '#5E35B1'
    },
    divider: {
        margin: '15px 0px 15px 0px',
        backgroundColor: '#707070'
    },
    novedadContainer: {
        width: '100%',
        display: 'flex',
        padding: '8% 5% 0% 5%',
        flexDirection: 'column'
    },
    buttonsContainer: {
        display: 'flex',
        alignItems: 'center',
        padding: '10% 0px 5% 0px',
        justifyContent: 'flex-end'
    },
    sucursalInput: {
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#707070',
            },
            '&:hover fieldset': {
                borderColor: '#5E35B1',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#5E35B1',
            },
        },
    },
    spinner: {
        color: '#FFFFFF'
    }
});

export default NewSucursalLayout;
