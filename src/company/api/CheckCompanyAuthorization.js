import { Endpoints } from "../../api/Endpoints";

export default async (idType, idNumber) => {
    let result;
    const data = JSON.stringify({
        'idType': idType,
        'idNumber': idNumber
    });

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: data
    };

    await fetch(Endpoints.sendAuthArus, requestOptions)
        .then((response) => response.json())
        .then((apiResult) => {
            result = apiResult;
        })
        .catch((error) => {
            result = error;
            console.log('Error ejecutando fetch de verificación de nit', error);
        });

    return result;
}