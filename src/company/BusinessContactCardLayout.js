import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    Card,
    Typography,
    TextField,
    Grid,
    InputAdornment,
    // ButtonBase,
    MenuItem,
    Button,
    CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import './Styles.css';
import { COMPANY } from './../redux/ActionTypes';
import { companyAction } from './../redux/Actions';
import {
    BC_FIELDS_ID,
    BUSINESS_CONTACT,
    DOCUMENT_TYPE,
    CANCEL_BUTTON,
    SAVE_BUTTON,
    // EDIT_BUTTON
} from './Constants';
import avatar from './../assets/img/men_one.svg';
// import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import { MatchNameFields } from './../utils/MatchNameFields';
import REGEX from './../utils/RegularExpressions';
import HelperFunctions from './../utils/HelperFunctions';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const BusinessContactCardLayout = (props) => {

    const classes = useStyles();
    const disabledInputClasses = useDisabledInputStyles();
    const requiredFieldClasses = requiredFieldStyles();
    const dispatch = useDispatch();
    const name = useSelector(state => state.company.businessContact.name.display);
    const email = useSelector(state => state.company.businessContact.email);
    const identificationType = useSelector(state => state.company.businessContact.identificationType);
    const identificationNumber = useSelector(state => state.company.businessContact.identificationNumber);
    const phoneNumber = useSelector(state => state.company.businessContact.phoneNumber);
    const cellphoneNumber = useSelector(state => state.company.businessContact.cellphoneNumber);
    const editBusinessContact = props.editBusinessContact;
    const createdCompany = props.createdCompany;
    const disableInput = createdCompany ? editBusinessContact ? false : true : false;
    const mainCompany = useSelector(state => state.company.mainCompany);
    const businessContactRequiredFields = useSelector(state => state.company.businessContactRequiredFields);

    const editBusinessContactFieldValue = (event) => {

        const value = event.target.value;
        const fieldId = event.target.id;
        const fieldName = event.target.name;

        switch (fieldId || fieldName) {
            case BC_FIELDS_ID.NAME:
                if (value === '' || REGEX.PERSON_NAME.test(value)) {
                    const name_data = MatchNameFields(value);
                    const new_name = {
                        display: name_data.displayName,
                        firstName: name_data.firstName,
                        secondName: name_data.secondName,
                        firstSurname: name_data.firstSurname,
                        secondSurname: name_data.secondSurname
                    }
                    dispatch(companyAction(COMPANY.COMPANY_SAVE_BC_NAME, new_name));
                }
                if (name === '') {
                    removeRequiredField(BC_FIELDS_ID.NAME);
                }
                break;

            case BC_FIELDS_ID.EMAIL:
                dispatch(companyAction(COMPANY.COMPANY_SAVE_BC_EMAIL, value));
                if (email === '') {
                    removeRequiredField(BC_FIELDS_ID.EMAIL);
                }
                break;

            case BC_FIELDS_ID.PHONE_NUMBER:
                if (value === '' || REGEX.PHONENUMBER.test(value)) {
                    dispatch(companyAction(COMPANY.COMPANY_SAVE_BC_PHONE_NUMBER, value));
                }
                if (phoneNumber === '') {
                    removeRequiredField(BC_FIELDS_ID.PHONE_NUMBER);
                }
                break;

            case BC_FIELDS_ID.CELLPHONE_NUMBER:
                if (value === '' || REGEX.PHONENUMBER.test(value)) {
                    dispatch(companyAction(COMPANY.COMPANY_SAVE_BC_CELLPHONE_NUMBER, value));
                }
                if (cellphoneNumber === '') {
                    removeRequiredField(BC_FIELDS_ID.CELLPHONE_NUMBER);
                }
                break;

            case BC_FIELDS_ID.IDENTIFICATION_TYPE:
                dispatch(companyAction(COMPANY.COMPANY_SAVE_BC_IDENTIFICATION_TYPE, value));
                if (identificationType === '') {
                    removeRequiredField(BC_FIELDS_ID.IDENTIFICATION_TYPE);
                }
                break;

            case BC_FIELDS_ID.IDENTIFICATION_NUMBER:
                if (value === '' || REGEX.NUMBERS.test(value)) {
                    dispatch(companyAction(COMPANY.COMPANY_SAVE_BC_IDENTIFICATION_NUMBER, value));
                }
                if (identificationNumber === '') {
                    removeRequiredField(BC_FIELDS_ID.IDENTIFICATION_NUMBER);
                }
                break;

            default:
                return;
        }
    }

    // const editBusinessContactAction = () => {
    //     dispatch(companyAction(COMPANY.COMPANY_EDIT_BUSINESS_CONTACT, null));
    //     dispatch(companyAction(COMPANY.COMPANY_COPY_COMPANY_DATA, 'businessContact'));
    // }

    const cancelBusinessContactAction = () => {
        dispatch(companyAction(COMPANY.COMPANY_EDIT_BUSINESS_CONTACT, null));
        dispatch(companyAction(COMPANY.COMPANY_RESET_BACKUP_DATA, 'businessContact'));
    }

    const removeRequiredField = (field) => {
        const data = {
            field,
            status: false
        }
        dispatch(companyAction(COMPANY.COMPANY_BUSINESS_CONTACT_REQUIRED_FIELD, data));
    }

    return (
        <div className={classes.container}>
            <Card className={classes.card}>
                {
                    mainCompany ?
                        <div>
                            {
                                createdCompany ?
                                    <Grid
                                        container
                                        direction='row'
                                        justify='flex-end'
                                        alignItems='center'
                                        className={classes.editGrid}
                                    >
                                        {/* <ButtonBase
                                            className={classes.editButton}
                                            disabled={editBusinessContact}
                                            onClick={editBusinessContactAction}
                                        >
                                            <BorderColorRoundedIcon className={classes.editIcon} />
                                            <Typography
                                                variant='subtitle2'
                                                className={classes.editButtonText}
                                            >
                                                {EDIT_BUTTON}
                                            </Typography>
                                        </ButtonBase> */}
                                    </Grid>
                                    : null
                            }
                        </div>
                        : null
                }
                <div className={classes.contentContainer} style={{ pointerEvents: disableInput ? 'none' : 'auto' }}>
                    <div className={classes.firstContainer}>
                        <img src={avatar} alt='avatar' className={classes.avatar} />
                    </div>
                    <div className={classes.secondContainer}>
                        <Grid>
                            <Typography
                                variant='subtitle1'
                                align='left'
                                className={classes.cardTitle}
                            >
                                {BUSINESS_CONTACT}
                            </Typography>
                        </Grid>
                        <TextField
                            className={classes.nameField}
                            placeholder='Nombre'
                            helperText='Nombre'
                            value={name}
                            id={BC_FIELDS_ID.NAME}
                            onChange={editBusinessContactFieldValue}
                            disabled={disableInput}
                            inputProps={{
                                style: businessContactRequiredFields.includes(BC_FIELDS_ID.NAME) ?
                                    textfieldStyles.required
                                    :
                                    createdCompany ?
                                        !editBusinessContact ?
                                            textfieldStyles.disabled
                                            :
                                            {}
                                        : {}
                            }}
                            InputProps={{
                                classes: {
                                    underline: businessContactRequiredFields.includes(BC_FIELDS_ID.NAME) ? requiredFieldClasses.underlineRequired : classes.underline,
                                    disabled: disabledInputClasses.disabled
                                },
                                disableUnderline: createdCompany ? !editBusinessContact : false
                            }}
                        />
                        <TextField
                            className={classes.emailField}
                            placeholder='Correo electrónico'
                            helperText='Correo electrónico'
                            value={email}
                            id={BC_FIELDS_ID.EMAIL}
                            onChange={editBusinessContactFieldValue}
                            disabled={disableInput}
                            inputProps={{
                                style: businessContactRequiredFields.includes(BC_FIELDS_ID.EMAIL) ?
                                    textfieldStyles.required
                                    :
                                    createdCompany ?
                                        !editBusinessContact ?
                                            textfieldStyles.disabled
                                            :
                                            {}
                                        : {}
                            }}
                            InputProps={{
                                classes: {
                                    underline: businessContactRequiredFields.includes(BC_FIELDS_ID.EMAIL) ? requiredFieldClasses.underlineRequired : classes.underline,
                                    disabled: disabledInputClasses.disabled
                                },
                                disableUnderline: createdCompany ? !editBusinessContact : false
                            }}
                        />
                    </div>
                    <div className={classes.thirdContainer}>
                        <TextField
                            className={classes.identificationField}
                            placeholder='Número identificación'
                            helperText='Identificación'
                            value={
                                !createdCompany ?
                                    identificationNumber
                                    :
                                    editBusinessContact ?
                                        identificationNumber
                                        : HelperFunctions.formatStringNumber(identificationNumber)
                            }
                            onChange={editBusinessContactFieldValue}
                            id={BC_FIELDS_ID.IDENTIFICATION_NUMBER}
                            disabled={disableInput}
                            inputProps={{
                                style: businessContactRequiredFields.includes(BC_FIELDS_ID.IDENTIFICATION_NUMBER) ?
                                    textfieldStyles.required
                                    :
                                    {}
                            }}
                            InputProps={{
                                classes: {
                                    disabled: disabledInputClasses.disabled,
                                    underline: businessContactRequiredFields.includes(BC_FIELDS_ID.IDENTIFICATION_NUMBER) ? requiredFieldClasses.underlineRequired : classes.underline
                                },
                                style: createdCompany ?
                                    !editBusinessContact ?
                                        { ...textfieldStyles.disabled, ...textfieldStyles.disabledDocumentInput }
                                        :
                                        {}
                                    : {},
                                startAdornment:
                                    <InputAdornment position='start'>
                                        <TextField
                                            select={true}
                                            value={DOCUMENT_TYPE.find(id => id.code === identificationType.code)}
                                            onChange={editBusinessContactFieldValue}
                                            name={BC_FIELDS_ID.IDENTIFICATION_TYPE}
                                            disabled={disableInput}
                                            InputProps={{
                                                disableUnderline: true,
                                                classes: {
                                                    disabled: disabledInputClasses.disabled
                                                }
                                            }}
                                            SelectProps={{
                                                classes: {
                                                    selectMenu: businessContactRequiredFields.includes(BC_FIELDS_ID.IDENTIFICATION_TYPE) ? requiredFieldClasses.selectMenu : null
                                                },
                                                IconComponent: createdCompany ? !editBusinessContact ? (props) => null : ArrowDropDownIcon : ArrowDropDownIcon
                                            }}
                                        >
                                            {
                                                DOCUMENT_TYPE.map((item, index) => (
                                                    <MenuItem key={index} value={item}>
                                                        {item.display}
                                                    </MenuItem>
                                                ))
                                            }
                                        </TextField>
                                    </InputAdornment>,
                                disableUnderline: createdCompany ? !editBusinessContact : false
                            }}
                        />
                        <Grid
                            className={classes.phoneGrid}
                            container
                            direction='row'
                            justify='space-between'
                            alignItems='center'
                        >
                            <TextField
                                className={classes.phoneNumberField}
                                placeholder='Teléfono de contacto'
                                helperText='Teléfono de contacto'
                                value={phoneNumber}
                                onChange={editBusinessContactFieldValue}
                                id={BC_FIELDS_ID.PHONE_NUMBER}
                                disabled={disableInput}
                                InputProps={{
                                    classes: {
                                        underline: businessContactRequiredFields.includes(BC_FIELDS_ID.PHONE_NUMBER) ? requiredFieldClasses.underlineRequired : classes.underline,
                                        disabled: disabledInputClasses.disabled
                                    },
                                    disableUnderline: createdCompany ? !editBusinessContact : false
                                }}
                                inputProps={{
                                    style: businessContactRequiredFields.includes(BC_FIELDS_ID.PHONE_NUMBER) ?
                                        textfieldStyles.required
                                        :
                                        createdCompany ?
                                            !editBusinessContact ?
                                                textfieldStyles.disabled
                                                :
                                                {}
                                            : {}
                                }}
                            />
                            {/* <TextField
                                className={classes.cellphoneNumberField}
                                placeholder='Celular'
                                helperText='Celular'
                                value={cellphoneNumber}
                                onChange={editBusinessContactFieldValue}
                                id={BC_FIELDS_ID.CELLPHONE_NUMBER}
                                disabled={disableInput}
                                InputProps={{
                                    classes: {
                                        underline: businessContactRequiredFields.includes(BC_FIELDS_ID.CELLPHONE_NUMBER) ? requiredFieldClasses.underlineRequired : classes.underline,
                                        disabled: disabledInputClasses.disabled
                                    },
                                    disableUnderline: createdCompany ? !editBusinessContact : false
                                }}
                                inputProps={{
                                    style: businessContactRequiredFields.includes(BC_FIELDS_ID.CELLPHONE_NUMBER) ?
                                        textfieldStyles.required
                                        :
                                        createdCompany ?
                                            !editBusinessContact ?
                                                textfieldStyles.disabled
                                                :
                                                {}
                                            : {}
                                }}
                            /> */}
                        </Grid>
                    </div>
                </div>
                {
                    editBusinessContact ?
                        <Grid
                            container
                            direction='row'
                            justify='center'
                            alignItems='center'
                            className={classes.saveButtonsContainer}
                        >
                            <Button
                                className={classes.cancelButton}
                                onClick={cancelBusinessContactAction}
                                disabled={props.disableBCButton}
                            >
                                <Typography
                                    variant='subtitle2'
                                    className={classes.cancelButtonText}
                                >
                                    {CANCEL_BUTTON}
                                </Typography>
                            </Button>
                            <Button
                                variant='contained'
                                className={classes.saveButton}
                                onClick={props.saveBusinessContactChanges}
                            >
                                {
                                    props.disableBCButton ?
                                        <CircularProgress className={classes.spinner} size={20} />
                                        :
                                        <Typography
                                            variant='subtitle2'
                                            className={classes.saveButtonText}
                                        >
                                            {SAVE_BUTTON}
                                        </Typography>
                                }
                            </Button>
                        </Grid>
                        : null
                }
            </Card>
        </div>
    );
}

const useDisabledInputStyles = makeStyles({
    disabled: {
        '&.Mui-disabled:before': {
            borderBottomStyle: 'solid'
        },
        '&.MuiInputBase-input': {
            color: '#545454'
        }
    }
});

const requiredFieldStyles = makeStyles({
    underlineRequired: {
        '&.MuiInput-underline:after': {
            borderBottom: '2px solid #FF495A'
        },
        '&.MuiInput-underline:before': {
            borderBottom: '1px solid #FF495A'
        },
        '&.MuiInput-underline:hover:before': {
            borderBottom: '2px solid #FF495A'
        }
    },
    inputRoot: {
        '&.MuiAutocomplete-inputRoot': {
            backgroundColor: '#FF495A26'
        }
    },
    selectMenu: {
        '&.MuiSelect-selectMenu': {
            backgroundColor: '#FF495A26'
        }
    }
});

const textfieldStyles = {
    required: {
        backgroundColor: '#FF495A26'
    },
    disabled: {
        backgroundColor: '#ECEFF1',
        borderRadius: 7,
        padding: '5px 10px',
        height: 22
    },
    disabledDocumentInput: {
        height: 32
    }
}

const useStyles = makeStyles(({
    container: {
        width: '90%',
        marginTop: 50
    },
    contentContainer: {
        display: 'flex'
    },
    card: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        paddingTop: 30,
        paddingBottom: 30,
        boxShadow: '0px 3px 16px #00000029',
        borderRadius: 7
    },
    firstContainer: {
        width: '15%',
        height: '100%',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    avatar: {
        width: 110,
        height: 110,
        marginTop: 30,
        marginBottom: 20
    },
    secondContainer: {
        width: '55%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        paddingRight: 30
    },
    nameField: {
        margin: '10px 0px 10px 0px'
    },
    emailField: {
        margin: '10px 0px 10px 0px'
    },
    fieldsGrid: {
        margin: '10px 0px 10px 0px'
    },
    fieldGrid: {
        width: '45%'
    },
    thirdContainer: {
        width: '30%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        paddingTop: 30
    },
    identificationField: {
        width: '85%',
        marginTop: 38
    },
    phoneNumberField: {
        width: '100%'
    },
    cellphoneNumberField: {
        width: '45%'
    },
    phoneGrid: {
        marginTop: 20,
        width: '85%'
    },
    editGrid: {
        paddingRight: 30,
        paddingTop: 10,
        paddingBottom: 10
    },
    editButton: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    editIcon: {
        width: 17,
        height: 17,
        marginRight: 5,
        color: '#6A32B5'
    },
    saveButtonsContainer: {
        paddingTop: 30
    },
    saveButton: {
        background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
        borderRadius: 100,
        marginLeft: 10,
        width: '10%',
        textTransform: 'none'
    },
    saveButtonText: {
        color: 'white',
        fontWeight: 'bold'
    },
    cancelButtonText: {
        color: '#6A32B5'
    },
    cancelButton: {
        marginRight: 10,
        borderRadius: 100,
        textTransform: 'none'
    },
    editButtonText: {
        color: '#6A32B5',
        fontWeight: 'bold'
    },
    cardTitle: {
        color: '#263238',
        fontWeight: 'bold',
        fontSize: '1.4rem',
        marginBottom: 20
    },
    spinner: {
        color: '#FFFFFF'
    },
    underline: {
        '&.MuiInput-underline:after': {
            borderBottom: '2px solid #6A32B5'
        }
    }
}));

export default BusinessContactCardLayout;