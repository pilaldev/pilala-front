import React, { useEffect, useState } from 'react';
import { Card, Link, Typography } from '@material-ui/core';
import Button from '@bit/pilala.pilalalib.components.button';
import TextInput from '@bit/pilala.pilalalib.components.textinput';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import FaceIcon from '@material-ui/icons/Face';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import zxcvbn from 'zxcvbn';
import moment from 'moment';
import {
  companyAction,
  currentUserAction,
  navigationAction,
  resourcesAction,
  signUpAction,
  snackBarAction,
} from '../redux/Actions';
import {
  COMPANY,
  CURRENT_USER,
  NAVIGATION,
  RESOURCES,
  SIGN_UP,
  SNACKBAR,
} from '../redux/ActionTypes';
import { CreateUserAccount, SendUserData } from './api';
import Firebase, { FIREBASE_AUTH } from '../api/Firebase';
import { MatchNameFields } from '../utils/MatchNameFields';
import HelperFunctions from '../utils/HelperFunctions';
import { getResources } from '../api/GetResources';
import BlockScreen from '../components/BlockScreen';
import PasswordValidator from './PasswordValidator';
import { useCreatePasswordStyles } from './styles';
import { TERMS_LINK } from './Constants';

const CreatePassword = () => {
  const classes = useCreatePasswordStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const showPassword = useSelector((state) => state.signUp.showPassword);
  const [passwordValidation, setPasswordValidation] = useState({
    score: 0,
    error: false,
  });
  const userPassword = useSelector((state) => state.signUp.userPassword);
  const email = useSelector((state) => state.signUp.email);
  const userName = useSelector((state) => state.signUp.name);
  const documentType = useSelector((state) => state.signUp.documentType);
  const documentNumber = useSelector((state) => state.signUp.documentNumber);
  const phoneNumber = useSelector((state) => state.signUp.phoneNumber);
  const [inputError, setInputError] = useState({
    passwordInput: false,
    message: '',
  });
  const [termsCheck, setTermsCheck] = useState(false);
  const [disableCreateAccountButton, setDisableCreateAccountButton] = useState(
    false
  );

  useEffect(() => {
    ChangePageTitle('Crear contraseña | Pilalá');
    const saveIncomingParams = (params) => {
      const name = params.UN ? params.UN : '';
      const urlPhoneNumber = params.PN ? params.PN : '';
      const urlEmail = params.MA ? params.MA : '';
      const urlDocumentNumber = params.IN ? params.IN : '';
      const urlDocumentType = params.IT ? params.IT : 'CC';
      const urlNit = params.NI ? params.NI : '';

      dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_FULL_NAME, name));
      dispatch(
        signUpAction(SIGN_UP.SIGN_UP_GET_USER_PHONE_NUMBER, urlPhoneNumber)
      );
      dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_EMAIL, urlEmail));
      dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_ID, urlDocumentNumber));
      dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_TYPE_USER_ID, urlDocumentType));
      dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_COMPANY_NIT, urlNit));
    };

    const getPayloadParams = () => {
      const params = {};
      window.location.href.replace(
        /[?&]+([^=&]+)=([^&]*)/gi,
        (m, key, value) => {
          params[key] = value.split('_').join(' ');
        }
      );
      saveIncomingParams(params);
    };

    getPayloadParams();
  }, [dispatch]);

  useEffect(() => {
    dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, true));
    const downloadResources = () => {
      getResources().then((result) => {
        dispatch(
          resourcesAction(RESOURCES.RESOURCES_SAVE_LOCATION_DATA, {
            cities: HelperFunctions.formatCitiesArray(result.cities),
            provinces: HelperFunctions.formatProvincesArray(result.provinces),
            ciiu: HelperFunctions.formatCIIUArray(result.codeList),
            arl: HelperFunctions.formatARLArray(result.arl),
            identificacionAportante: result.identificacionAportante,
            camaras: result.camaras,
          })
        );
        dispatch(navigationAction(NAVIGATION.NAVIGATION_BLOCK_SCREEN, false));
      });
    };
    downloadResources();
  }, [dispatch]);

  const handleShowPassword = () => {
    dispatch(signUpAction(SIGN_UP.SIGN_UP_SHOW_PASSWORD_CONTENT, null));
  };

  const getUserPassword = (event) => {
    const value = event.target.value.trim();
    const { score } = zxcvbn(value);
    dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_PASSWORD, value));
    setPasswordValidation((prevState) => ({
      ...prevState,
      score,
    }));
  };

  const handleOnFocus = () => {
    if (inputError.passwordInput) {
      setInputError((prevState) => ({
        ...prevState,
        passwordInput: false,
      }));
    }
  };

  const handleCheckboxTerms = () => {
    setTermsCheck((prevState) => !prevState);
  };

  const checkSignUpFields = () => {
    if (userPassword === '') {
      setInputError({
        passwordInput: true,
        message: 'Este campo es obligatorio',
      });
      return false;
    }

    if (userPassword.length < 8) {
      setInputError({
        passwordInput: true,
        message: 'La contraseña debe tener mínimo 8 caracteres',
      });
      return false;
    }

    if (passwordValidation.score < 2) {
      setInputError({
        passwordInput: true,
        message: '',
      });
      setPasswordValidation((prevState) => ({
        ...prevState,
        error: true,
      }));
      return false;
    }

    return true;
  };

  const saveCurrentUserData = (data) => {
    const nameData = MatchNameFields(userName);
    const newName = {
      display: nameData.displayName,
      firstName: nameData.firstName,
      secondName: nameData.secondName,
      firstSurname: nameData.firstSurname,
      secondSurname: nameData.secondSurname,
    };
    const businessContactData = {
      name: newName,
      email,
      province: '',
      city: '',
      identificationType: { display: documentType, code: documentType },
      identificationNumber: documentNumber,
      phoneNumber: '',
      cellphoneNumber: phoneNumber,
    };
    dispatch(
      currentUserAction(CURRENT_USER.CURRENT_USER_SAVE_USER_NAME, userName)
    );
    dispatch(
      currentUserAction(
        CURRENT_USER.CURRENT_USER_SAVE_USER_PHONE_NUMBER,
        phoneNumber
      )
    );
    dispatch(
      currentUserAction(CURRENT_USER.CURRENT_USER_SAVE_USER_EMAIL, email)
    );
    dispatch(
      currentUserAction(CURRENT_USER.CURRENT_USER_SAVE_USER_ID, documentNumber)
    );
    dispatch(
      currentUserAction(
        CURRENT_USER.CURRENT_USER_SAVE_TYPE_USER_ID,
        documentType
      )
    );
    dispatch(
      currentUserAction(
        CURRENT_USER.CURRENT_USER_SAVE_FIRST_SIGNIN,
        data.firstSignIn
      )
    );
    dispatch(
      currentUserAction(CURRENT_USER.CURRENT_USER_SAVE_USER_UID, data.uid)
    );
    dispatch(
      currentUserAction(
        CURRENT_USER.CURRENT_USER_SAVE_CREATED_COMPANY,
        data.createdCompany
      )
    );
    dispatch(
      companyAction(COMPANY.COMPANY_SAVE_BC_ALL_DATA, businessContactData)
    );
  };

  const createUserAccount = (e) => {
    e.preventDefault();
    if (disableCreateAccountButton) return;
    setDisableCreateAccountButton(true);

    if (checkSignUpFields()) {
      if (termsCheck) {
        CreateUserAccount(email, userPassword).then((accountResult) => {
          if (accountResult.status === 'SUCCESS') {
            const diaActual = 1;
            const periodoCotizacionActual = {
              display: `${moment({})
                .date(diaActual)
                .subtract(1, 'months')
                .format('DD-MM-YYYY')}`,
              value: `${moment({})
                .date(diaActual)
                .subtract(1, 'months')
                .format('DD_MM_YYYY')}`,
              month: `${moment()
                .date(diaActual)
                .subtract(1, 'months')
                .format('MMMM_YYYY')}`,
              monthDate: moment({})
                .date(diaActual)
                .subtract(1, 'months')
                .format('M'),
            };
            const data = {
              userData: {
                firstSignIn: true,
                uid: accountResult.userCredential.user.uid,
                createdCompany: false,
                pilalaTerms: termsCheck,
                arusTerms: termsCheck,
                periodoCotizacionActual,
              },
              userEmail: email,
              dateRegistered: Firebase.firestore.Timestamp.fromMillis(
                new Date().getTime()
              ),
            };
            SendUserData(data).then(async (dataResult) => {
              if (dataResult.status === 'SUCCESS') {
                const nameData = MatchNameFields(userName);
                await FIREBASE_AUTH.currentUser.updateProfile({
                  displayName: nameData.firstName,
                });
                saveCurrentUserData(data.userData);
                setDisableCreateAccountButton(false);
                localStorage.setItem('loggedIn', JSON.stringify(true));
                localStorage.setItem(
                  'loggedUid',
                  JSON.stringify(accountResult.userCredential.user.uid)
                );
                history.replace(
                  `/${accountResult.userCredential.user.uid}/fetchData`
                );
              } else {
                dispatch(
                  snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                    title: 'Error inesperado',
                    description:
                      'Hemos tenido problemas para completar esta acción, inténtelo nuevamente',
                    color: 'error',
                  })
                );
                setDisableCreateAccountButton(false);
              }
            });
          } else if (accountResult.status === 'FAILED') {
            dispatch(
              snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: 'Error al crear la cuenta',
                description: accountResult.message,
                color: 'error',
              })
            );
            setDisableCreateAccountButton(false);
          }
        });
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Información requerida',
            description:
              'Para crear su cuenta debe aceptar los términos y condiciones',
            color: 'warning',
          })
        );
        setDisableCreateAccountButton(false);
      }
    } else {
      setDisableCreateAccountButton(false);
    }
  };

  return (
    <div className={classes.container}>
      <Card className={classes.card}>
        <Typography className={classes.title} variant="h4">
          Crea tu contraseña
        </Typography>
        <form className={classes.form}>
          <TextInput
            fullWidth
            label="Correo electrónico"
            value={email}
            startIcon={<FaceIcon />}
            className={classes.emailInput}
            disabled
            displayOnly
            InputProps={{
              autoComplete: 'email',
            }}
          />
          <TextInput
            fullWidth
            label="Contraseña"
            type={showPassword ? 'text' : 'password'}
            placeholder="Contraseña"
            onChange={getUserPassword}
            value={userPassword}
            error={inputError.passwordInput}
            onFocus={handleOnFocus}
            id="userPassword"
            helperText={inputError.message}
            startIcon={<VpnKeyIcon />}
            endIcon={
              showPassword ? (
                <IconButton
                  onClick={handleShowPassword}
                  size="small"
                  buttoncolor="grey"
                  filled={false}
                >
                  <VisibilityIcon />
                </IconButton>
              ) : (
                <IconButton
                  onClick={handleShowPassword}
                  size="small"
                  buttoncolor="grey"
                  filled={false}
                >
                  <VisibilityOffIcon />
                </IconButton>
              )
            }
            InputProps={{
              autoComplete: 'new-password',
            }}
          />
          <div className={classes.validatorContainer}>
            <PasswordValidator
              score={passwordValidation.score}
              passwordStrengthError={passwordValidation.error}
            />
          </div>
          <div className={classes.termsContainer}>
            <IconButton
              onClick={handleCheckboxTerms}
              size="small"
              filled={false}
            >
              {termsCheck ? (
                <CheckBoxOutlinedIcon className={classes.termsCheckboxIcon} />
              ) : (
                <CheckBoxOutlineBlankIcon className={classes.termsCheckbox} />
              )}
            </IconButton>
            <Typography className={classes.termsText} variant="subtitle2">
              <>
                {'Aceptar'}{' '}
                <b>
                  <Link href={TERMS_LINK} target="_blank" rel="noopener">
                    términos y condiciones de uso.
                  </Link>
                </b>
              </>
            </Typography>
          </div>
          <div className={classes.buttonsContainer}>
            <Button
              onClick={createUserAccount}
              loading={disableCreateAccountButton}
              type="submit"
            >
              Siguiente
            </Button>
          </div>
        </form>
      </Card>
      <BlockScreen />
    </div>
  );
};

export default CreatePassword;
