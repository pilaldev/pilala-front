import { makeStyles } from '@material-ui/core/styles';

const useSignUpStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    height: 'auto',
    width: '100%',
    overflow: 'auto',
    backgroundColor: theme.palette.common.white,
    [theme.breakpoints.up('sm')]: {
      height: '100vh',
    },
  },
  backgroundContainer: {
    display: 'none',
    width: '100%',
    height: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: theme.spacing(15),
    [theme.breakpoints.up('lg')]: {
      display: 'flex',
    },
  },
  backgroundPymes: {
    background:
      'radial-gradient(circle, rgba(143,39,220,1) 0%, rgba(53,26,124,1) 33%, rgba(42,27,89,1) 66%, rgba(31,28,53,1) 100%)',
  },
  backgroundIndependientes: {
    background:
      'radial-gradient(circle, rgba(6,220,251,1) 0%, rgba(0,128,181,1) 50%, rgba(17,68,131,1) 100%)',
  },
  contentPymes: {
    backgroundColor: theme.palette.primary[200],
  },
  contentIndependientes: {
    backgroundColor: theme.palette.warning[200],
  },
  logo: {
    minWidth: '10rem',
    height: 'auto',
    width: '10rem',
    [theme.breakpoints.up('sm')]: {
      width: '13rem',
      minWidth: '13rem',
    },
  },
  formContainer: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: theme.spacing(5),
  },
  contentContainerEmpresas: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    padding: theme.spacing(4),
    backgroundColor: theme.palette.common.white,
  },
  formRowOne: {
    display: 'flex',
  },
  idTypeContainer: {
    display: 'flex',
    flex: 0.4,
    marginRight: theme.spacing(3),
    [theme.breakpoints.down('xs')]: {
      marginRight: 0,
    },
  },
  idNumberContainer: {
    display: 'flex',
    flex: 1,
    [theme.breakpoints.down('xs')]: {
      marginTop: '1rem',
    },
  },
  formRowTwo: {
    display: 'flex',
    marginTop: '1rem',
    flexDirection: 'column',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      alignItems: 'flex-end',
      marginTop: '1.5rem',
    },
  },
  genreContainer: {
    display: 'flex',
    flex: 1,
    marginRight: theme.spacing(1.5),
    [theme.breakpoints.down('xs')]: {
      marginRight: 0,
    },
  },
  phoneContainer: {
    display: 'flex',
    flex: 1,
    marginLeft: theme.spacing(1.5),
    [theme.breakpoints.down('xs')]: {
      marginLeft: 0,
      marginTop: '1rem',
    },
  },
  provinceContainer: {
    display: 'flex',
    flex: 1,
    marginRight: theme.spacing(1.5),
    [theme.breakpoints.down('xs')]: {
      marginRight: 0,
    },
  },
  cityContainer: {
    display: 'flex',
    flex: 1,
    marginLeft: theme.spacing(1.5),
    [theme.breakpoints.down('xs')]: {
      marginLeft: 0,
      marginTop: '1rem',
    },
  },
  formTitle: {
    fontWeight: 700,
    marginBottom: theme.spacing(4),
  },
  buttonContainer: {
    display: 'flex',
    alignItems: 'center',
    marginTop: '1.8rem',
    justifyContent: 'flex-end',
  },
  contentContainerIndependientes: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    padding: theme.spacing(4),
    backgroundColor: theme.palette.common.white,
  },
  contentContainer: {
    display: 'flex',
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    padding: theme.spacing(5, 3),
    [theme.breakpoints.up('xs')]: {
      padding: theme.spacing(5, 3),
    },
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(5, 8),
    },
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(5, 25),
    },
    [theme.breakpoints.up('lg')]: {
      width: '30%',
      padding: theme.spacing(5),
    },
    [theme.breakpoints.up('xl')]: {
      padding: theme.spacing(5, 7),
    },
  },
  card: {
    backgroundColor: theme.palette.common.white,
    borderRadius: '0.625rem',
    boxShadow: theme.elevations[2],
  },
  Card__Tabs: {
    height: '3.5rem',
  },
  Card__Tab: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    height: 'inherit',
    padding: theme.spacing(0, 2),
  },
  Card__Tab__Label: {
    marginLeft: theme.spacing(2),
  },
}));

export default useSignUpStyles;
