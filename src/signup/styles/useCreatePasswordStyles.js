import { makeStyles } from '@material-ui/core/styles';

const useCreatePasswordStyles = makeStyles((theme) => ({
  container: {
    background:
      'radial-gradient(circle, rgba(53,26,124,1) 0%, rgba(42,27,89,1) 33%, rgba(31,28,53,1) 66%, rgba(29,14,51,1) 100%)',
    display: 'flex',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    display: 'flex',
    borderRadius: 10,
    boxShadow: 'none',
    width: '100%',
    padding: theme.spacing(4, 6),
    flexDirection: 'column',
    minWidth: '90%',
    maxWidth: '90%',
    [theme.breakpoints.up('sm')]: {
      minWidth: '450px',
      maxWidth: '450px',
    },
  },
  title: {
    fontWeight: 'bold',
    color: theme.palette.common.black,
    textAlign: 'left',
    marginBottom: theme.spacing(3),
  },
  stepperContainer: {
    width: '30%',
    margin: theme.spacing(5, 0),
  },
  form: {},
  emailInput: {
    marginBottom: theme.spacing(3),
  },
  termsContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginTop: theme.spacing(6),
  },
  termsCheckboxIcon: {
    color: '#6A32B5',
    fontSize: '1.2rem',
    [theme.breakpoints.up('sm')]: {
      fontSize: '1.4rem',
    },
  },
  termsCheckbox: {
    color: '#6A32B5',
    fontSize: '1.2rem',
    [theme.breakpoints.up('sm')]: {
      fontSize: '1.4rem',
    },
  },
  termsText: {
    color: theme.palette.primary.main,
    textAlign: 'left',
    marginLeft: theme.spacing(1),
  },
  termsLink: {
    color: theme.palette.primary.main,
    cursor: 'pointer',
  },
  buttonsContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(3),
  },
  validatorContainer: {
    marginTop: theme.spacing(1),
  },
}));

export default useCreatePasswordStyles;
