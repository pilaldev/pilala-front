/* Titles, subtitles, etc. */
export const MAIN_TITLE = '¡YA ESTÁS A UN PASO!';
export const MAIN_SUBTITLE = 'Hemos adelantado algunos datos';
export const TERMS_LINK = 'https://www.pilala.co/terminos-y-condiciones';
export const WELCOME_TITLE =
  'Crea tu contraseña y empieza a disfrutar de la mejor experiencia pagando la seguridad social';

/* Buttons */
export const SIGN_UP_BUTTON = 'CONTINUAR';

/* Fields */
export const FIELDS = {
  NAME: 'name',
  DOCUMENT_TYPE: 'documentType',
  DOCUMENT_NUMBER: 'documentNumber',
  GENRE: 'genre',
  PHONE_NUMBER: 'phoneNumber',
  PROVINCE: 'province',
  CITY: 'city',
  EMAIL: 'email',
};

/* Arrays, Objects, etc. */
export const ID_VALUES = [
  {
    value: 'CC',
    display: 'CC',
  },
  {
    value: 'CE',
    display: 'CE',
  },
];

/* Status variables */
export const SUCCESS = 'SUCCESS';
export const FAILED = 'FAILED';

/* MESSAGES */
export const EMAIL_ALREADY_IN_USE =
  'Ya existe una cuenta con este correo electrónico';
export const INVALID_EMAIL =
  'Correo electrónico inválido, por favor verifiquelo';
export const WEAK_PASSWORD =
  'La contraseña es muy débil, por favor utilice una más fuerte';
export const DEFAULT_MESSAGE = 'Ocurrió algo inesperado :(';

/* API */
export const CREATE_USER_URL =
  'https://us-central1-pilala-develop.cloudfunctions.net/sendUserData';

export const GENRE_VALUES = [
  {
    value: 0,
    display: 'Masculino',
  },
  {
    value: 1,
    display: 'Femenino',
  },
];
