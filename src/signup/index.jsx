import React, { useEffect, useLayoutEffect, useState } from 'react';
import { Card, Grid, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@bit/pilala.pilalalib.components.button';
import TextInput from '@bit/pilala.pilalalib.components.textinput';
import { AlexFace, LogoPilala } from '@bit/pilala.pilalalib.assets';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ID_VALUES, GENRE_VALUES, FIELDS } from './Constants';
import { DropdownInput, Select } from '../components';
import alex from '../assets/img/alex.png';
import camilo from '../assets/img/camilo.png';
import RegularExpressions from '../utils/RegularExpressions';
import {
  resourcesAction,
  signUpAction,
  snackBarAction,
} from '../redux/Actions';
import { RESOURCES, SIGN_UP, SNACKBAR } from '../redux/ActionTypes';
import { getResources } from '../api/GetResources';
import HelperFunctions from '../utils/HelperFunctions';
import { WriteUserData } from './api';
import { useSignUpStyles } from './styles';

const SignUp = () => {
  const classes = useSignUpStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const currentForm = useSelector((state) => state.signUp.currentForm);
  const provinces = useSelector((state) => state.resources.provinces);
  const cities = useSelector((state) => state.resources.cities);
  const name = useSelector((state) => state.signUp.name);
  const documentType = useSelector((state) => state.signUp.documentType);
  const documentNumber = useSelector((state) => state.signUp.documentNumber);
  const genre = useSelector((state) => state.signUp.genre);
  const phoneNumber = useSelector((state) => state.signUp.phoneNumber);
  const province = useSelector((state) => state.signUp.province);
  const city = useSelector((state) => state.signUp.city);
  const email = useSelector((state) => state.signUp.email);
  const [requiredFields, setRequiredFields] = useState({});
  const [disableSendButton, setDisableSendButton] = useState(false);
  const loggedUid = JSON.parse(localStorage.getItem('loggedUid'));
  const loggedIn = JSON.parse(localStorage.getItem('loggedIn'));

  useLayoutEffect(() => {
    if (loggedIn) {
      history.replace(`/${loggedUid}/fetchData`);
    }
  }, [history, loggedIn, loggedUid]);

  useEffect(() => {
    ChangePageTitle('Regístrate | Pilalá');
    const downloadResources = () => {
      getResources().then((result) => {
        dispatch(
          resourcesAction(RESOURCES.RESOURCES_SAVE_LOCATION_DATA, {
            cities: HelperFunctions.formatCitiesArray(result.cities),
            provinces: HelperFunctions.formatProvincesArray(result.provinces),
          })
        );
      });
    };
    downloadResources();
  }, [dispatch]);

  useEffect(() => {
    if (currentForm) {
      setRequiredFields({});
    }
  }, [currentForm]);

  const checkRequiredFields = () => {
    let fields = { ...requiredFields };
    if (name === '') {
      if (!requiredFields[FIELDS.NAME]) {
        fields = {
          ...fields,
          [FIELDS.NAME]: true,
        };
      }
    }
    if (documentNumber === '') {
      if (!requiredFields[FIELDS.DOCUMENT_NUMBER]) {
        fields = {
          ...fields,
          [FIELDS.DOCUMENT_NUMBER]: true,
        };
      }
    }
    if (phoneNumber === '') {
      if (!requiredFields[FIELDS.PHONE_NUMBER]) {
        fields = {
          ...fields,
          [FIELDS.PHONE_NUMBER]: true,
        };
      }
    }
    if (province.value === '') {
      if (!requiredFields[FIELDS.PROVINCE]) {
        fields = {
          ...fields,
          [FIELDS.PROVINCE]: true,
        };
      }
    }
    if (city.value === '') {
      if (!requiredFields[FIELDS.CITY]) {
        fields = {
          ...fields,
          [FIELDS.CITY]: true,
        };
      }
    }
    if (email === '') {
      if (!requiredFields[FIELDS.EMAIL]) {
        fields = {
          ...fields,
          [FIELDS.EMAIL]: true,
        };
      }
    }
    setRequiredFields(fields);
    return Object.keys(fields).length === 0;
  };

  const sendUserData = async (e) => {
    e.preventDefault();
    if (disableSendButton) return;
    setDisableSendButton(true);

    if (checkRequiredFields()) {
      const result = await WriteUserData({
        email,
        documentType,
        documentNumber,
        genre,
        phoneNumber,
        location: {
          province,
          city,
        },
        name,
      });

      if (result.status === 'SUCCESS' && !result.data.existingCheck) {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Registro exitoso',
            description:
              'Te hemos enviado un correo electrónico para terminar tu registro',
            color: 'success',
          })
        );
        setDisableSendButton(false);
        dispatch(signUpAction(SIGN_UP.SIGN_UP_RESET_FIELDS));
      } else if (result.status === 'SUCCESS' && result.data.existingCheck) {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al crear cuenta',
            description:
              'Parece que ya existe una cuenta de Pilalá asociada a este correo electrónico',
            color: 'error',
          })
        );
        setDisableSendButton(false);
      }
    } else {
      setDisableSendButton(false);
    }
  };

  const getOptionLabel = (item) => item.display;

  const filterCitiesList = (currentOptions, optionsState) => {
    if (optionsState.inputValue === '') {
      return cities.filter((item) => item.provinceCode === province.code);
    }
    return currentOptions.filter(
      (item) =>
        item.provinceCode === province.code &&
        item.value.indexOf(optionsState.inputValue.toLowerCase()) > -1
    );
  };

  const getFieldValue = (event) => {
    const { value } = event.target;
    const fieldId = event.target.id;
    const fieldName = event.target.name;

    switch (fieldId || fieldName) {
      case FIELDS.NAME:
        if (value === '' || RegularExpressions.NAMES.test(value)) {
          dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_FULL_NAME, value));
        }
        break;

      case FIELDS.DOCUMENT_TYPE:
        dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_TYPE_USER_ID, value.value));
        break;

      case FIELDS.DOCUMENT_NUMBER:
        if (value === '' || RegularExpressions.NUMBERS.test(value)) {
          dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_ID, value));
        }
        break;

      case FIELDS.GENRE:
        dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_GENRE, value));
        break;

      case FIELDS.PHONE_NUMBER:
        if (value === '' || RegularExpressions.NUMBERS.test(value)) {
          dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_PHONE_NUMBER, value));
        }
        break;

      case FIELDS.EMAIL:
        dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_EMAIL, value));
        break;

      default:
        break;
    }
    if (requiredFields[fieldId] || requiredFields[fieldName]) {
      const fieldsCopy = JSON.parse(JSON.stringify(requiredFields));
      delete fieldsCopy[fieldId || fieldName];
      setRequiredFields(fieldsCopy);
    }
  };

  const getListValue = (event, value) => {
    const fieldId = event.target.id.split('-')[0];

    switch (fieldId) {
      case FIELDS.PROVINCE:
        dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_PROVINCE, value));
        break;

      case FIELDS.CITY:
        dispatch(signUpAction(SIGN_UP.SIGN_UP_GET_USER_CITY, value));
        break;

      default:
        return;
    }
    if (requiredFields[fieldId]) {
      const fieldsCopy = JSON.parse(JSON.stringify(requiredFields));
      delete fieldsCopy[fieldId];
      setRequiredFields(fieldsCopy);
    }
  };

  return (
    <div className={classes.container}>
      <div
        className={clsx(classes.contentContainer, {
          [classes.contentPymes]: currentForm === 'empresas',
          [classes.contentIndependientes]: currentForm === 'independientes',
        })}
      >
        <img src={LogoPilala} alt="Logo Pilalá" className={classes.logo} />
        <form>
          <div className={classes.formContainer}>
            <Typography variant="h5" className={classes.formTitle}>
              ¡Crea tu cuenta!
            </Typography>
            <Card className={classes.card}>
              <Grid
                container
                alignItems="center"
                className={classes.Card__Tabs}
              >
                <Grid
                  item
                  container
                  alignItems="center"
                  className={classes.Card__Tab}
                >
                  <img src={AlexFace} width={40} height={40} alt="Alex face" />
                  <Typography
                    variant="subtitle1"
                    noWrap
                    className={classes.Card__Tab__Label}
                  >
                    Empresas
                  </Typography>
                </Grid>
              </Grid>
              {currentForm === 'empresas' ? (
                <div className={classes.contentContainerEmpresas}>
                  <div className={classes.formRowOne}>
                    <TextInput
                      label="Nombre completo"
                      fullWidth
                      placeholder="Nombre completo"
                      value={name}
                      id={FIELDS.NAME}
                      onChange={getFieldValue}
                      error={requiredFields[FIELDS.NAME] ?? false}
                    />
                  </div>
                  <div className={classes.formRowTwo}>
                    <div className={classes.idTypeContainer}>
                      <Select
                        label="Documento"
                        options={ID_VALUES}
                        fullWidth
                        value={documentType}
                        name={FIELDS.DOCUMENT_TYPE}
                        onChange={getFieldValue}
                      />
                    </div>
                    <div className={classes.idNumberContainer}>
                      <TextInput
                        fullWidth
                        placeholder="Número documento"
                        value={documentNumber}
                        id={FIELDS.DOCUMENT_NUMBER}
                        onChange={getFieldValue}
                        error={requiredFields[FIELDS.DOCUMENT_NUMBER] ?? false}
                      />
                    </div>
                  </div>
                  <div className={classes.formRowTwo}>
                    <div className={classes.genreContainer}>
                      <Select
                        label="Género"
                        options={GENRE_VALUES}
                        value={genre.value}
                        fullWidth
                        name={FIELDS.GENRE}
                        onChange={getFieldValue}
                      />
                    </div>
                    <div className={classes.phoneContainer}>
                      <TextInput
                        fullWidth
                        placeholder="Número teléfono"
                        label="Teléfono"
                        value={phoneNumber}
                        id={FIELDS.PHONE_NUMBER}
                        onChange={getFieldValue}
                        error={requiredFields[FIELDS.PHONE_NUMBER] ?? false}
                      />
                    </div>
                  </div>
                  <div className={classes.formRowTwo}>
                    <div className={classes.provinceContainer}>
                      <DropdownInput
                        fullWidth
                        label="Departamento"
                        options={provinces}
                        placeholder="Departamento"
                        value={province}
                        getOptionLabel={getOptionLabel}
                        onChange={getListValue}
                        id={FIELDS.PROVINCE}
                        error={requiredFields[FIELDS.PROVINCE] ?? false}
                      />
                    </div>
                    <div className={classes.cityContainer}>
                      <DropdownInput
                        fullWidth
                        label="Ciudad"
                        options={cities}
                        value={city}
                        placeholder="Ciudad"
                        filterOptions={filterCitiesList}
                        getOptionLabel={getOptionLabel}
                        onChange={getListValue}
                        id={FIELDS.CITY}
                        error={requiredFields[FIELDS.CITY] ?? false}
                      />
                    </div>
                  </div>
                  <div className={classes.formRowTwo}>
                    <TextInput
                      label="Correo electrónico"
                      fullWidth
                      placeholder="Correo electrónico"
                      value={email}
                      id={FIELDS.EMAIL}
                      onChange={getFieldValue}
                      error={requiredFields[FIELDS.EMAIL] ?? false}
                    />
                  </div>
                  <div className={classes.buttonContainer}>
                    <Button
                      onClick={sendUserData}
                      loading={disableSendButton}
                      type="submit"
                    >
                      ¡Listo!
                    </Button>
                  </div>
                </div>
              ) : null}
            </Card>
          </div>
        </form>
      </div>
      <div
        className={clsx(classes.backgroundContainer, {
          [classes.backgroundPymes]: currentForm === 'empresas',
          [classes.backgroundIndependientes]: currentForm === 'independientes',
        })}
      >
        <Character
          content={currentForm === 'empresas' ? alex : camilo}
          height={currentForm === 'empresas' ? '95%' : '80%'}
        />
      </div>
    </div>
  );
};

const Character = ({ content, height }) => {
  return (
    <img src={content} width="auto" height={height} alt="Personaje registro" />
  );
};

Character.propTypes = {
  content: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
};

export default SignUp;
