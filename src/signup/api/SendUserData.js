import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default (data) => {
	return new Promise((resolve, reject) => {
		fetch(Endpoints.sendUserData, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error enviando la información del usuario',
					error,
				});
			});
	});
};
