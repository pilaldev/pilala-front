import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default async (nit) => {
  let result;
  const requestBody = JSON.stringify({
    nit,
  });

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: requestBody,
  };

  await fetch(Endpoints.requestMisDatos, requestOptions)
    .then((response) => response.json())
    .then((apiResult) => {
      result = apiResult;
    })
    .catch((error) => {
      console.log('Error fetch de consulta en mis datos', error);
    });
  return result;
};
