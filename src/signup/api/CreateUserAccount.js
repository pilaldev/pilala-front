import { FIREBASE_AUTH } from '../../api/Firebase';
import {
    SUCCESS,
    FAILED,
    EMAIL_ALREADY_IN_USE,
    INVALID_EMAIL,
    WEAK_PASSWORD,
    DEFAULT_MESSAGE
} from './../Constants';

export default async (email, password) => {
    return new Promise(async (resolve) => {
        await FIREBASE_AUTH.createUserWithEmailAndPassword(email, password).then((userCredential) => {
            resolve({
                status: SUCCESS,
                userCredential
            });
        }).catch((error) => {
            switch (error.code) {
                case 'auth/email-already-in-use':
                    resolve({
                        status: FAILED,
                        message: EMAIL_ALREADY_IN_USE
                    });
                    break;

                case 'auth/invalid-email':
                    resolve({
                        status: FAILED,
                        message: INVALID_EMAIL
                    });
                    break;

                case 'auth/weak-password':
                    resolve({
                        status: FAILED,
                        message: WEAK_PASSWORD
                    });
                    break;

                default:
                    resolve({
                        status: FAILED,
                        message: DEFAULT_MESSAGE
                    });
            }
        });
    });
}
