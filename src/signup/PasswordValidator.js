import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import clsx from 'clsx';
import { useSelector } from 'react-redux';

const PasswordValidator = ({ score, passwordStrengthError }) => {
    const classes = useStyles();
    const userPassword = useSelector(state => state.signUp.userPassword);
    const showValidator = userPassword.length > 0;
    const [passwordLevel, setPasswordLevel] = useState([]);

    useEffect(() => {
        const formatScore = () => {
            let result = [];
            switch (score) {
                case 0:
                    result = [{ value: 'Débil', colorId: 1 }];
                    break;
                case 1:
                    result = [{ value: 'Débil', colorId: 1 }];
                    break;
                case 2:
                    result = [{ value: 'Débil', colorId: 2 }, { value: 'Media', colorId: 2 }];
                    break;
                case 3:
                    result = [{ value: 'Débil', colorId: 3 }, { value: 'Media', colorId: 3 }, { value: 'Fuerte', colorId: 3 }];
                    break;
                case 4:
                    result = [{ value: 'Débil', colorId: 4 }, { value: 'Media', colorId: 4 }, { value: 'Fuerte', colorId: 4 }, { value: 'Muy fuerte', colorId: 4 }];
                    break;
                default:
                    result = [];
            }
            setPasswordLevel(result);
        }
        formatScore();
    }, [score]);

    return (
        <div
            className={clsx(classes.container, {
                [classes.hideContainer]: !showValidator
            })}
        >
            <div className={classes.contentContainer}>
                {
                    passwordLevel.map((level, index) => {
                        const lastLevel = passwordLevel.length - 1;
                        return (
                            <div
                                className={clsx(classes.levelContainer, {
                                    [classes.removeMargin]: lastLevel === index
                                })}
                                key={level.value}
                            >
                                <div className={clsx(classes.levelItem, classes[`levelBackground_${level.colorId}`])} />
                                {
                                    lastLevel === index ?
                                        <Typography className={clsx(classes.levelText, classes[`levelColor_${level.colorId}`])}>
                                            {level.value}
                                        </Typography>
                                        : null
                                }
                            </div>
                        );
                    })
                }
            </div>
            {
                passwordStrengthError ?
                    <div className={classes.passwordMessageContainer}>
                        <Typography className={classes.checkListMessage}>
                            Tu contraseña debe alcanzar como mínimo un nivel de seguridad medio
                    </Typography>
                    </div>
                    : null
            }
        </div>
    );
}

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    hideContainer: {
        display: 'none'
    },
    contentContainer: {
        display: 'flex',
        width: '100%',
        marginTop: theme.spacing(1)
    },
    levelContainer: {
        width: '25%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginRight: theme.spacing(1),
        justifyContent: 'flex-start'
    },
    levelItem: {
        width: '100%',
        height: theme.spacing(1),
        borderRadius: 5
    },
    levelText: {
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.7rem'
        },
        [theme.breakpoints.up('sm')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.9rem'
        }
    },
    levelBackground_1: {
        backgroundColor: theme.palette.error[400]
    },
    levelBackground_2: {
        backgroundColor: theme.palette.info[400]
    },
    levelBackground_3: {
        backgroundColor: theme.palette.success[400]
    },
    levelBackground_4: {
        backgroundColor: theme.palette.success.main
    },
    levelColor_1: {
        color: theme.palette.error.main
    },
    levelColor_2: {
        color: theme.palette.info.main
    },
    levelColor_3: {
        color: theme.palette.success.main
    },
    levelColor_4: {
        color: theme.palette.success.main
    },
    removeMargin: {
        marginRight: 0
    },
    passwordMessageContainer: {
        backgroundColor: theme.palette.info[200],
        width: '100%',
        display: 'flex',
        borderRadius: theme.spacing(1),
        marginTop: theme.spacing(1),
        padding: theme.spacing(1, 2)
    },
    checkListMessage: {
        color: theme.palette.info.main,
        [theme.breakpoints.up('xs')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '0.9rem'
        }
    }
}));

export default PasswordValidator;
