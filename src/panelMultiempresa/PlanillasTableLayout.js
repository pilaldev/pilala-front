import React from 'react';
import MaterialTable, { MTableCell } from 'material-table';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    ButtonBase,
    Avatar
} from '@material-ui/core';
import BusinessTwoToneIcon from '@material-ui/icons/BusinessTwoTone';
import HelperFunctions from './../utils/HelperFunctions';
import {
    COMPANY,
    PLANILLA_DATE,
    PAID,
    PAY,
    CHECKING,
    PLANILLA_VALUE,
    SHOW_PLANILLA,
    PLANILLA_ID,
    PERIOD,
    PLANILLA_TYPE,
    PLANILLA_STATE
} from './Constants';
import { useSelector } from 'react-redux';
import moment from 'moment';
import 'moment/locale/es';

moment.locale('es');

const PlanillasTableLayout = (props) => {

    const classes = useStyles();
    const columnProps = {
        sorting: false,
        headerStyle: {
            fontSize: '1em',
            fontWeight: 'bold',
            textAlign: 'center',
            color: '#263238'
        },
        cellStyle: {
            border: '0px solid transparent'
        }
    }
    const columns = [
        {
            title: COMPANY,
            field: 'company',
            ...columnProps,
            headerStyle: {
                ...columnProps.headerStyle,
                textAlign: 'left',
            },
            render: (props) => (<AportanteColumn { ...props } />)
        },
        {
            title: PLANILLA_DATE,
            name: 'date',
            ...columnProps,
            render: (props) => (<DateColumn { ...props } />)
        },
        {
            title: PLANILLA_ID,
            name: 'idPlanilla',
            ...columnProps,
            render: (props) => (<PlanillaIdColumn { ...props } />)
        },
        {
            title: PERIOD,
            name: 'period',
            ...columnProps,
            render: (props) => (<PeriodColumn { ...props } />)
        },
        {
            title: PLANILLA_TYPE,
            name: 'planillaType',
            ...columnProps,
            render: (props) => (<PlanillaTypeColumn { ...props } />)
        },
        {
            title: PLANILLA_VALUE,
            name: 'value',
            ...columnProps,
            render: (props) => (<PlanillaValueColumn { ...props } />)
        },
        {
            title: PLANILLA_STATE,
            name: 'state',
            ...columnProps,
            headerStyle: {
                ...columnProps.headerStyle,
                textAlign: 'left'
            },
            render: (props) => (<PlanillaStateColumn { ...props } />)
        }

    ];
    const multiPlanillas = useSelector(state => state.panelMultiempresa.multiPlanillas);
    const planillasData = multiPlanillas.map((item) => ({
        company: 'Pilalá Colombia SAS',
        date: moment(item.periodo.value, 'DD-MM-AAAA').format('DD/MM/YYYY'),
        idPlanilla: item.numeroPlanilla,
        period: moment(item.periodo.monthDate, 'MM').format('MMMM'),
        planillaType: item.tipoPlanilla,
        value: HelperFunctions.formatStringNumber(item.totalPagar),
        state: item.estadoPlanilla
    }));

    return (
        <div className={ classes.container }>
            <MaterialTable
                columns={ columns }
                data={ planillasData }
                style={ tableStyles.table }
                components={ {
                    Toolbar: (props) => null,
                    Cell: (props) => <MTableCell { ...props } classes={ { root: classes.table } } />
                } }
                options={ {
                    draggable: false,
                    headerStyle: tableStyles.header,
                    rowStyle: (data) => ({
                        backgroundColor: (data.tableData.id % 2) === 0 ? '#ECEFF1' : '#FFFFFF'
                    }),
                    emptyRowsWhenPaging: false,
                    minBodyHeight: !(planillasData.length > 5) ? (5 * 58) + 44 : null
                } }
            />
        </div>
    );
}

const AportanteColumn = (props) => {

    const classes = makeStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            width: '100%'
        },
        avatar: {
            width: 35,
            height: 35
        },
        planillaCompanyNameContainer: {
            display: 'flex',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-start'
        },
        planillaCompanyName: {
            fontSize: '1em',
            fontWeight: 'bold',
            color: '#455A64',
            marginLeft: 10
        }
    })();

    return (
        <div className={ classes.container }>
            <Avatar className={ classes.avatar }>
                <BusinessTwoToneIcon />
            </Avatar>
            <div className={ classes.planillaCompanyNameContainer }>
                <Typography className={ classes.planillaCompanyName }>
                    { `${props.company}` }
                </Typography>
            </div>
        </div>
    );
};

const DateColumn = (props) => {

    const classes = makeStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            width: '100%',
            justifyContent: 'center'
        },
        planillaDate: {
            fontSize: '1em',
            color: '#455A64'
        }
    })();

    return (
        <div className={ classes.container }>
            <Typography className={ classes.planillaDate }>
                { props.date }
            </Typography>
        </div>
    );
}

const PlanillaIdColumn = (props) => {

    const classes = makeStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            width: '100%',
            justifyContent: 'center'
        },
        planillaId: {
            fontSize: '1em',
            color: '#455A64'
        }
    })();

    return (
        <div className={ classes.container }>
            <Typography className={ classes.planillaId }>
                { props.idPlanilla }
            </Typography>
        </div>
    );
}

const PeriodColumn = (props) => {

    const classes = makeStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            width: '100%',
            justifyContent: 'center'
        },
        planillaPeriod: {
            fontSize: '1em',
            color: '#455A64',
            fontWeight: 'bold',
            textTransform: 'capitalize'
        }
    })();

    return (
        <div className={ classes.container }>
            <Typography className={ classes.planillaPeriod }>
                { props.period }
            </Typography>
        </div>
    );
}

const PlanillaTypeColumn = (props) => {

    const classes = makeStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            width: '100%',
            justifyContent: 'center'
        },
        planillaType: {
            fontSize: '1em',
            color: '#455A64'
        }
    })();

    return (
        <div className={ classes.container }>
            <Typography className={ classes.planillaType }>
                { props.planillaType }
            </Typography>
        </div>
    );
}

const PlanillaValueColumn = (props) => {

    const classes = makeStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            width: '100%',
            justifyContent: 'center'
        },
        planillaType: {
            fontSize: '1em',
            color: '#455A64'
        }
    })();

    return (
        <div className={ classes.container }>
            <Typography className={ classes.planillaType }>
                { `$${HelperFunctions.formatStringNumber(props.value)}` }
            </Typography>
        </div>
    );
}

const PlanillaStateColumn = (props) => {

    const classes = makeStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            width: '100%'
        },
        planillaPayButton: {
            backgroundColor: props.state === 'paid' ? '#95989A26' : props.state === 'pay' ? '#5E35B126' : props.state === 'pending' ? '#2962FF26' : '#FFFFFF',
            width: '45%',
            padding: '4px 0px 4px 0px',
            borderRadius: 100,
            border: '2px solid',
            borderColor: props.state === 'paid' ? '#95989A' : props.state === 'pay' ? '#5E35B1' : props.state === 'pending' ? '#2962FF' : '#FFFFFF',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 30
        },
        planillaPayText: {
            color: props.state === 'paid' ? '#95989A' : props.state === 'pay' ? '#5E35B1' : props.state === 'pending' ? '#2962FF' : '#FFFFFF',
            fontSize: '1em'
        },
        planillaDetail: {
            color: '#2962FF',
            fontSize: '1em'
        }
    })();

    return (
        <div className={ classes.container }>
            <div className={ classes.planillaPayButton }>
                <Typography className={ classes.planillaPayText }>
                    {
                        props.state === 'paid' ?
                            PAID :
                            props.state === 'pay' ?
                                PAY :
                                props.state === 'pending' ?
                                    CHECKING : ''
                    }
                </Typography>
            </div>
            <ButtonBase disableRipple>
                <Typography className={ classes.planillaDetail }>
                    { SHOW_PLANILLA }
                </Typography>
            </ButtonBase>
        </div>
    );
}

const tableStyles = {
    table: {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        borderWidth: 0,
        boxShadow: 'none'
    },
    header: {
        border: 0
    }
}

const useStyles = makeStyles({
    container: {
        width: '100%',
        display: 'flex',
        flex: 1
    },
    table: {
        '&.MuiTableCell-root': {
            padding: 8
        }
    },
    pagination: {
        '&.MuiTableFooter-root': {
            border: '2px solid red',
            display: 'flex'
        }
    }
});

export default PlanillasTableLayout;
