import Endpoints from '@bit/pilala.pilalalib.endpoints';

const getMultiPlanillasInfo = async (idUser, companies) => {
	let result = {};

	const requestData = {
		idUser,
		companies,
	};

	const requestBody = JSON.stringify(requestData);

	const requestOptions = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: requestBody,
	};

	await fetch(Endpoints.getMultiPlanillasInfo, requestOptions)
		.then((response) => response.json())
		.then((resultFetch) => {
			result.status = resultFetch.status;
			result.data = resultFetch.result;
		})
		.catch((error) => {
			console.log(
				'Error en fetch de obtener historial de planillas de todos los aportantes',
				error
			);
			result.status = 'FETCH_FAILED';
			result.data = null;
		});

	return result;
};

export default getMultiPlanillasInfo;
