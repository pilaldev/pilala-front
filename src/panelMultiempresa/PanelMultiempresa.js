import React, { Component } from 'react';
import { connect } from 'react-redux';
import PanelMultiempresaLayout from './PanelMultiempresaLayout';
import {
  currentUserAction,
  panelMultiempresaAction,
  companyAction,
  cotizantesAction,
  navigationAction,
  snackBarAction,
  prePlanillaAction,
  credentialsModalAction,
} from './../redux/Actions';
import {
  CURRENT_USER,
  PANEL_MULTIEMPRESA,
  COMPANY,
  COTIZANTES,
  NAVIGATION,
  SNACKBAR,
  PRE_PLANILLA,
  CREDENTIALS_MODAL,
} from './../redux/ActionTypes';
import Endpoints from '@bit/pilala.pilalalib.endpoints';
import { FIELDS_ID, LR_FIELDS_ID, BC_FIELDS_ID } from './../company/Constants';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import GetMultiPlanillasInfo from './api/GetMultiPlanillasInfo';
import HelperFunctions from '../utils/HelperFunctions';
import moment from 'moment';
import 'moment/locale/es';

moment.locale('es');

class PanelMultiempresa extends Component {
  state = {
    disableCompanyButton: false,
  };

  componentDidMount() {
    ChangePageTitle('Panel multiempresa | Pilalá');
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'panelMultiempresa'
    );
    this.getMultiPlanillasInfo();
  }

  getMultiPlanillasInfo = async () => {
    const { adminEmail: idUser, companies } = this.props;
    const idCompanies = companies.map((item) => item.idCompany);
    const historyResult = await GetMultiPlanillasInfo(idUser, idCompanies);
    if (historyResult.status === 'SUCCESS') {
      const planillasData = historyResult.data.map((item) => ({
        company: 'Pilalá Colombia SAS',
        date: moment(item.periodo.value, 'DD-MM-AAAA').format('DD/MM/YYYY'),
        idPlanilla: item.numeroPlanilla,
        period: moment(item.periodo.monthDate, 'MM').format('MMMM'),
        planillaType: item.tipoPlanilla,
        value: HelperFunctions.formatStringNumber(item.totalPagar),
        state: item.estadoPlanilla,
      }));
      this.props.panelMultiempresaAction(
        PANEL_MULTIEMPRESA.PANEL_MULTIEMPRESA_SAVE_MULTI_PLANILLAS,
        planillasData
      );
    }
  };

  selectCompany = (idCompany) => {
    if (this.props.currentUser.companiesInfo[idCompany]) {
      const selectedCompany = this.props.currentUser.companiesInfo[idCompany];
      let companyData = {
        companyName: selectedCompany.companyName,
        economicActivity: selectedCompany.economicActivity,
        province: selectedCompany.location.province,
        city: selectedCompany.location.city,
        phoneNumber: selectedCompany.phoneNumber,
        cellphoneNumber: selectedCompany.mobileNumber,
        identificationType: selectedCompany.identificationType,
        identificationNumber: selectedCompany.nit,
        verificationDigit: selectedCompany.verificationDigit,
        arl: selectedCompany.arl,
        cajaCompensacion: selectedCompany.cajaCompensacion,
        claseArl: selectedCompany.claseArl,
        address: selectedCompany.location.address,
        claseAportante: selectedCompany.clasificacionAportanteCodigo,
        exentoParafiscales: selectedCompany.exentoPagoParafiscales,
        beneficiadoLey590: selectedCompany.beneficiadoLey590Del2000,
        constitucionDate: selectedCompany.fechaConstitucion,
        obligacionSena: !selectedCompany.exentoPagoParafiscales,
        obligacionICBF: !selectedCompany.exentoPagoParafiscales,
      };

      let LRData = {
        name: selectedCompany.legalRepresentative.name,
        email: selectedCompany.legalRepresentative.email,
        identificationType: selectedCompany.legalRepresentative.documentType,
        identificationNumber:
          selectedCompany.legalRepresentative.documentNumber,
        phoneNumber: selectedCompany.legalRepresentative.phone,
        cellphoneNumber: selectedCompany.legalRepresentative.mobileNumber,
      };

      let BCData = {
        name: selectedCompany.companyContact.name,
        email: selectedCompany.companyContact.email,
        identificationType: selectedCompany.companyContact.documentType,
        identificationNumber: selectedCompany.companyContact.documentNumber,
        phoneNumber: selectedCompany.companyContact.phone,
        cellphoneNumber: selectedCompany.companyContact.mobileNumber,
      };

      const fields = {
        mainCompany: selectedCompany.isCompany,
        selectedSucursal: selectedCompany.isCompany
          ? 'Principal'
          : selectedCompany.sucursalName,
      };

      this.props.companyAction(COMPANY.COMPANY_UPDATE_SOME_FIELDS, fields);
      this.props.companyAction(COMPANY.COMPANY_SAVE_ALL_DATA, companyData);
      this.props.companyAction(COMPANY.COMPANY_SAVE_LR_ALL_DATA, LRData);
      this.props.companyAction(COMPANY.COMPANY_SAVE_BC_ALL_DATA, BCData);
      this.props.currentUserAction(
        CURRENT_USER.CURRENT_USER_SAVE_USER_SELECTED_COMPANY,
        idCompany
      );
      this.props.navigationAction(
        NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
        'panelControl'
      );
      this.props.navigationAction(
        NAVIGATION.NAVIGATION_ACTIVE_PANEL_MULTIEMPRESA,
        false
      );
      this.props.history.push(
        `/${this.props.currentUser.uid}/console/controlPanel`
      );
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Error al seleccionar aportante',
        description: 'No se encontró la información de esta compañía',
        color: 'error',
      });
    }
  };

  openNewCompanyModal = async () => {
    if (this.props.showNewCompanyModal === false) {
      await this.props.companyAction(
        COMPANY.COMPANY_COPY_COMPANY_DATA,
        'company'
      );
      await this.props.companyAction(
        COMPANY.COMPANY_COPY_COMPANY_DATA,
        'businessContact'
      );
      await this.props.companyAction(
        COMPANY.COMPANY_COPY_COMPANY_DATA,
        'legalRepresentative'
      );
      await this.props.companyAction(COMPANY.COMPANY_REMOVE_CARDS_DATA, null);
    } else {
      await this.props.companyAction(
        COMPANY.COMPANY_RESET_BACKUP_DATA,
        'company'
      );
      await this.props.companyAction(
        COMPANY.COMPANY_RESET_BACKUP_DATA,
        'businessContact'
      );
      await this.props.companyAction(
        COMPANY.COMPANY_RESET_BACKUP_DATA,
        'legalRepresentative'
      );
    }
    this.props.panelMultiempresaAction(
      PANEL_MULTIEMPRESA.PANEL_MULTIEMPRESA_SHOW_NEW_COMPANY_MODAL,
      null
    );
  };

  createNewCompany = async () => {
    if (this.state.disableCompanyButton) return;
    this.setState({
      disableCompanyButton: true,
    });

    const fieldsStatus = await this.checkAllFields();
    if (fieldsStatus === true) {
      const managerData = {
        name: this.props.managerName,
        email: this.props.managerEmail,
        documentType: this.props.managerDocumentType,
        documentNumber: this.props.managerDocumentNumber,
        phone: this.props.managerPhoneNumber,
        mobileNumber: this.props.managerCellphoneNumber,
      };

      const companyContact = {
        name: this.props.companyContactName,
        email: this.props.companyContactEmail,
        documentType: this.props.companyContactDocumentType,
        documentNumber: this.props.companyContactDocumentNumber,
        phone: this.props.companyContactPhoneNumber,
        mobileNumber: this.props.companyContactcellphoneNumber,
      };

      const companyData = {
        companyName: this.props.companyName,
        arl: this.props.arl,
        claseArl: this.props.claseArl,
        cajaCompensacion: this.props.cajaCompensacion,
        phoneNumber: this.props.phoneNumber,
        mobileNumber: this.props.cellphoneNumber,
        nitCompany: this.props.nit,
        identificationType: this.props.identificationType,
        verificationDigit: this.props.verificationDigit,
        actividadEconomica: this.props.economicActivity,
        companyContact: companyContact,
        managerData: managerData,
        exentoPagoParafiscales: this.props.exentoPagoParafiscales,
        beneficiadoLey590Del2000: this.props.beneficiadoLey590Del2000,
        fechaConstitucion: this.props.fechaConstitucion,
        clasificacionAportanteCodigo: this.props.claseAportante,
      };
      const companyFullDataToLoad = {
        companyData: companyData,
        location: {
          province: this.props.province,
          city: this.props.city,
          address: this.props.address,
        },
        adminPlatform: {
          displayName: this.props.adminName,
          email: this.props.adminEmail,
        },
        nitCompany: companyData.nitCompany,
      };

      await fetch(Endpoints.sendCompanyData, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(companyFullDataToLoad),
      })
        .then((response) => response.json())
        .then(async (resultData) => {
          if (resultData.status === 'SUCCESS') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Creación exitosa',
              description: 'El aportante ha sido creado correctamente',
              color: 'success',
            });
            if (this.props.showNewCompanyModal === true) {
              this.props.panelMultiempresaAction(
                PANEL_MULTIEMPRESA.PANEL_MULTIEMPRESA_SHOW_NEW_COMPANY_MODAL,
                null
              );
            }
            await this.formatCompanyData(resultData.result.companyObject);
            await this.createdCompanyNextSteps();
            this.props.companyAction(COMPANY.COMPANY_UPDATE_SOME_FIELDS, {
              selectedSucursal: resultData.result.companyObject.isCompany
                ? 'Principal'
                : resultData.result.companyObject.companyName,
            });
          } else if (resultData.status === 'FAILED') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear aportante',
              description:
                'Hemos tenido problemas para crear el aportante, inténtelo nuevamente',
              color: 'error',
            });
            this.setState({
              disableCompanyButton: false,
            });
          }
        })
        .catch((error) => {
          console.log('ERROR', error);

          this.setState({
            disableCompanyButton: false,
          });
        });
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Información requerida',
        description:
          'Hacen falta algunos datos obligatorios para crear el aportante',
        color: 'warning',
      });
      this.setState({
        disableCompanyButton: false,
      });
    }
  };

  checkCompanyFields = async () => {
    if (this.props.companyName === '') {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.COMPANY_NAME)) {
        await this.addRequiredField(FIELDS_ID.COMPANY_NAME, 'company');
      }
    }

    if (this.props.identificationType.code === '') {
      if (
        !this.props.companyRequiredFields.includes(
          FIELDS_ID.IDENTIFICATION_TYPE
        )
      ) {
        await this.addRequiredField(FIELDS_ID.IDENTIFICATION_TYPE, 'company');
      }
    }

    if (this.props.identificationNumber === '') {
      if (
        !this.props.companyRequiredFields.includes(
          FIELDS_ID.IDENTIFICATION_NUMBER
        )
      ) {
        await this.addRequiredField(FIELDS_ID.IDENTIFICATION_NUMBER, 'company');
      }
    }

    if (
      this.props.economicActivity === '' ||
      this.props.economicActivity.code === '-1'
    ) {
      if (
        !this.props.companyRequiredFields.includes(FIELDS_ID.ECONOMIC_ACTIVITY)
      ) {
        await this.addRequiredField(FIELDS_ID.ECONOMIC_ACTIVITY, 'company');
      }
    }

    if (this.props.claseAportante.code === -1) {
      if (
        !this.props.companyRequiredFields.includes(FIELDS_ID.CLASE_APORTANTE)
      ) {
        await this.addRequiredField(FIELDS_ID.CLASE_APORTANTE, 'company');
      }
    }

    // if (this.props.address.tipoVial.code === '') {
    // 	if (!this.props.companyRequiredFields.includes(FIELDS_ID.ADDRESS.TIPO_VIAL)) {
    // 		await this.addRequiredField(FIELDS_ID.ADDRESS.TIPO_VIAL, 'company');
    // 	}
    // }

    // if (this.props.address.numeroVial === '') {
    // 	if (!this.props.companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_VIAL)) {
    // 		await this.addRequiredField(FIELDS_ID.ADDRESS.NUMERO_VIAL, 'company');
    // 	}
    // }

    // if (this.props.address.numeroViaGeneradora === '') {
    // 	if (!this.props.companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA)) {
    // 		await this.addRequiredField(FIELDS_ID.ADDRESS.NUMERO_VIA_GENERADORA, 'company');
    // 	}
    // }

    // if (this.props.address.numeroPlaca === '') {
    // 	if (!this.props.companyRequiredFields.includes(FIELDS_ID.ADDRESS.NUMERO_PLACA)) {
    // 		await this.addRequiredField(FIELDS_ID.ADDRESS.NUMERO_PLACA, 'company');
    // 	}
    // }

    if (this.props.province.code === '') {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.PROVINCE)) {
        await this.addRequiredField(FIELDS_ID.PROVINCE, 'company');
      }
    }

    if (this.props.city.code === '') {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.CITY)) {
        await this.addRequiredField(FIELDS_ID.CITY, 'company');
      }
    }

    if (this.props.arl === '') {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.ARL)) {
        await this.addRequiredField(FIELDS_ID.ARL, 'company');
      }
    }

    if (this.props.cajaCompensacion.value === null) {
      if (
        !this.props.companyRequiredFields.includes(FIELDS_ID.CAJA_COMPENSACION)
      ) {
        await this.addRequiredField(FIELDS_ID.CAJA_COMPENSACION, 'company');
      }
    }

    if (this.props.claseArl.code === -1) {
      if (!this.props.companyRequiredFields.includes(FIELDS_ID.CLASE_ARL)) {
        await this.addRequiredField(FIELDS_ID.CLASE_ARL, 'company');
      }
    }

    if (this.props.beneficiadoLey590Del2000 === true) {
      if (this.props.fechaConstitucion.code === '') {
        if (
          !this.props.companyRequiredFields.includes(
            FIELDS_ID.FECHA_CONSTITUCION
          )
        ) {
          await this.addRequiredField(FIELDS_ID.FECHA_CONSTITUCION, 'company');
        }
      }
    }

    if (this.props.companyRequiredFields.length === 0) {
      return true;
    } else {
      return false;
    }
  };

  checkBusinessContactFields = async () => {
    if (this.props.companyContactName.display === '') {
      if (
        !this.props.businessContactRequiredFields.includes(BC_FIELDS_ID.NAME)
      ) {
        await this.addRequiredField(BC_FIELDS_ID.NAME, 'businessContact');
      }
    }

    if (this.props.companyContactDocumentType === '') {
      if (
        !this.props.businessContactRequiredFields.includes(
          BC_FIELDS_ID.IDENTIFICATION_TYPE
        )
      ) {
        await this.addRequiredField(
          BC_FIELDS_ID.IDENTIFICATION_TYPE,
          'businessContact'
        );
      }
    }

    if (this.props.companyContactDocumentNumber === '') {
      if (
        !this.props.businessContactRequiredFields.includes(
          BC_FIELDS_ID.IDENTIFICATION_NUMBER
        )
      ) {
        await this.addRequiredField(
          BC_FIELDS_ID.IDENTIFICATION_NUMBER,
          'businessContact'
        );
      }
    }

    if (this.props.companyContactPhoneNumber === '') {
      if (
        !this.props.businessContactRequiredFields.includes(
          BC_FIELDS_ID.PHONE_NUMBER
        )
      ) {
        await this.addRequiredField(
          BC_FIELDS_ID.PHONE_NUMBER,
          'businessContact'
        );
      }
    }

    if (this.props.companyContactcellphoneNumber === '') {
      if (
        !this.props.businessContactRequiredFields.includes(
          BC_FIELDS_ID.CELLPHONE_NUMBER
        )
      ) {
        await this.addRequiredField(
          BC_FIELDS_ID.CELLPHONE_NUMBER,
          'businessContact'
        );
      }
    }

    if (this.props.companyContactEmail === '') {
      if (
        !this.props.businessContactRequiredFields.includes(BC_FIELDS_ID.EMAIL)
      ) {
        await this.addRequiredField(BC_FIELDS_ID.EMAIL, 'businessContact');
      }
    }

    if (this.props.businessContactRequiredFields.length === 0) {
      return true;
    } else {
      return false;
    }
  };

  checkLegalRepresentativeFields = async () => {
    if (this.props.managerName.display === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.NAME
        )
      ) {
        await this.addRequiredField(LR_FIELDS_ID.NAME, 'legalRepresentative');
      }
    }

    if (this.props.managerDocumentType === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.IDENTIFICATION_TYPE
        )
      ) {
        await this.addRequiredField(
          LR_FIELDS_ID.IDENTIFICATION_TYPE,
          'legalRepresentative'
        );
      }
    }

    if (this.props.managerDocumentNumber === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.IDENTIFICATION_NUMBER
        )
      ) {
        await this.addRequiredField(
          LR_FIELDS_ID.IDENTIFICATION_NUMBER,
          'legalRepresentative'
        );
      }
    }

    if (this.props.managerPhoneNumber === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.PHONE_NUMBER
        )
      ) {
        await this.addRequiredField(
          LR_FIELDS_ID.PHONE_NUMBER,
          'legalRepresentative'
        );
      }
    }

    if (this.props.managerCellphoneNumber === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.CELLPHONE_NUMBER
        )
      ) {
        await this.addRequiredField(
          LR_FIELDS_ID.CELLPHONE_NUMBER,
          'legalRepresentative'
        );
      }
    }

    if (this.props.managerEmail === '') {
      if (
        !this.props.legalRepresentativeRequiredFields.includes(
          LR_FIELDS_ID.EMAIL
        )
      ) {
        await this.addRequiredField(LR_FIELDS_ID.EMAIL, 'legalRepresentative');
      }
    }

    if (this.props.legalRepresentativeRequiredFields.length === 0) {
      return true;
    } else {
      return false;
    }
  };

  formatCompanyData = async (data) => {
    const company = {
      city: data.location.city.value,
      companyName: data.companyName,
      id: data.idCompany,
      nitCompany: data.nit,
      province: data.location.province.value,
      sucursales: [],
    };
    const companyInfo = {
      data: {
        ...data,
      },
      idCompany: data.idCompany,
    };
    const periodoInfo = {
      activeCompany: data.idCompany,
      periodo: data.periodoCotizacionActual,
    };
    const prePlanillaValue = {
      [data.idCompany]: {
        [data.periodoCotizacionActual.value]: {
          dependientes: {
            [data.periodoCotizacionActual.month]: {},
          },
        },
      },
    };

    await this.props.prePlanillaAction(
      PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA,
      prePlanillaValue
    );
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_SAVE_USER_SELECTED_COMPANY,
      data.idCompany
    );
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_UPDATE_COMPANIES,
      company
    );
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_UPDATE_COMPANIES_INFO,
      companyInfo
    );
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_UPDATE_PERIODOS_INFO,
      periodoInfo
    );
  };

  createdCompanyNextSteps = async () => {
    await this.props.currentUserAction(
      CURRENT_USER.CURRENT_USER_SAVE_CREATED_COMPANY,
      true
    );
    await this.props.cotizantesAction(
      COTIZANTES.COTIZANTES_OPEN_NEW_COTIZANTE_DIALOG,
      null
    );
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'cotizantes'
    );
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_ACTIVE_PANEL_MULTIEMPRESA,
      false
    );
    this.setState({
      disableCompanyButton: false,
    });
    this.props.history.push(
      `/${this.props.currentUser.uid}/console/cotizantes`
    );
    const fields = {
      copyCompanyData: {},
      copylegalRepresentativeData: {},
      copybusinessContactData: {},
    };
    this.props.companyAction(COMPANY.COMPANY_UPDATE_SOME_FIELDS, fields);
  };

  addRequiredField = async (field, card) => {
    const data = {
      field,
      status: true,
    };

    if (card === 'company')
      await this.props.companyAction(
        COMPANY.COMPANY_COMPANY_REQUIRED_FIELD,
        data
      );

    if (card === 'legalRepresentative')
      await this.props.companyAction(
        COMPANY.COMPANY_LEGAL_REPRESENTATIVE_REQUIRED_FIELD,
        data
      );

    if (card === 'businessContact')
      await this.props.companyAction(
        COMPANY.COMPANY_BUSINESS_CONTACT_REQUIRED_FIELD,
        data
      );
  };

  checkAllFields = async () => {
    const resultCompany = await this.checkCompanyFields();
    const resultLR = await this.checkLegalRepresentativeFields();
    const resultBC = await this.checkBusinessContactFields();
    return resultCompany && resultLR && resultBC;
  };

  openPlanillaDetail = (idPlanilla) => {
    this.props.history.push(
      `/${this.props.currentUser.uid}/console/planillaDetail/${idPlanilla}`
    );
  };

  openCheckCompanyModal = () => {
    this.props.credentialsModalAction(CREDENTIALS_MODAL.OPEN_MODAL, 'default');
  };

  render() {
    const layoutProps = {
      companies: this.props.companies,
      selectCompany: this.selectCompany,
      showNewCompanyModal: this.props.showNewCompanyModal,
      openNewCompanyModal: this.openNewCompanyModal,
      createNewCompany: this.createNewCompany,
      disableCompanyButton: this.state.disableCompanyButton,
      openPlanillaDetail: this.openPlanillaDetail,
      openCheckCompanyModal: this.openCheckCompanyModal,
    };

    return <PanelMultiempresaLayout {...layoutProps} />;
  }
}

const mapStateToProps = (state) => {
  return {
    companies: state.currentUser.companies.map((item) => ({
      companyName: item.companyName,
      idCompany: item.id,
      sucursales: item.sucursales,
    })),
    currentUser: state.currentUser,
    showNewCompanyModal: state.panelMultiempresa.showNewCompanyModal,
    companyName: state.company.companyName,
    arl: state.company.arl,
    claseArl: state.company.claseArl,
    cajaCompensacion: state.company.cajaCompensacion,
    phoneNumber: state.company.phoneNumber,
    cellphoneNumber: state.company.cellphoneNumber,
    identificationType: state.company.identificationType,
    identificationNumber: state.company.identificationNumber,
    nit: state.company.identificationNumber,
    verificationDigit: state.company.verificationDigit,
    economicActivity: state.company.economicActivity,
    address: state.company.address,
    province: state.company.province,
    city: state.company.city,
    adminName: state.currentUser.name,
    adminEmail: state.currentUser.email,
    managerName: state.company.legalRepresentative.name,
    managerEmail: state.company.legalRepresentative.email,
    managerDocumentType: state.company.legalRepresentative.identificationType,
    managerDocumentNumber:
      state.company.legalRepresentative.identificationNumber,
    managerPhoneNumber: state.company.legalRepresentative.phoneNumber,
    managerCellphoneNumber: state.company.legalRepresentative.cellphoneNumber,
    companyContactName: state.company.businessContact.name,
    companyContactEmail: state.company.businessContact.email,
    companyContactDocumentType:
      state.company.businessContact.identificationType,
    companyContactDocumentNumber:
      state.company.businessContact.identificationNumber,
    companyContactPhoneNumber: state.company.businessContact.phoneNumber,
    companyContactcellphoneNumber:
      state.company.businessContact.cellphoneNumber,
    exentoPagoParafiscales: state.company.exentoParafiscales,
    beneficiadoLey590Del2000: state.company.beneficiadoLey590,
    fechaConstitucion: state.company.constitucionDate,
    claseAportante: state.company.claseAportante,
    companyRequiredFields: state.company.companyRequiredFields,
    legalRepresentativeRequiredFields:
      state.company.legalRepresentativeRequiredFields,
    businessContactRequiredFields: state.company.businessContactRequiredFields,
    credentialsId: state.panelMultiempresa.credentialsId,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    currentUserAction: (actionType, value) =>
      dispatch(currentUserAction(actionType, value)),
    panelMultiempresaAction: (actionType, value) =>
      dispatch(panelMultiempresaAction(actionType, value)),
    companyAction: (actionType, value) =>
      dispatch(companyAction(actionType, value)),
    cotizantesAction: (actionType, value) =>
      dispatch(cotizantesAction(actionType, value)),
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
    prePlanillaAction: (actionType, value) =>
      dispatch(prePlanillaAction(actionType, value)),
    credentialsModalAction: (actionType, value) =>
      dispatch(credentialsModalAction(actionType, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PanelMultiempresa);
