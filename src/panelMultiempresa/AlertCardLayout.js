import React from 'react';
import {
    Typography,
    ButtonBase,
    Avatar,
    Card
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
    ALERT_CARD_MESSAGE,
    PLANILLA_VALUE,
    SHOW_PLANILLA
} from './Constants';

const AlertCardLayout = (props) => {

    const classes = useStyles();

    return (
        <Card
            className={classes.cardContainer}
            variant='outlined'
        >
            <div className={classes.infoContainer}>
                <Avatar className={classes.avatar}>
                    <Typography className={classes.dayText}>
                        {'05'}
                    </Typography>
                    <Typography className={classes.monthText}>
                        {'Junio'}
                    </Typography>
                </Avatar>
                <div className={classes.alertTextContainer}>
                    <Typography className={classes.messageText}>
                        {ALERT_CARD_MESSAGE}
                    </Typography>
                    <div className={classes.companyNameContainer}>
                        <Typography className={classes.companyName} noWrap>
                            {'Grupo Quince Minutos de Fama SAS'}
                        </Typography>
                    </div>
                </div>
            </div>
            <div className={classes.valueContainer}>
                <Typography className={classes.valueTextOne}>
                    {PLANILLA_VALUE}
                    <Typography
                        className={classes.valueTextTwo}
                        component='span'
                    >
                        {`$ ${'1.000.000.000'}`}
                    </Typography>
                </Typography>
                <ButtonBase className={classes.showPlanillaButton}>
                    <Typography className={classes.showPlanillaText}>
                        {SHOW_PLANILLA}
                    </Typography>
                </ButtonBase>
            </div>
        </Card>
    );
}

const useStyles = makeStyles({
    cardContainer: {
        width: '31%',
        height: '100%',
        display: 'flex',
        backgroundColor: '#FFFFFF',
        border: '1px solid #5E35B1',
        flexDirection: 'column',
        boxShadow: '0px 0px 6px #6060B229'
    },
    infoContainer: {
        display: 'flex',
        flex: 1,
        padding: '4% 6% 0% 6%',
        alignItems: 'center'
    },
    avatar: {
        display: 'flex',
        width: 60,
        backgroundColor: '#FFFFFF',
        border: '1px solid #5E35B1',
        height: 60,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    dayText: {
        color: '#5E35B1',
        margin: 0,
        fontSize: '1.3em',
        lineHeight: 1.3
    },
    monthText: {
        color: '#5E35B1',
        margin: 0,
        fontSize: '0.5em',
        lineHeight: 0
    },
    alertTextContainer: {
        display: 'flex',
        flex: 1,
        height: '100%',
        paddingLeft: '4%',
        flexDirection: 'column'
    },
    messageText: {
        textAlign: 'left',
        fontSize: '1.3em',
        color: '#5E35B1',
        fontWeight: 'bold',
        lineHeight: 1.2
    },
    companyNameContainer: {
        display: 'flex',
        width: '90%',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    companyName: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: '1.1',
        color: '#263238'
    },
    valueContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '4% 6% 4% 6%'
    },
    valueTextOne: {
        fontSize: '1em',
        flexDirection: 'row',
        display: 'flex',
        color: '#707070',
        fontWeight: 'bold'
    },
    valueTextTwo: {
        fontSize: '1em',
        paddingLeft: 5,
        color: '#707070'
    },
    showPlanillaButton: {
        backgroundColor: '#5E35B1',
        textTransform: 'none',
        width: '31%',
        borderRadius: 4,
        padding: '5px 0px 5px 0px',
        zIndex: 0
    },
    showPlanillaText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: '1em'
    }
});

export default AlertCardLayout;
