import React from 'react';
import {
    Typography,
    ButtonBase,
    Card
} from '@material-ui/core';
import PlayCircleFilledTwoToneIcon from '@material-ui/icons/PlayCircleFilledTwoTone';
import { makeStyles } from '@material-ui/core/styles';
import {
    HELP_BUTTON
} from './Constants';

const InfoCardLayout = (props) => {

    const classes = useStyles();

    return (
        <Card
            className={classes.cardContainer}
            variant='outlined'
        >
            <div className={classes.titleContainer}>
                <Typography className={classes.titleText}>
                    {'¿Cómo entender la aplicación del\nnuevo decreto 2388 de 2016?'}
                </Typography>
            </div>
            <div className={classes.messageContainer}>
                <Typography className={classes.messageText}>
                    {'Con este decreto se actualizó la forma de gestionar\nel reporte de novedades para todos los tipo de cotizantes.'}
                </Typography>
                <ButtonBase
                    className={classes.messageButton}
                    disableRipple
                >
                    <PlayCircleFilledTwoToneIcon className={classes.messageButtonIcon} />
                    <Typography className={classes.messageButtonText}>
                        {HELP_BUTTON}
                    </Typography>
                </ButtonBase>
            </div>
        </Card>
    );
}

const useStyles = makeStyles({
    cardContainer: {
        width: '31%',
        height: '100%',
        display: 'flex',
        backgroundColor: '#37474F',
        flexDirection: 'column',
        boxShadow: '0px 0px 6px #6060B229'
    },
    titleContainer: {
        display: 'flex',
        flex: 1,
        padding: '4% 6% 0% 6%',
        alignItems: 'center'
    },
    titleText: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: '1.3em',
        color: '#FFFFFF',
        lineHeight: 1.2
    },
    messageContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        padding: '0% 6% 4% 6%',
        flex: 1,
        flexDirection: 'column'
    },
    messageText: {
        color: '#FFFFFF',
        fontSize: '0.8em',
        textAlign: 'left',
        lineHeight: 1
    },
    messageButton: {
        display: 'flex',
        alignItems: 'center',
        borderRadius: 7,
        width: '50%',
        justifyContent: 'flex-start'
    },
    messageButtonIcon: {
        fontSize: '1.3em',
        color: '#FFFFFF',
        marginRight: 5
    },
    messageButtonText: {
        color: '#FFFFFF',
        fontSize: '0.9em'
    }
});

export default InfoCardLayout;
