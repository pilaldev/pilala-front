import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
    Popper,
    Paper,
    ClickAwayListener,
    Typography,
    ButtonBase,
    Grid,
    Avatar,
    Dialog,
    DialogContent,
    Button,
    CircularProgress,
    TableHead,
    TableRow,
    TableCell
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AvatarAportanteLayout from './../components/AvatarAportanteLayout';
import BusinessTwoToneIcon from '@material-ui/icons/BusinessTwoTone';
import CompanyCardLayout from './../company/CompanyCardLayout';
import BusinessContactCardLayout from './../company/BusinessContactCardLayout';
import LegalRepresentativeCardLayout from './../company/LegalRepresentativeCardLayout';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';
// import AlertCardLayout from './AlertCardLayout';
// import InfoCardLayout from './InfoCardLayout';
import {
    SUCURSALES,
    MAIN_COMPANY,
    PLANILLAS_TITLE,
    CREATE_COMPANY
} from './Constants';
import InfoTable from '../components/InfoTable';
import HelperFunctions from '../utils/HelperFunctions';

const PanelMultiempresaLayout = (props) => {

    const classes = useStyles();

    const [sucursalesAnchor, setSucursalesAnchor] = useState(null);
    const [activeIndex, setActiveIndex] = useState(null);
    const currentUserName = useSelector(state => state.currentUser.name);
    const multiPlanillas = useSelector(state => state.panelMultiempresa.multiPlanillas);
    const featureFlags = useSelector(state => state.currentUser.featureFlags);

    const handleOpenSucursalesPopper = (index, event) => {
        setActiveIndex(index);
        setSucursalesAnchor(event.currentTarget);
    }

    const handleCloseSucursalesPopper = () => {
        setActiveIndex(null);
        setSucursalesAnchor(null);
    };

    const TableHeader = () => (
        <TableHead>
            <TableRow>
                <TableCell className={classes.headerTitle}>
                    Aportante
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    Fecha
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    N° Planilla
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    Periodo
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    Tipo
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    Valor
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    Estado
                </TableCell>
                <TableCell />
            </TableRow>
        </TableHead>
    );

    const RowLayout = ({ row, index }) => {

        return (
            <TableRow
                style={{
                    backgroundColor: index % 2 === 0 ? '#ECEFF1' : '#FFFFFF'
                }}
            >
                <TableCell>
                    <Typography className={classes.planillaAportante}>
                        {row.company}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <Typography className={classes.planillaDate}>
                        {row.date}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <Typography className={classes.planillaNumber}>
                        {row.idPlanilla}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <Typography className={classes.planillaPeriod}>
                        {row.period}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <Typography className={classes.planillaType}>
                        {'Empleados'}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <Typography className={classes.planillaValue}>
                        {`$${HelperFunctions.formatStringNumber(row.value)}`}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <div
                        className={classes.planillaStateContainer}
                        style={{
                            backgroundColor: row.state === 'paid' ? '#95989A26' : row.state === 'pay' ? '#5E35B126' : row.state === 'pending' ? '#2962FF26' : '#FFFFFF',
                            borderColor: row.state === 'paid' ? '#95989A' : row.state === 'pay' ? '#5E35B1' : row.state === 'pending' ? '#2962FF' : '#FFFFFF'
                        }}
                    >
                        <Typography
                            className={classes.planillaStateText}
                            style={{
                                color: row.state === 'paid' ? '#95989A' : row.state === 'pay' ? '#5E35B1' : row.state === 'pending' ? '#2962FF' : '#FFFFFF',
                            }}
                        >
                            {
                                row.state === 'paid' ?
                                    'Pagada' :
                                    row.state === 'pay' ?
                                        'Pagar' :
                                        row.state === 'pending' ?
                                            'Verificando' : ''
                            }
                        </Typography>
                    </div>
                </TableCell>
                <TableCell align='center'>
                    <ButtonBase disableRipple onClick={props.openPlanillaDetail.bind(this, row.idPlanilla)}>
                        <Typography className={classes.planillaDetail}>
                            {'Ver planilla'}
                        </Typography>
                    </ButtonBase>
                </TableCell>
            </TableRow>
        );
    }

    return (
        <div
            className={classes.container}
        >
            <Typography className={classes.panelTitle}>
                {`¡Hola ${currentUserName.split(' ')[0]}!`}
            </Typography>
            <div className={classes.companiesContainer}>
                {
                    featureFlags.ccCompany &&
                    <AvatarAportanteLayout
                        name='+ Aportante'
                        // action={props.openNewCompanyModal}
                        action={props.openCheckCompanyModal}
                        index={-1}
                    />
                }
                <div className={classes.companiesSecondContainer}>
                    {
                        props.companies.map((item, index) => (
                            <div key={index}>
                                <div
                                    onMouseEnter={handleOpenSucursalesPopper.bind(this, index)}
                                >
                                    <AvatarAportanteLayout
                                        name={item.companyName}
                                        action={props.selectCompany.bind(this, item.idCompany)}
                                        key={index}
                                        index={index}
                                    />
                                </div>
                                <Popper
                                    open={activeIndex === index}
                                    disablePortal
                                    placement='bottom'
                                    anchorEl={sucursalesAnchor}
                                    className={classes.sucursalesPopper}
                                >
                                    <Paper className={classes.sucursalesPaper}>
                                        <ClickAwayListener onClickAway={handleCloseSucursalesPopper}>
                                            <div>
                                                <Typography className={classes.sucursalesTitle}>
                                                    {SUCURSALES}
                                                </Typography>
                                                <div className={classes.cardSucursales}>
                                                    <ButtonBase
                                                        className={classes.cardSucursalButton}
                                                        disableRipple
                                                        onClick={props.selectCompany.bind(this, item.idCompany)}
                                                    >
                                                        <Avatar className={classes.avatar}>
                                                            <BusinessTwoToneIcon className={classes.avatarIcon} />
                                                        </Avatar>
                                                        <div className={classes.cardNamesContainer}>
                                                            <Typography className={classes.cardSucursalName}>
                                                                {MAIN_COMPANY}
                                                            </Typography>
                                                            <div className={classes.cardCompanyNameContainer}>
                                                                <Typography className={classes.cardCompanyName}>
                                                                    {item.companyName}
                                                                </Typography>
                                                            </div>
                                                        </div>
                                                    </ButtonBase>
                                                    {
                                                        item.sucursales.map((sucursalItem, sucursalIndex) => (
                                                            <ButtonBase
                                                                key={sucursalIndex}
                                                                className={classes.cardSucursalButton}
                                                                disableRipple
                                                                onClick={props.selectCompany.bind(this, sucursalItem.idSucursal)}
                                                            >
                                                                <Avatar className={classes.avatar}>
                                                                    <BusinessTwoToneIcon className={classes.avatarIcon} />
                                                                </Avatar>
                                                                <div className={classes.cardNamesContainer}>
                                                                    <Typography className={classes.cardSucursalName}>
                                                                        {sucursalItem.sucursalName}
                                                                    </Typography>
                                                                    <div className={classes.cardCompanyNameContainer}>
                                                                        <Typography className={classes.cardCompanyName}>
                                                                            {item.companyName}
                                                                        </Typography>
                                                                    </div>
                                                                </div>
                                                            </ButtonBase>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        </ClickAwayListener>
                                    </Paper>
                                </Popper>
                            </div>
                        ))
                    }
                </div>
            </div>
            {/* <div className={classes.cardsContainer}>
                <AlertCardLayout />
                <AlertCardLayout />
                <InfoCardLayout />
            </div> */}
            <Typography className={classes.planillasTitle}>
                {PLANILLAS_TITLE}
            </Typography>
            <div className={classes.planillasContainer}>
                <InfoTable
                    data={multiPlanillas}
                    RowItem={(props) =>
                        <RowLayout
                            {...props}
                        />
                    }
                    enablePagination={true}
                    TableHeader={TableHeader}
                />
            </div>
            <Dialog
                open={props.showNewCompanyModal}
                maxWidth='xl'
                PaperProps={{
                    className: classes.paperContainer
                }}
            >
                <DialogContent className={classes.dialogContent}>
                    <div className={classes.closeButtonContainer}>
                        <ButtonBase disableRipple onClick={props.openNewCompanyModal}>
                            <CloseRoundedIcon className={classes.closeButtonIcon} />
                        </ButtonBase>
                    </div>
                    <CompanyCardLayout
                        createdCompany={false}
                        disableInputs={false}
                        editCompany={false}
                    />
                    <BusinessContactCardLayout
                        createdCompany={false}
                        editBusinessContact={false}
                    />
                    <LegalRepresentativeCardLayout
                        createdCompany={false}
                        editLegalRepresentative={false}
                    />
                    <Grid
                        container
                        direction='column'
                        justify='center'
                        alignItems='flex-end'
                        className={classes.buttonContainer}
                    >
                        <Button
                            variant='contained'
                            className={classes.continueButton}
                            endIcon={
                                props.disableCompanyButton ? null : <ArrowForwardRoundedIcon className={classes.continueIcon} />
                            }
                            onClick={props.createNewCompany}
                        >
                            {
                                props.disableCompanyButton ?
                                    <CircularProgress size={27} className={classes.spinner} />
                                    :
                                    <Typography
                                        variant='subtitle1'
                                        className={classes.continueText}
                                    >
                                        {CREATE_COMPANY}
                                    </Typography>
                            }
                        </Button>
                    </Grid>
                </DialogContent>
            </Dialog>
        </div>
    );
}

const useStyles = makeStyles({
    container: {
        padding: '3% 2% 3% 3%',
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    panelTitle: {
        fontSize: '1.7rem',
        fontWeight: 'bold',
        color: '#000000'
    },
    companiesContainer: {
        backgroundColor: '#ECEFF1',
        width: '100%',
        borderRadius: 7,
        marginTop: '1.5%',
        display: 'flex',
        padding: '2% 0.5% 2% 0.5%',
        boxShadow: '0px 3px 6px #00000029'
    },
    avatar: {
        width: 60,
        height: 60,
        backgroundColor: '#FFFFFF',
        border: '1px solid #707070'
    },
    avatarIcon: {
        color: '#6060B2',
        fontSize: '1em'
    },
    cardCompanyName: {
        textAlign: 'left',
        fontSize: '0.7em',
        color: '#263238'
    },
    cardCompanyNameContainer: {
        border: '1px solid #263238',
        borderRadius: 9,
        padding: '0px 8px 0px 8px',
        backgroundColor: '#26323826'
    },
    cardNamesContainer: {
        display: 'flex',
        flexDirection: 'column',
        marginLeft: 8
    },
    cardSucursalName: {
        fontSize: '1em',
        fontWeight: 'bold',
        color: '#263238',
        textAlign: 'left'
    },
    cardSucursalButton: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: '6%'
    },
    cardSucursales: {
        display: 'flex',
        flexDirection: 'column'
    },
    sucursalesPopper: {
        width: '20%',
        display: 'flex',
        alignItems: 'left',
        flexDirection: 'column',
        zIndex: 400000
    },
    sucursalesTitle: {
        textAlign: 'left',
        marginBottom: 12,
        fontWeight: 'bold',
        color: '#263238'
    },
    sucursalesPaper: {
        padding: '6% 9% 0% 9%'
    },
    companiesSecondContainer: {
        display: 'flex',
        flex: 1
    },
    paperContainer: {
        backgroundColor: '#FFFFFF',
        display: 'flex',
        flex: 1
    },
    dialogContent: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    continueButton: {
        borderRadius: 100,
        width: 230,
        marginRight: 60,
        background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
        textTransform: 'none'
    },
    continueText: {
        color: 'white',
        fontWeight: 'bold'
    },
    continueIcon: {
        color: '#FFFFFF'
    },
    buttonContainer: {
        marginBottom: 50
    },
    closeButtonContainer: {
        display: 'flex',
        alignSelf: 'flex-end',
        marginBottom: 20
    },
    closeButtonIcon: {
        fontSize: '2.5em',
        color: '#263238'
    },
    cardsContainer: {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        margin: '2% 0% 3% 0%'
    },
    divider: {
        margin: '0% 3.5% 0% 2%',
        backgroundColor: '#707070'
    },
    planillasContainer: {
        width: '100%',
        height: '40%'
    },
    spinner: {
        color: '#FFFFFF'
    },
    planillasTitle: {
        fontSize: '1.5rem',
        fontWeight: 'bold',
        color: '#263238',
        textAlign: 'left',
        marginBottom: '1rem',
        marginTop: '3rem'
    },
    headerTitle: {
        fontSize: '1rem',
        fontWeight: 'bold',
        color: '#263238'
    },
    planillaAportante: {
        fontWeight: 'bold',
        color: '#455A64',
        fontSize: '0.9rem',
        textAlign: 'left'
    },
    planillaDate: {
        color: '#455A64',
        fontSize: '0.9rem'
    },
    planillaNumber: {
        color: '#455A64',
        fontSize: '0.9rem'
    },
    planillaPeriod: {
        fontWeight: 'bold',
        color: '#455A64',
        fontSize: '0.9rem',
        textTransform: 'capitalize'
    },
    planillaType: {
        color: '#455A64',
        fontSize: '0.9rem'
    },
    planillaValue: {
        color: '#455A64',
        fontSize: '0.9rem'
    },
    planillaStateContainer: {
        padding: '0.1em 0.5em',
        borderRadius: 100,
        border: '2px solid',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    planillaStateText: {
        fontSize: '0.9rem'
    },
    planillaDetail: {
        color: '#2962FF',
        fontSize: '0.9rem'
    }
});

export default PanelMultiempresaLayout;
