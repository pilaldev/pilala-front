export const FAILED = 'FAILED';
export const SUCCESS = 'SUCCESS';
export const REQUEST_MISDATOS_API = 'https://us-central1-pilala-develop.cloudfunctions.net/requestMisDatos';
export const REQUEST_COMPANIES_DATA = 'https://us-central1-pilala-develop.cloudfunctions.net/requestCompaniesData';
export const REQUEST_COTIZANTES_DATA = 'https://us-central1-pilala-develop.cloudfunctions.net/requestCotizantesData';
export const LOADER_MESSAGE = 'Estamos cargando tu mejor experiencia PILA';
