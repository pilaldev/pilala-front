import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import {
  companyAction,
  resourcesAction,
  currentUserAction,
  cotizantesInfoAction,
  prePlanillaAction,
  navigationAction,
} from '../redux/Actions';
import {
  COMPANY,
  RESOURCES,
  CURRENT_USER,
  COTIZANTE_INFO,
  PRE_PLANILLA,
  NAVIGATION,
} from '../redux/ActionTypes';
import { getResources } from '../api/GetResources';
import HelperFunctions from '../utils/HelperFunctions';
import { requestPreplanillaData } from '../pila/pilaTable/api/RequestPreplanillaData';
import { LOADER_MESSAGE } from './Constants';
import { MatchNameFields } from '../utils/MatchNameFields';
import { GetCompaniesData, GetCotizantesData, GetUserData } from './api';
import { Loader } from '../components';

const FetchData = () => {
  const dispatch = useDispatch();
  const locationData = useLocation();
  const history = useHistory();
  const [requestStatus, setRequestStatus] = useState({
    USER_DATA: 'PENDING',
    COMPANIES: 'PENDING',
    COTIZANTES: 'PENDING',
    RESOURCES: 'PENDING',
  });
  const userData = useRef({});
  const currentUserEmail = useSelector((state) => state.currentUser.email);

  const saveUserData = useCallback(async () => {
    const emailFromLocation = locationData.state?.email;

    const result = await GetUserData(
      emailFromLocation ?? currentUserEmail ?? undefined
    );
    const data = {
      name: result.data.data?.name ?? 'User',
      email: result.data.data?.email ?? '',
      uid: result.data.data?.uid ?? '',
      documentNumber: result.data.data?.documentNumber ?? '',
      documentType: result.data.data?.documentType ?? '',
      firstSignIn: result.data.data?.firstSignIn ?? null,
      createdCompany: result.data.data?.createdCompany ?? null,
      phoneNumber: result.data.data?.phoneNumber ?? '',
      companies: result.data.data?.companies ?? [],
      activeCompany: result.data.data?.companies[0]?.id ?? null,
      accountManager: result.data.data?.accountManager ?? [],
    };
    userData.current = data;
    setRequestStatus((prevState) => ({
      ...prevState,
      USER_DATA: result.status,
    }));
    dispatch(currentUserAction(CURRENT_USER.CURRENT_USER_SAVE_DATA, data));
  }, [currentUserEmail, dispatch, locationData.state]);

  const setBusinessContactData = useCallback(() => {
    if (!userData.current?.createdCompany && userData.current?.uid) {
      const nameData = MatchNameFields(userData.current?.name);
      const newName = {
        display: nameData.displayName,
        firstName: nameData.firstName,
        secondName: nameData.secondName,
        firstSurname: nameData.firstSurname,
        secondSurname: nameData.secondSurname,
      };
      const businessContactData = {
        name: newName,
        email: userData.current?.email,
        province: '',
        city: '',
        identificationType: {
          display: userData.current?.documentType,
          code: userData.current?.documentType,
        },
        identificationNumber: userData.current?.documentNumber,
        phoneNumber: '',
        cellphoneNumber: userData.current?.phoneNumber,
      };

      dispatch(
        companyAction(COMPANY.COMPANY_SAVE_BC_ALL_DATA, businessContactData)
      );
    }
  }, [dispatch]);

  useEffect(() => {
    if (requestStatus.USER_DATA === 'PENDING') {
      saveUserData();
      setBusinessContactData();
    }
  }, [setBusinessContactData, requestStatus.USER_DATA, saveUserData]);

  const requestLists = useCallback(() => {
    getResources().then((result) => {
      if (result.status === 'SUCCESS') {
        dispatch(
          resourcesAction(RESOURCES.RESOURCES_SAVE_LOCATION_DATA, {
            cities: HelperFunctions.formatCitiesArray(result.cities),
            provinces: HelperFunctions.formatProvincesArray(result.provinces),
            ciiu: HelperFunctions.formatCIIUArray(result.codeList),
            arl: HelperFunctions.formatARLArray(result.arl),
            ccf: HelperFunctions.formatCCFArray(result.ccf),
            afp: HelperFunctions.formatAFPArray(result.afp),
            eps: HelperFunctions.formatEPSArray(result.eps),
            smlmv: result.SMLMV,
            camaras: result.camaras,
            clasesArl: HelperFunctions.formatClaseARLArray(result.clases),
            integralSalary: result.integralSalary,
            subtiposCotizantes: result.subtiposCotizantes,
            tiposCotizantes: result.tiposCotizantes,
            tipoVial: result.tipoVial,
            tiposCuadrante: result.tiposCuadrante,
            clasesAportante: result.clasesAportante,
            identificacionAportante: result.identificacionAportante,
          })
        );
        setRequestStatus((prevState) => ({
          ...prevState,
          RESOURCES: result.status,
        }));
      } else {
        setRequestStatus((prevState) => ({
          ...prevState,
          RESOURCES: 'FAILED',
        }));
      }
    });
  }, [dispatch]);

  const requestCotizantes = useCallback(() => {
    GetCotizantesData({ companiesArray: userData.current?.companies }).then(
      (result) => {
        if (result.result === 'SUCCESS') {
          dispatch(
            cotizantesInfoAction(
              COTIZANTE_INFO.COTIZANTE_INFO_SET_COTIZANTES_DATA,
              result.newData
            )
          );
          setRequestStatus((prevState) => ({
            ...prevState,
            COTIZANTES: result.result,
          }));
        } else {
          setRequestStatus((prevState) => ({
            ...prevState,
            COTIZANTES: 'FAILED',
          }));
        }
      }
    );
  }, [dispatch]);

  const getMainCompanyData = useCallback(
    (nit, companyId) => {
      requestPreplanillaData({
        nit,
        companyId,
        specificPlanilla: false,
        specificInfo: null,
      }).then((result) => {
        if (result.result === 'SUCCESS') {
          dispatch(
            prePlanillaAction(
              PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA,
              result.data
            )
          );
        }
      });
    },
    [dispatch]
  );

  const requestCompanies = useCallback(() => {
    GetCompaniesData({ companiesArray: userData.current?.companies })
      .then((result) => {
        if (result.result === 'SUCCESS' && Object.keys(result.data).length) {
          const firstCompanyId = userData.current?.companies[0].id;
          const companyData = {
            companyName: result.data[firstCompanyId].companyName,
            economicActivity: result.data[firstCompanyId].economicActivity,
            province: result.data[firstCompanyId].location.province,
            city: result.data[firstCompanyId].location.city,
            phoneNumber: result.data[firstCompanyId].phoneNumber,
            cellphoneNumber: result.data[firstCompanyId].mobileNumber,
            identificationType: result.data[firstCompanyId].identificationType,
            identificationNumber: result.data[firstCompanyId].nit,
            verificationDigit: result.data[firstCompanyId].verificationDigit,
            arl: result.data[firstCompanyId].arl,
            cajaCompensacion: result.data[firstCompanyId].cajaCompensacion,
            claseArl: result.data[firstCompanyId].claseArl,
            address: result.data[firstCompanyId].location.address,
            claseAportante:
              result.data[firstCompanyId].clasificacionAportanteCodigo,
            exentoParafiscales:
              result.data[firstCompanyId].exentoPagoParafiscales,
            beneficiadoLey590:
              result.data[firstCompanyId].beneficiadoLey590Del2000,
            constitucionDate: result.data[firstCompanyId].fechaConstitucion,
            obligacionSena: !result.data[firstCompanyId].exentoPagoParafiscales,
            obligacionICBF: !result.data[firstCompanyId].exentoPagoParafiscales,
          };

          const LRData = {
            name: result.data[firstCompanyId].legalRepresentative.name,
            email: result.data[firstCompanyId].legalRepresentative.email,
            identificationType:
              result.data[firstCompanyId].legalRepresentative.documentType,
            identificationNumber:
              result.data[firstCompanyId].legalRepresentative.documentNumber,
            phoneNumber: result.data[firstCompanyId].legalRepresentative.phone,
            cellphoneNumber:
              result.data[firstCompanyId].legalRepresentative.mobileNumber,
          };

          const BCData = {
            name: result.data[firstCompanyId].companyContact.name,
            email: result.data[firstCompanyId].companyContact.email,
            identificationType:
              result.data[firstCompanyId].companyContact.documentType,
            identificationNumber:
              result.data[firstCompanyId].companyContact.documentNumber,
            phoneNumber: result.data[firstCompanyId].companyContact.phone,
            cellphoneNumber:
              result.data[firstCompanyId].companyContact.mobileNumber,
          };

          dispatch(companyAction(COMPANY.COMPANY_SAVE_ALL_DATA, companyData));
          dispatch(companyAction(COMPANY.COMPANY_SAVE_LR_ALL_DATA, LRData));
          dispatch(companyAction(COMPANY.COMPANY_SAVE_BC_ALL_DATA, BCData));

          getMainCompanyData(
            result.data[firstCompanyId].nit,
            result.data[firstCompanyId].idCompany
          );

          dispatch(
            currentUserAction(
              CURRENT_USER.CURRENT_USER_SAVE_USER_COMPANIES_INFO,
              result.data
            )
          );
          dispatch(
            currentUserAction(
              CURRENT_USER.CURRENT_USER_SAVE_COMPANIES_PERIODOS_INFO,
              result.periodosInfo
            )
          );
          dispatch(
            currentUserAction(
              CURRENT_USER.CURRENT_USER_SAVE_COMPANIES_PREPLANILLA_PERIODOS_INFO,
              result.prePlanillaPeriodos
            )
          );
          setRequestStatus((prevState) => ({
            ...prevState,
            COMPANIES: 'SUCCESS',
          }));
        } else {
          setRequestStatus((prevState) => ({
            ...prevState,
            COMPANIES: 'FAILED',
          }));
        }
      })
      .catch(() => {
        setRequestStatus((prevState) => ({
          ...prevState,
          COMPANIES: 'FAILED',
        }));
      });
  }, [dispatch, getMainCompanyData]);

  const getRemoteData = useCallback(() => {
    if (
      userData.current?.createdCompany === true &&
      userData.current?.uid !== ''
    ) {
      requestLists();
      requestCotizantes();
      requestCompanies();
    } else if (userData.current?.uid !== '') {
      requestLists();
    }
  }, [requestCompanies, requestCotizantes, requestLists]);

  useEffect(() => {
    if (requestStatus.USER_DATA === 'SUCCESS') {
      getRemoteData();
    }
  }, [getRemoteData, requestStatus.USER_DATA]);

  useEffect(() => {
    if (requestStatus.USER_DATA === 'FAILED') {
      console.log('Ocurrio un error en la solicitud de info de usuario');
    }

    if (
      requestStatus.USER_DATA !== 'PENDING' &&
      requestStatus.RESOURCES !== 'PENDING' &&
      requestStatus.COMPANIES !== 'PENDING' &&
      requestStatus.COTIZANTES !== 'PENDING'
    ) {
      if (
        requestStatus.USER_DATA === 'SUCCESS' &&
        requestStatus.RESOURCES === 'SUCCESS' &&
        requestStatus.COMPANIES === 'SUCCESS' &&
        requestStatus.COTIZANTES === 'SUCCESS'
      ) {
        if (userData.current?.companies.length === 1) {
          history.replace(`/${userData.current?.uid}/console/liquidarPila/1`);
          dispatch(
            navigationAction(
              NAVIGATION.NAVIGATION_ACTIVE_PANEL_MULTIEMPRESA,
              false
            )
          );
        } else {
          history.replace(
            `/${userData.current?.uid}/console/multicompanyPanel`
          );
          dispatch(
            navigationAction(
              NAVIGATION.NAVIGATION_ACTIVE_PANEL_MULTIEMPRESA,
              true
            )
          );
        }
      } else {
        // TODO: Plantear una redirección o re-ejecución de las solicitudes en caso de que alguna falle cuando ya se tienen compañias creadas
        console.log('Ocurrio un error en las solicitudes iniciales 1');
      }
    }

    if (
      requestStatus.USER_DATA !== 'PENDING' &&
      requestStatus.RESOURCES !== 'PENDING' &&
      requestStatus.COMPANIES === 'PENDING' &&
      requestStatus.COTIZANTES === 'PENDING'
    ) {
      if (
        requestStatus.USER_DATA === 'SUCCESS' &&
        requestStatus.RESOURCES === 'SUCCESS'
      ) {
        if (userData.current?.companies.length === 0) {
          history.replace(`/${userData.current?.uid}/console/company`);
          dispatch(
            navigationAction(
              NAVIGATION.NAVIGATION_ACTIVE_PANEL_MULTIEMPRESA,
              false
            )
          );
        }
      } else {
        // TODO: Plantear una redirección o re-ejecución de las solicitudes en caso de que alguna falle cuando no se ha creado la 1 compañia
        console.log('Ocurrio un error en las solicitudes iniciales 2');
      }
    }
  }, [dispatch, history, requestStatus]);

  return <Loader message={LOADER_MESSAGE} />;
};

export default FetchData;
