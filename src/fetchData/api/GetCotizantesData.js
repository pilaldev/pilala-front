import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default async (data) => {
	return new Promise(async (resolve, reject) => {
		await fetch(Endpoints.requestCotizantesData, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error consultando la información de cotizantes',
					error,
				});
			});
	});
};
