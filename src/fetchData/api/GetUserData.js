import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default async (email) => {
	let result = {};

	const requestOptions = {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
		},
	};

	const url = `${Endpoints.getUserData}${email}`;

	await fetch(url, requestOptions)
		.then(async (response) => {
			if (response.status === 200) {
				result.status = 'SUCCESS';
				result.data = await response.json();
			} else {
				result.status = 'FAILED';
				result.data = await response.json();
			}
		})
		.catch((error) => {
			console.log('Error fetch de obtener info de usuario', error);
		});
	return result;
};
