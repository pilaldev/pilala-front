import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
    Dialog,
    Typography,
    DialogContent,
    Divider,
    IconButton,
    Button
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { UploadFile } from '../components';

const UploadPILAFile = (props) => {
    const classes = useStyles();

    return (
        <Dialog
            open={true}
            maxWidth='sm'
            PaperProps={{
                className: classes.paperContainer
            }}
        >
            <DialogContent className={classes.dialogContent}>
                <div className={classes.headerContainer}>
                    <Typography className={classes.dialogTitle}>
                        {'Cargar archivo PILA'}
                    </Typography>
                    <IconButton
                        size='small'
                    >
                        <CloseIcon className={classes.closeIcon} />
                    </IconButton>
                </div>
                <Divider className={classes.divider} />
                <div className={classes.uploadFileContainer}>
                    <UploadFile
                        message='Arrastra aquí tu archivo PILA.txt'
                        onUploadFile={(file)=>console.log('archivooooo', file)}
                    />
                </div>
                <div className={classes.buttonContainer}>
                    <Button className={classes.continueButton}>
                        Continuar
                    </Button>
                </div>
            </DialogContent>
        </Dialog>
    );
}

const useStyles = makeStyles((theme) => ({
    paperContainer: {
        backgroundColor: '#FFFFFF',
        display: 'flex',
        flex: 1,
    },
    dialogContent: {
        padding: theme.spacing(0)
    },
    dialogTitle: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '2em',
        color: '#6A32B5'
    },
    divider: {
        margin: '15px 0px 0px 0px',
        backgroundColor: '#707070'
    },
    closeIcon: {
        color: '#6A32B5',
        fontSize: '1.5rem'
    },
    headerContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing(0, 3)
    },
    uploadFileContainer: {
        padding: theme.spacing(5, 5, 0, 5)
    },
    buttonContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: '100%',
        padding: theme.spacing(3, 5)
    },
    continueButton: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        textTransform: 'none',
        marginTop: theme.spacing(2),
        borderRadius: 8,
        '&:hover': {
            backgroundColor: theme.palette.primary[700],
        },
        '&:focus': {
            backgroundColor: theme.palette.primary[600]
        }
    }
}));

export default UploadPILAFile;
