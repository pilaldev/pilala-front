import React from 'react';
import { Grid } from '@material-ui/core';
import CotizantesListLayout from './CotizantesListLayout';
import NewCotizanteLayout from './NewCotizanteLayout';
import { makeStyles } from '@material-ui/core/styles';
import CreateCotizanteOptions from './CreateCotizanteOptions';


const CotizantesLayout = (props) => {

    const classes = useStyles();
    
	return (
	
		<Grid
            container
            direction='column'
            justify='flex-start'
            alignItems='center'
            className={classes.container}
        >
            <CotizantesListLayout
                cotizantes={props.cotizantes}
                filterCotizantesList={props.filterCotizantesList}
                // setShowNewCotizante={props.showCreateCotizanteOptions}
                setShowNewCotizante={props.setShowNewCotizante}
                setDetailData={props.setDetailData}
                goToPila={props.goToPila}
            />
            <NewCotizanteLayout
                showNewCotizante={props.showNewCotizante}
                setShowNewCotizante={props.setShowNewCotizante}
                createCotizanteStepOne={props.createCotizanteStepOne}
                createCotizanteStepTwo={props.createCotizanteStepTwo}
                disableStepOneButton={props.disableStepOneButton}
                disableStepTwoButton={props.disableStepTwoButton}
            />
            <CreateCotizanteOptions
                downloadTemplate={props.getCotizantesTemplate}
            />
		</Grid>
	);
};

const useStyles = makeStyles({
    container: {
        padding: '3% 2% 0% 3%'
    }
});

export default CotizantesLayout;
