import React from 'react';
import { Grid, Typography, ButtonBase, Paper, InputBase, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
	COTIZANTES_LIST,
	ADD_NEW_COTIZANTE,
	// DOWNLOAD_INFO,
	SEARCHBAR_PLACEHOLDER,
	PAY_PILA_BUTTON
} from './Constants';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
// import GetAppRoundedIcon from '@material-ui/icons/GetAppRounded';
import SearchIcon from '@material-ui/icons/Search';
import CotizantesCard from './CotizantesCard';
import { useSelector } from 'react-redux';

const CotizantesListLayout = (props) => {
	const classes = useStyles();
	const featureFlags = useSelector(state => state.currentUser.featureFlags);

	return (
		<Grid container className={classes.container} direction="column">
			<Grid container direction="column" alignItems="flex-start" justify="center">
				<Typography variant="subtitle1" className={classes.panelTitle}>
					{COTIZANTES_LIST}
				</Typography>
			</Grid>
			<Grid
				container
				direction="row"
				justify="flex-start"
				alignItems="center"
				className={classes.optionsContainer}
			>
				<div className={classes.rightOptionsContainer}>
					{
						featureFlags.ccCotizante &&
						<ButtonBase className={classes.newCotizanteOptionContainer} onClick={props.setShowNewCotizante}>
							<Grid container direction="row" justify="flex-start" alignItems="center">
								<AddCircleOutlineRoundedIcon />
								<Typography variant="subtitle2">{ADD_NEW_COTIZANTE}</Typography>
							</Grid>
						</ButtonBase>
					}
					{/* <ButtonBase className={classes.downloadOptionContainer}>
						<Grid container direction="row" justify="flex-start" alignItems="center">
							<GetAppRoundedIcon />
							<Typography variant="subtitle2">{DOWNLOAD_INFO}</Typography>
						</Grid>
					</ButtonBase> */}
				</div>
				<div className={classes.searchBarContainer}>
					<Paper component="form" className={classes.searchBar}>
						<SearchIcon />
						<InputBase
							className={classes.searchBarInput}
							placeholder={SEARCHBAR_PLACEHOLDER}
							onChange={props.filterCotizantesList}
						/>
					</Paper>
				</div>
			</Grid>
			<Grid container className={classes.cotizantesContainer}>
				<Grid container direction="row" justify="flex-start" alignItems="flex-start">
					{props.cotizantes.map((item, index) => (
						<CotizantesCard
							{...item}
							key={item.documentNumber}
							setDetailData={props.setDetailData}
							index={index}
						/>
					))}
				</Grid>
				<Grid
					container
					direction="row"
					justify="center"
					alignItems="center"
					className={classes.payButtonContainer}
				>
					<Button
						variant="outlined"
						className={classes.payButton}
						onClick={props.goToPila}
					>
						<Typography variant="subtitle2" className={classes.payButtonText}>{PAY_PILA_BUTTON}</Typography>
					</Button>
				</Grid>
			</Grid>
		</Grid>
	);
};

const useStyles = makeStyles({
	container: {
		flex: 1
	},
	panelTitle: {
		fontSize: '1.8em',
		fontWeight: 'bold',
		color: '#263238',
		marginTop: '-0.3%'
	},
	optionsContainer: {

	},
	rightOptionsContainer: {
		display: 'flex',
		flexDirection: 'row',
		flex: 1
	},
	newCotizanteOptionContainer: {
		margin: '0px 16px 0px 0px'
	},
	downloadOptionContainer: {
		margin: '0px 16px 0px 0px'
	},
	filterOptionContainer: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	filterOptionSelect: {
	},
	searchBarContainer: {
		width: '30%',
	},
	searchBar: {
		padding: '2px 4px',
		display: 'flex',
		alignItems: 'center',
		width: '100%'
	},
	searchBarInput: {
		flex: 1
	},
	cotizantesContainer: {
		flex: 1
	},
	payButtonContainer: {
		paddingTop: 50,
		paddingBottom: 50
	},
	payButton: {
		borderRadius: 100,
		border: '1px solid #6A32B5',
		textTransform: 'none'
	},
	payButtonText: {
		color: '#6A32B5',
		fontWeight: 'bold'
	}
});

export default CotizantesListLayout;
