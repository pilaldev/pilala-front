import React from 'react';
// import PropTypes from 'prop-types';
// import { useSelector, useDispatch } from 'react-redux';
// import { cotizantesInfoAction } from '../redux/Actions';
// import { COTIZANTE_INFO } from '../redux/ActionTypes';
// import { COTIZANTE_STATUS } from '../cotizantes/Constants';

import { makeStyles, createMuiTheme, ThemeProvider, /*withStyles*/ } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import BrandingWatermarkOutlinedIcon from '@material-ui/icons/BrandingWatermarkOutlined';
// import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import CardHeader from '@material-ui/core/CardHeader';
// import Switch from '@material-ui/core/Switch';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#00E676'
		},
		secondary: {
			main: '#FF495A'
		}
	},
	overrides: {
		MuiChip: {
			labelSmall: {
				width: '100%'
			}
		}
	}
});

const useStyles = makeStyles({
	root: {
		width: 250,
		height: 350,
		marginLeft: 20,
		marginTop: 20
	},
	header: {
		padding: 8,
		height: 40
	},
	media: {
		height: 80,
		width: 'auto',
		backgroundSize: 'contain'
	},
	infoContent: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		height: 230,
		paddingBottom: 0,
		paddingTop: 5,
		paddingLeft: 16,
		paddingRight: 16
	},
	chip: {
		width: '70%',
		color: 'white',
		margin: 0
	},
	switch: {},
	typographyLeft: {
		textAlign: 'initial'
	},
	bottomContainer: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'space-evenly',
		// marginBottom: 15,
		// height: 125,
		width: '100%',
		flex: 1
	},
	bottomInfoContainer: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center'
	},
	bottomIcons: {
		marginRight: 10
	}
});

// const IOSSwitch = withStyles((theme) => ({
// 	root: {
// 		width: 40,
// 		height: '100%',
// 		padding: 0,
// 		margin: 0
// 		// margin: theme.spacing(1)
// 	},
// 	switchBase: {
// 		padding: 1,
// 		color: '#FF495A',
// 		'&$checked': {
// 			transform: 'translateX(17px)',
// 			// color: theme.palette.common.white,
// 			'& + $track': {
// 				backgroundColor: 'white',
// 				opacity: 1,
// 				// border: 'none'
// 				borderColor: theme.palette.primary.main,
// 				borderWidth:1.5
// 			}
// 		},
// 		'& + $track': {
// 			backgroundColor: 'white',
// 			opacity: 1,
// 			// border: 'none'
// 			borderColor: theme.palette.secondary.main,
// 			borderWidth:1.5
// 		}
// 	},
// 	thumb: {
// 		width: 22,
// 		height: 22
// 	},
// 	track: {
// 		borderRadius: 22 / 2,
// 		border: `1px solid ${theme.palette.grey[400]}`,
// 		backgroundColor: theme.palette.grey[50],
// 		opacity: 1,
// 		transition: theme.transitions.create([ 'background-color', 'border' ])
// 	},
// 	checked: {},
// 	focusVisible: {}
// }))(({ classes, ...props }) => {
// 	return (
// 		<Switch
// 			focusVisibleClassName={classes.focusVisible}
// 			disableRipple
// 			classes={{
// 				root: classes.root,
// 				switchBase: classes.switchBase,
// 				thumb: classes.thumb,
// 				track: classes.track,
// 				checked: classes.checked
// 			}}
// 			{...props}
// 		/>
// 	);
// });

const CotizantesCard = (props) => {
	const classes = useStyles();

	// const dispatch = useDispatch();

	// const activeCompany = useSelector((state) => state.currentUser.activeCompany);
	// const activeInCompany = useSelector((state) => state.cotizantesInfo[activeCompany][props.documentNumber]['activeInCompany']);

	// const editCotizanteStatus = () => {
	// 	/** COTIZANTE_STATUS = [ { value: false, activeText: 'Inactivo' }, { value: true, activeText: 'Activo' } ] **/
	// 	let data = activeInCompany.value ? COTIZANTE_STATUS[0] : COTIZANTE_STATUS[1];

	// 	dispatch(
	// 		cotizantesInfoAction(COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_STATUS, {
	// 			activeCompany: activeCompany,
	// 			documentNumber: props.documentNumber,
	// 			data
	// 		})
	// 	);
	// };

	return (
		<ThemeProvider theme={theme}>
			<Card className={classes.root}>
				<CardHeader
					className={classes.header}
					action={
						<IconButton aria-label="settings">
							{/* <EditIcon /> */}
							<BorderColorIcon />
						</IconButton>
					}
					title=""
					subheader=""
				/>
				<CardActionArea
					onClick={() => props.setDetailData(props.id, props.tipoCotizante, props.documentNumber)}
					disableRipple={true}
				>
					<CardMedia
						className={classes.media}
						image={
							props.genre.value ? (
								require(`../assets/img/women_one.svg`)
							) : (
								require(`../assets/img/men_one.svg`)
							)
						}
						title="User"
					/>

					<CardContent className={classes.infoContent}>
						<Typography variant="h5" component="h5" style={{ margin: 0 }}>
							{props.name.display}
						</Typography>
						<Typography variant="body2" color="textSecondary" component="p" style={{ margin: 5 }}>
							{props.tipoCotizante.display}
						</Typography>
						{/* <div style={{ display: 'flex', width: '100%', alignItems: 'center', justifyContent: 'center' }}>
							<Chip
								size="small"
								clickable
								color={props.activeInCompany.value ? 'primary' : 'secondary'}
								className={classes.chip}
								label={props.activeInCompany.activeText}
								onDelete={editCotizanteStatus}
								deleteIcon={
									// <Switch
									// 	checked={props.active}
									// 	// onClick={(e) => {
									// 	// 	e.stopPropagation();
									// 	// 	editCotizanteStatus(props.tipoCotizante, props.id);
									// 	// }}
									// 	color={props.active ? 'primary' : 'secondary'}
									// 	className={classes.switch}
									// 	inputProps={{ 'aria-label': 'secondary checkbox' }}

									// 	// checkedIcon={<div>ACTIVE</div>}
									// 	// icon={<div>INACTIVE</div>}
									// />
									<IOSSwitch
										checked={props.activeInCompany.value}
										color={props.activeInCompany.value ? 'primary' : 'secondary'}
										disableRipple={true}
									/>
								}
							/>
						</div> */}

						{/* <Switch
							checked={props.active}
							onClick={(e) => {
								e.stopPropagation();
								editCotizanteStatus(props.tipoCotizante, props.id);
							}}
							color={props.active ? 'primary' : 'secondary'}
							className={classes.switch}
							inputProps={{ 'aria-label': 'secondary checkbox' }}
							
							// checkedIcon={<div>ACTIVE</div>}
							// icon={<div>INACTIVE</div>}
						/> */}

						<Box className={classes.bottomContainer}>
							<Box className={classes.bottomInfoContainer}>
								<LocationOnOutlinedIcon className={classes.bottomIcons} fontSize="small" />
								<Typography
									className={classes.typographyLeft}
									variant="body2"
									color="textSecondary"
									component="p"
								>
									{props.sucursal}
								</Typography>
							</Box>
							<Box className={classes.bottomInfoContainer}>
								<EmailOutlinedIcon className={classes.bottomIcons} fontSize="small" />
								<Typography
									className={classes.typographyLeft}
									variant="body2"
									color="textSecondary"
									component="p"
								>
									{props.email}
								</Typography>
							</Box>

							<Box className={classes.bottomInfoContainer}>
								<BrandingWatermarkOutlinedIcon className={classes.bottomIcons} fontSize="small" />
								<Typography
									className={classes.typographyLeft}
									variant="body2"
									color="textSecondary"
									component="p"
								>
									{props.documentNumber}
								</Typography>
							</Box>
						</Box>
					</CardContent>
				</CardActionArea>
			</Card>
		</ThemeProvider>
	);
};

// CotizantesCard.propTypes = {
// 	active: PropTypes.bool.isRequired,
// 	genre: PropTypes.number.isRequired,
// 	sucursal: PropTypes.string.isRequired,
// 	id: PropTypes.string,
// 	email: PropTypes.string,
// 	firstName: PropTypes.string.isRequired,
// 	secondName: PropTypes.string.isRequired,
// 	firstSurname: PropTypes.string.isRequired,
// 	secondSurname: PropTypes.string.isRequired,
// 	documentNumber: PropTypes.string.isRequired,
// 	tipoCotizante: PropTypes.string.isRequired
// };

export default CotizantesCard;
