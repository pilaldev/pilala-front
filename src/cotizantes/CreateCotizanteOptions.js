import React, { useState } from 'react';
import { Dialog, Divider, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import singleEmployee from './../assets/img/single_employee.svg';
import multiEmployees from './../assets/img/multi_employees.svg';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useDispatch, useSelector } from 'react-redux';
import { cotizantesAction } from '../redux/Actions';
import { COTIZANTES } from '../redux/ActionTypes';
import FlatfileWrapper from './FlatfileWrapper';
import Button from '@bit/pilala.pilalalib.components.button';

const CreateCotizantesOptions = ({ downloadTemplate }) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const showOptionsModal = useSelector((state) => state.cotizantes.showOptionsModal);
	const [showNextModal, setShowNextModal] = useState(false);

	const openNewCotizanteModal = () => {
		setShowNextModal(true);
		onCloseModal();
	};

	const onCloseModal = () => {
		if (showOptionsModal) {
			dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SHOW_OPTIONS_MODAL, false));
		}
	};

	const onExitedModal = () => {
		if (showNextModal) {
			dispatch(cotizantesAction(COTIZANTES.COTIZANTES_OPEN_NEW_COTIZANTE_DIALOG, null));
			setShowNextModal(false);
		}
	};

	const getNewCotizantes = (result) => {
		console.log('res', result);
		if (showOptionsModal) {
			dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SHOW_OPTIONS_MODAL, false));
		}
	};

	return (
		<Dialog
			fullScreen={true}
			open={showOptionsModal}
			className={classes.container}
			PaperProps={{
				className: classes.paperContainer,
			}}
			BackdropProps={{
				className: classes.backdrop,
			}}
			disableBackdropClick={true}
			onExited={onExitedModal}
			disableEnforceFocus={true}
		>
			<Button
				buttonColor='primary'
				className={classes.closeButton}
				startIcon={<ArrowBackIcon />}
				onClick={onCloseModal}
				variant='text'
			>
				Volver
			</Button>
			<div className={classes.contentContainer}>
				<Paper className={classes.card}>
					<Typography className={classes.cardOneTitle}>Crea un cotizante</Typography>
					<Divider className={classes.divider} />
					<div className={classes.content}>
						<img src={singleEmployee} alt='Crea un cotizante' width='22%' height='auto' />
						<Button
							buttonColor='primary'
							className={classes.cardButtonOne}
							onClick={openNewCotizanteModal}
						>
							Crear cotizante
						</Button>
					</div>
				</Paper>
				<Paper className={classes.card}>
					<Typography className={classes.cardOneTitle}>Carga muchos cotizantes</Typography>
					<Divider className={classes.divider} />
					<div className={classes.content}>
						<img src={multiEmployees} alt='Carga muchos cotizantes' width='35%' height='auto' />
						<Button
							startIcon={<CloudDownloadIcon />}
							className={classes.downloadButton}
							size='small'
							variant='text'
							onClick={downloadTemplate}
						>
							Descargar formato
						</Button>
						<FlatfileWrapper
							onUploadData={getNewCotizantes}
							element={(props) => <Button {...props}>Cargar archivo</Button>}
						/>
					</div>
				</Paper>
			</div>
		</Dialog>
	);
};

const useStyles = makeStyles((theme) => ({
	container: {
		backgroundColor: '#000000CC',
	},
	paperContainer: {
		backgroundColor: 'transparent',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	backdrop: {
		backgroundColor: 'transparent',
	},
	contentContainer: {
		display: 'flex',
		alignItems: 'center',
		width: '50%',
		height: '35%',
		justifyContent: 'center',
	},
	card: {
		display: 'flex',
		flexDirection: 'column',
		backgroundColor: theme.palette.common.white,
		height: '100%',
		width: '40%',
		padding: theme.spacing(2, 0),
		margin: theme.spacing(0, 4),
	},
	cardOneTitle: {
		color: theme.palette.primary.main,
		fontSize: '1.6rem',
		fontWeight: 'bold',
		margin: theme.spacing(0, 0, 2, 3),
	},
	divider: {
		backgroundColor: '#707070',
		height: 1.5,
		width: '100%',
	},
	content: {
		display: 'flex',
		flex: 1,
		alignItems: 'center',
		flexDirection: 'column',
		justifyContent: 'center',
	},
	cardButtonOne: {
		marginTop: theme.spacing(5),
	},
	downloadIcon: {
		color: theme.palette.primary.main,
	},
	downloadButton: {
		margin: theme.spacing(1, 0),
	},
	closeButton: {
		position: 'absolute',
		top: '5%',
		left: '4%',
		color: theme.palette.common.white,
		'&:hover': {
			backgroundColor: `${theme.palette.grey[700]}20`,
		},
	},
}));

export default CreateCotizantesOptions;
