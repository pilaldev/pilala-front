import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Endpoints from '@bit/pilala.pilalalib.endpoints';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import moment from 'moment';
import CotizantesDetailLayout from './CotizantesDetailLayout';
import { EDITABLE_FIELDS, DROPDOWN_ID } from './Constants';
import {
  sendCotizanteData,
  sendCotizanteStatus,
} from './api/sendCotizanteData';
import {
  COTIZANTES_DETAIL,
  COTIZANTE_INFO,
  INGRESO,
  RETIRO,
  VARIACION_PERMANENTE,
  PRE_PLANILLA,
  SNACKBAR,
  NAVIGATION,
} from '../../redux/ActionTypes';
import {
  currentUserAction,
  cotizantesDetailAction,
  cotizantesInfoAction,
  novedadIngresoAction,
  novedadRetiroAction,
  novedadVariacionPermanenteAction,
  prePlanillaAction,
  snackBarAction,
  navigationAction,
} from '../../redux/Actions';
import { requestPreplanillaData } from '../../pila/pilaTable/api/RequestPreplanillaData';

import { FIRESTORE } from '../../api/Firebase';
import { Loader } from '../../components';

moment.locale('es');
let SnapshotSubscription = null;

export class CotizantesDetail extends Component {
  constructor() {
    super();
    this.state = {
      disableDeleteCotizanteButton: false,
      showDeleteConfirmationModal: false,
      dataLoaded: false,
      disableDisableCotizanteButton: false,
      showDisableCotizanteConfirmationModal: false,
      newCotizanteStatus: {},
      disableChangeArlButton: false,
      showChangeArlConfirmationModal: false,
      newCotizanteArl: {},
    };
  }

  componentDidMount() {
    const documentNumber = this.setDetailData();

    const companyInfo = this.props.companiesInfo[this.props.activeCompany];
    const cotizanteData = this.props.cotizantesInfo[this.props.activeCompany][
      documentNumber || this.props.cotizanteDocumentNumber
    ];

    if (companyInfo && cotizanteData) {
      const id = companyInfo.isCompany
        ? companyInfo.idCompany
        : companyInfo.idSucursal;
      SnapshotSubscription = FIRESTORE.collection('cotizantes')
        .doc(companyInfo.location.province.value)
        .collection(companyInfo.location.city.value)
        .doc(id)
        .collection(cotizanteData.tipoCotizante.value)
        .doc(cotizanteData.id)
        .onSnapshot((doc) => {
          const payload = {
            activeCompany: this.props.activeCompany,
            documentNumber: cotizanteData.documentNumber,
            cotizanteData: doc.data(),
          };
          // console.log('entroooo al cot', doc.data());

          this.props.cotizantesInfoAction(
            COTIZANTE_INFO.COTIZANTE_INFO_ADD_NEW_COTIZANTE,
            payload
          );
        });
      // }
    }

    if (this.props.activeCompany) {
      const companyInfo = this.props.companiesInfo[this.props.activeCompany];

      if (companyInfo) {
        requestPreplanillaData({
          nit: this.props.activeCompanyNit,
          companyId: companyInfo.isCompany
            ? companyInfo.idCompany
            : companyInfo.idSucursal,
          specificPlanilla: false,
          specificInfo: null,
        }).then((result) => {
          // console.log('result', result.result === 'SUCCESS', result);
          if (result.result === 'SUCCESS') {
            this.props.prePlanillaAction(
              PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA,
              result.data
            );
          }
        });
      }
    }

    ChangePageTitle('Detalle del cotizante | Pilalá');
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'cotizantes'
    );
  }

  componentWillUnmount() {
    /// DESCONECTAR EL LISTENER DE COTIZANTE
    if (SnapshotSubscription) {
      SnapshotSubscription();
    }

    if (this.props.cotizanteDetailData.editedDataIsSaved === false) {
      this.discarCotizanteEditedData();
    }

    this.props.cotizantesDetailAction(
      COTIZANTES_DETAIL.COTIZANTES_DETAIL_RESET_DATA,
      {}
    );
  }

  setDetailData = () => {
    let data = {};

    if (!this.props.cotizanteDocumentNumber) {
      const { detailID } = this.props.match.params;

      const tempData = Object.keys(
        this.props.cotizantesInfo[this.props.activeCompany]
      ).map((key) => {
        if (
          detailID ===
          this.props.cotizantesInfo[this.props.activeCompany][key].id
        ) {
          return this.props.cotizantesInfo[this.props.activeCompany][key];
        } else {
          return [null];
        }
      });

      data = tempData[0];

      if (!data) {
        this.props.history.replace('/NotFound');
      } else {
        this.props.cotizantesDetailAction(
          COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_DATA,
          {
            id: data.id,
            type: data.tipoCotizante,
            documentNumber: data.documentNumber,
          }
        );
      }
    } else {
      data = this.props.cotizantesInfo[this.props.activeCompany][
        this.props.cotizanteDocumentNumber
      ];
    }

    if (!data) {
      return null;
    }

    this.props.cotizantesDetailAction(
      COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_COTIZANTE_DATA,
      {
        mobileNumber: data.mobileNumber,
        email: data.email,
        salary: data.salary,
        name: data.name,
      }
    );

    this.setState({
      dataLoaded: true,
    });

    return data.documentNumber;
  };

  setRequestData = (fieldName, requestData) => {
    let cotizanteData = this.props.cotizantesInfo[this.props.activeCompany][
      this.props.cotizanteDocumentNumber
    ];

    let companyInfo = this.props.companiesInfo[this.props.activeCompany];

    let data = {};

    // console.log('companyInfo', companyInfo);

    switch (fieldName) {
      case DROPDOWN_ID.COTIZANTE_STATUS_DROPDOWN:
        data = {
          tipoCotizante: cotizanteData.tipoCotizante,
          cotizanteData: {
            documentNumber: cotizanteData.documentNumber,
            companyName: companyInfo.isCompany
              ? companyInfo.companyName
              : companyInfo.companyName,
            active: requestData,
          },
          idCotizante: cotizanteData.id,
          idAportante: this.props.activeCompany,
          nitCompany: this.props.companiesInfo[this.props.activeCompany].nit,
          location: companyInfo.location,
          adminPlatform: companyInfo.platformAdmin,
        };

        // console.log('DATTTA', data);
        break;

      case DROPDOWN_ID.COTIZANTE_ARL_LEVEL_DROPDOWN:
        data = {
          tipoCotizante: cotizanteData.tipoCotizante,
          cotizanteData: {
            ...cotizanteData,
            nivelArl: requestData,
          },
          idCotizante: cotizanteData.id,
          idSucursal: this.props.activeCompany,
          nitCompany: this.props.companiesInfo[this.props.activeCompany].nit,
          location: companyInfo.location,
          adminPlatform: companyInfo.platformAdmin,
        };
        break;

      default:
        data = {
          tipoCotizante: cotizanteData.tipoCotizante,
          cotizanteData: {
            ...cotizanteData,
          },
          idCotizante: cotizanteData.id,
          idSucursal: this.props.activeCompany,
          nitCompany: this.props.companiesInfo[this.props.activeCompany].nit,
          location: companyInfo.location,
          adminPlatform: companyInfo.platformAdmin,
        };
        break;
    }

    return data;
  };

  saveCotizanteEditedData = () => {
    let data = this.setRequestData();

    sendCotizanteData(data)
      .then((result) => {
        if (result.status === 'SUCCESS') {
          this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Actualización exitosa',
            description: 'El cotizante ha sido actualizado correctamente',
            color: 'success',
          });

          this.setDetailData();
        } else {
          throw result;
        }
      })
      .catch((error) => {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error al actualizar',
          description:
            'Hemos tenido problemas para actualizar el cotizante, inténtelo nuevamente',
          color: 'error',
        });

        this.discarCotizanteEditedData();
      });
  };

  discarCotizanteEditedData = () => {
    let data = {
      email: this.props.cotizanteDetailData.email,
      mobileNumber: this.props.cotizanteDetailData.mobileNumber,
    };

    this.props.cotizantesInfoAction(
      COTIZANTE_INFO.COTIZANTE_INFO_DISCARD_EDITED_COTIZANTE_INFO,
      {
        activeCompany: this.props.activeCompany,
        documentNumber: this.props.cotizanteDocumentNumber,
        data,
      }
    );
  };

  editCotizanteData = (event) => {
    let fieldName = event.target.name;
    let data = event.target.value;

    switch (fieldName) {
      case EDITABLE_FIELDS.EMAIL:
        this.props.cotizantesInfoAction(
          COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_EMAIL,
          {
            activeCompany: this.props.activeCompany,
            documentNumber: this.props.cotizanteDocumentNumber,
            data,
          }
        );

        break;

      case EDITABLE_FIELDS.MOBILE_NUMBER:
        if (!isNaN(data)) {
          this.props.cotizantesInfoAction(
            COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_PHONE_NUMBER,
            {
              activeCompany: this.props.activeCompany,
              documentNumber: this.props.cotizanteDocumentNumber,
              data,
            }
          );
        }
        break;

      default:
        break;
    }
  };

  editCotizanteDataFromDropdown = (fieldName, data) => {
    // let datos = {};
    switch (fieldName) {
      case DROPDOWN_ID.COTIZANTE_TYPE_DROPDOWN:
        this.props.cotizantesInfoAction(
          COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_TYPE,
          {
            activeCompany: this.props.activeCompany,
            documentNumber: this.props.cotizanteDocumentNumber,
            data,
          }
        );
        break;

      case DROPDOWN_ID.COTIZANTE_STATUS_DROPDOWN:
        if (
          data.value !==
          this.props.cotizantesInfo[this.props.activeCompany][
            this.props.cotizanteDocumentNumber
          ].activeInCompany.value
        ) {
          this.setState({
            showDisableCotizanteConfirmationModal: true,
            newCotizanteStatus: data,
            // disableCotizanteConfirmationModalMessage: data.value
            // 	? '¡Este cambio activará el cotizante y lo agregará a la planilla actual!'
            // 	: '¡Este cambio desactivará el cotizante y lo retirará de la planilla actual!'
          });
        }

        break;

      case DROPDOWN_ID.COTIZANTE_ARL_LEVEL_DROPDOWN:
        if (
          data.code !==
          this.props.cotizantesInfo[this.props.activeCompany][
            this.props.cotizanteDocumentNumber
          ].nivelArl.code
        ) {
          this.setState({
            showChangeArlConfirmationModal: true,
            newCotizanteArl: data,
          });
        }
        break;

      default:
        break;
    }
  };

  /* Novedad de ingreso */

  addCotizante = () => {
    const { activeCompany, cotizantesInfo, cotizanteDetailData } = this.props;
    const cotizanteData = this.props.prePlanilla[activeCompany][
      this.props.periodosInfo[this.props.activeCompany].value
    ]['dependientes'][this.props.periodosInfo[this.props.activeCompany].month][
      cotizanteDetailData.documentNumber
    ];
    const novedad = cotizanteData.basicas.find(
      (item) => item.type === 'Ingresos'
    );
    const novedad_fields = {
      cotizanteName:
        cotizantesInfo[activeCompany][cotizanteDetailData.documentNumber].name
          .display,
      cotizanteSalary:
        cotizantesInfo[activeCompany][cotizanteDetailData.documentNumber].salary
          .value,
      regLine: {}, //cotizanteData.regLine,
      newEntry: cotizanteData.newEntry,
      basicas: cotizanteData.basicas,
      idCotizante: cotizanteData.id,
      cotizanteDocumentNumber: cotizanteData.documentNumber,
      tipoCotizante: cotizanteData.tipoCotizante.value,
    };
    if (typeof novedad === 'object') {
      novedad_fields.dateEntry = novedad.fechaInicio;
      novedad_fields.idNovedad = novedad.id;
    }

    this.props.novedadIngresoAction(
      INGRESO.INGRESO_SAVE_NOVEDAD_FIELDS,
      novedad_fields
    );
    this.props.novedadIngresoAction(INGRESO.INGRESO_OPEN_INGRESO_MODAL, null);
  };

  /* Novedad de Retiro */

  removeCotizante = () => {
    const { activeCompany, cotizantesInfo, cotizanteDetailData } = this.props;
    const cotizanteData = this.props.prePlanilla[activeCompany][
      this.props.periodosInfo[this.props.activeCompany].value
    ]['dependientes'][this.props.periodosInfo[this.props.activeCompany].month][
      cotizanteDetailData.documentNumber
    ];
    const novedad = cotizanteData.basicas.find(
      (item) => item.type === 'Retiros'
    );
    const novedad_fields = {
      cotizanteName:
        cotizantesInfo[activeCompany][cotizanteDetailData.documentNumber].name
          .display,
      cotizanteSalary:
        cotizantesInfo[activeCompany][cotizanteDetailData.documentNumber].salary
          .value,
      regLine: {}, //cotizanteData.regLine,
      retired: cotizanteData.retired,
      basicas: cotizanteData.basicas,
      idCotizante: cotizanteData.id,
      cotizanteDocumentNumber: cotizanteData.documentNumber,
      tipoCotizante: cotizanteData.tipoCotizante.value,
    };
    if (typeof novedad === 'object') {
      novedad_fields.dateRetirement = novedad.fechaInicio;
      novedad_fields.idNovedad = novedad.id;
    }
    this.props.novedadRetiroAction(
      RETIRO.RETIRO_SAVE_NOVEDAD_FIELDS,
      novedad_fields
    );
    this.props.novedadRetiroAction(RETIRO.RETIRO_OPEN_RETIRO_MODAL, null);
  };

  /* Variación permanente de salario */

  changeCotizanteSalary = () => {
    const {
      activeCompany,
      cotizanteDetailData,
      periodosInfo,
      prePlanilla,
    } = this.props;
    const periodoInfo = periodosInfo[activeCompany];
    const cotizanteData =
      prePlanilla[activeCompany][periodoInfo.value]['dependientes'][
        periodoInfo.month
      ][cotizanteDetailData.documentNumber];
    const novedad = cotizanteData.salariales.find(
      (item) => item.nombreNovedad === 'Ajuste de salario permanente'
    );
    const novedad_fields = {
      cotizanteName: cotizanteData.name.display,
      cotizanteSalary: cotizanteData.salary.value,
      cotizanteDocumentNumber: cotizanteData.documentNumber,
      cotizanteType: cotizanteData.tipoCotizante.value,
      idCotizante: cotizanteData.id,
      salariales: cotizanteData.salariales,
      ajustePermanente: cotizanteData.salaryVariation,
      regLine: {},
    };
    if (typeof novedad === 'object') {
      novedad_fields.dateStarted = novedad.fechaInicio;
      novedad_fields.fechaInicio = novedad.fechaInicio;
      novedad_fields.idNovedad = novedad.id;
      novedad_fields.newSalary = novedad.value.newSalary;
      novedad_fields.duracionDias = novedad.duracionDias;
      novedad_fields.cotizanteSalary = novedad.value.previousSalary;
    }
    this.props.novedadVariacionPermanenteAction(
      VARIACION_PERMANENTE.VARIACION_PERMANENTE_SET_COTIZANTE_DATA,
      novedad_fields
    );
    this.props.novedadVariacionPermanenteAction(
      VARIACION_PERMANENTE.VARIACION_PERMANENTE_MODAL_SET_STATUS,
      null
    );
  };

  backToCotizantes = () => {
    this.props.history.replace(
      `/${this.props.match.params.userId}/console/cotizantes`
    );
  };

  /* DELETE COTIZANTE */

  deleteCotizante = () => {
    this.setState({
      showDeleteConfirmationModal: true,
    });
  };

  confirmDeleteCotizante = () => {
    if (this.state.disableDeleteCotizanteButton) return;
    this.setState({
      disableDeleteCotizanteButton: true,
    });
    let cotizanteData = this.props.cotizantesInfo[this.props.activeCompany][
      this.props.cotizanteDocumentNumber
    ];

    let companyInfo = this.props.companiesInfo[this.props.activeCompany];

    let deleteCotizanteData = {
      documentNumber: cotizanteData.documentNumber,
      idAportante: this.props.activeCompany,
      adminPlatform: companyInfo.platformAdmin.email,
    };

    fetch(Endpoints.deleteCotizante, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(deleteCotizanteData),
    })
      .then((response) =>
        response.json().then((data) => ({
          data: data,
          status: response.status,
        }))
      )
      .then((result) => {
        if (result.status === 200 && result.data) {
          this.backToCotizantes();

          this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Eliminación exitosa',
            description: 'El cotizante ha sido eliminado correctamente',
            color: 'success',
          });

          this.setState({
            showDeleteConfirmationModal: false,
            disableDeleteCotizanteButton: false,
          });

          this.props.cotizantesInfoAction(
            COTIZANTE_INFO.COTIZANTE_INFO_DELETE_COTIZANTE,
            {
              activeCompany: this.props.activeCompany,
              documentNumber: cotizanteData.documentNumber,
            }
          );
          const cotizante_fields = {
            companyNit: this.props.activeCompany,
            periodoActual: this.props.periodosInfo[this.props.activeCompany]
              .value,
            idPlanilla: this.props.periodosInfo[this.props.activeCompany].month,
            documentNumber: cotizanteData.documentNumber,
          };

          this.props.prePlanillaAction(
            PRE_PLANILLA.PRE_PLANILLA_REMOVE_COTIZANTE,
            cotizante_fields
          );
        } else {
          this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Error al eliminar',
            description:
              'Hemos tenido problemas para eliminar el cotizante, inténtelo nuevamente',
            color: 'error',
          });
        }
      })
      .catch((error) => {
        console.log('ERROR', error);
      });
  };

  cancelConfirmDeleteCotizante = () => {
    this.setState({
      showDeleteConfirmationModal: false,
      disableDeleteCotizanteButton: false,
    });
  };

  /* DISABLE COTIZANNTE */

  confirmDisableCotizante = () => {
    if (this.state.disableDisableCotizanteButton) return;
    this.setState({
      disableDisableCotizanteButton: true,
    });

    let datos = this.setRequestData(
      DROPDOWN_ID.COTIZANTE_STATUS_DROPDOWN,
      this.state.newCotizanteStatus.value
    );

    sendCotizanteStatus(datos)
      .then((result) => {
        if (result.status === 'SUCCESS') {
          this.props.cotizantesInfoAction(
            COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_STATUS,
            {
              activeCompany: this.props.activeCompany,
              documentNumber: this.props.cotizanteDocumentNumber,
              data: this.state.newCotizanteStatus,
            }
          );

          this.setState({
            showDisableCotizanteConfirmationModal: false,
            disableDisableCotizanteButton: false,
          });

          this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Actualización exitosa',
            description: 'El cotizante ha sido deshabilitado correctamente',
            color: 'success',
          });
        } else {
          throw result;
        }
      })
      .then((result) => {
        if (this.props.activeCompany) {
          let companyInfo = this.props.companiesInfo[this.props.activeCompany];

          if (companyInfo) {
            requestPreplanillaData({
              nit: this.props.activeCompanyNit,
              specificPlanilla: false,
              specificInfo: null,
              companyId: companyInfo.idCompany,
            }).then((result) => {
              if (result.result === 'SUCCESS') {
                this.props.prePlanillaAction(
                  PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA,
                  result.data
                );
              }
            });
          }
        }
      })
      .catch((error) => {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error al deshabilitar',
          description:
            'Hemos tenido problemas para deshabilitar el cotizante, inténtelo nuevamente',
          color: 'error',
        });
        this.setState({
          showDisableCotizanteConfirmationModal: false,
          disableDisableCotizanteButton: false,
        });
      });
  };

  cancelConfirmDisableCotizante = () => {
    this.setState({
      showDisableCotizanteConfirmationModal: false,
      disableDisableCotizanteButton: false,
      newCotizanteStatus: {},
    });
  };

  /* CHANGE COTIZANTE ARL */

  confirmChangeCotizanteArl = () => {
    if (this.state.disableChangeArlButton) return;
    this.setState({
      disableChangeArlButton: true,
    });

    let datos = this.setRequestData(
      DROPDOWN_ID.COTIZANTE_ARL_LEVEL_DROPDOWN,
      this.state.newCotizanteArl
    );

    sendCotizanteData(datos)
      .then((result) => {
        if (result.status === 'SUCCESS') {
          this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: 'Actualización exitosa',
            description: 'El cotizante ha sido actualizado correctamente',
            color: 'success',
          });

          this.props.cotizantesInfoAction(
            COTIZANTE_INFO.COTIZANTE_INFO_EDIT_COTIZANTE_ARL_LEVEL,
            {
              activeCompany: this.props.activeCompany,
              documentNumber: this.props.cotizanteDocumentNumber,
              data: this.state.newCotizanteArl,
            }
          );

          this.setState({
            disableChangeArlButton: false,
            showChangeArlConfirmationModal: false,
            newCotizanteArl: {},
          });
        } else {
          throw result;
        }
      })
      .catch((error) => {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error al actualizar',
          description:
            'Hemos tenido problemas para actualizar el cotizante, inténtelo nuevamente',
          color: 'error',
        });

        this.setState({
          disableChangeArlButton: false,
          showChangeArlConfirmationModal: false,
          newCotizanteArl: {},
        });

        this.discarCotizanteEditedData();
      });
  };

  cancelConfirmChangeCotizanteArl = () => {
    this.setState({
      disableChangeArlButton: false,
      showChangeArlConfirmationModal: false,
      newCotizanteArl: {},
    });
  };

  render() {
    const layoutProps = {
      ...this.props.cotizantesInfo[this.props.activeCompany][
        this.props.cotizanteDocumentNumber
      ],
      cotizantesTypesList: this.props.cotizantesTypesList,
      arlClasesList: this.props.arlClasesList,
      editCotizanteData: this.editCotizanteData,
      editCotizanteDataFromDropdown: this.editCotizanteDataFromDropdown,
      discarCotizanteEditedData: this.discarCotizanteEditedData,
      saveCotizanteEditedData: this.saveCotizanteEditedData,
      addCotizante: this.addCotizante,
      removeCotizante: this.removeCotizante,
      changeCotizanteSalary: this.changeCotizanteSalary,
      backToCotizantes: this.backToCotizantes,
      deleteCotizante: this.deleteCotizante,
      disableDeleteCotizanteButton: this.state.disableDeleteCotizanteButton,
      showDeleteConfirmationModal: this.state.showDeleteConfirmationModal,
      confirmDeleteCotizante: this.confirmDeleteCotizante,
      cancelConfirmDeleteCotizante: this.cancelConfirmDeleteCotizante,
      showDisableCotizanteConfirmationModal: this.state
        .showDisableCotizanteConfirmationModal,
      disableDisableCotizanteButton: this.state.disableDisableCotizanteButton,
      confirmDisableCotizante: this.confirmDisableCotizante,
      cancelConfirmDisableCotizante: this.cancelConfirmDisableCotizante,
      newCotizanteStatus: this.state.newCotizanteStatus.value,
      confirmChangeCotizanteArl: this.confirmChangeCotizanteArl,
      cancelConfirmChangeCotizanteArl: this.cancelConfirmChangeCotizanteArl,
      disableChangeArlButton: this.state.disableChangeArlButton,
      showChangeArlConfirmationModal: this.state.showChangeArlConfirmationModal,
    };

    return this.state.dataLoaded ? (
      <CotizantesDetailLayout {...layoutProps} />
    ) : (
      <Loader />
    );
  }
}

const mapStateToProps = (state, currentProps) => {
  return {
    tipoCotizante: state.cotizanteDetailData.tipoCotizante,
    cotizanteDocumentNumber: state.cotizanteDetailData.documentNumber,
    cotizanteId: state.cotizanteDetailData.id,
    activeCompany: state.currentUser.activeCompany,
    activeCompanyNit: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany].nit
      : null,
    companyName: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
          .isCompany
        ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .companyName
        : state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .sucursalName
      : null,
    companiesInfo: state.currentUser.companiesInfo,
    companies: state.currentUser.companies,
    periodosInfo: state.currentUser.periodosInfo,
    cotizantesInfo: state.cotizantesInfo,
    cotizanteDetailData: state.cotizanteDetailData,
    cotizantesTypesList: state.resources.tiposCotizantes,
    arlClasesList: state.resources.clasesArl,
    novedadIngreso: state.novedadIngreso,
    novedadRetiro: state.novedadRetiro,
    showIngresoConfirmModal: state.novedadIngreso.showConfirmModal,
    showRetiroConfirmModal: state.novedadRetiro.showConfirmModal,
    novedadVariacionPermanente: state.novedadVariacionPermanente,
    showVariacionPermanenteConfirmModal:
      state.novedadVariacionPermanente.openedConfirmationModal,
    prePlanilla: state.prePlanilla,
    adminName: state.currentUser.name,
    adminEmail: state.currentUser.email,
    idCompany: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
          .isCompany
        ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .idCompany
        : state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .idSucursal
      : null,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    currentUserAction: (actionType, value) =>
      dispatch(currentUserAction(actionType, value)),
    cotizantesDetailAction: (actionType, value) =>
      dispatch(cotizantesDetailAction(actionType, value)),
    cotizantesInfoAction: (actionType, value) =>
      dispatch(cotizantesInfoAction(actionType, value)),
    novedadIngresoAction: (actionType, value) =>
      dispatch(novedadIngresoAction(actionType, value)),
    novedadRetiroAction: (actionType, value) =>
      dispatch(novedadRetiroAction(actionType, value)),
    novedadVariacionPermanenteAction: (actionType, value) =>
      dispatch(novedadVariacionPermanenteAction(actionType, value)),
    prePlanillaAction: (actionType, value) =>
      dispatch(prePlanillaAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(CotizantesDetail)
);
