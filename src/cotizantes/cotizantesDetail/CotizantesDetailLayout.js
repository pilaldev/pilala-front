import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { cotizantesDetailAction } from '../../redux/Actions';
import { COTIZANTES_DETAIL } from '../../redux/ActionTypes';
import { DROPDOWN_ID, COTIZANTE_STATUS_ARRAY, EDITABLE_FIELDS, TEXTS } from './Constants';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import ButtonBase from '@material-ui/core/ButtonBase';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import EditIcon from '@material-ui/icons/Edit';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxTwoToneIcon from '@material-ui/icons/CheckBoxTwoTone';
import CheckBoxOutlineBlankRoundedIcon from '@material-ui/icons/CheckBoxOutlineBlankRounded';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import IconButton from '@material-ui/core/IconButton';
import ConfirmationModalLayout from './../../pila/ConfirmationModalLayout';
import { NovedadIngreso, NovedadRetiro, NovedadVariacionPermanente } from './../../pila';

const CotizantesDetailLayout = (props) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const [ anchorCotizanteStatus, setAnchorCotizanteStatus ] = useState(null);
	const [ anchorCotizanteArlLevel, setAnchorCotizanteArlLevel ] = useState(null);
	const [ editedCotizanteInfo, setEditedCotizanteInfoStatus ] = useState(false);
	const editInfoStatus = useSelector((state) => state.cotizanteDetailData.disabledInfoEdit);
	const editedDataIsSaved = useSelector((state) => state.cotizanteDetailData.editedDataIsSaved);
	const activeCompany = useSelector((state) => state.currentUser.activeCompany);
	const companiesInfo = useSelector((state) => state.currentUser.companiesInfo);

	const handleClick = (event) => {
		let name = event.currentTarget.name;

		switch (name) {
			case DROPDOWN_ID.COTIZANTE_STATUS_DROPDOWN:
				setAnchorCotizanteStatus(event.currentTarget);
				break;

			case DROPDOWN_ID.COTIZANTE_ARL_LEVEL_DROPDOWN:
				setAnchorCotizanteArlLevel(event.currentTarget);
				break;

			default:
				break;
		}
	};

	const handleMenuItemClick = (event, item) => {
		let fieldName = event.currentTarget.id;
		props.editCotizanteDataFromDropdown(fieldName, item);
		handleClose();
	};

	const handleClose = () => {
		setAnchorCotizanteStatus(null);
		setAnchorCotizanteArlLevel(null);
	};

	const changeEditInfoStatus = () => {
		setEditedCotizanteInfoStatus(false);
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_EDIT_INFO, {}));
	};

	const editCotizanteData = (event) => {
		if (!editInfoStatus) {
			setEditedCotizanteInfoStatus(true);
		}

		if (editedDataIsSaved === null) {
			dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_SAVED_DATA_STATUS, false));
		}

		props.editCotizanteData(event);
	};

	const discarCotizanteEditedData = () => {
		props.discarCotizanteEditedData();
		setEditedCotizanteInfoStatus(false);
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_EDIT_INFO, {}));
		if (editedDataIsSaved !== null) {
			dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_SAVED_DATA_STATUS, null));
		}
	};

	const saveCotizanteEditedData = () => {
		props.saveCotizanteEditedData();
		setEditedCotizanteInfoStatus(false);
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_EDIT_INFO, {}));
		if (editedDataIsSaved !== null) {
			dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_SAVED_DATA_STATUS, null));
		}
	};

	return (
		<div className={classes.container}>
			<Grid container direction="column" justify="flex-start" alignItems="center">
				<Grid
					direction="row"
					container
					style={{
						overflow: 'hidden',
						height: '20vh',
						background:
							'linear-gradient(90deg, rgba(108,42,169,1) 0%, rgba(108,42,169,1) 52%, rgba(122,31,162,1) 88%)'
					}}
				>
					<div className={classes.headerFirstContainer}>
						<Avatar
							alt="user avatar"
							src={
								props.genre ? props.genre.value ? (
									require(`../../assets/img/women_one.svg`)
								) : (
									require(`../../assets/img/men_one.svg`)
								) : (
									require(`../../assets/img/men_one.svg`)
								)
							}
							className={classes.userAvatar}
						/>
						<IconButton
							className={classes.backButton}
							disableFocusRipple
							disableRipple
							onClick={() => {
								props.backToCotizantes();
							}}
						>
							<ArrowBackIcon />
						</IconButton>
					</div>
					<div className={classes.headerSecondContainer}>
						<div className={classes.nameContainer}>
							<Typography
								variant="h5"
								component="h5"
								style={{
									margin: 0,
									color: 'white',
									marginLeft: '10px',
									marginBottom: '5px',
									fontWeight: 'bold'
								}}
							>
								{props.name.display ? props.name.display : ''}
							</Typography>
						</div>
						<div className={classes.secondaryInfoContainer}>
							<Button
								aria-controls="customized-menu"
								aria-haspopup="true"
								variant="outlined"
								name={DROPDOWN_ID.COTIZANTE_TYPE_DROPDOWN}
								// onClick={handleClick}
								className={classes.secondaryInfoContainerDropDown}
								endIcon={<KeyboardArrowDownIcon />}
								// color='error'
							>
								{props.tipoCotizante.display}
							</Button>
							{/* <Menu
								anchorEl={anchorCotizanteType}
								// keepMounted
								name={DROPDOWN_ID.COTIZANTE_TYPE_DROPDOWN}
								open={Boolean(anchorCotizanteType)}
								elevation={0}
								getContentAnchorEl={null}
								onClose={handleClose}
								anchorOrigin={{
									vertical: 'bottom',
									horizontal: 'center'
								}}
								transformOrigin={{
									vertical: 'top',
									horizontal: 'center'
								}}
							>
								{props.cotizantesTypesList.map((item, index) => {
									return (
										<MenuItem
											onClick={(event) => handleMenuItemClick(event, item)}
											id={DROPDOWN_ID.COTIZANTE_TYPE_DROPDOWN}
											key={item.code}
										>
											<ListItemText primary={item.display.toString()} />
										</MenuItem>
									);
								})}
							</Menu> */}

							<Button
								aria-controls="customized-menu"
								aria-haspopup="true"
								variant="outlined"
								onClick={handleClick}
								className={classes.secondaryInfoContainerDropDown}
								endIcon={<KeyboardArrowDownIcon />}
								name={DROPDOWN_ID.COTIZANTE_STATUS_DROPDOWN}
							>
								{props.activeInCompany.activeText}
							</Button>
							<Menu
								anchorEl={anchorCotizanteStatus}
								// keepMounted
								name={DROPDOWN_ID.COTIZANTE_STATUS_DROPDOWN}
								open={Boolean(anchorCotizanteStatus)}
								elevation={0}
								getContentAnchorEl={null}
								onClose={handleClose}
								anchorOrigin={{
									vertical: 'bottom',
									horizontal: 'center'
								}}
								transformOrigin={{
									vertical: 'top',
									horizontal: 'center'
								}}
							>
								{COTIZANTE_STATUS_ARRAY.map((item) => {
									return (
										<MenuItem
											onClick={(event) => handleMenuItemClick(event, item)}
											id={DROPDOWN_ID.COTIZANTE_STATUS_DROPDOWN}
											key={item.activeText}
										>
											<ListItemText primary={item.activeText.toString()} />
										</MenuItem>
									);
								})}
							</Menu>

							{props.activeInCompany.value && (
								<React.Fragment>
									<Typography
										onClick={props.addCotizante}
										variant="body1"
										noWrap={true}
										style={{
											color: 'white',
											marginTop: '9px',
											marginLeft: '10px',
											textDecoration: 'underline',
											cursor: 'pointer'
										}}
									>
										{TEXTS.ADD_COTIZANTE}
									</Typography>
									<Typography
										variant="body1"
										style={{
											color: 'white',
											marginTop: '9px',
											marginLeft: '10px',
											textDecoration: 'underline'
										}}
									>
										{TEXTS.FARES_SEPARATOR}
									</Typography>
									<Typography
										onClick={props.removeCotizante}
										variant="body1"
										noWrap={true}
										style={{
											color: 'white',
											marginTop: '9px',
											marginLeft: '10px',
											textDecoration: 'underline',
											cursor: 'pointer'
										}}
									>
										{TEXTS.REMOVE_COTIZANTE}
									</Typography>
									<Typography
										variant="body1"
										style={{
											color: 'white',
											marginTop: '9px',
											marginLeft: '10px',
											textDecoration: 'underline'
										}}
									>
										{TEXTS.FARES_SEPARATOR}
									</Typography>
									<Typography
										onClick={props.deleteCotizante}
										variant="body1"
										noWrap={true}
										style={{
											color: 'white',
											marginTop: '9px',
											marginLeft: '10px',
											textDecoration: 'underline',
											cursor: 'pointer'
										}}
									>
										{TEXTS.DELETE_COTIZANTE}
									</Typography>
								</React.Fragment>
							)}
						</div>
					</div>
					<div className={classes.headerThirdContainer}>
						{/* <Button
							aria-controls="customized-menu"
							aria-haspopup="true"
							variant="contained"
							className={classes.historyButton}
							name={DROPDOWN_ID.COTIZANTE_HISTORY_DROPDOWN}
						>
							{TEXTS.NOVELTIES_HISTORY}
						</Button> */}
					</div>
				</Grid>

				<Grid container direction="column" alignItems="flex-start" style={{ width: '95%' }}>
					<Typography
						variant="h5"
						component="h5"
						style={{
							margin: 0,
							color: 'black',
							marginTop: '40px',
							marginBottom: '20px',
							alignSelf: 'left',
							fontWeight: 'bold'
						}}
					>
						{TEXTS.COTIZANTE_INFO}
					</Typography>
				</Grid>

				<Grid
					direction="row"
					container
					style={{
						height: '35vh',
						width: '95%',
						paddingBottom: 20,
						overflow: 'hidden',
						justifyContent: 'space-between'
					}}
				>
					<Card
						className={classes.cotizanteEditableInfoCard}
						variant="outlined"
						style={!editInfoStatus ? { border: '1.5px solid #7A1FA2' } : {}}
					>
						<Grid
							container
							direction="row"
							justify="flex-end"
							alignItems="center"
							className={classes.editGrid}
							style={{ height: '20%' }}
						>
							<ButtonBase
								className={classes.editButton}
								onClick={changeEditInfoStatus}
								disabled={!editInfoStatus || !props.activeInCompany.value}
							>
								<BorderColorRoundedIcon className={classes.editIcon} />
								<Typography variant="subtitle2">{TEXTS.EDIT}</Typography>
							</ButtonBase>
						</Grid>

						<div className={classes.cotizantesEditableInfoCardDataContainer}>
							<div className={classes.cotizantesInfoCardContainerRow}>
								<div className={classes.cotizantesInfoCardContainerTitleDataBox}>
									<Typography className={classes.formTitle}>{TEXTS.NAME}</Typography>
								</div>
								<div className={classes.cotizantesInfoCardContainerDataBox}>
									{/* <TextField
										variant="standard"
										size="small"
										className={classes.salarytextInput}
										placeholder={'name'}
										name={EDITABLE_FIELDS.NAME}
										onChange={(e) => editCotizanteData(e, props.id, props.tipoCotizante)}
										value={props.name.displayName}
										disabled={true}
									/> */}

									<Typography className={classes.formText}>{props.name.display}</Typography>
								</div>
							</div>

							<div className={classes.cotizantesInfoCardContainerRow}>
								<div className={classes.cotizantesInfoCardContainerTitleDataBox}>
									<Typography className={classes.formTitle}>{TEXTS.DOCUMENT_NUMBER}</Typography>
								</div>
								<div className={classes.cotizantesInfoCardContainerDataBox}>
									{/* <TextField
										variant="standard"
										size="small"
										className={classes.salarytextInput}
										placeholder={'hola'}
										onChange={(e) => editCotizanteData(e, props.id, props.tipoCotizante)}
										name={EDITABLE_FIELDS.DOCUMENT_NUMBER}
										value={props.documentNumber}
										// error={props.inputError.userPassword}
										disabled={true}
									/> */}
									<Typography className={classes.formText}>{props.documentNumber}</Typography>
								</div>
							</div>
							<div className={classes.cotizantesInfoCardContainerRow}>
								<div className={classes.cotizantesInfoCardContainerTitleDataBox}>
									<Typography className={classes.formTitle}>{TEXTS.EMAIL}</Typography>
								</div>
								<div className={classes.cotizantesInfoCardContainerDataBox}>
									{!editInfoStatus ? (
										<TextField
											variant="standard"
											size="small"
											className={classes.salarytextInput}
											placeholder={'email'}
											onChange={(e) => editCotizanteData(e, props.id, props.tipoCotizante)}
											value={props.email}
											name={EDITABLE_FIELDS.EMAIL}
											// error={props.inputError.userPassword}
											disabled={editInfoStatus}
										/>
									) : (
										<Typography className={classes.formText}>{props.email}</Typography>
									)}
								</div>
							</div>
							<div className={classes.cotizantesInfoCardContainerRow}>
								<div className={classes.cotizantesInfoCardContainerTitleDataBox}>
									<Typography className={classes.formTitle}>{TEXTS.PHONE_NUMBER}</Typography>
								</div>
								<div className={classes.cotizantesInfoCardContainerDataBox}>
									{!editInfoStatus ? (
										<TextField
											variant="standard"
											size="small"
											className={classes.salarytextInput}
											placeholder={'phone'}
											onChange={(e) => editCotizanteData(e, props.id, props.tipoCotizante)}
											value={props.mobileNumber}
											name={EDITABLE_FIELDS.MOBILE_NUMBER}
											// error={props.inputError.userPassword}
											disabled={editInfoStatus}
										/>
									) : (
										<Typography className={classes.formText}>{props.mobileNumber}</Typography>
									)}
								</div>
							</div>
							{!editInfoStatus ? (
								<Grid
									container
									direction="row"
									justify="center"
									alignItems="center"
									style={{ paddingTop: 5, paddingBottom: 5 }}
								>
									<Button className={classes.cancelButton} onClick={discarCotizanteEditedData}>
										<Typography variant="subtitle2" className={classes.cancelButtonText}>
											{TEXTS.CANCEL}
										</Typography>
									</Button>
									{editedCotizanteInfo && (
										<Button
											variant="contained"
											className={classes.saveButton}
											disabled={!editedCotizanteInfo}
											onClick={saveCotizanteEditedData}
										>
											<Typography variant="subtitle2" className={classes.saveButtonText}>
												{TEXTS.SAVE}
											</Typography>
										</Button>
									)}
								</Grid>
							) : null}
						</div>
					</Card>

					<Card className={classes.cotizanteInfoCard} variant="outlined">
						<CardContent>
							<div className={classes.cotizanteMainInfoCardContainer}>
								<div className={classes.chipsContainer}>
									<div className={classes.chipsDataBox}>
										<Typography noWrap className={classes.faresTitle}>
											{TEXTS.SALARY}
										</Typography>
										{props.activeInCompany.value ? (
											<div
												className={classes.salaryChip}
											>
												<AttachMoneyIcon />
												{props.salary.value ? props.salary.value : ''}
												<IconButton onClick={props.changeCotizanteSalary} size='small'>
													<EditIcon />
												</IconButton>
											</div>
										) : (
											<div
												className={classes.mainInfoChip}
											>
												{props.salary.value ? props.salary.value : ''}
											</div>
										)}
									</div>
									<div className={classes.chipsDataBox}>
										<Typography className={classes.faresTitle}>{TEXTS.ARL}</Typography>

										{props.activeInCompany.value ? (
											<React.Fragment>
												<Button
													aria-controls="customized-menu"
													aria-haspopup="true"
													variant="contained"
													onClick={handleClick}
													className={classes.mainInfoDropdown}
													endIcon={<KeyboardArrowDownIcon />}
													name={DROPDOWN_ID.COTIZANTE_ARL_LEVEL_DROPDOWN}
												>
													<Typography noWrap variant={'caption'}>
														{props.nivelArl.display.split('-')[1]}
													</Typography>
												</Button>
												<Menu
													anchorEl={anchorCotizanteArlLevel}
													// keepMounted
													open={Boolean(anchorCotizanteArlLevel)}
													name={DROPDOWN_ID.COTIZANTE_ARL_LEVEL_DROPDOWN}
													elevation={0}
													getContentAnchorEl={null}
													onClose={handleClose}
													anchorOrigin={{
														vertical: 'bottom',
														horizontal: 'center'
													}}
													transformOrigin={{
														vertical: 'top',
														horizontal: 'center'
													}}
												>
													{props.arlClasesList.map((item) => {
														return (
															<MenuItem
																onClick={(event) => handleMenuItemClick(event, item)}
																id={DROPDOWN_ID.COTIZANTE_ARL_LEVEL_DROPDOWN}
																key={item.display}
															>
																<ListItemText primary={item.display} />
															</MenuItem>
														);
													})}
												</Menu>
											</React.Fragment>
										) : (
											<div
												className={classes.mainInfoChip}
											>
												{props.nivelArl.display.split('-')[1]}
											</div>
										)}
									</div>
									<div className={classes.chipsDataBox}>
										<Typography className={classes.faresTitle}>{TEXTS.EPS}</Typography>
										<div
											className={classes.mainInfoChip}
										>
											{props.eps.display ? props.eps.display : ''}
										</div>
									</div>
									<div className={classes.chipsDataBox}>
										<Typography className={classes.faresTitle} noWrap>
											{TEXTS.PENSIONES_CESANTIAS}
										</Typography>
										<div
											className={classes.mainInfoChip}
										>
											{props.pension.display ? props.pension.display : ''}
										</div>
									</div>
								</div>

								<div className={classes.faresCard}>
									<div className={classes.faresCardContent}>
										<Typography className={classes.faresTitle}>{TEXTS.FARES_TITLE}</Typography>
										<Typography className={classes.faresTextGroup} noWrap={true} variant={'body2'}>
											<span className={classes.faresTitleBlack}>{TEXTS.SALUD}</span>
											{props.subsistemas.salud.value * 100}%{TEXTS.FARES_SEPARATOR}
											<span className={classes.faresTitleBlack}>{TEXTS.PENSION}</span>
											{props.subsistemas.pension.value * 100}%{TEXTS.FARES_SEPARATOR}
											<span className={classes.faresTitleBlack}>{TEXTS.RIESGOS}</span>
											{(props.subsistemas.arl.value * 100).toFixed(3)}%{TEXTS.FARES_SEPARATOR}
											<span className={classes.faresTitleBlack}>{TEXTS.CCF}</span>
											{props.subsistemas.ccf.value * 100}%{TEXTS.FARES_SEPARATOR}
											<span className={classes.faresTitleBlack}>{TEXTS.ICBF}</span>
											{props.subsistemas.icbf.value * 100}%{TEXTS.FARES_SEPARATOR}
											<span className={classes.faresTitleBlack}>{TEXTS.SENA}</span>
											{props.subsistemas.sena.value * 100}%
										</Typography>
										<div className={classes.faresCheckBoxContainer}>
											<Checkbox
												checked={companiesInfo[activeCompany].exentoPagoParafiscales}
												disabled={true}
												color="secondary"
												className={classes.obligacionCheckbox}
												checkedIcon={
													<CheckBoxTwoToneIcon className={classes.obligacionCheckboxIcon} />
												}
												icon={
													<CheckBoxOutlineBlankRoundedIcon
														className={classes.obligacionCheckboxIcon}
													/>
												}
											/>
											<Typography className={classes.obligacionName} noWrap>
												{TEXTS.PAY_EXEMPTION}
											</Typography>
											<Checkbox
												checked={companiesInfo[activeCompany].beneficiadoLey590Del2000}
												disabled={true}
												color="secondary"
												className={classes.obligacionCheckbox}
												checkedIcon={
													<CheckBoxTwoToneIcon className={classes.obligacionCheckboxIcon} />
												}
												icon={
													<CheckBoxOutlineBlankRoundedIcon
														className={classes.obligacionCheckboxIcon}
													/>
												}
											/>
											<Typography className={classes.obligacionName} noWrap>
												{TEXTS.LAW_590_BENEFICIARY}
											</Typography>
										</div>
									</div>
								</div>
							</div>
						</CardContent>
					</Card>
				</Grid>
			</Grid>
			<NovedadIngreso />
			<NovedadRetiro />
			<NovedadVariacionPermanente />
			<ConfirmationModalLayout
				id="confirmDelete"
				showModal={props.showDeleteConfirmationModal}
				message="¡Este cambio eliminará el cotizante de tu lista definitivamente!"
				cancelAction={props.cancelConfirmDeleteCotizante}
				confirmAction={props.confirmDeleteCotizante}
				alertColor="#FF495A"
				textColor="#FFFFFF"
				disableConfirmButton={props.disableDeleteCotizanteButton}
			/>
			<ConfirmationModalLayout
				id="confirmDisableCotizante"
				showModal={props.showDisableCotizanteConfirmationModal}
				message={
					props.newCotizanteStatus ? (
						'¡Este cambio activará el cotizante y lo agregará a la planilla actual!'
					) : (
						'¡Este cambio desactivará el cotizante y lo retirará de la planilla actual!'
					)
				}
				cancelAction={props.cancelConfirmDisableCotizante}
				confirmAction={props.confirmDisableCotizante}
				alertColor={props.newCotizanteStatus ? '#7B1FA2' : '#FF495A'}
				textColor="#FFFFFF"
				disableConfirmButton={props.disableDisableCotizanteButton}
			/>
			<ConfirmationModalLayout
				id="confirmChangeArl"
				showModal={props.showChangeArlConfirmationModal}
				message="¡Este cambio modificará el nivel de ARL del cotizante y esto afectará los totales de cotización en la planilla actual!"
				cancelAction={props.cancelConfirmChangeCotizanteArl}
				confirmAction={props.confirmChangeCotizanteArl}
				alertColor="#7B1FA2"
				textColor="#FFFFFF"
				disableConfirmButton={props.disableChangeArlButton}
			/>
		</div>
	);
};

const useStyles = makeStyles({
	container: {
		display: 'flex',
		flex: 1
	},
	headerFirstContainer: {
		display: 'flex',
		flexDirection: 'column',
		width: '10%',
		height: '100%',
		justifyContent: 'flex-end',
		// alignItems: 'flex-end',
		alignItems: 'center'
	},
	userAvatar: {
		width: 100,
		height: 100,
		background: 'white'
		// marginTop: 5
	},
	backButton: {
		color: 'white',
		padding: 8
		// marginTop: 5
	},
	headerSecondContainer: {
		display: 'flex',
		flexDirection: 'column',
		width: '60%'
	},
	nameContainer: {
		height: '50%',
		display: 'flex',
		alignItems: 'flex-end'
	},
	secondaryInfoContainer: {
		marginTop: 5,
		height: '50%',
		display: 'flex',
		flexDirection: 'row'
	},
	secondaryInfoContainerDropDown: {
		marginLeft: 5,
		color: 'white',
		borderStyle: 'solid',
		borderColor: 'white',
		height: '45%',
		borderRadius: 10,
		overflow: 'hidden'
	},
	headerThirdContainer: {
		display: 'flex',
		width: '40%',
		justifyContent: 'flex-end',
		alignItems: 'center',
		paddingRight: 80
	},
	historyButton: {
		display: 'inline-block',
		color: '#7B1FA2 ',
		backgroundColor: 'white',
		height: '25%',
		borderRadius: 30
	},

	/************** EDITABLE_FIELDS_CARD **************/

	cotizanteEditableInfoCard: {
		height: '100%',
		width: '40%',
		boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'
	},
	editButton: {
		marginRight: 5
	},

	editIcon: {
		marginRight: 5,
		color: '#757AFF'
	},
	formTitle: {
		color: 'grey',
		fontWeight: 'bold'
	},
	formText: {
		color: 'black',
		fontWeight: 'bold'
	},

	cotizantesEditableInfoCardDataContainer: {
		display: 'flex',
		flexDirection: 'column',
		flex: 1,
		height: '80%'
	},
	cotizantesInfoCardContainerRow: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		flex: 1,
		marginTop: 3
	},
	cotizantesInfoCardContainerTitleDataBox: {
		display: 'flex',
		justifyContent: 'flex-start',
		width: '40%',
		marginLeft: 10
	},
	cotizantesInfoCardContainerDataBox: {
		display: 'flex',
		justifyContent: 'flex-start',
		flex: 1
	},
	saveButton: {
		background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
		borderRadius: 100,
		marginLeft: 10
	},
	saveButtonText: {
		color: 'white'
	},
	cancelButtonText: {
		color: '#5E35B1'
	},
	cancelButton: {
		marginRight: 10,
		borderRadius: 100
	},

	/************** CARD **************/

	/*** CHIPS-CARD ***/

	cotizanteInfoCard: {
		height: '100%',
		width: '58%',
		boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'
	},
	cotizanteMainInfoCardContainer: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		height: '100%',
		width: '100%'
	},
	chipsContainer: {
		display: 'flex',
		flexDirection: 'row',
		height: '50%',
		width: '100%',
		// backgroundColor: 'red',
		marginTop: 3,
		marginBottom: 3,
		justifyContent: 'space-between'
	},
	chipsDataBox: {
		display: 'flex',
		flexDirection: 'column',
		// backgroundColor: 'orange',
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1
	},
	mainInfoChip: {
		color: 'black',
		backgroundColor: 'white',
		width: '95%',
		height: 40,
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 5,
		boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
	},
	mainInfoDropdown: {
		color: 'black',
		backgroundColor: 'white',
		width: '95%',
		height: 40,
		borderRadius: 5,
		boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
		overflow: 'hidden'
	},
	salaryChip: {
		display: 'flex',
		justifyContent: 'space-between',
		color: 'black',
		backgroundColor: 'white',
		width: '95%',
		height: 40,
		alignItems: 'center',
		borderRadius: 5,
		borderColor: 'black',
		boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
	},
	salarytextInput: {
		width: '90%',
		background: '#FFFFFF'
	},

	/*** FARES-CARD ***/

	faresCard: {
		width: '100%',
		height: '50%',
		backgroundColor: '#F7F7F7',
		padding: 10,
		borderRadius: 8
	},
	faresCardContent: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'flex-start',
		justifyItems: 'center',
		overflow: 'hidden'
	},
	faresTitle: {
		fontWeight: 'bold',
		color: '#7B1FA2 '
	},
	faresTitleBlack: {
		fontWeight: 'bold',
		color: 'black'
	},
	faresCheckBoxContainer: {
		display: 'flex',
		flex: 1,
		justifyContent: 'space-between',
		overflow: 'hidden',
		marginTop: 5
	},

	obligacionCheckbox: {
		padding: 0,
		marginRight: 5
	},
	obligacionCheckboxIcon: {
		width: 18,
		height: 18,
		color: '#263238'
	},
	obligacionName: {
		marginRight: 20
	},
	faresTextGroup: {
		marginTop: 5
	}
});

export default CotizantesDetailLayout;
