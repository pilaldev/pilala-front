import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Typography } from '@material-ui/core';

import { cotizantesDetailAction, currentUserAction, lineaRegistroAction } from '../../redux/Actions';
import { COTIZANTES_DETAIL, CURRENT_USER, RECORDS } from '../../redux/ActionTypes';
import { makeStyles } from '@material-ui/core/styles';

const ConfirmSalaryChange = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const modalStatus = useSelector((state) => state.cotizanteDetailData.confirmSalaryChangeModalStatus);
	const activeCompany = useSelector((state) => state.currentUser.activeCompany);
	const id = useSelector((state) => state.cotizanteDetailData.id);
	const tipoCotizante = useSelector((state) => state.cotizanteDetailData.tipoCotizante);
	const documentNumber = useSelector((state) => state.cotizanteDetailData.documentNumber);
	const newSalary = useSelector((state) => state.cotizanteDetailData.newSalary);
	const startDate = useSelector((state) => state.cotizanteDetailData.newSalaryStartDate);
	const changeModalStatus = () => {
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_CHANGE_CONFIRM_MODAL_STATUS, {}));
	};

	const cancelModalActions = () => {
		changeModalStatus();
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_RESET_EDIT_SALARY_DATA, {}));
	};

	const saveModalActions = () => {
		changeModalStatus();
		dispatch(
			currentUserAction(CURRENT_USER.CURRENT_USER_EDIT_COTIZANTE_INFO, {
				field: 'salary',
				id,
				tipoCotizante,
				data: newSalary
			})
		);

		//// EVALUAR VIABILIDAD ////
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_SALARY, newSalary));

		dispatch(
			lineaRegistroAction(RECORDS.CREATE_NEW_RECORD, {
				companyNit: activeCompany,
				cotizanteId: documentNumber,
				recordId: 'B1',
				newFields: {
					21: 'X',
					82: startDate
				}
			})
		);
	};

	return (
		<Dialog
			open={modalStatus}
			onClose={changeModalStatus}
			aria-labelledby="alert-dialog-title"
			aria-describedby="alert-dialog-description"
			className={classes.root}
		>
			<DialogTitle id="alert-dialog-title" className={classes.textColor}>
				{'!Este cambio creará una novedad para tu planillad del próximo periodo!'}
			</DialogTitle>
			<DialogActions>
				<Button className={classes.cancelButton}>
					<Typography variant="subtitle2" className={classes.cancelButtonText} onClick={cancelModalActions}>
						Cancelar novedad
					</Typography>
				</Button>

				<Button variant="contained" className={classes.saveButton} onClick={saveModalActions}>
					<Typography variant="subtitle2" className={classes.saveButtonText}>
						SAVE
					</Typography>
				</Button>
			</DialogActions>
		</Dialog>
	);
};

const useStyles = makeStyles({
	root: {
		'& .MuiDialog-paperWidthSm': {
			backgroundColor: '#5E35B1'
		}
	},
	textColor: {
		color: 'white'
	},
	saveButton: {
		backgroundColor: 'white',
		borderRadius: 100,
		marginLeft: 10
	},
	saveButtonText: {
		color: '#5E35B1'
	},
	cancelButtonText: {
		color: 'white'
	},
	cancelButton: {
		marginRight: 10,
		borderRadius: 100
	}
});

export default ConfirmSalaryChange;
