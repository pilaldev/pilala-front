export const EDITABLE_FIELDS = {
	SALARY: 'salary',
	TIPO_COTIZANTE: 'tipoCotizante',
	NIVEL_ARL: 'nivelArl',
	SUCURSAL: 'sucursal',
	STATUS: 'active',
	MOBILE_NUMBER: 'mobileNumber',
	EMAIL: 'email',
	DOCUMENT_NUMBER: 'documentNumber',
	NAME: 'name'
};

export const REG_EXPS = {
	name: /^[A-Za-zÁÉÍÓÚñáéíóúÑ ]*$/,
	numbers: /^[0-9]*$/,
	email: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/
};

export const COTIZANTE_STATUS_ARRAY = [
	{ value: true, activeText: 'Activo' },
	{ value: false, activeText: 'Inactivo' }
];

export const DROPDOWN_ID = {
	COTIZANTE_TYPE_DROPDOWN: 'COTIZANTE_TYPE_DROPDOWN',
	COTIZANTE_STATUS_DROPDOWN: 'COTIZANTE_STATUS_DROPDOWN',
	COTIZANTE_HISTORY_DROPDOWN: 'COTIZANTE_HISTORY_DROPDOWN',
	COTIZANTE_ARL_LEVEL_DROPDOWN: 'COTIZANTE_ARL_LEVEL_DROPDOWN'
};

export const TEXTS = {
	NOVELTIES_HISTORY: 'Historial de novedades',
	COTIZANTE_INFO:'Información del cotizante',
	NAME: 'Nombre completo:',
	EMAIL: 'Correo electrónico:',
	DOCUMENT_NUMBER: 'Cédula de ciudadanía:',
	PHONE_NUMBER: 'Teléfono celular:',
	SALARY: 'Salario base (IBC):',
	ARL:'Riesgo ARL:',
	EPS: 'EPS:',
	PENSIONES_CESANTIAS:'Pensiones y cesantías:',
	FARES_TITLE: 'Tarifas de cotizacón',
	SALUD: 'Salud: ',
	PENSION: 'Pensión: ',
	RIESGOS:'Riesgos: ',
	CCF:'CCF: ',
	SENA:'SENA: ',
	ICBF:'ICBF: ',
	FARES_SEPARATOR:' | ',
	PERCENTAGE:'%',
	PAY_EXEMPTION: 'Exento de pago de parafiscales',
	LAW_590_BENEFICIARY: 'Beneficiario ley 590 del 2000',
	EDIT:'EDITAR',
	CANCEL:'CANCELAR',
	SAVE:'GUARDAR',
	ADD_COTIZANTE: 'Ingresar Cotizante',
	REMOVE_COTIZANTE: 'Retirar Cotizante',
	DELETE_COTIZANTE: 'Eliminar Cotizante'


}