import React from 'react';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import {
	Typography,
	Dialog,
	DialogContent,
	DialogTitle,
	Grid,
	TextField,
	InputAdornment,
	Button
} from '@material-ui/core';

import clsx from 'clsx';
import { useSelector, useDispatch } from 'react-redux';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { cotizantesDetailAction } from '../../redux/Actions';
import { COTIZANTES_DETAIL } from '../../redux/ActionTypes';

import Chip from '@material-ui/core/Chip';

import MonetizationOnTwoToneIcon from '@material-ui/icons/MonetizationOnTwoTone';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import TodayIcon from '@material-ui/icons/Today';

const ChangeSalaryModal = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const modalStatus = useSelector((state) => state.cotizanteDetailData.changeSalaryModalStatus);
	const displayName = useSelector((state) => state.cotizanteDetailData.name.display);
	const salary = useSelector((state) => state.cotizanteDetailData.salary.value);
	const newSalary = useSelector((state) => state.cotizanteDetailData.newSalary);

	const changeModalStatus = () => {
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_CHANGE_MODAL_STATUS, {}));
	};

	const cancelModalActions = () => {
		changeModalStatus();
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_RESET_EDIT_SALARY_DATA, {}));
	};

	const saveModalActions = () => {
		changeModalStatus();
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_CHANGE_CONFIRM_MODAL_STATUS, {}));
	};

	const handleSalaryValue = (value) => {
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_NEW_SALARY, value));
	};

	const setFormatedDate = (date) => {
		dispatch(cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_NEW_SALARY_START_DATE, date));
	};

	const [ selectedDate, handleDateChange ] = React.useState(new Date());

	const formatDate = (date) => {
		let month = '' + (date.getMonth() + 1);
		let day = '' + date.getDate();
		let year = date.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		let formatted_date = [ year, month, day ].join('-');

		handleDateChange(date);
		setFormatedDate(formatted_date);
	};

	return (
		<ThemeProvider theme={theme}>
			<Dialog open={modalStatus} onClose={changeModalStatus} fullWidth={true} maxWidth="sm">
				<DialogTitle disableTypography>
					<Grid container direction="column" alignItems="center">
						<Typography className={classes.modalTitle} variant="h4">
							Cambio de Salario
						</Typography>
						<Typography variant="h6">
							{displayName} | Salario: ${salary}
						</Typography>
					</Grid>
				</DialogTitle>
				<DialogContent dividers style={{ paddingTop: 0, paddingBottom: 0 }}>
					<Grid container direction="column" alignItems="center" className={classes.modalContentContainer}>
						<div className={classes.mainContentContainer}>
							<Typography className={classes.modalTitle} variant="h6">
								Variacion permanente de salario (VSP)
							</Typography>
							<Grid container direction="column" className={classes.editableFieldsContainer}>
								<div className={classes.editableFieldsContainerFirstRow}>
									<Typography className={classes.modalTitle} variant="subtitle1">
										Nuevo salario
									</Typography>
									<MonetizationOnTwoToneIcon className={classes.modalIcons} />
									<Chip className={classes.salaryChip} label={salary ? `$${salary}` : ''} />
									<ArrowForwardIcon className={classes.modalIcons} />
									<MonetizationOnTwoToneIcon className={classes.modalIcons} />
									<TextField
										variant="outlined"
										size="small"
										className={clsx(classes.root, classes.salarytextInput)}
										placeholder={'salario'}
										InputProps={{
											className: classes.input,
											startAdornment: (
												<InputAdornment position="start">
													<AttachMoneyIcon />
												</InputAdornment>
											)
										}}
										value={newSalary}
										onChange={(event) => handleSalaryValue(event.target.value)}
									/>
								</div>
								<div className={classes.editableFieldsContainerSecondRow}>
									<Typography className={classes.modalTitle} variant="subtitle1">
										Inicio del nuevo salario
									</Typography>
									<TodayIcon className={classes.modalIcons} />
									<MuiPickersUtilsProvider utils={DateFnsUtils}>
										<DatePicker
											size="small"
											variant="inline"
											inputVariant="outlined"
											value={selectedDate}
											onChange={formatDate}
											format="yyyy-MM-dd"
											autoOk
										/>
									</MuiPickersUtilsProvider>
								</div>
							</Grid>
						</div>

						<Grid
							container
							direction="row"
							justify="flex-end"
							alignItems="center"
							style={{ paddingTop: 30 }}
						>
							<Button className={classes.cancelButton} onClick={cancelModalActions}>
								<Typography variant="subtitle2" className={classes.cancelButtonText}>
									CANCEL
								</Typography>
							</Button>

							<Button variant="contained" className={classes.saveButton} onClick={saveModalActions}>
								<Typography variant="subtitle2" className={classes.saveButtonText}>
									SAVE
								</Typography>
							</Button>
						</Grid>
					</Grid>
				</DialogContent>
			</Dialog>
		</ThemeProvider>
	);
};

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#5E35B1'
		},
		secondary: {
			main: '#f44336'
		}
	}
});

const useStyles = makeStyles({
	root: {
		'& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
			borderColor: '#5E35B1'
		},
		'&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
			borderColor: '#5E35B1'
		},
		'& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
			borderColor: '#5E35B1'
		}
	},
	modalTitle: {
		color: '#5E35B1'
	},
	modalIcons: {
		color: '#5E35B1'
	},
	modalContentContainer: {
		paddingTop: 30,
		paddingBottom: 30
	},
	mainContentContainer: {
		display: 'flex',
		flexDirection: 'column',
		width: '100%',
		height: '80%',
		justifyContent: 'flex-start'
	},
	editableFieldsContainer: {
		paddingTop: 30,
		paddingLeft: 5,
		paddingRight: 5,
		paddingBottom: 30,
		backgroundColor: '#F7F7F7 ',
		borderTop: '1px solid #5E35B1'
	},
	editableFieldsContainerFirstRow: {
		display: 'flex',
		height: 40,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row'
	},
	salaryChip: {
		height: '100%',
		width: '25%',
		color: '#5E35B1',
		backgroundColor: 'rgb(94, 53, 177,0.2)',
		borderRadius: 10,
		border: '1px solid #5E35B1'
	},
	salarytextInput: {
		width: '25%',
		height: '100%',
		background: '#FFFFFF',
		borderRadius: 10
	},
	input: {
		borderRadius: 10
	},
	editableFieldsContainerSecondRow: {
		marginTop: 15,
		display: 'flex',
		height: 40,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row'
	},
	saveButton: {
		background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
		borderRadius: 100,
		marginLeft: 10
	},
	saveButtonText: {
		color: 'white'
	},
	cancelButtonText: {
		color: '#5E35B1'
	},
	cancelButton: {
		marginRight: 10,
		borderRadius: 100
	}
});

export default ChangeSalaryModal;
