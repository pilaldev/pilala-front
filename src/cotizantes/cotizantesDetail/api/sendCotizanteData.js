import Endpoints from '@bit/pilala.pilalalib.endpoints';

export const sendCotizanteData = async (data) => {
	return new Promise((resolve, reject) => {
		fetch(Endpoints.sendCotizanteData, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto la CF de edición de cotizante',
					error,
				});
			});
	});
};

export const sendCotizanteStatus = async (data) => {
	return new Promise((resolve, reject) => {
		fetch(Endpoints.sendCotizanteStatus, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto la CF de edición de status cotizante',
					error,
				});
			});
	});
};
