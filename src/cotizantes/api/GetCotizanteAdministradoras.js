import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default async (documentType, documentNumber) => {
	let result;
	const data = JSON.stringify({
		documentType: documentType,
		documentNumber: documentNumber,
	});

	const requestOptions = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: data,
	};

	await fetch(Endpoints.getCotizanteAdministradoras, requestOptions)
		.then((response) => response.json())
		.then((apiResult) => {
			result = apiResult;
		})
		.catch((error) => {
			result = error;
			console.log('Error ejecutando fetch de obtener administradoras', error);
		});

	return result;
};
