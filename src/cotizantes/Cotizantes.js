import React, { Component } from 'react';
import { connect } from 'react-redux';
import CotizantesLayout from './CotizantesLayout';
import {
  cotizantesAction,
  cotizantesDetailAction,
  lineaRegistroAction,
  cotizantesInfoAction,
  navigationAction,
  snackBarAction,
} from './../redux/Actions';
import {
  COTIZANTES,
  COTIZANTES_DETAIL,
  COTIZANTE_INFO,
  NAVIGATION,
  SNACKBAR,
} from './../redux/ActionTypes';
import { withRouter } from 'react-router';
import Endpoints from '@bit/pilala.pilalalib.endpoints';
import { FIELDS_ID } from './Constants';
import HelperFunctions from './../utils/HelperFunctions';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import { getCotizanteAdministradoras } from './api';
import REGEX from './../utils/RegularExpressions';
import { MatchNameFields } from '../utils/MatchNameFields';
import { DownloadFile } from '../api';

class Cotizantes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cotizantes: [],
      disableStepOneButton: false,
      disableStepTwoButton: false,
    };
    this.cotizantes = [];
  }

  componentDidMount() {
    ChangePageTitle('Cotizantes | Pilalá');
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'cotizantes'
    );
    this.setCotizantesToProps();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.cotizantesInfo !== this.props.cotizantesInfo) {
      this.setCotizantesToProps();
    }
  }

  setCotizantesToProps = () => {
    let mappedCotizantes = [];
    if (this.props.cotizantesInfo[this.props.activeCompany]) {
      Object.keys(this.props.cotizantesInfo[this.props.activeCompany]).forEach(
        (key) => {
          mappedCotizantes.push({
            ...this.props.cotizantesInfo[this.props.activeCompany][key],
          });
        }
      );
    }

    // console.log('MAPPED', mappedCotizantes);

    this.setState({
      cotizantes: mappedCotizantes,
    });

    this.cotizantes = mappedCotizantes;
  };

  filterCotizantesList = (event) => {
    const text = event.target.value;
    const new_cotizantes = this.cotizantes.filter((item) => {
      const temp_name = `${item.name.display.toLowerCase()}`;
      const temp_id = `${item.documentNumber.toLowerCase()}`;
      const temp_email = `${item.email.toLowerCase()}`;
      const temp_sucursal = `${item.sucursal.toLowerCase()}`;
      const temp_type = `${item.tipoCotizante.value.toLowerCase()}`;
      const temp_active = `${item.activeInCompany.activeText.toLowerCase()}`;
      const cotizante = text.toLowerCase();
      return (
        temp_name.indexOf(cotizante) > -1 ||
        temp_id.indexOf(cotizante) > -1 ||
        temp_email.indexOf(cotizante) > -1 ||
        temp_sucursal.indexOf(cotizante) > -1 ||
        temp_type.indexOf(cotizante) > -1 ||
        temp_active.indexOf(cotizante) === 0
      );
    });
    this.setState({
      cotizantes: new_cotizantes,
    });
  };

  setShowNewCotizante = () => {
    this.props.cotizantesAction(
      COTIZANTES.COTIZANTES_OPEN_NEW_COTIZANTE_DIALOG,
      null
    );
  };

  setDetailData = (id, type, documentNumber) => {
    this.props.cotizantesDetailAction(
      COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_DATA,
      {
        id,
        type,
        documentNumber,
      }
    );
    this.props.history.push(`${this.props.match.url}/${id}`);
  };

  createCotizanteStepOne = async () => {
    if (this.state.disableStepOneButton) return;
    this.setState({
      disableStepOneButton: true,
    });

    const stepOneStatus = await this.checkCotizanteFieldsOne();
    if (stepOneStatus === true) {
      await this.checkCotizanteAdministradoras(
        this.props.identificationType,
        this.props.identificationNumber
      );
      this.props.cotizantesAction(
        COTIZANTES.COTIZANTES_CHANGE_CARD_TYPE_PROP,
        null
      );
      this.setState({
        disableStepOneButton: false,
      });
    } else {
      this.setState({
        disableStepOneButton: false,
      });
    }
  };

  createCotizanteStepTwo = async () => {
    if (this.state.disableStepTwoButton) return;
    this.setState({
      disableStepTwoButton: true,
    });

    const stepTwoStatus = await this.checkCotizanteFieldsTwo();
    if (stepTwoStatus === true) {
      const cotizanteDataToLoad = {
        tipoCotizante: this.props.cotizanteType,
        nitCompany: this.props.activeCompanyNit,
        idSucursal: this.props.idCompany,
        idCotizante: null,
        location: {
          province: this.props.cotizanteProvince,
          city: this.props.cotizanteCity,
        },
        adminPlatform: {
          email: this.props.adminEmail,
        },
        cotizanteData: {
          name: this.props.cotizanteName,
          documentType: this.props.identificationType,
          documentNumber: this.props.identificationNumber,
          genre: this.props.cotizanteGenre,
          arl: this.props.cotizanteArl,
          eps: this.props.cotizanteEps,
          pension: this.props.cotizantePension,
          nivelArl: this.props.cotizanteNivelArl,
          salary: this.props.salary,
          sucursal: this.props.cotizanteSucursal,
          email: this.props.cotizanteEmail,
          subtipo: this.props.cotizanteSubtype,
          minSalary: this.props.cotizanteMinSalary,
          integralSalary: this.props.cotizanteIntegralSalary,
          newEntry: this.props.enableJoinDate,
          dateEntry: this.props.joinDate.value,
          cajaCompensacion: this.props.cotizanteCajaCompensacion,
          mobileNumber: '',
        },
      };

      fetch(Endpoints.sendCotizanteData, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(cotizanteDataToLoad),
      })
        .then((response) => response.json())
        .then(async (result) => {
          if (result.status === 'SUCCESS') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Cotizante creado correctamente',
              description:
                'Se ha creado el nuevo cotizante en tu aportante actual',
              color: 'success',
            });
            const cotizanteFullData = {
              ...cotizanteDataToLoad.cotizanteData,
              tipoCotizante: this.props.cotizanteType,
              location: cotizanteDataToLoad.location,
              news: [],
              dateRetired: null,
              firstPila: false,
              transitorySalary: {
                code: null,
                dateCreated: null,
                dateApply: null,
                value: null,
              },
              pilaHistory: [],
              dateUpdate: null,
              dateCreated: null,
              transitorySalaryVariation: false,
              id: result.cotizante.id,
              activeInCompany: {
                value: true,
                activeText: 'active',
              },
              salaryVariation: false,
              retired: false,
              deleteDate: null,
              subsistemas: {
                sena: {
                  display: 'SENA',
                  value: 0,
                },
                arl: {
                  display: 'ARL',
                  value: 0,
                },
                icbf: {
                  display: 'ICBF',
                  value: 0,
                },
                salud: {
                  display: 'SALUD',
                  value: 0,
                },
                pension: {
                  display: 'PENSIÓN',
                  value: 0,
                },
                ccf: {
                  display: 'CCF',
                  value: 0,
                },
              },
              deleteInCompany: false,
              salaryHistory: [],
            };
            await this.props.cotizantesInfoAction(
              COTIZANTE_INFO.COTIZANTE_INFO_ADD_NEW_COTIZANTE,
              {
                cotizanteData: cotizanteFullData,
                activeCompany: this.props.activeCompany,
                documentNumber: this.props.identificationNumber,
              }
            );
            this.props.cotizantesAction(
              COTIZANTES.COTIZANTES_OPEN_NEW_COTIZANTE_DIALOG,
              null
            );
            this.props.cotizantesAction(
              COTIZANTES.COTIZANTES_RESET_COTIZANTE_FIELDS,
              null
            );
            this.setState({
              disableStepTwoButton: false,
            });
            return;
          } else if (result.status === 'FAILED') {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al crear el cotizante',
              description:
                'Hemos tenido problemas para crear el cotizante, inténtelo nuevamente',
              color: 'error',
            });
            this.setState({
              disableStepTwoButton: false,
            });
          }
        })
        .catch((error) => {
          console.log('ERROR', error);
          this.setState({
            disableStepTwoButton: false,
          });
        });
    } else {
      this.setState({
        disableStepTwoButton: false,
      });
    }
  };

  goToPila = () => {
    this.props.history.push(`/${this.props.userUid}/console/liquidarPila/1`);
  };

  checkCotizanteFieldsOne = async () => {
    if (this.props.identificationType === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.IDENTIFICATION_TYPE
        )
      ) {
        await this.addRequiredField(FIELDS_ID.IDENTIFICATION_TYPE);
      }
    }

    if (this.props.identificationNumber === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.IDENTIFICATION_NUMBER
        )
      ) {
        await this.addRequiredField(FIELDS_ID.IDENTIFICATION_NUMBER);
      }
    }

    if (this.props.cotizanteType.code === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_TYPE)
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_TYPE);
      }
    }

    if (this.props.cotizanteSubtype.code === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.COTIZANTE_SUBTYPE
        )
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_SUBTYPE);
      }
    }

    if (this.props.salary.code === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.COTIZANTE_SALARY
        )
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_SALARY);
      }
    }

    if (this.props.enableJoinDate === true) {
      if (this.props.joinDate.code === '') {
        if (
          !this.props.cotizantesRequiredFields.includes(FIELDS_ID.JOIN_DATE)
        ) {
          await this.addRequiredField(FIELDS_ID.JOIN_DATE);
        }
      }
    }

    if (this.props.cotizantesRequiredFields.length === 0) {
      return true;
    } else {
      return false;
    }
  };

  checkCotizanteFieldsTwo = async () => {
    if (this.props.requestBDUA) {
      this.props.cotizantesAction(
        COTIZANTES.COTIZANTES_CHANGE_BDUA_REQUEST_STATUS,
        null
      );
    }

    if (this.props.cotizanteName.display === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_NAME)
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_NAME);
      }
    } else {
      if (!REGEX.VALID_NAME.test(this.props.cotizanteName.display)) {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description:
            'Los nombres y apellidos del cotizante deben ser de mínimo 2 letras',
          color: 'warning',
        });
        if (
          !this.props.cotizantesRequiredFields.includes(
            FIELDS_ID.COTIZANTE_NAME
          )
        ) {
          await this.addRequiredField(FIELDS_ID.COTIZANTE_NAME);
        }
      }
    }

    if (this.props.cotizanteType.code === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_TYPE)
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_TYPE);
      }
    }

    if (this.props.cotizanteSubtype.code === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.COTIZANTE_SUBTYPE
        )
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_SUBTYPE);
      }
    }

    if (this.props.identificationType === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.IDENTIFICATION_TYPE
        )
      ) {
        await this.addRequiredField(FIELDS_ID.IDENTIFICATION_TYPE);
      }
    }

    if (this.props.identificationNumber === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.IDENTIFICATION_NUMBER
        )
      ) {
        await this.addRequiredField(FIELDS_ID.IDENTIFICATION_NUMBER);
      }
    }

    if (this.props.cotizanteGenre.value === -1) {
      if (
        !this.props.cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_GENRE)
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_GENRE);
      }
    }

    if (this.props.cotizanteEps.code === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SALUD)
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_SALUD);
      }
    }

    if (this.props.cotizantePension.code === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.COTIZANTE_PENSION
        )
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_PENSION);
      }
    }

    if (this.props.cotizanteNivelArl.code === -1) {
      if (!this.props.cotizantesRequiredFields.includes(FIELDS_ID.ARL_RISK)) {
        await this.addRequiredField(FIELDS_ID.ARL_RISK);
      }
    }

    if (this.props.salary.code === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(
          FIELDS_ID.COTIZANTE_SALARY
        )
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_SALARY);
      }
    }

    if (this.props.cotizanteEmail === '') {
      if (
        !this.props.cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_EMAIL)
      ) {
        await this.addRequiredField(FIELDS_ID.COTIZANTE_EMAIL);
      }
    }

    if (this.props.cotizantesRequiredFields.length === 0) {
      return true;
    } else {
      if (this.props.initialFields) {
        this.props.cotizantesAction(
          COTIZANTES.COTIZANTES_CHANGE_INITIAL_FIELDS,
          false
        );
      }
      return false;
    }
  };

  addRequiredField = async (field) => {
    const data = {
      field,
      status: true,
    };

    await this.props.cotizantesAction(
      COTIZANTES.COTIZANTES_REQUIRED_FIELD,
      data
    );
  };

  checkCotizanteAdministradoras = async (tipoDocumento, numeroDocumento) => {
    const adminsResult = await getCotizanteAdministradoras(
      tipoDocumento,
      numeroDocumento
    );
    if (adminsResult.status === 'SUCCESS') {
      if (adminsResult.data.administradoraBDUA !== '') {
        const eps = HelperFunctions.matchInObjectArray(
          adminsResult.data.administradoraBDUA,
          'code',
          this.props.epsData
        );
        this.props.cotizantesAction(
          COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALUD,
          eps
        );
      }
      if (adminsResult.data.administradoraRUAF !== '') {
        const afp = HelperFunctions.matchInObjectArray(
          adminsResult.data.administradoraRUAF,
          'code',
          this.props.afpData
        );
        this.props.cotizantesAction(
          COTIZANTES.COTIZANTES_SAVE_COTIZANTE_PENSION,
          afp
        );
      }
      if (
        adminsResult.data.administradoraBDUA === '' &&
        adminsResult.data.administradoraRUAF === ''
      ) {
        this.props.cotizantesAction(
          COTIZANTES.COTIZANTES_CHANGE_BDUA_REQUEST_STATUS,
          null
        );
      }
      const nameData = MatchNameFields(
        this.capitalizeName(adminsResult.data.name.display)
      );
      const newName = {
        display: nameData.displayName,
        firstName: nameData.firstName,
        secondName: nameData.secondName,
        firstSurname: nameData.firstSurname,
        secondSurname: nameData.secondSurname,
      };
      this.props.cotizantesAction(
        COTIZANTES.COTIZANTES_SAVE_COTIZANTE_NAME,
        newName
      );
    } else {
      this.props.cotizantesAction(
        COTIZANTES.COTIZANTES_CHANGE_BDUA_REQUEST_STATUS,
        null
      );
    }
  };

  getCotizantesTemplate = async () => {
    const templateResult = await DownloadFile();
    if (templateResult.status === 'SUCCESS') {
      window.open(templateResult.data.url, '_blank');
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Error al descargar la plantilla',
        description:
          'Hemos tenido problemas para descargar la plantilla, inténtalo nuevamente',
        color: 'error',
      });
    }
  };

  showCreateCotizanteOptions = () => {
    this.props.cotizantesAction(COTIZANTES.COTIZANTES_SHOW_OPTIONS_MODAL, true);
  };

  capitalizeName = (name) => {
    const splitName = name.toLowerCase().split(' ');
    for (let i = 0; i < splitName.length; i++) {
      splitName[i] =
        splitName[i].charAt(0).toUpperCase() + splitName[i].substring(1);
    }
    return splitName.join(' ');
  };

  render() {
    const layoutProps = {
      cotizantes: this.state.cotizantes,
      filterCotizantesList: this.filterCotizantesList,
      showNewCotizante: this.props.openNewCotizanteDialog,
      setShowNewCotizante: this.setShowNewCotizante,
      setDetailData: this.setDetailData,
      createCotizanteStepOne: this.createCotizanteStepOne,
      createCotizanteStepTwo: this.createCotizanteStepTwo,
      goToPila: this.goToPila,
      disableStepOneButton: this.state.disableStepOneButton,
      disableStepTwoButton: this.state.disableStepTwoButton,
      getCotizantesTemplate: this.getCotizantesTemplate,
      showCreateCotizanteOptions: this.showCreateCotizanteOptions,
    };

    return <CotizantesLayout {...layoutProps} />;
  }
}

const mapStateToProps = (state) => {
  return {
    openNewCotizanteDialog: state.cotizantes.openNewCotizanteDialog,
    activeCompany: state.currentUser.activeCompany,
    activeCompanyNit: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany].nit
      : null,
    companyName: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
          .isCompany
        ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .companyName
        : state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .sucursalName
      : null,
    cotizantesInfo: state.cotizantesInfo,
    identificationType: state.cotizantes.identificationType,
    identificationNumber: state.cotizantes.identificationNumber,
    cotizanteType: state.cotizantes.cotizanteType,
    cotizanteSubtype: state.cotizantes.cotizanteSubtype,
    salary: state.cotizantes.salary,
    joinDate: state.cotizantes.joinDate,
    enableJoinDate: state.cotizantes.enableJoinDate,
    companyNit: state.company.identificationNumber,
    companyProvince: state.company.province,
    companyCity: state.company.city,
    adminName: state.currentUser.name,
    adminEmail: state.currentUser.email,
    cotizanteName: state.cotizantes.cotizanteName,
    cotizanteGenre: state.cotizantes.cotizanteGenre,
    cotizanteArl: state.company.arl,
    cotizanteEps: state.cotizantes.cotizanteSalud,
    cotizantePension: state.cotizantes.cotizantePension,
    cotizanteNivelArl: state.cotizantes.arlRisk,
    cotizanteSucursal: state.cotizantes.cotizanteSucursal,
    cotizanteEmail: state.cotizantes.cotizanteEmail,
    cotizanteMinSalary: state.cotizantes.selectedMinSalary,
    cotizanteIntegralSalary: state.cotizantes.selectedIntegralSalary,
    cotizanteCajaCompensacion: state.cotizantes.cajaCompensacion,
    userUid: state.currentUser.uid,
    companyArlRisk: state.company.claseArl,
    cotizantesRequiredFields: state.cotizantes.cotizantesRequiredFields,
    epsData: state.resources.eps,
    afpData: state.resources.afp,
    initialFields: state.cotizantes.initialFields,
    idCompany: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
          .isCompany
        ? state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .idCompany
        : state.currentUser.companiesInfo[state.currentUser.activeCompany]
            .idSucursal
      : null,
    requestBDUA: state.cotizantes.requestBDUA,
    cotizanteProvince: state.cotizantes.cotizanteProvince,
    cotizanteCity: state.cotizantes.cotizanteCity,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cotizantesAction: (actionType, value) =>
      dispatch(cotizantesAction(actionType, value)),
    cotizantesDetailAction: (actionType, value) =>
      dispatch(cotizantesDetailAction(actionType, value)),
    lineaRegistroAction: (actionType, value) =>
      dispatch(lineaRegistroAction(actionType, value)),
    cotizantesInfoAction: (actionType, value) =>
      dispatch(cotizantesInfoAction(actionType, value)),
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Cotizantes)
);
