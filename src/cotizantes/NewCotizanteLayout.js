import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import {
    Typography,
    Grid,
    ButtonBase,
    Dialog,
    DialogTitle,
    IconButton,
    TextField,
    InputAdornment,
    MenuItem,
    Checkbox,
    Button,
    CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import CheckBoxTwoToneIcon from '@material-ui/icons/CheckBoxTwoTone';
import CheckBoxOutlineBlankRoundedIcon from '@material-ui/icons/CheckBoxOutlineBlankRounded';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import TodayTwoToneIcon from '@material-ui/icons/TodayTwoTone';
import {
    CARD_TITLE,
    COTIZANTE_DATA_TITLE,
    ID_VALUES,
    SALARY,
    SMLMV,
    // UPLOAD_FILE_TITLE,
    // DOWNLOAD_FILE_BUTTON,
    // UPLOAD_FILE_BUTTON,
    CANCEL_BUTTON,
    CONTINUE_BUTTON,
    FIELDS_ID,
    FIELDS_HELPER_TEXT,
    CREATE_COTIZANTE,
    INTEGRAL,
    JOIN_DATE_TITLE,
    GENRES
} from './Constants';
import { cotizantesAction } from './../redux/Actions';
import { COTIZANTES } from './../redux/ActionTypes';
import logo from './../assets/img/men_one.svg';
import { MatchNameFields } from './../utils/MatchNameFields';
import moment from 'moment';
import 'moment/locale/es';
import clsx from 'clsx';
import { Autocomplete } from '@material-ui/lab';
import REGEX from './../utils/RegularExpressions';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import MaskedInput from 'react-text-mask';
import { Calendar } from '../components';
import { CAJA_COMPENSACION, CUSTOM_COMPANY_CCF } from '../company/Constants';
import ModalCCF from '../company/ModalCCF';

moment.locale('es');

const MaskedSalaryInput = (props) => {
    const { inputRef, ...other } = props;
    const salaryMask = createNumberMask({
        prefix: '',
        thousandsSeparatorSymbol: '.',
        allowDecimal: false
    });

    return (
        <MaskedInput
            {...other}
            mask={salaryMask}
            ref={(ref) => {
                inputRef(ref ? ref.inputElement : null);
            }}
            guide={false}
        />
    );
}

const NewCotizanteLayout = (props) => {

    const classes = FirstCardStyles();
    const classes2 = SecondCardStyles();
    const requiredFieldClasses = requiredFieldStyles();
    const dispatch = useDispatch();
    const identificationType = useSelector(state => state.cotizantes.identificationType);
    const identificationNumber = useSelector(state => state.cotizantes.identificationNumber);
    const cotizanteType = useSelector(state => state.cotizantes.cotizanteType);
    const cotizanteSubtype = useSelector(state => state.cotizantes.cotizanteSubtype);
    const salary = useSelector(state => state.cotizantes.salary);
    const smlmvValue = useSelector(state => state.resources.smlmv);
    const cotizanteName = useSelector(state => state.cotizantes.cotizanteName.display);
    const cotizanteGenre = useSelector(state => state.cotizantes.cotizanteGenre);
    const cotizanteSalud = useSelector(state => state.cotizantes.cotizanteSalud);
    const cotizantePension = useSelector(state => state.cotizantes.cotizantePension);
    const arlRisk = useSelector(state => state.cotizantes.arlRisk);
    // const cotizanteSucursal = useSelector(state => state.cotizantes.cotizanteSucursal);
    const cotizanteEmail = useSelector(state => state.cotizantes.cotizanteEmail);
    const cotizanteCard = useSelector(state => state.cotizantes.cotizanteCard);
    const [showDatePicker, setShowDatePicker] = useState(false);
    const joinDate = useSelector(state => state.cotizantes.joinDate);
    const [joinDateAnchor, setJoinDateAnchor] = useState({});
    const enableJoinDate = useSelector(state => state.cotizantes.enableJoinDate);
    const integralSalary = useSelector(state => state.resources.integralSalary);
    const subtiposCotizantes = useSelector(state => state.resources.subtiposCotizantes);
    const tiposCotizantes = useSelector(state => state.resources.tiposCotizantes);
    const EPS = useSelector(state => state.resources.eps);
    const AFP = useSelector(state => state.resources.afp);
    const ARL_RISK = useSelector(state => state.resources.clasesArl);
    const companyName = useSelector(state => state.company.companyName);
    const selectedMinSalary = useSelector(state => state.cotizantes.selectedMinSalary);
    const selectedIntegralSalary = useSelector(state => state.cotizantes.selectedIntegralSalary);
    const cotizantesRequiredFields = useSelector(state => state.cotizantes.cotizantesRequiredFields);
    const initialFields = useSelector(state => state.cotizantes.initialFields);
    const requestBDUA = useSelector(state => state.cotizantes.requestBDUA);
    const activeCompany = useSelector(state => state.currentUser.activeCompany);
    const periodoActual = useSelector(state => state.currentUser.periodosInfo[activeCompany]);
    const cajaCompensacion = useSelector(state => state.cotizantes.cajaCompensacion);
    const [openCCFModal, setOpenCCFModal] = useState(false);
    const companyCCF = useSelector(state => state.company.cajaCompensacion);
    const provinces = useSelector(state => state.resources.provinces);
    const cities = useSelector(state => state.resources.cities);
    const cotizanteProvince = useSelector(state => state.cotizantes.cotizanteProvince);
    const cotizanteCity = useSelector(state => state.cotizantes.cotizanteCity);
    const companyProvince = useSelector(state => state.company.province);
    const companyCity = useSelector(state => state.company.city);
    const tiposCotizantesFull = [
        {
            code: '',
            display: 'Tipo empleado',
        },
        ...tiposCotizantes
    ];
    const subtiposCotizantesFull = [
        {
            code: '',
            display: 'Subtipo empleado',
        },
        ...subtiposCotizantes
    ];
    const epsFull = [
        {
            code: '',
            display: 'Salud',
            value: {}
        },
        ...EPS
    ];
    const afpFull = [
        {
            code: '',
            display: 'Pensión',
            value: {}
        },
        ...AFP
    ];
    const arlRiskFull = [
        {
            code: -1,
            display: 'Nivel de riesgo',
            value: 'nivel de riesgo'
        },
        ...ARL_RISK
    ];

    useEffect(() => {
        if (props.showNewCotizante) {
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SUCURSAL, companyName));
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_CCF, companyCCF));
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_PROVINCE, companyProvince));
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_CITY, companyCity));
        }

    }, [companyCCF, companyCity, companyName, companyProvince, dispatch, props.showNewCotizante]);

    const saveCotizanteFieldValue = (event) => {
        const value = event.target.value;
        const fieldId = event.target.id;
        const fieldName = event.target.name;

        switch (fieldId || fieldName) {

            case FIELDS_ID.IDENTIFICATION_TYPE:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_IDENTIFICATION_TYPE, value));
                if (identificationType === '') {
                    removeRequiredField(FIELDS_ID.IDENTIFICATION_TYPE);
                }
                break;

            case FIELDS_ID.IDENTIFICATION_NUMBER:
                if (value === '' || REGEX.NUMBERS.test(value)) {
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_IDENTIFICATION_NUMBER, value));
                }
                if (identificationNumber === '') {
                    removeRequiredField(FIELDS_ID.IDENTIFICATION_NUMBER);
                }
                break;

            case FIELDS_ID.COTIZANTE_TYPE:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_TYPE, value));
                if (cotizanteType.code === '') {
                    removeRequiredField(FIELDS_ID.COTIZANTE_TYPE);
                }
                break;

            case FIELDS_ID.COTIZANTE_SUBTYPE:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SUBTYPE, value));
                if (cotizanteSubtype.code === '') {
                    removeRequiredField(FIELDS_ID.COTIZANTE_SUBTYPE);
                }
                break;

            case FIELDS_ID.COTIZANTE_SALARY:
                const data = {
                    dateCreated: moment().format('YYYY-MM-DD'),
                    value: value.replace(/[.\s]/g, ''),
                    dateApply: '',
                    code: value.replace(/[.\s]/g, '')
                }
                if (selectedIntegralSalary) {
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SELECT_INTEGRAL_SALARY, false));
                }
                if (selectedMinSalary) {
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SELECT_MIN_SALARY, false));
                }
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALARY, data));
                if (salary.code === '') {
                    removeRequiredField(FIELDS_ID.COTIZANTE_SALARY);
                }
                break;

            case FIELDS_ID.SMLMV_CHECK:
                if (!selectedMinSalary) {
                    const data = {
                        dateCreated: '',
                        value: smlmvValue,
                        dateApply: moment().format('YYYY-MM-DD'),
                        code: smlmvValue
                    }
                    if (selectedIntegralSalary) {
                        dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SELECT_INTEGRAL_SALARY, false));
                    }
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALARY, data));
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SELECT_MIN_SALARY, true));
                }
                else {
                    const data = {
                        dateCreated: '',
                        value: '',
                        dateApply: '',
                        code: ''
                    }
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALARY, data));
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SELECT_MIN_SALARY, false));
                }
                if (salary.code === '') {
                    removeRequiredField(FIELDS_ID.COTIZANTE_SALARY);
                }
                break;

            case FIELDS_ID.INTEGRAL_CHECK:
                if (!selectedIntegralSalary) {
                    const data = {
                        dateCreated: '',
                        value: integralSalary,
                        dateApply: moment().format('YYYY-MM-DD'),
                        code: integralSalary
                    }
                    if (selectedMinSalary) {
                        dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SELECT_MIN_SALARY, false));
                    }
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALARY, data));
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SELECT_INTEGRAL_SALARY, true));
                }
                else {
                    const data = {
                        dateCreated: '',
                        value: '',
                        dateApply: '',
                        code: ''
                    }
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALARY, data));
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SELECT_INTEGRAL_SALARY, false));
                }
                if (salary.code === '') {
                    removeRequiredField(FIELDS_ID.COTIZANTE_SALARY);
                }
                break;

            case FIELDS_ID.COTIZANTE_NAME:
                if (value === '' || REGEX.PERSON_NAME.test(value)) {
                    const name_data = MatchNameFields(value);
                    const new_name = {
                        display: name_data.displayName,
                        firstName: name_data.firstName,
                        secondName: name_data.secondName,
                        firstSurname: name_data.firstSurname,
                        secondSurname: name_data.secondSurname
                    }
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_NAME, new_name));
                    if (cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_NAME)) {
                        removeRequiredField(FIELDS_ID.COTIZANTE_NAME);
                    }
                }
                break;

            case FIELDS_ID.COTIZANTE_GENRE:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_GENRE, value));
                if (cotizanteGenre.value === -1) {
                    removeRequiredField(FIELDS_ID.COTIZANTE_GENRE);
                }
                break;

            case FIELDS_ID.ARL_RISK:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_ARL_RISK, value));
                if (arlRisk.code === -1) {
                    removeRequiredField(FIELDS_ID.ARL_RISK);
                }
                break;

            case FIELDS_ID.COTIZANTE_SUCURSAL:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SUCURSAL, value));
                break;

            case FIELDS_ID.COTIZANTE_EMAIL:
                if (value === '' || REGEX.EMAIL_CHARACTERS.test(value)) {
                    dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_EMAIL, value));
                    if (cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_EMAIL)) {
                        removeRequiredField(FIELDS_ID.COTIZANTE_EMAIL);
                    }
                }
                break;

            default:
                return;
        }
    }

    const closeOpenedDialog = () => {
        props.setShowNewCotizante();
        dispatch(cotizantesAction(COTIZANTES.COTIZANTES_RESET_COTIZANTE_FIELDS, null));
    }

    const cancelCreateNewCotizante = () => {
        props.setShowNewCotizante();
        dispatch(cotizantesAction(COTIZANTES.COTIZANTES_RESET_COTIZANTE_FIELDS, null));
    }

    const nextStepCreateNewCotizante = (e) => {
        e.preventDefault();
        props.createCotizanteStepOne();
    }

    const resetCotizanteCardLayout = () => {
        if (cotizanteCard) {
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_CHANGE_CARD_TYPE_PROP, null));
        }
    }

    const openDatePicker = (event) => {
        setJoinDateAnchor(event.currentTarget);
        setShowDatePicker(!showDatePicker);
    }

    const selectJoinDate = (data) => {
        const value = {
            display: moment(data).format('DD/MM/YYYY'),
            code: moment(data).format('YYYY-MM-DD'),
            value: moment(data).format('DD/MM/YYYY')
        }
        dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_JOIN_DATE, value));
        setShowDatePicker(!showDatePicker);
        if (joinDate.code === '') {
            removeRequiredField(FIELDS_ID.JOIN_DATE);
        }
    }

    const enableJoinDateCalendar = () => {
        if (!enableJoinDate) {
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_ENABLE_JOIN_DATE, null));
        }
        else {
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_ENABLE_JOIN_DATE, null));
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_RESET_JOIN_DATE, null));
        }
    }

    const getListValue = (event, value, reason) => {
        const fieldId = event.target.id.split('-')[0];
        switch (fieldId) {
            case FIELDS_ID.COTIZANTE_SALUD:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_SALUD, value));
                if (cotizanteSalud.code === '') {
                    removeRequiredField(FIELDS_ID.COTIZANTE_SALUD);
                }
                break;

            case FIELDS_ID.COTIZANTE_PENSION:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_COTIZANTE_PENSION, value));
                if (cotizantePension.code === '') {
                    removeRequiredField(FIELDS_ID.COTIZANTE_PENSION);
                }
                break;

            case FIELDS_ID.PROVINCE:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_PROVINCE, value));
                break;

            case FIELDS_ID.CITY:
                dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_CITY, value));
                break;

            default:
                return;
        }
    }

    const createNewCotizante = (e) => {
        e.preventDefault();
        props.createCotizanteStepTwo();
    }

    const removeRequiredField = (field) => {
        const data = {
            field,
            status: false
        }
        dispatch(cotizantesAction(COTIZANTES.COTIZANTES_REQUIRED_FIELD, data));
    }

    const getCotizanteCCF = () => {
        if (!openCCFModal) {
            setOpenCCFModal(true);
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_COPY_CCF, companyCCF));
        }
    }

    const closeCCFModal = () => {
        if (openCCFModal) {
            setOpenCCFModal(false);
            dispatch(cotizantesAction(COTIZANTES.COTIZANTES_COPY_CCF, null));
        }
    }

    const selectCCF = (item) => {
        dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SAVE_CCF, item));
        if (openCCFModal) {
            setOpenCCFModal(false);
        }
    }

    const filterCitiesList = (currentOptions, optionsState) => {

        if (optionsState.inputValue !== '') {
            return currentOptions.filter(item => item.provinceCode === cotizanteProvince.code && item.value.indexOf(optionsState.inputValue.toLowerCase()) > -1);
        }
        else {
            return cities.filter(item => item.provinceCode === cotizanteProvince.code);
        }
    }

    return (
        <Dialog
            open={props.showNewCotizante}
            fullWidth={true}
            maxWidth='md'
            onExited={resetCotizanteCardLayout}
            PaperProps={{
                className: classes.paperContainer
            }}
            BackdropProps={{
                className: classes.backdrop
            }}
            className={classes.dialog}
        >
            <DialogTitle
                disableTypography
                className={classes.dialogTitleContainer}
            >
                <Grid
                    container
                    direction='row'
                    justify='space-between'
                    alignItems='center'
                >
                    <Typography
                        variant='h6'
                        className={classes.cardTitle}
                    >
                        {CARD_TITLE}
                    </Typography>
                    <IconButton
                        onClick={closeOpenedDialog}
                    >
                        <CloseIcon />
                    </IconButton>
                </Grid>
            </DialogTitle>
            {
                (!cotizanteCard) ?
                    <form>
                        <Grid
                            container
                            direction='column'
                        >
                            <Grid
                                container
                                direction='column'
                            >
                                <div
                                    className={classes.cardSubtitle}
                                >
                                    <Typography
                                        variant='subtitle2'
                                        className={classes.cardSubtitleText}
                                    >
                                        {COTIZANTE_DATA_TITLE}
                                    </Typography>
                                </div>
                                <Grid
                                    container
                                    direction='row'
                                    justify='space-evenly'
                                    className={classes.firstFieldsContainer}
                                >
                                    <TextField
                                        className={classes.field}
                                        placeholder={FIELDS_HELPER_TEXT.IDENTIFICATION_NUMBER}
                                        helperText={FIELDS_HELPER_TEXT.IDENTIFICATION_NUMBER}
                                        value={identificationNumber}
                                        onChange={saveCotizanteFieldValue}
                                        id={FIELDS_ID.IDENTIFICATION_NUMBER}
                                        inputProps={{
                                            style: {
                                                backgroundColor: cotizantesRequiredFields.includes(FIELDS_ID.IDENTIFICATION_NUMBER) ? '#FF495A26' : 'transparent'
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: cotizantesRequiredFields.includes(FIELDS_ID.IDENTIFICATION_NUMBER) ? requiredFieldClasses.underlineRequired : classes.underline
                                            },
                                            startAdornment:
                                                <InputAdornment
                                                    position='start'
                                                >
                                                    <TextField
                                                        select={true}
                                                        value={identificationType}
                                                        name={FIELDS_ID.IDENTIFICATION_TYPE}
                                                        onChange={saveCotizanteFieldValue}
                                                        InputProps={{
                                                            disableUnderline: true
                                                        }}
                                                        SelectProps={{
                                                            classes: {
                                                                selectMenu: cotizantesRequiredFields.includes(FIELDS_ID.IDENTIFICATION_TYPE) ? requiredFieldClasses.selectMenu : null
                                                            }
                                                        }}
                                                    >
                                                        {
                                                            ID_VALUES.map((item, index) => (
                                                                <MenuItem key={index} value={item}>
                                                                    {item}
                                                                </MenuItem>
                                                            ))
                                                        }
                                                    </TextField>
                                                </InputAdornment>
                                        }}
                                    />
                                    <TextField
                                        select={true}
                                        helperText={FIELDS_HELPER_TEXT.COTIZANTE_TYPE}
                                        value={tiposCotizantesFull.find(type => type.code === cotizanteType.code)}
                                        onChange={saveCotizanteFieldValue}
                                        className={classes.field}
                                        name={FIELDS_ID.COTIZANTE_TYPE}
                                        SelectProps={{
                                            classes: {
                                                selectMenu: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_TYPE) ? requiredFieldClasses.selectMenu : null
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_TYPE) ? requiredFieldClasses.underlineRequired : classes.underline
                                            }
                                        }}
                                    >
                                        {
                                            tiposCotizantesFull.map((item, index) => (
                                                <MenuItem key={index} value={item} disabled={index === 0}>
                                                    {item.display}
                                                </MenuItem>
                                            ))
                                        }
                                    </TextField>
                                    <TextField
                                        select={true}
                                        helperText={FIELDS_HELPER_TEXT.COTIZANTE_SUBTYPE}
                                        value={subtiposCotizantesFull.find(subtype => subtype.code === cotizanteSubtype.code)}
                                        onChange={saveCotizanteFieldValue}
                                        className={classes.field}
                                        name={FIELDS_ID.COTIZANTE_SUBTYPE}
                                        SelectProps={{
                                            classes: {
                                                selectMenu: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SUBTYPE) ? requiredFieldClasses.selectMenu : null
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SUBTYPE) ? requiredFieldClasses.underlineRequired : classes.underline
                                            }
                                        }}
                                    >
                                        {
                                            subtiposCotizantesFull.map((item, index) => (
                                                <MenuItem key={index} value={item} disabled={index === 0}>
                                                    {item.display}
                                                </MenuItem>
                                            ))
                                        }
                                    </TextField>
                                    <div
                                        className={classes.salaryContainer}
                                    >
                                        <TextField
                                            placeholder={SALARY}
                                            value={salary.value}
                                            onChange={saveCotizanteFieldValue}
                                            className={classes.salaryTextfield}
                                            id={FIELDS_ID.COTIZANTE_SALARY}
                                            InputProps={{
                                                startAdornment: <InputAdornment position='start'>$</InputAdornment>,
                                                classes: {
                                                    underline: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SALARY) ? requiredFieldClasses.underlineRequired : classes.underline
                                                },
                                                inputComponent: MaskedSalaryInput
                                            }}
                                            inputProps={{
                                                style: {
                                                    backgroundColor: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SALARY) ? '#FF495A26' : 'transparent'
                                                }
                                            }}
                                        />
                                        <div
                                            className={classes.salaryHelperTextContainer}
                                        >
                                            <div
                                                className={classes.salaryCheckboxContainer}
                                            >
                                                <Checkbox
                                                    checked={selectedMinSalary}
                                                    id={FIELDS_ID.SMLMV_CHECK}
                                                    onChange={saveCotizanteFieldValue}
                                                    color='default'
                                                    className={classes.salaryCheckbox}
                                                    checkedIcon={
                                                        <CheckBoxTwoToneIcon className={classes.checkedIcon} />
                                                    }
                                                    icon={
                                                        <CheckBoxOutlineBlankRoundedIcon className={classes.uncheckedIcon} />
                                                    }
                                                />
                                                <Typography
                                                    className={classes.smlmvText}
                                                >
                                                    {SMLMV}
                                                </Typography>
                                            </div>
                                            <div
                                                className={classes.salaryCheckboxContainer}
                                            >
                                                <Checkbox
                                                    checked={selectedIntegralSalary}
                                                    id={FIELDS_ID.INTEGRAL_CHECK}
                                                    onChange={saveCotizanteFieldValue}
                                                    color='default'
                                                    className={classes.salaryCheckbox}
                                                    checkedIcon={
                                                        <CheckBoxTwoToneIcon className={classes.checkedIcon} />
                                                    }
                                                    icon={
                                                        <CheckBoxOutlineBlankRoundedIcon className={classes.uncheckedIcon} />
                                                    }
                                                />
                                                <Typography
                                                    className={classes.smlmvText}
                                                >
                                                    {INTEGRAL}
                                                </Typography>
                                            </div>
                                        </div>
                                    </div>
                                </Grid>
                                <Grid
                                    container
                                    direction='row'
                                    className={classes.joinDateContainer}
                                >
                                    <div className={classes.joinDateSecondContainer}>
                                        <div className={classes.joinDateCheckContainer}>
                                            <ButtonBase
                                                onClick={enableJoinDateCalendar}
                                            >
                                                <Checkbox
                                                    checked={enableJoinDate}
                                                    disabled={true}
                                                    color='secondary'
                                                    checkedIcon={
                                                        <CheckBoxTwoToneIcon className={classes.joinDateCheckBoxChecked} />
                                                    }
                                                    icon={
                                                        <CheckBoxOutlineBlankRoundedIcon />
                                                    }
                                                />
                                                <Typography>
                                                    {JOIN_DATE_TITLE}
                                                </Typography>
                                            </ButtonBase>
                                            <ArrowForwardRoundedIcon className={classes.joinDateArrowIcon} />
                                        </div>
                                        <div className={classes.joinDateCalendarContainer}>
                                            <IconButton
                                                onClick={openDatePicker}
                                                disabled={!enableJoinDate}
                                            >
                                                <TodayTwoToneIcon
                                                    className={clsx(
                                                        classes.joinDateCalendarIcon, {
                                                        [classes.joinDateActiveColor]: enableJoinDate
                                                    }
                                                    )}
                                                />
                                            </IconButton>
                                            <ButtonBase
                                                className={clsx(
                                                    classes.joinDateCalendarButton, {
                                                    [classes.joinDateActiveButton]: enableJoinDate,
                                                    [classes.joinDateRequiredButton]: cotizantesRequiredFields.includes(FIELDS_ID.JOIN_DATE)
                                                }
                                                )}
                                                onClick={openDatePicker}
                                                disabled={!enableJoinDate}
                                            >
                                                <Typography
                                                    className={clsx(
                                                        classes.joinDateText, {
                                                        [classes.joinDateActiveColor]: enableJoinDate,
                                                        [classes.joinDateRequiredColor]: cotizantesRequiredFields.includes(FIELDS_ID.JOIN_DATE)
                                                    }
                                                    )}
                                                >
                                                    {joinDate.display}
                                                </Typography>
                                            </ButtonBase>
                                            <Calendar
                                                open={showDatePicker}
                                                onChange={selectJoinDate}
                                                anchorElement={joinDateAnchor}
                                                focusDate={periodoActual.display}
                                                color='primary'
                                            />
                                        </div>
                                    </div>
                                </Grid>
                                <Grid
                                    container
                                    direction='row'
                                    justify='flex-end'
                                    className={classes.buttonsContainer}
                                >
                                    <Button
                                        className={classes.cancelButton}
                                        onClick={cancelCreateNewCotizante}
                                        disabled={props.disableStepOneButton}
                                    >
                                        <Typography
                                            variant='subtitle2'
                                            className={classes.cancelButtonText}
                                        >
                                            {CANCEL_BUTTON}
                                        </Typography>
                                    </Button>
                                    <Button
                                        variant='contained'
                                        type='submit'
                                        className={classes.saveButton}
                                        onClick={nextStepCreateNewCotizante}
                                        endIcon={
                                            props.disableStepOneButton ? null : <ArrowForwardRoundedIcon className={classes.continueButtonIcon} />
                                        }
                                    >
                                        {
                                            props.disableStepOneButton ?
                                                <CircularProgress className={classes.spinner} size={20} />
                                                :
                                                <Typography
                                                    variant='subtitle2'
                                                    className={classes.saveButtonText}
                                                >
                                                    {CONTINUE_BUTTON}
                                                </Typography>
                                        }
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </form>
                    :
                    <form>
                        <Grid
                            className={classes2.container}
                        >
                            <div
                                className={classes2.leftContainer}
                            >
                                <div className={classes2.avatarContainer}>
                                    <img src={logo} alt='Avatar' className={classes2.avatar} />
                                </div>
                            </div>
                            <Grid
                                className={classes2.rightContainer}
                            >
                                <div
                                    className={classes.cardSubtitle}
                                >
                                    <Typography
                                        variant='subtitle2'
                                        className={classes2.secondCardSubtitleText}
                                    >
                                        {COTIZANTE_DATA_TITLE}
                                    </Typography>
                                </div>
                                <Grid
                                    container
                                    direction='row'
                                    justify='space-between'
                                    alignItems='center'
                                    className={classes2.fieldsGrid}
                                >
                                    <TextField
                                        placeholder={FIELDS_HELPER_TEXT.COTIZANTE_NAME}
                                        helperText={FIELDS_HELPER_TEXT.COTIZANTE_NAME}
                                        className={classes2.nameInput}
                                        id={FIELDS_ID.COTIZANTE_NAME}
                                        onChange={saveCotizanteFieldValue}
                                        value={cotizanteName}
                                        inputProps={{
                                            style: {
                                                backgroundColor: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_NAME) ? '#FF495A26' : 'transparent'
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: initialFields ?
                                                    requiredFieldClasses.underlineInitial : cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_NAME) ? requiredFieldClasses.underlineRequired : null
                                            }
                                        }}
                                    />
                                    <TextField
                                        select={true}
                                        helperText={FIELDS_HELPER_TEXT.COTIZANTE_TYPE}
                                        value={tiposCotizantesFull.find(type => type.code === cotizanteType.code)}
                                        onChange={saveCotizanteFieldValue}
                                        className={classes.field}
                                        name={FIELDS_ID.COTIZANTE_TYPE}
                                        SelectProps={{
                                            classes: {
                                                selectMenu: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_TYPE) ? requiredFieldClasses.selectMenu : null
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_TYPE) ? requiredFieldClasses.underlineRequired : null
                                            }
                                        }}
                                    >
                                        {
                                            tiposCotizantesFull.map((item, index) => (
                                                <MenuItem key={index} value={item} disabled={index === 0}>
                                                    {item.display}
                                                </MenuItem>
                                            ))
                                        }
                                    </TextField>
                                    <TextField
                                        select={true}
                                        helperText={FIELDS_HELPER_TEXT.COTIZANTE_SUBTYPE}
                                        value={subtiposCotizantesFull.find(subtype => subtype.code === cotizanteSubtype.code)}
                                        onChange={saveCotizanteFieldValue}
                                        className={classes.field}
                                        name={FIELDS_ID.COTIZANTE_SUBTYPE}
                                        SelectProps={{
                                            classes: {
                                                selectMenu: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SUBTYPE) ? requiredFieldClasses.selectMenu : null
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SUBTYPE) ? requiredFieldClasses.underlineRequired : null
                                            }
                                        }}
                                    >
                                        {
                                            subtiposCotizantesFull.map((item, index) => (
                                                <MenuItem key={index} value={item} disabled={index === 0}>
                                                    {item.display}
                                                </MenuItem>
                                            ))
                                        }
                                    </TextField>
                                </Grid>
                                <Grid
                                    container
                                    direction='row'
                                    justify='space-between'
                                    alignItems='center'
                                    className={classes2.fieldsGrid}
                                >
                                    <TextField
                                        className={classes2.idField}
                                        placeholder={FIELDS_HELPER_TEXT.IDENTIFICATION_NUMBER}
                                        helperText={FIELDS_HELPER_TEXT.IDENTIFICATION_NUMBER}
                                        value={identificationNumber}
                                        onChange={saveCotizanteFieldValue}
                                        id={FIELDS_ID.IDENTIFICATION_NUMBER}
                                        inputProps={{
                                            style: {
                                                backgroundColor: cotizantesRequiredFields.includes(FIELDS_ID.IDENTIFICATION_NUMBER) ? '#FF495A26' : 'transparent'
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: cotizantesRequiredFields.includes(FIELDS_ID.IDENTIFICATION_NUMBER) ? requiredFieldClasses.underlineRequired : null
                                            },
                                            startAdornment:
                                                <InputAdornment
                                                    position='start'
                                                >
                                                    <TextField
                                                        select={true}
                                                        value={identificationType}
                                                        name={FIELDS_ID.IDENTIFICATION_TYPE}
                                                        onChange={saveCotizanteFieldValue}
                                                        InputProps={{
                                                            disableUnderline: true
                                                        }}
                                                        SelectProps={{
                                                            classes: {
                                                                selectMenu: cotizantesRequiredFields.includes(FIELDS_ID.IDENTIFICATION_TYPE) ? requiredFieldClasses.selectMenu : null
                                                            }
                                                        }}
                                                    >
                                                        {
                                                            ID_VALUES.map((item, index) => (
                                                                <MenuItem key={index} value={item}>
                                                                    {item}
                                                                </MenuItem>
                                                            ))
                                                        }
                                                    </TextField>
                                                </InputAdornment>
                                        }}
                                    />
                                    <TextField
                                        select={true}
                                        placeholder={FIELDS_HELPER_TEXT.COTIZANTE_GENRE}
                                        helperText={FIELDS_HELPER_TEXT.COTIZANTE_GENRE}
                                        className={classes2.genreField}
                                        name={FIELDS_ID.COTIZANTE_GENRE}
                                        onChange={saveCotizanteFieldValue}
                                        value={GENRES.find(genre => genre.value === cotizanteGenre.value)}
                                        SelectProps={{
                                            classes: {
                                                selectMenu: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_GENRE) ? requiredFieldClasses.selectMenu : null
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: initialFields ?
                                                    requiredFieldClasses.underlineInitial : cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_GENRE) ? requiredFieldClasses.underlineRequired : null
                                            }
                                        }}
                                    >
                                        {
                                            GENRES.map((item, index) => (
                                                <MenuItem key={index} value={item} disabled={index === 0}>
                                                    {item.display}
                                                </MenuItem>
                                            ))
                                        }
                                    </TextField>
                                    <Autocomplete
                                        id={FIELDS_ID.COTIZANTE_SALUD}
                                        onChange={getListValue}
                                        value={cotizanteSalud}
                                        className={classes2.saludField}
                                        style={{
                                            marginTop: -8
                                        }}
                                        disableClearable={true}
                                        options={epsFull}
                                        getOptionLabel={option => option.display}
                                        classes={{
                                            inputRoot: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SALUD) ? requiredFieldClasses.inputRoot : null,
                                            option: classes2.option,
                                            input: classes2.input
                                        }}
                                        renderInput={params =>
                                            <TextField
                                                {...params}
                                                margin='normal'
                                                helperText={FIELDS_HELPER_TEXT.COTIZANTE_SALUD}
                                                InputProps={{
                                                    ...params.InputProps,
                                                    classes: {
                                                        underline: requestBDUA ?
                                                        requiredFieldClasses.underlineInitial : cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SALUD) ? requiredFieldClasses.underlineRequired : null
                                                    }
                                                }}
                                            />
                                        }
                                    />
                                    <Autocomplete
                                        id={FIELDS_ID.COTIZANTE_PENSION}
                                        onChange={getListValue}
                                        value={cotizantePension}
                                        className={classes2.pensionField}
                                        style={{
                                            marginTop: -8
                                        }}
                                        disableClearable={true}
                                        options={afpFull}
                                        getOptionLabel={option => option.display}
                                        classes={{
                                            inputRoot: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_PENSION) ? requiredFieldClasses.inputRoot : null,
                                            option: classes2.option,
                                            input: classes2.input
                                        }}
                                        renderInput={params =>
                                            <TextField
                                                {...params}
                                                margin='normal'
                                                helperText={FIELDS_HELPER_TEXT.COTIZANTE_PENSION}
                                                InputProps={{
                                                    ...params.InputProps,
                                                    classes: {
                                                        underline: requestBDUA ?
                                                        requiredFieldClasses.underlineInitial : cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_PENSION) ? requiredFieldClasses.underlineRequired : null
                                                    }
                                                }}
                                            />
                                        }
                                    />
                                </Grid>
                                <Grid
                                    container
                                    direction='row'
                                    justify='space-between'
                                    alignItems='flex-end'
                                    className={classes2.fieldsGrid}
                                >
                                    <TextField
                                        select={true}
                                        helperText={FIELDS_HELPER_TEXT.ARL_RISK}
                                        value={arlRiskFull.find(risk => risk.code === arlRisk.code)}
                                        onChange={saveCotizanteFieldValue}
                                        className={classes2.cotizanteTypeField}
                                        name={FIELDS_ID.ARL_RISK}
                                        SelectProps={{
                                            classes: {
                                                selectMenu: cotizantesRequiredFields.includes(FIELDS_ID.ARL_RISK) ? requiredFieldClasses.selectMenu : null
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                underline: initialFields ?
                                                    requiredFieldClasses.underlineInitial : cotizantesRequiredFields.includes(FIELDS_ID.ARL_RISK) ? requiredFieldClasses.underlineRequired : null
                                            }
                                        }}
                                    >
                                        {
                                            arlRiskFull.map((item, index) => (
                                                <MenuItem key={index} value={item} disabled={index === 0}>
                                                    {item.display}
                                                </MenuItem>
                                            ))
                                        }
                                    </TextField>
                                    <TextField
                                        placeholder={SALARY}
                                        value={salary.value}
                                        helperText={SALARY}
                                        onChange={saveCotizanteFieldValue}
                                        className={classes2.salaryField}
                                        id={FIELDS_ID.COTIZANTE_SALARY}
                                        InputProps={{
                                            startAdornment: <InputAdornment position='start'>$</InputAdornment>,
                                            classes: {
                                                underline: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SALARY) ? requiredFieldClasses.underlineRequired : null
                                            },
                                            inputComponent: MaskedSalaryInput
                                        }}
                                        inputProps={{
                                            style: {
                                                backgroundColor: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_SALARY) ? '#FF495A26' : 'transparent'
                                            }
                                        }}
                                    />
                                    <Autocomplete
                                        id={FIELDS_ID.PROVINCE}
                                        options={provinces}
                                        onChange={getListValue}
                                        disableClearable={true}
                                        value={cotizanteProvince}
                                        getOptionLabel={option => option.display}
                                        className={classes2.province}
                                        renderInput={params =>
                                            <TextField
                                                {...params}
                                                margin='normal'
                                                helperText='Departamento'
                                                style={{
                                                    marginBottom: 0
                                                }}
                                                InputProps={{
                                                    ...params.InputProps
                                                }}
                                            />
                                        }
                                    />
                                    <Autocomplete
                                        id={FIELDS_ID.CITY}
                                        options={cities}
                                        onChange={getListValue}
                                        disableClearable={true}
                                        value={cotizanteCity}
                                        getOptionLabel={option => option.display}
                                        className={classes2.province}
                                        filterOptions={filterCitiesList}
                                        renderInput={params =>
                                            <TextField
                                                {...params}
                                                margin='normal'
                                                helperText='Municipio'
                                                style={{
                                                    marginBottom: 0
                                                }}
                                                InputProps={{
                                                    ...params.InputProps
                                                }}
                                            />
                                        }
                                    />
                                </Grid>
                                <Grid
                                    container
                                    direction='row'
                                    alignItems='flex-end'
                                    className={classes2.fieldsGrid}
                                >
                                    <TextField
                                        placeholder={FIELDS_HELPER_TEXT.COTIZANTE_EMAIL}
                                        helperText={FIELDS_HELPER_TEXT.COTIZANTE_EMAIL}
                                        className={classes2.emailField}
                                        id={FIELDS_ID.COTIZANTE_EMAIL}
                                        onChange={saveCotizanteFieldValue}
                                        value={cotizanteEmail}
                                        InputProps={{
                                            classes: {
                                                underline: initialFields ?
                                                requiredFieldClasses.underlineInitial : cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_EMAIL) ? requiredFieldClasses.underlineRequired : null
                                            }
                                        }}
                                        inputProps={{
                                            style: {
                                                backgroundColor: cotizantesRequiredFields.includes(FIELDS_ID.COTIZANTE_EMAIL) ? '#FF495A26' : 'transparent'
                                            }
                                        }}
                                    />
                                    {/* <TextField
                                        placeholder={FIELDS_HELPER_TEXT.COTIZANTE_SUCURSAL}
                                        helperText={FIELDS_HELPER_TEXT.COTIZANTE_SUCURSAL}
                                        className={classes2.sucursalField}
                                        id={FIELDS_ID.COTIZANTE_SUCURSAL}
                                        onChange={saveCotizanteFieldValue}
                                        value={cotizanteSucursal}
                                        disabled={true}
                                        InputProps={{
                                            classes: {
                                                disabled: classes2.disabled
                                            }
                                        }}
                                    /> */}
                                    <div className={classes2.ccfContainer}>
                                        <Button
                                            variant='outlined'
                                            className={classes2.ccfButton}
                                            onClick={getCotizanteCCF}
                                        >
                                            {
                                                cajaCompensacion.value ?
                                                    <img
                                                        src={cajaCompensacion.value.logo}
                                                        alt={cajaCompensacion.display}
                                                        style={{
                                                            height: 'auto',
                                                            width: CUSTOM_COMPANY_CCF[cajaCompensacion.display]
                                                        }}
                                                    />
                                                    :
                                                    <Typography
                                                        variant='subtitle2'
                                                        className={classes2.ccfText}
                                                    >
                                                        {CAJA_COMPENSACION}
                                                    </Typography>
                                            }
                                        </Button>
                                        <Typography className={classes2.ccfTitle}>
                                            Caja de compensación
                                        </Typography>
                                    </div>
                                </Grid>
                                <Grid
                                    container
                                    direction='row'
                                    justify='flex-end'
                                    alignItems='center'
                                    className={classes2.buttonsGrid}
                                >
                                    <Button
                                        className={classes.cancelButton}
                                        onClick={cancelCreateNewCotizante}
                                        disabled={props.disableStepTwoButton}
                                    >
                                        <Typography
                                            variant='subtitle2'
                                            className={classes.cancelButtonText}
                                        >
                                            {CANCEL_BUTTON}
                                        </Typography>
                                    </Button>
                                    <Button
                                        variant='contained'
                                        className={classes.createCotizanteButton}
                                        onClick={createNewCotizante}
                                        type='submit'
                                    >
                                        {
                                            props.disableStepTwoButton ?
                                                <CircularProgress className={classes.spinner} size={20} />
                                                :
                                                <Typography
                                                    variant='subtitle2'
                                                    className={classes.saveButtonText}
                                                >
                                                    {CREATE_COTIZANTE}
                                                </Typography>
                                        }
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <ModalCCF
                            open={openCCFModal}
                            onClose={closeCCFModal}
                            onSelectCCF={selectCCF}
                            cajaCompensacion={cajaCompensacion}
                            filterValue={cotizanteProvince}
                        />
                    </form>
            }
        </Dialog>
    );
}

NewCotizanteLayout.propTypes = {
    setShowNewCotizante: PropTypes.func.isRequired,
    showNewCotizante: PropTypes.bool.isRequired
}

const requiredFieldStyles = makeStyles(theme => ({
    underlineInitial: {
        '&.MuiInput-underline:after': {
            borderBottom: '2px solid #6A32B5'
        },
        '&.MuiInput-underline:before': {
            borderBottom: '2px solid #6A32B5'
        },
        '&.MuiInput-underline:hover:before': {
            borderBottom: '2px solid #6A32B5'
        }
    },
    underlineRequired: {
        '&.MuiInput-underline:after': {
            borderBottom: '2px solid #FF495A'
        },
        '&.MuiInput-underline:before': {
            borderBottom: '1px solid #FF495A'
        },
        '&.MuiInput-underline:hover:before': {
            borderBottom: '2px solid #FF495A'
        }
    },
    inputRoot: {
        '&.MuiAutocomplete-inputRoot': {
            backgroundColor: '#FF495A26'
        }
    },
    selectMenu: {
        '&.MuiSelect-selectMenu': {
            backgroundColor: '#FF495A26'
        }
    }
}));

const FirstCardStyles = makeStyles(theme => ({
    paperContainer: {
        backgroundColor: theme.palette.common.white
    },
    backdrop: {
        backgroundColor: 'transparent'
    },
    dialog: {
        backgroundColor: '#000000CC'
    },
    dialogTitleContainer: {
        borderBottom: '1px solid black'
    },
    cardTitle: {
        fontSize: '2em',
        fontWeight: 'bold',
        paddingLeft: '1.5%',
        color: '#7B1FA2'
    },
    cardSubtitle: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        paddingTop: 10
    },
    cardSubtitleText: {
        paddingLeft: '4%',
        fontSize: '1.5em'
    },
    firstFieldsContainer: {
        paddingTop: 10,
        paddingBottom: 10
    },
    field: {
        width: '20%'
    },
    salaryContainer: {
        display: 'flex',
        flexDirection: 'column',
        width: '20%'
    },
    salaryTextfield: {
        width: '100%'
    },
    salaryHelperTextContainer: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        paddingTop: 3,
        justifyContent: 'space-between'
    },
    salaryText: {
        fontSize: '0.75rem',
        color: 'rgba(0,0,0,0.54)'
    },
    salaryCheckboxContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    salaryCheckbox: {
        width: '10px',
        height: '10px',
        fontSize: 10
    },
    checkedIcon: {
        fontSize: 15,
        color: '#4CAF50'
    },
    uncheckedIcon: {
        fontSize: 15
    },
    smlmvText: {
        fontSize: '0.75rem',
        color: 'rgba(0,0,0,0.54)'
    },
    uploadFileContainer: {
        justifyContent: 'space-evenly',
        paddingTop: 10,
        width: '75%'
    },
    uploadFileTextContainer: {
        width: '68%',
        paddingTop: 10
    },
    uploadFileTitleContainer: {
        border: '1px solid black',
        borderRadius: 7,
        padding: '2.5px 0px 2.5px 10px'
    },
    downloadFileButton: {
        borderRadius: 8,
        paddingLeft: '1.5%',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        color: theme.palette.primary.main,
        fontWeight: 'bold',
        '&:hover': {
            backgroundColor: theme.palette.primary[200],
        },
        '&:focus': {
            backgroundColor: theme.palette.primary[200]
        }
    },
    uploadFileButtonContainer: {
        width: '20%',
        height: 50,
        paddingTop: 10
    },
    buttonsContainer: {
        padding: '20px 4% 20px 0px'
    },
    continueButtonIcon: {
        color: 'white'
    },
    saveButton: {
        background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
        borderRadius: 100,
        marginLeft: 10,
        width: '20%',
        textTransform: 'none'
    },
    createCotizanteButton: {
        background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
        borderRadius: 100,
        marginLeft: 10,
        width: '25%',
        textTransform: 'none'
    },
    saveButtonText: {
        color: 'white',
        fontWeight: 'bold'
    },
    cancelButtonText: {
        color: '#5E35B1'
    },
    cancelButton: {
        marginRight: 10,
        borderRadius: 100,
        textTransform: 'none'
    },
    joinDateContainer: {
        paddingLeft: '3%'
    },
    joinDateSecondContainer: {
        backgroundColor: '#ECEFF1',
        padding: '10px 0px 10px 0px',
        justifyContent: 'space-between',
        borderRadius: 10,
        width: '71%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    joinDateCheckContainer: {
        display: 'flex',
        flexDirection: 'row',
        padding: '0px 0% 0px 2%',
        alignItems: 'center',
        width: '65%'
    },
    joinDateCheckBoxChecked: {
        color: '#4CAF50'
    },
    joinDateArrowIcon: {
        fontSize: '2em',
        marginLeft: '15%'
    },
    joinDateCalendarContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'left',
        alignItems: 'center',
        width: '35%'
    },
    joinDateCalendarIcon: {
        color: '#95989A'
    },
    joinDateCalendarButton: {
        border: '2px solid #95989A',
        width: '66%',
        borderRadius: 5,
        padding: '3px 10% 3px 10%',
        marginLeft: '2%',
        backgroundColor: 'rgba(149, 152, 154, 0.2)'
    },
    joinDateText: {
        color: '#95989A',
        fontWeight: 'bold'
    },
    joinDateActiveColor: {
        color: '#7E57C2'
    },
    joinDateActiveButton: {
        borderColor: '#7E57C2',
        backgroundColor: 'rgba(126, 87, 194, 0.2)'
    },
    joinDateRequiredButton: {
        border: '2px solid #FF495A',
        backgroundColor: '#FF495A26'
    },
    joinDateRequiredColor: {
        color: '#FF495A'
    },
    spinner: {
        color: '#FFFFFF'
    },
    underline: {
        '&.MuiInput-underline:after': {
            borderBottom: '2px solid #6A32B5'
        }
    },
    uploadButton: {
        borderRadius: 8,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        textTransform: 'none',
        '&:hover': {
            backgroundColor: theme.palette.primary[700],
        },
        '&:focus': {
            backgroundColor: theme.palette.primary[600]
        }
    }
}));

const SecondCardStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexDirection: 'row'
    },
    leftContainer: {
        display: 'flex',
        width: '20%',
        justifyContent: 'flex-start',
        paddingTop: 30
    },
    avatarContainer: {
        width: '100%',
        height: '33.33%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        width: '45%'
    },
    nameInput: {
        width: '50%'
    },
    idField: {
        width: '30%'
    },
    rightContainer: {
        width: '80%',
        paddingRight: '5%',
        display: 'flex',
        flexDirection: 'column'
    },
    fieldsGrid: {
        padding: '10px 0px 10px 0px'
    },
    genreField: {
        width: '15%'
    },
    saludField: {
        width: '20%'
    },
    pensionField: {
        width: '20%'
    },
    cotizanteTypeField: {
        width: '30%'
    },
    salaryField: {
        width: '15%'
    },
    sucursalField: {
        width: '45%'
    },
    emailField: {
        width: '50%'
    },
    buttonsGrid: {
        paddingTop: 20,
        paddingBottom: 20
    },
    secondCardSubtitleText: {
        fontSize: '1.5em'
    },
    disabled: {
        '&.Mui-disabled:before': {
            borderBottomStyle: 'solid'
        },
        '&.MuiInputBase-input': {
            color: '#545454'
        }
    },
    option: {
        '&.MuiAutocomplete-option': {
            fontSize: '0.85em'
        }
    },
    input: {
        '&.MuiAutocomplete-input': {
            fontSize: '0.85em'
        }
    },
    ccfContainer: {
        width: '20%',
        display: 'flex',
        flexDirection: 'column',
        marginLeft: '2.3rem'
    },
    ccfButton: {
        width: '100%',
        border: '2px solid #6A32B5',
        height: 51,
        borderRadius: 7,
        '&:hover': {
            backgroundColor: theme.palette.common.white,
        },
    },
    ccfText: {
        color: '#6A32B5',
        fontSize: '0.8rem',
        fontWeight: 'bold'
    },
    ccfTitle: {
        fontSize: '0.75rem',
        marginTop: 3,
        color: 'rgba(0,0,0,0.54)'
    },
    province: {
        width: '20%'
    }
}));

export default NewCotizanteLayout;

