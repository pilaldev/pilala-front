import React from 'react';
import { FlatfileButton } from '@flatfile/react';
import { useDispatch } from 'react-redux';
import { cotizantesAction } from '../redux/Actions';
import { COTIZANTES } from '../redux/ActionTypes';

const LICENSE_KEY = 'd55b2a8a-8e65-458d-b6f7-2f9e44be1397';
const importerOptions = {
    type: 'cotizantes',
    fields: [
        { label: 'Tipo ID', key: 'tipoId' },
        { label: 'Identificación', key: 'identificacion' },
        { label: 'Primer nombre', key: 'primerNombre' },
        { label: 'Segundo nombre', key: 'segundoNombre' },
        { label: 'Primer apellido', key: 'primerApellido' },
        { label: 'Segundo apellido', key: 'segundoApellido' },
        { label: 'Tipo cotizante', key: 'tipoCotizante' },
        { label: 'Subtipo cotizante', key: 'subtipoCotizante' },
        { label: 'Riesgo ARL', key: 'riesgoArl' },
        { label: 'CCF', key: 'ccf' },
        { label: 'Teléfono', key: 'telefono' }
    ],
    title: 'Carga tu lista de cotizantes',
    managed: true
}
const customerData = {
    companyId: 'pilala',
    companyName: 'Pilalá SAS',
    userId: '2020'
}

const FlatfileWrapper = ({element: RenderElement, onUploadData}) => {
    const dispatch = useDispatch();
    
    const onUpload = async ({$data: data}) => {
        const tempData = data.map((row) => row.data);
        onUploadData({
            successful: true,
            data: tempData
        });
    }

    const onCancel = () => {
        dispatch(cotizantesAction(COTIZANTES.COTIZANTES_SHOW_OPTIONS_MODAL, false));
    }

    if (!RenderElement) return null;
    return (
        <FlatfileButton
            settings={importerOptions}
            licenseKey={LICENSE_KEY}
            customer={customerData}
            onData={onUpload}
            onCancel={onCancel}
            render={(importer, launch) => {
                return (
                    <RenderElement
                        onClick={launch}
                    />
                )
            }}
        />
    );
}

export default FlatfileWrapper;
