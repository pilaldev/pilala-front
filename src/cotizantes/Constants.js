/* Cotizantes list */
export const COTIZANTES_LIST = 'Listado de cotizantes';
export const ADD_NEW_COTIZANTE = 'Agregar nuevo cotizante';
export const DOWNLOAD_INFO = 'Descargar información';
export const FILTER_BY = 'Filtrar por';
export const SEARCHBAR_PLACEHOLDER = 'Buscar por nombre o documento';
export const PAY_PILA_BUTTON = 'Ir a liquidar PILA';

/* New cotizante card */
export const CARD_TITLE = 'Crea un cotizante';
export const COTIZANTE_DATA_TITLE = 'Datos del cotizante';
export const ID_VALUES = [ 'CC', 'CE', 'PE'];
export const COTIZANTES_TYPE = [ 'Tipo empleado', 'Empleado', 'Pensionado', 'Estudiante' ];
export const COTIZANTES_SUBTYPE = [ 'Subtipo empleado', 'Empleado', 'Pensionado', 'Estudiante' ];
export const SALARY = 'Salario';
export const SMLMV = 'SMLMV';
export const INTEGRAL = 'Integral';
export const UPLOAD_FILE_TITLE = '¿Tienes varios cotizantes?, prueba cargando un archivo EXCEL';
export const DOWNLOAD_FILE_BUTTON = 'Descargar formato';
export const UPLOAD_FILE_BUTTON = 'Subir archivo';
export const CANCEL_BUTTON = 'Cancelar';
export const CONTINUE_BUTTON = 'Continuar';
export const CREATE_COTIZANTE = 'Crear cotizante';
export const ARL_TYPES = [
	'Nivel de riesgo',
	'Clase 1 - Riesgo mínimo',
	'Clase 2 - Riesgo bajo',
	'Clase 3 - Riesgo medio',
	'Clase 4 - Riesgo alto',
	'Clase 5 - Riesgo máximo'
];
export const FIELDS_ID = {
	IDENTIFICATION_NUMBER: 'identificationNumber',
	IDENTIFICATION_TYPE: 'identificationType',
	COTIZANTE_TYPE: 'cotizanteType',
	COTIZANTE_SUBTYPE: 'cotizanteSubtype',
	COTIZANTE_SALARY: 'cotizanteSalary',
	SMLMV_CHECK: 'salaryCheckbox',
	COTIZANTE_NAME: 'cotizanteName',
	COTIZANTE_GENRE: 'cotizanteGenre',
	COTIZANTE_SALUD: 'cotizanteSalud',
	COTIZANTE_PENSION: 'cotizantePension',
	ARL_RISK: 'arlRisk',
	COTIZANTE_SUCURSAL: 'cotizanteSucursal',
	COTIZANTE_EMAIL: 'cotizanteEmail',
	JOIN_DATE_CHECK: 'joinDateCheck',
	INTEGRAL_CHECK: 'integralSalaryCheck',
	JOIN_DATE: 'joinDate',
	PROVINCE: 'province',
	CITY: 'city'
};
export const FIELDS_HELPER_TEXT = {
	IDENTIFICATION_NUMBER: 'Número identificación',
	COTIZANTE_TYPE: 'Tipo',
	COTIZANTE_SUBTYPE: 'Subtipo',
	COTIZANTE_NAME: 'Nombre cotizante',
	COTIZANTE_GENRE: 'Sexo',
	COTIZANTE_SALUD: 'Salud',
	COTIZANTE_PENSION: 'Pensión',
	ARL_RISK: 'Nivel de riesgo',
	COTIZANTE_SUCURSAL: 'Asignar sucursal',
	COTIZANTE_EMAIL: 'Correo electrónico'
};
export const JOIN_DATE_TITLE = 'Crear novedad de ingreso (ING)';
export const GENRES = [
	{
		display: 'Sexo',
		value: -1
	},
	{
		display: 'Masculino',
		value: 0
	},
	{
		display: 'Femenino',
		value: 1
	}
];

/** COTIZANTE CARD **/

export const COTIZANTE_STATUS = [ { value: false, activeText: 'Inactivo' }, { value: true, activeText: 'Activo' } ];
