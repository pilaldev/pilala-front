import VISA from './../assets/cards/visa.svg';
import MASTERCARD from './../assets/cards/mastercard.svg';
import AMERICAN_EXPRESS from './../assets/cards/american_express.svg';
import UNKNOWN from './../assets/cards/unknown.svg';

const cardsAssets = {
    visa: VISA,
    mastercard: MASTERCARD,
    'american-express': AMERICAN_EXPRESS,
    'default': UNKNOWN
}

const availableCards = [
    'visa',
    'mastercard',
    'american-express'
];

export { cardsAssets as CARD_LOGO };
export { availableCards as AVAILABLE_CARDS };