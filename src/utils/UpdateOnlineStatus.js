import { FIRESTORE } from './../api/Firebase';

const updateOnlineStatus = async (userEmail, status) => {
    await FIRESTORE.collection('users').doc(userEmail).set({
        online: status
    }, {merge: true});
}

export default updateOnlineStatus;