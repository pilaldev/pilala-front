const calcularDigitoVerificacion = (nit) => {
    let primeNumbers = [];
    let auxA, auxB, auxC;
    const temp_nit = nit;
    if (Number.isNaN(temp_nit)) {
        throw new Error('El nit ingresado no es válido');
    }
    else {
        primeNumbers = new Array(16);
        auxA = 0;
        auxB = 0;
        auxC = temp_nit.length;

        primeNumbers[1] = 3;
        primeNumbers[2] = 7;
        primeNumbers[3] = 13;
        primeNumbers[4] = 17;
        primeNumbers[5] = 19;
        primeNumbers[6] = 23;
        primeNumbers[7] = 29;
        primeNumbers[8] = 37;
        primeNumbers[9] = 41;
        primeNumbers[10] = 43;
        primeNumbers[11] = 47;
        primeNumbers[12] = 53;
        primeNumbers[13] = 59;
        primeNumbers[14] = 67;
        primeNumbers[15] = 71;

        for (let i = 0; i < auxC; i++) {
            auxB = temp_nit.substr(i, 1);
            auxA += (auxB * primeNumbers[auxC - i]);
        }
        auxB = auxA % 11;

        return (auxB > 1) ? 11 - auxB : auxB;
    }
}

export default calcularDigitoVerificacion;