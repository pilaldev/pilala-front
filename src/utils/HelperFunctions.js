import StringSimilarity from "string-similarity";
import moment from 'moment'

const matchInStringArray = (targetString, mainStrings) => {
    const temp_targetString = targetString.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
    const temp_mainStrings = Array.from(mainStrings, item => item.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase());
    const result = StringSimilarity.findBestMatch(temp_targetString, temp_mainStrings);
    let matchedString = '';
    mainStrings.forEach((item) => {
        const tempItem = item.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
        const tempTarget = result.bestMatch.target.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
        if (tempItem === tempTarget) {
            matchedString = item;
        }
    });

    return matchedString;
}

const matchInObjectArray = (targetString, matchKey, mainStrings) => {
    const temp_targetString = targetString.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
    const temp_mainStrings = Array.from(mainStrings, item => item[matchKey].normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase());
    const result = StringSimilarity.findBestMatch(temp_targetString, temp_mainStrings);
    let matchedString = '';
    mainStrings.forEach((item) => {
        const tempItem = item[matchKey].normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
        const tempTarget = result.bestMatch.target.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
        if (tempItem === tempTarget) {
            matchedString = item;
        }
    });

    return matchedString;
}

const formatProvincesArray = (provinces) => {
    return provinces.map((item) => ({
        code: item.code,
        display: item.province,
        value: item.province.toLowerCase()
    }));
}

const formatCitiesArray = (cities) => {
    return cities.map((item) => ({
        code: item.cityCode,
        display: item.city,
        provinceCode: item.provinceCode,
        value: item.city.toLowerCase()
    }));
}

const formatCIIUArray = (ciiu) => {
    return ciiu.map((item) => ({
        code: item.code,
        display: item.description,
        value: item.description.toLowerCase()
    }));
}

const formatARLArray = (arls) => {
    return arls.map((item) => ({
        code: item.codigo,
        display: item.razonSocial,
        value: {
            ...item
        }
    }));
}

const formatCCFArray = (ccfs) => {
    return ccfs.map((item) => ({
        code: item.codigo,
        display: item.razonSocial,
        province: item.departamento,
        value: {
            ...item
        }
    }));
}

const formatAFPArray = (afps) => {
    return afps.map((item) => ({
        code: item.codigo,
        display: item.razonSocial,
        value: {
            ...item
        }
    }));
}

const formatEPSArray = (epss) => {
    return epss.map((item) => ({
        code: item.codigo,
        display: item.razonSocial,
        value: {
            ...item
        }
    }));
}

const formatClaseARLArray = (clasesARL) => {
    return clasesARL.map((item) => ({
        code: item.value,
        display: item.label,
        value: item.label.toLowerCase()
    }));
}

const paddingField = (currentValue, fieldType, fieldLength) => {
    if (currentValue === undefined || fieldType === undefined || fieldLength === undefined) throw new Error('Campos de función requeridos');

    if (currentValue.toString().length > fieldLength) throw new Error('La longitud del valor actual es mayor a la permitida');

    if (fieldType === 'A') {
        let temp_value = currentValue;
        while (temp_value.length < fieldLength) {
            temp_value = temp_value + ' ';
        }
        return temp_value;
    }
    else if (fieldType === 'N') {
        let temp_value = String(currentValue);
        while (temp_value.length < fieldLength) {
            temp_value = '0' + temp_value;
        }
        return temp_value;
    }
}

const createPaddingField = (fieldType, fieldLength) => {
    if (fieldType === undefined || fieldLength === undefined) throw new Error('Campos de función requeridos');

    if (fieldType === 'A') {
        let temp_value = '';
        while (temp_value.length < fieldLength) {
            temp_value += ' ';
        }
        return temp_value;
    }
    else if (fieldType === 'N') {
        let temp_value = '';
        while (temp_value.length < fieldLength) {
            temp_value += '0';
        }
        return temp_value;
    }
}

const getIngresoDays = (date) => {
    const monthDay = moment(date, 'DD-MM-YYYY').date();
    if (monthDay > 30) {
        return '1';
    }
    else {
        let days = 0;
        const currentDays = moment(date, 'DD-MM-YYYY').date();
        for (let index = currentDays; index <= 30; index++) {
            days += 1;
        }
        return days.toString();
    }
}

const getRetiroDays = (date) => {
    const monthDay = moment(date, 'DD-MM-YYYY').date();
    if (monthDay > 30) {
        return '30';
    }
    else {
        let days = 0;
        const currentDays = moment(date, 'DD-MM-YYYY').date();
        for (let index = currentDays; index >= 1; index--) {
            days += 1;
        }
        return days.toString()
    }
}

const formatStringNumber = (value) => {
    const number = value === undefined || value === null ? '' : value;
    return number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
}

export default {
    matchInStringArray,
    matchInObjectArray,
    formatProvincesArray,
    formatCitiesArray,
    formatCIIUArray,
    formatARLArray,
    formatCCFArray,
    formatClaseARLArray,
    formatAFPArray,
    formatEPSArray,
    paddingField,
    createPaddingField,
    getIngresoDays,
    getRetiroDays,
    formatStringNumber
}