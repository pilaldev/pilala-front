export default {
    NAMES: /^[a-zA-Z0-9 ñÑáéíóú´]*$/,
    NUMBERS: /^[0-9]*$/,
    PHONENUMBER: /^[0-9() ]*$/,
    ADDRESS_NUMBER: /^[0-9]{1,4}$/,
    ADDRESS_LETTER: /^[a-zA-Z]{1,3}$/,
    PERSON_NAME: /^[a-zA-Z ñÑáéíóú´]*$/,
    NIT: /^[0-9]*$/,
    EMAIL_CHARACTERS: /^[a-zA-Z0-9._@-]*$/,
    VALID_NAME: /^[a-zA-ZñÑáéíóú´]{2,30}(?: [a-zA-ZñÑáéíóú´]{2,30})+$/
}