export const MatchNameFields = (name) => {
	let fullName = name || '';
	let result = {
		firstName: '',
		secondName: '',
		firstSurname: '',
		secondSurname: '',
		displayName: ''
	};

	fullName = fullName.split(' ').filter((el) => {
		if (el) {
			return el;
		} else {
			return null;
		}
	});

	if (fullName.length > 0) {
		// let nameTokens = fullName.match(/[A-ZÁ-ÚÑÜ][a-zá-úñü]+|([aeodlszy]+\s+)+[A-ZÁ-ÚÑÜ][a-zá-úñü]+/g) || [];

		// console.log('NAMETOKENS', nameTokens);
		// console.log(nameTokens)

		result.firstName = fullName[0];
		result.secondName = fullName.length > 1 ? fullName.slice(1, fullName.length - 2).join(' ') : '';
		result.firstSurname = fullName.length > 2 ? fullName.slice(-2, -1).join(' ') : fullName[1] ? fullName[1] : '';
		result.secondSurname = fullName.length > 2 ? fullName.slice(-1).join(' ') : '';
		result.displayName = name;

		// if (nameTokens.length > 3) {
		// 	result.firstName = nameTokens.slice(0, 2).join(' ');
		// } else {
		// 	result.firstName = nameTokens.slice(0, 1).join(' ');
		// }

		// if (nameTokens.length > 2) {
		// 	result.firstSurname = nameTokens.slice(-2, -1).join(' ');
		// 	result.secondSurname = nameTokens.slice(-1).join(' ');
		// } else {
		// 	result.firstSurname = nameTokens.slice(-1).join(' ');
		// 	result.secondSurname = '';
		// }
	}

	// console.log('RESULT', result);

	return result;

	// let nameArray = name.split(' ').filter((el) => {
	// 	if (el) {
	// 		return el;
	// 	} else {
	// 		return null;
	// 	}
	// });

	// let matchedData = {
	// 	firstName: nameArray.length ? nameArray[0] : '',
	// 	secondName: nameArray.length > 1 ? nameArray.slice(1, nameArray.length - 2).join(' ') : '',
	// 	firstSurname: nameArray.length > 2 ? nameArray[nameArray.length - 2] : '',
	// 	secondSurname: nameArray.length > 2 ? nameArray[nameArray.length - 1] : '',
	// 	displayName: name
	// };

	// return matchedData;
};
