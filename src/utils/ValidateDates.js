import moment from 'moment';
moment.locale('es');

const validateDates = (
	cotizanteData,
	dateNovelty,
	isIngreso,
	isRetiro,
	noveltyIndex,
	noveltyId,
	isStart,
	isEnd,
	currentNovelties
) => {
	let ingreso = cotizanteData.newEntry;
	let ingresoDate = null;
	let retiro = cotizanteData.retired;
	let retiroDate = null;

	let fechaNovedad = moment.isMoment(dateNovelty)
		? dateNovelty.startOf('day')
		: moment(dateNovelty, 'DD/MM/YYYY');
	let fechaInicioNovedad = null;
	let fechaFinNovedad = null;

	if (currentNovelties && currentNovelties[noveltyIndex].fechaInicio) {
		fechaInicioNovedad = moment.isMoment(currentNovelties[noveltyIndex].fechaInicio)
			? currentNovelties[noveltyIndex].fechaInicio.startOf('day')
			: moment(currentNovelties[noveltyIndex].fechaInicio, 'DD/MM/YYYY');
	}

	// console.log('hola', currentNovelties[noveltyIndex].nombreNovedad, noveltyType);

	if (currentNovelties && currentNovelties[noveltyIndex].fechaFin) {
		fechaFinNovedad = moment.isMoment(currentNovelties[noveltyIndex].fechaFin)
			? currentNovelties[noveltyIndex].fechaFin.startOf('day')
			: moment(currentNovelties[noveltyIndex].fechaFin, 'DD/MM/YYYY');
	}
	// console.log('fechanovedad', fechaNovedad);

	if (ingreso && !isIngreso) {
		ingresoDate = moment(cotizanteData.dateEntry, 'DD/MM/YYYY');

		if (!ingresoDate) throw new Error('La fecha de ingreso no se encuentra');
		// console.log('fechanovedad', fechaNovedad, ingresoDate);
		if (fechaNovedad.isBefore(ingresoDate)) {
			return {
				result: false,
				message: 'La fecha de novedad es antes del ingreso del cotizante',
			};
		}
	}

	if (retiro && !isRetiro) {
		retiroDate = moment(cotizanteData.dateRetired, 'DD/MM/YYYY');

		if (!retiroDate) throw new Error('La fecha de retiro no se encuentra');

		// console.log('fechanovedad', fechaNovedad, retiroDate);

		if (fechaNovedad.isAfter(retiroDate)) {
			return {
				result: false,
				message: 'La fecha de novedad está después del retiro del cotizante',
			};
		}
	}

	let licencias = cotizanteData.licencias;
	let vacaciones = cotizanteData.vacaciones;
	let suspensiones = cotizanteData.suspensiones;
	let incapacidades = cotizanteData.incapacidades;

	let noveltiesObject = {
		licencias: {
			noveltiesArray: licencias,
			arrayLength: licencias.length,
		},
		vacaciones: {
			noveltiesArray: vacaciones,
			arrayLength: vacaciones.length,
		},
		suspensiones: {
			noveltiesArray: suspensiones,
			arrayLength: suspensiones.length,
		},
		incapacidades: {
			noveltiesArray: incapacidades,
			arrayLength: incapacidades.length,
		},
	};

	let noveltiesKeys = Object.keys(noveltiesObject);
	let overlapResults = [];

	if (currentNovelties && currentNovelties.length >= 0) {
		let overlapCurrentNovelties = currentNovelties.some((novedad, index) => {
			if (index === noveltyIndex) {
				return false;
			}

			let fechaInicio = moment(novedad.fechaInicio, 'DD/MM/YYYY');
			let fechaFin = moment(novedad.fechaFin, 'DD/MM/YYYY');

			if (isIngreso) {
				// if (fechaNovedad.isBetween(fechaInicio, fechaFin, 'D', '(]') || fechaNovedad.isAfter(fechaFin)) {
				if (fechaNovedad.isAfter(fechaInicio)) {
					return true;
				} else {
					return false;
				}
			}

			if (isRetiro) {
				if (fechaNovedad.isBefore(fechaFin)) {
					return true;
				} else {
					return false;
				}
			}

			if (fechaInicioNovedad && !isStart) {
				if (
					fechaInicio.isBetween(fechaInicioNovedad, fechaNovedad, 'D', '()') ||
					fechaFin.isBetween(fechaInicioNovedad, fechaNovedad, 'D', '()') ||
					(fechaInicio.isSame(fechaInicio) && fechaFin.isSame(fechaNovedad))
				) {
					// console.log('entró acá a start');
					return true;
				}
			}

			if (fechaFinNovedad && !isEnd) {
				if (
					fechaInicio.isBetween(fechaNovedad, fechaFinNovedad, 'D', '()') ||
					fechaFin.isBetween(fechaNovedad, fechaFinNovedad, 'D', '()') ||
					(fechaInicio.isSame(fechaNovedad) && fechaFin.isSame(fechaFinNovedad))
				) {
					// console.log('Entró aacá al final');
					return true;
				}
			}

			if (fechaNovedad.isBetween(fechaInicio, fechaFin, 'D', '()')) {
				return true;
			}

			return false;
		});

		if (overlapCurrentNovelties) {
			return {
				result: false,
				message: `La fecha seleccionada se cruza con las novedades actuales`,
			};
		}
	}

	for (var i = 0; i < noveltiesKeys.length; i++) {
		if (noveltiesObject[noveltiesKeys[i]].arrayLength) {
			let overlapNovelty = noveltiesObject[noveltiesKeys[i]].noveltiesArray.some(
				(novedad, index) => {
					// let nombreNovedad = novedad.nombreNovedad;

					// console.log('adsa', noveltyType, noveltyIndex, nombreNovedad, noveltyType, index, noveltyIndex);
					// console.log('NOVID', novedad.id, noveltyType);
					if (novedad.id === noveltyId) {
						// console.log('ENTRÓ A LA VALIDACIÓN DESEADA');
						return false;
					}

					let fechaInicio = moment(novedad.fechaInicio, 'DD/MM/YYYY');
					let fechaFin = moment(novedad.fechaFin, 'DD/MM/YYYY');

					if (isIngreso) {
						// if (fechaNovedad.isBetween(fechaInicio, fechaFin, 'D', '(]') || fechaNovedad.isAfter(fechaFin)) {
						if (fechaNovedad.isAfter(fechaInicio)) {
							return true;
						} else {
							return false;
						}
					}

					if (isRetiro) {
						// if (
						// 	fechaNovedad.isBetween(fechaInicio, fechaFin, 'D', '[)') ||
						// 	fechaNovedad.isBefore(fechaInicio)
						// )
						if (fechaNovedad.isBefore(fechaFin)) {
							return true;
						} else {
							return false;
						}
					}

					if (fechaInicioNovedad && !isStart) {
						if (
							fechaInicio.isBetween(fechaInicioNovedad, fechaNovedad, 'D', '()') ||
							fechaFin.isBetween(fechaInicioNovedad, fechaNovedad, 'D', '()') ||
							(fechaInicio.isSame(fechaInicio) && fechaFin.isSame(fechaNovedad))
						) {
							// console.log('entró acá a start');
							return true;
						}
					}

					if (fechaFinNovedad && !isEnd) {
						if (
							fechaInicio.isBetween(fechaNovedad, fechaFinNovedad, 'D', '()') ||
							fechaFin.isBetween(fechaNovedad, fechaFinNovedad, 'D', '()') ||
							(fechaInicio.isSame(fechaNovedad) && fechaFin.isSame(fechaFinNovedad))
						) {
							// console.log('Entró aacá al final');
							return true;
						}
					}

					if (fechaNovedad.isBetween(fechaInicio, fechaFin, 'D', '()')) {
						return true;
					}

					return false;
				}
			);

			if (overlapNovelty) {
				overlapResults.push({
					result: false,
					message: `La fecha seleccionada se cruza con las ${noveltiesKeys[i]}`,
				});

				break;
			}
		}
	}

	if (overlapResults.length) {
		return {
			result: false,
			message: overlapResults[0].message,
		};
	} else {
		return {
			result: true,
			message: 'Validacion exitosa',
		};
	}
};

export default validateDates;
