/* eslint-disable no-throw-literal */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import ControlPanelLayout from './ControlPanelLayout';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import {
  navigationAction,
  payPilaAction,
  planillasHistoryAction,
  snackBarAction,
} from '../redux/Actions';
import {
  NAVIGATION,
  PAY_PILA,
  PLANILLAS_HISTORY,
  SNACKBAR,
} from '../redux/ActionTypes';
import SendPayButton from './../pila/api/SendPayButton';
import GetPilaInfo from '../planillasHistory/api/GetPilaInfo';
import moment from 'moment';
import 'moment/locale/es';

class ControlPanel extends Component {
  state = {
    disablePayButton: false,
    disableSendPayButton: false,
    planillas: [],
  };

  componentDidMount() {
    ChangePageTitle('Panel de control | Pilalá');
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'panelControl'
    );
    this.getPilaHistory();
  }

  goToLiquidarPILA = () => {
    this.props.history.push(`/${this.props.userUid}/console/liquidarPila/1`);
  };

  openSendPayButtonModal = () => {
    if (!this.props.showSendPayButtonModal) {
      this.props.payPilaAction(
        PAY_PILA.PAY_PILA_SHOW_SEND_PAY_BUTTON_MODAL,
        null
      );
    }
  };

  cancelSendPayButton = () => {
    if (this.props.showSendPayButtonModal) {
      this.props.payPilaAction(
        PAY_PILA.PAY_PILA_SHOW_SEND_PAY_BUTTON_MODAL,
        null
      );
    }

    if (this.props.showConfirmSendPayButtonModal) {
      this.props.payPilaAction(
        PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL,
        null
      );
    }
    this.props.payPilaAction(PAY_PILA.PAY_PILA_SAVE_PAY_BUTTON_EMAIL, '');
  };

  getPlanillaPayLink = (numeroPlanilla) => {
    this.props.history.replace(
      `/paymentCheck?NP=${numeroPlanilla}&IT=${this.props.companyIdentificationType}&IN=${this.props.activeCompanyNit}`
    );
  };

  continueSendPayButton = () => {
    if (this.props.showSendPayButtonModal) {
      if (
        /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
          this.props.payButtonEmail
        )
      ) {
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SHOW_SEND_PAY_BUTTON_MODAL,
          null
        );
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL,
          null
        );
      } else {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Información requerida',
          description: 'Ingrese el correo electrónico del destinatario',
          color: 'warning',
        });
      }
    }
  };

  confirmSendPayButtonModal = async (index) => {
    if (this.state.disableSendPayButton) return;
    this.setState({
      disableSendPayButton: true,
    });

    const {
      adminName,
      currentCompanyName,
      companyIdentificationType,
      activeCompanyNit,
      payButtonEmail,
    } = this.props;
    const { planillas } = this.state;
    const { total, numeroPlanilla, numeroCotizantes } = planillas[index];
    if (this.props.showConfirmSendPayButtonModal) {
      const payButtonResult = await SendPayButton(
        adminName,
        numeroPlanilla,
        currentCompanyName,
        numeroCotizantes,
        total,
        'dependientes',
        companyIdentificationType,
        activeCompanyNit,
        payButtonEmail
      );
      if (payButtonResult.status === 'SUCCESS') {
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL,
          null
        );
        this.props.payPilaAction(PAY_PILA.PAY_PILA_SAVE_PAY_BUTTON_EMAIL, '');
        this.setState({
          disableSendPayButton: false,
        });
      } else {
        this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error al enviar botón de pago',
          description:
            'Hemos tenido problemas para enviar el botón de pago, inténtalo nuevamente',
          color: 'error',
        });
        this.props.payPilaAction(
          PAY_PILA.PAY_PILA_SHOW_CONFIRM_SEND_PAY_BUTTON_MODAL,
          null
        );
        this.setState({
          disableSendPayButton: false,
        });
      }
    }
  };

  getPilaHistory = async () => {
    const { activeCompany } = this.props;
    const historyResult = await GetPilaInfo(activeCompany);
    console.log(historyResult);
    if (historyResult.status === 'SUCCESS') {
      this.formatPlanillasData(historyResult.data);
      this.props.planillasHistoryAction(
        PLANILLAS_HISTORY.PLANILLAS_HISTORY_SAVE_PLANILLAS,
        historyResult.data
      );
    }
  };

  formatPlanillasData = (data) => {
    const tempPlanillas = [];
    data.map((item) => {
      const periodo = moment(item.periodo.value, 'DD_MM_YYYY').format('MMMM');
      tempPlanillas.push({
        total: item.totalPagar,
        numeroPlanilla: item.idPlanilla,
        periodo,
        tipo: 'Planilla de empleados',
        numeroCotizantes: item.countCotizantes,
      });
      return null;
    });
    this.setState({
      planillas: tempPlanillas,
    });
  };

  render() {
    const layoutProps = {
      goToLiquidarPILA: this.goToLiquidarPILA,
      openSendPayButtonModal: this.openSendPayButtonModal,
      cancelSendPayButton: this.cancelSendPayButton,
      getPlanillaPayLink: this.getPlanillaPayLink,
      continueSendPayButton: this.continueSendPayButton,
      confirmSendPayButtonModal: this.confirmSendPayButtonModal,
      disablePayButton: this.state.disablePayButton,
      disableSendPayButton: this.state.disableSendPayButton,
      planillas: this.state.planillas,
    };

    return <ControlPanelLayout {...layoutProps} />;
  }
}

const mapStateToProps = (state) => {
  return {
    userUid: state.currentUser.uid,
    showSendPayButtonModal: state.payPila.showSendPayButtonModal,
    showConfirmSendPayButtonModal: state.payPila.showConfirmSendPayButtonModal,
    payButtonEmail: state.payPila.payButtonEmail,
    activeCompanyNit: state.currentUser.activeCompany
      ? state.currentUser.companiesInfo[state.currentUser.activeCompany].nit
      : null,
    adminName: state.currentUser.name,
    companyIdentificationType: state.company.identificationType.code,
    currentCompanyName: state.company.companyName,
    planillas: state.payPila.planillas,
    activeCompany: state.currentUser.activeCompany,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
    payPilaAction: (actionType, value) =>
      dispatch(payPilaAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
    planillasHistoryAction: (actionType, value) =>
      dispatch(planillasHistoryAction(actionType, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ControlPanel);
