import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    Typography,
    ButtonBase,
    Tooltip,
    CircularProgress,
    Button,
    Checkbox,
    Dialog,
    TextField,
    TableHead,
    TableRow,
    TableCell,
    DialogContent
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
    PAY_BUTTON,
    BANNER_MESSAGE,
    // TOTAL_VALUE,
    PLANILLA_TYPE
} from './Constants';
import HelperFunctions from '../utils/HelperFunctions';
import EmailTwoToneIcon from '@material-ui/icons/EmailTwoTone';
import ConfirmationModalLayout from '../pila/ConfirmationModalLayout';
import CheckBoxTwoToneIcon from '@material-ui/icons/CheckBoxTwoTone';
import CheckBoxOutlineBlankRoundedIcon from '@material-ui/icons/CheckBoxOutlineBlankRounded';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import { payPilaAction } from '../redux/Actions';
import { PAY_PILA } from '../redux/ActionTypes';
import moment from 'moment';
import 'moment/locale/es';
import { Table } from '../components';

const ControlPanelLayout = (props) => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const currentUserName = useSelector(state => state.currentUser.name);
    // const planillas = useSelector(state => state.payPila.planillas);
    const [payButtonIndex, setPayButtonIndex] = useState(null);
    const [payButtonItem, setPayButtonItem] = useState('');
    const [payLinkIndex, setPayLinkIndex] = useState(null);
    const showSendPayButtonModal = useSelector(state => state.payPila.showSendPayButtonModal);
    const showConfirmSendPayButtonModal = useSelector(state => state.payPila.showConfirmSendPayButtonModal);
    const activeCompany = useSelector(state => state.currentUser.activeCompany);
    const periodoActual = useSelector(state => state.currentUser.periodosInfo[activeCompany]);
    const month = moment(periodoActual.value, 'DD_MM_YYYY').format('MMMM');

    const openPayButtonModal = (item, index) => {
        setPayButtonIndex(index);
        setPayButtonItem(item);
        props.openSendPayButtonModal();
    }

    const getPayButtonEmail = (event) => {
        dispatch(payPilaAction(PAY_PILA.PAY_PILA_SAVE_PAY_BUTTON_EMAIL, event.target.value));
    }

    const closeConfirmSendPayButtonModal = () => {
        setPayButtonIndex(null);
        setPayButtonItem('');
        props.cancelSendPayButton();
    }

    const closeSendPayButtonModal = () => {
        setPayButtonIndex(null);
        setPayButtonItem('');
    }

    const handlePayLinkButton = (index, numeroPlanilla) => {
        setPayLinkIndex(index);
        props.getPlanillaPayLink(numeroPlanilla);
    }

    const TableHeader = () => (
        <TableHead>
            <TableRow>
                <TableCell className={classes.headerTitle}>
                    Tipo de planilla
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    N° Planilla
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    Periodo
                </TableCell>
                <TableCell align='center' className={classes.headerTitle}>
                    Total
                </TableCell>
                <TableCell />
                <TableCell />
            </TableRow>
        </TableHead>
    );

    const RowLayout = ({ row, index }) => {

        const showPayModal = () => {
            openPayButtonModal(row, index);
        }

        const openPayLink = () => {
            handlePayLinkButton(index, row.numeroPlanilla);
        }

        return (
            <TableRow
                style={{
                    backgroundColor: index % 2 === 0 ? '#ECEFF1' : '#FFFFFF'
                }}
            >
                <TableCell>
                    <Typography className={classes.planillaAportante}>
                        {row.tipo}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <Typography className={classes.planillaNumber}>
                        {row.numeroPlanilla}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <Typography className={classes.planillaPeriod}>
                        {row.periodo}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <Typography className={classes.planillaItemValue}>
                        {`$${HelperFunctions.formatStringNumber(row.total)}`}
                    </Typography>
                </TableCell>
                <TableCell align='center'>
                    <ButtonBase
                        disableRipple
                        onClick={showPayModal}
                    >
                        <Tooltip
                            arrow
                            placement='top'
                            classes={{
                                tooltip: classes.linkTooltip,
                                arrow: classes.linkTooltipArrow
                            }}
                            title={
                                <>
                                    {'Enviar link de pago'}
                                </>
                            }
                        >
                            <EmailTwoToneIcon className={classes.sendPayButtonText} />
                        </Tooltip>
                    </ButtonBase>
                </TableCell>
                <TableCell align='center'>
                    <ButtonBase
                        className={classes.payButton}
                        onClick={openPayLink}
                        disabled={props.disablePayButton}
                    >
                        {
                            props.disablePayButton && payLinkIndex === index ?
                                <CircularProgress size={18.5} className={classes.payButtonSpinner} />
                                :
                                <Typography className={classes.payButtonText}>
                                    Pagar
                                </Typography>
                        }
                    </ButtonBase>
                </TableCell>
            </TableRow>
        );
    }

    return (
        <div className={classes.container}>
            <Typography className={classes.panelTitle}>
                {`¡Hola ${currentUserName.split(' ')[0]}!`}
            </Typography>
            <div className={classes.bannerContainer}>
                <div className={classes.avatar}>
                    <Typography className={classes.monthText}>
                        {`${month}`}
                    </Typography>
                </div>
                <div className={classes.messageContainer}>
                    <Typography className={classes.messageText}>
                        {BANNER_MESSAGE}
                    </Typography>
                    {/* <Typography className={classes.planillaValue}>
                        {`${TOTAL_VALUE} ${'1.000.000'}`}
                    </Typography> */}
                    <Typography className={classes.planillaType}>
                        {`${PLANILLA_TYPE} ${'Empleados'}`}
                    </Typography>
                </div>
                <ButtonBase className={classes.pilaButton} onClick={props.goToLiquidarPILA}>
                    <Typography className={classes.pilaButtonText}>
                        {PAY_BUTTON}
                    </Typography>
                </ButtonBase>
            </div>
            <div
                className={classes.planillasContent}
                style={{
                    height: props.planillas.length === 0 ? '50%' : null
                }}
            >
                <Typography className={classes.planillasTitle}>
                    Estado de planillas guardadas
                </Typography>
                <div className={classes.planillasContainer}>
                    <Table
                        Row={
                            (props) => (
                                <RowLayout
                                    {...props}
                                    openPayButtonModal={openPayButtonModal}
                                    handlePayLinkButton={handlePayLinkButton}
                                    payLinkIndex={payLinkIndex}
                                    disablePayButton={props.disablePayButton}
                                />
                            )
                        }
                        data={props.planillas}
                        withActionbar={false}
                        Head={TableHeader}
                    />
                </div>
            </div>
            <Dialog
                open={showSendPayButtonModal}
                maxWidth='sm'
                PaperProps={{
                    className: classes.paperContainer
                }}
                onClose={closeSendPayButtonModal}
                disableScrollLock={true}
                disableBackdropClick={true}
            >
                <DialogContent>
                    <Typography className={classes.sendButtonTitle}>
                        ¿A quién le vas a enviar el botón?
                    </Typography>
                    <TextField
                        variant='outlined'
                        className={classes.sendButtonInput}
                        inputProps={{
                            style: {
                                padding: '0.4em 0.8em'
                            }
                        }}
                        classes={{
                            root: classes.inputRoot
                        }}
                        onChange={getPayButtonEmail}
                    />
                    <div className={classes.sendButtonPlanillaItem}>
                        <Typography className={classes.planillaAportanteText}>
                            {payButtonItem.tipo}
                        </Typography>
                        <Typography className={classes.planillaNumberText}>
                            {payButtonItem.numeroPlanilla}
                        </Typography>
                        <Typography className={classes.planillaPeriodText}>
                            {payButtonItem.periodo}
                        </Typography>
                        <Typography className={classes.planillaValueText}>
                            {`$${HelperFunctions.formatStringNumber(payButtonItem.total)}`}
                        </Typography>
                    </div>
                    <div className={classes.sendMessageContainer}>
                        <Checkbox
                            checked={true}
                            disabled={true}
                            checkedIcon={
                                <CheckBoxTwoToneIcon className={classes.messageCheckedIcon} />
                            }
                            icon={
                                <CheckBoxOutlineBlankRoundedIcon className={classes.messageCheckbox} />
                            }
                        />
                        <Typography className={classes.sendMessageText}>
                            Esta persona cuenta con autorización para recibir esta información y realizar el pago.
                        </Typography>
                    </div>
                    <div
                        className={classes.buttonsContainer}
                    >
                        <Button
                            className={classes.cancelButton}
                            onClick={props.cancelSendPayButton}
                        >
                            <Typography
                                className={classes.cancelButtonText}
                            >
                                Cancelar
                            </Typography>
                        </Button>
                        <Button
                            className={classes.saveButton}
                            onClick={props.continueSendPayButton}
                        >
                            <Typography
                                className={classes.saveButtonText}
                            >
                                Continuar
                            </Typography>
                            <ArrowForwardRoundedIcon className={classes.arrowIcon} />
                        </Button>
                    </div>
                </DialogContent>
            </Dialog>
            <ConfirmationModalLayout
                id="sendPayButton"
                showModal={showConfirmSendPayButtonModal}
                message="¡Enviaremos un correo electrónico a esta persona con un resumen de la planilla y el botón de pago. Los valores pueden ser ajustados por validaciones del operador de información!"
                cancelAction={closeConfirmSendPayButtonModal}
                confirmAction={props.confirmSendPayButtonModal.bind(this, payButtonIndex)}
                alertColor="#2962FF"
                textColor="#FFFFFF"
                disableConfirmButton={props.disableSendPayButton}
            />
        </div>
    );
}

const useStyles = makeStyles({
    container: {
        display: 'flex',
        flex: 1,
        padding: '3% 2% 0% 3%',
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    bannerContainer: {
        width: '100%',
        display: 'flex',
        background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
        borderRadius: 5,
        marginTop: '1.5%',
        padding: '1% 2.5% 1% 1.5%',
        alignItems: 'center',
        boxShadow: '0px 3px 6px #00000029'
    },
    panelTitle: {
        fontSize: '1.7rem',
        fontWeight: 'bold',
        color: '#000000'
    },
    avatar: {
        border: '2px solid #FFFFFF',
        borderRadius: 7,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: '2%',
        padding: '0.5rem 1.2rem'
    },
    dayText: {
        color: '#FFFFFF',
        margin: 0,
        fontSize: '1.8em',
        lineHeight: 1.4,
        fontWeight: 'bold'
    },
    monthText: {
        color: '#FFFFFF',
        fontSize: '1.2rem',
        textTransform: 'capitalize'
    },
    messageContainer: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    pilaButton: {
        backgroundColor: '#FFBC00',
        borderRadius: 100,
        width: '23%',
        padding: '0.8% 0% 0.8% 0%'
    },
    pilaButtonText: {
        color: '#FFFFFF',
        fontSize: '1rem',
        fontWeight: 'bold'
    },
    messageText: {
        color: '#FFFFFF',
        fontSize: '1.7em',
        fontWeight: 'bold'
    },
    planillaValue: {
        color: '#FFFFFF',
        fontSize: '1.3em',
        fontWeight: 'bold'
    },
    planillaType: {
        color: '#FFFFFF',
        fontSize: '1em',
        fontWeight: 'bold'
    },
    planillasTitle: {
        fontSize: '1.5rem',
        fontWeight: 'bold',
        color: '#263238',
        textAlign: 'left',
        marginBottom: '1%'
    },
    planillasContainer: {
        width: '70%',
        height: '100%'
    },
    planillaItem: {
        width: '100%',
        height: 50,
        display: 'flex',
        alignItems: 'center'
    },
    planillaAportante: {
        fontWeight: 'bold',
        color: '#455A64',
        fontSize: '0.9rem',
        textAlign: 'left'
    },
    planillaNumber: {
        color: '#455A64',
        fontSize: '0.9rem'
    },
    planillaPeriod: {
        fontWeight: 'bold',
        color: '#455A64',
        fontSize: '0.9rem',
        textTransform: 'capitalize'
    },
    planillaItemValue: {
        color: '#455A64',
        fontSize: '0.9rem'
    },
    linkTooltip: {
        backgroundColor: '#000000'
    },
    linkTooltipArrow: {
        color: '#000000'
    },
    sendPayButtonText: {
        fontSize: '1.5rem',
        color: '#2962FF'
    },
    payButton: {
        backgroundColor: '#6A32B526',
        borderRadius: 100,
        border: '2px solid #6A32B5',
        padding: '0.25em 0px 0.25em 0px'
    },
    payButtonText: {
        fontSize: '0.9rem',
        color: '#6A32B5',
        padding: '0px 1.5em'
    },
    payButtonSpinner: {
        color: '#6A32B5',
        margin: '0.1em 2.4em'
    },
    planillasContent: {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        marginTop: '5%'
    },
    planillaAportanteContainer: {
        display: 'flex',
        flex: 0.4,
        paddingLeft: '2%'
    },
    planillaNumberContainer: {
        display: 'flex',
        flex: 0.1
    },
    planillaPeriodContainer: {
        display: 'flex',
        flex: 0.1
    },
    planillaValueContainer: {
        display: 'flex',
        flex: 0.15
    },
    mailContainer: {
        display: 'flex',
        flex: 0.05,
        justifyContent: 'center'
    },
    payButtonContainer: {
        display: 'flex',
        flex: 0.2,
        justifyContent: 'center'
    },
    planillasHeader: {
        width: '100%',
        height: '14.3%',
        display: 'flex',
        alignItems: 'center',
        paddingLeft: '1.5%'
    },
    planillasSubcontainer: {
        display: 'table',
        width: '100%',
        height: '85.8%',
        flexDirection: 'column'
    },
    headerAportanteContainer: {
        display: 'flex',
        flex: 0.4
    },
    headerNumberContainer: {
        display: 'flex',
        flex: 0.1
    },
    headerPeriodContainer: {
        display: 'flex',
        flex: 0.1
    },
    headerValueContainer: {
        display: 'flex',
        flex: 0.15
    },
    headerTitle: {
        fontSize: '1rem',
        fontWeight: 'bold',
        color: '#263238'
    },
    paperContainer: {
        backgroundColor: '#FFFFFF',
        display: 'flex',
        flex: 1,
        padding: '2em'
    },
    sendButtonTitle: {
        color: '#5E35B1',
        fontWeight: 'bold',
        fontSize: '1.1rem'
    },
    sendButtonInput: {
        width: '100%',
        borderRadius: 7,
        marginTop: '5%',
        marginBottom: '5%'
    },
    inputRoot: {
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#707070',
            },
            '&:hover fieldset': {
                borderColor: '#5E35B1',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#5E35B1',
            },
        },
    },
    sendButtonPlanillaItem: {
        backgroundColor: '#ECEFF1',
        borderRadius: 7,
        padding: '0.4em 0.8em',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    planillaAportanteText: {
        fontWeight: 'bold',
        color: '#455A64',
        fontSize: '0.9rem'
    },
    planillaNumberText: {
        color: '#455A64',
        fontSize: '0.9rem'
    },
    planillaPeriodText: {
        fontWeight: 'bold',
        color: '#455A64',
        fontSize: '0.9rem',
        textTransform: 'capitalize'
    },
    planillaValueText: {
        color: '#455A64',
        fontSize: '0.9rem'
    },
    sendMessageContainer: {
        display: 'flex',
        marginTop: '3%'
    },
    messageCheckedIcon: {
        fontSize: '1em',
        color: '#4CAF50',
    },
    messageCheckbox: {
        color: '#4CAF50',
        fontSize: '1em',
    },
    sendMessageText: {
        color: '#95989A',
        fontSize: '0.9rem',
        paddingRight: '10%'
    },
    buttonsContainer: {
        display: 'flex',
        alignItems: 'center',
        marginTop: '5%',
        justifyContent: 'flex-end'
    },
    cancelButton: {
        marginRight: 10,
        borderRadius: 100,
        textTransform: 'none',
        padding: '0.4em 1em'
    },
    cancelButtonText: {
        color: '#5E35B1'
    },
    saveButton: {
        background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
        borderRadius: 22,
        marginLeft: 10,
        textTransform: 'none',
        display: 'flex',
        alignItems: 'center',
        padding: '0.4em 1.5em'
    },
    saveButtonText: {
        color: 'white',
        fontWeight: 'bold',
        marginRight: '1em'
    },
    arrowIcon: {
        color: '#FFFFFF'
    }
});

export default ControlPanelLayout;
