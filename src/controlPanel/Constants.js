export const PAY_BUTTON = 'Liquidar Planilla';
export const BANNER_MESSAGE = '¡Ya está lista tu planilla para el próximo periodo!';
export const TOTAL_VALUE = 'Valor a pagar: $';
export const PLANILLA_TYPE = 'Tipo de planilla:';