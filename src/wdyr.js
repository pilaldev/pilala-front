/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable global-require */
import React from 'react';

if (process.env.REACT_APP_ENV === 'DEV') {
  const whyDidYouRender = require('@welldone-software/why-did-you-render');
  whyDidYouRender(React, {
    trackAllPureComponents: true,
    titleColor: '#18dcff',
    diffNameColor: '#c56cf0',
    diffPathColor: '#ff4d4d',
    collapseGroups: true,
  });
}
