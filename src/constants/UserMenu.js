export const CARD_TYPE = {
    USER: 'user',
    COMPANY: 'company'
}

export const MULTI_COMPANY_PANEL = 'Abrir panel multiempresa';
export const SUCURSAL = 'Sucursal';
export const MAIN_COMPANY = 'Principal';