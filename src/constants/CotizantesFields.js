import HelperFunctions from './../utils/HelperFunctions';

export const emptyRecordFields = {
    1: HelperFunctions.createPaddingField('N', 2),
    2: HelperFunctions.createPaddingField('N', 5),
    3: HelperFunctions.createPaddingField('A', 2),
    4: HelperFunctions.createPaddingField('A', 16),
    5: HelperFunctions.createPaddingField('N', 2),
    6: HelperFunctions.createPaddingField('N', 2),
    7: HelperFunctions.createPaddingField('A', 1),
    8: HelperFunctions.createPaddingField('A', 1),
    9: HelperFunctions.createPaddingField('A', 2),
    10: HelperFunctions.createPaddingField('A', 3),
    11: HelperFunctions.createPaddingField('A', 20),
    12: HelperFunctions.createPaddingField('A', 30),
    13: HelperFunctions.createPaddingField('A', 20),
    14: HelperFunctions.createPaddingField('A', 30),
    15: HelperFunctions.createPaddingField('A', 1),
    16: HelperFunctions.createPaddingField('A', 1),
    17: HelperFunctions.createPaddingField('A', 1),
    18: HelperFunctions.createPaddingField('A', 1),
    19: HelperFunctions.createPaddingField('A', 1),
    20: HelperFunctions.createPaddingField('A', 1),
    21: HelperFunctions.createPaddingField('A', 1),
    22: HelperFunctions.createPaddingField('A', 1),
    23: HelperFunctions.createPaddingField('A', 1),
    24: HelperFunctions.createPaddingField('A', 1),
    25: HelperFunctions.createPaddingField('A', 1),
    26: HelperFunctions.createPaddingField('A', 1),
    27: HelperFunctions.createPaddingField('A', 1),
    28: HelperFunctions.createPaddingField('A', 1),
    29: HelperFunctions.createPaddingField('A', 1),
    30: HelperFunctions.createPaddingField('N', 2),
    31: HelperFunctions.createPaddingField('A', 6),
    32: HelperFunctions.createPaddingField('A', 6),
    33: HelperFunctions.createPaddingField('A', 6),
    34: HelperFunctions.createPaddingField('A', 6),
    35: HelperFunctions.createPaddingField('A', 6),
    36: HelperFunctions.createPaddingField('N', 2),
    37: HelperFunctions.createPaddingField('N', 2),
    38: HelperFunctions.createPaddingField('N', 2),
    39: HelperFunctions.createPaddingField('N', 2),
    40: HelperFunctions.createPaddingField('N', 9),
    41: HelperFunctions.createPaddingField('A', 1),
    42: HelperFunctions.createPaddingField('N', 9),
    43: HelperFunctions.createPaddingField('N', 9),
    44: HelperFunctions.createPaddingField('N', 9),
    45: HelperFunctions.createPaddingField('N', 9),
    46: HelperFunctions.createPaddingField('N', 7),
    47: HelperFunctions.createPaddingField('N', 9),
    48: HelperFunctions.createPaddingField('N', 9),
    49: HelperFunctions.createPaddingField('N', 9),
    50: HelperFunctions.createPaddingField('N', 9),
    51: HelperFunctions.createPaddingField('N', 9),
    52: HelperFunctions.createPaddingField('N', 9),
    53: HelperFunctions.createPaddingField('N', 9),
    54: HelperFunctions.createPaddingField('N', 7),
    55: HelperFunctions.createPaddingField('N', 9),
    56: HelperFunctions.createPaddingField('N', 9),
    57: HelperFunctions.createPaddingField('A', 15),
    58: HelperFunctions.createPaddingField('N', 9),
    59: HelperFunctions.createPaddingField('A', 15),
    60: HelperFunctions.createPaddingField('N', 9),
    61: HelperFunctions.createPaddingField('N', 9),
    62: HelperFunctions.createPaddingField('N', 9),
    63: HelperFunctions.createPaddingField('N', 9),
    64: HelperFunctions.createPaddingField('N', 7),
    65: HelperFunctions.createPaddingField('N', 9),
    66: HelperFunctions.createPaddingField('N', 7),
    67: HelperFunctions.createPaddingField('N', 9),
    68: HelperFunctions.createPaddingField('N', 7),
    69: HelperFunctions.createPaddingField('N', 9),
    70: HelperFunctions.createPaddingField('N', 7),
    71: HelperFunctions.createPaddingField('N', 9),
    72: HelperFunctions.createPaddingField('N', 7),
    73: HelperFunctions.createPaddingField('N', 9),
    74: HelperFunctions.createPaddingField('A', 2),
    75: HelperFunctions.createPaddingField('A', 16),
    76: HelperFunctions.createPaddingField('A', 1),
    77: HelperFunctions.createPaddingField('A', 6),
    78: HelperFunctions.createPaddingField('A', 1),
    79: HelperFunctions.createPaddingField('A', 1),
    80: HelperFunctions.createPaddingField('A', 10),
    81: HelperFunctions.createPaddingField('A', 10),
    82: HelperFunctions.createPaddingField('A', 10),
    83: HelperFunctions.createPaddingField('A', 10),
    84: HelperFunctions.createPaddingField('A', 10),
    85: HelperFunctions.createPaddingField('A', 10),
    86: HelperFunctions.createPaddingField('A', 10),
    87: HelperFunctions.createPaddingField('A', 10),
    88: HelperFunctions.createPaddingField('A', 10),
    89: HelperFunctions.createPaddingField('A', 10),
    90: HelperFunctions.createPaddingField('A', 10),
    91: HelperFunctions.createPaddingField('A', 10),
    92: HelperFunctions.createPaddingField('A', 10),
    93: HelperFunctions.createPaddingField('A', 10),
    94: HelperFunctions.createPaddingField('A', 10),
    95: HelperFunctions.createPaddingField('N', 9),
    96: HelperFunctions.createPaddingField('N', 3),
    97: HelperFunctions.createPaddingField('A', 10)
}

export const editRecordField = {
    1: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 2)),
    2: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 5)),
    3: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 2)),
    4: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 16)),
    5: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 2)),
    6: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 2)),
    7: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    8: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    9: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 2)),
    10: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 3)),
    11: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 20)),
    12: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 30)),
    13: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 20)),
    14: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 30)),
    15: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    16: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    17: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    18: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    19: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    20: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    21: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    22: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    23: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    24: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    25: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    26: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    27: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    28: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    29: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    30: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 2)),
    31: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 6)),
    32: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 6)),
    33: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 6)),
    34: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 6)),
    35: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 6)),
    36: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 2)),
    37: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 2)),
    38: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 2)),
    39: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 2)),
    40: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    41: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    42: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    43: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    44: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    45: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    46: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 7)),
    47: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    48: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    49: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    50: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    51: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    52: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    53: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    54: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 7)),
    55: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    56: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    57: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 15)),
    58: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    59: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 15)),
    60: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    61: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    62: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    63: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    64: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 7)),
    65: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    66: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 7)),
    67: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    68: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 7)),
    69: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    70: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 7)),
    71: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    72: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 7)),
    73: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    74: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 2)),
    75: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 16)),
    76: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    77: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 6)),
    78: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    79: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 1)),
    80: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    81: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    82: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    83: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    84: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    85: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    86: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    87: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    88: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    89: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    90: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    91: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    92: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    93: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    94: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10)),
    95: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 9)),
    96: (currentValue) => (HelperFunctions.paddingField(currentValue, 'N', 3)),
    97: (currentValue) => (HelperFunctions.paddingField(currentValue, 'A', 10))
}

export const RECORDS_TYPE = {
    // Ingreso
    1: 'A',
    // Variacion permantente salario
    2: 'B1',
    // Variacion transitoria salario
    3: 'B2',
    // Vacaciones o licencia remunerada
    4: 'C1',
    // Licencia maternidad
    5: 'C2',
    // Suspension de contrato o licencia no remunerada
    6: 'D',
    // Incapacidad temporal por enfermedad general
    7: 'E1',
    // Incapacidad por accidente de trabajo o enfermedad laboral
    8: 'E2',
    // Descuentos ARL
    9: 'F'
}