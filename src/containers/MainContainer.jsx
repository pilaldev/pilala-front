import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Switch, Route, useHistory } from 'react-router-dom';
import SignIn from '@bit/pilala.pilalalib.components.signin';
import moment from 'moment';
import FetchData from '../fetchData/FetchData';
import AppContainer from './AppContainer';
import PayButtonCheck from '../payButtonCheck/PayButtonCheck';
import NotFound from '../components/NotFound';
import WithAuthorization from '../components/SecuredComponent';
import ResetPassword from '../authentication/ResetPassword';
import Snackbar from '../components/Snackbar';
import BlockScreen from '../components/BlockScreen';
import SignUp from '../signup';
import CreatePassword from '../signup/CreatePassword';
import ForgotPassword from '../authentication/ForgotPassword';
import { FIREBASE_AUTH } from '../api/Firebase';
import { signOutAction } from '../redux/Actions';
import { useUpdateVersion } from '../hooks';
import { CredentialsModal } from '../components';
import './styles.css';

moment.locale('es');

const MainContainer = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const loggedIn = JSON.parse(localStorage.getItem('loggedIn'));
  const loggedUid = JSON.parse(localStorage.getItem('loggedUid'));
  const currentUserEmail = useSelector((state) => state.currentUser.email);
  const email = useRef(currentUserEmail);
  const [isLatestVersion, updateLatestVersion] = useUpdateVersion();

  useEffect(() => {
    if (isLatestVersion === false) {
      updateLatestVersion();
      history.replace(`/${loggedUid}/fetchData`, {
        email: email.current,
      });
      dispatch(signOutAction());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLatestVersion]);

  return (
    <div className="mainContainer">
      <Switch>
        <Route
          exact
          path="/"
          render={(props) => (
            <SignIn {...props} firebaseInstance={FIREBASE_AUTH} />
          )}
        />
        <Route path="/signUp" component={SignUp} />
        <Route path="/createPassword" component={CreatePassword} />
        <Route exact path="/forgotPassword" component={ForgotPassword} />
        <Route path="/resetPassword" component={ResetPassword} />
        <Route path="/paymentCheck" component={PayButtonCheck} />
        <WithAuthorization
          path="/:userId/fetchData/"
          component={FetchData}
          auth={loggedIn}
        />
        <WithAuthorization
          path="/:userId/console/"
          component={AppContainer}
          auth={loggedIn}
        />
        <Route component={NotFound} />
      </Switch>
      <Snackbar />
      <BlockScreen />
      <CredentialsModal />
    </div>
  );
};

export default MainContainer;
