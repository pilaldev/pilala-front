import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppContainerLayout from './../components/AppContainerLayout';
import { FIREBASE_AUTH, FIRESTORE } from './../api/Firebase';
import {
  currentUserAction,
  companyAction,
  navigationAction,
  snackBarAction,
  signOutAction,
} from './../redux/Actions';
import {
  CURRENT_USER,
  COMPANY,
  NAVIGATION,
  SNACKBAR,
} from './../redux/ActionTypes';
import UpdateOnlineStatus from './../utils/UpdateOnlineStatus';

class AppContainer extends Component {
  constructor() {
    super();
    this.flagsListener = () => {};
  }

  componentDidMount() {
    if (!this.props.location.key) {
      this.props.history.replace(
        `/${this.props.match.params.userId}/fetchData`
      );
    }

    FIREBASE_AUTH.onAuthStateChanged((user) => {
      if (!user) {
        localStorage.setItem('loggedIn', JSON.stringify(false));
        this.props.history.replace('/');
        this.props.signOutAction();
      }
    });

    this.getRemoteFlags();
  }

  componentWillUnmount() {
    this.flagsListener();
  }

  getRemoteFlags = () => {
    this.flagsListener = FIRESTORE.collection('users')
      .doc(this.props.currentUser.email)
      .onSnapshot(
        (doc) => {
          const { featureFlags, accountManager } = doc.data();
          this.props.currentUserAction(
            CURRENT_USER.SAVE_FEATURE_FLAGS,
            featureFlags ?? {}
          );
          this.props.currentUserAction(
            CURRENT_USER.SAVE_ACCOUNT_MANAGER,
            accountManager ?? []
          );
        },
        (error) => {
          console.log('Error leyendo feature flags', error);
        }
      );
  };

  closeUserSession = async () => {
    await UpdateOnlineStatus(this.props.currentUser.email, false);
    FIREBASE_AUTH.signOut().catch(() => {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Error inesperado',
        description: 'Ha ocurrido un error al cerrar la sesión',
        color: 'error',
      });
    });
  };

  openMulticompanyPanel = () => {
    this.props.history.push(
      `/${this.props.currentUser.uid}/console/multicompanyPanel`
    );
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'panelMultiempresa'
    );
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_ACTIVE_PANEL_MULTIEMPRESA,
      true
    );
  };

  selectCompany = (idCompany) => {
    if (this.props.currentUser.companiesInfo[idCompany]) {
      const selectedCompany = this.props.currentUser.companiesInfo[idCompany];
      let companyData = {
        companyName: selectedCompany.companyName,
        economicActivity: selectedCompany.economicActivity,
        province: selectedCompany.location.province,
        city: selectedCompany.location.city,
        phoneNumber: selectedCompany.phoneNumber,
        cellphoneNumber: selectedCompany.mobileNumber,
        identificationType: selectedCompany.identificationType,
        identificationNumber: selectedCompany.nit,
        verificationDigit: selectedCompany.verificationDigit,
        arl: selectedCompany.arl,
        cajaCompensacion: selectedCompany.cajaCompensacion,
        claseArl: selectedCompany.claseArl,
        address: selectedCompany.location.address,
        claseAportante: selectedCompany.clasificacionAportanteCodigo,
        exentoParafiscales: selectedCompany.exentoPagoParafiscales,
        beneficiadoLey590: selectedCompany.beneficiadoLey590Del2000,
        constitucionDate: selectedCompany.fechaConstitucion,
        obligacionSena: !selectedCompany.exentoPagoParafiscales,
        obligacionICBF: !selectedCompany.exentoPagoParafiscales,
      };

      let LRData = {
        name: selectedCompany.legalRepresentative.name,
        email: selectedCompany.legalRepresentative.email,
        identificationType: selectedCompany.legalRepresentative.documentType,
        identificationNumber:
          selectedCompany.legalRepresentative.documentNumber,
        phoneNumber: selectedCompany.legalRepresentative.phone,
        cellphoneNumber: selectedCompany.legalRepresentative.mobileNumber,
      };

      let BCData = {
        name: selectedCompany.companyContact.name,
        email: selectedCompany.companyContact.email,
        identificationType: selectedCompany.companyContact.documentType,
        identificationNumber: selectedCompany.companyContact.documentNumber,
        phoneNumber: selectedCompany.companyContact.phone,
        cellphoneNumber: selectedCompany.companyContact.mobileNumber,
      };

      const fields = {
        mainCompany: selectedCompany.isCompany,
        selectedSucursal: selectedCompany.isCompany
          ? 'Principal'
          : selectedCompany.sucursalName,
      };

      this.props.companyAction(COMPANY.COMPANY_UPDATE_SOME_FIELDS, fields);
      this.props.companyAction(COMPANY.COMPANY_SAVE_ALL_DATA, companyData);
      this.props.companyAction(COMPANY.COMPANY_SAVE_LR_ALL_DATA, LRData);
      this.props.companyAction(COMPANY.COMPANY_SAVE_BC_ALL_DATA, BCData);
      this.props.currentUserAction(
        CURRENT_USER.CURRENT_USER_SAVE_USER_SELECTED_COMPANY,
        idCompany
      );
      this.props.navigationAction(
        NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
        'panelControl'
      );
      this.props.navigationAction(
        NAVIGATION.NAVIGATION_ACTIVE_PANEL_MULTIEMPRESA,
        false
      );
      this.props.history.push(
        `/${this.props.currentUser.uid}/console/controlPanel`
      );
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Aportante no encontrado',
        description: 'No se encontró la información de este aportante',
        color: 'warning',
      });
    }
  };

  render() {
    const layoutProps = {
      userCardProps: {
        options: [
          {
            label: 'Cerrar sesión',
            action: this.closeUserSession,
          },
        ],
        openMulticompanyPanel: this.openMulticompanyPanel,
        selectCompany: this.selectCompany,
      },
    };

    return <AppContainerLayout {...layoutProps} />;
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    currentUserAction: (actionType, value) =>
      dispatch(currentUserAction(actionType, value)),
    companyAction: (actionType, value) =>
      dispatch(companyAction(actionType, value)),
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
    signOutAction: (actionType, value) =>
      dispatch(signOutAction(actionType, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
