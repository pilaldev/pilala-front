import React from 'react';
import { action } from '@storybook/addon-actions';
import { Chip } from '../components';
import FaceIcon from '@material-ui/icons/Face';

export default {
    title: 'Atoms/Chip',
    component: Chip,
    args: {
        label: 'Chip Pilalá'
    },
    argTypes: {
        chipcolor: {
            control: {
                type: 'select',
                options: ['primary', 'secondary', 'info', 'success', 'warning', 'error', 'grey'],
                default: 'primary'
            },
        },
        icon: {
            control: {
                disable: true
            }
        }
    },
};

const Template = args => <Chip {...args} />;

export const Default = Template.bind({});

export const Outlined = Template.bind({});
Outlined.args = {
    outlined: true,
}

export const WithIcon = Template.bind({});
WithIcon.args = {
    icon: FaceIcon
}

export const Clickable = Template.bind({});
Clickable.args = {
    clickable: true,
    handleclick: action('¡Chip presionado!')
}

