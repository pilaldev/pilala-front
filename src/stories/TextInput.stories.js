import React from 'react';
import { action } from '@storybook/addon-actions';
import { TextInput } from '../components';
import FaceIcon from '@material-ui/icons/Face';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default {
    title: 'Atoms/TextInput',
    component: TextInput,
    args: {
        onChange: action('Escribiendo en text input')
    },
    argTypes: {
        startIcon: {
            control: {
                disable: true
            },
        },
        endIcon: {
            control: {
                disable: true
            },
        }
    },
};

const Template = args => <TextInput {...args} />;

export const Default = Template.bind({});
Default.args = {
    placeholder: 'Nombre completo'
}

export const WithStartIcon = Template.bind({});
WithStartIcon.args = {
    startIcon: <FaceIcon />,
    placeholder: 'Nombre de usuario'
}

export const WithEndIcon = Template.bind({});
WithEndIcon.args = {
    endIcon: <ExpandMoreIcon />,
    placeholder: 'Seleccionar categoría'
}

export const Disabled = Template.bind({});
Disabled.args = {
    disabled: true,
    placeholder: 'Correo electrónico'
}

export const Complete = Template.bind({});
Complete.args = {
    value: 'my_email@example.com',
    label: 'Correo electrónico',
    helperText: 'Ingresa tu correo electrónico'
}

export const WithError = Template.bind({});
WithError.args = {
    label: 'Correo electrónico',
    value: 'email@-@example.com',
    helperText: 'Ingresa un correo electrónico válido',
    error: true
}



