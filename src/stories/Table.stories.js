import React from 'react';
import { TableCell, TableHead, TableRow } from '@material-ui/core';
import { Table } from '../components';
import { action } from '@storybook/addon-actions';
import CloudDownloadOutlinedIcon from '@material-ui/icons/CloudDownloadOutlined';

export default {
    title: 'Components/Table',
    component: Table,
    args: {
        data: [
            {"id": "16530819 3860", "name": "Quintessa Bates", "phone": "1-750-163-0803", "email": "mattis.velit.justo@fringilla.edu", "city": "Meux", "company": "Ornare Egestas Ltd"},
            {"id": "16180404 8666", "name": "Gay Strickland", "phone": "1-796-227-7591", "email": "enim@feliseget.edu", "city": "Langley", "company": "Vitae Erat LLP"},
            {"id": "16150302 6252", "name": "Brenna Rhodes", "phone": "410-2287", "email": "Mauris.vel.turpis@nunc.com", "city": "Mombaruzzo", "company": "Etiam Laoreet Libero Consulting"},
            {"id": "16730326 1999", "name": "Jenette Hernandez", "phone": "177-3698", "email": "eleifend@egestasDuis.edu", "city": "Avelgem", "company": "Amet Associates"},
            {"id": "16410110 2913", "name": "Bianca Rowe", "phone": "1-820-382-0455", "email": "sem.ut@elit.co.uk", "city": "Sikar", "company": "Egestas Aliquam Consulting"},
            {"id": "16740305 9749", "name": "Lana Clements", "phone": "429-4979", "email": "enim.diam@ipsumportaelit.co.uk", "city": "Calvello", "company": "Amet Company"},
            {"id": "16210314 0154", "name": "Irene Sosa", "phone": "1-989-485-0993", "email": "nisl@etmagnis.com", "city": "Ramskapelle", "company": "Ultrices Duis Volutpat Ltd"},
            {"id": "16230920 8631", "name": "Fleur Sanford", "phone": "656-5327", "email": "quis.turpis@Nuncpulvinar.edu", "city": "Parrano", "company": "Sollicitudin Commodo Foundation"},
            {"id": "16330724 7357", "name": "Reagan Barr", "phone": "475-8489", "email": "Proin.eget.odio@lectus.com", "city": "Tielrode", "company": "Ut Odio Foundation"},
            {"id": "16740805 4778", "name": "Melinda Collins", "phone": "1-195-496-3555", "email": "magna@consectetuer.com", "city": "Denbigh", "company": "Aliquet Magna Company"},
            {"id": "16480528 3456", "name": "Mari Page", "phone": "845-5295", "email": "enim@mieleifendegestas.org", "city": "Cartago", "company": "Elit Pede Malesuada Ltd"},
            {"id": "16600523 1425", "name": "Gisela Cabrera", "phone": "1-932-124-7082", "email": "Phasellus@ipsumporta.edu", "city": "Sagamu", "company": "Nec Ante Blandit Foundation"},
            {"id": "16420309 3382", "name": "Pamela Hewitt", "phone": "1-288-496-0854", "email": "auctor.Mauris.vel@quamquisdiam.ca", "city": "Campbeltown", "company": "Mauris Limited"},
            {"id": "16340111 5047", "name": "Olivia Moody", "phone": "1-932-662-9888", "email": "nascetur@utodiovel.net", "city": "Saint-Pierre", "company": "Lorem Donec Elementum Corporation"},
            {"id": "16281011 4021", "name": "Ava Cardenas", "phone": "812-7214", "email": "nibh.lacinia@turpisAliquamadipiscing.com", "city": "Saint-Pierre", "company": "Malesuada Fames Ac Corp."},
            {"id": "16730120 1336", "name": "Lesley Brooks", "phone": "545-1846", "email": "nisi@Donecatarcu.ca", "city": "Salem", "company": "Auctor Odio Associates"},
            {"id": "16470514 6688", "name": "Naida Hernandez", "phone": "874-6933", "email": "amet@euismodin.ca", "city": "North Cowichan", "company": "Tristique Foundation"},
            {"id": "16830816 2307", "name": "Chantale Heath", "phone": "732-1023", "email": "Mauris.non.dui@eusemPellentesque.ca", "city": "Frutillar", "company": "Purus Duis LLP"},
            {"id": "16641125 6172", "name": "Yolanda Todd", "phone": "1-443-144-9399", "email": "eu.dolor.egestas@duiSuspendisseac.co.uk", "city": "Yongin", "company": "Est Incorporated"},
            {"id": "16770414 7722", "name": "Kimberly Flowers", "phone": "526-7999", "email": "nibh@acarcuNunc.com", "city": "Ongole", "company": "In Tempus LLC"},
            {"id": "16941009 7738", "name": "Kirestin David", "phone": "325-7403", "email": "ligula@sempereratin.edu", "city": "Tame", "company": "Mauris Quis Industries"},
            {"id": "16791102 9838", "name": "Jael Meadows", "phone": "980-2555", "email": "massa.Quisque.porttitor@uteros.co.uk", "city": "Plast", "company": "Aenean Euismod Industries"},
            {"id": "16901202 3215", "name": "Hilary Baker", "phone": "1-529-729-8271", "email": "Donec.feugiat@laoreet.co.uk", "city": "Stirling", "company": "Adipiscing Elit Consulting"},
            {"id": "16770115 2121", "name": "Denise Buck", "phone": "197-2459", "email": "enim.consequat@purusMaecenas.ca", "city": "Nankana Sahib", "company": "Accumsan Neque PC"},
            {"id": "16840124 6502", "name": "Ocean Kerr", "phone": "1-630-422-5070", "email": "In.tincidunt.congue@euligula.ca", "city": "Grande Prairie", "company": "Non PC"},
            {"id": "16530428 2428", "name": "Sloane Lloyd", "phone": "1-116-653-1370", "email": "ullamcorper@Nullamut.ca", "city": "Hisar", "company": "Sem Consequat LLP"},
            {"id": "16450812 4973", "name": "Bell Daniel", "phone": "1-336-717-4704", "email": "molestie@Praesentinterdumligula.edu", "city": "Parchim	City", "company": "Vulputate Mauris LLP"},
            {"id": "16200805 9343", "name": "Jenette Morton", "phone": "770-0759", "email": "lacinia@iaculis.co.uk", "city": "Montemilone", "company": "Elementum Ltd"},
            {"id": "16510623 2670", "name": "Sonia Payne", "phone": "1-510-741-7549", "email": "nunc.sed@eueros.edu", "city": "Lang", "company": "Blandit Nam Inc."},
            {"id": "16520317 3744", "name": "Joy Gordon", "phone": "888-6658", "email": "non.enim@ipsumprimis.org", "city": "Biryuch", "company": "Arcu Curabitur Ut Incorporated"},
            {"id": "16010201 1681", "name": "Karen Mcfadden", "phone": "1-220-651-9255", "email": "et@In.net", "city": "Montague", "company": "Ridiculus Mus Limited"},
            {"id": "16370806 7081", "name": "Kalia Ryan", "phone": "1-422-623-7243", "email": "a.felis.ullamcorper@Vivamuseuismodurna.net", "city": "Navsari", "company": "Odio Industries"},
            {"id": "16750818 3410", "name": "Cecilia Wilcox", "phone": "1-197-975-5400", "email": "magna.malesuada.vel@Vivamusmolestie.net", "city": "Cherain", "company": "Tellus Justo Corporation"},
            {"id": "16590322 1942", "name": "Lacy Graves", "phone": "920-7413", "email": "orci.Ut.sagittis@Quisque.ca", "city": "Sint-Pieters-Kapelle", "company": "Ullamcorper Viverra Maecenas Limited"},
            {"id": "16560214 2605", "name": "Aubrey Hopper", "phone": "1-653-254-3023", "email": "urna@magnis.co.uk", "city": "Bristol", "company": "Mollis LLC"},
            {"id": "16010628 2460", "name": "Emi Conley", "phone": "1-942-747-5400", "email": "neque@CuraePhasellusornare.ca", "city": "Werder", "company": "Natoque Penatibus Incorporated"},
            {"id": "16480629 8735", "name": "Tanisha Leonard", "phone": "509-3851", "email": "Nunc@gravidamauris.ca", "city": "Alert Bay", "company": "Semper Corporation"},
            {"id": "16701024 2027", "name": "Charissa Willis", "phone": "621-7633", "email": "id.erat@interdumCurabiturdictum.ca", "city": "Ophain-Bois-Seigneur-Isaac", "company": "Convallis Ante Lectus LLP"},
            {"id": "16041223 3678", "name": "Gillian Gibson", "phone": "232-2284", "email": "ornare.lectus@urnanecluctus.ca", "city": "Malang", "company": "Magna Corp."},
            {"id": "16461105 2384", "name": "Kyra Hale", "phone": "174-1732", "email": "a.purus@vehicula.co.uk", "city": "Lugnano in Teverina", "company": "Ornare Placerat Orci Foundation"},
    ]
    },
    argTypes: {
        data: {
            control: {
                disable: true
            }
        },
        tabsArray: {
            control: {
                disable: true
            }
        },
        actionsArray: {
            control: {
                disable: true
            }
        },
        withTabs: {
            control: {
                disable: true
            }
        },
        withActions: {
            control: {
                disable: true
            }
        },
        withActionbar: {
            control: {
                disable: true
            }
        },
        withPagination: {
            control: {
                disable: true
            }
        },
        withLoader: {
            control: {
                disable: true
            }
        },
        TextInputProps: {
            control: {
                disable: true
            }
        },
        filterKeys: {
            control: {
                disable: true
            }
        }
    }
};

const HeaderLayout = () => {
	const styles = {
        cell: {
            fontWeight: 'bold',
            fontSize: '1rem'
        }
    }
	return (
		<TableHead>
			<TableRow>
				<TableCell style={styles.cell}>
					ID
				</TableCell>
				<TableCell align='center' style={styles.cell}>
                    Name
				</TableCell>
				<TableCell align='center' style={styles.cell}>
                    Phone
				</TableCell>
				<TableCell align='center' style={styles.cell}>
                    Email
				</TableCell>
				<TableCell align='center' style={styles.cell}>
                    City
				</TableCell>
				<TableCell align='center' style={styles.cell}>
                    Company
				</TableCell>
			</TableRow>
		</TableHead>
	);
}

const RowLayout = ({row}) => {
	
	return (
        <TableRow>
            <TableCell>
                {row.id}
            </TableCell>
            <TableCell align='center'>
                {row.name}
            </TableCell>
            <TableCell align='center'>
                {row.phone}
            </TableCell>
            <TableCell align='center'>
                {row.email}
            </TableCell>
            <TableCell align='center'>
                {row.city}
            </TableCell>
            <TableCell align='center'>
                {row.company}
            </TableCell>
        </TableRow>
	);
}


const Template = args => <Table {...args} />

export const Simple = Template.bind({});
Simple.args = {
    Head: HeaderLayout,
    withActionbar: false,
    Row: RowLayout,
    withPagination: false
}

export const WithTabs = Template.bind({});
WithTabs.args = {
    Head: HeaderLayout,
    Row: RowLayout,
    withTabs: true,
    tabsArray: ['Tab 1', 'Tab 2', 'Tab 3'],
    withActionbar: false,
    withPagination: false
}

export const WithActionBar = Template.bind({});
WithActionBar.args = {
    Head: HeaderLayout,
    Row: RowLayout,
    withActionbar: true,
    withPagination: false
}

export const WithActions = Template.bind({});
WithActions.args = {
    Head: HeaderLayout,
    Row: RowLayout,
    withActions: true,
    actionsArray: [
        {
            label: 'Action One',
            action: action('Action One pressed'),
            variant: 'outlined',
            startIcon: <CloudDownloadOutlinedIcon />
        },
        {
            label: 'Action Two',
            action: action('Action Two pressed')
        }
    ],
    withPagination: false
}

export const WithPagination = Template.bind({});
WithPagination.args = {
    Head: HeaderLayout,
    Row: RowLayout,
    withActionbar: false
}

export const Complete = Template.bind({});
Complete.args = {
    Head: HeaderLayout,
    Row: RowLayout,
    actionsArray: [
        {
            label: 'Action One',
            action: action('Action One pressed'),
            variant: 'outlined',
            startIcon: <CloudDownloadOutlinedIcon />
        },
        {
            label: 'Action Two',
            action: action('Action Two pressed')
        }
    ],
    withActions: true,
    withTabs: true,
    tabsArray: ['Tab 1', 'Tab 2', 'Tab 3'],
    filterKeys: ['id', 'name', 'phone', 'email', 'city', 'company']
}

export const Empty = args => <div style={{height: '50vh'}}><Table {...args} /></div>
Empty.args = {
    data: [],
    Head: HeaderLayout,
    Row: RowLayout,
    withLoader: false
}

