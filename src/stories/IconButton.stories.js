import React from 'react';
import { action } from '@storybook/addon-actions';
import { IconButton } from '../components';
import EditIcon from '@material-ui/icons/Edit';

export default {
    title: 'Atoms/IconButton',
    component: IconButton,
    args: {
        children: <EditIcon />,
        onClick: action('Botón de icono presionado')
    },
    argTypes: {
        children: {
            control: {
                disable: true
            }
        }
    },
};

const Template = args => <IconButton {...args} />;

export const Default = Template.bind({});

export const Disabled = Template.bind({});
Disabled.args = {
    disabled: true
}

export const NotFilled = Template.bind({});
NotFilled.args = {
    filled: false
}

