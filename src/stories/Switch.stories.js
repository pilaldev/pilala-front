import React from 'react';
import { Switch } from '../components';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Atoms/Switch',
    component: Switch,
    args: {
        onChange: action('Switch cambiado'),
    },
};

const Template = args => <Switch {...args} />;

export const Default = Template.bind({});
export const Checked = Template.bind({});
Checked.args = {
    checked: true
}
export const InactiveDisabled = Template.bind({});
InactiveDisabled.args = {
    disabled: true
}
export const ActiveDisabled = Template.bind({});
ActiveDisabled.args = {
    disabled: true,
    checked: true
}

