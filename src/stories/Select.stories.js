import React from 'react';
import { action } from '@storybook/addon-actions';
import { Select } from '../components';

const options = [
    {
        label: 'Opción 1',
        value: 0
    },
    {
        label: 'Opción 2',
        value: 0
    },
    {
        label: 'Opción 3',
        value: 0
    },
    {
        label: 'Opción 4',
        value: 0
    },
    {
        label: 'Opción 5',
        value: 0
    }
]

export default {
    title: 'Atoms/Select',
    component: Select,
    args: {
        onChange: action('Seleccionando en el select'),
        options: options
    },
    argTypes: {
        value: {
            control: {
                disable: true
            },
        }
    },
};

const Template = args => <Select {...args} />;

export const Default = Template.bind({});
Default.args = {
    value: options[1]
}

export const Disabled = Template.bind({});
Disabled.args = {
    disabled: true,
    value: options[2]
}

export const Complete = Template.bind({});
Complete.args = {
    label: 'Tipo de documento',
    helperText: 'Selecciona el tipo de documento'
}