import React from 'react';
import { Checkbox } from '../components';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Atoms/Checkbox',
    component: Checkbox,
    args: {
        onChange: action('Checkbox cambiado'),
    },
};

const Template = args => <Checkbox {...args} />;

export const Default = Template.bind({});
export const Checked = Template.bind({});
Checked.args = {
    checked: true
}
export const InactiveDisabled = Template.bind({});
InactiveDisabled.args = {
    disabled: true
}
export const ActiveDisabled = Template.bind({});
ActiveDisabled.args = {
    disabled: true,
    checked: true
}
export const WithLabel = Template.bind({});
WithLabel.args = {
    label: 'Seleccionar opción'
}


