import React from 'react';
import { action } from '@storybook/addon-actions';
import { DropdownInput } from '../components';

export default {
    title: 'Atoms/DropdownInput',
    component: DropdownInput,
    args: {
        onChange: action('Seleccionando en el dropdown'),
        options: ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5']
    },
    argTypes: {
        value: {
            control: {
                disable: true
            },
        },
        placeholder: {
            control: {
                disable: true
            },
        }
    },
};

const Template = args => <DropdownInput {...args} />;

export const Default = Template.bind({});
Default.args = {
    placeholder: 'Seleccionar ciudad'
}

export const Disabled = Template.bind({});
Disabled.args = {
    disabled: true,
    placeholder: 'Buscar país'
}

export const Complete = Template.bind({});
Complete.args = {
    label: 'Ciudad',
    helperText: 'Selecciona el nombre de la ciudad'
}