import React, { useState } from 'react';
import { Alert as PilalaAlert, Button } from '../components';
import { action } from '@storybook/addon-actions';

const Alert = (props) => {
    const [open, setOpen] = useState(false);

    const onCancel = () => {
        setOpen(false);
    }

    const onAccept = () => {
        action('Acción aceptada correctamente');
        onCancel();
    }

    return (
        <>
            <Button onClick={() => setOpen(!open)}>
                Abrir Alert
            </Button>
            <PilalaAlert
                open={open}
                onCancel={onCancel}
                onAccept={onAccept}
                title={props.title}
                message={props.message}
            />
        </>
    )
}

export default {
    title: 'Components/Alert',
    component: Alert,
    argTypes: {
        title: {
            name: 'title',
            type: { name: 'string', required: true },
            defaultValue: 'Titulo de la alerta',
            description: 'Titulo mostrado en la parte superior de la alerta'
        },
        message: {
            name: 'message',
            type: { name: 'string', required: true },
            defaultValue: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            description: 'Mensaje mostrado en la alerta'
        },
        onCancel: {
            action: 'Cancelado',
            description: 'Acción para cancelar la alerta',
            control: false,
            type: {
                required: true
            }
        },
        onAccept: {
            action: 'Aceptado',
            description: 'Acción para aceptar la alerta',
            control: false,
            type: {
                required: true
            }
        },
        open: {
            name: 'open',
            type: { name: 'boolean', required: true },
            defaultValue: false,
            description: 'Mostrar u ocultar la alerta',
            control: false
        },
    }
};

export const Default = args => <Alert {...args} />