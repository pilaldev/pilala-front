import React from 'react';
import { RadioButton } from '../components';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Atoms/RadioButton',
    component: RadioButton,
    args: {
        onChange: action('RadioButton cambiado'),
    },
};

const Template = args => <RadioButton {...args} />;

export const Default = Template.bind({});
export const Checked = Template.bind({});
Checked.args = {
    checked: true
}
export const InactiveDisabled = Template.bind({});
InactiveDisabled.args = {
    disabled: true
}
export const ActiveDisabled = Template.bind({});
ActiveDisabled.args = {
    disabled: true,
    checked: true
}
export const WithLabel = Template.bind({});
WithLabel.args = {
    label: 'Seleccionar opción'
}