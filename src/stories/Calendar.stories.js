import React from 'react';
import { Paper } from '@material-ui/core';
import { Calendar } from '../components';
import { action } from '@storybook/addon-actions';

const styles = {
    width: 320,
    height: '100%',
    borderRadius: 12,
    boxShadow: '0px 0px 32px 8px rgba(0, 0, 0, 0.08)',
    overflow: 'hidden'
}

const Container = ({children}) => (
    <Paper style={styles}>
        {children}
    </Paper>
);


export default {
    title: 'Components/Calendar',
    component: Calendar,
    args: {
        open: true,
        variant: 'static',
        anchorElement: {},
        onChange: action('Fecha seleccionada'),
        focusDate: '26/10/2020'
    },
    argTypes: {
        color: {
            control: {
                type: 'select',
                options: ['primary', 'secondary', 'info', 'success', 'warning', 'error'],
                default: 'primary'
            },
        },
        anchorElement: {
            control: {
                disable: true
            }
        },
        focusDate: {
            control: {
                disable: true
            }
        },
        formatFocusDate: {
            control: {
                disable: true
            }
        },
        variant: {
            control: {
                disable: true
            }
        },
        open: {
            control: {
                disable: true
            }
        },
    },
};

export const Default = args => (
    <Container>
        <Calendar {...args}  />
    </Container>
);
