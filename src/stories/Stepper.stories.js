import React from 'react';
import { Stepper } from '../components';

export default {
    title: 'Components/Stepper',
    component: Stepper,
    argTypes: {
        steps: {
            control: {
                type: 'number',
                min: 2,
                step: 1
            },
        },
        activeStep: {
            control: {
                type: 'number',
                min: 1,
                step: 1
            },
        }
    },
};

const Template = args => <Stepper {...args} />;

export const Default = Template.bind({});

export const Complete = Template.bind({});
Complete.args = {
    steps: 3,
    activeStep: 4
}