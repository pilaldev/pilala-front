import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from '../components';
import FaceIcon from '@material-ui/icons/Face';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default {
    title: 'Atoms/Button',
    component: Button,
    args: {
        children: 'Button',
        onClick: action('Botón presionado')
    },
    argTypes: {
        startIcon: {
            control: {
                disable: true
            },
        },
        endIcon: {
            control: {
                disable: true
            },
        }
    },
};

const Template = args => <Button {...args} />;

export const Default = Template.bind({});

export const WithStartIcon = Template.bind({});
WithStartIcon.args = {
    startIcon: <FaceIcon />
}

export const WithEndIcon = Template.bind({});
WithEndIcon.args = {
    endIcon: <ExpandMoreIcon />
}

export const Disabled = Template.bind({});
Disabled.args = {
    disabled: true
}

export const Loading = Template.bind({});
Loading.args = {
    loading: true
}