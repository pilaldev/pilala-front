import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PLANILLA_DETAIL_OPTIONS } from './Constants';
import BugReportTwoToneIcon from '@material-ui/icons/BugReportTwoTone';
import DescriptionTwoToneIcon from '@material-ui/icons/DescriptionTwoTone';
import PictureAsPdfTwoToneIcon from '@material-ui/icons/PictureAsPdfTwoTone';
import SendTwoToneIcon from '@material-ui/icons/SendTwoTone';
import PlanillaDetailLayout from './PlanillaDetailLayout';

export class PlanillaDetail extends Component {
	render() {

		const receiptOptions = [
			{
				label: PLANILLA_DETAIL_OPTIONS[1],
				icon: <BugReportTwoToneIcon style={{ fontSize: '1.5em', color: '#263238', marginRight: 5 }} />,
				action: () => {}
			},
			{
				label: PLANILLA_DETAIL_OPTIONS[2],
				icon: <DescriptionTwoToneIcon style={{ fontSize: '1.5em', color: '#263238', marginRight: 5 }} />,
				action: () => {}
			},
			{
				label: PLANILLA_DETAIL_OPTIONS[3],
				icon: <PictureAsPdfTwoToneIcon style={{ fontSize: '1.5em', color: '#263238', marginRight: 5 }} />,
				action: () => {}
			},
			{
				label: PLANILLA_DETAIL_OPTIONS[4],
				icon: <PictureAsPdfTwoToneIcon style={{ fontSize: '1.5em', color: '#263238', marginRight: 5 }} />,
				action: () => {}
			},
			{
				label: PLANILLA_DETAIL_OPTIONS[5],
				icon: <PictureAsPdfTwoToneIcon style={{ fontSize: '1.5em', color: '#263238', marginRight: 5 }} />,
				action: () => {}
			},
			{
				label: PLANILLA_DETAIL_OPTIONS[6],
				icon: <SendTwoToneIcon style={{ fontSize: '1.5em', color: '#263238', marginRight: 5 }} />,
				action: () => {}
			}
		];

		const layoutProps = {
			receiptOptions
		}

		return <PlanillaDetailLayout {...layoutProps} />;
	}
}

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = (dispatch) => {
	return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(PlanillaDetail);
