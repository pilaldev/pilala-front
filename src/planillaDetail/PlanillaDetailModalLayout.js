import React from 'react';
import { useSelector } from 'react-redux';
import { Dialog, Typography, DialogContent, Divider, Button, CircularProgress } from '@material-ui/core';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { PLANILLA_DETAIL_MODAL } from './Constants';
import PlanillaDetailModalTable from './PlanillaDetailModalTable';

const PlanillaDetailModalLayout = (props) => {
	const classes = useStyles();

	const modalStatus = useSelector((state) => state.planillaDetail.openedModal);

	return (
		<ThemeProvider theme={theme}>
			<Dialog
				open={modalStatus}
				maxWidth="sm"
				PaperProps={{
					className: classes.paperContainer
				}}
			>
				<DialogContent className={classes.dialogContent}>
					<Typography className={classes.dialogTitle}>{PLANILLA_DETAIL_MODAL.TITLE}</Typography>

					<Divider className={classes.divider} />
					<div className={classes.novedadContainer}>
						<div className={classes.novedadContent}>
							<PlanillaDetailModalTable />
						</div>
						<div className={classes.buttonsContainer}>
							<Button
								className={classes.cancelButton}
								onClick={props.cancelAction}
								disabled={props.disableSendCoreccionButton}
							>
								<Typography variant="subtitle2" className={classes.cancelButtonText}>
									{PLANILLA_DETAIL_MODAL.CANCEL_BUTTON}
								</Typography>
							</Button>
							<Button
								variant="contained"
								className={classes.saveButton}
								onClick={props.saveAction}
								disabled={props.disableSendCoreccionButton}
							>
								{props.disableSendCoreccionButton ? (
									<CircularProgress size={20} style={{ color: 'black' }} />
								) : (
									<Typography variant="subtitle2" className={classes.saveButtonText}>
										{PLANILLA_DETAIL_MODAL.SAVE_BUTTON}
									</Typography>
								)}
							</Button>
						</div>
					</div>
				</DialogContent>
			</Dialog>
		</ThemeProvider>
	);
};

const theme = createMuiTheme({
	overrides: {
		MuiPickersToolbar: {
			toolbar: {
				backgroundColor: 'rgba(94, 53, 177, 0.25)'
			}
		},
		MuiPickersToolbarText: {
			toolbarBtnSelected: {
				color: '#5E35B1'
			},
			toolbarTxt: {
				color: '#5E35B1'
			}
		},
		MuiPickersDay: {
			daySelected: {
				backgroundColor: '#5E35B1'
			}
		},
		MuiOutlinedInput: {
			root: {
				'& fieldset': {
					borderColor: 'purple'
				},
				'&:hover fieldset': {
					borderColor: 'purple'
				},
				'&.Mui-focused fieldset': {
					borderColor: 'purple'
				},
				'&:hover:not($disabled):not($focused):not($error) $notchedOutline': {
					borderColor: 'purple'
				}
			}
		}
	},

	palette: {
		primary: {
			main: '#7B1FA2'
		},
		secondary: {
			main: '#f44336'
		}
	}
});

const useStyles = makeStyles({
	paperContainer: {
		backgroundColor: '#FFFFFF',
		display: 'flex',
		flex: 1
	},
	contentContainer: {
		display: 'flex',
		flexDirection: 'column'
	},

	saveButton: {
		background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
		borderRadius: 22,
		marginLeft: 10,
		textTransform: 'none'
	},
	saveButtonText: {
		color: 'white'
	},
	cancelButtonText: {
		color: '#263238',
		transform: 'no'
	},
	cancelButton: {
		marginRight: 10,
		borderRadius: 22,
		textTransform: 'none'
	},

	dialogContent: {
		paddingRight: 0,
		paddingLeft: 0
	},
	dialogTitle: {
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: '2em',
		color: '#5E35B1'
	},

	dividerSymbol: {
		fontSize: '2em',
		margin: '0px 5px 0px 5px',
		color: '#37474F'
	},

	divider: {
		margin: '15px 0px 15px 0px',
		backgroundColor: '#707070'
	},
	novedadContainer: {
		width: '100%',
		display: 'flex',
		padding: '8% 5% 0% 5%',
		flexDirection: 'column'
	},

	novedadContent: {
		backgroundColor: '#F7F7F7',
		width: '100%',

		display: 'flex',
		flexDirection: 'column',
		padding: '5% 0px 5% 0px'
	},
	buttonsContainer: {
		display: 'flex',
		alignItems: 'center',
		padding: '10% 0px 5% 0px',
		justifyContent: 'flex-end'
	}
});

export default PlanillaDetailModalLayout;
