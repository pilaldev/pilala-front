export const PLANILLA_DETAIL_MODAL = {
	TITLE: 'Seleccionar cotizantes para correción',
	CANCEL_BUTTON: 'Cancelar',
	SAVE_BUTTON: 'Guardar'
};
export const PLANILLA_DETAIL_OPTIONS = {
	1: 'Comprobante de pago planilla',
	2: 'Descargar planilla',
	3: 'Comprobante detallado administradoras',
	4: 'Liquidación intereses por mora',
	5: 'Informe de empleados',
	6: 'Enviar comprobante a los cotizantes'
};
