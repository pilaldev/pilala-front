import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import {
  planillaDetailAction,
  prePlanillaAction,
  currentUserAction,
  payPilaAction,
  snackBarAction,
} from './../redux/Actions';
import {
  PLANILLA_DETAIL,
  /*PRE_PLANILLA*/ CURRENT_USER,
  PAY_PILA,
  SNACKBAR,
} from './../redux/ActionTypes';
import Endpoints from '@bit/pilala.pilalalib.endpoints';

import PlanillaDetailModalLayout from './PlanillaDetailModalLayout';
class PlanillaDetailModal extends Component {
  state = {
    disableSendCoreccionButton: false,
  };

  saveAction = () => {
    if (this.state.disableSendCoreccionButton) return;

    this.setState({
      disableSendCoreccionButton: true,
    });

    if (
      this.props.planillaDetail.selectedCotizantes &&
      this.props.planillaDetail.selectedCotizantes.length
    ) {
      let correctionData = {
        idAportante: this.props.activeCompany,
        tipoCotizante: this.props.planillaDetail.tipoPlanilla,
        arrayUsers: this.props.planillaDetail.selectedCotizantes.map(
          (cotizante) => {
            return cotizante.id;
          }
        ),
        idPlanilla: this.props.planillaDetail.numeroPlanilla.toString(),
      };

      fetch(Endpoints.sendCorrectionData, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(correctionData),
      })
        .then((response) =>
          response.json().then((data) => ({
            data: data,
            status: response.status,
          }))
        )
        .then((result) => {
          if (result.status === 200 && result.data) {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Corrección exitosa',
              description: 'La información ha sido corregida correctamente',
              color: 'success',
            });

            this.props.planillaDetailAction(
              PLANILLA_DETAIL.PLANILLA_DETAIL_SET_MODAL_STATUS,
              false
            );
            this.props.currentUserAction(
              CURRENT_USER.CURRENT_USER_UPDATE_CORRECIONES_INFO,
              {
                activeCompany: this.props.activeCompany,
                periodo: {
                  display: this.props.planillaDetail.periodoPlanilla,
                  month: this.props.planillaDetail.numeroPlanilla.toString(),
                  value: this.props.planillaDetail.periodoPlanilla,
                  tipoCotizante: this.props.planillaDetail.tipoPlanilla,
                },
              }
            );
            const currentPlanilla = `correccion ${this.props.planillaDetail.idPlanilla}`;
            this.props.payPilaAction(
              PAY_PILA.PAY_PILA_SET_CURRENT_PLANILLA,
              currentPlanilla
            );
            this.props.currentUserAction(
              CURRENT_USER.CURRENT_USER_UPDATE_ACTIVE_CORRECIONES_FLAG,
              true
            );
            this.props.history.push(
              `/${this.props.match.params.userId}/console/liquidarPila/1`
            );
          } else {
            this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
              title: 'Error al corregir planilla',
              description:
                'Tenemos problemas para realizar la corrección, inténtelo nuevamente',
              color: 'error',
            });
          }

          this.setState({
            disableSendCoreccionButton: false,
          });
        });
    } else {
      this.props.snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
        title: 'Información requerida',
        description: 'Por favor seleccione los cotiantes que desea corregir',
        color: 'warning',
      });
    }

    // let correccionStructure = {
    // 	[this.props.activeCompany]: {
    // 		[this.props.planillaDetail.periodoPlanilla]: {
    // 			[this.props.planillaDetail.tipoPlanilla]: {
    // 				[this.props.planillaDetail.idPlanilla]: {}
    // 			}
    // 		}
    // 	}
    // };

    // this.props.planillaDetail.selectedCotizantes.forEach((cotizante) => {
    // 	// console.log('cotizantes', cotizante);
    // 	correccionStructure[this.props.activeCompany][this.props.planillaDetail.periodoPlanilla][
    // 		this.props.planillaDetail.tipoPlanilla
    // 	][this.props.planillaDetail.idPlanilla][cotizante.documentNumber] = { ...cotizante };
    // });

    // this.props.prePlanillaAction(PRE_PLANILLA.PRE_PLANILLA_SET_REQUESTED_DATA, correccionStructure);

    // // let correccionesInfo = {
    // // 	[this.props.activeCompany]: {
    // // 		display: this.props.planillaDetail.periodoPlanilla,
    // // 		month: this.props.planillaDetail.idPlanilla,
    // // 		value: this.props.planillaDetail.periodoPlanilla
    // // 	}
    // // };

    // this.props.currentUserAction(CURRENT_USER.CURRENT_USER_UPDATE_CORRECIONES_INFO, {
    // 	activeCompany: this.props.activeCompany,
    // 	periodo: {
    // 		display: this.props.planillaDetail.periodoPlanilla,
    // 		month: this.props.planillaDetail.idPlanilla,
    // 		value: this.props.planillaDetail.periodoPlanilla
    // 	}
    // });
  };

  cancelAction = () => {
    this.props.planillaDetailAction(
      PLANILLA_DETAIL.PLANILLA_DETAIL_SET_MODAL_STATUS,
      false
    );
  };

  render() {
    const layoutProps = {
      saveAction: this.saveAction,
      cancelAction: this.cancelAction,
      disableSendCoreccionButton: this.state.disableSendCoreccionButton,
    };

    return <PlanillaDetailModalLayout {...layoutProps} />;
  }
}

const mapStateToProps = (state) => {
  return {
    planillaDetail: state.planillaDetail,
    activeCompany: state.currentUser.activeCompany,
    periodosInfo: state.currentUser.periodosInfo,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    planillaDetailAction: (actionType, value) =>
      dispatch(planillaDetailAction(actionType, value)),
    prePlanillaAction: (actionType, value) =>
      dispatch(prePlanillaAction(actionType, value)),
    currentUserAction: (actionType, value) =>
      dispatch(currentUserAction(actionType, value)),
    payPilaAction: (actionType, value) =>
      dispatch(payPilaAction(actionType, value)),
    snackBarAction: (actionType, value) =>
      dispatch(snackBarAction(actionType, value)),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PlanillaDetailModal)
);
