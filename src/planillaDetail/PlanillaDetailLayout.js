import React from 'react';
import { Typography, Card, ButtonBase } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import QueryBuilderTwoToneIcon from '@material-ui/icons/QueryBuilderTwoTone';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import MonetizationOnTwoToneIcon from '@material-ui/icons/MonetizationOnTwoTone';

import PlanillaDetailTable from './PlanillaDetailTable';
import PlanillaDetailModal from './PlanillaDetailModal';

const PlanillaDetailLayout = (props) => {
	const classes = useStyles();

	const modalStatus = useSelector((state) => state.planillaDetail.openedModal);

	return (
		<div  className={classes.container}>
			<div className={classes.headerContainer}>
				<Typography className={classes.headerTitle}>Historial de planillas</Typography>
				<div className={classes.infoContainer}>
					<div className={classes.cardContainer}>
						<div className={classes.cardTitleContainer}>
							<Typography className={classes.cardTitle}>Periodo</Typography>
							<Typography className={classes.cardTitle}>Pagado el</Typography>
						</div>

						<div className={classes.cardSubcontainer}>
							<QueryBuilderTwoToneIcon className={classes.cardIcons} />
							<Typography className={classes.cardSubtitle}>Mayo 2020</Typography>
							<ArrowForwardRoundedIcon className={classes.cardIcons} />
							<MonetizationOnTwoToneIcon className={classes.cardIcons} />
							<Typography className={classes.cardSubtitle}>03/05/2020</Typography>
						</div>
					</div>
				</div>
			</div>
			<div className={classes.planillaTypeContainer} />

			<div className={classes.buttonsContainer}>
				<Card className={classes.buttonsCard}>
					{
						props.receiptOptions.map((item, index) => {
							return (
								<div className={classes.optionContainer}>
									<ButtonBase
										key={index}
										className={classes.receiptOption}
										onClick={item.action}
										disableRipple
									>
										{item.icon}
										<Typography className={classes.receiptOptionText}>
											{item.label}
										</Typography>
									</ButtonBase>
								</div>
							);
						})
					}
				</Card>
			</div>
			<div className={classes.tableContainer}>
				<PlanillaDetailTable />
			</div>
			{modalStatus && <PlanillaDetailModal />}
		</div>
	);
};

const useStyles = makeStyles({
	container: {
		display: 'flex',
		width: '100%',
		height: '100%',
		flexDirection: 'column',
		padding: '3% 3% 3% 3%',
		overflow: 'auto'

	},
	headerTitle: {
		fontSize: '1.8em',
        fontWeight: 'bold',
		color: '#263238',
		marginBottom: '1.6%'
	},
	tableContainer: {
		display: 'flex',
		flex: 1,
		width: '100%',
		height: '100%',
		flexDirection: 'column',
		justifyContent: 'flex-end',
		padding: '2% 2% 0px 2%'
	},
	headerContainer: {
		display: 'flex',
		width: '100%',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	infoContainer: {
		display: 'flex',
		height: '100%',
		alignItems: 'center'
	},
	cardContainer: {
		display: 'flex',
		flexDirection: 'column',
		border: '1px solid #757AFF',
		backgroundColor: '#757AFF29',
		padding: '10px',
		borderRadius: 7
	},
	cardTitle: {
		color: '#6060B2',
		fontSize: '1em',
		fontWeight: 'bold',
		textAlign: 'left',
		width: '50%',
		paddingLeft: 5
	},
	cardTitleContainer: {
		width: '100%',
		display: 'flex',
		justifyContent: 'space-around'
	},
	cardSubcontainer: {
		display: 'flex',
		width: '100%',
		alignItems: 'center'
	},
	cardIcons: {
		color: '#757AFF',
		fontSize: '1.5em'
	},
	cardSubtitle: {
		color: 'black',
		margin: '0px 5px 0px 5px',
		fontSize: '1em',
		fontWeight: 'bold'
	},
	buttonsContainer: {
		display: 'flex', 
		width: '100%',
		height: 150,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonsCard: {
		display: 'flex',
		width: '65%',
		alignItems: 'center',
		flexWrap: 'wrap',
		padding: '2%'
	},
	receiptOption: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start'
	},
	receiptOptionText: {
        textAlign: 'left',
        fontSize: '1.1em',
        color: '#263238',
        fontWeight: 'bold'
	},
	optionContainer: {
		display: 'flex',
		width: '33.33%',
		marginTop: 5,
		marginBottom: 5
	}
});

export default PlanillaDetailLayout;
