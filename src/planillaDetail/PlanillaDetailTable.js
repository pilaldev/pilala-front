import React, { Component } from 'react';
import { connect, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import { makeStyles, createMuiTheme, withStyles } from '@material-ui/core/styles';
import { Typography, ButtonBase, ThemeProvider, Chip } from '@material-ui/core';

import { COTIZANTES_DETAIL, PLANILLA_DETAIL } from '../redux/ActionTypes';

import MaterialTable, { MTableToolbar } from 'material-table';
import { cotizantesDetailAction, planillaDetailAction } from '../redux/Actions';

import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';

class PlanillaDetailTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			cotizantes: [],
			columns: [
				{
					title: 'Empleado',
					field: 'name.display',
					sorting: false,
					render: (rowData) => <UserInfoComponent {...rowData} goToDetail={this.goToDetail} />,
					customFilterAndSearch: (filter, rowData) => {
						let text = filter.toLowerCase();
						let tempName = rowData.name.display.toLowerCase();

						if (tempName.indexOf(text) > -1 || rowData.documentNumber.indexOf(text) > -1) {
							return true;
						} else {
							return false;
						}
					},
					cellStyle: {
						// backgroundColor: '#039be5',
						// color: '#FFF'
					},
					headerStyle: {
						textAlign: 'left',
						fontWeight: 'bold',
						color: 'grey'
					}
				},

				{
					title: 'Novedades',
					field: 'total',
					render: (rowData) => <NoveltiesContainer {...rowData} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'Salud',
					field: 'eps.display',
					render: (rowData) => <TextColumn text={rowData.eps.display} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'ARL',
					field: 'arl.display',
					render: (rowData) => <TextColumn text={rowData.arl.display} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'Pensión',
					field: 'pension.display',
					render: (rowData) => <TextColumn text={rowData.pension.display} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'CCF',
					field: 'cajaCompensacion.display',
					render: (rowData) => <TextColumn text={rowData.cajaCompensacion.display} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'SENA',
					field: 'total',
					render: (rowData) => <IconColumn checked={rowData.subsistemas.sena.value > 0} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'ICBF',
					field: 'total',
					render: (rowData) => <IconColumn checked={rowData.subsistemas.icbf.value > 0} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'FSP',
					field: 'total',
					render: (rowData) => <IconColumn {...rowData} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'MEN',
					field: 'total',
					render: (rowData) => <IconColumn {...rowData} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				},
				{
					title: 'ESAP',
					field: 'total',
					render: (rowData) => <IconColumn {...rowData} />,
					headerStyle: {
						textAlign: 'center',
						fontWeight: 'bold',
						color: 'grey'
					}
				}
			]
		};
	}

	componentDidMount() {
		this.mapCotizantes();
	}

	mapCotizantes = () => {
		let numeroPlanilla = this.props.match.params.planillaID;

		if (this.props.planillasHistory.planillas.length && numeroPlanilla) {
			let mapedData = [];
			let planilla = this.props.planillasHistory.planillas.find(
				(planilla) => planilla.numeroPlanilla.toString() === numeroPlanilla
			);

			if (planilla && planilla.cotizantes) {
				planilla.cotizantes.forEach((cotizanteData) => {
					if (cotizanteData.activeInCompany.value) {
						mapedData.push({
							...cotizanteData
						});
					}
				});

				this.setState({
					cotizantes: mapedData
				});

				this.props.planillaDetailAction(PLANILLA_DETAIL.PLANILLA_DETAIL_SET_DATA, {
					cotizantes: mapedData,
					tipoPlanilla: planilla.tipoCotizante,
					idPlanilla: planilla.periodo.month,
					periodoPlanilla: planilla.periodo.value
				});
			}
		}
	};

	goToDetail = (id, type, documentNumber) => {
		this.props.cotizantesDetailAction(COTIZANTES_DETAIL.COTIZANTES_DETAIL_SET_DATA, { id, type, documentNumber });
		this.props.history.push(`/${this.props.match.params.userId}/console/cotizantes/${id}`);
	};

	openCorrecionModal = () => {
		this.props.planillaDetailAction(PLANILLA_DETAIL.PLANILLA_DETAIL_SET_MODAL_STATUS, true);
	};

	render() {
		return (
			<ThemeProvider theme={theme}>
				<MaterialTable
					components={{
						Toolbar: (props) => (
							<div style={{ display: 'flex' }}>
								<StyledMTableToolbar {...props} selectedRows={[]} />
							</div>
						)
					}}
					title={<TitleComponent openCorrecionModal={this.openCorrecionModal} />}
					columns={this.state.columns}
					data={this.state.cotizantes}
					options={{
						sorting: false,
						selection: false,
						draggable: false
					}}
				/>
			</ThemeProvider>
		);
	}
}

const StyledMTableToolbar = withStyles({
	root: {
		display: 'flex',
		width: '100%'
	},
	title: {
		// backgroundColor:'orange',
		display: 'flex',
		width: '75%',
		justifyContent: 'space-between'
	},
	spacer: {
		display: 'none'
	},
	searchField: {
		display: 'flex',
		width: '25%'
	},
	highlight: {
		backgroundColor: '#FFF',
		color: 'black'
	}
})(MTableToolbar);

const TitleComponent = (props) => {
	const classes = useStyles();
	const featureFlags = useSelector(state => state.currentUser.featureFlags);

	return (
		<div
			style={{
				display: 'flex',
				width: '100%',
				height: '100%',
				justifyContent: 'space-between'
			}}
		>
			COTIZANTES
			{
				featureFlags.ccPayrollCorrection &&
				<div className={classes.corregirPLanillaButtonContainer} onClick={props.openCorrecionModal}>
					<ButtonBase className={classes.corregirPLanillaButton} disableRipple>
						<Typography
							style={{
								color: '#7B1FA2',
								fontSize: '1.1em',
								fontWeight: 'bold'
							}}
						>
							Corregir Planilla
						</Typography>
					</ButtonBase>
				</div>
			}
		</div>
	);
};

const UserInfoComponent = (props) => {
	const classes = useStyles();
	// console.log('props', props.index);
	return (
		<div
			className={classes.userInfoContainer}
			onClick={() => props.goToDetail(props.id, props.tipoCotizante, props.documentNumber)}
		>
			{/* <img
				alt="user"
				className={classes.userInfoImage}
				src={
					props.genre.value ? (
						require(`../../assets/img/women_one.svg`)
					) : (
						require(`../../assets/img/men_one.svg`)
					)
				}
			/> */}
			<div className={classes.userInfoTextContainer}>
				<Typography className={classes.userInfoTextBox} noWrap style={{ fontWeight: 'bold' }}>
					{props.name.display}
				</Typography>
				<Typography className={classes.userInfoTextBox} noWrap>
					{props.documentNumber}
				</Typography>
			</div>
		</div>
	);
};

const TextColumn = (props) => {
	const classes = useStyles();

	return (
		<div className={classes.textColumn}>
			<Typography className={classes.textColumnText}>{props.text ? props.text.split(' ')[0] : ''}</Typography>
		</div>
	);
};

const IconColumn = (props) => {
	const classes = useStyles();

	return <div className={classes.textColumn}>{props.checked ? <CheckIcon /> : <CloseIcon />}</div>;
};

const NoveltiesContainer = (props) => {
	const classes = useStyles();

	let vacacionesDurationDays = 0;
	let incapacidadesDurationDays = 0;
	let suspensionesDurationDays = 0;
	let licenciasDurationDays = 0;

	props.licencias.forEach((item) => {
		licenciasDurationDays += item.duracionDias;
	});

	props.vacaciones.forEach((item) => {
		vacacionesDurationDays += item.duracionDias;
	});

	props.suspensiones.forEach((item) => {
		suspensionesDurationDays += item.duracionDias;
	});

	props.incapacidades.forEach((item) => {
		incapacidadesDurationDays += item.duracionDias;
	});

	return (
		<div className={classes.noveltiesContainer}>
			<div className={classes.countersRow}>
				{licenciasDurationDays > 0 && (
					<NovedadContador
						novedadText={'L'}
						durationDays={licenciasDurationDays}
						backgroundColor={'rgba(94, 53, 177, 0.16)'}
						typographyColor={'#5E35B1'}
					/>
				)}

				{vacacionesDurationDays > 0 && (
					<NovedadContador
						novedadText={'V'}
						durationDays={vacacionesDurationDays}
						backgroundColor={'rgba(94, 53, 177, 0.16)'}
						typographyColor={'#5E35B1'}
						onClick={props.onClick}
					/>
				)}
				{suspensionesDurationDays > 0 && (
					<NovedadContador
						novedadText={'S'}
						durationDays={suspensionesDurationDays}
						backgroundColor={'rgba(94, 53, 177, 0.16)'}
						typographyColor={'#5E35B1'}
					/>
				)}
				{incapacidadesDurationDays > 0 && (
					<NovedadContador
						novedadText={'I'}
						durationDays={incapacidadesDurationDays}
						backgroundColor={'rgba(94, 53, 177, 0.16)'}
						typographyColor={'#5E35B1'}
					/>
				)}
			</div>
			{(props.retired || props.newEntry) && (
				<div className={classes.chipsRow}>
					{props.retired && (
						<Chip
							className={classes.novedadChip}
							label={'Retiro'}
							variant="outlined"
							style={{
								color: 'rgb(255,73,90)',
								backgroundColor: 'rgb(255,73,90,0.2)',
								borderColor: 'rgb(255,73,90)'
							}}
						/>
					)}

					{props.newEntry && (
						<Chip
							className={classes.novedadChip}
							label={'Ingreso'}
							variant="outlined"
							style={{
								color: 'rgb(117, 122, 255)',
								backgroundColor: 'rgb(117, 122, 255,0.2)',
								borderColor: 'rgb(117, 122, 255)'
							}}
						/>
					)}
				</div>
			)}
		</div>
	);
};

const NovedadContador = (props) => {
	const classes = useStyles();
	return (
		<div className={classes.novedadContadorContainer}>
			<Typography
				style={{
					fontSize: '1.1em',
					fontWeight: 'bold'
				}}
			>
				{props.novedadText}
			</Typography>

			<ButtonBase
				className={classes.dayCounterField}
				style={{
					backgroundColor: props.backgroundColor
				}}
				disableRipple
			>
				<Typography
					style={{
						color: props.typographyColor,
						fontSize: '1.1em',
						fontWeight: 'bold'
					}}
				>
					{props.durationDays}
				</Typography>
			</ButtonBase>
		</div>
	);
};

const theme = createMuiTheme({
	overrides: {
		MuiOutlinedInput: {
			root: {
				'& fieldset': {
					borderColor: 'purple'
				},
				'&:hover fieldset': {
					borderColor: 'purple'
				},
				'&.Mui-focused fieldset': {
					borderColor: 'purple'
				},
				'&:hover:not($disabled):not($focused):not($error) $notchedOutline': {
					borderColor: 'purple'
				}
			}
		},
		MTableToolbar: {
			root: {
				backgroundColor: 'orange'
			}
		}
		// ,
		// MTableToolbar:{
		// 	backgroundColor:'red',
		// 	root:{
		// 		backgroundColor:'red'
		// 	}
		// }

		// MuiTableSortLabel: {
		// 	root: {
		// 		justifyContent: 'center',
		// 		'&MuiTableSortLabel-active':{
		// 			color:'grey'
		// 		}
		// 	},
		// 	icon:{
		// 		display:'none'
		// 	},
		// 	active:{
		// 		color:'grey'
		// 	}

		// }
	},
	palette: {
		primary: {
			main: '#7B1FA2'
		},
		secondary: {
			main: '#f44336'
		}
	}
});

const useStyles = makeStyles({
	userInfoContainer: {
		display: 'flex',
		flexDirection: 'row',
		width: '100%',
		height: 80,
		cursor: 'pointer',
		'&:hover': {
			// backgroundColor: 'grey'
		},
		alignItems: 'center'
	},

	userInfoTextContainer: {
		display: 'flex',
		flexDirection: 'column',
		flex: 1,
		justifyContent: 'space-evenly',
		marginLeft: 5
	},

	textColumn: {
		display: 'flex',
		flexDirection: 'column',
		width: '100%',
		height: 80,
		alignItems: 'center',
		justifyContent: 'center'
	},
	textColumnText: {
		color: '#455A64',
		fontWeight: 'bold'
	},
	corregirPLanillaButton: {
		border: '1.5px solid #7B1FA2',
		backgroundColor: 'white',
		borderRadius: 30,
		width: '80%',
		height: 30,
		margin: '0px 6px 0px 6px'
	},
	buttonContainer: {
		width: '100%'
	},
	corregirPLanillaButtonContainer: {
		display: 'flex',
		justifyContent: 'flex-end',
		width: '20%'
	},
	noveltiesContainer: {
		display: 'flex',
		flexDirection: 'column',
		width: '100%',
		height: 80
	},
	countersRow: {
		display: 'flex',
		flex: 1,
		flexDirection: 'row',
		width: '100%'
	},

	novedadContadorContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		width: 80,
		cursor: 'pointer'
	},
	dayCounterField: {
		border: '1.5px solid #5E35B1',
		borderRadius: 7,
		width: '60%',
		height: 30,
		margin: '0px 6px 0px 6px'
	},
	chipsRow: {
		display: 'flex',
		flex: 1,
		flexDirection: 'row',
		width: '100%',
		alignItems: 'center'
	},
	novedadChip: {
		flex: 1,
		color: 'gray',
		height: '80%',
		borderRadius: 20,
		backgroundColor: 'white',
		fontWeight: 'bold',
		borderColor: 'gray'
	}
});

const mapStateToProps = (state) => {
	return {
		// prePlanilla: state.prePlanilla,
		// activeCompany: state.currentUser.activeCompany,
		// periodosInfo: state.currentUser.periodosInfo
		planillaDetail: state.planillaDetail,
		planillasHistory: state.planillasHistory
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		cotizantesDetailAction: (actionType, value) => dispatch(cotizantesDetailAction(actionType, value)),
		planillaDetailAction: (actionType, value) => dispatch(planillaDetailAction(actionType, value))
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PlanillaDetailTable));
