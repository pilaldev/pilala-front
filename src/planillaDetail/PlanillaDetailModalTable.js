import React, { Component } from 'react';
import { connect } from 'react-redux';
import MaterialTable from 'material-table';
import { Typography, ThemeProvider, Chip, ButtonBase } from '@material-ui/core';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';

import { planillaDetailAction } from './../redux/Actions';
import { PLANILLA_DETAIL } from './../redux/ActionTypes';

class PlanillaDetailModalTable extends Component {
	handleSelectionChange = (rows) => {
		this.props.planillaDetailAction(PLANILLA_DETAIL.PLANILLA_DETAIL_SET_SELECTED_COTIZANTES, rows);
	};

	render() {
		return (
			<ThemeProvider theme={theme}>
				<MaterialTable
					title="Cotizantes"
					columns={[
						{
							title: 'Empleado',
							field: 'name.display',
							render: (rowData) => <UserInfoComponent {...rowData} />,
							customFilterAndSearch: (filter, rowData) => {
								let text = filter.toLowerCase();
								let tempName = rowData.name.display.toLowerCase();

								if (tempName.indexOf(text) > -1 || rowData.documentNumber.indexOf(text) > -1) {
									return true;
								} else {
									return false;
								}
							},
							headerStyle: headerStyle
						},
						{
							title: 'Novedades',
							render: (rowData) => <NoveltiesContainer {...rowData} />,
							headerStyle: headerStyle
						}
					]}
					data={this.props.cotizantes}
					options={{
						sorting: false,
						draggable: false,
						selection: true,
						selectionProps: (rowData) => ({
							checked:
								this.props.selectedCotizantes.findIndex(
									(cotizante) => cotizante.documentNumber === rowData.documentNumber
								) >= 0
						}),
						rowStyle: (rowData) => ({
							backgroundColor:
								this.props.selectedCotizantes.findIndex(
									(cotizante) => cotizante.documentNumber === rowData.documentNumber
								) >= 0
									? 'rgb(239, 228, 243)'
									: '#FFF'
						})
					}}
					localization={{
						toolbar: {
							nRowsSelected: '{0} cotizante(s) seleccionados'
						},
						body: {
							emptyDataSourceMessage: 'No records to display'
						}
					}}
					onSelectionChange={(rows) => this.handleSelectionChange(rows)}
				/>
			</ThemeProvider>
		);
	}
}

const headerStyle = {
	textAlign: 'left',
	fontWeight: 'bold',
	color: 'grey'
};

const UserInfoComponent = (props) => {
	const classes = useStyles();
	// console.log('props', props.index);
	return (
		<div className={classes.userInfoContainer}>
			<img
				alt="user"
				className={classes.userInfoImage}
				src={props.genre.value ? require(`../assets/img/women_one.svg`) : require(`../assets/img/men_one.svg`)}
			/>
			<div className={classes.userInfoTextContainer}>
				<Typography className={classes.userInfoTextBox} noWrap style={{ fontWeight: 'bold' }}>
					{props.name.display}
				</Typography>
				<Typography className={classes.userInfoTextBox} noWrap>
					{props.documentNumber}
				</Typography>
			</div>
		</div>
	);
};

const NoveltiesContainer = (props) => {
	const classes = useStyles();

	let vacacionesDurationDays = 0;
	let incapacidadesDurationDays = 0;
	let suspensionesDurationDays = 0;
	let licenciasDurationDays = 0;

	props.licencias.forEach((item) => {
		licenciasDurationDays += item.duracionDias;
	});

	props.vacaciones.forEach((item) => {
		vacacionesDurationDays += item.duracionDias;
	});

	props.suspensiones.forEach((item) => {
		suspensionesDurationDays += item.duracionDias;
	});

	props.incapacidades.forEach((item) => {
		incapacidadesDurationDays += item.duracionDias;
	});

	return (
		<div className={classes.noveltiesContainer}>
			<div className={classes.countersRow}>
				{licenciasDurationDays > 0 && (
					<NovedadContador
						novedadText={'L'}
						durationDays={licenciasDurationDays}
						backgroundColor={'rgba(94, 53, 177, 0.16)'}
						typographyColor={'#5E35B1'}
					/>
				)}

				{vacacionesDurationDays > 0 && (
					<NovedadContador
						novedadText={'V'}
						durationDays={vacacionesDurationDays}
						backgroundColor={'rgba(94, 53, 177, 0.16)'}
						typographyColor={'#5E35B1'}
						onClick={props.onClick}
					/>
				)}
				{suspensionesDurationDays > 0 && (
					<NovedadContador
						novedadText={'S'}
						durationDays={suspensionesDurationDays}
						backgroundColor={'rgba(94, 53, 177, 0.16)'}
						typographyColor={'#5E35B1'}
					/>
				)}
				{incapacidadesDurationDays > 0 && (
					<NovedadContador
						novedadText={'I'}
						durationDays={incapacidadesDurationDays}
						backgroundColor={'rgba(94, 53, 177, 0.16)'}
						typographyColor={'#5E35B1'}
					/>
				)}
			</div>
			{(props.retired || props.newEntry) && (
				<div className={classes.chipsRow}>
					{props.retired && (
						<Chip
							className={classes.novedadChip}
							label={'Retiro'}
							variant="outlined"
							style={{
								color: 'rgb(255,73,90)',
								backgroundColor: 'rgb(255,73,90,0.2)',
								borderColor: 'rgb(255,73,90)'
							}}
						/>
					)}

					{props.newEntry && (
						<Chip
							className={classes.novedadChip}
							label={'Ingreso'}
							variant="outlined"
							style={{
								color: 'rgb(117, 122, 255)',
								backgroundColor: 'rgb(117, 122, 255,0.2)',
								borderColor: 'rgb(117, 122, 255)'
							}}
						/>
					)}
				</div>
			)}
		</div>
	);
};

const NovedadContador = (props) => {
	const classes = useStyles();
	return (
		<div className={classes.novedadContadorContainer}>
			<Typography
				style={{
					fontSize: '1.1em',
					fontWeight: 'bold'
				}}
			>
				{props.novedadText}
			</Typography>

			<ButtonBase
				className={classes.dayCounterField}
				style={{
					backgroundColor: props.backgroundColor
				}}
				disableRipple
			>
				<Typography
					style={{
						color: props.typographyColor,
						fontSize: '1.1em',
						fontWeight: 'bold'
					}}
				>
					{props.durationDays}
				</Typography>
			</ButtonBase>
		</div>
	);
};

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#7B1FA2'
		},
		secondary: {
			main: '#7B1FA2'
		}
	}
});

const useStyles = makeStyles({
	userInfoContainer: {
		display: 'flex',
		flexDirection: 'row',
		width: '100%',
		height: 80,
		cursor: 'pointer',
		'&:hover': {
			// backgroundColor: 'grey'
		},
		alignItems: 'center'
	},
	userInfoImage: {
		height: '80%',
		width: 'auto',
		backgroundSize: 'contain'
	},
	userInfoTextContainer: {
		display: 'flex',
		flexDirection: 'column',
		flex: 1,
		justifyContent: 'space-evenly',
		marginLeft: 5
	},
	noveltiesContainer: {
		display: 'flex',
		flexDirection: 'column',
		width: '100%',
		height: 80
	},
	countersRow: {
		display: 'flex',
		justifyContent: 'center',
		flex: 1,
		flexDirection: 'row',
		width: '100%'
	},

	novedadContadorContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		width: 50,
		cursor: 'pointer'
	},
	dayCounterField: {
		border: '1.5px solid #5E35B1',
		borderRadius: 7,
		width: '50%',
		height: 30,
		margin: '0px 6px 0px 6px'
	},
	chipsRow: {
		display: 'flex',
		flex: 1,
		flexDirection: 'row',
		width: '100%',
		alignItems: 'center'
	},
	novedadChip: {
		flex: 1,
		color: 'gray',
		height: '80%',
		borderRadius: 20,
		backgroundColor: 'white',
		fontWeight: 'bold',
		borderColor: 'gray'
	}
});

const mapStateToProps = (state) => {
	return {
		cotizantes: state.planillaDetail.cotizantes,
		selectedCotizantes: state.planillaDetail.selectedCotizantes
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		planillaDetailAction: (actionType, value) => dispatch(planillaDetailAction(actionType, value))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(PlanillaDetailModalTable);
