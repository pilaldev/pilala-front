import React, { useCallback, useEffect, useState } from 'react';
import { Grid, Card, Typography, CircularProgress } from '@material-ui/core';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import TextInput from '@bit/pilala.pilalalib.components.textinput';
import FaceIcon from '@material-ui/icons/Face';
import Button from '@bit/pilala.pilalalib.components.button';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { FIREBASE_AUTH } from '../api/Firebase';
import {
  RESTORE_PASSWORD_TITLE,
  RESTORE_PASSWORD_BUTTON,
  RESET_PASSWORD_MESSAGES,
  EMAIL,
  SUCCESS_TITLE,
  SUCCESS_MESSAGE,
  RETURN_BUTTON,
  CONFIRM_PASSWORD,
  NEW_PASSWORD,
  INFO_ERROR_TITLE,
  UPDATE_ERROR_TITLE,
  PASSWORD_CHECK_ERROR_TITLE,
  PASSWORD_CHECK_ERROR_MESSAGE,
} from './Constants';
import { useResetPasswordStyles } from './styles';
import { snackBarAction } from '../redux/Actions';
import { SNACKBAR } from '../redux/ActionTypes';

export default () => {
  const classes = useResetPasswordStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [tokenValidationStatus, setTokenValidationStatus] = useState(null);
  const [params, setParams] = useState({});
  const [userEmail, setUserEmail] = useState('');
  const [password, setPassword] = useState({
    newPassword: '',
    confirmed: '',
  });
  const [inputError, setInputError] = useState({
    newPassword: false,
    newPasswordHelper: '',
    confirmed: false,
    confirmedHelper: '',
  });
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmedPassword, setShowConfirmedPassword] = useState(false);
  const [disableSaveButton, setDisableSaveButton] = useState(false);
  const [showReturnButton, setShowReturnButton] = useState(false);

  const verifyPasswordResetCode = useCallback(
    async (oobCode) => {
      try {
        const email = await FIREBASE_AUTH.verifyPasswordResetCode(oobCode);
        if (!userEmail) {
          setUserEmail(email);
        }
        setTokenValidationStatus(true);
        return email;
      } catch (errorObject) {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: INFO_ERROR_TITLE,
            description:
              RESET_PASSWORD_MESSAGES[errorObject?.code || 'DEFAULT'],
            color: 'error',
          })
        );
        setTokenValidationStatus(false);
      }
      return undefined;
    },
    [dispatch, userEmail]
  );

  useEffect(() => {
    ChangePageTitle('Actualizar contraseña | Pilalá');

    const saveIncomingParams = async () => {
      const urlParams = {};
      window.location.href.replace(
        /[?&]+([^=&]+)=([^&]*)/gi,
        (_, key, value) => {
          urlParams[key] = value;
        }
      );
      setParams(urlParams);

      if (urlParams.mode === 'resetPassword' && urlParams.oobCode) {
        await verifyPasswordResetCode(urlParams.oobCode);
      } else {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: INFO_ERROR_TITLE,
            description: RESET_PASSWORD_MESSAGES.BAD_REQUEST,
            color: 'error',
          })
        );
        setTokenValidationStatus(false);
      }
    };
    saveIncomingParams();
  }, [dispatch, verifyPasswordResetCode]);

  const confirmPasswordReset = async () => {
    await FIREBASE_AUTH.confirmPasswordReset(
      params.oobCode,
      password.newPassword
    )
      .then(() => {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: SUCCESS_TITLE,
            description: SUCCESS_MESSAGE,
            color: 'success',
          })
        );
        setDisableSaveButton(false);
        setShowReturnButton(true);
      })
      .catch((errorObject) => {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: UPDATE_ERROR_TITLE,
            description:
              RESET_PASSWORD_MESSAGES[errorObject?.code || 'DEFAULT'],
            color: 'error',
          })
        );
        setDisableSaveButton(false);
      });
  };

  const checkPassword = () => {
    if (!password.newPassword && !password.confirmed) {
      setInputError({
        newPassword: true,
        confirmed: true,
        newPasswordHelper: RESET_PASSWORD_MESSAGES.REQUIRED_FIELD,
        confirmedHelper: RESET_PASSWORD_MESSAGES.REQUIRED_FIELD,
      });
      return false;
    }

    if (!password.newPassword) {
      setInputError((prevState) => ({
        ...prevState,
        newPassword: true,
        newPasswordHelper: RESET_PASSWORD_MESSAGES.REQUIRED_FIELD,
      }));
      return false;
    }

    if (!password.confirmed) {
      setInputError((prevState) => ({
        ...prevState,
        confirmed: true,
        confirmedHelper: RESET_PASSWORD_MESSAGES.REQUIRED_FIELD,
      }));
      return false;
    }

    if (!(password.newPassword === password.confirmed)) {
      setInputError({
        newPassword: true,
        confirmed: true,
        newPasswordHelper: RESET_PASSWORD_MESSAGES.UNEQUEAL_PASSWORD,
        confirmedHelper: RESET_PASSWORD_MESSAGES.UNEQUEAL_PASSWORD,
      });
      return false;
    }

    if (!(password.newPassword.length >= 8 && password.confirmed.length >= 8)) {
      setInputError({
        newPassword: true,
        confirmed: true,
        newPasswordHelper: RESET_PASSWORD_MESSAGES.INVALID_LENGTH,
        confirmedHelper: RESET_PASSWORD_MESSAGES.INVALID_LENGTH,
      });
      return false;
    }

    return true;
  };

  const onFocus = (e) => {
    const { name } = e.target;
    if (name === 'newPassword') {
      if (inputError.newPassword) {
        setInputError((prevState) => ({
          ...prevState,
          newPassword: false,
          newPasswordHelper: '',
        }));
      }
    }
    if (name === 'confirmedPassword') {
      if (inputError.confirmed) {
        setInputError((prevState) => ({
          ...prevState,
          confirmed: false,
          confirmedHelper: '',
        }));
      }
    }
  };

  const onChange = (e) => {
    const { name, value } = e.target;
    if (name === 'newPassword') {
      setPassword((prevState) => ({
        ...prevState,
        newPassword: value.trim(),
      }));
    } else {
      setPassword((prevState) => ({
        ...prevState,
        confirmed: value.trim(),
      }));
    }
  };

  const onSave = (e) => {
    e.preventDefault();
    if (disableSaveButton) return;
    setDisableSaveButton(true);

    if (checkPassword()) {
      confirmPasswordReset();
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: PASSWORD_CHECK_ERROR_TITLE,
          description: PASSWORD_CHECK_ERROR_MESSAGE,
          color: 'error',
        })
      );
      setDisableSaveButton(false);
    }
  };

  const handleShowNewPassword = () => {
    setShowNewPassword(!showNewPassword);
  };

  const handleShowConfirmedPassword = () => {
    setShowConfirmedPassword(!showConfirmedPassword);
  };

  const returnToSignIn = () => {
    history.replace('/');
  };

  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.ResetPassword}
    >
      <Card className={classes.ResetPassword__Card}>
        <Grid container direction="column">
          <Typography
            variant="h4"
            className={classes.ResetPassword__Card__Title}
          >
            {RESTORE_PASSWORD_TITLE}
          </Typography>
          <form>
            <TextInput
              fullWidth
              displayOnly
              value={userEmail}
              label={EMAIL}
              placeholder={EMAIL}
              startIcon={<FaceIcon />}
              InputProps={{
                autoComplete: 'email',
              }}
            />
            <div className={classes.ResetPassword__Card__InputContainer}>
              <TextInput
                fullWidth
                value={password.newPassword}
                label={NEW_PASSWORD}
                placeholder={NEW_PASSWORD}
                name="newPassword"
                onChange={onChange}
                onFocus={onFocus}
                error={inputError.newPassword}
                helperText={inputError.newPasswordHelper}
                type={showNewPassword ? 'text' : 'password'}
                InputProps={{
                  autoComplete: 'new-password',
                }}
                endIcon={
                  <IconButton
                    onClick={handleShowNewPassword}
                    filled={false}
                    size="small"
                    buttoncolor="grey"
                  >
                    {showNewPassword ? (
                      <VisibilityIcon />
                    ) : (
                      <VisibilityOffIcon />
                    )}
                  </IconButton>
                }
              />
            </div>
            <div className={classes.ResetPassword__Card__InputContainer}>
              <TextInput
                fullWidth
                value={password.confirmed}
                label={CONFIRM_PASSWORD}
                placeholder={CONFIRM_PASSWORD}
                name="confirmedPassword"
                onChange={onChange}
                onFocus={onFocus}
                error={inputError.confirmed}
                helperText={inputError.confirmedHelper}
                type={showConfirmedPassword ? 'text' : 'password'}
                InputProps={{
                  autoComplete: 'new-password',
                }}
                endIcon={
                  <IconButton
                    onClick={handleShowConfirmedPassword}
                    filled={false}
                    size="small"
                    buttoncolor="grey"
                  >
                    {showConfirmedPassword ? (
                      <VisibilityIcon />
                    ) : (
                      <VisibilityOffIcon />
                    )}
                  </IconButton>
                }
              />
            </div>
            <Grid
              item
              container
              justify="space-evenly"
              className={classes.ResetPassword__Card__Submit}
            >
              {tokenValidationStatus === null ? (
                <CircularProgress size={30} />
              ) : !showReturnButton ? (
                <Button
                  disabled={!tokenValidationStatus}
                  onClick={onSave}
                  type="submit"
                  loading={disableSaveButton}
                >
                  {RESTORE_PASSWORD_BUTTON}
                </Button>
              ) : (
                <Button onClick={returnToSignIn} variant="outlined">
                  {RETURN_BUTTON}
                </Button>
              )}
            </Grid>
          </form>
        </Grid>
      </Card>
    </Grid>
  );
};
