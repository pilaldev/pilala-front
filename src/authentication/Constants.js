/* Labels */
export const RETRIEVAL_PASSWORD_TITLE = 'Recupera tu contraseña';
export const FORGOT_PASSWORD_MESSAGES = {
  INVALID_EMAIL: 'Por favor ingrese un correo electrónico válido',
  USER_NOT_FOUND:
    'No hemos encontrado el correo electrónico ingresado, por favor verificalo',
  DEFAULT: 'Tenemos problemas para procesar tu solicitud, inténtelo más tarde',
  SUCCESS:
    'Revisa tu correo electrónico para continuar con el proceso de restaurar tu contraseña',
  TOO_MANY_REQUESTS:
    'Has excedido el número de solicitudes permitidas por lo tanto han sido bloqueadas',
  SUCCESS_TITLE: 'Link de restauración enviado',
  ERROR_TITLE: 'Error al recuperar contraseña',
};
export const RESTORE_PASSWORD_TITLE = 'Restaurar contraseña';
export const RESET_PASSWORD_MESSAGES = {
  'auth/expired-action-code':
    'El link de cambio de contraseña ha expirado, por favor solicite uno nuevo',
  'auth/invalid-action-code':
    'El link de cambio de contraseña es inválido o ya ha sido utilizado, por favor solicite uno nuevo',
  'auth/user-disabled':
    'El usuario que solicitó el cambio de contraseña está inhabilitado, por favor comuniquese con el administrador',
  'auth/user-not-found':
    'El usuario que solicitó el cambio de contraseña no existe, por favor comuniquese con el administrador',
  'auth/weak-password':
    'La contraseña es muy débil, por favor ingrese una contraseña diferente',
  BAD_REQUEST:
    'Parece que el link actual no es válido, por favor solicite un nuevo cambio de contraseña',
  DEFAULT:
    'Tenemos problemas para procesar tu solicitud, por favor inténtelo más tarde',
  UNEQUEAL_PASSWORD: 'Las contraseñas no coinciden',
  INVALID_LENGTH: 'La longitud mínima es de 8 caracteres',
  REQUIRED_FIELD: 'Este campo es obligatorio',
};
export const SUCCESS_MESSAGE =
  'Tu contraseña ha sido actualizada correctamente';
export const SUCCESS_TITLE = 'Actualización exitosa';
export const INFO_ERROR_TITLE = 'Error al validar información';
export const UPDATE_ERROR_TITLE = 'Error al actualizar contraseña';
export const PASSWORD_CHECK_ERROR_TITLE = 'Error al validar contraseñas';
export const PASSWORD_CHECK_ERROR_MESSAGE =
  'Por favor verifica la contraseña ingresada';

/* Errors */
export const FORGOT_PASSWORD_ERRORS = {
  INVALID_EMAIL: 'auth/invalid-email',
  USER_NOT_FOUND: 'auth/user-not-found',
  TOO_MANY_REQUESTS: 'auth/too-many-requests',
};

/* Buttons */
export const RETRIEVAL_PASSWORD_BUTTON = 'Recuperar';
export const CANCEL_BUTTON = 'Cancelar';
export const RESEND = 'Reenviar';
export const CONFIRM = 'Confirmar';
export const RESTORE_PASSWORD_BUTTON = 'Restaurar';
export const RETURN_BUTTON = 'Ir a iniciar sesión';

/* Fields */
export const EMAIL = 'Correo electrónico';
export const EMAIL_ERROR_HELPER =
  'Por favor ingrese un correo electrónico válido';
export const NEW_PASSWORD = 'Nueva contraseña';
export const CONFIRM_PASSWORD = 'Confirmar contraseña';
