import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  ResetPassword: {
    background:
      'radial-gradient(circle, rgba(53,26,124,1) 0%, rgba(42,27,89,1) 33%, rgba(31,28,53,1) 66%, rgba(29,14,51,1) 100%)',
  },
  ResetPassword__Card: {
    display: 'flex',
    width: '90%',
    borderRadius: '0.625rem',
    boxShadow: 'none',
    padding: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      minWidth: '33.75rem',
      maxWidth: '33.75rem',
    },
  },
  ResetPassword__Card__Title: {
    textAlign: 'left',
    fontWeight: 'bold',
    color: theme.palette.common.black,
    marginBottom: theme.spacing(4),
  },
  ResetPassword__Card__InputContainer: {
    marginTop: theme.spacing(2),
  },
  ResetPassword__Card__Submit: {
    marginTop: theme.spacing(4),
  },
  ResetPassword__Card__Submit__Message: {
    color: theme.palette.error.main,
    fontWeight: 'bold',
  },
}));
