import React, { useEffect, useState } from 'react';
import { Card, Fade, Grid, Typography } from '@material-ui/core';
import FaceIcon from '@material-ui/icons/Face';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
import TextInput from '@bit/pilala.pilalalib.components.textinput';
import Button from '@bit/pilala.pilalalib.components.button';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import { useForgotPasswordStyles } from './styles';
import {
  RETRIEVAL_PASSWORD_TITLE,
  RETRIEVAL_PASSWORD_BUTTON,
  CANCEL_BUTTON,
  EMAIL,
  RESEND,
  CONFIRM,
  FORGOT_PASSWORD_ERRORS,
  FORGOT_PASSWORD_MESSAGES,
  EMAIL_ERROR_HELPER,
} from './Constants';
import { FIREBASE_AUTH } from '../api/Firebase';
import { snackBarAction } from '../redux/Actions';
import { SNACKBAR } from '../redux/ActionTypes';

const ForgotPassword = () => {
  const classes = useForgotPasswordStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [inputError, setInputError] = useState({
    error: false,
    message: undefined,
  });
  const [fadeState, setFadeState] = useState(false);
  const [disableAcceptButton, setDisableAcceptButton] = useState(false);
  const [disableResendButton, setDisableResendButton] = useState(false);
  const [firstAttempt, setFirstAttempt] = useState(false);

  useEffect(() => {
    ChangePageTitle('Recuperar contraseña | Pilalá');
  }, []);

  useEffect(() => {
    setFadeState(true);
  }, []);

  const onCancel = () => {
    setFadeState(false);
  };

  const onChange = (e) => {
    setEmail(e.target.value.trim());
  };

  const onFocus = () => {
    if (inputError.error) {
      setInputError((prevState) => ({
        ...prevState,
        error: false,
      }));
    }
  };

  const sendResetEMail = () => {
    FIREBASE_AUTH.sendPasswordResetEmail(email)
      .then(() => {
        dispatch(
          snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
            title: FORGOT_PASSWORD_MESSAGES.SUCCESS_TITLE,
            description: FORGOT_PASSWORD_MESSAGES.SUCCESS,
            color: 'success',
          })
        );
        setFirstAttempt(true);
        setDisableAcceptButton(false);
        setDisableResendButton(false);
      })
      .catch((error) => {
        setDisableAcceptButton(false);
        setDisableResendButton(false);
        switch (error.code) {
          case FORGOT_PASSWORD_ERRORS.INVALID_EMAIL:
            dispatch(
              snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: FORGOT_PASSWORD_MESSAGES.ERROR_TITLE,
                description: FORGOT_PASSWORD_MESSAGES.INVALID_EMAIL,
                color: 'error',
              })
            );
            break;
          case FORGOT_PASSWORD_ERRORS.USER_NOT_FOUND:
            dispatch(
              snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: FORGOT_PASSWORD_MESSAGES.ERROR_TITLE,
                description: FORGOT_PASSWORD_MESSAGES.USER_NOT_FOUND,
                color: 'error',
              })
            );
            break;
          case FORGOT_PASSWORD_ERRORS.TOO_MANY_REQUESTS:
            dispatch(
              snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: FORGOT_PASSWORD_MESSAGES.ERROR_TITLE,
                description: FORGOT_PASSWORD_MESSAGES.TOO_MANY_REQUESTS,
                color: 'error',
              })
            );
            break;
          default:
            dispatch(
              snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
                title: FORGOT_PASSWORD_MESSAGES.ERROR_TITLE,
                description: FORGOT_PASSWORD_MESSAGES.DEFAULT,
                color: 'error',
              })
            );
        }
      });
  };

  const onConfirm = (e) => {
    e.preventDefault();
    if (disableAcceptButton || disableResendButton) return;
    setDisableAcceptButton(true);
    setDisableResendButton(true);

    if (
      email !== '' &&
      /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})*$/.test(email)
    ) {
      sendResetEMail();
    } else {
      setInputError({
        error: true,
        message: EMAIL_ERROR_HELPER,
      });
      setDisableAcceptButton(false);
      setDisableResendButton(false);
    }
  };

  const onExiting = () => {
    history.goBack();
  };

  return (
    <Fade in={fadeState} onExiting={onExiting}>
      <Grid
        container
        justify="center"
        alignItems="center"
        className={classes.ForgotPassword}
      >
        <Card className={classes.ForgotPassword__Card}>
          <Grid container direction="column">
            <Typography
              variant="h4"
              className={classes.ForgotPassword__Card__Title}
            >
              {RETRIEVAL_PASSWORD_TITLE}
            </Typography>
            <form>
              <TextInput
                fullWidth
                label={EMAIL}
                startIcon={<FaceIcon />}
                InputProps={{
                  autoComplete: 'email',
                }}
                placeholder={EMAIL}
                error={inputError?.error}
                helperText={inputError?.message}
                onChange={onChange}
                onFocus={onFocus}
                value={email}
              />
              {!firstAttempt ? (
                <Grid
                  item
                  container
                  justify="space-evenly"
                  className={classes.ForgotPassword__Card__Submit}
                >
                  <Button
                    variant="outlined"
                    onClick={onCancel}
                    disabled={disableAcceptButton}
                  >
                    {CANCEL_BUTTON}
                  </Button>
                  <Button
                    onClick={onConfirm}
                    type="submit"
                    loading={disableAcceptButton}
                  >
                    {RETRIEVAL_PASSWORD_BUTTON}
                  </Button>
                </Grid>
              ) : (
                <Grid
                  item
                  container
                  justify="space-evenly"
                  className={classes.ForgotPassword__Card__Submit}
                >
                  <Button
                    variant="text"
                    onClick={onConfirm}
                    loading={disableResendButton}
                    type="submit"
                  >
                    {RESEND}
                  </Button>
                  <Button onClick={onCancel} disabled={disableResendButton}>
                    {CONFIRM}
                  </Button>
                </Grid>
              )}
            </form>
          </Grid>
        </Card>
      </Grid>
    </Fade>
  );
};

export default ForgotPassword;
