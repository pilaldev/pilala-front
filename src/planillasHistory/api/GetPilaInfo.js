import Endpoints from '@bit/pilala.pilalalib.endpoints';

const getPilaInfo = async (idAportante) => {
	let result = {};

	const requestData = {
		idAportante,
	};

	const requestBody = JSON.stringify(requestData);

	const requestOptions = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: requestBody,
	};

	await fetch(Endpoints.getPilaInfo, requestOptions)
		.then((response) => response.json())
		.then((resultFetch) => {
			result.status = resultFetch.status;
			result.data = resultFetch.result;
		})
		.catch((error) => {
			console.log('Error en fetch de obtener historial de planillas', error);
			result.status = 'FETCH_FAILED';
			result.data = null;
		});

	return result;
};

export default getPilaInfo;
