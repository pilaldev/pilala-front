import React, { Component } from 'react';
import { connect } from 'react-redux';
import HistoryLayout from './HistoryLayout';
import { ChangePageTitle } from '@bit/pilala.pilalalib.utils';
import GetPilaInfo from './api/GetPilaInfo';
import {
  planillasHistoryAction,
  navigationAction,
  planillaDetailAction,
} from '../redux/Actions';
import { PLANILLAS_HISTORY, PLANILLA_DETAIL } from '../redux/ActionTypes';
import { NAVIGATION } from '../redux/ActionTypes';
import moment from 'moment';
import 'moment/locale/es';

moment.locale('es');

class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      planillas: [],
    };
    this.planillas = [];
  }

  componentDidMount() {
    ChangePageTitle('Historial de planillas | Pilalá');
    this.props.navigationAction(
      NAVIGATION.NAVIGATION_SAVE_SIDE_TAB,
      'historialPago'
    );
    this.getPilaHistory();
  }

  openPlanillaDetail = (idPlanilla) => {
    this.props.planillaDetailAction(
      PLANILLA_DETAIL.PLANILLA_DETAIL_SET_PLANILLA_NUMBER,
      idPlanilla
    );
    this.props.history.push(
      `/${this.props.currentUser.uid}/console/planillaDetail/${idPlanilla}`
    );
  };

  getPilaHistory = async () => {
    const { activeCompany } = this.props.currentUser;
    const historyResult = await GetPilaInfo(activeCompany);
    if (historyResult.status === 'SUCCESS') {
      this.formatPlanillasData(historyResult.data);
      this.props.planillasHistoryAction(
        PLANILLAS_HISTORY.PLANILLAS_HISTORY_SAVE_PLANILLAS,
        historyResult.data
      );
    }
  };

  filterPlanillasList = (event) => {};

  formatPlanillasData = (data) => {
    let periodos = {};
    data.map((item) => {
      const periodo = moment(item.periodo.monthDate, 'MM').format('MMMM');
      const año = item.periodo.month.split('_')[1];
      const label = `${periodo} ${año}`;
      if (periodos[periodo]) {
        periodos[periodo] = {
          ...periodos[periodo],
          planillas: [...periodos[periodo].planillas, item],
        };
      } else {
        periodos[periodo] = {
          label,
          planillas: [item],
        };
      }
      return null;
    });
    this.setState({
      planillas: periodos,
    });
    this.planillas = periodos;
  };

  render() {
    const layoutProps = {
      openPlanillaDetail: this.openPlanillaDetail,
      filterPlanillasList: this.filterPlanillasList,
      planillas: this.state.planillas,
    };

    return <HistoryLayout {...layoutProps} />;
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser,
    planillasHistory: state.planillasHistory.planillas,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    planillasHistoryAction: (actionType, value) =>
      dispatch(planillasHistoryAction(actionType, value)),
    navigationAction: (actionType, value) =>
      dispatch(navigationAction(actionType, value)),
    planillaDetailAction: (actionType, value) =>
      dispatch(planillaDetailAction(actionType, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(History);
