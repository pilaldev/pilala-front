import React, { useState, useEffect } from 'react';
import {
	Typography,
	// ButtonBase,
	TextField,
	InputAdornment,
	Table as MuiTable,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	Collapse,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
import HistoryImage from './../assets/img/alex_historial_pila.jpg';
import ExpandMoreRoundedIcon from '@material-ui/icons/ExpandMoreRounded';
import ExpandLessRoundedIcon from '@material-ui/icons/ExpandLessRounded';
import {
	PLANILLAS_HISTORY,
	// PAYMENT_REPORT,
	SEARCHBAR_PLACEHOLDER,
} from './Constants';
import moment from 'moment';
import 'moment/locale/es';
import clsx from 'clsx';
import './styles.css';
import { Table } from '../components';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
// import MonetizationOnOutlinedIcon from '@material-ui/icons/MonetizationOnOutlined';
// import EditIcon from '@material-ui/icons/Edit';
// import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';

moment.locale('es');

const HistoryLayout = (props) => {
	const classes = useStyles();
	const { openPlanillaDetail } = props;
	const [planillasArray, setPlanillasArray] = useState([]);

	useEffect(() => {
		const tempPlanillas = Object.keys(props.planillas).map((key) => props.planillas[key]);
		const sortedArray = tempPlanillas.sort((a, b) => {
			return moment(b.label, 'MMMM') - moment(a.label, 'MMMM');
		});
		setPlanillasArray(sortedArray);
	}, [props.planillas]);

	const payButtonAction = (e) => {
		e.stopPropagation();
	};

	const editButtonAction = (e) => {
		e.stopPropagation();
	};

	const deleteButtonAction = (e) => {
		e.stopPropagation();
	};

	return (
		<div className={classes.container} id='historyContainer'>
			<div className={classes.headerContainer}>
				<Typography className={classes.title}>{PLANILLAS_HISTORY}</Typography>
				{/* <ButtonBase className={classes.reportButton}>
                    <Typography className={classes.reportButtonText}>
                        {PAYMENT_REPORT}
                    </Typography>
                </ButtonBase> */}
			</div>
			<div className={classes.searchBarContainer}>
				<TextField
					className={classes.searchBar}
					onChange={props.filterPlanillasList}
					variant='outlined'
					placeholder={SEARCHBAR_PLACEHOLDER}
					InputProps={{
						startAdornment: (
							<InputAdornment position='start'>
								<SearchRoundedIcon className={classes.searchBarIcon} />
							</InputAdornment>
						),
					}}
					inputProps={{
						style: {
							padding: '10px 10px 10px 0px',
						},
					}}
					classes={{
						root: classes.searchBarCustom,
					}}
				/>
			</div>
			<img src={HistoryImage} alt='history' className={classes.historyImage} />
			<div className={classes.contentContainer}>
				<div className={classes.listContainer}>
					<Table
						Row={(props) => (
							<RowLayout
								{...props}
								openPlanillaDetail={openPlanillaDetail}
								payButtonAction={payButtonAction}
								editButtonAction={editButtonAction}
								deleteButtonAction={deleteButtonAction}
							/>
						)}
						data={planillasArray}
						withActionbar={false}
					/>
				</div>
			</div>
		</div>
	);
};

const RowLayout = (props) => {
	const classes = useStyles();
	const {
		row,
		index,
		// payButtonAction,
		// editButtonAction,
		// deleteButtonAction
	} = props;
	const [open, setOpen] = useState(-1);
	const [periodoStatus, setPeriodoStatus] = useState('');
	const OPEN = open === index;

	useEffect(() => {
		if (row.planillas.every((item) => item.estadoPlanilla === 'OK')) {
			setPeriodoStatus('paid');
		} else if (row.planillas.some((item) => item.estadoPlanilla === 'NA')) {
			setPeriodoStatus('pending');
		}
	}, [row.planillas]);

	const handleExpandRow = (e) => {
		e.stopPropagation();

		if (OPEN) {
			setOpen(-1);
		} else {
			setOpen(index);
		}
	};

	return (
		<React.Fragment>
			<TableRow
				hover={true}
				className={classes.tableRow}
				classes={{
					root: classes.openedRow,
				}}
				onClick={handleExpandRow}
			>
				<TableCell className={classes.headerCellPeriodo}>
					<Typography
						className={clsx(classes.columnTitle, {
							[classes.openedRowTitle]: OPEN,
						})}
					>
						{row.label}
					</Typography>
				</TableCell>
				<TableCell className={classes.headerCellButton}>
					<IconButton
						buttoncolor={OPEN ? 'primary' : 'grey'}
						filled={false}
						onClick={handleExpandRow}
						size='small'
					>
						{OPEN ? <ExpandLessRoundedIcon /> : <ExpandMoreRoundedIcon />}
					</IconButton>
				</TableCell>
			</TableRow>
			<TableRow className={classes.collapsibleRow}>
				<TableCell style={{ padding: 0 }} colSpan={6}>
					<Collapse in={OPEN} timeout='auto' unmountOnExit>
						<MuiTable size='small'>
							<TableHead>
								<TableRow>
									<TableCell align='left' className={classes.rowPlanillaId}>
										N° Planilla
									</TableCell>
									<TableCell align='center' className={classes.rowPlanillaFecha}>
										Fecha
									</TableCell>
									<TableCell align='center' className={classes.rowPlanillaTipo}>
										Tipo
									</TableCell>
									<TableCell align='center' className={classes.rowPlanillaCantCotizantes}>
										Cant. Cotizantes
									</TableCell>
									<TableCell align='center' className={classes.rowPlanillaValor}>
										Valor
									</TableCell>
									<TableCell align='center' className={classes.rowPlanillaEstado}>
										Estado
									</TableCell>
									<TableCell align='center' className={classes.rowPlanillaAcciones}>
										Acciones
									</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{row.planillas.map((item, index) => {
									const date = moment(item.dateCreated).format('DD/MM/YYYY');
									return (
										<TableRow
											key={index}
											style={{ cursor: 'pointer' }}
											hover
											onClick={props.openPlanillaDetail.bind(this, item.numeroPlanilla)}
										>
											<TableCell className={classes.rowPlanillaIdText}>
												{item.numeroPlanilla}
											</TableCell>
											<TableCell>{date}</TableCell>
											<TableCell align='center' className={classes.rowPlanillaTipoText}>
												{'Empleados'}
											</TableCell>
											<TableCell align='center' className={classes.rowPlanillaCantCotizantesText}>
												{item.countCotizantes}
											</TableCell>
											<TableCell align='center' className={classes.rowPlanillaValorText}>
												{`$${item.totalPagar
													.toString()
													.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')}`}
											</TableCell>
											<TableCell
												align='center'
												className={clsx({
													[classes.rowPlanillaEstadoTextOne]: item.estadoPlanilla === 'paid',
													[classes.rowPlanillaEstadoTextTwo]: item.estadoPlanilla === 'pending',
												})}
											>
												<div
													className={clsx({
														[classes.statusContainerOne]: periodoStatus === 'paid',
														[classes.statusContainerTwo]: periodoStatus === 'pending',
													})}
												>
													<Typography
														className={clsx({
															[classes.statusTextOne]: periodoStatus === 'paid',
															[classes.statusTextTwo]: periodoStatus === 'pending',
														})}
													>
														{item.estadoPlanilla === 'OK'
															? 'Pagada'
															: item.estadoPlanilla === 'NA'
															? 'Pendiente'
															: ''}
													</Typography>
												</div>
											</TableCell>
											<TableCell align='center'>
												{/* <div className={classes.actionsCell}>
                                                        <IconButton
                                                            onClick={payButtonAction}
                                                            size='small'
                                                        >
                                                            <MonetizationOnOutlinedIcon />
                                                        </IconButton>
                                                        <IconButton
                                                            onClick={editButtonAction}
                                                            size='small'
                                                        >
                                                            <EditIcon />
                                                        </IconButton>
                                                        <IconButton
                                                            onClick={deleteButtonAction}
                                                            size='small'
                                                        >
                                                            <DeleteOutlineOutlinedIcon />
                                                        </IconButton>
                                                    </div> */}
											</TableCell>
										</TableRow>
									);
								})}
							</TableBody>
						</MuiTable>
					</Collapse>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
};

const useStyles = makeStyles((theme) => ({
	container: {
		display: 'flex',
		flex: 1,
		padding: '3% 5% 0% 3%',
		flexDirection: 'column',
		alignItems: 'flex-start',
		overflow: 'auto',
	},
	title: {
		fontSize: '1.7rem',
		fontWeight: 'bold',
		color: '#000000',
	},
	headerContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
		width: '100%',
	},
	reportButton: {
		background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
		padding: '0.5em 1.3em',
		borderRadius: 100,
	},
	reportButtonText: {
		color: '#FFFFFF',
		fontSize: '1rem',
		fontWeight: 'bold',
	},
	searchBarContainer: {
		display: 'flex',
		width: '100%',
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
		padding: '2% 5% 1% 0%',
	},
	searchBar: {
		width: '20%',
		borderRadius: 7,
	},
	searchBarCustom: {
		'& .MuiOutlinedInput-root': {
			'& fieldset': {
				borderColor: '#95989A',
			},
			'&:hover fieldset': {
				borderColor: '#95989A',
			},
			'&.Mui-focused fieldset': {
				borderColor: '#5E35B1',
			},
		},
	},
	searchBarIcon: {
		color: '#95989A',
		fontSize: '1.2em',
	},
	contentContainer: {
		display: 'flex',
		width: '100%',
		height: '70%',
		justifyContent: 'flex-end',
		paddingRight: '5%',
		paddingTop: '1%',
	},
	listContainer: {
		width: '65%',
	},
	historyImage: {
		width: '10%',
		height: 'auto',
		position: 'absolute',
		top: '40%',
		left: '20vw',
	},
	columnTitle: {
		color: '#263238',
		fontSize: '1rem',
		textTransform: 'capitalize',
	},
	statusContainerOne: {
		display: 'flex',
		borderRadius: 30,
		minWidth: '30%',
		backgroundColor: theme.palette.success[200],
		alignItems: 'center',
		padding: '3px 8px',
		justifyContent: 'center',
	},
	statusContainerTwo: {
		display: 'flex',
		borderRadius: 30,
		minWidth: '30%',
		backgroundColor: theme.palette.warning[200],
		alignItems: 'center',
		padding: '3px 8px',
		justifyContent: 'center',
	},
	statusTextOne: {
		fontSize: '0.9rem',
		color: theme.palette.success.main,
		fontWeight: 'bold',
	},
	statusTextTwo: {
		fontSize: '0.9rem',
		color: theme.palette.warning.main,
		fontWeight: 'bold',
	},
	tableRow: {
		'& > *': {
			borderBottom: 'unset',
		},
		display: 'flex',
		width: '100%',
	},
	headerCellButton: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		flex: 0.1,
	},
	headerCellPeriodo: {
		display: 'flex',
		alignItems: 'center',
		flex: 1,
	},
	headerCellEstado: {
		display: 'flex',
		justifyContent: 'center',
		flex: 0.5,
	},
	rowPlanillaId: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	rowPlanillaCantCotizantes: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	rowPlanillaTipo: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	rowPlanillaFecha: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	rowPlanillaAcciones: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	rowPlanillaValor: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	rowPlanillaEstado: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	rowIcon: {
		color: '#263238',
		fontSize: '2rem',
	},
	rowPlanillaIdText: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
	},
	rowPlanillaCantCotizantesText: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
	},
	rowPlanillaTipoText: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
	},
	rowPlanillaValorText: {
		color: theme.palette.common.black,
		fontSize: '0.8rem',
	},
	rowPlanillaEstadoTextOne: {
		color: '#2962FF',
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	rowPlanillaEstadoTextTwo: {
		color: '#FF495A',
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
	openedRow: {
		'&.MuiTableRow-root.MuiTableRow-hover:hover': {
			backgroundColor: theme.palette.primary[200],
			cursor: 'pointer',
		},
	},
	openedRowTitle: {
		color: theme.palette.primary.main,
	},
	openedRowIcon: {
		color: theme.palette.primary.main,
	},
	collapsibleRow: {
		backgroundColor: '#F9F9F9',
	},
	actionsCell: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-evenly',
	},
}));

export default HistoryLayout;
