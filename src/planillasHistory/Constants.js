export const PLANILLAS_HISTORY = 'Historial de planillas';
export const PAYMENT_REPORT = 'Reporte anual de pagos';
export const SEARCHBAR_PLACEHOLDER = 'Buscar por periodo';
export const COLUMNS = {
    PERIODO: 'Periodo',
    COTIZANTES: 'Cant. Cotizantes',
    VALOR: 'Valor',
    ESTADO: 'Estado'
}
export const PREVIOUS_PERIODS = 'Periodos anteriores';