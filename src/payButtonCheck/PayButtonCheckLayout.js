import React from 'react';

import { Typography, Grid, CircularProgress, Card, ButtonBase } from '@material-ui/core';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
// import logo from '../assets/img/logopilala.svg';
import { Logo } from '@bit/pilala.pilalalib.assets';
import MuiAlert from '@material-ui/lab/Alert';
import InfoTwoToneIcon from '@material-ui/icons/InfoTwoTone';
import { REDIRECTION_ALERT_TEXT, PAYMENT_INFO_ALERT_TEXT, UI_TEXT } from './Constants';

function Alert(props) {
	return <MuiAlert {...props} />;
}

const FeedBackLayout = (props) => {
	const classes = useStyles();

	return (
		<div className={classes.feedBackContainer}>
			{props.validationError ? (
				<FeedBackMessage {...props} />
			) : (
				[
					<CircularProgress key={classes.loader} className={classes.loader} size={40} />,

					<Typography key={props.feedBackMessage} component='h1' variant='h6'>
						{props.feedBackMessage}
					</Typography>,
				]
			)}
		</div>
	);
};

const FeedBackMessage = (props) => {
	return <Alert severity={props.feedBackSeverity}>{props.feedBackMessage}</Alert>;
};

const PayButtonCheckLayout = (props) => {
	const classes = useStyles();

	return (
		<Grid container direction='column' className={classes.container}>
			{/* {!props.validationStatus ? <FeedBackLayout {...props} /> : <PaymentSummary {...props} />} */}
			<PaymentSummary {...props} />
		</Grid>
	);
};

const PaymentSummary = (props) => {
	const classes = useStyles();
	return (
		<ThemeProvider theme={theme}>
			<Card className={classes.summaryCard}>
				<div className={classes.headerContainer}>
					<img src={Logo} alt='Logo Pilalá' className={classes.logo} />
				</div>

				<Typography className={classes.titleText} component='h1' variant='h6'>
					{UI_TEXT.PAYMENT_TITLE}
				</Typography>

				<div className={classes.dataContainer}>
					<Typography className={classes.dataMainText}>{UI_TEXT.OPERATOR_TITLE}</Typography>

					<Typography className={classes.dataResultText}>{UI_TEXT.OPERATOR_TEXT}</Typography>
				</div>
				<div className={classes.dataContainer}>
					<Typography className={classes.dataMainText}>{UI_TEXT.PAYROLL_NUMBER_TITLE}</Typography>

					<Typography className={classes.dataResultText}>
						{props.estadoPlanilla.numeroPlanilla}
					</Typography>
				</div>

				{props.validationStatus && [
					<React.Fragment key={props.estadoPlanilla.totalSinMora}>
						{props.estadoPlanilla.valorMora > 0 && (
							<div className={classes.dataContainer}>
								<Typography className={classes.dataMainText}>
									{UI_TEXT.TOTAL_WITHOUT_INTEREST_TITLE}
								</Typography>
								<Typography className={classes.dataResultText}>
									${' '}
									{props.estadoPlanilla.totalSinMora
										.toString()
										.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')}
								</Typography>
							</div>
						)}
					</React.Fragment>,
					<React.Fragment key={props.estadoPlanilla.valorMora}>
						{props.estadoPlanilla.valorMora > 0 && (
							<div className={classes.dataContainer}>
								<Typography className={classes.dataMainText}>
									{UI_TEXT.INTEREST_TOTAL_TITLE}
								</Typography>

								<Typography className={classes.dataResultText}>
									${' '}
									{props.estadoPlanilla.valorMora
										.toString()
										.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')}
								</Typography>
							</div>
						)}
					</React.Fragment>,
				]}

				<div className={classes.dataContainer}>
					<Typography className={classes.dataMainText}>{UI_TEXT.TOTAL_PAYMENT_TITLE}</Typography>

					<Typography className={classes.dataResultText}>
						{props.estadoPlanilla.totalPagar
							? '$ ' +
							  props.estadoPlanilla.totalPagar
									.toString()
									.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
							: ''}
					</Typography>
				</div>

				{!props.validationStatus ? (
					<FeedBackLayout {...props} />
				) : (
					<React.Fragment>
						<ButtonBase
							variant='contained'
							className={classes.saveButton}
							disableRipple
							disabled={props.getURLStatus}
							onClick={() => {
								props.getPaymentURL();
							}}
						>
							{props.getURLStatus ? (
								<CircularProgress key={classes.loaderButton} className={classes.loader} size={25} />
							) : (
								<Typography variant='subtitle2' className={classes.saveButtonText}>
									{UI_TEXT.PAY_BUTTON_LABEL}
								</Typography>
							)}
						</ButtonBase>
					</React.Fragment>
				)}
			</Card>

			{props.validationStatus && (
				<React.Fragment>
					<Alert
						className={classes.alertInfo}
						severity='info'
						icon={<InfoTwoToneIcon className={classes.alertInfoIcon} />}
					>
						<Typography variant='subtitle2' className={classes.alertInfoText}>
							{REDIRECTION_ALERT_TEXT}
						</Typography>
					</Alert>

					<Alert
						className={classes.secondaryAlertInfo}
						icon={<InfoTwoToneIcon className={classes.secondaryAlertInfoIcon} />}
					>
						<Typography variant='subtitle2' className={classes.secondaryAlertInfoTextTitle}>
							{PAYMENT_INFO_ALERT_TEXT.TITLE}
						</Typography>
						<Typography variant='subtitle2' className={classes.secondaryAlertInfoTextTitle}>
							{PAYMENT_INFO_ALERT_TEXT.FIRST_INFO_TITLE}
						</Typography>

						<Typography variant='subtitle2' className={classes.secondaryAlertInfoText}>
							{PAYMENT_INFO_ALERT_TEXT.FIRST_INTO_TEXT}
						</Typography>

						<Typography variant='subtitle2' className={classes.secondaryAlertInfoTextTitle}>
							{PAYMENT_INFO_ALERT_TEXT.SECOND_INFO_TITLE}
						</Typography>

						<Typography variant='subtitle2' className={classes.secondaryAlertInfoText}>
							{PAYMENT_INFO_ALERT_TEXT.SECOND_INFO_TEXT}
						</Typography>
					</Alert>
				</React.Fragment>
			)}
		</ThemeProvider>
	);
};

const useStyles = makeStyles({
	container: {
		width: '100%',
		// height: '100vh',
		display: 'flex',
		alignItems: 'center',
	},
	headerContainer: {
		marginTop: 15,
	},
	feedBackContainer: {
		marginTop: 20,
		display: 'flex',
		width: '100%',
		flexDirection: 'column',
		alignItems: 'center',
	},
	loader: {
		color: 'black',
	},
	loaderButton: {
		color: 'grey',
	},
	summaryCard: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'space-between',
		// height: '20%',
		width: '25%',
		marginTop: 20,
		padding: 20,
	},
	titleText: {
		fontWeight: 'bold',
		marginTop: 10,
	},

	dataContainer: {
		display: 'flex',
		justifyContent: 'space-between',
		height: '10%',
		width: '80%',
		backgroundColor: '#5E35B126',
		borderRadius: 5,
		alignItems: 'center',
		marginTop: 10,
	},
	dataMainText: {
		fontWeight: 'bold',
		margin: 10,
		color: '#5E35B1',
	},
	dataResultText: {
		fontWeight: 'bold',
		margin: 10,
	},

	summaryCardText: {
		fontWeight: 'bold',
		color: '#5E35B1',
	},
	saveButton: {
		// background: 'linear-gradient(90deg, rgba(94,53,177,1) 0%, rgba(123,31,162,1) 70%)',
		marginTop: 15,
		marginBottom: 15,
		// background: '#5E35B1',
		// background: 'linear-gradient(90deg, rgba(108,42,169,1) 52%, rgba(108,42,169,1) 100%, rgba(122,31,162,1) 100%)',
		backgroundColor: '#5E35B1',
		borderRadius: 22,
		textTransform: 'none',
		width: '80%',
		height: '40px',
	},
	saveButtonText: {
		color: 'white',
		fontWeight: 'bold',
	},
	warningAlert: {
		marginTop: 20,
	},
	alertInfo: {
		width: '25%',
		marginTop: 25,
	},
	alertInfoText: {
		color: '#2962FF',
		textAlign: 'left',
	},
	alertInfoIcon: {
		color: '#2962FF',
	},
	secondaryAlertInfoIcon: {
		color: '#95989A',
	},
	secondaryAlertInfo: {
		backgroundColor: '#ECEFF1',
		width: '25%',
		marginTop: 25,
		marginBottom: 15,
	},
	secondaryAlertInfoTextTitle: {
		color: '#95989A',
		textAlign: 'left',
		fontWeight: 'bold',
		marginBottom: 5,
	},
	secondaryAlertInfoText: {
		marginTop: 5,
		marginLeft: 15,
		color: '#95989A',
		textAlign: 'left',
	},
});

const theme = createMuiTheme({
	overrides: {
		MuiAlert: {
			standardError: {
				backgroundColor: '#FF495A26',
				color: '#FF495A',
				fontWeight: 'bold',
			},
			standardInfo: {
				backgroundColor: '#2962FF26',
			},
		},
	},
});

export default PayButtonCheckLayout;
