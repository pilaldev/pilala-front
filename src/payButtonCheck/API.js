import Endpoints from '@bit/pilala.pilalalib.endpoints';

export const getPlanillaEstado = async (numeroPlanilla, idType, idNumber) => {
	return new Promise(async (resolve, reject) => {
		// console.log('ksjda', numeroPlanilla, idType, idNumber);
		const requestOptions = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				numeroPlanilla,
				idType,
				idNumber,
			}),
		};

		await fetch(Endpoints.getEstadoPlanilla, requestOptions)
			.then((response) =>
				response.json().then((data) => ({
					data: data,
					status: response.status,
				}))
			)
			.then((result) => {
				if (result.status === 200) {
					resolve({
						message: 'El estado de la planilla fue obtenido correctamente',
						result: result.data,
					});
				} else {
					reject({
						message: 'Error consultando el estado de la Planilla',
						result: result.data,
					});
				}
			})
			.catch((error) => {
				console.log('Error al consultar el estado de la planilla', error);
				reject({
					message: 'El estado de la planilla no pudo ser consultado',
					result: error,
				});
			});
	});
};

export const getPaymentURL = async (numeroPlanilla, idType, idNumber) => {
	// console.log('PLANILLA', numeroPlanilla);
	return new Promise(async (resolve, reject) => {
		const requestOptions = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				numeroPlanilla,
				idType,
				idNumber,
			}),
		};

		await fetch(Endpoints.getPaymentURL, requestOptions)
			.then((response) =>
				response.json().then((data) => ({
					data: data,
					status: response.status,
				}))
			)
			.then((result) => {
				// console.log('Se obtuvo el link de pago de la Planilla', result);

				if (result.status === 200) {
					resolve({
						message: 'El link de pago de la planilla fue obtenido correctamente',
						result: result.data,
					});
				} else {
					reject({
						message: 'Error consultando la url de pago de la Planilla',
						result: result.data,
					});
				}
			})
			.catch((error) => {
				console.log('Error al consultar el link de pago de la planilla', error);
				reject({
					message: 'El link de pago de la planilla no pudo ser consultado',
					result: error,
				});
			});
	});
};
