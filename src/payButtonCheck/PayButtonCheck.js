import React, { Component } from 'react';
import { connect } from 'react-redux';
import PayButtonCheckLayout from './PayButtonCheckLayout';
import { payButtonCheckAction } from '../redux/Actions';
import { PAY_BUTTON_CHECK } from '../redux/ActionTypes';
import {
	FEEDBACK_MESSAGES,
	// ESTADOS_PLANILLA,
	// ESTADOS_PLANILLA_FEEDBACK_MESSAGES,
	// STATUS_CONFIRMATION,
	ESTADOS_PLANILLA_SEVERITY
} from './Constants';

import { getPlanillaEstado, getPaymentURL } from './API';

class PayButtonCheck extends Component {
	async componentDidMount() {
		this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_RESET_DATA, null);
		let params = this.getPayloadParams();

		if (params) {
			try {
				let estadoPlanilla = await getPlanillaEstado(params.NP, params.IT, params.IN);

				this.props.payButtonCheckAction(
					PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_ESTADO_PLANILLA,
					estadoPlanilla.result.data
				);

				this.props.payButtonCheckAction(
					PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_MESSAGE,
					estadoPlanilla.result.message
				);

				this.props.payButtonCheckAction(
					PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_SEVERITY,
					ESTADOS_PLANILLA_SEVERITY[estadoPlanilla.result.planillaStatus]
				);

				if (estadoPlanilla.result.paymentAvailable) {
					this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_STATUS, true);
				} else {
					this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_ERROR, true);
				}
			} catch (error) {
				if (error.message) {
					this.props.payButtonCheckAction(
						PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_MESSAGE,
						error.message
					);
				}
				this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_ERROR, true);
			}
		} else {
			this.props.payButtonCheckAction(
				PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_MESSAGE,
				FEEDBACK_MESSAGES.INCOMPLETE_PARAMS
			);
			this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_ERROR, true);
		}
	}

	componentWillUnmount() {
		this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_RESET_DATA, null);
	}

	getPaymentURL = async () => {
		this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_GET_URL_STATUS, true);
		// let win = this.openExternalWindow();
		let urlPago = await getPaymentURL(this.props.urlParams.NP, this.props.urlParams.IT, this.props.urlParams.IN);

		if (urlPago.result.url) {
			this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_GET_URL_STATUS, false);
			// win.location.href = urlPago.result.url;
			// win.focus();
			this.redirect(urlPago.result.url);
		} else {
			this.props.payButtonCheckAction(
				PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_SEVERITY,
				ESTADOS_PLANILLA_SEVERITY[urlPago.result.estadoPlanilla.planillaStatus]
			);
			this.props.payButtonCheckAction(
				PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_MESSAGE,
				urlPago.result.estadoPlanilla.message
			);
			this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_ERROR, true);
			this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_STATUS, false);
		}
	};

	openExternalWindow = (url) => {
		// let n = window.open(url, '_blank');
		// console.log('N', n);
		// if (n == null) {
		//  console.log('ENTRÓ POR EL NULL', n);
		//  window.open(url, '');
		//  // externalWindow.location.href = url;
		// }

		var id = new Date().getTime();
		var myWindow = window.open(
			'',
			id,
			'toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600,left = 240,top = 212'
		);

		return myWindow;
	};

	redirect = (url) => {
		const a = document.createElement('a');
		a.href = url;
		a.target = '_blank';
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	};

	getPayloadParams = () => {
		let params = {};
		window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, (m, key, value) => {
			params[key] = value.split('_').join(' ');
		});

		if (params.NP && params.IT && params.IN) {
			this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_URL_PARAMS, params);
			return params;
		} else {
			this.props.payButtonCheckAction(
				PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_FEEDBACK_MESSAGE,
				FEEDBACK_MESSAGES.INCOMPLETE_PARAMS
			);
			this.props.payButtonCheckAction(PAY_BUTTON_CHECK.PAY_BUTTON_CHECK_SET_VALIDATION_ERROR, true);
		}

		return null;
	};

	render() {
		const layoutProps = {
			validationStatus: this.props.validationStatus,
			validationError: this.props.validationError,
			feedBackMessage: this.props.feedBackMessage,
			urlParams: this.props.urlParams,
			estadoPlanilla: this.props.estadoPlanilla,
			paymentURL: this.props.paymentURL,
			feedBackSeverity: this.props.feedBackSeverity,
			getURLStatus: this.props.getURLStatus,
			getPaymentURL: this.getPaymentURL
		};

		return <PayButtonCheckLayout {...layoutProps} />;
	}
}

const mapStateToProps = (state) => {
	return {
		validationStatus: state.payButtonCheck.validationStatus,
		validationError: state.payButtonCheck.validationError,
		feedBackMessage: state.payButtonCheck.feedBackMessage,
		urlParams: state.payButtonCheck.urlParams,
		estadoPlanilla: state.payButtonCheck.estadoPlanilla,
		paymentURL: state.payButtonCheck.paymentURL,
		feedBackSeverity: state.payButtonCheck.feedBackSeverity,
		getURLStatus: state.payButtonCheck.getURLStatus
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		payButtonCheckAction: (actionType, value) => dispatch(payButtonCheckAction(actionType, value))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(PayButtonCheck);
