export const FEEDBACK_MESSAGES = {
	INCOMPLETE_PARAMS: 'El link de pago es inválido. Por favor solicite uno nuevo o comuniquese con el administrador',
	AUTHENTICATION_ERROR: 'Error de autenticación',
	AUTHORIZATION_ERROR: 'Error de autorización',
	GET_PLANILLA_STATUS_ERROR: 'Error consultando el estado de la planilla',
	GET_PLANILLA_PAYMENT_URL_ERROR: 'Error obteniendo el link de pago de la planilla',
	DEFAULT_ERROR: 'Se produjo un error de validación. Intente más tarde',
	VALIDATION_MESSAGE: 'Estamos validando la información'
};

export const ESTADOS_PLANILLA = {
	PAY_AVAILABLE: [ 'GU' ],
	PENDING_STATUS: [ 'PP', 'PD', 'PS', 'PP', 'PPC', 'PEA', 'PC' ],
	PAYED: [ 'OK' ],
	FAILED_STATUS: [ 'NA', 'ER', 'FL' ]
};

export const ESTADOS_PLANILLA_SEVERITY = {
	PENDING_STATUS: 'warning',
	PAYED: 'info',
	FAILED_STATUS: 'error'
};

export const STATUS_CONFIRMATION = {
	PAY_AVAILABLE: true,
	PENDING_STATUS: false,
	PAYED: false,
	FAILED_STATUS: false
};

export const ESTADOS_PLANILLA_FEEDBACK_MESSAGES = {
	PENDING_STATUS: 'La planilla se encuentra en estado pendiente de validación',
	PAYED: 'La planilla ya fue pagada',
	FAILED_STATUS: 'Hay un error con la planilla. Por favor revise de nuevo'
};

export const UI_TEXT = {
	PAYMENT_TITLE: 'Proceso de pago de Planilla PILA',
	OPERATOR_TITLE: 'Operador',
	OPERATOR_TEXT: 'ARUS S.A',
	PAYROLL_NUMBER_TITLE: 'N° Planilla',
	TOTAL_WITHOUT_INTEREST_TITLE: 'Total sin mora',
	INTEREST_TOTAL_TITLE: 'Total mora',
	TOTAL_PAYMENT_TITLE:'Total a pagar',
	PAY_BUTTON_LABEL: 'Iniciar pago con el operador'
};

export const REDIRECTION_ALERT_TEXT =
	'Al continuar el proceso serás dirigido al proceso del operador de información. El estado de tu planilla se verá reflejado en Pilalá una vez el operador confirme el pago de la planilla.';

export const PAYMENT_INFO_ALERT_TEXT = {
	TITLE: 'Ten presente: ',
	FIRST_INFO_TITLE: '1. 4:30 PM → 6:30 PM',
	FIRST_INTO_TEXT:
		'No se permite el pago de aportes debido a que en este horario ya los operadores de PILA se encuentran en proceso compensación.',
	SECOND_INFO_TITLE: '2.',
	SECOND_INFO_TEXT:
		'Pagar antes de tu fecha límite y evita ajustes de los totales por concepto de intereses al momento de realizar el pago'
};
