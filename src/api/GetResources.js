import { FAILED } from '../constants/API';
import Endpoints from '@bit/pilala.pilalalib.endpoints';

export const getResources = async () => {
	return new Promise(async (resolve, reject) => {
		await fetch(Endpoints.getResources, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: FAILED,
					message: 'Error ejecutanto la CF de departamentos y ciudades desde el front',
					error,
				});
			});
	});
};
