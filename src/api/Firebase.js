import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions"
import "firebase/storage"

const firebaseDevConfig = {
    apiKey: "AIzaSyAn78370920ijJ0wzxFAL1dcUxzApKZBsI",
    authDomain: "pilala-develop.firebaseapp.com",
    databaseURL: "https://pilala-develop.firebaseio.com",
    projectId: "pilala-develop",
    storageBucket: "pilala-develop.appspot.com",
    messagingSenderId: "511158834045",
    appId: "1:511158834045:web:7a1ef68a435015136c3214",
    measurementId: "G-X3Q51F4K3Q"
};

const firebaseQaConfig = {
    apiKey: "AIzaSyBuQrYUiatuhj8tMOr-CcONOyQyG0jWicQ",
    authDomain: "pilala-qa.firebaseapp.com",
    databaseURL: "https://pilala-qa.firebaseio.com",
    projectId: "pilala-qa",
    storageBucket: "pilala-qa.appspot.com",
    messagingSenderId: "253108382068",
    appId: "1:253108382068:web:31bc475a63884cdfde2421"
};

const firebaseProdConfig = {
    apiKey: "AIzaSyBEryuqP-aX1Q_o5bk5VFPbARmH3hDvrhA",
    authDomain: "pilala-b25de.firebaseapp.com",
    databaseURL: "https://pilala-b25de.firebaseio.com",
    projectId: "pilala-b25de",
    storageBucket: "pilala-b25de.appspot.com",
    messagingSenderId: "303295935990",
    appId: "1:303295935990:web:87db534f76e882961a03d7",
    measurementId: "G-GNQEFCTFYS"
};

if (process.env.REACT_APP_ENV) {
    if (process.env.REACT_APP_ENV === 'DEV') {
        firebase.initializeApp(firebaseDevConfig);
    }
    else if (process.env.REACT_APP_ENV === 'QA') {
        firebase.initializeApp(firebaseQaConfig);
    }
    else if (process.env.REACT_APP_ENV === 'PROD') {
        firebase.initializeApp(firebaseProdConfig);
    }
}
else {
    firebase.initializeApp(firebaseDevConfig);
}

export const FIREBASE_AUTH = firebase.auth();
export const FIRESTORE = firebase.firestore();
export const FUNCTIONS = firebase.functions();
export const STORAGE = firebase.storage();

export default firebase;