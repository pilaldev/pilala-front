import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default async ({
  idType,
  idNumber,
  companyId,
  user,
  pwd,
  userEmail,
  id,
}) => {
  let result;
  const data = JSON.stringify({
    idType,
    idNumber,
    companyId,
    user,
    pwd,
    userEmail,
    operator: null,
    id,
    update: false,
  });

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: data,
  };

  await fetch(Endpoints.connectCredentials, requestOptions)
    .then((response) => response.json())
    .then((apiResult) => {
      result = apiResult;
    })
    .catch((error) => {
      result = error;
      console.log('Error ejecutando fetch de conectar credenciales', error);
    });

  return result;
};
