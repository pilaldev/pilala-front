import Endpoints from '@bit/pilala.pilalalib.endpoints';

export default async (pilaFile, path, resource) => {
  let result;
  const data = JSON.stringify({
    pilaFile: pilaFile ?? false,
    pathFile: path,
    resource: resource ?? '',
  });

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: data,
  };

  await fetch(Endpoints.downloadFile, requestOptions)
    .then((response) => response.json())
    .then((apiResult) => {
      result = apiResult;
    })
    .catch((error) => {
      result = error;
      console.log('Error ejecutando fetch de descargar archivo', error);
    });

  return result;
};
