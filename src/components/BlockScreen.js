import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

const BlockScreen = () => {
    const classes = useStyles();
    const blockScreen = useSelector(state => state.navigation.blockScreen);

    return (
        <>
            {
                blockScreen ?
                    <div className={classes.container} />
                    : null
            }
        </>
    );
}

const useStyles = makeStyles({
    container: {
        backgroundColor: 'transparent',
        display: 'block',
        position: 'fixed',
        zIndex: 1000,
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
    }
});

export default BlockScreen;
