import React from 'react';
import { Step, StepConnector, StepLabel, Stepper as MuiStepper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import CheckIcon from '@material-ui/icons/Check';
import PropTypes from 'prop-types';

/**
 * Los stepper permiten indicar al usuario que una acción se compone de 2 o más pasos
 */
const Stepper = ({ steps, activeStep }) => {
    const classes = useStyles();
    const stepsArray = new Array(steps).fill();

    return (
        <div className={classes.container}>
            <MuiStepper
                activeStep={activeStep-1}
                connector={<StepConnector classes={{line: classes.line, root: classes.connectorRoot}} />}
                classes={{
                    root: classes.root
                }}
            >
                {
                    stepsArray.map((_, index) => (
                        <Step
                            key={index}
                            classes={{
                                root: classes.stepRoot,
                                horizontal: classes.horizontal
                            }}
                        >
                            <StepLabel StepIconComponent={StepperIcon} classes={{iconContainer: classes.iconContainer}}/>
                        </Step>
                    ))
                }
            </MuiStepper>
        </div>
    );
}

const StepperIcon = (props) => {
    const classes = useStyles();
    const { active, completed, icon } = props;
    
    return (
        <div
            className={classes.iconRoot}
        >
            <div
                className={clsx(classes.circle, {
                    [classes.active]: active,
                    [classes.completed]: completed,
                })}
            >
                {
                    completed ?
                    <CheckIcon className={classes.checkIcon} />
                    :
                    icon
                }
            </div>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        padding: 0,
        backgroundColor: 'transparent'
    },
    container: {
        width: '100%',
        backgroundColor: 'transparent'
    },
    stepRoot: {
        padding: '0px 14px'
    },
    line: {
        borderColor: theme.palette.primary[400],
        borderTopWidth: 1,
        borderRadius: 1,
    },
    iconRoot: {
        display: 'flex',
        height: 22,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circle: {
        width: 22,
        height: 22,
        borderRadius: '50%',
        border: '1px solid',
        color: theme.palette.primary[400],
        borderColor: theme.palette.primary[400],
        fontSize: '0.9rem',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    active: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white,
        borderWidth: 0,
    },
    completed: {
        zIndex: 1,
        color: theme.palette.common.white,
        borderWidth: 0,
        backgroundColor: theme.palette.primary.main,
    },
    checkIcon: {
        color: theme.palette.common.white,
        fontSize: '1rem'
    },
    iconContainer: {
        padding: 0
    },
    horizontal: {
        paddingRight: 0,
        paddingLeft: 0,
        backgroundColor: 'transparent'
    },
    connectorRoot: {
        margin: '0px 14px'
    }
}));

Stepper.propTypes = {
    /**
     * Número de steps que debe tener el stepper. Debe ser mayor o igual que 2
     */
    steps: PropTypes.number.isRequired,
    /**
     * Número del paso actual del stepper
     */
    activeStep: PropTypes.number.isRequired
}

Stepper.defaultProps = {
    steps: 2,
    activeStep: 1
}

export { Stepper as default };
