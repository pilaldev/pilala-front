import {
  ButtonBase,
  makeStyles,
  TextField,
  Typography,
} from '@material-ui/core';
import React, { useState } from 'react';
import Validate from 'card-validator';
import MaskedInput from 'react-text-mask';
import { useDispatch } from 'react-redux';
import { snackBarAction } from '../redux/Actions';
import { SNACKBAR } from '../redux/ActionTypes';
import { AVAILABLE_CARDS, CARD_LOGO } from '../utils/CreditCardAssets';

const MaskedCardNumberInput = (props) => {
  const { inputRef, mask, value, ...other } = props;
  return (
    <MaskedInput
      {...other}
      mask={mask}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      guide={false}
      value={value}
    />
  );
};

const MaskedCardDateInput = (props) => {
  const { inputRef, ...other } = props;
  return (
    <MaskedInput
      {...other}
      mask={[/\d/, /\d/, '/', /\d/, /\d/]}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      guide={false}
    />
  );
};

const CreditCard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [updatePaymentMethod, setUpdatePaymentMethod] = useState(false);
  const [validationResult, setValidationResult] = useState(null);
  const [cardValues, setCardValues] = useState({
    cardNumber: '',
    cardName: '',
    cardDate: '',
    cardCode: '',
  });
  const LOGO = validationResult
    ? validationResult.card
      ? AVAILABLE_CARDS.includes(validationResult.card.type)
        ? CARD_LOGO[validationResult.card.type]
        : CARD_LOGO['default']
      : CARD_LOGO['default']
    : CARD_LOGO['default'];

  const handleUpdatePaymentMethod = () => {
    setUpdatePaymentMethod(true);
  };

  const cancelUpdatePaymentMethod = () => {
    setUpdatePaymentMethod(false);
    setCardValues({
      cardNumber: '',
      cardName: '',
      cardDate: '',
      cardCode: '',
    });
    setValidationResult(null);
  };

  const getCardInputValue = (event) => {
    const { value, name } = event.target;

    setCardValues({
      ...cardValues,
      [name]: value,
    });
    if (name === 'cardNumber') {
      const cardValidation = Validate.number(value);
      setValidationResult(cardValidation);
    }
  };

  const getCardNumberMask = (cardNumber) => {
    const mask = [];
    const cardNumberValidation = Validate.number(cardNumber);
    for (let i = 0; i < cardNumber.length; i++) {
      mask.push(/\d/);
    }
    if (cardNumberValidation.card) {
      cardNumberValidation.card.gaps.forEach((gap, index) => {
        if (cardNumber.length > gap) {
          mask.splice(gap + index, 0, ' ');
        }
      });
    }
    return mask;
  };

  const validateCardFields = () => {
    const numberResult = Validate.number(cardValues.cardNumber);
    const nameResult = Validate.cardholderName(cardValues.cardName);
    const dateResult = Validate.expirationDate(cardValues.cardDate);
    const codeResult = Validate.cvv(cardValues.cardCode);

    return (
      numberResult.isValid &&
      nameResult.isValid &&
      dateResult.isValid &&
      codeResult.isValid
    );
  };

  const onSaveCreditCard = () => {
    const isValid = validateCardFields();
    if (isValid) {
      console.log('tarjeta válida');
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error con tu tarjeta',
          description: 'La tarjeta no es válida, por favor verifíquela.',
          color: 'error',
        })
      );
    }
  };

  return (
    <div className={classes.paymentMethodContainer}>
      <div className={classes.paymentMethodHeaderContainer}>
        <Typography className={classes.paymentMethodTitle}>
          Método de pago
        </Typography>
        <ButtonBase
          className={classes.paymentMethodButton}
          disabled={updatePaymentMethod}
          onClick={handleUpdatePaymentMethod}
        >
          <Typography className={classes.paymentMethodButtonText}>
            Actualizar método
          </Typography>
        </ButtonBase>
      </div>
      <div className={classes.paymentMethodNumberContainer}>
        <div className={classes.cardLogoContainer}>
          <img src={LOGO} className={classes.cardLogo} alt="Card logo" />
        </div>
        {updatePaymentMethod ? (
          <TextField
            name="cardNumber"
            value={cardValues.number}
            placeholder="Número de tarjeta"
            inputProps={{
              className: classes.cardInput,
              mask: getCardNumberMask,
              value: cardValues.number,
            }}
            InputProps={{
              inputComponent: MaskedCardNumberInput,
              classes: {
                underline: classes.input,
              },
            }}
            onChange={getCardInputValue}
            className={classes.cardNumberTextfield}
          />
        ) : (
          <Typography className={classes.paymentInfoText}>
            ************1234
          </Typography>
        )}
      </div>
      <div className={classes.paymentMethodInfoContainer}>
        {updatePaymentMethod ? (
          <TextField
            name="cardName"
            placeholder="Nombre titular"
            onChange={getCardInputValue}
            inputProps={{
              className: classes.cardInput,
            }}
            className={classes.cardNameTextfield}
            InputProps={{
              classes: {
                underline: classes.input,
              },
            }}
          />
        ) : (
          <Typography className={classes.paymentInfoText}>
            Nombre del titular
          </Typography>
        )}
        {updatePaymentMethod ? (
          <TextField
            name="cardDate"
            onChange={getCardInputValue}
            value={cardValues.cardDate}
            placeholder="Fecha Exp."
            inputProps={{
              className: classes.cardInput,
            }}
            InputProps={{
              inputComponent: MaskedCardDateInput,
              classes: {
                underline: classes.input,
              },
            }}
            className={classes.cardDateTextfield}
          />
        ) : (
          <Typography className={classes.paymentInfoDate}>MM/AA</Typography>
        )}
        {updatePaymentMethod ? (
          <TextField
            name="cardCode"
            placeholder={
              validationResult
                ? validationResult.card
                  ? validationResult.card.code.name
                  : ''
                : ''
            }
            onChange={getCardInputValue}
            inputProps={{
              className: classes.cardInput,
              maxLength: validationResult
                ? validationResult.card
                  ? validationResult.card.code.size
                  : 0
                : 0,
            }}
            InputProps={{
              classes: {
                underline: classes.input,
              },
            }}
            className={classes.cardCodeTextfield}
          />
        ) : null}
      </div>
      {updatePaymentMethod ? (
        <div className={classes.paymentMethodButtonsContainer}>
          <ButtonBase
            className={classes.paymentMethodCancelButton}
            onClick={cancelUpdatePaymentMethod}
          >
            <Typography className={classes.paymentMethodCancelButtonText}>
              Cancelar
            </Typography>
          </ButtonBase>
          <ButtonBase
            className={classes.paymentMethodSaveButton}
            onClick={onSaveCreditCard}
          >
            <Typography className={classes.paymentMethodSaveButtonText}>
              Guardar
            </Typography>
          </ButtonBase>
        </div>
      ) : null}
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  paymentMethodContainer: {
    height: '100%',
    backgroundColor: '#ECEFF1',
    padding: '0.5rem 0.8rem',
    borderRadius: 7,
    minWidth: '100%',
    maxWidth: '100%',
  },
  paymentMethodHeaderContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  paymentMethodTitle: {
    color: '#37474F',
    fontWeight: 'bold',
    [theme.breakpoints.up('xs')]: {
      fontSize: '0.8rem',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '0.9rem',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '1rem',
    },
  },
  paymentMethodButton: {
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    boxShadow: '0px 3px 6px #00000029',
    minHeight: '2rem',
    padding: '0px 0.4rem',
  },
  paymentMethodButtonText: {
    color: '#37474F',
    [theme.breakpoints.up('xs')]: {
      fontSize: '0.7rem',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '0.8rem',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '0.9rem',
    },
  },
  paymentMethodNumberContainer: {
    display: 'flex',
    alignItems: 'center',
    marginTop: '0.5rem',
  },
  paymentMethodInfoContainer: {
    display: 'flex',
    alignItems: 'center',
    marginTop: '0.5rem',
    justifyContent: 'space-between',
  },
  paymentMethodButtonsContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginTop: '1rem',
  },
  paymentMethodSaveButton: {
    borderRadius: 100,
    textTransform: 'none',
    backgroundColor: '#6A32B5',
    padding: '0px 1rem',
    minHeight: '2rem',
  },
  paymentMethodSaveButtonText: {
    color: '#FFFFFF',
    [theme.breakpoints.up('xs')]: {
      fontSize: '0.7rem',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '0.8rem',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '0.9rem',
    },
  },
  paymentMethodCancelButton: {
    borderRadius: 100,
    textTransform: 'none',
    padding: '0px 1rem',
    minHeight: '2rem',
  },
  paymentMethodCancelButtonText: {
    color: '#37474F',
    [theme.breakpoints.up('xs')]: {
      fontSize: '0.7rem',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '0.8rem',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '0.9rem',
    },
  },
  paymentInfoText: {
    color: '#37474F',
    [theme.breakpoints.up('xs')]: {
      fontSize: '0.8rem',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '1rem',
    },
  },
  paymentInfoDate: {
    color: '#37474F',
    fontSize: '1rem',
    [theme.breakpoints.up('xs')]: {
      fontSize: '0.8rem',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '1rem',
    },
  },
  cardLogoContainer: {
    minWidth: '2.5rem',
    maxWidth: '2.5rem',
    height: '2rem',
    marginRight: '1rem',
  },
  cardLogo: {
    width: '100%',
    height: 'auto',
  },
  cardInput: {
    [theme.breakpoints.up('xs')]: {
      fontSize: '0.7rem',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '0.9rem',
    },
  },
  cardNumberTextfield: {
    width: '100%',
  },
  cardNameTextfield: {
    width: '60%',
  },
  cardDateTextfield: {
    width: '20%',
  },
  cardCodeTextfield: {
    width: '10%',
  },
  input: {
    '&.MuiInput-underline:after': {
      borderBottom: '2px solid #6A32B5',
    },
    '&.MuiInput-underline:before': {
      borderBottom: '1px solid #707070',
    },
    '&.MuiInput-underline:hover:before': {
      borderBottom: '2px solid #6A32B5',
    },
  },
}));

export default CreditCard;
