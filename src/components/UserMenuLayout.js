import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
    MenuItem,
    Popper,
    Paper,
    ClickAwayListener,
    MenuList,
    Avatar,
    Typography,
    ButtonBase
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { MULTI_COMPANY_PANEL, MAIN_COMPANY, SUCURSAL } from './../constants/UserMenu';
import BusinessTwoToneIcon from '@material-ui/icons/BusinessTwoTone';
import ArrowDropDownCircleRoundedIcon from '@material-ui/icons/ArrowDropDownCircleRounded';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';

const UserMenuLayout = (props) => {

    const classes = useStyles();
    const [showMenu, setShowMenu] = useState(false);
    const [anchorEl, setAnchorEl] = useState(null);
    const sideTab = useSelector(state => state.navigation.sideTab);
    const companyName = useSelector(state => state.company.companyName);
    const userName = useSelector(state => state.currentUser.name);
    const avatarContent = userName.charAt(0);
    const currentCompany = useSelector(state => state.currentUser.companies.find((item) => item.id === state.currentUser.activeCompany));
    const isCompany = useSelector(state => state.currentUser.companiesInfo[state.currentUser.activeCompany] ? state.currentUser.companiesInfo[state.currentUser.activeCompany].isCompany : false);
    const sucursales = isCompany ? currentCompany.sucursales : [];
    const sucursalName = useSelector(state => state.currentUser.companiesInfo[state.currentUser.activeCompany] ? state.currentUser.companiesInfo[state.currentUser.activeCompany].sucursalName : '');
    const idCompany = useSelector(state => state.currentUser.companiesInfo[state.currentUser.activeCompany] ? state.currentUser.companiesInfo[state.currentUser.activeCompany].idCompany : '');
    const createdCompany = useSelector(state => state.currentUser.createdCompany);
    const featureFlags = useSelector(state => state.currentUser.featureFlags);

    const showUserMenu = (event) => {
        setAnchorEl(event.currentTarget);
        setShowMenu(!showMenu);
    }

    const closeUserMenu = () => {
        if (showMenu) {
            setShowMenu(!showMenu);
        }
    }

    return (
        <div className={classes.container}>
            {
                createdCompany ?
                    <>
                        {
                            sideTab === 'panelMultiempresa' ?
                                <>
                                    <ButtonBase className={classes.buttonContainer} onClick={showUserMenu}>
                                        <Avatar className={classes.avatar}>{avatarContent}</Avatar>
                                        <div className={classes.cardNameContainer}>
                                            <Typography className={classes.userName} noWrap>
                                                {userName}
                                            </Typography>
                                        </div>
                                        <ArrowDropDownCircleRoundedIcon className={classes.arrow} />
                                    </ButtonBase>
                                    <Popper open={showMenu} disablePortal placement='bottom-end' anchorEl={anchorEl} className={classes.popperContainer}>
                                        <Paper className={classes.paper}>
                                            <ClickAwayListener onClickAway={closeUserMenu}>
                                                <div>
                                                    <div className={classes.userInfoContainerOne}>
                                                        <Typography className={classes.userInfoNameOne} noWrap>
                                                            {userName}
                                                        </Typography>
                                                    </div>
                                                    <MenuList autoFocusItem={false}>
                                                        {
                                                            props.options.map((item, index) => {
                                                                return (
                                                                    <MenuItem onClick={item.action} key={index} className={classes.menuOption}>{item.label}</MenuItem>
                                                                );
                                                            })
                                                        }
                                                    </MenuList>
                                                </div>
                                            </ClickAwayListener>
                                        </Paper>
                                    </Popper>
                                </>
                                :
                                <>
                                    <ButtonBase className={classes.buttonContainer} onClick={showUserMenu}>
                                        <Avatar className={classes.companyAvatar}>
                                            <BusinessTwoToneIcon style={{ color: '#6060B2' }} />
                                        </Avatar>
                                        <div className={classes.controlCardContainer}>
                                            <div className={classes.cardNameContainer}>
                                                <Typography className={classes.companyName} noWrap>
                                                    {isCompany ? companyName : sucursalName}
                                                </Typography>
                                            </div>
                                            <div className={classes.aportanteTitleContainer}>
                                                <Typography className={classes.aportanteTitle}>
                                                    {isCompany ? MAIN_COMPANY : SUCURSAL}
                                                </Typography>
                                            </div>
                                        </div>
                                        <ArrowDropDownCircleRoundedIcon className={classes.arrow} />
                                    </ButtonBase>
                                    <Popper open={showMenu} disablePortal placement='bottom-end' anchorEl={anchorEl} className={classes.popperContainer}>
                                        <Paper className={classes.paper}>
                                            <ClickAwayListener onClickAway={closeUserMenu}>
                                                <div>
                                                    <div className={classes.userInfoContainerOne}>
                                                        <Typography className={classes.userInfoNameOne} noWrap>
                                                            {userName}
                                                        </Typography>
                                                        <Typography className={classes.companyNameOne} noWrap>
                                                            {isCompany ? companyName : sucursalName}
                                                        </Typography>
                                                    </div>
                                                    <MenuList autoFocusItem={false}>
                                                        <ButtonBase
                                                            className={classes.cardSucursalButton}
                                                            disableRipple
                                                            onClick={props.selectCompany.bind(this, idCompany)}
                                                        >
                                                            <Avatar className={classes.menuAvatar}>
                                                                <BusinessTwoToneIcon className={classes.avatarIcon} />
                                                            </Avatar>
                                                            <div className={classes.cardNamesContainer}>
                                                                <Typography className={classes.cardSucursalName}>
                                                                    {MAIN_COMPANY}
                                                                </Typography>
                                                                <div className={classes.cardCompanyNameContainer}>
                                                                    <Typography className={classes.cardCompanyName}>
                                                                        {companyName}
                                                                    </Typography>
                                                                </div>
                                                            </div>
                                                        </ButtonBase>
                                                        {
                                                            sucursales.map((sucursalItem, sucursalIndex) => (
                                                                <ButtonBase
                                                                    key={sucursalIndex}
                                                                    className={classes.cardSucursalButton}
                                                                    disableRipple
                                                                    onClick={props.selectCompany.bind(this, sucursalItem.idSucursal)}
                                                                >
                                                                    <Avatar className={classes.menuAvatar}>
                                                                        <BusinessTwoToneIcon className={classes.avatarIcon} />
                                                                    </Avatar>
                                                                    <div className={classes.cardNamesContainer}>
                                                                        <Typography className={classes.cardSucursalName}>
                                                                            {sucursalItem.sucursalName}
                                                                        </Typography>
                                                                        <div className={classes.cardCompanyNameContainer}>
                                                                            <Typography className={classes.cardCompanyName}>
                                                                                {companyName}
                                                                            </Typography>
                                                                        </div>
                                                                    </div>
                                                                </ButtonBase>
                                                            ))
                                                        }
                                                        {
                                                            props.options.map((item, index) => {
                                                                return (
                                                                    <MenuItem
                                                                        onClick={item.action}
                                                                        key={index}
                                                                        className={classes.menuOption}
                                                                    >
                                                                        {item.label}
                                                                    </MenuItem>
                                                                );
                                                            })
                                                        }
                                                    </MenuList>
                                                    {
                                                        featureFlags.ccMultiCompany ?
                                                        <MenuItem onClick={props.openMulticompanyPanel} className={classes.panelMultiempresaOption}>
                                                            {MULTI_COMPANY_PANEL}
                                                            <ArrowForwardRoundedIcon className={classes.horizontalArrow} />
                                                        </MenuItem>
                                                        :
                                                        null
                                                    }
                                                </div>
                                            </ClickAwayListener>
                                        </Paper>
                                    </Popper>
                                </>
                        }
                    </>
                    :
                    <>
                        <ButtonBase className={classes.buttonContainer} onClick={showUserMenu}>
                            <Avatar className={classes.avatar}>{avatarContent}</Avatar>
                            <div className={classes.cardNameContainer}>
                                <Typography className={classes.userName} noWrap>
                                    {userName}
                                </Typography>
                            </div>
                            <ArrowDropDownCircleRoundedIcon className={classes.arrow} />
                        </ButtonBase>
                        <Popper open={showMenu} disablePortal placement='bottom-end' anchorEl={anchorEl} className={classes.popperContainer}>
                            <Paper className={classes.paper}>
                                <ClickAwayListener onClickAway={closeUserMenu}>
                                    <div>
                                        <MenuList autoFocusItem={false}>
                                            {
                                                props.options.map((item, index) => {
                                                    return (
                                                        <MenuItem onClick={item.action} key={index} className={classes.menuOption}>{item.label}</MenuItem>
                                                    );
                                                })
                                            }
                                        </MenuList>
                                    </div>
                                </ClickAwayListener>
                            </Paper>
                        </Popper>
                    </>
            }
        </div>
    );
}

UserMenuLayout.propTypes = {
    options: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            action: PropTypes.func.isRequired
        }).isRequired
    ).isRequired
}

UserMenuLayout.defaultProps = {
    options: []
}

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        [theme.breakpoints.up('sm')]: {
            minWidth: '70%',
            maxWidth: '70%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '30%',
            maxWidth: '30%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '20%',
            maxWidth: '20%'
        }
    },
    buttonContainer: {
        display: 'flex',
        height: '100%',
        justifyContent: 'flex-start',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        [theme.breakpoints.up('sm')]: {
            minWidth: '95%',
            maxWidth: '95%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '85%',
            maxWidth: '85%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '80%',
            maxWidth: '80%'
        }
    },
    avatar: {
        border: '2px solid #C6CCD0',
        backgroundColor: '#F7F7F7',
        color: '#C6CCD0'
    },
    arrow: {
        color: '#78909C',
        fontSize: '0.8rem',
        marginLeft: '0.5rem',
        [theme.breakpoints.down('xs')]: {
            display: 'none'
        }
    },
    companyAvatar: {
        border: '2px solid #78909C',
        backgroundColor: '#FFFFFF',
        width: 40,
        height: 40
    },
    companyName: {
        color: '#78909C',
        fontWeight: 'bold',
        fontSize: '1.2em'
    },
    aportanteTitle: {
        textAlign: 'center',
        color: '#263238',
        fontSize: '0.7rem'
    },
    aportanteTitleContainer: {
        border: '1px solid #263238',
        borderRadius: 50,
        backgroundColor: '#26323826',
        [theme.breakpoints.up('sm')]: {
            minWidth: '45%',
            maxWidth: '45%'
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '60%',
            maxWidth: '60%'
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: '70%',
            maxWidth: '70%'
        },
        [theme.breakpoints.up('xl')]: {
            minWidth: '55%',
            maxWidth: '55%'
        }
    },
    userName: {
        color: '#78909C',
        fontWeight: 'bold',
        marginLeft: '0.5rem',
        [theme.breakpoints.up('sm')]: {
            fontSize: '0.8rem'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '0.9rem'
        }
    },
    cardNameContainer: {
        display: 'none',
        flex: 1,
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        [theme.breakpoints.up('sm')]: {
            display: 'flex'
        }
    },
    controlCardContainer: {
        display: 'none',
        flexDirection: 'column',
        flex: 1,
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        marginLeft: '0.5rem',
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            display: 'flex'
        }
    },
    panelMultiempresaOption: {
        display: 'flex',
        alignItems: 'center',
        color: '#2962FF',
        fontWeight: 'bold',
        fontSize: '1.1em',
        borderTop: '1px solid #00000029'
    },
    menuOption: {
        color: '#263238',
        fontSize: '0.9rem'
    },
    cardSucursalButton: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: '6%',
        marginLeft: '6%'
    },
    menuAvatar: {
        width: 40,
        height: 40,
        backgroundColor: '#FFFFFF',
        border: '1px solid #707070'
    },
    avatarIcon: {
        color: '#6060B2',
        fontSize: '1em'
    },
    cardNamesContainer: {
        display: 'flex',
        flexDirection: 'column',
        marginLeft: 8
    },
    cardSucursalName: {
        fontSize: '1em',
        fontWeight: 'bold',
        color: '#263238',
        textAlign: 'left'
    },
    cardCompanyNameContainer: {
        border: '1px solid #263238',
        borderRadius: 9,
        padding: '0px 8px 0px 8px',
        backgroundColor: '#26323826'
    },
    cardCompanyName: {
        textAlign: 'left',
        fontSize: '0.7em',
        color: '#263238'
    },
    popperContainer: {
        border: '1px solid #263238',
        borderRadius: 7,
        marginTop: '0.3rem',
        minWidth: 256
    },
    horizontalArrow: {
        marginLeft: '0.5rem'
    },
    userInfoContainerOne: {
        display: 'none',
        flexDirection: 'column',
        padding: '0.5rem 1rem 0rem 1rem',
        [theme.breakpoints.down('xs')]: {
            display: 'flex'
        }
    },
    userInfoNameOne: {
        color: '#78909C',
        fontWeight: 'bold',
        fontSize: '0.8rem',
        textAlign: 'left'
    },
    menuContainer: {
        padding: '0.5rem'
    },
    companyNameOne: {
        color: '#78909C',
        fontStyle: 'italic',
        fontSize: '0.8rem',
        textAlign: 'left'
    },
    paper: {
        borderRadius: 7
    }
}));

export default UserMenuLayout;