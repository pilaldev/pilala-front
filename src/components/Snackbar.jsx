import React from 'react';
import Snackbar from '@bit/pilala.pilalalib.components.snackbar';
import { useSelector, useDispatch } from 'react-redux';
import { snackBarAction } from '../redux/Actions';
import { SNACKBAR } from '../redux/ActionTypes';

export default () => {
  const dispatch = useDispatch();

  const open = useSelector((state) => state.snackBar.isOpened);
  const title = useSelector((state) => state.snackBar.title);
  const description = useSelector((state) => state.snackBar.description);
  const color = useSelector((state) => state.snackBar.color);

  const onClose = () => {
    dispatch(snackBarAction(SNACKBAR.CLOSE_SNACKBAR));
  };

  const onExited = () => {
    dispatch(snackBarAction(SNACKBAR.RESET_SNACKBAR));
  };

  return (
    <Snackbar
      open={open}
      color={color}
      title={title}
      description={description}
      onClose={onClose}
      onExited={onExited}
    />
  );
};
