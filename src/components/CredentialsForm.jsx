import React, { useCallback, useState } from 'react';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { Tooltip, Typography } from '@material-ui/core';
import TextInput from '@bit/pilala.pilalalib.components.textinput';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
import Button from '@bit/pilala.pilalalib.components.button';
import { useDispatch, useSelector } from 'react-redux';
import RegularExpressions from '../utils/RegularExpressions';
import { Select } from '.';
import { ID_VALUES } from '../signup/Constants';
import { useCredentialsFormStyles } from './styles';
import { credentialsModalAction } from '../redux/Actions';
import { CREDENTIALS_MODAL } from '../redux/ActionTypes';

const ID_PYMES = [
  {
    value: 'NI',
    display: 'NIT',
  },
];

export const OPERADOR_FORM_FIELDS = {
  OPERADOR: 'operador',
  COMPANY_ID_TYPE: 'companyIdType',
  COMPANY_ID_NUMBER: 'companyIdNumber',
  USER_TYPE: 'userType',
  USER_NUMBER: 'userNumber',
  PASSWORD: 'password',
  CREDENTIALS: 'credentials',
};

const CredentialsForm = React.memo(
  ({ disableCancelButton, disableConfirmButton, onCancel, onConfirm }) => {
    const classes = useCredentialsFormStyles();
    const dispatch = useDispatch();
    const [errors, setErrors] = useState({});
    const [showPassword, setShowPassword] = useState(false);
    const values = useSelector((state) => state.credentialsModal.values);
    const context = useSelector((state) => state.credentialsModal.context);
    const accountManager = useSelector(
      (state) => state.currentUser.accountManager
    );
    const isTextInputDisabled = context === 'credentials';
    const withHistory = context === 'credentials' && accountManager.length > 0;
    const enableCredentials = context === 'credentials' ?? false;
    const [showHistory, setShowHistory] = useState(withHistory);
    const credentialsOptions = [
      ...accountManager.map((item) => ({
        display: item.user,
        value: item,
      })),
      {
        display: '+ Agregar credenciales',
        value: -1,
      },
    ];

    const handleShowPassword = useCallback(() => {
      setShowPassword((prevState) => !prevState);
    }, []);

    const checkFields = () => {
      let fields = {};
      if (values[OPERADOR_FORM_FIELDS.COMPANY_ID_NUMBER] === '') {
        fields = {
          ...fields,
          [OPERADOR_FORM_FIELDS.COMPANY_ID_NUMBER]: true,
        };
      } else {
        fields = {
          ...fields,
          [OPERADOR_FORM_FIELDS.COMPANY_ID_NUMBER]: false,
        };
      }

      if (enableCredentials) {
        if (showHistory) {
          if (values[OPERADOR_FORM_FIELDS.CREDENTIALS] === 0) {
            fields = {
              ...fields,
              [OPERADOR_FORM_FIELDS.CREDENTIALS]: true,
            };
          } else {
            fields = {
              ...fields,
              [OPERADOR_FORM_FIELDS.CREDENTIALS]: false,
            };
          }
        } else {
          if (values[OPERADOR_FORM_FIELDS.USER_NUMBER] === '') {
            fields = {
              ...fields,
              [OPERADOR_FORM_FIELDS.USER_NUMBER]: true,
            };
          } else {
            fields = {
              ...fields,
              [OPERADOR_FORM_FIELDS.USER_NUMBER]: false,
            };
          }
          if (values[OPERADOR_FORM_FIELDS.PASSWORD] === '') {
            fields = {
              ...fields,
              [OPERADOR_FORM_FIELDS.PASSWORD]: true,
            };
          } else {
            fields = {
              ...fields,
              [OPERADOR_FORM_FIELDS.PASSWORD]: false,
            };
          }
        }
      }

      setErrors(fields);
      return Object.keys(fields).every((item) => fields[item] === false);
    };

    const onChange = (event) => {
      const data = event.target.value;
      const fieldName = event.target.name;

      switch (fieldName) {
        case OPERADOR_FORM_FIELDS.COMPANY_ID_NUMBER:
          if (data === '' || RegularExpressions.NUMBERS.test(data)) {
            dispatch(
              credentialsModalAction(
                CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES,
                { [OPERADOR_FORM_FIELDS.COMPANY_ID_NUMBER]: data }
              )
            );
          }
          break;

        case OPERADOR_FORM_FIELDS.COMPANY_ID_TYPE:
          dispatch(
            credentialsModalAction(CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES, {
              [OPERADOR_FORM_FIELDS.COMPANY_ID_TYPE]: data.value,
            })
          );
          break;

        case OPERADOR_FORM_FIELDS.USER_TYPE:
          dispatch(
            credentialsModalAction(CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES, {
              [OPERADOR_FORM_FIELDS.USER_TYPE]: data.value,
            })
          );
          break;

        case OPERADOR_FORM_FIELDS.CREDENTIALS:
          if (data.value !== -1) {
            dispatch(
              credentialsModalAction(
                CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES,
                {
                  [OPERADOR_FORM_FIELDS.OPERADOR]: data.value.operator.toLowerCase(),
                  [OPERADOR_FORM_FIELDS.USER_TYPE]: data.value.user.slice(0, 2),
                  [OPERADOR_FORM_FIELDS.USER_NUMBER]: data.value.user.slice(
                    2,
                    data.value.user.length
                  ),
                  [OPERADOR_FORM_FIELDS.CREDENTIALS]: data.value,
                }
              )
            );
          } else if (data.value === -1) {
            dispatch(
              credentialsModalAction(
                CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES,
                {
                  [OPERADOR_FORM_FIELDS.OPERADOR]: 'arus',
                  [OPERADOR_FORM_FIELDS.USER_TYPE]: 'CC',
                  [OPERADOR_FORM_FIELDS.USER_NUMBER]: '',
                  [OPERADOR_FORM_FIELDS.PASSWORD]: '',
                  [OPERADOR_FORM_FIELDS.CREDENTIALS]: data.value,
                }
              )
            );
            setShowHistory(false);
          }
          if (errors[fieldName]) {
            setErrors((prevState) => ({
              ...prevState,
              [fieldName]: false,
            }));
          }
          break;

        default:
          dispatch(
            credentialsModalAction(CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES, {
              [fieldName]: data,
            })
          );
          break;
      }
    };

    const onFocus = useCallback(
      (event) => {
        const fieldName = event.target.name;
        if (errors[fieldName]) {
          setErrors((prevState) => ({
            ...prevState,
            [fieldName]: false,
          }));
        }
      },
      [errors]
    );

    const backToHistory = useCallback(() => {
      setShowHistory(true);
      dispatch(
        credentialsModalAction(CREDENTIALS_MODAL.SAVE_CREDENTIALS_VALUES, {
          [OPERADOR_FORM_FIELDS.CREDENTIALS]: 0,
        })
      );
    }, [dispatch]);

    const handleConfirm = (e) => {
      e.preventDefault();
      if (checkFields()) {
        onConfirm(values);
      }
    };

    const handleCancel = useCallback(
      (e) => {
        e.preventDefault();
        onCancel();
      },
      [onCancel]
    );

    return (
      <form>
        <Typography className={classes.checkNitTitle} variant="h4">
          Confirma tu NIT
        </Typography>
        <Typography className={classes.subtitle} variant="subtitle2">
          Con esto esperamos automatizar la creación de tu empresa en Pilalá
        </Typography>
        <div className={classes.form}>
          <div className={classes.formRowOne}>
            <div className={classes.idTypeContainer}>
              <Select
                options={ID_PYMES}
                fullWidth
                value={values[OPERADOR_FORM_FIELDS.COMPANY_ID_TYPE]}
                name={OPERADOR_FORM_FIELDS.COMPANY_ID_TYPE}
                onChange={onChange}
                disabled={isTextInputDisabled}
              />
            </div>
            <div className={classes.idNumberContainer}>
              <TextInput
                fullWidth
                placeholder="Nit de tu empresa"
                value={values[OPERADOR_FORM_FIELDS.COMPANY_ID_NUMBER]}
                name={OPERADOR_FORM_FIELDS.COMPANY_ID_NUMBER}
                helperText="Sin dígito de verificación"
                onChange={onChange}
                error={errors[OPERADOR_FORM_FIELDS.COMPANY_ID_NUMBER] ?? false}
                onFocus={onFocus}
                disabled={isTextInputDisabled}
              />
            </div>
          </div>
          {enableCredentials && (
            <div>
              {showHistory ? (
                <Select
                  options={credentialsOptions}
                  fullWidth
                  label="Seleccionar credenciales"
                  value={values[OPERADOR_FORM_FIELDS.CREDENTIALS]}
                  name={OPERADOR_FORM_FIELDS.CREDENTIALS}
                  onChange={onChange}
                  error={errors[OPERADOR_FORM_FIELDS.CREDENTIALS] ?? false}
                />
              ) : (
                <div>
                  <div className={classes.formRowTwo}>
                    <div className={classes.idTypeContainer}>
                      <Select
                        label="Usuario"
                        options={ID_VALUES}
                        fullWidth
                        value={values[OPERADOR_FORM_FIELDS.USER_TYPE]}
                        name={OPERADOR_FORM_FIELDS.USER_TYPE}
                        onChange={onChange}
                      />
                    </div>
                    <div className={classes.idNumberContainer}>
                      <Tooltip
                        title={
                          <>
                            {
                              'Este usuario debe tener permisos de administrador en el aportante. '
                            }
                            {
                              <b>
                                <Typography
                                  className={classes.tooltipLink}
                                  onClick={() => window.open('')}
                                >
                                  ¿Cómo asignar permisos?
                                </Typography>
                              </b>
                            }
                          </>
                        }
                        placement="top"
                        arrow
                        interactive
                        classes={{
                          tooltip: classes.tooltip,
                          arrow: classes.tooltipArrow,
                        }}
                      >
                        <div className={classes.tooltipChildren}>
                          <TextInput
                            fullWidth
                            placeholder="Usuario de tu operador"
                            value={values[OPERADOR_FORM_FIELDS.USER_NUMBER]}
                            name={OPERADOR_FORM_FIELDS.USER_NUMBER}
                            onChange={onChange}
                            error={
                              errors[OPERADOR_FORM_FIELDS.USER_NUMBER] ?? false
                            }
                            InputProps={{
                              autoComplete: 'username',
                            }}
                            onFocus={onFocus}
                          />
                        </div>
                      </Tooltip>
                    </div>
                  </div>
                  <div className={classes.formRowThree}>
                    {withHistory && (
                      <div className={classes.backTextContainer}>
                        <Typography
                          variant="subtitle2"
                          className={classes.selectCredentials}
                          onClick={backToHistory}
                        >
                          Seleccionar credenciales
                        </Typography>
                      </div>
                    )}
                    <div className={classes.passwordContainer}>
                      <TextInput
                        fullWidth
                        label="Contraseña"
                        value={values[OPERADOR_FORM_FIELDS.PASSWORD]}
                        startIcon={<VpnKeyIcon />}
                        placeholder="Contraseña de tu operador"
                        name={OPERADOR_FORM_FIELDS.PASSWORD}
                        onChange={onChange}
                        type={showPassword ? 'text' : 'password'}
                        error={errors[OPERADOR_FORM_FIELDS.PASSWORD] ?? false}
                        onFocus={onFocus}
                        endIcon={
                          showPassword ? (
                            <IconButton
                              onClick={handleShowPassword}
                              size="small"
                              buttoncolor="grey"
                              filled={false}
                            >
                              <VisibilityIcon />
                            </IconButton>
                          ) : (
                            <IconButton
                              onClick={handleShowPassword}
                              size="small"
                              buttoncolor="grey"
                              filled={false}
                            >
                              <VisibilityOffIcon />
                            </IconButton>
                          )
                        }
                        InputProps={{
                          autoComplete: 'current-password',
                        }}
                      />
                    </div>
                  </div>
                </div>
              )}
            </div>
          )}
          <div className={classes.buttonsContainer}>
            <Button
              disabled={disableConfirmButton || disableCancelButton}
              onClick={handleCancel}
              loading={disableCancelButton}
              variant="outlined"
            >
              Cancelar
            </Button>
            <div className={classes.confirmButton}>
              <Button
                onClick={handleConfirm}
                type="submit"
                disabled={disableCancelButton || disableConfirmButton}
                loading={disableConfirmButton}
              >
                Continuar
              </Button>
            </div>
          </div>
        </div>
      </form>
    );
  }
);

export default CredentialsForm;
