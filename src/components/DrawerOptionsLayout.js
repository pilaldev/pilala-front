import React from 'react';
import { withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import { ButtonBase, Typography } from '@material-ui/core';
import ExploreTwoToneIcon from '@material-ui/icons/ExploreTwoTone';
import AssignmentTwoToneIcon from '@material-ui/icons/AssignmentTwoTone';
import ReceiptTwoToneIcon from '@material-ui/icons/ReceiptTwoTone';
import PeopleAltTwoToneIcon from '@material-ui/icons/PeopleAltTwoTone';
import StoreTwoToneIcon from '@material-ui/icons/StoreTwoTone';
import SettingsTwoToneIcon from '@material-ui/icons/SettingsTwoTone';
import DomainTwoToneIcon from '@material-ui/icons/DomainTwoTone';
import { useSelector } from 'react-redux';

const DrawerOptionsLayout = (props) => {

    const classes = useStyles();
    const sideTab = useSelector(state => state.navigation.sideTab);
    const activePanelMultiempresa = useSelector(state => state.navigation.activePanelMultiempresa);
    const createdCompany = useSelector(state => state.currentUser.createdCompany);
    const featureFlags = useSelector(state => state.currentUser.featureFlags);

    const options = [
        {
            id: 'panelMultiempresa',
            callback: () => {
                props.history.push(`/${props.match.params.userId}/console/multicompanyPanel`);
            },
            label: 'Panel multiempresa',
            icon: <DomainTwoToneIcon className={classes.optionIcon} />
        },
        {
            id: 'panelControl',
            callback: () => {
                props.history.push(`/${props.match.params.userId}/console/controlPanel`);
            },
            label: 'Panel de control',
            icon: <ExploreTwoToneIcon className={classes.optionIcon} />
        },
        {
            id: 'liquidarPila',
            callback: () => {
                props.history.push(`/${props.match.params.userId}/console/liquidarPila/1`);
            },
            label: 'Liquidar PILA',
            icon: <AssignmentTwoToneIcon className={classes.optionIcon} />
        },
        {
            id: 'historialPago',
            callback: () => {
                props.history.push(`/${props.match.params.userId}/console/payHistory`);
            },
            label: 'Historial de pago',
            icon: <ReceiptTwoToneIcon className={classes.optionIcon} />
        },
        {
            id: 'cotizantes',
            callback: () => {
                props.history.push(`/${props.match.params.userId}/console/cotizantes`);
            },
            label: 'Cotizantes',
            icon: <PeopleAltTwoToneIcon className={classes.optionIcon} />
        },
        {
            id: 'compañia',
            callback: () => {
                props.history.push(`/${props.match.params.userId}/console/company`);
            },
            label: 'Mi compañía',
            icon: <StoreTwoToneIcon className={classes.optionIcon} />
        },
        {
            id: 'configuracion',
            callback: () => {
                props.history.push(`/${props.match.params.userId}/console/configuration`);
            },
            label: 'Configuración',
            icon: <SettingsTwoToneIcon className={classes.optionIcon} />
        },
        // {
        //     id: 'centroAyuda',
        //     callback: () => {
        //         props.history.push(`/${props.match.params.userId}/console/helpCenter`);
        //     },
        //     label: 'Centro de ayuda',
        //     icon: <SpaTwoToneIcon className={classes.optionIcon} />
        // }
    ];

    return (
        <div className={classes.container}>
            {
                activePanelMultiempresa ?
                    <>
                        {
                            options.map((item) => {
                                if (item.id === 'configuracion' || item.id === 'centroAyuda' || item.id === 'panelMultiempresa') {
                                    return (
                                        <ButtonBase
                                            className={classes.buttonContainer}
                                            key={item.id}
                                            id={item.id}
                                            onClick={item.callback}
                                        >
                                            <div className={classes.iconContainer}>{item.icon}</div>
                                            <Typography
                                                style={{
                                                    fontWeight: item.id === sideTab ? 'bold' : 'normal'
                                                }}
                                                className={classes.tabName}
                                            >
                                                {item.label}
                                            </Typography>
                                        </ButtonBase>
                                    );
                                }
                                return null;
                            })
                        }
                    </>
                    :
                    !createdCompany ?
                        <>
                            {
                                options.map((item) => {
                                    if (item.id === 'compañia' || item.id === 'configuracion' || item.id === 'centroAyuda') {
                                        return (
                                            <ButtonBase
                                                className={classes.buttonContainer}
                                                key={item.id}
                                                id={item.id}
                                                onClick={item.callback}
                                            >
                                                <div className={classes.iconContainer}>{item.icon}</div>
                                                <Typography
                                                    style={{
                                                        fontWeight: item.id === sideTab ? 'bold' : 'normal'
                                                    }}
                                                    className={classes.tabName}
                                                >
                                                    {item.label}
                                                </Typography>
                                            </ButtonBase>
                                        );
                                    }
                                    return null;
                                })
                            }
                        </>
                        :
                        <>
                            {
                                options.map((item) => {
                                    if (item.id !== 'panelMultiempresa') {
                                        if (item.id === 'historialPago' && featureFlags.csPayHistory) {
                                            return (
                                                <ButtonBase
                                                    className={classes.buttonContainer}
                                                    key={item.id}
                                                    id={item.id}
                                                    onClick={item.callback}
                                                >
                                                    <div className={classes.iconContainer}>{item.icon}</div>
                                                    <Typography
                                                        style={{
                                                            fontWeight: item.id === sideTab ? 'bold' : 'normal'
                                                        }}
                                                        className={classes.tabName}
                                                    >
                                                        {item.label}
                                                    </Typography>
                                                </ButtonBase>
                                            );
                                        }
                                        else if (item.id !== 'historialPago') {
                                            return (
                                                <ButtonBase
                                                    className={classes.buttonContainer}
                                                    key={item.id}
                                                    id={item.id}
                                                    onClick={item.callback}
                                                >
                                                    <div className={classes.iconContainer}>{item.icon}</div>
                                                    <Typography
                                                        style={{
                                                            fontWeight: item.id === sideTab ? 'bold' : 'normal'
                                                        }}
                                                        className={classes.tabName}
                                                    >
                                                        {item.label}
                                                    </Typography>
                                                </ButtonBase>
                                            );
                                        }
                                    }
                                    return null;
                                })
                            }
                        </>
            }
        </div>
    );
}

const useStyles = makeStyles({
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        height: 50,
        paddingLeft: '1rem'
    },
    optionIcon: {
        color: '#263238',
        fontSize: '1.3rem'
    },
    container: {
        backgroundColor: 'transparent',
        display: 'flex',
        flexDirection: 'column'
    },
    iconContainer: {
        display: 'flex',
        justifyContent: 'center',
        marginRight: '0.7rem'
    },
    tabName: {
        color: '#263238',
        fontSize: '1rem'
    }
})

export default withRouter(DrawerOptionsLayout);