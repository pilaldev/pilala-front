import React from 'react';
import { PulseBubble } from '@bit/pilala.pilalalib.components.loaders';
import { Typography, makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  message: {
    fontSize: '1.2rem',
    color: '#6A32B5',
    fontWeight: 'bold',
  },
});

const Loader = ({ message }) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <PulseBubble size={135} color="#6A32B5" />
      <div className="status_message">
        <Typography className={classes.message}>{message}</Typography>
      </div>
    </div>
  );
};

Loader.propTypes = {
  message: PropTypes.string.isRequired,
};

export default React.memo(Loader);
