import React from 'react';
import { Dialog, DialogContent, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
import CloseIcon from '@material-ui/icons/Close';

export default ({ title, children, onCloseModal, ...props }) => {
	const classes = useStyles();

	return (
		<Dialog
			{...props}
			PaperProps={{
				className: classes.paperContainer,
			}}
			BackdropProps={{
				className: classes.backdrop,
			}}
			fullWidth
		>
			<DialogContent className={classes.dialogContent}>
				<Grid
					container
					direction='row'
					justify='space-between'
					alignItems='center'
					className={classes.rowOne}
				>
					<Typography className={classes.dialogTitle} variant='h5'>
						{title}
					</Typography>
					<IconButton filled={false} size='small' onClick={onCloseModal}>
						<CloseIcon />
					</IconButton>
				</Grid>
				{children}
			</DialogContent>
		</Dialog>
	);
};

const useStyles = makeStyles((theme) => ({
	backdrop: {
		backgroundColor: theme.modalNovedadBackground,
	},
	paperContainer: {
		backgroundColor: theme.palette.common.white,
		display: 'flex',
		flex: 1,
		borderRadius: 10,
		padding: theme.spacing(4),
		boxShadow: 'none',
	},
	dialogContent: {
		padding: 0,
		'&:first-child': {
			padding: 0,
		},
	},
	dialogTitle: {
		textAlign: 'left',
		fontWeight: 'bold',
		color: theme.palette.common.black,
	},
}));
