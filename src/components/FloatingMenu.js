import React from 'react';
import PropTypes from 'prop-types';
import { FloatingCard, MenuItem } from '.';
import { Typography } from '@material-ui/core';

/**
 * Los floating menu permiten crear un menú flotante con una lista de opciones
 */
const FloatingMenu = ({ position, options, anchorEl, onClose }) => {
    return (
        <FloatingCard anchorEl={anchorEl} position={position} onClose={onClose}>
            {
                options.map((item, index) => (
                    <MenuItem key={index} onClick={item.action}>
                        <Typography variant='subtitle2'>
                            {item.label}
                        </Typography>
                    </MenuItem>
                ))
            }
        </FloatingCard>
    );
}

FloatingMenu.propTypes = {
    /**
     * Array de opciones que se listarán en el menu
     */
    options: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired
    })).isRequired,
    /**
     * Posición en la que debe aparecer la card
     */
    position: PropTypes.string,
    /**
     * Elemento al que se debe ligar el menu
     */
    anchorEl: PropTypes.any
}

FloatingMenu.defaultProps = {
    position: 'right'
}

export { FloatingMenu as default };