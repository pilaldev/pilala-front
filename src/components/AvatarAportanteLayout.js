import React from 'react';
import {
    Avatar,
    Typography,
    ButtonBase
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import BusinessTwoToneIcon from '@material-ui/icons/BusinessTwoTone';


const AvatarAportanteLayout = (props) => {

    const classes = useStyles();

    return (
        <ButtonBase
            className={classes.buttonContainer}
            disableRipple={true}
            onClick={props.action}
        >
            <Avatar
                className={classes.avatar}
                style={{
                    border: `2px solid ${props.index === -1 ? '#6060B2' : '#707070'}`
                }}
            >
                {
                    props.image ?
                    null
                    :
                    <BusinessTwoToneIcon className={classes.avatarIcon}/>
                }
            </Avatar>
            <div className={classes.nameContainer}>
                <Typography
                    noWrap
                    className={classes.nameText}
                    style={{
                        color: `${props.index === -1 ? '#263238' : '#707070'}`
                    }}
                >
                    {props.name}
                </Typography>
            </div>
        </ButtonBase>
    );
}

const useStyles = makeStyles({
    buttonContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        height: 'auto'
    },
    nameContainer: {
        overflow: 'hidden',
        textOverflow: "ellipsis",
        width: 110
    },
    nameText: {
        fontSize: '1em',
        fontWeight: 'bold',
        marginTop: 5,
        textTransform: 'capitalize'
    },
    avatar: {
        width: '3em',
        height: '3em',
        backgroundColor: '#FFFFFF'
    },
    avatarIcon: {
        color: '#6060B2',
        fontSize: '2em'
    }
});

export default AvatarAportanteLayout;
