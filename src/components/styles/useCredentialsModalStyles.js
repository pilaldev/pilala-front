import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  checkNitTitle: {
    fontWeight: 'bold',
    color: theme.palette.common.black,
    textAlign: 'left',
    marginBottom: theme.spacing(3),
  },
  checkNitCard: {
    display: 'flex',
    borderRadius: 10,
    boxShadow: 'none',
    width: '100%',
    padding: theme.spacing(4, 6),
    flexDirection: 'column',
    minWidth: '90%',
    maxWidth: '90%',
    [theme.breakpoints.up('sm')]: {
      minWidth: '450px',
      maxWidth: '450px',
    },
  },
  confirmButton: {
    marginLeft: 0,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
    },
  },
  buttonsContainer: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginTop: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      justifyContent: 'flex-end',
    },
  },
}));
