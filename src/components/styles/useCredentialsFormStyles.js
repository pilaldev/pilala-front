import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  emailInput: {
    marginBottom: theme.spacing(3),
  },
  formRowOne: {
    display: 'flex',
    marginBottom: '1rem',
    flexDirection: 'column',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
    },
  },
  formRowTwo: {
    display: 'flex',
    marginBottom: '1rem',
    flexDirection: 'column',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      alignItems: 'flex-end',
    },
  },
  formRowThree: {
    display: 'flex',
    marginTop: theme.spacing(3),
    alignItems: 'flex-end',
  },
  idTypeContainer: {
    display: 'flex',
    flex: 0.4,
    marginRight: theme.spacing(3),
    [theme.breakpoints.down('xs')]: {
      marginRight: 0,
    },
  },
  idNumberContainer: {
    display: 'flex',
    flex: 1,
    [theme.breakpoints.down('xs')]: {
      marginTop: '1rem',
    },
  },
  passwordContainer: {
    display: 'flex',
    flex: 1,
  },
  tooltip: {
    backgroundColor: theme.palette.common.black,
    borderRadius: 8,
  },
  tooltipArrow: {
    color: theme.palette.common.black,
  },
  tooltipLink: {
    color: theme.palette.common.white,
    fontWeight: 'bold',
    textDecoration: 'underline',
    fontSize: '0.7rem',
    '&:hover': {
      cursor: 'pointer',
    },
  },
  tooltipChildren: {
    width: '100%',
  },
  operadoresContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(3),
  },
  operadorButton: {
    width: '9rem',
    display: 'flex',
    justifyContent: 'flex-start',
    padding: theme.spacing(1),
    border: '1px solid',
    borderColor: theme.palette.grey[400],
    borderRadius: 8,
    marginRight: theme.spacing(3),
  },
  operadorButtonSelected: {
    boxShadow: theme.elevations[2],
    borderColor: theme.palette.primary.main,
  },
  selectedIcon: {
    color: theme.palette.primary.main,
    fontSize: '1.2rem',
    position: 'absolute',
    top: 5,
    right: 5,
  },
  unselectedIcon: {
    color: theme.palette.grey[400],
    fontSize: '1.2rem',
    position: 'absolute',
    top: 5,
    right: 5,
  },
  lastOperadorButton: {
    marginRight: 0,
  },
  messageContainer: {
    backgroundColor: theme.palette.primary[200],
    width: '100%',
    display: 'flex',
    borderRadius: theme.spacing(1),
    marginTop: theme.spacing(1),
    padding: theme.spacing(1, 2),
    alignItems: 'center',
    marginBottom: theme.spacing(2),
  },
  message: {
    color: theme.palette.primary.main,
    marginLeft: theme.spacing(1),
  },
  confirmButton: {
    marginLeft: 0,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
    },
  },
  buttonsContainer: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginTop: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      justifyContent: 'flex-end',
    },
  },
  backTextContainer: {
    display: 'flex',
    flex: 0.4,
    marginRight: theme.spacing(3),
    [theme.breakpoints.down('xs')]: {
      marginRight: 0,
    },
  },
  selectCredentials: {
    color: theme.palette.primary.main,
    fontWeight: 'bold',
    '&:hover': {
      cursor: 'pointer',
      textDecoration: 'underline',
    },
  },
  checkNitTitle: {
    fontWeight: 'bold',
    color: theme.palette.common.black,
    textAlign: 'left',
    marginBottom: theme.spacing(3),
  },
  subtitle: {
    color: theme.palette.common.black,
    textAlign: 'left',
    marginBottom: theme.spacing(3),
  },
}));
