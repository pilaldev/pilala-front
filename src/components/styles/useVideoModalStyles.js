import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.common.white,
    borderRadius: 10,
    padding: theme.spacing(4),
  },
  iframeContainer: {
    marginTop: theme.spacing(4),
  },
  video: {
    borderWidth: 0,
  },
  title: {
    textAlign: 'left',
    fontWeight: 'bold',
    color: theme.palette.common.black,
  },
}));
