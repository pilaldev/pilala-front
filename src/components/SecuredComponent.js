import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const withAuthorization = ({ component: Component, auth, ...rest }) => {
	return auth ? <Route {...rest} component={Component} /> : <Redirect to='/' />;
};

export default withAuthorization;
