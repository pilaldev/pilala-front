import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import NavBarLayout from './NavBarLayout';
import SideBarLayout from './SideBarLayout';
import UserMenuLayout from './UserMenuLayout';
import ContentLayout from './ContentLayout';
import Company from '../company/Company';
import Cotizantes from './../cotizantes/Cotizantes';
import CotizantesDetail from './../cotizantes/cotizantesDetail/CotizantesDetail';
import PILA from './../pila/PILA';
import PanelMultiempresa from './../panelMultiempresa/PanelMultiempresa';
import ControlPanel from './../controlPanel/ControlPanel';
import History from './../planillasHistory/History';
import PlanillaDetail from '../planillaDetail/PlanillaDetail';
import Settings from './../settings/Settings';

const AppContainerLayout = (props) => {
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = useState(false);
  const history = useHistory();

  const handleDrawer = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className={classes.container}>
      <NavBarLayout
        openMobileDrawer={handleDrawer}
        userCard={<UserMenuLayout {...props.userCardProps} />}
      />
      <div className={classes.secondContainer}>
        <SideBarLayout
          mobileDrawer={mobileOpen}
          openMobileDrawer={handleDrawer}
        />
        <div className={classes.contentContainer}>
          <ContentLayout>
            <ControlPanel history={history} />
            <PILA />
            <History history={history} />
            <Cotizantes />
            <Company history={history} />
            <Settings history={history} />
            <div />
            <CotizantesDetail />
            <PanelMultiempresa history={history} />
            <PlanillaDetail></PlanillaDetail>
          </ContentLayout>
        </div>
      </div>
    </div>
  );
};

AppContainerLayout.propTypes = {
  userCardProps: PropTypes.object.isRequired,
};

AppContainerLayout.defaultProps = {
  userCardProps: {},
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  secondContainer: {
    display: 'flex',
    flex: 1,
    marginTop: 75,
    width: '100%',
  },
  sideBarContainer: {
    width: 220,
  },
  contentContainer: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#FFFFFF',
    width: '100%',
    minHeight: '100%',
    height: ' fit-content',
  },
}));

export default AppContainerLayout;
