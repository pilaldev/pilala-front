import React from 'react'
import PropTypes from 'prop-types';
import { Switch as MuiSwitch } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

/**
 * Los switches activan (ON) o desactivan (OFF) el estado de un solo elemento
 */
const Switch = ({ onChange, disabled, checked, size }) => {
    const classes = useStyles({ checked });

    return (
        <MuiSwitch
            onChange={onChange}
            disableRipple
            color='default'
            disabled={disabled}
            size={size}
            checked={checked}
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                track: classes.track,
                checked: classes.checked,
            }}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiIconButton-root:hover': {
            backgroundColor: 'rgba(166, 166, 166, 0.1)'
        },
    },
    switchBase: {
        color: theme.palette.common.white,
        '&.Mui-checked + .MuiSwitch-track': {
            backgroundColor: theme.palette.primary[400],
            opacity: 1,
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: 1,
            backgroundColor: props => props.checked ? theme.palette.primary[200] : theme.palette.grey[400]
        },
        '&.Mui-disabled': {
            color: props => props.checked ? theme.palette.primary[400] : theme.palette.common.white
        },
    },
    track: {
        backgroundColor: theme.palette.grey[400],
        opacity: 1
    },
    checked: {
        color: theme.palette.primary.main,
        '&.MuiIconButton-root:hover': {
            backgroundColor: 'rgba(106, 50, 181, 0.1)'
        }
    }
}));

Switch.propTypes = {
    /**
     * Función para cambiar el estado del switch
     */
    onChange: PropTypes.func.isRequired,
    /**
     * Activar o desactivar el switch
     */
    disabled: PropTypes.bool,
    /**
     * Estado actual del switch
     */
    checked: PropTypes.bool,
    /**
     * Tamaño del switch
     */
    size: PropTypes.oneOf(['medium', 'small'])
}

Switch.defaultProps = {
    disabled: false,
    checked: false,
    size: 'medium'
}

export { Switch as default };
