import React from 'react';
import Modal from '@bit/pilala.pilalalib.components.modal';
import IconButton from '@bit/pilala.pilalalib.components.icon-button';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { useVideoModalStyles } from './styles';

const VideoModal = React.memo(({ open, url, onClose, title }) => {
  const classes = useVideoModalStyles();

  return (
    <Modal open={open} title={title} onClose={onClose} fullWidth>
      <div className={classes.container}>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
          className={classes.rowOne}
        >
          <Typography className={classes.title} variant="h5">
            {title}
          </Typography>
          <IconButton filled={false} size="small" onClick={onClose}>
            <CloseIcon />
          </IconButton>
        </Grid>
        <div className={classes.iframeContainer}>
          <iframe
            title={title}
            allowFullScreen
            src={url}
            width="100%"
            height="315px"
            className={classes.video}
          />
        </div>
      </div>
    </Modal>
  );
});

export default VideoModal;
