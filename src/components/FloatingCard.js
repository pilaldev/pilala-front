import React, { useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Popover } from '@material-ui/core';

/**
 * Los floating cards permiten crear una card flotante ligada a un elemento del DOM
 */
const FloatingCard = ({ children, position, ...props }) => {
    const classes = useStyles();
    const open = Boolean(props.anchorEl);

    const getPosition = useCallback((position) => {
        switch (position) {
            case 'left':
                return {
                    anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'left',
                    },
                    transformOrigin: {
                        vertical: 'top',
                        horizontal: 'left'
                    }
                }

            case 'center':
                return {
                    anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'center',
                    },
                    transformOrigin: {
                        vertical: 'top',
                        horizontal: 'center',
                    }
                }

            case 'right':
                return {
                    anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'right',
                    },
                    transformOrigin: {
                        vertical: 'top',
                        horizontal: 'right'
                    }
                    }
            default:
                break;
        }
    }, []);

    return (
        <Popover
            {...props}
            open={open}
            classes={{
                paper: classes.paper
            }}
            anchorOrigin={getPosition(position).anchorOrigin}
            transformOrigin={getPosition(position).transformOrigin}
            disableScrollLock
        >
            {children}
        </Popover>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        minWidth: 100,
        borderRadius: 8,
        boxShadow: theme.elevations[2],
        marginTop: theme.spacing(1)
    }
}));

FloatingCard.propTypes = {
    /**
     * Elemento renderizado en la card
     */
    children: PropTypes.any.isRequired,
    /**
     * Posición en la que debe aparecer la card
     */
    position: PropTypes.string.isRequired
}

FloatingCard.defaultProps = {
    position: 'right'
}

export { FloatingCard as default };