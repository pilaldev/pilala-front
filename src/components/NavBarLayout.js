import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, AppBar, IconButton } from '@material-ui/core';
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';
import { LogoPilala } from '@bit/pilala.pilalalib.assets';
import PropTypes from 'prop-types';

const NavBarLayout = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.navBar}>
        <div className={classes.appBarContent}>
          <IconButton
            size="small"
            disableRipple={true}
            className={classes.menuIconButton}
            onClick={props.openMobileDrawer}
          >
            <MenuRoundedIcon className={classes.menuIcon} />
          </IconButton>
          <img src={LogoPilala} alt="Logo Pilalá" className={classes.logo} />
        </div>
        <div className={classes.userCard}>
          {props.userCard ? props.userCard : null}
        </div>
      </AppBar>
    </div>
  );
};

NavBarLayout.propTypes = {
  openMobileDrawer: PropTypes.func.isRequired,
  userCard: PropTypes.element.isRequired,
};

NavBarLayout.defaultProps = {
  openMobileDrawer: () => {},
  userCard: null,
};

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  navBar: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    display: 'flex',
    flexDirection: 'row',
    zIndex: theme.zIndex.drawer + 1,
    height: 75,
    alignItems: 'center',
    boxShadow: '0px 3px 6px #00000029',
    padding: '0px 1rem',
  },
  logo: {
    height: 'auto',
    marginLeft: '0.5rem',
    [theme.breakpoints.up('xs')]: {
      width: '35%',
    },
    [theme.breakpoints.up('sm')]: {
      width: '40%',
    },
    [theme.breakpoints.up('lg')]: {
      width: '50%',
    },
  },
  menuIconButton: {
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
  },
  menuIcon: {
    color: '#263238',
    [theme.breakpoints.up('xs')]: {
      fontSize: '1.5rem',
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '2rem',
    },
  },
  appBarContent: {
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  userCard: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'flex-end',
  },
}));

export default NavBarLayout;
