import React from "react";
import { Chip as MuiChip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import clsx from "clsx";

/**
 * Los chips son elementos compactos que sirven para mostrar información relevante
 */
const Chip = ({ icon, outlined, clickable, handleclick, chipcolor, ...props }) => {
    const classes = useStyles({ chipcolor });
    const Icon = icon;
    return (
        <MuiChip
            className={clsx({
                [classes.outlined]: outlined,
                [classes.default]: !outlined
            })}
            variant={outlined ? 'outlined' : 'default'}
            icon={icon ? <Icon className={classes.icon} /> : null}
            onClick={clickable && handleclick ? handleclick : null}
            classes={{
                clickable: classes.clickable
            }}
            {...props}
        />
    );
}

const useStyles = makeStyles(theme => ({
    default: {
        backgroundColor: (props) => props.chipcolor ? theme.palette[props.chipcolor][200] : theme.palette.primary[200],
        color: props => props.chipcolor ? theme.palette[props.chipcolor].main : theme.palette.primary.main
    },
    outlined: {
        color: props => props.chipcolor ? theme.palette[props.chipcolor].main : theme.palette.primary.main,
        borderColor: props => props.chipcolor ? theme.palette[props.chipcolor].main : theme.palette.primary.main,
    },
    icon: {
        color: props => props.chipcolor ? theme.palette[props.chipcolor].main : theme.palette.primary.main
    },
    clickable: {
        '&:hover': {
            backgroundColor: props => props.chipcolor ? theme.palette[props.chipcolor][400] : theme.palette.primary[400]
        },
        '&:focus': {
            backgroundColor: props => props.chipcolor ? theme.palette[props.chipcolor][400] : theme.palette.primary[400]
        }
    }
}));

Chip.propTypes = {
    /**
     * Color elegido para el chip
     */
    chipcolor: PropTypes.oneOf(['primary', 'secondary', 'info', 'success', 'warning', 'error', 'grey']),
    /**
     * Contenido del chip
     */
    label: PropTypes.any.isRequired,
    /**
     * Función ejecutada cuando el chip es clickeable
     */
    handleclick: PropTypes.func,
    /**
     * Elegir variante outlined del chip
     */
    outlined: PropTypes.bool,
    /**
     * Icono ubicado a la izquierda del chip
     */
    icon: PropTypes.object,
    /**
     * Permitir que el chip sea clickeable
     */
    clickable: PropTypes.bool
};

Chip.defaultProps = {
    chipcolor: 'primary',
    outlined: false,
    clickable: false,
    handleclick: undefined,
    icon: undefined
}

export { Chip as default };