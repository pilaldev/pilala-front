import React from 'react'
import PropTypes from 'prop-types';
import { FormControlLabel, Radio } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

/**
 * Los radio buttons le permiten al usuario seleccionar una opción única dentro de un set de múltiples items
 */
const RadioButton = ({ onChange, disabled, checked, size, label }) => {
    const classes = useStyles({ checked });

    return (
        <FormControlLabel
            control={
                <Radio
                    checked={checked}
                    onChange={onChange}
                    size={size}
                    color='default'
                    disabled={disabled}
                    classes={{
                        root: classes.root,
                        checked: classes.checked,
                    }}
                />
            }
        label={label || ''}
      />
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        color: theme.palette.grey[600],
        '&.Mui-disabled': {
            color: props => props.checked ? theme.palette.primary[400] : theme.palette.grey[400]
        }
    },
    checked: {
        color: theme.palette.primary.main,
        '&:hover': {
            backgroundColor: theme.palette.primary[200],
        },
    },
}));

RadioButton.propTypes = {
    /**
     * Función para cambiar el estado del radio button
     */
    onChange: PropTypes.func.isRequired,
    /**
     * Activar o desactivar el radio button
     */
    disabled: PropTypes.bool,
    /**
     * Estado actual del radio button
     */
    checked: PropTypes.bool,
    /**
     * Tamaño del radio button
     */
    size: PropTypes.oneOf(['medium', 'small']),
    /**
     * Text que acompaña al radio button
     */
    label: PropTypes.string
}

RadioButton.defaultProps = {
    disabled: false,
    checked: false,
    size: 'medium'
}

export { RadioButton as default };
