import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Autocomplete } from '@material-ui/lab';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextInput from '@bit/pilala.pilalalib.components.textinput';

/**
 * Los dropdown inputs son los elementos que permiten tener una lista larga de elementos junto a un text input para buscar por palabras clave
 */
const DropdownInput = ({
	onChange,
	value,
	options,
	placeholder,
	fullWidth,
	disabled,
	label,
	helperText,
	error,
	...props
}) => {
	const classes = useStyles({ fullWidth });

	return (
		<Autocomplete
			{...props}
			onChange={onChange}
			value={value}
			options={options}
			openOnFocus={true}
			popupIcon={<ExpandMoreIcon />}
			disableClearable={true}
			className={classes.main}
			disabled={disabled}
			noOptionsText='No hay opciones'
			renderInput={(params) => (
				<TextInput
					{...params}
					placeholder={placeholder}
					label={label}
					helperText={helperText}
					error={error}
				/>
			)}
			classes={{
				popupIndicator: classes.popupIndicator,
				paper: classes.paper,
				root: classes.root,
			}}
		/>
	);
};

const useStyles = makeStyles((theme) => ({
	main: {
		width: (props) => (props.fullWidth ? '100%' : '15rem'),
	},
	popupIndicator: {
		color: theme.palette.grey[700],
		borderRadius: 5,
		'&:hover': {
			backgroundColor: theme.palette.grey[400],
		},
	},
	paper: {
		borderRadius: 8,
		margin: theme.spacing(1.25, 0),
		boxShadow: theme.elevations[2],
	},
	root: {
		'& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"] .MuiAutocomplete-input:first-child': {
			paddingLeft: 12,
		},
	},
}));

DropdownInput.propTypes = {
	/**
	 * Función para obtener el valor del dropdown
	 */
	onChange: PropTypes.func,
	/**
	 * Valor seleccionado del dropdown
	 */
	value: PropTypes.any,
	/**
	 * Array con las opciones que se mostrarán en la lista
	 */
	options: PropTypes.array.isRequired,
	/**
	 * Texto de ejemplo o informativo ubicado adentro del text input
	 */
	placeholder: PropTypes.string,
	/**
	 * El dropdown debe tomar todo el ancho disponible
	 */
	fullWidth: PropTypes.bool,
	/**
	 * Activar o desactivar el dropdown
	 */
	disabled: PropTypes.bool,
	/**
	 * Texto mostrado sobre el dropdown
	 */
	label: PropTypes.string,
	/**
	 * Texto de ayuda mostrado bajo el dropdown
	 */
	helperText: PropTypes.string,
};

DropdownInput.defaultProps = {
	onChange: undefined,
	value: undefined,
	placeholder: undefined,
	fullWidth: false,
	disabled: false,
};

export { DropdownInput as default };
