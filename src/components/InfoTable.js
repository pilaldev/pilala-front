import React, { useState, useEffect } from 'react';
import { Table, TableBody, Paper, TablePagination, CircularProgress, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const LOAD_TIME = 7000;

const InfoTable = ({ data, RowItem, enablePagination, TableHeader, tableStyles }) => {
    const classes = useStyles();
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [page, setPage] = useState(0);
    const [currentData, setCurrentData] = useState([]);
    const [loadStatus, setLoadStatus] = useState('pending');

    useEffect(() => {
        const loadTimeout = setTimeout(() => {
            if (data.length === 0) {
                setLoadStatus('not_data');
            }
        }, LOAD_TIME);
        return () => {
            clearTimeout(loadTimeout);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        const tempData = data.slice(page * rowsPerPage, (page * rowsPerPage) + rowsPerPage);
        setCurrentData(tempData);
    }, [data, page, rowsPerPage]);

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
        setPage(0);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    return (
        <Paper
            className={clsx(classes.tableContainer, {
                [classes.emptyDataContainer]: data.length === 0
            })}
            style={tableStyles}
        >
            {
                data.length > 0 ?
                    <>
                        <Table className={classes.table}>
                            {
                                TableHeader !== undefined ?
                                    <TableHeader />
                                    : null
                            }
                            <TableBody>
                                {
                                    currentData.map((item, index) => {
                                        return (
                                            <RowItem key={index} row={item} index={index} />
                                        );
                                    })
                                }
                            </TableBody>
                        </Table>
                        {
                            enablePagination ?
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25]}
                                    labelRowsPerPage='Filas por página'
                                    component='div'
                                    count={data.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                    SelectProps={{
                                        inputProps: {
                                            MenuProps: { disableScrollLock: true }
                                        }
                                    }}
                                    labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
                                />
                                : null
                        }
                    </>
                    :
                    <>
                        {
                            loadStatus === 'pending' ?
                                <CircularProgress className={classes.spinner} />
                                :
                                <Typography className={classes.message}>
                                    No hemos encontrado información reciente
                                </Typography>
                        }
                    </>
            }
        </Paper>
    );
}

const useStyles = makeStyles(theme => ({
    tableContainer: {
        display: 'flex',
        minWidth: '100%',
        maxWidth: '100%',
        flexDirection: 'column',
        height: 'content-fit',
        boxShadow: '0px 3px 16px #00000029',
        borderRadius: 7,
        overflowX: 'auto'
    },
    table: {
        minWidth: 320
    },
    emptyDataContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '100%',
        padding: '2rem'
    },
    spinner: {
        color: '#6A32B5',
        [theme.breakpoints.up('xs')]: {
            width: '20px !important',
            height: '20px !important'
        },
        [theme.breakpoints.up('sm')]: {
            width: '30px !important',
            height: '30px !important'
        },
        [theme.breakpoints.up('md')]: {
            width: '35px !important',
            height: '35px !important'
        },
    },
    message: {
        fontSize: '0.9rem',
        fontWeight: 'bold',
        color: '#FF495A',
        [theme.breakpoints.up('sm')]: {
            fontSize: '1rem'
        }
    }
}));

export default InfoTable;
