import React from 'react'
import PropTypes from 'prop-types';
import { FormControlLabel, Checkbox as MuiCheckbox } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

/**
 * Los checkboxes le permiten al usuario una o más opciones dentro de una posibilidad de items
 */
const Checkbox = ({ onChange, disabled, checked, size, label }) => {
    const classes = useStyles({ checked });

    return (
        <FormControlLabel
            control={
                <MuiCheckbox
                    checked={checked}
                    onChange={onChange}
                    size={size}
                    color='default'
                    disabled={disabled}
                    classes={{
                        root: classes.root,
                        checked: classes.checked,
                    }}
                />
            }
        label={label || ''}
      />
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        color: theme.palette.grey[600],
        '&.Mui-disabled': {
            color: props => props.checked ? theme.palette.primary[400] : theme.palette.grey[400]
        }
    },
    checked: {
        color: theme.palette.primary.main,
        '&:hover': {
            backgroundColor: theme.palette.primary[200],
        },
    },
}));

Checkbox.propTypes = {
    /**
     * Función para cambiar el estado del checkbox
     */
    onChange: PropTypes.func.isRequired,
    /**
     * Activar o desactivar el checkbox
     */
    disabled: PropTypes.bool,
    /**
     * Estado actual del checkbox
     */
    checked: PropTypes.bool,
    /**
     * Tamaño del checkbox
     */
    size: PropTypes.oneOf(['medium', 'small']),
    /**
     * Text que acompaña al checkbox
     */
    label: PropTypes.string
}

Checkbox.defaultProps = {
    disabled: false,
    checked: false,
    size: 'medium'
}

export { Checkbox as default };
