import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { MenuItem } from '@material-ui/core';
import TextInput from '@bit/pilala.pilalalib.components.textinput';

/**
 * Los select son los elementos que permiten seleccionar un valor de una lista de pocos elementos
 */
const Select = ({ value, options, onChange, disabled, label, helperText, fullWidth, ...props }) => {
	const classes = useStyles();

	return (
		<TextInput
			{...props}
			select={true}
			value={options.find((item) => item.value === value) || ''}
			fullWidth={fullWidth}
			onChange={onChange}
			disabled={disabled}
			label={label}
			helperText={helperText}
			SelectProps={{
				IconComponent: ExpandMoreIcon,
				MenuProps: {
					PopoverClasses: {
						paper: classes.paper,
					},
				},
				classes: {
					disabled: classes.disabled,
					outlined: classes.outlined,
				},
			}}
		>
			{options.map((item, index) => (
				<MenuItem
					value={item}
					key={`${item.display}_${Math.pow(index, 2)}`}
					classes={{
						root: classes.menuItemRoot,
					}}
				>
					{item.display}
				</MenuItem>
			))}
		</TextInput>
	);
};

const useStyles = makeStyles((theme) => ({
	paper: {
		backgroundColor: theme.palette.common.white,
		borderRadius: 8,
		boxShadow: theme.elevations[2],
		marginTop: theme.spacing(7.5),
	},
	menuItemRoot: {
		color: theme.palette.grey.main,
		backgroundColor: 'transparent',
		'&:hover': {
			backgroundColor: theme.palette.grey[200],
		},
		'&.Mui-selected': {
			backgroundColor: theme.palette.primary[200],
			color: theme.palette.primary.main,
		},
		'&.Mui-selected:hover': {
			backgroundColor: theme.palette.primary[200],
		},
	},
	disabled: {
		color: theme.palette.grey[400],
	},
	outlined: {
		textAlign: 'left',
	},
}));

Select.propTypes = {
	/**
	 * Array con las opciones que se mostrarán en la lista
	 */
	options: PropTypes.arrayOf(
		PropTypes.shape({
			display: PropTypes.string.isRequired,
			value: PropTypes.any.isRequired,
		})
	),
	/**
	 * Valor seleccionado de la lista de opciones
	 */
	value: PropTypes.any.isRequired,
	/**
	 * Función para obtener el valor seleccionado de la lista
	 */
	onChange: PropTypes.func,
	/**
	 * Activar o desactivar el select
	 */
	disabled: PropTypes.bool,
	/**
	 * Texto mostrado sobre el select
	 */
	label: PropTypes.string,
	/**
	 * Texto de ayuda mostrado bajo el select
	 */
	helperText: PropTypes.string,
	/**
	 * El select debe tomar todo el ancho disponible
	 */
	fullWidth: PropTypes.bool,
};

Select.defaultProps = {
	options: [],
	value: undefined,
	disabled: false,
	label: undefined,
	helperText: undefined,
	fullWidth: false,
};

export { Select as default };
