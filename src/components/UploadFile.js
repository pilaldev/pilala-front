import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { Button, Typography } from '@material-ui/core';
import clsx from 'clsx';

const UploadFile = ({message, onUploadFile}) => {
    const classes = useStyles();
    const fileInputRef = useRef(null);
    const [fileName, setFileName] = useState(null);
    const containerRef = useRef(null);
    const dragCounter = useRef(0);
    const [dragging, setDragging] = useState(false);

    useEffect(() => {
        const handleDragIn = (e) => {
            e.preventDefault();
            e.stopPropagation();
            dragCounter.current++;
            if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
                setDragging(true);
            }
        }
    
        const handleDragOut = (e) => {
            e.preventDefault();
            e.stopPropagation();
            dragCounter.current--;
            if (dragCounter > 0) return;
            setDragging(false);
        }
    
        const handleDrag = (e) => {
            e.preventDefault();
            e.stopPropagation();
        }
    
        const handleDrop = (e) => {
            e.preventDefault();
            e.stopPropagation();
            setDragging(false);
            if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
                setFileName(e.dataTransfer.files[0].name);
                onUploadFile({
                    successfulUpload: true,
                    file: e.dataTransfer.files[0],
                });
                e.dataTransfer.clearData();
                dragCounter.current = 0;
            }
        }

        let container = containerRef.current;
        container.addEventListener('dragenter', handleDragIn)
        container.addEventListener('dragleave', handleDragOut)
        container.addEventListener('dragover', handleDrag)
        container.addEventListener('drop', handleDrop)

        return () => {
            container.removeEventListener('dragenter', handleDragIn)
            container.removeEventListener('dragleave', handleDragOut)
            container.removeEventListener('dragover', handleDrag)
            container.removeEventListener('drop', handleDrop)
        }
    }, [onUploadFile]);

    const selectFile = () => {
        if (fileInputRef.current) {
            fileInputRef.current.click();
        }
    }

    const getSelectedFile = (e) => {
        setFileName(e.target.files[0].name);
        onUploadFile({
            successfulUpload: true,
            file: e.target.files[0],
        });
    }

    return (
        <div
            className={clsx(classes.container, {
                [classes.activeDragging]: dragging
            })}
            ref={containerRef}
        >
            <CloudUploadIcon className={classes.uploadIcon}/>
            <Typography className={classes.message}>
                {fileName || message}
            </Typography>
            <input
                type='file'
                ref={fileInputRef}
                style={{display: 'none'}}
                size={1}
                multiple={false}
                onChange={getSelectedFile}
            />
            <Button
                className={classes.uploadButton}
                onClick={selectFile}
            >
                Cargar archivo
            </Button>
        </div>
    );
}


UploadFile.propTypes = {
    message: PropTypes.string,
    onUploadFile: PropTypes.func.isRequired
}

UploadFile.defaultProps = {
    message: 'Arrastra aquí tu archivo'
}

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        border: `1px dashed ${theme.palette.primary.main}`,
        borderRadius: 10,
        padding: theme.spacing(2, 0, 4, 0)
    },
    uploadIcon: {
        color: theme.palette.primary[400],
        fontSize: '4rem'
    },
    message: {
        color: theme.palette.common.black,
        fontSize: '1rem'
    },
    uploadButton: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        textTransform: 'none',
        marginTop: theme.spacing(2),
        borderRadius: 8,
        '&:hover': {
            backgroundColor: theme.palette.primary[700],
        },
        '&:focus': {
            backgroundColor: theme.palette.primary[600]
        }
    },
    activeDragging: {
        backgroundColor: theme.palette.primary[200]
    }
}));

export default UploadFile
