import React from 'react';
import PropTypes from 'prop-types';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import 'moment/locale/es';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

moment.locale('es');

class CustomUtils extends MomentUtils {
    getWeekdays() {
        return ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
    }

    getCalendarHeaderText(date) {
        const month = moment(date).format('MMMM');
        const year = moment(date).format('YYYY');
        const currentMonth = month.slice(0, 1).toUpperCase() + month.slice(1);
        return `${currentMonth} de ${year}`;
    }
}

/**
 * Permiten al usuario seleccionar una fecha o un rango de fechas
 */
const Calendar = ({open, onChange, anchorElement, focusDate, formatFocusDate, color, variant}) => {
    const getAnchorEl = () => anchorElement;
    
    return (
        <ThemeProvider theme={stylesSchema[color]}>
            <MuiPickersUtilsProvider
                utils={CustomUtils}
                libInstance={moment}
            >
                <DatePicker
                    open={open}
                    autoOk={false}
                    initialFocusedDate={moment(focusDate, formatFocusDate)}
                    onChange={onChange}
                    variant={variant}
                    PopoverProps={{
                        anchorEl: getAnchorEl,
                        anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'center',
                        },
                        transformOrigin: {
                            vertical: 'top',
                            horizontal: 'center',
                        },
                        PaperProps: {
                            style: {
                                borderRadius: 12
                            }
                        }
                    }}
                    TextFieldComponent={() => (null)}
                    ToolbarComponent={() => (null)}
                />
            </MuiPickersUtilsProvider>
        </ThemeProvider>
    );
}

const stylesSchema = {
    primary: createMuiTheme({
        overrides: {
            MuiPickersDay: {
                daySelected: {
                    backgroundColor: '#C3ADE1',
                    color: '#000000',
                    '&:hover': {
                        backgroundColor: '#C3ADE1'
                    },
                },
                day: {
                    color: '#000000'
                },
                current: {
                    color: '#000000'
                }
            },
            MuiIconButton: {
                root: {
                    '&:hover': {
                        backgroundColor: '#F9F9F9'
                    }
                }
            },
            MuiPickersCalendarHeader: {
                dayLabel: {
                    color: '#6A32B5',
                    fontWeight: 700
                },
                iconButton: {
                    borderRadius: 8,
                    padding: 4,
                    backgroundColor: '#F3EFF9',
                    marginLeft: 22,
                    marginRight: 22,
                    '&:hover': {
                        backgroundColor: '#F3EFF9'
                    }
                },
                transitionContainer: {
                    '& > p': {
                        color: '#000000',
                        fontWeight: 700
                    }
                }
            },
            MuiSvgIcon: {
                root: {
                    color: '#6A32B5'
                }
            },
        }
    }),
    secondary: createMuiTheme({
        overrides: {
            MuiPickersDay: {
                daySelected: {
                    backgroundColor: '#FFBE99',
                    color: '#000000',
                    '&:hover': {
                        backgroundColor: '#FFBE99'
                    },
                },
                day: {
                    color: '#000000'
                },
                current: {
                    color: '#000000'
                }
            },
            MuiIconButton: {
                root: {
                    '&:hover': {
                        backgroundColor: '#F9F9F9'
                    }
                }
            },
            MuiPickersCalendarHeader: {
                dayLabel: {
                    color: '#FF5C00',
                    fontWeight: 700
                },
                iconButton: {
                    borderRadius: 8,
                    padding: 4,
                    backgroundColor: '#FFEEE5',
                    marginLeft: 22,
                    marginRight: 22,
                    '&:hover': {
                        backgroundColor: '#FFEEE5'
                    }
                },
                transitionContainer: {
                    '& > p': {
                        color: '#000000',
                        fontWeight: 700
                    }
                }
            },
            MuiSvgIcon: {
                root: {
                    color: '#FF5C00'
                }
            },
        }
    }),
    info: createMuiTheme({
        overrides: {
            MuiPickersDay: {
                daySelected: {
                    backgroundColor: '#A9C0FF',
                    color: '#000000',
                    '&:hover': {
                        backgroundColor: '#A9C0FF'
                    },
                },
                day: {
                    color: '#000000'
                },
                current: {
                    color: '#000000'
                }
            },
            MuiIconButton: {
                root: {
                    '&:hover': {
                        backgroundColor: '#F9F9F9'
                    }
                }
            },
            MuiPickersCalendarHeader: {
                dayLabel: {
                    color: '#2962FF',
                    fontWeight: 700
                },
                iconButton: {
                    borderRadius: 8,
                    padding: 4,
                    backgroundColor: '#E9EFFF',
                    marginLeft: 22,
                    marginRight: 22,
                    '&:hover': {
                        backgroundColor: '#E9EFFF'
                    }
                },
                transitionContainer: {
                    '& > p': {
                        color: '#000000',
                        fontWeight: 700
                    }
                }
            },
            MuiSvgIcon: {
                root: {
                    color: '#2962FF'
                }
            },
        }
    }),
    success: createMuiTheme({
        overrides: {
            MuiPickersDay: {
                daySelected: {
                    backgroundColor: '#B7DFB9',
                    color: '#000000',
                    '&:hover': {
                        backgroundColor: '#B7DFB9'
                    },
                },
                day: {
                    color: '#000000'
                },
                current: {
                    color: '#000000'
                }
            },
            MuiIconButton: {
                root: {
                    '&:hover': {
                        backgroundColor: '#F9F9F9'
                    }
                }
            },
            MuiPickersCalendarHeader: {
                dayLabel: {
                    color: '#4CAF50',
                    fontWeight: 700
                },
                iconButton: {
                    borderRadius: 8,
                    padding: 4,
                    backgroundColor: '#EDF7ED',
                    marginLeft: 22,
                    marginRight: 22,
                    '&:hover': {
                        backgroundColor: '#EDF7ED'
                    }
                },
                transitionContainer: {
                    '& > p': {
                        color: '#000000',
                        fontWeight: 700
                    }
                }
            },
            MuiSvgIcon: {
                root: {
                    color: '#4CAF50'
                }
            },
        }
    }),
    error: createMuiTheme({
        overrides: {
            MuiPickersDay: {
                daySelected: {
                    backgroundColor: '#FFB6BD',
                    color: '#000000',
                    '&:hover': {
                        backgroundColor: '#FFB6BD'
                    },
                },
                day: {
                    color: '#000000'
                },
                current: {
                    color: '#000000'
                }
            },
            MuiIconButton: {
                root: {
                    '&:hover': {
                        backgroundColor: '#F9F9F9'
                    }
                }
            },
            MuiPickersCalendarHeader: {
                dayLabel: {
                    color: '#FF495A',
                    fontWeight: 700
                },
                iconButton: {
                    borderRadius: 8,
                    padding: 4,
                    backgroundColor: '#FFE4E6',
                    marginLeft: 22,
                    marginRight: 22,
                    '&:hover': {
                        backgroundColor: '#FFE4E6'
                    }
                },
                transitionContainer: {
                    '& > p': {
                        color: '#000000',
                        fontWeight: 700
                    }
                }
            },
            MuiSvgIcon: {
                root: {
                    color: '#FF495A'
                }
            },
        }
    }),
    warning: createMuiTheme({
        overrides: {
            MuiPickersDay: {
                daySelected: {
                    backgroundColor: '#FFE0AE',
                    color: '#000000',
                    '&:hover': {
                        backgroundColor: '#FFE0AE'
                    },
                },
                day: {
                    color: '#000000'
                },
                current: {
                    color: '#000000'
                }
            },
            MuiIconButton: {
                root: {
                    '&:hover': {
                        backgroundColor: '#F9F9F9'
                    }
                }
            },
            MuiPickersCalendarHeader: {
                dayLabel: {
                    color: '#FFB134',
                    fontWeight: 700
                },
                iconButton: {
                    borderRadius: 8,
                    padding: 4,
                    backgroundColor: '#FFF7EA',
                    marginLeft: 22,
                    marginRight: 22,
                    '&:hover': {
                        backgroundColor: '#FFF7EA'
                    }
                },
                transitionContainer: {
                    '& > p': {
                        color: '#000000',
                        fontWeight: 700
                    }
                }
            },
            MuiSvgIcon: {
                root: {
                    color: '#FFB134'
                }
            },
        }
    }),
}

Calendar.propTypes = {
    /**
     * Mostrar u ocultar el calendario
     */
    open: PropTypes.bool,
    /**
     * Función para obtener la fecha seleccionada en el calendario
     */
    onChange: PropTypes.func.isRequired,
    /**
     * Elemento del DOM al que se debe anclar el calendario si se usa la variante inline
     */
    anchorElement: PropTypes.object,
    /**
     * Fecha seleccionada por defecto al abrir el calendario
     */
    focusDate: PropTypes.string,
    /**
     * Formato de la fecha proporcionada en focusDate
     */
    formatFocusDate: PropTypes.string,
    /**
     * Color elegido para el calendario
     */
    color: PropTypes.oneOf(['primary', 'secondary', 'info', 'success', 'warning', 'error']),
    /**
     * Tipo de calendario que se mostrará
     */
    variant: PropTypes.oneOf(['inline', 'static'])
}

Calendar.defaultProps = {
    open: false,
    formatFocusDate: 'DD_MM_YYYY',
    color: 'primary',
    variant: 'inline'
}

export default Calendar;
