import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

const ContentLayout = (props) => {
	const classes = useStyles();

	return (
		<div className={classes.container}>
			<Switch>
				<Route path="/:userId/console/controlPanel">{props.children[0]}</Route>
				<Route path="/:userId/console/liquidarPila/:stepPila">{props.children[1]}</Route>
				<Route path="/:userId/console/payHistory">{props.children[2]}</Route>
				<Route exact path="/:userId/console/cotizantes">{props.children[3]}</Route>
				<Route path="/:userId/console/company">{props.children[4]}</Route>
				<Route path="/:userId/console/configuration">{props.children[5]}</Route>
				<Route path="/:userId/console/helpCenter">{props.children[6]}</Route>
				<Route exact path="/:userId/console/cotizantes/:detailID">{props.children[7]}</Route>
				<Route path="/:userId/console/multicompanyPanel">{props.children[8]}</Route>
				<Route path="/:userId/console/planillaDetail/:planillaID">{props.children[9]}</Route>
			</Switch>
		</div>
	);
};

const useStyles = makeStyles({
	container: {
		display: 'flex',
		flex: 1,
		width: '100%'
	}
});

export default ContentLayout;
