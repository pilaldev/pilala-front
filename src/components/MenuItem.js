import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { MenuItem as MuiMenuItem } from '@material-ui/core';

/**
 * Los menu item permiten representar las opciones de un menu flotante
 */
const MenuItem = ({ children, ...props }) => {
    const classes = useStyles();

    return (
        <MuiMenuItem
            {...props}
            classes={{
                root: classes.root
            }}
        >
            {children}
        </MuiMenuItem>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.common.white,
        color: theme.palette.primary.main,
        '&:hover': {
            backgroundColor: theme.palette.primary[200]
        }
    }
}));

MenuItem.propTypes = {
    /**
     * Elemento renderizado en cada opción de la lista
     */
    children: PropTypes.any.isRequired
}

export { MenuItem as default };