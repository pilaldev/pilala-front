import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import { Modal } from '.';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@bit/pilala.pilalalib.components.button';

/**
 * Los alerts enfocan la atención del usuario en un área en especial que requiere de una acción
 */
const Alert = ({ onCancel, onAccept, message, title, open }) => {
	const classes = useStyles();

	return (
		<Modal open={open} title={title} onCloseModal={onCancel}>
			<Grid
				container
				direction='row'
				justify='flex-start'
				alignItems='center'
				className={classes.messageRow}
			>
				<Typography variant='subtitle1' className={classes.message}>
					{message}
				</Typography>
			</Grid>
			<Grid container direction='row' justify='flex-end' alignItems='center'>
				<div className={classes.cancelButtonContainer}>
					<Button onClick={onCancel} variant='outlined'>
						Cancelar
					</Button>
				</div>
				<Button onClick={onAccept}>Aceptar</Button>
			</Grid>
		</Modal>
	);
};

const useStyles = makeStyles((theme) => ({
	cancelButtonContainer: {
		marginRight: theme.spacing(2),
	},
	message: {
		color: theme.palette.grey[600],
	},
	messageRow: {
		margin: theme.spacing(3, 0),
	},
}));

Alert.propTypes = {
	/**
	 * Acción para cancelar la alerta
	 */
	onCancel: PropTypes.func.isRequired,
	/**
	 * Acción para aceptar la alerta
	 */
	onAccept: PropTypes.func.isRequired,
	/**
	 * Mensaje mostrado en la alerta
	 */
	message: PropTypes.string.isRequired,
	/**
	 * Titulo mostrado en la parte superior de la alerta
	 */
	title: PropTypes.string.isRequired,
	/**
	 * Mostrar u ocultar la alerta
	 */
	open: PropTypes.bool.isRequired,
};

export { Alert as default };
