import React, { useState } from 'react';
import { Card } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Modal from '@bit/pilala.pilalalib.components.modal';
import { useCredentialsModalStyles } from './styles';
import CredentialsForm from './CredentialsForm';
import {
  companyAction,
  credentialsModalAction,
  panelMultiempresaAction,
  snackBarAction,
} from '../redux/Actions';
import {
  COMPANY,
  CREDENTIALS_MODAL,
  PANEL_MULTIEMPRESA,
  SNACKBAR,
} from '../redux/ActionTypes';
import ConnectCredentials from '../api/ConnectCredentials';
import { SearchCompanyInfo } from '../signup/api';
import HelperFunctions from '../utils/HelperFunctions';
import CalcularDV from '../utils/CalcularDV';
import { MatchNameFields } from '../utils/MatchNameFields';
import UpdateOnlineStatus from '../utils/UpdateOnlineStatus';
import { FIREBASE_AUTH } from '../api/Firebase';

const CredentialsModal = React.memo(() => {
  const classes = useCredentialsModalStyles();
  const dispatch = useDispatch();
  const isCredentialsModalOpen = useSelector(
    (state) => state.credentialsModal.isModalOpen
  );
  const context = useSelector((state) => state.credentialsModal.context);
  const [disableConfirmButton, setDisableConfirmButton] = useState(false);
  const [disableCancelButton, setDisableCancelButton] = useState(false);
  const createdCompany = useSelector(
    (state) => state.currentUser.createdCompany
  );
  const userEmail = useSelector((state) => state.currentUser.email);
  const idCompany = useSelector((state) => state.currentUser.activeCompany);
  const name = useSelector((state) => state.currentUser.name);
  const documentType = useSelector((state) => state.currentUser.documentType);
  const documentNumber = useSelector(
    (state) => state.currentUser.documentNumber
  );
  const phoneNumber = useSelector((state) => state.currentUser.phoneNumber);
  const ciiu = useSelector((state) => state.resources.ciiu);
  const provinces = useSelector((state) => state.resources.provinces);
  const cities = useSelector((state) => state.resources.cities);
  const camaras = useSelector((state) => state.resources.camaras);
  const identificacionAportante = useSelector(
    (state) => state.resources.identificacionAportante
  );
  const showNewCompanyModal = useSelector(
    (state) => state.panelMultiempresa.showNewCompanyModal
  );

  const connectOperatorCredentials = async (data) => {
    const {
      companyIdType: idType,
      companyIdNumber: idNumber,
      userNumber,
      password: pwd,
      credentials,
      userType,
    } = data;
    const credentialsId = credentials !== -1 ? credentials?.id : null;
    const connectionResult = await ConnectCredentials({
      idType,
      idNumber,
      companyId: idCompany,
      user: userType + userNumber,
      pwd,
      userEmail,
      id: credentialsId,
    });

    if (connectionResult.statusCode === 200) {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Conexión exitosa',
          description: connectionResult?.message,
          color: 'success',
        })
      );
      setDisableConfirmButton(false);
      dispatch(credentialsModalAction(CREDENTIALS_MODAL.CLOSE_MODAL));
    } else {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error en la conexión',
          description: connectionResult?.message,
          color: 'error',
        })
      );
      setDisableConfirmButton(false);
    }
  };

  const formatMisDatosInfo = (nitCompany, data) => {
    const newCiiu = HelperFunctions.matchInObjectArray(
      data?.actividad ?? '',
      'value',
      ciiu
    );
    let newProvince = null;
    let newCity = null;
    if (data.departamento && data.ciudad) {
      newProvince = HelperFunctions.matchInObjectArray(
        data?.departamento ?? 'antioquia',
        'value',
        provinces
      );
      newCity = HelperFunctions.matchInObjectArray(
        data?.ciudad ?? 'medellin',
        'value',
        cities
      );
    } else if (
      (data?.departamento === '' || data?.ciudad === '') &&
      data?.nombreCamara
    ) {
      const tempCamara = HelperFunctions.matchInObjectArray(
        data?.nombreCamara ?? 'MEDELLIN PARA ANTIOQUIA',
        'camara',
        camaras
      );
      newProvince = HelperFunctions.matchInObjectArray(
        tempCamara?.province,
        'value',
        provinces
      );
      newCity = HelperFunctions.matchInObjectArray(
        tempCamara?.city,
        'value',
        cities
      );
    } else {
      newProvince = HelperFunctions.matchInObjectArray(
        'antioquia',
        'value',
        provinces
      );
      newCity = HelperFunctions.matchInObjectArray('medellin', 'value', cities);
    }

    const companyData = {
      city: newCity,
      province: newProvince,
      companyName: data?.razonSocial ?? '',
      economicActivity: newCiiu,
      phoneNumber: data?.telefono ?? '',
      identificationType: HelperFunctions.matchInObjectArray(
        data?.claseIdentificacion ?? 'nit',
        'display',
        identificacionAportante
      ),
      identificationNumber: nitCompany,
      verificationDigit: CalcularDV(nitCompany),
    };
    dispatch(companyAction(COMPANY.COMPANY_SAVE_ALL_DATA, companyData));

    if (data?.categoriaMatricula === 'PERSONA NATURAL') {
      const nameArray = data.razonSocial.split(' ');
      let reorderedName = [];
      reorderedName = [
        nameArray[nameArray.length - 1],
        ...nameArray.slice(1, nameArray.length - 1),
        nameArray[0],
      ].join(' ');

      const RLData = {
        city: newCity,
        province: newProvince,
        name: reorderedName,
        phoneNumber: data?.telefono ?? '',
        cellphoneNumber: '',
        identificationType: HelperFunctions.matchInObjectArray(
          'CC',
          'display',
          identificacionAportante
        ),
        identificationNumber: nitCompany,
      };
      dispatch(companyAction(COMPANY.COMPANY_SAVE_LR_ALL_DATA, RLData));
    }
  };

  const saveDefaultCompanyData = (nitCompany) => {
    const companyData = {
      province: HelperFunctions.matchInObjectArray(
        'antioquia',
        'value',
        provinces
      ),
      city: HelperFunctions.matchInObjectArray('medellin', 'value', cities),
      identificationNumber: nitCompany,
      verificationDigit: CalcularDV(nitCompany),
      economicActivity: HelperFunctions.matchInObjectArray('', 'value', ciiu),
    };
    dispatch(companyAction(COMPANY.COMPANY_SAVE_ALL_DATA, companyData));
  };

  const checkCompanyNit = async (nitCompany) => {
    const searchInfoResult = await SearchCompanyInfo(nitCompany);

    if (!createdCompany) {
      if (searchInfoResult?.statusCode === 200 && searchInfoResult?.data) {
        formatMisDatosInfo(nitCompany, searchInfoResult?.data);
      } else {
        saveDefaultCompanyData(nitCompany);
      }
      dispatch(credentialsModalAction(CREDENTIALS_MODAL.CLOSE_MODAL));
      setDisableConfirmButton(false);
    } else {
      if (showNewCompanyModal === false) {
        dispatch(companyAction(COMPANY.COMPANY_COPY_COMPANY_DATA, 'company'));
        dispatch(
          companyAction(COMPANY.COMPANY_COPY_COMPANY_DATA, 'businessContact')
        );
        dispatch(
          companyAction(
            COMPANY.COMPANY_COPY_COMPANY_DATA,
            'legalRepresentative'
          )
        );
        dispatch(companyAction(COMPANY.COMPANY_REMOVE_CARDS_DATA, null));
      }
      if (searchInfoResult?.statusCode === 200 && searchInfoResult?.data) {
        formatMisDatosInfo(nitCompany, searchInfoResult?.data);
      } else {
        saveDefaultCompanyData(nitCompany);
      }
      const nameData = MatchNameFields(name);
      const newName = {
        display: nameData.displayName,
        firstName: nameData.firstName,
        secondName: nameData.secondName,
        firstSurname: nameData.firstSurname,
        secondSurname: nameData.secondSurname,
      };
      const businessContactData = {
        name: newName,
        email: userEmail,
        province: '',
        city: '',
        identificationType: {
          display: documentType,
          code: documentType,
        },
        identificationNumber: documentNumber,
        phoneNumber: '',
        cellphoneNumber: phoneNumber,
      };
      dispatch(
        companyAction(COMPANY.COMPANY_SAVE_BC_ALL_DATA, businessContactData)
      );

      setDisableConfirmButton(false);
      dispatch(credentialsModalAction(CREDENTIALS_MODAL.CLOSE_MODAL));
      dispatch(
        panelMultiempresaAction(
          PANEL_MULTIEMPRESA.PANEL_MULTIEMPRESA_SHOW_NEW_COMPANY_MODAL,
          null
        )
      );
    }
  };

  const handleOnSave = async (data) => {
    if (disableConfirmButton) return;
    setDisableConfirmButton(true);

    if (context === 'credentials') {
      connectOperatorCredentials(data);
    } else if (context === 'default') {
      checkCompanyNit(data.companyIdNumber);
    } else {
      setDisableConfirmButton(false);
    }
  };

  const closeUserSession = async () => {
    if (createdCompany) {
      await UpdateOnlineStatus(userEmail, false);
    }
    FIREBASE_AUTH.signOut().catch(() => {
      dispatch(
        snackBarAction(SNACKBAR.OPEN_SNACKBAR, {
          title: 'Error al cerrar la sesión',
          description: 'Ha ocurrido un error inesperado al cerrar la sesión',
          color: 'error',
        })
      );
    });
  };

  const handleOnCancel = () => {
    if (disableCancelButton) return;
    setDisableCancelButton(true);

    if (!createdCompany) {
      closeUserSession();
    }
    setDisableConfirmButton(false);
    setDisableCancelButton(false);
    dispatch(credentialsModalAction(CREDENTIALS_MODAL.CLOSE_MODAL));
  };

  const resetModalValues = () => {
    dispatch(credentialsModalAction(CREDENTIALS_MODAL.RESET_MODAL_VALUES));
  };

  return (
    <Modal
      open={isCredentialsModalOpen}
      type="dark"
      onExited={resetModalValues}
    >
      <Card className={classes.checkNitCard}>
        <CredentialsForm
          onCancel={handleOnCancel}
          onConfirm={handleOnSave}
          disableCancelButton={disableCancelButton}
          disableConfirmButton={disableConfirmButton}
        />
      </Card>
    </Modal>
  );
});

export default CredentialsModal;
