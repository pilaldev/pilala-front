import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import LastPageIcon from '@material-ui/icons/LastPage';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import {
	ButtonBase,
	CircularProgress,
	IconButton,
	Paper,
	Tab,
	Table as MuiTable,
	TableBody,
	TableContainer,
	Tabs,
	Typography,
} from '@material-ui/core';
import clsx from 'clsx';
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
import PropTypes from 'prop-types';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import { PaginationItem, Pagination } from '@material-ui/lab';
import Button from '@bit/pilala.pilalalib.components.button';
import TextInput from '@bit/pilala.pilalalib.components.textinput';

const ROWS_PER_PAGE = 10;
const LOAD_TIME = 7000;

/**
 * Las tablas nos permiten ver gran cantidad de información de forma clara y organizada
 */
const Table = ({
	data,
	Row,
	Head,
	withPagination,
	withTabs,
	withActionbar,
	tabsArray,
	withActions,
	actionsArray,
	withLoader,
	TextInputProps,
	filterKeys,
}) => {
	const classes = useStyles();
	const COUNT = data.length;
	const [page, setPage] = useState(1);
	const [MAX_PAGE, setMaxPage] = useState(Math.ceil(COUNT / ROWS_PER_PAGE));
	const [tabValue, setTabValue] = useState(0);
	const [currentData, setCurrentData] = useState(data.slice(0, ROWS_PER_PAGE));
	const [loadStatus, setLoadStatus] = useState('pending');
	const [filteredData, setFilteredData] = useState(data);

	useEffect(() => {
		if (withLoader) {
			const loadTimeout = setTimeout(() => {
				if (COUNT === 0) {
					setLoadStatus('not_data');
				}
			}, LOAD_TIME);
			return () => {
				clearTimeout(loadTimeout);
			};
		} else {
			setLoadStatus('not_data');
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		if (data.length > 0) {
			setFilteredData(data);
		}
	}, [data]);

	useEffect(() => {
		if (filteredData && filteredData.length > 0) {
			const tempData = filteredData.slice(
				(page - 1) * ROWS_PER_PAGE,
				(page - 1) * ROWS_PER_PAGE + ROWS_PER_PAGE
			);
			setCurrentData(tempData);
			const newMaxPage = Math.ceil(filteredData.length / ROWS_PER_PAGE);
			setMaxPage(newMaxPage);
		}
	}, [filteredData, page]);

	const handleFirstPageButton = () => {
		setPage(1);
	};

	const handleBackButton = () => {
		setPage((currentPage) => currentPage - 1);
	};

	const handleNextButton = () => {
		setPage((currentPage) => currentPage + 1);
	};

	const handleLastPageButton = () => {
		setPage(Math.max(1, MAX_PAGE));
	};

	// TODO: [PILA-351] Añadir una función onChangeTab a las props de la tabla
	const handleTabChange = (event, value) => {
		setTabValue(value);
	};

	const filterCotizantesList = (e) => {
		const value = e.target.value.toLowerCase();
		const filteredData = data.filter((item) => {
			let isMatched = false;
			filterKeys.forEach((filterKey) => {
				if (!filterKey.includes('.') && filterKey in item) {
					if (item[filterKey].toLowerCase().indexOf(value) > -1) {
						isMatched = true;
					}
				} else if (filterKey.includes('.')) {
					const nestedValue = getNestedValue(item, filterKey);
					if (typeof nestedValue === 'string' && nestedValue.toLowerCase().indexOf(value) > -1) {
						isMatched = true;
					}
				}
			});
			return isMatched;
		});
		setFilteredData(filteredData);
		const paginatedData = filteredData.slice(
			(page - 1) * ROWS_PER_PAGE,
			(page - 1) * ROWS_PER_PAGE + ROWS_PER_PAGE
		);
		setCurrentData(paginatedData);
	};

	const getNestedValue = (obj, path) => {
		if (!path) return obj;
		const properties = path.split('.');
		return getNestedValue(obj[properties.shift()], properties.join('.'));
	};

	return (
		<div
			className={clsx(classes.container, {
				[classes.emptyContainer]: data.length === 0,
			})}
		>
			<Paper
				className={clsx(classes.tableContainer, {
					[classes.emptyDataContainer]: data.length === 0,
				})}
			>
				{data.length > 0 ? (
					<>
						{withTabs ? (
							<Tabs
								value={tabValue}
								onChange={handleTabChange}
								className={classes.tabsContainer}
								classes={{
									indicator: classes.tabsIndicator,
								}}
								TabIndicatorProps={{ children: <div /> }}
							>
								{tabsArray.map((item, index) => (
									<Tab
										disableRipple
										key={item}
										label={
											<div className={classes.tabContentContainer}>
												<Typography
													className={index === tabValue ? classes.tabTextOne : classes.tabTextTwo}
												>
													{item}
												</Typography>
											</div>
										}
										classes={{
											root: classes.rootTab,
										}}
									/>
								))}
							</Tabs>
						) : null}
						{withActionbar ? (
							<div className={classes.actionBarContainer}>
								<div className={classes.searchBarContainer}>
									<TextInput
										endIcon={<SearchRoundedIcon />}
										onChange={filterCotizantesList}
										className={classes.searchBar}
										{...TextInputProps}
									/>
								</div>
								{withActions ? (
									<div className={classes.actionsContainer}>
										{actionsArray.map((item) => (
											<Button
												key={item.label}
												onClick={item.action}
												variant={item.variant}
												startIcon={item.startIcon}
												endIcon={item.endIcon}
												className={classes.actionButton}
											>
												{item.label}
											</Button>
										))}
									</div>
								) : null}
							</div>
						) : null}
						<TableContainer component={Paper}>
							<MuiTable>
								{Head !== undefined ? <Head /> : null}
								<TableBody>
									{currentData.map((item, index) => {
										return <Row key={index} row={item} index={index} />;
									})}
								</TableBody>
							</MuiTable>
						</TableContainer>
					</>
				) : (
					<>
						{loadStatus === 'pending' ? (
							<CircularProgress size={35} className={classes.tableSpinner} />
						) : (
							<div className={classes.emptyMessageContainer}>
								<InsertDriveFileIcon className={classes.emptyIcon} />
								<Typography className={classes.emptyText}>¡No encontramos registros!</Typography>
							</div>
						)}
					</>
				)}
			</Paper>
			{withPagination ? (
				<div className={classes.paginationContainer}>
					<IconButton size='small' onClick={handleFirstPageButton} disabled={page === 1}>
						<FirstPageIcon
							className={clsx(classes.paginationIcon, {
								[classes.disabledPaginationColor]: page === 1,
							})}
						/>
					</IconButton>
					<ButtonBase
						className={classes.paginationPrevButton}
						onClick={handleBackButton}
						disabled={page === 1}
						disableRipple
						classes={{
							root: classes.paginationButtonRoot,
						}}
					>
						<NavigateBeforeIcon
							className={clsx(classes.paginationIcon, {
								[classes.disabledPaginationColor]: page === 1,
							})}
						/>
						<Typography
							className={
								page === 1 ? classes.paginationTextBeforeTwo : classes.paginationTextBeforeOne
							}
						>
							Anterior
						</Typography>
					</ButtonBase>
					<Pagination
						count={MAX_PAGE}
						hideNextButton
						hidePrevButton
						page={page}
						disabled={true}
						renderItem={(item) => (
							<PaginationItem
								{...item}
								classes={{
									disabled: classes.currentPageItem,
									root: classes.pageItem,
								}}
							/>
						)}
					/>
					<ButtonBase
						className={classes.paginationNextButton}
						onClick={handleNextButton}
						disabled={page >= MAX_PAGE}
						disableRipple
						classes={{
							root: classes.paginationButtonRoot,
						}}
					>
						<Typography
							className={
								page >= MAX_PAGE ? classes.paginationTextNextTwo : classes.paginationTextNextOne
							}
						>
							Siguiente
						</Typography>
						<NavigateNextIcon
							className={clsx(classes.paginationIcon, {
								[classes.disabledPaginationColor]: page >= MAX_PAGE,
							})}
						/>
					</ButtonBase>
					<IconButton size='small' onClick={handleLastPageButton} disabled={page >= MAX_PAGE}>
						<LastPageIcon
							className={clsx(classes.paginationIcon, {
								[classes.disabledPaginationColor]: page >= MAX_PAGE,
							})}
						/>
					</IconButton>
				</div>
			) : null}
		</div>
	);
};

const useStyles = makeStyles((theme) => ({
	container: {
		display: 'grid',
		flexDirection: 'column',
	},
	emptyContainer: {
		height: '80%',
	},
	tableContainer: {
		display: 'flex',
		width: '100%',
		flexDirection: 'column',
		height: 'content-fit',
		overflow: 'hidden',
		boxShadow: '0px 4px 16px #00000029',
		borderRadius: 7,
	},
	emptyDataContainer: {
		minHeight: '100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	tabsContainer: {
		padding: '10px 0px 0px 15px',
	},
	tabsIndicator: {
		backgroundColor: 'transparent',
		'& > div': {
			maxWidth: '90%',
			width: '100%',
			backgroundColor: '#5E35B1',
		},
		display: 'flex',
		justifyContent: 'center',
	},
	tabContentContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	tabTextOne: {
		textTransform: 'none',
		color: '#5E35B1',
		fontWeight: 'bold',
		fontSize: '1.1rem',
	},
	tabTextTwo: {
		textTransform: 'none',
		color: '#95989A',
		fontSize: '1.1rem',
	},
	actionBarContainer: {
		display: 'flex',
		alignItems: 'center',
		width: '100%',
		backgroundColor: '#5E35B126',
		padding: '1em 2em',
		justifyContent: 'space-between',
	},
	tableSpinner: {
		color: '#6A32B5',
	},
	paginationContainer: {
		display: 'flex',
		width: '100&',
		height: 40,
		marginTop: 35,
		justifyContent: 'center',
		alignItems: 'center',
	},
	paginationIcon: {
		color: theme.palette.primary.main,
		fontSize: '1.4rem',
	},
	disabledPaginationColor: {
		color: theme.palette.grey[400],
	},
	paginationPrevButton: {
		marginLeft: theme.spacing(2),
		display: 'flex',
		alignItems: 'center',
		marginRight: theme.spacing(3),
		padding: theme.spacing(0, 1),
	},
	paginationNextButton: {
		marginRight: theme.spacing(2),
		display: 'flex',
		alignItems: 'center',
		marginLeft: theme.spacing(3),
		padding: theme.spacing(0, 1),
	},
	pageItem: {
		'&.MuiPaginationItem-page.Mui-disabled': {
			color: theme.palette.primary[600],
			fontSize: '1rem',
			backgroundColor: 'transparent',
			opacity: 1,
		},
	},
	currentPageItem: {
		'&.MuiPaginationItem-page.Mui-selected': {
			backgroundColor: theme.palette.secondary.main,
			color: theme.palette.common.white,
		},
	},
	actionsContainer: {
		height: '100%',
		display: 'flex',
		alignItems: 'center',
	},
	paginationTextBeforeOne: {
		color: theme.palette.primary.main,
		fontSize: '1rem',
		fontWeight: 'bold',
		marginLeft: theme.spacing(1),
	},
	paginationTextBeforeTwo: {
		color: theme.palette.grey[400],
		fontSize: '1rem',
		fontWeight: 'bold',
		marginLeft: theme.spacing(1),
	},
	paginationTextNextOne: {
		color: theme.palette.primary.main,
		fontSize: '1rem',
		fontWeight: 'bold',
		marginRight: theme.spacing(1),
	},
	paginationTextNextTwo: {
		color: theme.palette.grey[400],
		fontSize: '1rem',
		fontWeight: 'bold',
		marginRight: theme.spacing(1),
	},
	emptyIcon: {
		fontSize: '3rem',
		color: theme.palette.grey[400],
	},
	emptyText: {
		fontSize: '1rem',
		color: theme.palette.grey[400],
		fontWeight: 'bold',
	},
	emptyMessageContainer: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	rootTab: {
		'&:hover': {
			backgroundColor: theme.palette.grey[200],
			borderRadius: theme.spacing(1),
		},
	},
	paginationButtonRoot: {
		'&:hover': {
			backgroundColor: theme.commonHover,
			borderRadius: theme.spacing(1),
		},
	},
	actionButton: {
		marginLeft: theme.spacing(2),
	},
	searchBar: {
		width: '100%',
	},
	searchBarContainer: {
		width: '30%',
	},
}));

Table.propTypes = {
	/**
	 * Array de datos que serán listados en la tabla
	 */
	data: PropTypes.array.isRequired,
	/**
	 * Función que devuelve el componente que representa cada fila de la tabla
	 */
	Row: PropTypes.func.isRequired,
	/**
	 * Función que devuelve el componente que representa el encabezado de la tabla
	 */
	Head: PropTypes.func,
	/**
	 * Habilitar el paginador de la tabla
	 */
	withPagination: PropTypes.bool,
	/**
	 * Habilitar las tabs de la tabla
	 */
	withTabs: PropTypes.bool,
	/**
	 * Habilitar la barra de acciones de la tabla
	 */
	withActionbar: PropTypes.bool,
	/**
	 * Array con los nombres de las tabs de la tabla. Obligatorio si se especifica withTabs={true}
	 */
	tabsArray: PropTypes.arrayOf(PropTypes.string),
	/**
	 * Habilitar las acciones de la tabla
	 */
	withActions: PropTypes.bool,
	/**
	 * Array de objetos que representan las acciones de la tabla. Obligatorio si se especifica withActions={true}
	 */
	actionsArray: PropTypes.arrayOf(
		PropTypes.shape({
			label: PropTypes.string.isRequired,
			action: PropTypes.func.isRequired,
			variant: PropTypes.string,
			startIcon: PropTypes.node,
			endIcon: PropTypes.node,
		})
	),
	/**
	 * Habilitar el loader de carga de datos
	 */
	withLoader: PropTypes.bool,
	/**
	 * Propiedas opcionales que se aplican al TextInput de la tabla
	 */
	TextInputProps: PropTypes.shape({
		placeholder: PropTypes.string,
	}),
	/**
	 * Array con las keys por las que se puede filtrar la información de la tabla
	 */
	filterKeys: PropTypes.arrayOf(PropTypes.string),
};

Table.defaultProps = {
	withPagination: true,
	withTabs: false,
	withActionbar: true,
	tabsArray: [],
	actionsArray: [],
	withActions: false,
	withLoader: true,
	filterKeys: [],
};

export default Table;
