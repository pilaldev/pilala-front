import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    CssBaseline,
    Drawer,
    Hidden
} from '@material-ui/core';
import DrawerOptionsLayout from './DrawerOptionsLayout';

const DRAWER_WIDTH = 220;

const SideBarLayout = (props) => {

    const classes = useStyles();

    return (
        <>
            <CssBaseline />
            <Drawer
                variant='temporary'
                open={props.mobileDrawer}
                onClose={props.openMobileDrawer}
                classes={{
                    paper: classes.drawerPaper,
                }}
                ModalProps={{
                    keepMounted: true
                }}
            >
                <DrawerOptionsLayout />
            </Drawer>
            <Hidden mdDown implementation='css'>
                <div style={{ width: DRAWER_WIDTH }}>
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant='permanent'
                        open
                    >
                        <DrawerOptionsLayout />
                    </Drawer>
                </div>
            </Hidden>
        </>
    );
}



const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    drawerPaper: {
        width: DRAWER_WIDTH,
        paddingTop: '3rem',
        backgroundColor: '#F7F7F7',
        borderRight: 'transparent',
        [theme.breakpoints.up('lg')]: {
            paddingTop: '6rem'
        }
    }
}));

export default SideBarLayout;
